<?php

namespace NewsBytes\WebComponents\App\Services;

use Illuminate\Support\Facades\App;

class ColorMapper
{
    public static function color($category)
    {
        $ColorMap = App::make('ColorMap');

        return array_get($ColorMap, $category, $ColorMap['default']);
    }

    public static function createCategoryMap()
    {
        $data = config('web-components.colorMap');
        $categoryMap = [];
        foreach ($data as $key => $coll) {
            foreach ($coll as $categoryItem) {
                $categoryMap[$categoryItem] = $key;
            }
        }

        return $categoryMap;
    }
}
