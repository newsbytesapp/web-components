<?php

namespace NewsBytes\WebComponents\App\Http\Controllers;

class StaticFiles extends Controller
{
    public function jsrenderTemplates($name, $version)
    {
        $view = response();
        if ($name == 'web-components') {
            $view = $view->view('web-components::jsrender.template', []);
        } elseif ($name == 'web-components-partner') {
            $view = $view->view('web-components::jsrender.template-partner', []);
        }

        return $view->header('Content-Type', 'text/html; charset=UTF-8')->header('Cache-Control', 'max-age=31622400, public');
    }
}
