(function (w,d,n) {

var callBackBinder = function (cb, context) {
    return function () {
        cb.apply(context, arguments);
    }
}

var checkFirebase = true;

var loadMore = {

    init: function () {
        this.token = $('meta[name="csrf-token"]').attr('content');
        this.id = null;
        this.loadMoreParent = null;
        this.registerEvents();
    },

    registerEvents: function () {
        $(d).on('click', '.load-more', callBackBinder(this.eventHandler, this));
    },

    eventHandler: function (event) {
        switch (event.type) {
            case 'click':
                this.id = '#' + $(event.target).attr('id');
                this.loadMoreParent = $(this.id).closest('.load-more-parent');
                this.loadMoreConfig = {
                    template: $(this.id).attr('data-template'),
                    url: $(this.id).attr('data-api'),
                    searchFor: $(this.id).attr('data-search-for'),
                    appendTo: $(this.id).attr('data-append-to')
                }
                if (this.validateConfig()) {
                    this.showLoadingState();
                    this.ajaxManager("get", this.loadMoreConfig['url']);
                } else {
                    return;
                }

                break;
        }
    },

    ajaxManager: function (reqType, reqUrl, dataToSend) {
        switch (reqType) {
            case 'get':
                $.ajax({
                    async: true,
                    type: "GET",
                    url: reqUrl,
                    data: dataToSend,
                    success: callBackBinder(this.successCb, this),
                    error: callBackBinder(this.errorCb, this),
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", this.token);
                    }
                });
                break;

            case 'post':
                $.ajax({
                    async: true,
                    type: "POST",
                    url: reqUrl,
                    data: dataToSend,
                    success: callBackBinder(this.successCb, this),
                    error: callBackBinder(this.errorCb, this),
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", this.token);
                    }
                });

                break;
        }

    },

    successCb: function (result, status, xhr) {
        if (status === "success" && result['success']) {
            this.renderManager(result['data']);
        } else {
            return;
        }
    },

    errorCb: function (xhr, status, error) {
        this.errorReporter();
    },

    renderManager: function (resp) {
        if (resp['list']['data'].length > 0) {
            var templateId = this.loadMoreConfig['template'];
            var html = $.render[templateId](resp);
            var appendLocation = $(this.id).closest(this.loadMoreConfig['searchFor']).find(this.loadMoreConfig['appendTo']);
            $(appendLocation).append(html);
            this.delayImageLoading();
            $(this.id).attr('data-api', resp['list']['button']['api']);
            if (($(this.id).closest(this.loadMoreConfig['searchFor']).find('.disabled-link').length > 0) && (typeof resp['list']['button']['url'] !== 'undefined')) {
                $(this.id).closest(this.loadMoreConfig['searchFor']).find('.disabled-link').attr('href', resp['list']['button']['url'])
            }
            this.resetDefaults();
            if (resp['list']['button'].length == 0) {
                this.hideLoadMoreFunctionality();
            }
        } else {
            this.hideLoadMoreFunctionality();
        }
    },

    errorReporter: function () {
        $(this.id).addClass('hidden');
        $(this.loadMoreParent).find('.dot-loader').removeClass('active');
        $(this.loadMoreParent).find('.flash-error').addClass('active');
        setTimeout(callBackBinder(this.resetDefaults, this), 1000);
    },

    resetDefaults: function () {
        $(this.id).removeClass('hidden');
        $(this.loadMoreParent).find('.dot-loader').removeClass('active');
        $(this.loadMoreParent).find('.flash-error').removeClass('active');
    },

    showLoadingState: function () {
        $(this.id).addClass('hidden');
        $(this.loadMoreParent).find('.dot-loader').addClass('active');
    },

    hideLoadingState: function () {
        $(this.id).removeClass('hidden');
        $(this.loadMoreParent).find('.dot-loader').removeClass('active');
    },

    hideLoadMoreFunctionality: function () {
        $(this.loadMoreParent).find('.dot-loader').removeClass('active');
        $(this.id).addClass('hidden');
    },

    validateConfig: function () {
        for (var item in this.loadMoreConfig) {
            if (typeof this.loadMoreConfig[item] === "undefined") {
                return false;
            }
        }
        return true
    },

    delayImageLoading: function () {
        var lazyLoader = Object.create(lazyLoadModule);
        lazyLoader.init('.lazy', { ios: { query: '.inset-img-hidden', device: isIos() } });
        lazyLoader.lazy();
    }
}

var querySubmitModule = {

    init: function () {
        this.token = $('meta[name="csrf-token"]').attr('content');
        this.id = null;
        this.registerEvents();
    },

    registerEvents: function () {
        $(d).on('click', '.query-submit', callBackBinder(this.querySubmitHandler, this));
        $(d).on('keyup', '.query', callBackBinder(this.triggerSubmitButton, this))
    },

    querySubmitHandler: function (event) {
        this.id = '#' + $(event.target).attr('id');
        this.queryParent = $(this.id).closest('.query-section');
        this.config = {
            url: $(this.id).attr('data-api'),
        }
        if (!this.validateConfig()) return;
        this.showLoadingState();
        var data = {};
        if ($(this.id).attr('data-type').toLowerCase() == 'post') {
            $(this.queryParent).find('.query').each(function (i, item) {
                data[$(item).attr('data-name')] = (Object.create(utilityModule).cleanInputField($(item).val()));
            })
        }
        this.ajaxManager($(this.id).attr('data-type').toLowerCase(), this.config['url'], data);
    },

    ajaxManager: function (reqType, reqUrl, dataToSend) {
        switch (reqType) {
            case 'get':
                $.ajax({
                    async: true,
                    type: "GET",
                    url: reqUrl,
                    success: callBackBinder(this.successCb, this),
                    error: callBackBinder(this.errorCb, this),
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", this.token);
                    }
                });
                break;

            case 'post':
                $.ajax({
                    async: true,
                    type: "POST",
                    url: reqUrl,
                    data: dataToSend,
                    success: callBackBinder(this.successCb, this),
                    error: callBackBinder(this.errorCb, this),
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", this.token);
                    }
                });

                break;
        }

    },

    successCb: function (result, status, xhr) {
        if (status === "success" && result['success']) {
            this.renderManager(result['data']);
        } else {
            this.renderManager(result['data']);
        }
    },

    errorCb: function (xhr, status, error) {
        this.errorReporter();
    },

    renderManager: function (resp) {
        var _this = this;
        var templateId = resp.template;
        var html = $.render[templateId](resp);
        var appendLocation = $(this.id).closest('.query-template-parent');
        $(appendLocation).append(html);
        this.resetDefaults();
        if ($(this.queryParent).hasClass('remove-section')) {
            this.removeQuerySection();
        } else {
            $(_this.id).addClass('hidden');
            setTimeout(function () {
                $(appendLocation).children().filter('.response-section').remove();
                $(_this.id).removeClass('hidden');
            }, 1000 * 7);
        }
    },

    errorReporter: function () {
        $(this.id).addClass('hidden');
        $(this.queryParent).find('.dot-loader').removeClass('active');
        $(this.queryParent).find('.flash-error').addClass('active');
        setTimeout(callBackBinder(this.resetDefaults, this), 1000 * 7);
    },

    resetDefaults: function () {
        $(this.id).removeClass('hidden active');
        $(this.queryParent).find('.dot-loader').removeClass('active');
        $(this.queryParent).find('.flash-error').removeClass('active');
        $(this.queryParent).find('.query').each(function () {
            $(this).val('');
        })
    },

    showLoadingState: function () {
        $(this.id).addClass('hidden');
        $(this.queryParent).find('.dot-loader').addClass('active');
    },

    hideLoadingState: function () {
        $(this.id).removeClass('hidden');
        $(this.queryParent).find('.dot-loader').removeClass('active');
    },

    validateConfig: function () {
        for (var item in this.config) {
            if (typeof this.config[item] === "undefined") {
                return false;
            }
        }
        return true
    },

    removeQuerySection: function () {
        $(this.queryParent).remove();
    },

    triggerSubmitButton: function (event) {
        var button = $(event.target).closest('.query-section').find('.query-submit');
        var list = $(event.target).closest('.query-section').find('.query');
        var emptyBox = false;
        list.each(function (i, item) {
            var value = $(item).val().trim();
            if (value == '') {
                emptyBox = true;
                return false;
            }
        });
        if (!emptyBox) {
            button.addClass('active');
        } else {
            button.removeClass('active');
        }
    }

}

var validateGlobals = function (key) {
    if (typeof w[key] !== "undefined") {
        return true;
    }
    return false;
}

var track = function (tAction, type, action, category, label) {
    if ((typeof dataLayer !== 'undefined') && (typeof tAction == 'object')) {
        dataLayer.push(tAction);
        return;
    }
    // validateGlobals('ga') ? ga(tAction, type, action, category, label) : null;
}

var lazyLoadModule = {

    defaultLazyConfig: {
        effect: 'fadeIn',
        visibleOnly: true,
        placeholder: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAYAAAA7KqwyAAAABGdBTUEAALGPC/xhBQAAAB1JREFUKBVjfPn2438GCgATBXrBWkcNYGAYBmEAAK02A9g3RmLgAAAAAElFTkSuQmCC',
        ios: {
            query: '.inset-img-hidden',
            device: false
        },
        onError: function(element) {
            console.log('lazy error loading ' + element.data('src'));
        }
    },

    defaultQuery: '.lazy',

    init: function (query, lazyConfig) {
        this.query = query || this.defaultQuery;
        this.lazyConfig = this.getLazyConfig(this.getLazyConfig({}, this.defaultLazyConfig), (lazyConfig ? lazyConfig : {}));
    },

    getLazyConfig: function (target, source) {
        return Object.assign(target, source);
    },

    lazy: function () {
        this.handleIosCase();
        $(this.query).Lazy(this.lazyConfig);
    },

    handleIosCase: function () {
        if (this.lazyConfig.ios.device)
            $(this.lazyConfig.ios.query).remove();
    },

    lazyOnEvent: function (elem, evtLazyConfig) {
        $(elem).Lazy(this.getLazyConfig((evtLazyConfig ? evtLazyConfig : {}), this.lazyConfig));
    }

}


var sliderConf = {
    variableWidth: {
        infinite: false,
        slidesToShow: 1,
        variableWidth: true,
        nextArrow: '<div class="bg-primary br-rd-pr-50 bt-half cs-ptr cs-ptr pos-abs slick-arrow tp-half wd-ht-px-50 z-index-2" style="transform: translateY(-50%);box-shadow: 0 2px 22px 0 rgba(0, 0, 0, 0.06); right:20px;"><div class="dp-fx fx-al-ct fx-js-ct ht-full wd-full"><span class="cl-link fz-22 nb-icon-angle-down mg-l-8" style="transform:rotate(-90deg);"></span></div></div>',
        prevArrow: '<div class="bg-primary br-rd-pr-50 bt-half cs-ptr cs-ptr pos-abs slick-arrow tp-half lt-init wd-ht-px-50 z-index-2" style="transform: translateY(-50%);box-shadow: 0 2px 22px 0 rgba(0, 0, 0, 0.06);"><div class="dp-fx fx-al-ct fx-js-ct ht-full wd-full"><span class="cl-link fz-22 nb-icon-angle-down mg-r-8" style="transform:rotate(90deg);"></span></div></div>',
        responsive: [
            {
                breakpoint: 767,
                settings: "unslick"
            }
        ]
    }
}

var slideshowModule = {
    defaultConfig: {
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        pauseSlideshow: function () {
            $(this.query).slick('slickPause');
        },
        resumeSlideshow: function name() {
            $(this.query).slick('slickPlay');
        }
    },

    defaultQuery: '.slide-show.default',

    defaultParent: '.slide-show-parent',

    init: function (query, config, parent) {
        this.query = query || this.defaultQuery;
        this.config = this.merger(this.merger({}, this.defaultConfig), (config ? config : {}));
        this.parent = parent || this.defaultParent;
    },

    merger: function (target, source) {
        return Object.assign(target, source);
    },

    activateSlideshow: function () {
        $(this.query).slick(this.config);
        $(this.parent).removeClass('visibility-hidden');
    },

    handleEvents: function () {
        $(this.query).on('beforeChange', this.handleBeforeChangeEvent.bind(this));
        $(this.query).on('afterChange', this.handleAfterChangeEvent.bind(this));
    },

    handleBeforeChangeEvent: function (event, slick, currentSlide, nextSlide) {
    },

    handleAfterChangeEvent: function (event, slick, currentSlide) {
        this.highlightActiveSlide(currentSlide);
        this.highlightProgressBars(currentSlide);
    },

    highlightActiveSlide: function (currentSlide) {
        $(this.query).closest(this.parent).find('.slide-items-mapper .slide-item').removeClass('active cl-primary');
        $(this.query).closest(this.parent).find('.slide-items-mapper #slide-item-' + currentSlide).addClass('active');
    },

    highlightProgressBars: function (currentSlide) {
        if ($(this.query).closest(this.parent).find('.slide-show-bars .slick-dots li').length < 1) return;
        $(this.query).closest(this.parent).find('.slide-show-bars .slick-dots li button').removeClass('active reset');
        $(this.query).closest(this.parent).find('.slide-show-bars .slick-dots li button').slice(0, currentSlide).addClass('active');
        $(this.query).closest(this.parent).find('.slide-show-bars .slick-dots li button').slice(currentSlide + 1).addClass('reset');
    }
}

var loggedIn = function (data) {
    var fallbackIcon = '', user = data.user;
    if ((!user) || (typeof user === 'undefined') || (user == null)) return;
    // User is signed in.
    var isAnonymous = user.isAnonymous;
    var displayName = 'Anonymous';
    var email = '';
    var emailVerified = false;
    var photoURL = '';
    if (!isAnonymous) {
        displayName = user.displayName;
        email = user.email;
        emailVerified = user.emailVerified;
        photoURL = user.providerData[0].photoURL;
        if ($('body').data('app') == 'pwa') {
            $('body').data("loginDetails", { dpName: displayName.split(" ")[0], imgSrc: photoURL, status: "in" });
        }
    }
    $('.login').data('anon', isAnonymous);
    var uid = user.uid;
    var phoneNumber = user.phoneNumber;
    var providerData = user.providerData;
    if ($('.login').data('status') == 'out') {
        if (displayName !== 'undefined' && displayName !== null) {
            $('.userText').text(displayName.split(" ")[0]);
        }
        $('.login').addClass('hidden');
        $('.userText').removeClass('hidden');
        $('.userIcon').removeClass('hidden');
        $('.signed-in-user').removeClass('hidden');
        $('.signupText').addClass('hidden');
        $('.login-dismiss').click();
        //var imageContainer = $('.userIcon');
        $('.userIcon').replaceWith('<img class="br-rd-pr-50 user-signed-in-img userIcon wd-ht-px-50" src="" alt="User Placeholder" title="User Placeholder"/>');
        $('.svg.avatar').remove();
        if (photoURL !== 'undefined' && photoURL !== null && photoURL !== '') {
            $('.userIcon').attr('src', photoURL);
        }
        else {
            $('.userIcon').attr('src', fallbackIcon);
        }
        $('.login').data('status', 'in');
        $('#lb-contest-container').data('login-status', 'complete');
        if ((!user.emailVerified)) {
            postAccount({ token: data.token, action: 'validate', status: 'success' });
            if ((providerData.length > 0) && (providerData[0].providerId == 'password')) {
                var flashMessage = $('.flashMessage');
                flashMessage.html('To ask a question, you will have to first verify your email address. <span class="emailconf">Click here to receive the verification email again.</span>');
                flashMessage.addClass('error');
                flashMessage.removeClass('hidden');
            }
        }
    }
}

var logoutInit = function (e) {
    postAccount({ token: '', action: 'logout', status: 'success' });
}

var logout = function (e) {
    if ($('.login').data('status') != 'in') {
        return;
    }
    // User is signed out.
    $('.userIcon').each(function (i, item) {
        fallbackIcon = $(item).attr('data-fallback-icon');
    });
    $('.login').removeClass('hidden');
    $('.signupText').removeClass('hidden');
    $('.userText').text('');
    $('.login').data('status', 'out');
    $('.userText').addClass('hidden');
    $('.userIcon').addClass('hidden');
    $('html').removeClass('ovr-hidden');
    $('#lb-contest-container').data('login-status', 'incomplete');
    if ($('body').data('app') == 'pwa') {
        $('body').removeData('loginDetails');
        $('.user-credentials').removeClass('logged-in');
    }
    $('.signed-in-user').addClass('hidden');
    initLoginBackground();
}

var firebaseError = function (errorCode) {
    if (errorcode in loginErrorCodes['email']) {
        $('.error-email').text(loginErrorCodes['email'][errorcode]);
        $('#email').addClass('error-border');
    } else if (errorcode in loginErrorCodes['password']) {
        $('.error-password').text(loginErrorCodes['password'][errorcode]);
        $('#password').addClass('error-border');
    } else if (errorcode in loginErrorCodes['name']) {
        $('.error-name').text(loginErrorCodes['name'][errorcode]);
        $('#email').addClass('error-border');
    } else if (errorcode in loginErrorCodes['others']) {
        $('.error-email').text(loginErrorCodes['others'][errorcode]);
        $('#email').addClass('error-border');
    } else {
        $('.error-email').text(loginErrorCodes['default']);
        $('#email').addClass('error-border');
    }
};

var renderLoginModal = function () {
    $('.login-overlay').css({ height: '100vh' });
    $('.login-overlay').data('overlay', 'true');
}

var initLoginBackground = function () {
    $('body').find('#loginZone').remove();
    var obj = { s: getSubdomain(location.host), p: location.protocol, locale: '' };
    if (typeof $('html').attr('lang') !== 'undefined') obj.locale = '/' + $('html').attr('lang');
    var loginFormHtml = $.render.loginModal(obj);
    // $('body').attr('data-side-drawer', 'off');
    $('body').append(loginFormHtml);
    loginFrame = $('body').find('#loginZone iframe').eq(0);
    $('.login-overlay').data('overlay', 'false');
    $('.login-overlay').css({ height: '0px', });
    $('#lb-contest-container').data('login-status', 'incomplete');
}

var closeLogin = function (e) {
    $('.login-overlay').css({ height: '0px', });
    // $('body').find('#loginZone').remove();
    $('html').removeClass('ovr-hidden');
    // $('body').attr('data-side-drawer', 'off');
    $('.login-overlay').data('overlay', 'false');
}

var postUserQuestion = function (data) {
    postAccount({ data: data, action: 'userQuestion', status: 'success' });
}

var postUserQuestionSuccess = function (data) {
    var form = $('.askQuestion.marked' + data.timelineId);
    form.removeClass('marked' + data.timelineId);
    if (data['status'] == 'success') {
        var flashMessage = form.find('.flashMessage'), loader = form.find('.loader').eq(0), question = form.find('.userQuestion');
        loader.html('');
        flashMessage.html('Question submitted successfully');
        flashMessage.show(100, function (e) { setTimeout(function (e) { flashMessage.hide(1000); flashMessage.addClass('hidden') }, 2000) });
        question.val('');
        if (($('.login').data('anon') === true) || ($('.login').data('anon') === 'true'))
            $('.logout').click();
    } else {
        return;
    }
}

var postUserQuestionError = function (data) {
    var form = $('.askQuestion.marked' + data.timelineId);
    var flashMessage = form.find('.flashMessage'), loader = form.find('.loader').eq(0), question = form.find('.userQuestion');
    form.removeClass('marked' + data.timelineId);
    loader.html('');
    var message = (data.code == 401) ? 'Unauthorized access. The question has not been submitted.' : 'Something went wrong. Please submit your question again.';
    flashMessage.text(message);
    flashMessage.addClass('error');
    flashMessage.removeClass('success');
    flashMessage.removeClass('hidden');
}

var checkForLoginModule = function () {
    if ((typeof loginModule !== 'undefined') && (loginModule === 'enabled')) {
        return true;
    }
    return false;
}

var receiveMessage = function (event) {
    if (!allowedDomain(event.origin))
        return;
    var obj = event.data;
    if (obj.status == 'error') {
        console.log('error posted: ', obj)
        return;
    }
    if (obj.action == 'loggedIn') {
        loggedIn(obj.data);
        closeLogin();
    } else if (obj.action == 'logout') {
        logout();
    } else if (obj.action == 'closeLogin') {
        closeLogin();
    } else if (obj.action == 'postUserQuestionSuccess') {
        postUserQuestionSuccess(obj.data);
    } else if (obj.action == 'postUserQuestionError') {
        postUserQuestionError(obj.data);
    }
}

var allowedDomain = function (origin) {
    return allowedDomainRegExp.test(origin);
}

function getSubdomain(hostname) {
    var regexParse = new RegExp('[a-z\-0-9]{2,63}\.[a-z\.]{2,5}$');
    var urlParts = regexParse.exec(hostname);
    return hostname.replace(urlParts[0], '').slice(0, -1);
}

var allowedDomainRegExp = new RegExp('^https?://([a-z0-9]+[.])+newsbytesapp[.](com|test)');
var loginFrame;

var postAccount = function (data) {
    data = JSON.parse(JSON.stringify(data));
    var urlobj = get_domain_from_url(loginFrame.attr('src'));
    loginFrame[0].contentWindow.postMessage(data, urlobj.join('//'));
}

function get_domain_from_url(url) {
    var a = d.createElement('a')
    a.setAttribute('href', url);
    return [a.protocol, a.hostname];
}

var ajaxTemplates = {
    init: function () {
        this.token = $('meta[name="csrf-token"]').attr('content');
        this.id = null;
        this.loadTemplateParent = null;
        this.registerEvents();
    },

    registerEvents: function () {
        $(d).on('click', '.load-template', callBackBinder(this.loadTemplateHandler, this));
        $(d).on('click', '.template-tab', callBackBinder(this.templateTabsClickHandler, this));
    },

    loadTemplateHandler: function (event) {
        this.id = '#' + $(event.target).closest('.load-template').attr('id');
        this.loadTemplateParent = $(this.id).closest('.load-template-parent');
        this.loadingPractise = $(this.id).attr('data-loading-practise');
        this.loadConfig = {
            template: $(this.id).attr('data-template'),
            url: $(this.id).attr('data-url'),
            componentType: $(this.loadTemplateParent).attr('data-component')
        }
        if (this.validateConfig()) {
            this.componentApproval(event);
        } else {
            return;
        }
    },

    templateTabsClickHandler: function (event) {
        $(event.target).closest('.load-template-parent').find('.tabs input').attr('checked', false);
        $(event.target).closest('.load-template-parent').find('#' + $(event.target).closest('.template-tab').attr('for')).attr('checked', true);
    },

    componentApproval: function (event) {

        switch (this.loadConfig['componentType']) {
            case 'ajax-tabs-header-dropdown':
                var mappedToVal = $(this.id).closest('.search-for').attr('data-mapped-to');
                var tabContentItem = $(this.loadTemplateParent).find('.content').filter('[data-mapped-to=' + mappedToVal + ']');
                var appendLocation = $(tabContentItem).find('.render');
                if (this.loadingPractise !== "loaded-once") {
                    $(appendLocation).empty();

                    this.showLoadingState();
                    this.ajaxManager("get", this.loadConfig['url']);
                }
                if ($(event.target).closest('.load-template').hasClass('change-title-drdown')) {
                    var title = $(event.target).closest('.load-template').text();
                    $(tabContentItem).find('.drdown-title').text(title);
                }
                break;
        }
    },

    ajaxManager: function (reqType, reqUrl, dataToSend) {
        switch (reqType) {
            case 'get':
                $.ajax({
                    async: true,
                    type: "GET",
                    url: reqUrl,
                    data: dataToSend,
                    success: callBackBinder(this.successCb, this),
                    error: callBackBinder(this.errorCb, this),
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", this.token);
                    }
                });
                break;

            case 'post':
                $.ajax({
                    async: true,
                    type: "POST",
                    url: reqUrl,
                    data: dataToSend,
                    success: callBackBinder(this.successCb, this),
                    error: callBackBinder(this.errorCb, this),
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", this.token);
                    }
                });

                break;
        }

    },

    successCb: function (result, status, xhr) {
        if (status === "success" && result['success']) {
            this.renderManager(result['data']);
        } else {
            return;
        }
    },

    errorCb: function (xhr, status, error) {
        this.errorReporter();
    },


    renderManager: function (resp) {

        switch (this.loadConfig['componentType']) {
            case 'ajax-tabs-header-dropdown':
                var templateId = this.loadConfig['template'];
                var html = $.render[templateId](resp);

                var mappedToVal = $(this.id).closest('.search-for').attr('data-mapped-to');
                var tabContentItem = $(this.loadTemplateParent).find('.content').filter('[data-mapped-to=' + mappedToVal + ']');
                var appendLocation = $(tabContentItem).find('.render');

                $(appendLocation).append(html);
                switch (templateId) {
                    case 'listItemLgSqrRecImg':
                    case 'listItemComparison':
                        $(tabContentItem).find('.header-title').text(resp['header']['title']);
                        break;
                }

                if (typeof this.loadingPractise !== "undefined") {
                    $(this.id).attr("data-loading-practise", "loaded-once");
                }

                this.modifyUrl();
                this.resetDefaults();
                this.delayImageLoading();
                break;
        }
    },

    errorReporter: function () {
        $(this.loadTemplateParent).find('.dot-loader').removeClass('active');
        $(this.loadTemplateParent).find('.flash-error').addClass('active');
        setTimeout(callBackBinder(this.resetDefaults, this), 1000);
    },

    resetDefaults: function () {
        $(this.loadTemplateParent).find('.dot-loader').removeClass('active');
        $(this.loadTemplateParent).find('.flash-error').removeClass('active');
    },

    showLoadingState: function () {
        $(this.loadTemplateParent).find('.dot-loader').addClass('active');
    },

    hideLoadingState: function () {
        $(this.loadTemplateParent).find('.dot-loader').removeClass('active');
    },

    validateConfig: function () {
        for (var item in this.loadConfig) {
            if (typeof this.loadConfig[item] === "undefined") {
                return false;
            }
        }
        return true;
    },

    modifyUrl: function () {

    },

    validateKey: function (key) {
        if (typeof key === "undefined") {
            return false;
        }
        return true;
    },

    delayImageLoading: function () {
        var lazyLoader = Object.create(lazyLoadModule);
        lazyLoader.init('.lazy', { ios: { query: '.inset-img-hidden', device: isIos() } });
        lazyLoader.lazy();
    }
}

var dropDown = {

    init: function (query, parent, drdownBox) {
        this.elem = query;
        this.elemParent = parent;
        this.box = drdownBox;
        this.animationOnEnterance = '';
        this.animationOnExit = '';
        this.registerEvents();
    },

    registerEvents: function () {
        $(d).on('click', 'body', callBackBinder(this.eventHandler, this));
    },

    eventHandler: function (event) {
        switch (event.type) {
            case 'click':
                // event.stopPropagation(); 
                var activeDrDownCount = $(this.box + '[data-status=on]').length;
                var buttonId = $(event.target).closest(this.elem).attr('id');
                var drDownType = $(this.box + '[data-status=on]').attr('data-type');
                switch (this.targetInfo(event)) {
                    case 'target clicked':
                        if (activeDrDownCount == 0) {
                            this.animationOnEnterance = $(this.elem + "#" + buttonId + "~" + this.box).attr('data-animation-entrance');
                            this.animationOnExit = $(this.elem + "#" + buttonId + "~" + this.box).attr('data-animation-exit');
                            $(this.elem + "#" + buttonId + "~" + this.box).removeClass(this.animationOnExit).addClass(this.animationOnEnterance);
                            $(this.elem + "#" + buttonId + "~" + this.box).attr('data-status', 'on');
                            $(this.elem + "#" + buttonId).addClass('active');
                            this.handleShareLinks(buttonId);
                            this.handleDrDownWithState(buttonId);
                            evtEmitter.emit('pauseSlideshowOnDropdownOpen');
                        } else if (activeDrDownCount > 0) {
                            this.resetDefaults(event, activeDrDownCount);
                            evtEmitter.emit('resumeSlideshowOnDropdownClose');
                        }
                        break;

                    case 'target missed':
                        switch (drDownType) {
                            case 'statefull':
                                if ($(event.target).closest('.drdown-checkbox').length > 0) {
                                    this.handleCheckboxes(event);
                                } else {
                                    this.resetDefaults(event, activeDrDownCount);
                                }
                                break;
                            default:
                                this.resetDefaults(event, activeDrDownCount);
                                evtEmitter.emit('resumeSlideshowOnDropdownClose');
                                break;
                        }
                        break;
                }

                break;
        }
    },

    resetDefaults: function (event, activeDrDownCount) {
        if (activeDrDownCount > 0) {
            $(this.box + '[data-status=on]').each(function (index, item) {
                $(item).removeClass(this.animationOnEnterance).addClass(this.animationOnExit);
                $(item).attr('data-status', 'off');
            });
            $(this.elem).removeClass('active');
        } else {
            return;
        }
    },

    targetInfo: function (event) {
        var targetedAreaCount = $(event.target).closest(this.elem).length;
        if (targetedAreaCount > 0) {
            return 'target clicked';
        } else {
            return 'target missed';
        }
    },

    handleCheckboxes: function (event) {
        var target = $(event.target).closest('.drdown-checkbox');
        var filterFor = target.attr('data-filter-for');
        target.find('input:not(:checked)').removeClass('is-checked');
        target.find('input:checked').addClass('is-checked');
        $('#' + target.closest('.table-section').attr('id')).find('table tbody tr td[data-cell=' + filterFor + ']').each(function (i, item) {
            if (target.find('input').hasClass('is-checked')) {
                $(item).removeAttr('style');
                $(item).addClass('imp');
                $(item).removeClass('show-not');
            } else {
                $(item).css({ display: 'none' });
                $(item).removeClass('imp');
            }
        })
    },

    handleDrDownWithState: function (buttonId) {
        var button = $('#' + buttonId);
        var tableCells = button.closest('.table-section').find('table tbody tr').first().find('td');
        if (tableCells.length < 1) return;
        var openedDrDown = button.closest(this.elemParent).find(this.box);
        tableCells.each(function (i, item) {
            if ($(item).css('display') === 'none') return;
            openedDrDown.find('label[data-filter-for=' + $(item).attr('data-cell') + '] input').attr('checked', true).addClass('is-checked');
        })
    },

    handleShareLinks: function (buttonId) {
        $(this.elem + "#" + buttonId + "~" + this.box).find('.share-link').each(function (index, item) {
            var link = $(item).attr('data-share-link');
            var newLink = link.replace('<url>', w.location.href);
            $(item).attr('href', newLink);
        });
    },

}

var tableModule = {

    defaultQuery: '.table-section',

    init: function (query) {
        this.query = query || this.defaultQuery;
    },

    getParentTableId: function (target) {
        return '#' + $(target).closest(this.query).attr('id');
    },

    handleEvents: function () {
        $(d).on('click', 'td.sort-rows', this.handleRowSorting.bind(this));
        $(d).on('keyup', '.search-in-table', this.handleRowSearch.bind(this))
    },

    handleRowSorting: function (event) {
        var target = $(event.target).closest('.sort-rows');
        var parentId = this.getParentTableId(target);
        var n = $(target).closest('tr').find('td').index(target);
        var rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        switching = true;
        dir = "asc";
        while (switching) {
            switching = false;
            rows = $(parentId).find('tbody tr.sortable');
            for (i = 0; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                switchcount++;
            } else {
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    },

    handleRowSearch: function (event) {
        var value = event.target.value.toLowerCase().trim();
        var parentId = this.getParentTableId(event.target);
        var firstRow = $(parentId).find('tbody tr').first();
        var searchInColumn = $(firstRow).find('td.search-rows').first();
        var n = $(firstRow).find('td').index(searchInColumn);
        $(parentId).find('tbody tr.searchable').each(function (i, item) {
            var text = $($(item).find('td')[n]).text().toLowerCase().trim();
            $(item).toggle(text.indexOf(value) !== -1);
        });
    }

}

var checkUptoTouchSize = function () {
    var screenWd = w.innerWidth || d.documentElement.clientWidth || d.body.clientWidth;
    var screenHt = w.innerHeight || d.documentElement.clientHeight || d.body.clientHeight;
    if (screenWd < 1025) {
        return true;
    }
    return false;
}

var touchBacked = function () {
    if (typeof touchSupport !== 'undefined' && touchSupport) {
        return true;
    }
    return false;
}

function htmlLayout(query) {

    this.query = query || '.extensible-layout';

    this.init = function () {
        $(this.query).each((index, item) => {
            this.handleLayouts($(item).attr('data-layout'), $(item).attr('data-breakpoint'), $(item).attr('id'));
        });
        this.htmlArranged();
    };

    this.handleLayouts = function (layout, breakPoint, id) {
        switch (layout) {
            case 'two-column':
                this.twoColumnLayout(breakPoint, id);
                break;
        }
    };

    this.twoColumnLayout = function (breakPoint, id) {
        var twoColumnSize = parseInt(breakPoint) || 1025;
        if (w.innerWidth >= twoColumnSize) {
            var container = $('#' + id);
            container.find('.layout-left').each((i, item) => {
                $(item).appendTo(container.find('.extended-container[data-col="left"]'));
            });
            container.find('.layout-right').each((i, item) => {
                $(item).appendTo(container.find('.extended-container[data-col="right"]'));
            });
        }
    };

    this.htmlArranged = function () {
        $('body').removeClass('html-arranging');
    };

    this.init();
}

function stickySidebar(query) {

    this.query = query || '.sidebar.sticky-nature';
    this.init = function () {
        if (!this.stickyPluginPresent()) return;
        this.createStickyInstance();
    }

    this.createStickyInstance = function () {
        $(this.query).each((i, item) => {
            if (!this.isAlreadySticked(item)) {
                var id = '#' + this.getItemId(item);
                if (this.validateItemExistance(id)) {
                    var stickyConfig = this.getStickyConfig();
                    $(id).stickySidebar(stickyConfig);
                }
            }
        });
    }

    this.getStickyConfig = function () {
        return {
            topSpacing: 0,
            bottomSpacing: 0,
            containerSelector: '.sticky-sidebar-parent',
            innerWrapperSelector: '.sidebar__inner',
            stickyClass: 'is-sticked',
            resizeSensor: true,
            minWidth: 0
        };
    }

    this.validateItemExistance = function (id) {
        return $(id).length;
    }

    this.getItemId = function (item) {
        return $(item).attr('id');
    }

    this.isAlreadySticked = function (item) {
        return $(item).hasClass('.is-sticked');
    }

    this.stickyPluginPresent = function () {
        return validateGlobals('StickySidebar');
    }

    this.init();
}

var navigation = {
    init: function (isNotSticky) {
        this.topNav = $('#header-nav').find('.top-nav');
        this.sideNav = $('#header-nav').find('.side-nav');
        this.lastScrollTop = 0;
        this.isSticky = (isNotSticky == undefined) ?  true : !isNotSticky;
        this.registerEvents();
        this.addScrollToSliderImage();
        var headerInterval = setInterval(() => {
            if ($('body').hasClass('styles-loading')) return;
            clearInterval(headerInterval);
            this.populatePrimaryHeader();
            this.adjustPrimaryHeader();
            if(this.isSticky){
                $('.content-wrapper').css({ paddingTop: this.topNav.height() });
            }
            else 
            {
                $('.content-wrapper').addClass('pd-t-0');
                this.topNav.css({
                    position : 'relative'
                });
            }
        }, 150)
    },

    addScrollToSliderImage: function () {
        $('.img-slider').addClass('hz-scroll-ac');
    },

    populatePrimaryHeader: function () {
        var items = $('.slab').clone();
        var listHtml = '';
        for(let i= 0;i<items.length;i++){
          listHtml += '<li class="ovr-elem">' + items.eq(i).removeClass('md-ter-bd-3 ter-bd-3 pd-b-20 pd-l-10 pd-t-10 wd-full dp-ib slab').addClass('sel selector-url md-ter-reg-2 ter-reg-2 selector-label').prop('outerHTML') + '</li>';
        }
        $('ul.overflow-check').html(listHtml);
    },

    registerEvents: function () {
        $(d).on('click', '.side-drawer-toggle, .complete-tint', () => this.manageSideDrawer());
        $(d).on('click', '.accordion-selector', (event) => this.manageAccordion(event));
        $(d).on('click', '.menu-item', (event) => this.menuclick(event));
        $(d).on('click', 'footer a', (event) => this.footerclick(event));
        $(d).on('click', '.download-banner a', (event) => this.sidebardownloadclick(event));
        $(d).on('click', '.social-share a', (event) => this.socialshareclick(event));
        $(d).on('click', 'a.clickable-logo', (event) => this.logoclick(event));
        $(d).on('click', '.search-for', (event) => this.openSearchBar(event));
        $(d).on('click', '.cancel-search', () => this.cancelSearchBar());
        $(d).on('keypress', '.search-query', (event) => {
            if (event.keyCode == 13)
                this.searchResults();
        });
        $(window).scroll(() => this.scrollBehaviour());
    },

    scrollBehaviour: function (event) {
        if (checkForLoginModule()) {
            evtEmitter.emit('initLoginFrame');
        }
        var st = $(window).scrollTop();
        var height = this.topNav.outerHeight();
        var defaultTopNavConfig = {
            top: 0
        }
        if(this.isSticky){
            if ((st > this.lastScrollTop) && (st > height)) {
                this.topNav.css({
                    top: -(height + 4)
                });
            } else if ((st <= this.lastScrollTop) && (st > height)) {
                this.topNav.css(defaultTopNavConfig);
            } else {
                this.topNav.css(defaultTopNavConfig);
            }
            this.lastScrollTop = st;
        }
        evtEmitter.emit('initAnalyticsScripts');
    },

    searchResults: function () {
        let splitExp = w.location.href.split(/^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/);
        $('.search-query').each(function (i, item) {
            let query = $(item).val();
            if (query !== '') {
                track({
                    'event':'site_search',
                    'search_text': query,
                    'User_ID': undefined
                });
                var util = Object.create(utilityModule);
                w.location = splitExp[1] + splitExp[3] + '/search?q=' + util.rawUrlEncode(util.cleanInputField(query));
            }
        });
    },

    manageSideDrawer: function () {
        var sideDrawerStatus = $('body').attr('data-side-drawer');
        if (this.validateKey(sideDrawerStatus)) {
            evtEmitter.emit('initLoginFrame');
            switch (sideDrawerStatus) {
                case 'on':
                    this.sideNav.removeClass('ent-slide-left')
                    $('body').attr('data-side-drawer', 'off');
                    $('html').removeClass('ovr-hidden');
                    break;

                default:
                    this.sideNav.addClass('ent-slide-left');
                    $('body').attr('data-side-drawer', 'on');
                    $('html').addClass('ovr-hidden');
                    track({
                        'event':'hamburger_menu_clicks',
                        'menu_name':'hamburger menu',
                        'User_ID': undefined
                    });
                    break;
            }

        }
    },

    manageAccordion: function (event) {
        var selectedSlab = $(event.target).closest('.accordion-slab');
        $(selectedSlab).addClass('selected');
        $(event.target).closest('.accordion').find('.accordion-slab').each(function (i, item) {
            if ($(item).hasClass('selected'))
                return;
            $(item).find('.list').slideUp(225);
            $(item).find('.accordion-selector').removeClass('selected');
        });
        $(selectedSlab).find('.accordion-selector').toggleClass('selected');
        $(selectedSlab).find('.list').slideToggle(225);
        $(selectedSlab).removeClass('selected');
    },

    socialshareclick: function (e) {
        var name = 'download-box', href = getPageUrl(), platform = $(e.currentTarget).text().trim().toLowerCase();
        track({
            'event':'social_share',
            'article_name': d.title,
            'article_category': $('.cover-card').text().trim().toLowerCase(),
            'shared_platform': platform,
            'article_id': href,
            'User_ID': undefined
        });
    },

    sidebardownloadclick: function (event) {
        track({
            'event':'app_download',
            'download_link_placement':'sidebar',
            'User_ID': undefined
        });
    },

    footerclick: function (e) {
        e.preventDefault();
        e.stopPropagation();
        var name = $(e.currentTarget).text().toLowerCase(), href = $(e.currentTarget).attr('href');
        track({
            'event': 'footer_click',
            'menu_name': name,
            'User_ID': undefined
        });
        w.setTimeout(function() {
            w.location.href = href;
            // redirectWithReferrer(href, location.href.replace(/#.*/, ''));
        }, 200);
        return false;
    },

    menuclick: function (e) {
        _this = this;
        e.preventDefault();
        e.stopPropagation();
        var name = $(e.currentTarget).text().toLowerCase(), href = $(e.currentTarget).attr('href');
        track({
            'event': 'top_navigation_menu_click',
            'menu_name': name,
            'sub_menu_name': '',
            'User_ID': undefined
        });
        w.setTimeout(function() {
            if(typeof(partner) != 'undefined'){
                if($('body').attr('data-side-drawer') == 'on'){
                    _this.manageSideDrawer();
                }
                showInterstialAd(e.currentTarget.href);
            }else{
                w.location.href = href;
            }
            // redirectWithReferrer(href, location.href.replace(/#.*/, ''));
        }, 200);
        return false;
    },

    logoclick: function(e) {
        _this = this;
        e.preventDefault();
        e.stopPropagation();
        var name = 'logo', href = $(e.currentTarget).attr('href');
        if (href.indexOf('http') !== 0) {
            href = w.location.protocol+'//'+w.location.host+href;
        }
        track({'event':'logo_click'});
        w.setTimeout(function() {
            if(typeof(partner) != 'undefined'){
                if($('body').attr('data-side-drawer') == 'on'){
                    _this.manageSideDrawer();
                }
                showInterstialAd(e.currentTarget.href);
            }else{
                w.location.href = href;
            }
            // redirectWithReferrer(href, location.href.replace(/#.*/, ''));
        }, 200);
        return false;
    },

    openSearchBar: function (event) {
        event.preventDefault();
        var bar = $(event.target).closest('.top-bar');
        bar.attr('data-search', 'on');
        bar.find('.search-query').focus();
    },

    cancelSearchBar: function () {
        $('.top-bar').attr('data-search', 'off');
        $('.search-query').each(function (i, item) {
            $(item).val('');
        })
    },

    validateKey: function (key) {
        if (typeof key === "undefined") {
            return false;
        }
        return true;
    },

    adjustPrimaryHeader: function () {
        var bar = $('.top-bar.lg-screen');
        var delta = 100;
        var lastBoxPos = bar.find('.last-box')?.position()?.left;
        var ovrBox = bar.find('.overflow-check');
        var isHeaderFloating = false;
        if (lastBoxPos && ((ovrBox.position().left + ovrBox.width() + delta) >= lastBoxPos) ) {
            isHeaderFloating = true;
        }
        if (isHeaderFloating) {
            var list = [];
            ovrBox.find('.ovr-elem').each(function (i, item) {
                if (lastBoxPos && ((delta + $(item).position().left + $(item).width()) >= lastBoxPos)) {
                    list.push(item);
                }
            });
            list.forEach(function (item) {
                $(item).remove();
            });
            var html = this.populateExtraItems(list);
            $(ovrBox).append(html);
        }
        ovrBox.removeClass('visibility-hidden');
    },

    populateExtraItems: function (list) {
        var resp = {
            selector: { title: $('.top-bar .aux-text').text(), linkable: false }, content: { data: [] }
        };
        list.forEach(function (item) {
            var dItem = {};
            dItem['name'] = $(item).find('.selector-label').text();
            dItem['url'] = $(item).find('.selector-url').attr('href');
            dItem['selected'] = $(item).find('.selector-url').hasClass('selected');
            resp['content']['data'].push(dItem);
        });
        var html = $.render.dropdownHoverableButton(resp);
        return html;
    },

    highlightPrimarySelectorLabel: function (primaryHeaderItemsLookup, selectedItem) {
        var items = $('.top-bar.lg-screen').find('.overflow-check .sel').toArray();
        var extraItems = $('.top-bar.lg-screen').find('.overflow-check .drdown-box .hoverable-item').toArray();
        var primaryHeaderItems = items.concat(extraItems);
        var lookupKeys = Object.keys(primaryHeaderItemsLookup);
        var index = lookupKeys.indexOf(selectedItem.toLowerCase());
        if (typeof primaryHeaderItems[index] !== 'undefined') {
            $(primaryHeaderItems).removeClass('selected');
            $(primaryHeaderItems[index]).addClass('selected');
        }
    }

}

var verticalTabSelector = {
    $verticalTabs: null,
    $tabContents: null,
    observer: null,
    $stickyDiv:null,
    $verticalTabContent:null,
    active:true,
    onscrollHighlight:true,
    

    init: function () {
        var _this = this;
        this.$tabContents = $(".vtab-content");
        if(this.$tabContents.length<1){
            this.active = false;
            return;
        } 
        this.$verticalTabs = $(".vertical-tab");
        this.$stickyDiv = $(".vertical-tab-sticky-div");
        this.$verticalTabContent = $(".vertical-tab-content");

        // this.setupObserver();
        // this.$verticalTabs.first().addClass("active"); 
        if (this.$verticalTabs.length === 1)
        {
            return;
        }         
        this.attachClickHandlers();
    },

    setupObserver: function () {
        var _this = this;
        this.observer = new IntersectionObserver(
            (entries) => {
                entries.forEach((entry) => {
                    if (entry.isIntersecting) {
                        const tabId = $(entry.target).attr("id");
                        _this.$verticalTabs.removeClass("active");
                        $(`.vertical-tab[data-tab-id="${tabId}"]`).addClass("active");
                    }
                });
            },
            {
                root: $(".vertical-tab-content")[0],
                threshold: 0.8,
            }
        );

        $(".vtab-content").each(function () {
            _this.observer.observe(this);
        });
    },

    attachClickHandlers: function () {
        var _this = this;
        this.$verticalTabs.off("click");
        this.$verticalTabs.click(function () {
            const tabId = $(this).attr("data-tab-id");
            _this.$verticalTabs.removeClass("active");
            $(`.vertical-tab[data-tab-id="${tabId}"]`).addClass("active");
            const $tabContent = $(`#${tabId}`);
            _this.onscrollHighlight  = false;
            $tabContent[0].scrollIntoView({
                behavior: "instant",  
                block: "start", 
            });
        });
        $(window).on("scroll", function() {
            // if(_this.$verticalTabs.eq(_this.$verticalTabs.length - 2).hasClass("active") && _this.checkStickyDivScrollComplete()) 
            // {
            //     _this.$verticalTabs.removeClass("active");
            //     _this.$verticalTabs.eq(_this.$verticalTabs.length - 1).addClass("active");
            //     return;
            // }
            if(_this.onscrollHighlight)
            _this.highlightActiveTab();
            else 
            _this.onscrollHighlight = true;
            if ($(window).width() < 768 && ($(".vertical-tab").length > 3) ) {
                _this.scrollHorizontallyActiveTabIntoView();
            }
        });
        // $(d).on("click", ".tab-item", () => {
        //      _this.$tabContents.last().css("padding-bottom",  40 + "px");
        // });
    },

    highlightActiveTab: function(){
        var _this = this;
        var stickyDivPosition = _this.$stickyDiv.offset().top;        
        _this.$tabContents.each(function(index){
            var sectionTop = $(this).offset().top;
            if (stickyDivPosition >= sectionTop - 55) {
                _this.$verticalTabs.removeClass("active");
                _this.$verticalTabs.eq(index).addClass("active");
            }

        });
    },
    checkStickyDivScrollComplete: function() {
        const windowHeight = w.innerHeight; 
        const documentHeight = d.documentElement.scrollHeight; 
        const scrollPosition = w.scrollY || w.pageYOffset || d.body.scrollTop + (d.documentElement.scrollTop || 0);
        return (documentHeight - windowHeight - scrollPosition) <= 1; 
    },

    scrollHorizontallyActiveTabIntoView: function()
    {   var verticalTabActive = $(".vertical-tab.active");
        const viewportWidth = $(window).width();
        const elementLeft = verticalTabActive[0].getBoundingClientRect().left;
        const elementinViewport = verticalTabActive[0].getBoundingClientRect().right >= 0 && elementLeft + 60 <= viewportWidth;
        var scrollAmount =  $(".vertical-tabs").scrollLeft() + elementLeft;
        if (!elementinViewport)
        $(".vertical-tabs").scrollLeft(scrollAmount);
        // $(".vertical-tabs").scrollLeft += elementLeft;
    }

};

var reactionShare = {
    init: function () {
        this.registerEvents();
        this.widget = $('.reaction-widget');
        this.reactionInitiator = this.widget.find('.reaction-initiator');
        this.elemStack = this.widget.find('.stack-item');
    },

    registerEvents: function () {
        $(d).on('click', 'body', callBackBinder(this.eventHandler, this));
    },

    eventHandler: function (event) {
        switch (event.type) {
            case 'click':
                if ($(event.target).closest('.reaction-select').length > 0) {

                    $('.reaction-select').removeClass('active');
                    $(event.target).closest('.reaction-select').addClass('active');
                    var selectedReaction = $(event.target).closest('.reaction-select').attr('data-reaction');
                    this.reactionsList.forEach(function (item) {
                        if (item['val'] == selectedReaction) {
                            item['st'] = true;
                        } else {
                            item['st'] = false;
                        }
                    });
                    var reactionText = reactionManager[selectedReaction]['text'];
                    this.widget.find('.sticker-lg-version').attr('data-reaction', selectedReaction);
                    this.widget.find('.sticker-text').text(reactionText);
                    this.elemStack.removeClass('hidden');
                    this.hideInitiator();
                    this.modifyUrl();

                } else if ($(event.target).closest('.bottom-share').length > 0) {

                    this.elemStack.filter('.stack-item-2, .stack-item-4').each(function (index, item) {
                        $(item).removeClass('hidden');
                    });
                    this.hideInitiator();
                    this.modifyUrl();

                } else if ($(event.target).closest('.reaction-cancel').length > 0) {

                    this.resetDefaults();

                } else if ($(event.target).closest('.copy-link').length > 0) {

                    let link = $(event.target).closest('.copy-link');
                    copyToClipboard(link.siblings('.link-val')[0]);
                    link.siblings('.link-flag').removeClass('visibility-hidden');
                    setTimeout(function () {
                        link.siblings('.link-flag').addClass('visibility-hidden');
                    }, 2000);

                } else {
                    this.resetDefaults();
                }
                break;
        }
    },

    hideInitiator: function () {
        this.reactionInitiator.addClass('hidden');
    },

    resetDefaults: function () {
        this.widget.find('.stack-item').addClass('hidden');
        this.reactionInitiator.removeClass('hidden');
    },

    getReactionList: function () {
        var list = [];
        this.widget.find('.reaction-select').each(function (index, item) {
            let r = {};
            r['val'] = $(item).attr('data-reaction');
            r['st'] = $(item).hasClass('active') ? true : false;
            list.push(r);
        });
        return list;
    },

    stimulateReactions: function (list) {
        this.reactionsList = list;
    },

    showActiveReactions: function (list) {
        this.widget.find('.reaction-select').each(function (index, item) {
            $(item).attr('data-reaction', list[index]['val']);
            list[index]['st'] ? $(item).addClass('active') : $(item).removeClass('active');
        });
    },

    modifyUrl: function () {
        this.widget.find('.modify-url').each(function (i, item) {
            var splitUrl = w.location.href.split(/^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/);
            let url = $(item).attr('data-url').replace(/<url>/g, splitUrl[1] + splitUrl[3] + splitUrl[5]).replace(/<text>/g, $(item).attr('data-share-text'));
            $(item).attr('href', url);
        });
    },

    updateShareText: function (txt) {
        this.widget.find('.modify-url').each(function (i, item) {
            $(item).attr('data-share-text', txt);
        });
    }
}

var utilityModule = {

    encodeUrlFragment: function (url) {
        return encodeURIComponent(url);
    },

    encodeUrl: function (url) {
        return encodeURI(url);
    },

    cleanInputField: function (inputText) {
        return this.removeDomElements(inputText).trim();
    },

    removeDomElements: function (inputText) {
        var regex = /(\<\w*)((\s\/\>)|(.*\<\/\w*\>))/igm;
        if (regex.exec(inputText)) {
            return inputText.replace(regex, '');
        }
        return inputText;
    },

    rawUrlEncode: function (url) {
        return this.encodeUrlFragment(url).replace(/[!'()*]/g, function (c) {
            return '%' + c.charCodeAt(0).toString(16);
        });
    },
}

var eventCards = {
    init: function () {
        if ($('.events-list-holder').length == 0) {
            return;
        }

        // Initial State Variables
        this.token = $('meta[name="csrf-token"]').attr('content');
        this.currentArticleIndex = 0;
        this.scrollPositionCoverCards = [0];
        this.articlesCoordinates = [];
        this.idsForLoadingNextArticlesList = [];
        this.articlesLoadedList = [{ url: w.location.href, title: $('.events-list-holder').attr('data-title'), tag: $('.events-list-holder').attr('data-tag'), creatorId: ((typeof $('.events-list-holder').attr('data-creator-id') !== 'undefined') ? $('.events-list-holder').attr('data-creator-id') : null), eventsListWrapperId: $('[id|=wrapper-events-list]').first().attr('id') }];
        this.clonedStickyTitle = this.articlesLoadedList[0]['title'];
        this.lastScrollTop = 0;
        this.loadingNextEvents = false;
        //set configuration for first aricle - SmartOcto Analytics
        /* if (typeof _ain !== 'undefined') {
            this.smartOctoArticlesList = [{
                url: _ain.url,
                postid: _ain.postid,
                maincontent: _ain.maincontent,
                title: _ain.title,
                pubdate: _ain.pubdate,
                authors: _ain.authors,
                tags: _ain.tags
            }];
        } */
        this.nextArticleLoadUrl = ((typeof $('.events-list-holder').attr('data-next-latest') !== 'undefined') ? $('.events-list-holder').attr('data-next-latest') : $('.events-list-holder').attr('data-next'));
        this.scrollTriggered = false;
        this.notificationSubsChecked = false;
        this.urlQueryString = getFilteredQueryParams();

        //Mandatory Operations
        this.setIdForLoadingNextArticles();
        this.listenToHeightChange();
        this.hookUpEvents();
        this.storeArticleCoordinates();
        this.updateArticleNavigation();
        this.updateArticleCardStats();

        this.onSliderTabsClickEventHandler(this.articlesLoadedList[0]['eventsListWrapperId']);

        //Referencing Events Callbacks
        this.onScrollEventHandler = this.onScrollEventHandler.bind(this);
        this.onSliderTabsClickEventHandler = this.onSliderTabsClickEventHandler.bind(this);
        this.onArticleStatsClickEventHandler = this.onArticleStatsClickEventHandler.bind(this);
        this.checkForScrollOnIntervalsHandler = this.checkForScrollOnIntervalsHandler.bind(this);
        this.onLikeButtonClickEventHandler = this.onLikeButtonClickEventHandler.bind(this, {query: '.like-button'});

        //Registering Events
        $(window).scroll(this.onScrollEventHandler);
        $(d).on('click', '.slider-tabs .tab-item', this.onSliderTabsClickEventHandler);
        $(d).on('click', '.article-stats', this.onArticleStatsClickEventHandler);
        $(d).on('click', '.like-button', this.onLikeButtonClickEventHandler);
        // $(d).on('click', '.next-article-button', event => this.handleArticleNavigationClick(event, true));
        // $(d).on('click', '.prev-article-button', event => this.handleArticleNavigationClick(event, false));
        setInterval(this.checkForScrollOnIntervalsHandler, 100);
    },

    onScrollEventHandler: function () {
        if ($(window).scrollTop() + $(window).height() + 500 > $(d).height() - (0.5 * $(window).height()) && !this.loadingNextEvents) {
            if (! (typeof allowNavigation != 'undefined' && allowNavigation)) {
                this.loadNextEvents();
            }else{
                // this.hideArticleNaviagtion();
            }
        }

        this.scrollTriggered = true;
        var article = $('#' + this.articlesLoadedList[this.currentArticleIndex]['eventsListWrapperId']).find('article');
        let st = $(window).scrollTop();
        let currentCardIndex = -1;

        if(st > this.lastScrollTop){
            article.find('.jump-at').each((index, item) => {
                if($(item).offset().top > 0 && st > $(item).offset().top){
                    currentCardIndex = index;
                };
            });
            if(currentCardIndex > -1 && !article.find('.jump-at').eq(currentCardIndex).attr('data-card-read')){
                var percentageOfCardsRead = Math.round(((currentCardIndex + 1)/article.find('.jump-at').length)*100) +"%";
                article.find('.jump-at').eq(currentCardIndex).attr('data-card-read', true);

                track({
                    'event': 'card_depth_scroll',
                    'url': w.location.href,
                    'depth_percentage': percentageOfCardsRead,
                });
            }
        }

        if (typeof(partner) == 'undefined') {
            // if ($(window).scrollTop() > 1700) {
            //     this.notifPopupManager();
            // }
            this.highlightProgressBar();
            this.makeTitlesSticky();
        }
    },

    checkForScrollOnIntervalsHandler: function () {
        if (this.scrollTriggered) {
            this.changeCurrentArticleIndex();
            this.scrollTriggered = false;
        }
    },

    setIdForLoadingNextArticles: function () {
        var urlSegments = this.nextArticleLoadUrl.split('/');
        var length = urlSegments.length;
        var i = 0;
        while (length > i) {
            i++;
            if (urlSegments[length - i] !== '') {
                this.idForLoadingNextArticles = parseInt(urlSegments[length - i]);
                break;
            }
        }
    },

    loadNextEvents: function () {
        this.loadingNextEvents = true;

        if (this.idsForLoadingNextArticlesList.indexOf(this.idForLoadingNextArticles) != -1) {
            return;
        }
        this.idsForLoadingNextArticlesList.push(this.idForLoadingNextArticles);
     //     validateGlobals('ga') ? ga('send', 'event', 'next', 'timeline', w.location.href) : null;
        track('send', 'event', 'next', 'timeline', w.location.href);
        if (typeof(partner) != 'undefined' && partner) {
            track('partner.send', 'event', 'next', 'timeline', w.location.href);
        }

        $.ajax({
            async: true,
            type: "GET",
            url: this.nextArticleLoadUrl + this.urlQueryString,
            success: callBackBinder(this.successCbForEventsLoading, this),
            error: callBackBinder(this.errorCbForEventsLoading, this),
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", this.token);
            }
        });
    },

    successCbForEventsLoading: function (result, status, xhr) {
        if (status === "success" && result['success']) {
            this.renderManagerForEventsLoading(result['data']);
        } else {
            return;
        }
    },

    errorCbForEventsLoading: function (xhr, status, error) {
        this.idsForLoadingNextArticlesList.splice(-1, this.idForLoadingNextArticles);
    },

    renderManagerForEventsLoading: function (resp) {
        var eventsResp = Object.assign({}, resp)
        eventsResp['events'] = eventsResp['events'].filter(function (item, index) {
            return ($('#wrapper-events-list-' + item['htmlId']).length == 0);
        });

        let html;

        if (typeof(partner) != 'undefined' && partner) {
            eventsResp['events'] = eventsResp['events'].map(function (item) {
                if (typeof(partnerLogo) != 'undefined' && partnerLogo) {
                    item['partnerLogo'] = partnerLogo;
                }
                if (typeof(item['clickbaityTitle']) != 'undefined' && item['clickbaityTitle']) {
                    item['title'] = item['clickbaityTitle'];
                }

                item['tagColor'] = 'brand';
                return item;
            });

            html = $.render.v2articleCardPartner(eventsResp);
        }else{
            html = $.render.v2articleCard(eventsResp);
        }


        $('.append-events-list').append(html);

        if (typeof(partner) != 'undefined' && partner) {
            eventsResp['events'].forEach((event, index) => {
                var embedAdSlots = [];
                var article = $('article#' + 'article-' + event['htmlId']);
                var section = $('.embed-articleCard-banner-top[data-id=' + event['htmlId'] + ']');
                article.addClass('loaded');
                article.find('.cover-card').addClass('loaded');
                removeExternalLinks('article-' + event['htmlId']);

                if(partner == 'opera'){
                    embedAdSlots.push({slot : 's4', slotPos : section});
                }else{
                    embedAdSlots.push({slot : 's3', slotPos : section});
                    
                    if(w['advtsConf'][advt_unit_name]['slots']['s10']){
                        embedAdSlots.push({slot : 's10', slotPos : article.find('.hidden-image-card-slug').eq(0)});
                    }

                    if(embedAdSlots.length > 0){
                        embedAds(event['htmlId'], embedAdSlots, (article.prevAll('[data-advt-permit=enable]').length > 0));
                    }
                }
                // fetchAmazonBids(['s2']);
            });
        }
        this.delayImageLoading();
        this.updateScrollPositionCoverCardsList();
        this.storeArticleCoordinates();

        eventsResp['events'].forEach((item, index) => {
            var info = {
                url: (typeof(partner) != 'undefined' && partner) ? item['url'] + this.urlQueryString : item['url'],
                tag: item['tag'],
                title: item['title'],
                creatorId: (typeof item['creatorSnapshot']['creators'][0] !== 'undefined' ? item['creatorSnapshot']['creators'][0]['id'] : null),
                eventsListWrapperId: 'wrapper-events-list-' + item['htmlId']
            }
            this.articlesLoadedList.push(info);
        });

        if (typeof(partner) != 'undefined' && partner) {
            this.nextArticleLoadUrl = eventsResp['load']['nextPartnerEventsApi'];
        }else{
            this.nextArticleLoadUrl = eventsResp['load']['nextEventsApi'];
        }
        this.setIdForLoadingNextArticles();
        this.loadingNextEvents = false;
    },

    changeCurrentArticleIndex: function () {
        var st = $(window).scrollTop();;
        var hook = 0.8 * $(window).height();

        if (st + hook < this.scrollPositionCoverCards[this.currentArticleIndex]) {
            this.currentArticleIndex -= 1;
            if (this.currentArticleIndex == -1) {
                this.currentArticleIndex = 0;
            }
            this.executeOperationsToChangeInCurrentArticleIndex();
        }
        else if (st + hook > this.scrollPositionCoverCards[this.currentArticleIndex + 1]) {
            this.currentArticleIndex += 1;
            this.executeOperationsToChangeInCurrentArticleIndex();
        }
    },

    updateScrollPositionCoverCardsList: function () {
        this.scrollPositionCoverCards = [];
        $('.cover-card').each((index, item) => {
            this.scrollPositionCoverCards.push($(item).offset().top);
        });
    },

    storeArticleCoordinates: function () {
        this.articlesCoordinates = [];
        $('article').each((index, item) => {
            var topCoord = $(item).offset().top;
            var bottomCoord = topCoord + $(item).height();
            var coords = [topCoord, bottomCoord];
            this.articlesCoordinates.push(coords);
        });
    },

    executeOperationsToChangeInCurrentArticleIndex: function () {
        history.replaceState(null, this.articlesLoadedList[this.currentArticleIndex]['title'], this.articlesLoadedList[this.currentArticleIndex]['url']);
        // validateGlobals('ga') ? ga('send', 'pageview', w.location.href) : null;
        track('send', 'pageview', w.location.href);
        if (typeof(partner) != 'undefined' && partner) {
            track('partner.send', 'pageview', w.location.href);
        }
        /* if (this.smartOctoArticlesList[this.currentArticleIndex] instanceof Object && _ain && _ain.track instanceof Function) {
            this.startNewSmartOctoAnalyticsTrack(this.smartOctoArticlesList[this.currentArticleIndex]);
        } */
        if (this.articlesLoadedList[this.currentArticleIndex]['creatorId'] != null) {
            // validateGlobals('ga') ? ga('send', 'event', 'editor', 'pageview', 'ed-' + this.articlesLoadedList[this.currentArticleIndex]['creatorId']) : null;
            track('send', 'event', 'editor', 'pageview', 'ed-' + this.articlesLoadedList[this.currentArticleIndex]['creatorId']);
        }
    },

    /* startNewSmartOctoAnalyticsTrack: function (config) {
        //set new metadata to _ain
        for (const property in config) {
            _ain[property] = config[property];
        }
        _ain.track();
    }, */

    onLikeButtonClickEventHandler: function (obj, event) {
        var query = obj['query'];
        var button = $(event.target).closest(query);
        button.toggleClass('liked');
        var item = {
            name: d.title,
            link: w.location.href,
            time: (new Date()).toUTCString()
        };
        var articleID = button.attr('id');
        if (button.hasClass('liked')) {
            w.localStorage.setItem(articleID, JSON.stringify(item));
            // validateGlobals('ga') ? ga('send', 'event', 'glance', 'liked', w.location.href) : null;
            track('nb.send', 'event', 'glance', 'liked', w.location.href);
            if (typeof(partner) != 'undefined' && partner) {
                track('partner.send', 'event', 'glance', 'liked', w.location.href);
            }
        } else {
            w.localStorage.removeItem(articleID);
            // validateGlobals('ga') ? ga('send', 'event', 'glance', 'unliked', w.location.href) : null;
            track('nb.send', 'event', 'glance', 'unliked', w.location.href);
            if (typeof(partner) != 'undefined' && partner) {
                track('partner.send', 'event', 'glance', 'unliked', w.location.href);
            }
        }
    },

    onArticleStatsClickEventHandler: function (event) {
        var target = $(event.target).closest('.article-stats');
        target.siblings('.hidden-slug').last().addClass('hidden');
        target.addClass('hidden');
        target.siblings('.dot-loader').removeClass('hidden');
        this.loadRemainingArticle(target);
    },

    loadRemainingArticle: function (target) {
        $.ajax({
            async: true,
            type: "GET",
            url: target.attr('data-load'),
            success: this.successCbForLoadingRemainingArticle.bind(this, target),
            error: this.errorCbForLoadingRemainingArticle.bind(this, target),
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", this.token);
            }
        });
    },

    successCbForLoadingRemainingArticle: function (target, result, status, xhr) {
        if (status === "success" && result['success']) {
            this.renderManagerForLoadingRemainingArticle(target, result['data']);
        } else {
            return;
        }
    },

    errorCbForLoadingRemainingArticle: function (target, xhr, status, error) {
        target.siblings('.hidden-slug').last().removeClass('hidden');
        target.removeClass('hidden');
        target.siblings('.dot-loader').addClass('hidden');
    },

    renderManagerForLoadingRemainingArticle: function (target, resp) {
        resp['events']['list']['data'] = resp['events']['list']['data'].filter(function (item, index) {
            return index !== 0;
        });
        if(typeof(partner) != 'undefined' && partner){
            resp['reactionShare'] = {like: 'true'};
            resp['events']['list']['data'] = resp['events']['list']['data'].map(function (item) {
                item['tagColor'] = 'brand';

                return item;
            });
        }
        var eventCardsListHtml;
        
        
        if(typeof(partner) == 'undefined'){
            if(typeof withoutSlug == 'undefined'){
                eventCardsListHtml = $.render.v2eventCardsList(resp);
            }else{
                eventCardsListHtml = $.render.v2eventCardsListNoSlug(resp);
            }

            var articleCardBelowItems = $.render.v2articleCardBelowItems(resp);
            var articleCardSideItems = $.render.v2articleCardSideItems(resp);
            var articleCardBreadCrumbs = $.render.v2articleCardBreadCrumbs(resp);
        }else{
            if(typeof withoutSlug == 'undefined'){
                eventCardsListHtml = $.render.v2eventCardsList(resp);
            }else{
                eventCardsListHtml = $.render.v2eventCardsListNoSlugPartner(resp);
            }
        }

        var clonedTarget = target.clone();
        var id = clonedTarget.attr('data-id');

        target.siblings('.hidden-slug').last().remove();
        target.siblings('.dot-loader').remove();
        target.remove();

        $(eventCardsListHtml).insertAfter($('.embed-event-cards[data-id=' + id + ']'));
        this.hookUpEvents();
        
        if(typeof(partner) == 'undefined'){
            $(articleCardBelowItems).insertAfter($('.embed-articleCard-below-items[data-id=' + id + ']'));
            $(articleCardSideItems).insertAfter($('.embed-articleCard-side-items[data-id=' + id + ']'));
            $(articleCardBreadCrumbs).insertAfter($('.embed-articleCard-breadcrumbs[data-id=' + id + ']'));
            this.highlightProgressBar();
        }

        this.addSlidingBehaviourToTabs('sliderNews-' + resp['sliderNews']['id']);
        this.delayImageLoading();
        this.updateScrollPositionCoverCardsList();
        this.storeArticleCoordinates();

        pollCardModule.assignCompletedStatus();
        pollCardModule.showCompletedPollsState();

        if ($('#wrapper-events-list-' + resp['htmlId']).find('.quiz-card-parent').length > 0) {
            var quizCardModuleInstance = Object.create(quizCardModule);
            quizCardModuleInstance.init($('#wrapper-events-list-' + resp['htmlId']).find('.quiz-card-parent').attr('data-quizid'));
        }

        this.onSliderTabsClickEventHandler('sliderNews-' + resp['sliderNews']['id']);

        $('.' + resp['htmlId']).each(function (i, item) {
            slideShowMaker(item);
        })
        if (typeof FB != 'undefined')
            FB.XFBML.parse();
        else {
            launchFb(function () { FB.XFBML.parse(); });
        }

        if (typeof(partner) != 'undefined' && partner) {
            if(w['advtsConf'][advt_unit_name]['slots']['s5'] && resp['events']['list']['data'].length >= 5){
                embedAds(resp['htmlId'], [{slot : 's2', slotPos : $('article#' + 'article-' + resp['htmlId']).find('.event-card').eq(0)}, {slot : 's4', slotPos : $('article#' + 'article-' + resp['htmlId']).find('.event-card').eq(2)}, {slot : 's5', slotPos : $('article#' + 'article-' + resp['htmlId']).find('.event-card').eq(4)}], ($('article#' + 'article-' + resp['htmlId']).prevAll('[data-advt-permit=enable]').length > 0));
            }else{
                embedAds(resp['htmlId'], [{slot : 's2', slotPos : $('article#' + 'article-' + resp['htmlId']).find('.event-card').eq(0)}, {slot : 's4', slotPos : $('article#' + 'article-' + resp['htmlId']).find('.event-card').eq(2)}], ($('article#' + 'article-' + resp['htmlId']).prevAll('[data-advt-permit=enable]').length > 0));
            }
            // fetchAmazonBids(['s3']);

            removeExternalLinks('article-' + resp['htmlId']);
        }

    },

    delayImageLoading: function () {
        var lazyLoader = Object.create(lazyLoadModule);
        lazyLoader.init('.lazy', { ios: { query: '.inset-img-hidden', device: isIos() } });
        lazyLoader.lazy();
    },

    highlightProgressBar: function () {
        var currentArticleIndex = this.currentArticleIndex;
        var prevArticleIndex = this.currentArticleIndex - 1;

        var articlesVtLines = [];
        var indexes = [prevArticleIndex, currentArticleIndex];

        for (let i = 0; i < indexes.length; i++) {
            var index = indexes[i];
            if (index < 0) {
                continue;
            }
            var eventsListWrapperId = this.articlesLoadedList[index]['eventsListWrapperId'];
            var id = eventsListWrapperId.split('-').pop();
            articlesVtLines.push($('#article-' + id).find('.vt-line'));
        }

        articlesVtLines.forEach(function (element, index) {
            var vtLine = element;
            var totalHeight = vtLine.closest('.events-list-holder').height();
            var st = $(window).scrollTop();

            var topElement = vtLine.closest('.events-list-holder').find('.hidden-slug').first();
            var bottomElement = vtLine.closest('.events-list-holder').find('.hidden-slug').last();

            if (topElement.length == 0 || bottomElement.length == 0) {
                return
            }

            var offsetFromTop = topElement.offset().top;
            var offsetFromBottom = bottomElement.offset().top;

            var progressBar = vtLine.closest('.events-list-holder').find('.vt-line-progress-bar');
            var lastCardHeight = offsetFromTop + totalHeight - offsetFromBottom;
            var lastCardHeightOffset = 5;

            var vtLineOffset = vtLine.offset();
            progressBar.css({
                left: vtLineOffset.left,
            });

            if (st < offsetFromTop) {
                progressBar.css({
                    height: 0
                }).removeClass('pos-fix');
            }
            if (st >= offsetFromTop && st <= (offsetFromTop + totalHeight)) {
                progressBar.addClass('pos-fix');

                var scroll = ((st - offsetFromTop) / (totalHeight));
                scroll = 100 * ((st - offsetFromTop + scroll * $(window).height() + scroll * lastCardHeight) / (totalHeight - lastCardHeight));
                var maxHeight = Math.min(100, (100 * (totalHeight + offsetFromTop - st - (lastCardHeight - lastCardHeightOffset)) / $(window).height()));
                progressBar.css({
                    height: (scroll * $(window).height() / 100) + 'px',
                    maxHeight: (maxHeight * $(window).height() / 100) + 'px',
                    top: 6,
                });
            } else {
                progressBar.removeClass('pos-fix');
            }

        });
    },

    makeTitlesSticky: function () {
        var coordinatesList = this.articlesCoordinates;
        var st = $(window).scrollTop();
        var clonedStickyTitle = $('.cloned-sticky-title');
        for (var index = 0; index < coordinatesList.length; index++) {
            var articleInfo = this.articlesLoadedList[index];
            var articleCoords = coordinatesList[index];
            var article = $('#' + articleInfo['eventsListWrapperId']).find('article');

            var title = article.find('.sticky-title');
            if (title.length == 0) {
                continue;
            }
            var offsets = [title.offset().top + title.height(), articleCoords[1] - title.height()];

            var articleCoordsWithOffsets = [];
            for (var i = 0; i < offsets.length; i++) {
                var item = offsets[i];
                if (i == (offsets.length - 1)) {
                    articleCoordsWithOffsets.push(Math.min(item, articleCoords[i]));
                } else {
                    articleCoordsWithOffsets.push(Math.max(item, articleCoords[i]));
                }
            }
            var defaultClonedStickyTitleConfig = {
                top: -(clonedStickyTitle.attr('data-offset')) + 'px'
            }
            if (st > this.lastScrollTop) {
                if (st > articleCoordsWithOffsets[0] && st < articleCoordsWithOffsets[1]) {
                    clonedStickyTitle.css({
                        top: 0,
                        left: article.offset().left,
                        width: article.width(),
                    });

                    if (typeof(partner) != 'undefined' && partner) {
                        let advtAlreadyRenderedOnce = typeof(clonedStickyTitle.attr('data-advt-sticky-title')) != 'undefined' && clonedStickyTitle.attr('data-advt-sticky-title');
                        if(w['advtsConf'][advt_unit_name]['slots']['s7'] && !advtAlreadyRenderedOnce){
                            embedAds(article.attr('id'), [{slot : 's7', slotPos : clonedStickyTitle.find('.hidden-sticky-title-slug').eq(0)}], (clonedStickyTitle.prevAll('[data-advt-permit=enable]').length > 0));
    
                            clonedStickyTitle.attr('data-advt-sticky-title', true);
                        }
                    }

                    if(title.html() != this.clonedStickyTitle){
                        this.clonedStickyTitle = this.articlesLoadedList[index]['title'];
                        $('.cloned-sticky-title').find('.cloned-title').replaceWith('<div class="cloned-title">' + this.articlesLoadedList[index]['title'] + '</div>');
                    }

                    break;
                } else {
                    clonedStickyTitle.css(defaultClonedStickyTitleConfig)
                }
            } else if (st <= this.lastScrollTop) {
                clonedStickyTitle.css(defaultClonedStickyTitleConfig)
            }
        }
        this.lastScrollTop = st;
    },

    hookUpEvents: function () {
        $('.events-list-holder').each(function (i, listHolder) {
            $(listHolder).find('.cover-card').each(function (j, item) {
                var id = $(item).find('.evt-connector').attr('id');
                if(typeof id === 'undefined') id = $(item).data('id');

                var firstConnector = $(item).closest('.events-list-holder').find('.hidden-slug').first().offset();
                var lastConnector = $('.hidden-slug[data-hooked=' + id + ']').last().offset();

                if ((typeof firstConnector !== 'undefined') && (typeof lastConnector !== 'undefined')) {
                    var posFirst = firstConnector.top;
                    var posLast = lastConnector.top;
                    $(item).closest('.events-list-holder').find('.vt-line').css({ height: posLast - posFirst + 'px' });
                }
            });
        });
    },

    listenToHeightChange: function () {
        var lastHeight = d.body.clientHeight, newHeight;
        checkHeight = () => {
            newHeight = d.body.clientHeight;
            if (lastHeight != newHeight) {
                this.hookUpEvents();
                this.updateScrollPositionCoverCardsList();
                this.storeArticleCoordinates();
            }
            lastHeight = newHeight;
            if (d.body.onElementHeightChangeTimer) {
                clearTimeout(d.body.onElementHeightChangeTimer);
            }
            d.body.onElementHeightChangeTimer = setTimeout(checkHeight, 225);
        }
        checkHeight();
    },

    notifPopupManager: function () {
        if (!this.notificationSubsChecked) {
            this.notificationSubsChecked = true;
            validateGlobals('OneSignalConfig') ? (Object.create(oneSignalManager)).notify() : null;
        }
    },

    onSliderTabsClickEventHandler: function (event) {
        var target = $(event.target);
        var id = target.attr('data-id');
        target.siblings('.tab-item').removeClass('active');
        target.addClass('active');
        target.closest('.tabs').find('.tab-content').addClass('hidden');
        target.closest('.tabs').find('.tab-content[data-id=' + id + ']').removeClass('hidden');
        this.delayImageLoading();
    },

    delayImageLoading: function () {
        var lazyLoader = Object.create(lazyLoadModule);
        lazyLoader.init('.lazy', { ios: { query: '.inset-img-hidden', device: isIos() } });
        lazyLoader.lazy();
    },

    addSlidingBehaviourToTabs: function (id) {
        var sliderTabsConf = {
            infinite: false,
            slidesToShow: 1,
            variableWidth: true,
            nextArrow: '<div class="bg-primary br-rd-pr-50 bt-half cs-ptr cs-ptr pos-abs slick-arrow tp-half z-index-2" style="transform: translateY(-50%);box-shadow: 0 2px 22px 0 rgba(0, 0, 0, 0.06); right:10px; width:32px; height:32px;"><div class="dp-fx fx-al-ct fx-js-ct ht-full wd-full"><span class="cl-link fz-22 nb-icon-angle-down mg-l-8" style="transform:rotate(-90deg);"></span></div></div>',
            prevArrow: '<div class="bg-primary br-rd-pr-50 bt-half cs-ptr cs-ptr pos-abs slick-arrow tp-half z-index-2" style="transform: translateY(-50%);box-shadow: 0 2px 22px 0 rgba(0, 0, 0, 0.06); width:32px; height:32px; left:10px;"><div class="dp-fx fx-al-ct fx-js-ct ht-full wd-full"><span class="cl-link fz-22 nb-icon-angle-down mg-r-8" style="transform:rotate(90deg);"></span></div></div>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: "unslick"
                }
            ]
        }
        $('#' + id).find('.slider-tabs').slick(sliderTabsConf);
        this.delayImageLoading();
    },

    updateArticleNavigation: function () {
        let apiUrl = $('.events-list-holder').attr('data-nav-events');
        if(typeof apiUrl == 'undefined'){
            return;
        }

        let sessionNavigationInfo = sessionStorage?.getItem('sessionNavigationInfo');
        
        if(sessionNavigationInfo){
            sessionNavigationInfo = JSON.parse(sessionNavigationInfo);
        }
        let category = $('.events-list-holder').attr('data-tag').toLowerCase();
        if(!sessionNavigationInfo || Date.now() > sessionNavigationInfo.sessionExpTimestamp || sessionNavigationInfo.latestCategoryArticleChecked.indexOf(category) == -1){
            let isSessionExpired = !(sessionNavigationInfo && sessionNavigationInfo.sessionExpTimestamp > Date.now());
            sessionNavigationInfo = {
                latestCategoryArticleChecked: sessionNavigationInfo?.latestCategoryArticleChecked && !isSessionExpired ? sessionNavigationInfo.latestCategoryArticleChecked : [],
                visitedArticleList: sessionNavigationInfo?.visitedArticleList && !isSessionExpired ? sessionNavigationInfo.visitedArticleList: [],
                sessionExpTimestamp: !isSessionExpired ? sessionNavigationInfo.sessionExpTimestamp : Date.now() + 15 * 60 * 1000
            }
            sessionStorage.setItem('sessionNavigationInfo', JSON.stringify(sessionNavigationInfo));

            $.ajax({
                async: true,
                type: "GET",
                url: apiUrl,
                success: this.successCbForUpdatingArticleNavigation.bind(this),
                error: this.errorCbForUpdatingArticleNavigation.bind(this)
            });
        }
    },

    successCbForUpdatingArticleNavigation: function (result, status, xhr) {
        if (status === "success" && result['success']) {
            if($('.navigation-events').attr('nav-type') == 'partner'){
                result['data']['next'] = result['data']['nextEventPartner'] || {};
                result['data']['previous'] = result['data']['previousEventPartner'] || {};
            }

            this.renderNavigation(false, result);
        } else {
            return;
        }
    },

    errorCbForUpdatingArticleNavigation: function (xhr, status, error) {
        console.log("Failed to get article Naviagtion");
    },

    renderNavigation: function (isUpdate, result) {
        let prev = {}, next = {};
        if(! isUpdate){
            if(result?.data?.previous?.url){
                prev = result['data']['previous'];
            }

            if(result?.data?.next?.url){
                next = result['data']['next'];
            }
        }else{
            next.url = $('.next-article-button').attr('href');
            next.title = $('.next-article-button').attr('title');
            prev.url = $('.prev-article-button').attr('href');
            prev.title = $('.prev-article-button').attr('title');
        }

        let sessionNavigationInfo = sessionStorage?.getItem('sessionNavigationInfo');
        if(sessionNavigationInfo){
            sessionNavigationInfo = JSON.parse(sessionNavigationInfo);
        }
        
        if($('.next-article-button').length > 0 && next?.url && next.url != w.location.origin + w.location.pathname && !(sessionNavigationInfo?.visitedArticleList.indexOf(next.url) != -1)){
            let filteredQueryParams = getFilteredQueryParams($('.next-article-button').attr('href'));
            $('.next-article-button').attr('href', next.url + filteredQueryParams);
            $('.next-article-button').attr('title', next.title);
        }

        if($('.prev-article-button').length > 0 && prev?.url && prev.url != w.location.origin + w.location.pathname && !(sessionNavigationInfo?.visitedArticleList.indexOf(prev.url) != -1)){
            let filteredQueryParams = getFilteredQueryParams($('.prev-article-button').attr('href'));
            $('.prev-article-button').attr('href', prev.url + filteredQueryParams);
            $('.prev-article-button').attr('title', prev.title);
        }

        let prevUrl = $('.prev-article-button')?.attr('href');
        let nextUrl = $('.next-article-button')?.attr('href');

        if(! isUpdate && typeof prevUrl != 'undefined' && prevUrl != ""){
            $('.next-article-button').removeClass('hidden');
        }

        if(! isUpdate && typeof nextUrl != 'undefined' && nextUrl != ""){
            $('.prev-article-button').removeClass('hidden');
        }
    },

    // handleArticleNavigationClick: function (e, isNext) {
    //     e.preventDefault();
    //     e.stopPropagation();
    //     let url = href = $(e.currentTarget).attr('href');
    //     let title = title = $(e.currentTarget).attr('title');
    //     let tag = tag = $(e.currentTarget).attr('tag');
        
    //     if (url.indexOf('http') !== 0) {
    //         url = w.location.protocol+'//'+w.location.host+url;
    //     }
    //     if (typeof url !== 'undefined') {
    //         let sessionNavigationInfo = sessionStorage?.getItem('sessionNavigationInfo');
    //         if(sessionNavigationInfo){
    //             sessionNavigationInfo = JSON.parse(sessionNavigationInfo);
    //             if(sessionNavigationInfo.latestCategoryArticleChecked.indexOf(tag.toLowerCase()) == -1){
    //                 sessionNavigationInfo.latestCategoryArticleChecked.push(tag.toLowerCase());
    //             }
    //             sessionStorage.setItem('sessionNavigationInfo', JSON.stringify(sessionNavigationInfo));
    //         }

    //         track({
    //             'event': isNext ? 'article_nav_click_next' : 'article_nav_click_previous',
    //             'article_name':title,
    //             'article_category':tag,
    //             'author_name':undefined,
    //             'article_id': url,
    //             'User_ID':undefined
    //         });
    //         w.setTimeout(function() {
    //             w.location.href = url;
    //         }, 200);
    //     }
    // },

    hideArticleNaviagtion: function () {
        $('.navigation-events').animate({opacity: 0.5}, 'slow');
    },

    updateArticleCardStats: function () {
        $('.card-stats').each(function (i, item) {
            let title = $(item).attr('data-title');

            if($(item).attr('data-percentage')){
                title = title.replace(":read_pr:", "<span class='cl-venetian-red ter-bd-1'>" + $(item).attr('data-percentage') + "%</span>");
            }

            if(i%2 == 1 || !($(item).attr('data-percentage'))){
                $(item).html(title);
                $(item).addClass('pd-l-10 pd-r-10');
            }
        })
    },

}

var sliderModule = {

    init: function (id) {
        if(!id.includes('multiple-tabs'))        
        this.addSlidingBehaviourToTabs(id);
        
        this.onSliderTabsClickEventHandler = this.onSliderTabsClickEventHandler.bind(this);
        
        $(d).on('click', '.slider-tabs .tab-item', this.onSliderTabsClickEventHandler);
    },
    
    addSlidingBehaviourToTabs: function (id) {
        var sliderTabsConf = {
            infinite: false,
            slidesToShow: 1,
            variableWidth: true,
            nextArrow: '<div class="bg-primary br-rd-pr-50 bt-half cs-ptr cs-ptr pos-abs slick-arrow tp-half z-index-2" style="transform: translateY(-50%);box-shadow: 0 2px 22px 0 rgba(0, 0, 0, 0.06); right:10px; width:32px; height:32px;"><div class="dp-fx fx-al-ct fx-js-ct ht-full wd-full"><span class="cl-link fz-22 nb-icon-angle-down mg-l-8" style="transform:rotate(-90deg);"></span></div></div>',
            prevArrow: '<div class="bg-primary br-rd-pr-50 bt-half cs-ptr cs-ptr pos-abs slick-arrow tp-half z-index-2" style="transform: translateY(-50%);box-shadow: 0 2px 22px 0 rgba(0, 0, 0, 0.06); width:32px; height:32px; left:10px;"><div class="dp-fx fx-al-ct fx-js-ct ht-full wd-full"><span class="cl-link fz-22 nb-icon-angle-down mg-r-8" style="transform:rotate(90deg);"></span></div></div>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: "unslick"
                }
            ]
        }
        $('#' + id).find('.slider-tabs').slick(sliderTabsConf);
        this.delayImageLoading();
    },

    onSliderTabsClickEventHandler: function (event) {
        var target = $(event.target);
        var id = target.attr('data-id');
        target.siblings('.tab-item').removeClass('active');
        target.addClass('active');
        target.closest('.tabs').find('.tab-content').addClass('hidden');
        target.closest('.tabs').find('.tab-content[data-id=' + id + ']').removeClass('hidden');
        this.delayImageLoading();
    },

    delayImageLoading: function () {
        var lazyLoader = Object.create(lazyLoadModule);
        lazyLoader.init('.lazy', { ios: { query: '.inset-img-hidden', device: isIos() } });
        lazyLoader.lazy();
    },
}

var paginationModule = {

    init: function (item) {
        this.parent = item
        this.paginator = this.parent.find('.paginator');
        this.paginationBox = this.parent.find('.pagination-present');
        this.total = this.paginationBox.attr('data-pg-total');
        this.perView = this.paginationBox.attr('data-pg-view');
        this.countPerView = ((this.total - (this.total % this.perView)) / this.perView);
        this.registerEvents();
        this.renderTemplate();
    },

    registerEvents: function () {
        $(d).on('click', '.paginator .template .item', this.handleItemClick.bind(this));
    },

    renderTemplate: function () {
        var template = this.paginator.find('.template').clone();
        for (var index = 0; index < this.countPerView; index++) {
            var tItem = template.find('.item').clone();
            tItem.text(index + 1);
            this.paginator.find('.template').append(tItem);
        }
        this.paginator.find('.template').find('.item').first().remove();
        this.paginator.find('.template').find('.item').last().addClass('last');
        this.showPaginatorItems(0);
        this.showPaginator();
    },

    handleItemClick: function (event) {
        var target = $(event.target);
        var n = target.closest('.template').find('.item').index(target);
        this.showPaginatorItems(n);
        var paginationList = this.paginationBox.find('.pg-row').addClass('row-hidden');
        var start = n * this.countPerView;
        var end;
        if (target.hasClass('last'))
            end = paginationList.length;
        else
            end = start + this.countPerView;
        $(paginationList).slice(start, end).removeClass('row-hidden');
    },

    showPaginatorItems: function (index) {
        var window = 4;
        var list = this.paginator.find('.template').find('.item').addClass('hidden');
        $(list).removeClass('active cl-ink-dk');
        $(list[index]).addClass('cl-ink-dk');
        for (var i = index; i < ((index + window) - 1); i++) {
            $(list[i]).removeClass('hidden').addClass('active');
        }
        if (index == 0)
            $(list[index + window - 1]).removeClass('hidden').addClass('active');
        var firstActive = this.paginator.find('.template').find('.item.active').first();
        var firstActiveIndex = this.paginator.find('.template').find('.item').index(firstActive);
        $(list[firstActiveIndex - 1]).removeClass('hidden').addClass('active');
        if ((index + window) > list.length) {
            $(list).slice((index - ((index + window) - (list.length))), list.length).removeClass('hidden').addClass('active');
        }
    },

    showPaginator: function () {
        this.paginator.removeClass('hidden');
    }
}

var videoModule = {
    init: function () {
        this.registerEvents();
    },

    registerEvents: function () {
        $(d).on('click', '.play-video', (event) => {
            var videoSrc = $(event.target).closest('.play-video').attr('data-video');
            this.target = $(event.target).closest('.play-video');
            this.embedVideo(videoSrc);

            this.player = new YT.Player(this.current, {});
            this.onScrollEventHandler = this.onScrollEventHandler.bind(this);
            $(window).scroll(this.onScrollEventHandler);
        });

        $(d).on('click', '.close-video-player', (event) => {
            this.removePopupPlayer();
        });

        $(d).on('keydown', '.video-player', (event) => {
            if (event.keyCode == 27 && this.isPopupPlayerActive()) {
                this.removePopupPlayer();
            }
        });
    },

    onScrollEventHandler: function () {
        if(! (this.player.getPlayerState() == '2' || $('#' + this.current).isInViewport())){
            this.player.pauseVideo();
        }
    },

    embedVideo: function (src) {
        var html = this.getIframe(src);
        this.appendIframe(html);
    },

    appendIframe: function (html) {
        switch (this.getEmbedType()) {
            case 'in-place':
                $(this.target).replaceWith(html);
                break;
            default:
                $('.video-player').find('#frame-container').append(html);
                this.initPopupPlayer();
                break;
        }
    },

    getEmbedType: function () {
        if ($(this.target).hasClass('embed-in-place'))
            return 'in-place';
        return 'full-screen';
    },

    getIframe: function (src) {
        var iframe;
        var id = '_' + Math.random().toString(36).substr(2, 9);
        this.setCurrent(id);
        switch (this.getEmbedType()) {
            case 'in-place':
                iframe = $.render.youtubeFrameInPlace();
                break;

            default:
                iframe = $.render.youtubeFrameFullScreen();
                break;
        }
        var html = $(iframe).attr('src', this.getCompleteSrc(src)).attr('id', id);
        return html[0];
    },

    setCurrent: function (id) {
        this.current = id;
    },

    togglePopupPlayer: function (action) {
        switch (action) {
            case 'show':
                $('.video-player').addClass('active');
                $('html').addClass('ovr-hidden');
                break;
            default:
                $('.video-player').removeClass('active');
                $('html').removeClass('ovr-hidden');
                break;
        }
    },

    initPopupPlayer: function name() {
        this.popupPlayer = new YT.Player(this.current);
        this.togglePopupPlayer('show');
    },

    isPopupPlayerActive: function () {
        return $('.video-player').hasClass('active');
    },

    removePopupPlayer: function () {
        this.togglePopupPlayer('hide');
        this.popupPlayer.destroy();
    },

    getCompleteSrc: function (src) {
        if (this.checkQueryParamsExists(src)) {
            return src.split('?')[0] + this.setAutoPlay(this.getQueryParams(src));
        }
        return src.split('?')[0] + this.setAutoPlay({});
    },

    checkQueryParamsExists: function (src) {
        return src.indexOf('?') > 0;
    },

    getQueryParams: function (src) {
        var queryStr = src.substr(src.indexOf('?') + 1, src.length - 1);
        var rParams = queryStr.split('&');
        var params = {}
        for (i in rParams) {
            var param = rParams[i].split('=');
            params[param[0]] = param[1];
        }
        return params;
    },

    setAutoPlay: function (qParams) {
        qParams['autoplay'] = '1';
        return '?' + 'enablejsapi=1&' + $.param(qParams);
    }
}

/*
* Caution : Do not delete / remove
* This remains as backup when there is need to switch to the
* biDirectionalPaginatedResponsesComponent with the panel.
*/
var biDirectionalPaginatedResponsesComponent = {

    defaultComponentQuery: '.paginated-responses-container',
    defaultItemQuery: '.paginated-response-item',
    defaultItemTemplate: 'paginatedResponseListDescriptiveLgRecImg',
    seizeScrollEffectForLoad: false,

    init: function (componentQuery, itemQuery, templateItem) {
        this.componentQuery = componentQuery || this.defaultComponentQuery;
        this.itemQuery = itemQuery || this.defaultItemQuery;
        this.template = templateItem || this.defaultItemTemplate;
        this.currentArea = $(this.componentQuery).find(this.itemQuery);
        this.panel = $(this.componentQuery).find('.panel');
    },

    handleScrollEvent: function () {
        var lastScrollTop = 0;
        var st = 0;
        var _this = this;
        var delta = 500;
        $(window).scroll(function (event) {
            st = $(this).scrollTop();
            _this.updatePanel();
            if (st > lastScrollTop) {
                //For loading bottom set:
                var lastSet = $(_this.componentQuery).find(_this.itemQuery).last();
                if (st >= ((lastSet.offset().top + lastSet.outerHeight()) - w.innerHeight)) {
                    if (!_this.seizeScrollEffectForLoad) {
                        _this.seizeScrollEffectForLoad = true;
                        _this.loadNew('down');
                    }

                }
            } else {
                //For loading Upper set:
                var firstSet = $(_this.componentQuery).find(_this.itemQuery).first();
                if (st < (firstSet.offset().top)) {
                    if (!_this.seizeScrollEffectForLoad) {
                        _this.seizeScrollEffectForLoad = true;
                        _this.loadNew('up');
                    }
                }
            }

            lastScrollTop = st;
        })
    },

    handlePanelEvents: function () {
        var delta = 200;
        $(d).on('click', '.panel .page-scroll', (event) => {
            var areas = [];
            $(this.componentQuery).find(this.itemQuery).each(function (i, item) {
                areas.push({ id: $(item).attr('id'), status: $(item).isInViewport() });
            });
            var scrollToNextId;
            var scrollToPrevId;
            var offset = 5;
            var duration = 1500;
            var options = { duration: duration, easing: 'linear' };
            for (var index = areas.length; index > 0; index--) {
                if (areas[index - 1]['status']) {
                    scrollToNextId = areas[index - 1]['id'];
                    break;
                }
            }
            for (var index = 0; index < areas.length; index++) {
                if (areas[index]['status']) {
                    scrollToPrevId = areas[index]['id'];
                    break;
                }
            }
            if ($(event.target).hasClass('prev')) {
                if ($('#' + scrollToPrevId).prev('hr').length == 0) {
                    $(window).scrollTo(0, duration, options);
                    $(window).scrollTo(offset, 350, { duration: 350, easing: 'linear' });
                } else {
                    $(window).scrollTo($('#' + scrollToPrevId).prev('hr').prev(this.itemQuery).find('a').last(), duration, options);
                }
            } else {
                $(window).scrollTo($('hr[data-id=' + scrollToNextId + ']'), duration, options);
                // When nothing is visible on the viewport: that is on the top of the page
                if (typeof scrollToNextId === 'undefined') {
                    $(window).scrollTo($(this.componentQuery).find(this.itemQuery).first().next('hr'), duration, options);
                }
            }

        });
    },

    loadNew: function (dir) {
        var _this = this;
        var elem;
        var api;
        if (dir == 'up') {
            elem = $(_this.componentQuery).find(_this.itemQuery).first();
            api = elem.attr('data-prev-api');
        } else {
            elem = $(_this.componentQuery).find(_this.itemQuery).last();
            api = elem.attr('data-next-api');
        }
        if (dir == 'up' && ($(_this.componentQuery).hasClass('block-upward-load'))) {
            _this.resetDefaults();
            return;
        }
        if (typeof api !== 'undefined') {
            $.ajax({
                async: true,
                type: "GET",
                url: api,
                success: function (result, status, xhr) {
                    if (status === "success" && result['success']) {
                        var html = $.render[_this.template](result['data']);
                        _this.appendHtml(html, dir);
                        _this.delayImageLoading();
                        _this.removeStaticPanel();
                        _this.resetDefaults();
                    } else {
                        _this.resetDefaults();
                    }
                },
                error: function (xhr, status, error) {
                    _this.resetDefaults();
                }
            });
        } else {
            _this.resetDefaults();
        }
    },

    appendHtml: function (html, dir) {
        var _this = this;
        if (dir == 'up') {
            $(html).insertBefore($(_this.componentQuery).find(_this.itemQuery).first());
        } else {
            $(html).insertAfter($(_this.componentQuery).find(_this.itemQuery).last().next('hr'));
        }
    },

    updatePanel: function () {
        var _this = this;
        $(_this.componentQuery).find(_this.itemQuery).each(function (i, item) {
            if ($(this).isInViewport()) {
                var text = $(item).attr('data-text');
                var prevUrl = $(item).attr('data-prev-url');
                var nextUrl = $(item).attr('data-next-url');
                _this.panel.find('.prev a').attr('href', prevUrl);
                _this.panel.find('.next a').attr('href', nextUrl);
            }
        });
    },

    removeStaticPanel: function () {
        var _this = this;
        if (_this.panel.filter('.static').length > 0) {
            _this.panel.filter('.static').remove();
        }
        _this.panel.removeClass('hidden');
    },

    resetDefaults: function () {
        var _this = this;
        _this.seizeScrollEffectForLoad = false;
    },

    delayImageLoading: function () {
        var lazyLoader = Object.create(lazyLoadModule);
        lazyLoader.init('.lazy', { ios: { query: '.inset-img-hidden', device: isIos() } });
        lazyLoader.lazy();
    }

}
/* @end - biDirectionalPaginatedResponsesComponent*/

var paginatedResponsesComponent = {

    defaultComponentQuery: '.paginated-responses-container',
    defaultItemQuery: '.paginated-response-item',
    defaultItemTemplate: 'paginatedResponseListDescriptiveLgRecImg',

    init: function (componentQuery, itemQuery, templateItem) {
        this.componentQuery = componentQuery || this.defaultComponentQuery;
        this.itemQuery = itemQuery || this.defaultItemQuery;
        this.template = templateItem || this.defaultItemTemplate;
        this.currentArea = $(this.componentQuery).find(this.itemQuery);
        this.panel = $(this.componentQuery).find('.panel');
        this.taget = null;
    },

    handlePanelEvents: function () {
        $(d).on('click', '.panel .next', (event) => {
            event.preventDefault();
            this.target = $(event.target);
            this.showLoadingState();
            this.loadNew();
        });
    },

    loadNew: function () {
        var api = $(this.componentQuery).find(this.itemQuery).last().attr('data-next-api');
        if (typeof api !== 'undefined') {
            $.ajax({
                async: true,
                type: "GET",
                url: api,
                success: callBackBinder(this.successCb, this),
                error: callBackBinder(this.errorCb, this),
            });
        } else {
            this.resetDefaults();
        }
    },

    successCb: function (result, status, xhr) {
        if (status === "success" && result['success']) {
            if (result['data']['list']['data'].length > 0) {
                var html = $.render[this.template](result['data']);
                this.appendHtml(html);
                this.resetDefaults();
                this.delayImageLoading();
                this.updatePanel(result['data']);
            } else {
                this.hideLoadMoreFunctionality();
            }
        } else {
            this.resetDefaults();
        }
    },

    errorCb: function (xhr, status, error) {
        this.errorReporter();
    },

    appendHtml: function (html) {
        $(html).insertAfter($(this.componentQuery).find(this.itemQuery).last().next('hr'));
    },

    updatePanel: function (data) {
        if (typeof data['list']['panel']['next'] !== 'undefined') {
            this.panel.find('.next').attr('href', data['list']['panel']['next']['url']);
        } else {
            this.hideLoadMoreFunctionality();
        }
    },

    errorReporter: function () {
        this.target.addClass('hidden');
        this.target.closest('.panel').find('.dot-loader').removeClass('active');
        this.target.closest('.panel').find('.flash-error').addClass('active');
        setTimeout(callBackBinder(this.resetDefaults, this), 1000);
    },

    resetDefaults: function () {
        this.target.removeClass('hidden');
        this.target.closest('.panel').find('.dot-loader').removeClass('active');
        this.target.closest('.panel').find('.flash-error').removeClass('active');
    },

    showLoadingState: function (target) {
        this.target.addClass('hidden');
        this.target.closest('.panel').find('.dot-loader').addClass('active');
    },

    hideLoadMoreFunctionality: function () {
        this.target.closest('.panel').remove();
    },

    delayImageLoading: function () {
        var lazyLoader = Object.create(lazyLoadModule);
        lazyLoader.init('.lazy', { ios: { query: '.inset-img-hidden', device: isIos() } });
        lazyLoader.lazy();
    }

}

/**
 *  Live widget module - start
 */

var ajaxMaker = {
    request: function (type, url, qParams, cbs) {
        switch (type) {
            case 'get':
                $.ajax({
                    async: true,
                    type: "GET",
                    url: url + this.getqParams(qParams),
                    success: function (response, status, xhr) {
                        cbs['success']['cb'].apply(cbs['success']['context'], arguments);
                    },
                    error: function (xhr, status, error) {
                        cbs['error']['cb'].apply(cbs['error']['context'], arguments);
                    }
                });
                break;

            case 'post':
                break;
        }
    },

    getqParams: function (qParams) {
        if (typeof qParams !== 'undefined')
            return '?' + $.param(qParams);
    }

};
var liveScoreWidget = {
    init: function () {
        this.tInterval = null;
        this.ajaxMaker = Object.create(ajaxMaker);
        this.controller();
        this.sports = { tennis: { renderedOnce: false }, olympicsTally: { renderedOnce: false }, liveBlog: { renderedOnce: false }, cricket: { renderedOnce: false }, active: false };
    },

    successCb: function (response, status, xhr) {
        if (status != 'success') return;
        if (typeof response == 'string') {
            response = JSON.parse(response);
        }
        if (!response.data.active) {
            clearInterval(this.tInterval);
            return;
        }
        if (!this.sports.active) {
            switch (response.data.sport) {
                case 'tennis':
                    this.registerTennisTemplateEvents();
                    break;
                case 'olympics-tally':
                    this.registerOlympicsTallyTemplateEvents();
                    break;
                case 'live-blog':
                    this.registerLiveBlogTemplateEvents();
                    break;
                case 'cricket-test':
                    this.registerCricketTestTemplateEvents();
                    break;
                default:
                    break;
            }
            this.sports.active = true;
        }
        this.renderManager(response.data);
    },

    errorCb: function (xhr, status, error) {
        console.log('Oops some error happened', status, error);
    },

    controller: function () {
        var factor = 1000 * 60 * 0.5;
        this.ajaxMaker.request('get', widgetsConf['url'], {
            t: (Math.round(new Date().getTime() / factor) * factor)
        }, {
            success: { cb: this.successCb, context: this }, error: { cb: this.errorCb, context: this }
        })
    },

    regulate: function () {
        this.tInterval = setInterval(() => this.controller(), 60 * 1000);
    },

    renderManager: function (response) {
        switch (response.sport) {
            case 'tennis':
                this.renderTennisTemplate(response);
                break;
            case 'olympics-tally':
                this.renderOlympicsTallyTemplate(response);
                break;
            case 'live-blog':
                this.renderLiveBlogTemplate(response);
                break;
            case 'cricket-test':
                this.renderCricketTestTemplate(response);
                break;
            default:
                break;
        }
    },

    renderTennisTemplate: function (response) {
        if (response.tabs.list.data.length == 0) {
            return;
        }
        if (!this.sports.tennis.renderedOnce) {
            var html = $.render.liveScoreTennis(response);
            $('.live-widget').append(html);
            $('.live-widget').find('.carousel').owlCarousel({
                items: 1,
                loop: true,
                autoplay: true,
                autoplayTimeout: 5 * 1000,
                autoplayHoverPause: true
            });
            this.sports.tennis.renderedOnce = true;
        } else {
            var widgets = $('.live-widget').toArray();
            widgets.forEach(function (widget, i) {
                var cards = $(widget).find('.card');
                var tabContentIds = [];
                cards.each(function (j, card) {
                    tabContentIds.push($(card).closest('.tab-content').attr('data-mapped'));
                });
                tabContentIds = tabContentIds.filter(function (v, i, a) {
                    return a.indexOf(v) === i;
                });
                if (response.tabs.list.data.length > 0) {
                    response.tabs.list.data.forEach(function (tab, k) {
                        if (tab.list.data.length > 0) {
                            var innerTabs = tab.list.data;
                            innerTabs.forEach(function (innerTab, l) {
                                var id = innerTab.id;
                                var cardsMappedToId = $(widget).find('.tab-content[data-mapped=' + id + ']').find('.card');
                                if (cardsMappedToId.length > 0) {
                                    cardsMappedToId.toArray().forEach(function (card, m) {
                                        if ((innerTab.list.data.length > 0) && (typeof innerTab['list']['data'][m] !== 'undefined')) {
                                            var data = innerTab['list']['data'][m];
                                            data.items.forEach(function (item, n) {
                                                var paramRow = $(card).find('.param-row:eq(' + n + ')');
                                                var paramItem = paramRow.find('.param-item').first().clone();
                                                paramRow.empty();
                                                item.params.forEach(function (param, o) {
                                                    var appendItem = paramItem.clone().text(param['title']);
                                                    paramRow.append(appendItem.get(0));
                                                });
                                            });
                                            data.list.forEach(function (lItem, n) {
                                                if (n == 0) {
                                                    return;
                                                }
                                                var lRow = $(card).find('.score-row:eq(' + n + ')');
                                                lRow.find('.score-title').text(lItem['title']);
                                                lItem.data.forEach(function (item, o) {
                                                    lRow.find('.score-' + o).text(item['title']);
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    },

    renderCricketTestTemplate: function (response) {
        if (!this.sports.cricket.renderedOnce) {
            var html = $.render.liveScoreCricketTest(response);
            $('.live-widget').append(html);
            if((typeof widgetsConf['open'] !== 'undefined') && widgetsConf['open'])
                $(".live-widget").find(".tab-content").eq(0).children().filter('.tabular-item').removeClass('hidden');
            this.sports.cricket.renderedOnce = true;
        } else {
            var widgets = $('.live-widget').toArray();
            widgets.forEach(function (widget, i) {
                var html = $.render.liveScoreCricketTest(response);
                var jQHtml = $(html);
                
                var tabsSelected = $(widget).find('.nested-tabs').filter(function (i, item) {
                    return $(item).hasClass('selected');
                });
                var tabsContentNotHidden = $(widget).find('.tab-content').filter(function (i, item) {
                    return !$(item).hasClass('hidden');
                });
                jQHtml.find('.nested-tabs').removeClass('selected');
                jQHtml.find('.tab-content').addClass('hidden');
                $(tabsSelected).each(function (i, item) {
                    jQHtml.find('.nested-tabs[data-id=' + $(item).attr('data-id') + ']').addClass('selected');
                });
                $(tabsContentNotHidden).each(function (i, item) {
                    jQHtml.find('.tab-content[data-mapped=' + $(item).attr('data-mapped') + ']').removeClass('hidden');
                    if (!$(widget).find('.tab-content[data-mapped=' + $(item).attr('data-mapped') + ']').find('.tabular-item').hasClass('hidden')) {
                        jQHtml.find('.tab-content[data-mapped=' + $(item).attr('data-mapped') + ']').find('.tabular-item').removeClass('hidden');
                    }
                });
                $(widget).find('*').remove();
                $(widget).append(jQHtml.get(0));
            });
        }
    },

    registerTennisTemplateEvents: function () {
        this.nestedTabClickEvent();
    },

    registerCricketTestTemplateEvents: function () {
        this.nestedTabClickEvent();
        $(d).on('click', '.live-widget .live-cricket-test .comparison-item', function (e) {
            var section = $(this).siblings().filter('.tabular-item');
            section.toggleClass('hidden');
        });
    },

    renderOlympicsTallyTemplate: function (response) {
        if (response.tabs.list.data.length == 0) {
            return;
        }
        if (!this.sports.olympicsTally.renderedOnce) {
            var html = $.render.liveOlympicsTally(response);
            $('.live-widget').append(html);
            this.sports.olympicsTally.renderedOnce = true;
        } else {
            var widgets = $('.live-widget').toArray();
            widgets.forEach(function (widget, i) {
                var cards = $(widget).find('.card');
                var tabContentIds = [];
                cards.each(function (j, card) {
                    tabContentIds.push($(card).closest('.tab-content').attr('data-mapped'));
                });
                tabContentIds = tabContentIds.filter(function (v, i, a) {
                    return a.indexOf(v) === i;
                });
                if (response.tabs.list.data.length > 0) {
                    response.tabs.list.data.forEach(function (tab, k) {
                        if (tab.list.data.length > 0) {
                            var cardMappedToId = $(widget).find('.tab-content[data-mapped=' + tab.id + ']').find('.card');
                            tab.list.data.forEach(function (item, l) {
                                var row = $(cardMappedToId).find('.item-row:eq(' + l + ')');
                                if (row.length > 0) {
                                    row.find('.serial').text(item['serial']);
                                    row.find('.title').text(item['title']);
                                    row.find('.img').attr('src', item['img']).attr('alt', item['title']);
                                    item['params'].forEach(function (param, m) {
                                        var pItem = row.find('.param:eq(' + m + ')');
                                        if (pItem.length > 0) {
                                            pItem.text(param['title']);
                                        }
                                    })
                                }
                            })
                        }
                    });
                }
            });
        }
    },

    registerOlympicsTallyTemplateEvents: function () {
        this.nestedTabClickEvent();
    },

    renderLiveBlogTemplate: function (response) {
        if (response.items.length == 0) {
            return;
        }
        if (!this.sports.liveBlog.renderedOnce) {
            var html = $.render.liveBlog(response);
            var listHtml = $.render.liveBlogItemsList({ items: response.items });
            html = $(html);
            listHtml = $(listHtml);
            html.find('.append-list-items').append(listHtml.html());
            $('.live-widget').append(html[0]);
            this.sports.liveBlog.renderedOnce = true;
        } else {
            var cardItemsKeyById = {};
            response.items.forEach(function (item, i) {
                cardItemsKeyById[item['id']] = item;
            });
            var widgets = $('.live-widget').toArray();
            widgets.forEach(function (widget, i) {
                var newsCardsData = [];
                response.items.forEach(function (item, i) {
                    if ($(widget).find('.card[data-id=' + item['id'] + ']').length == 0) {
                        newsCardsData.push(cardItemsKeyById[item['id']]);
                    }
                })
                var listHtml = $.render.liveBlogItemsList({ items: newsCardsData });
                listHtml = $(listHtml);
                $(widget).find('.append-list-items').prepend(listHtml.html());
            });
        }

    },

    registerLiveBlogTemplateEvents: function () {
        $(d).on('click', '.live-widget .live-blog .header', function (e) {
            var section = $(this).siblings().filter('.container');
            section.toggleClass('close');
            if (section.hasClass('close')) {
                section.animate({ height: 0 }, 500);
            } else {
                section.animate({ height: 350 }, 500);
            }
        });
    },

    nestedTabClickEvent: function () {
        $(d).on('click', '.nested-tabs', function (e) {
            $(this).siblings().removeClass('selected');
            $(this).addClass('selected');
            var ids = [];
            $(this).siblings().each(function (i, item) {
                ids.push($(item).attr('data-id'));
            });
            ids.forEach(function (item, index) {
                $('.tab-content[data-mapped=' + item + ']').addClass('hidden');
            });
            $('.tab-content[data-mapped=' + $(this).attr('data-id') + ']').removeClass('hidden');
        });
    }
};

/**
 * Live widget module - end
 */

var pollCardModule = {

    init: function () {
        this.bindClickEventListener();
        this.assignCompletedStatus();
        this.attachClickEventListener();
        this.showCompletedPollsState();
    },

    bindClickEventListener: function () {
        this.bindedPollCardOptionClick = this.handlePollCardOptionClick.bind(this, '.poll-card-option');
    },

    attachClickEventListener: function () {
        $(d).on('click', '.poll-card-option', this.bindedPollCardOptionClick);
    },

    handlePollCardOptionClick: function () {
        var args = arguments;
        var optionSelected = $(args[1].target).closest(args[0]);
        var optionsParent = optionSelected.parents('.options-event-parent')
        if (optionsParent.hasClass('answered') || optionsParent.hasClass('completed')) {
            return;
        }

        track({
            'event': 'poll_card_option_click',
            'url': w.location.href
        });

        if(typeof(partner) != 'undefined' && partner && w['advtsConf'][advt_unit_name]['slots']['s8']){
            embedAds(optionsParent.attr('data-id'), [{slot : 's8', slotPos : optionsParent.find('.hidden-poll-card-ad-slug').eq(0)}], (optionsParent.prevAll('[data-advt-permit=enable]').length > 0));
        }

        optionsParent.addClass('answered');
        this.showHiddenScores(args[0], optionsParent);
        this.showHiddenEvent('.event', optionsParent, optionSelected);
        // this.manageOptionsUi(args[0], optionsParent, optionSelected);
        this.castVote(args[0], optionsParent, optionSelected);
    },

    showHiddenScores: function (query, optionsParent) {
        optionsParent.find(query + ' .subtitle').removeClass('hidden');
    },

    showHiddenEvent: function (query, optionsParent, optionSelected) {
        var id = optionSelected.attr('data-id');
        optionsParent.find('' + query + '[data-id="' + id + '"]' + '').removeClass('hidden');
    },

    getSortedScoresWithOption: function (query, optionsParent) {
        var options = optionsParent.find(query);
        var scores = [];
        options.each(function(i, item){
            var i = {
                option : item,
                score : parseFloat($(item).attr('data-score'))
            }
            scores.push(i)
        });
        scores.sort(function (a, b) {
            return a['score'] - b['score'];
        });
        return scores;
    },

    manageOptionsUi: function (query, optionsParent, optionSelected) {
        var sortedScoresWithOption = this.getSortedScoresWithOption(query, optionsParent);
        var sameScores = false;
        var prevElem = 0;
        for (var i = 0; i < sortedScoresWithOption.length; i++) {
            if (prevElem == parseInt(sortedScoresWithOption[i]['score'])) {
                sameScores = true;
            } else {
                sameScores = false;
            }
        }
        if (sameScores) {
            optionSelected.attr('data-score', '100');
            optionSelected.find('.subtitle').text('100%');
        }
        var optionWithMaxScore = sameScores ? optionSelected[0] : sortedScoresWithOption[sortedScoresWithOption.length - 1]['option'];
        var options = optionsParent.find(query);
        var highlightOption = (optionsParent.hasClass('completed'))?optionWithMaxScore:optionSelected[0];
        var _this = this;
        options.each(function (i, item) {
            var $item = $(item);
            if (item == highlightOption) {
                _this.showProgressBar($item, parseFloat($item.attr('data-score')), '#e3f0ff');
            } else {
                $item.find('.title').addClass($item.find('.title').attr('data-classlist'));
                _this.showProgressBar($item, parseFloat($item.attr('data-score')), '#f4f4f4');
            }
            $item.find('.subtitle').text($item.attr('data-score')+'%');
            $item.css({'border' : 0});
        });
    },

    showProgressBar: function ($item, value, color) {
        $item.find('.progress-bar').css({'backgroundColor' : color}).animate({width : "+=" + value +  "%"}, 'slow');
    },

    castVote: function (query, optionsParent, optionSelected) {
        var api = optionsParent.attr('data-apiurl');
        var _this = this;
        if (typeof api !== 'undefined') {
            $.ajax({
                async: true,
                type: "POST",
                data: {poll_id: optionSelected.attr('data-pollid'), choice: optionSelected.attr('data-choice')},
                url: api,
                success: function (result, status, xhr){
                    if (status === "success" && result['success']) {
                        _this.updateScores(query, optionsParent, result['data']);
                    } else {
                        optionsParent.removeClass('answered');
                    }
                    _this.manageOptionsUi(query, optionsParent, optionSelected);
                },
                error: function (xhr, status, error) {
                    _this.manageOptionsUi(query, optionsParent, optionSelected);
                    optionsParent.removeClass('answered');
                },
            });
        } else {
            return;
        }
    },

    updateScores: function (query, optionsParent, data) {
        var options = optionsParent.find(query);
        options.each(function (i, item) {
            var $item = $(item);
            $item.attr('data-score', data['options'][i]['score']);
        });
    },

    assignCompletedStatus: function () {
        $('.options-event-parent').each(function (i, item) {
            var $item = $(item);
            var startsAt = $item.attr('data-start');
            var endsAt = $item.attr('data-end');
            if (typeof startsAt === 'undefined' || typeof endsAt === 'undefined') {
                return;
            }
            var time = new Date().getTime();
            if (time >= new Date(parseInt(endsAt)).getTime()) {
                $item.addClass('completed');
            }
        });
    },

    showCompletedPollsState: function () {
        var _this = this;
        $('.options-event-parent').each(function (i, item) {
            var $item = $(item);
            if (!$item.hasClass('completed')) {
                return;
            }
            _this.showHiddenScores('.poll-card-option', $item);
            var sortedScoresWithOption = _this.getSortedScoresWithOption('.poll-card-option', $item);
            _this.manageOptionsUi('.poll-card-option', $item, sortedScoresWithOption[sortedScoresWithOption.length - 1]['option']);
            $item.find('.poll-result').removeClass('hidden');
        });
    }

}

var quizCardModule = {
    init : function (quizId) {
        if ($('.quiz-card-parent').length == 0) {
            return;
        }
        this.quizId = quizId;
        this.assignCompletedStatus();
        var completed = this.showCompletedQuizState();
        if (completed) {
            return;
        }
        this.bindClickEventListener();
        this.attachClickEventListener();
        this.resultsValue = [];
        this.quizHtmlState = this.getQuizHtmlState();
        this.correctSvg = $($('.quiz-card-parent[data-quizid="' + this.quizId + '"]').siblings('.quiz-siblings.svgs').find('.svg.correct'));
        this.incorrectSvg = $($('.quiz-card-parent[data-quizid="' + this.quizId + '"]').siblings('.quiz-siblings.svgs').find('.svg.incorrect'));
        this.currentQuestion = 0;
        // this.assignMaxHeight();
    },

    bindClickEventListener: function () {
        this.bindedTakeQuiz = this.handleTakeQuizClick.bind(this, '.take-quiz');
        this.bindedCloseQuiz = this.handleCloseQuizClick.bind(this, '.close-quiz');
        this.bindedQuizCardOptionClick = this.handleQuizCardOptionClick.bind(this, '.quiz-card-option');
        this.bindedShowAnswerClick = this.handleShowAnswerClick.bind(this, '.show-answer');
    },

    attachClickEventListener: function () {
        $(d).on('click', '.take-quiz[data-quizid="' + this.quizId + '"]', this.bindedTakeQuiz);
        $(d).on('click', '.close-quiz[data-quizid="' + this.quizId + '"]', this.bindedCloseQuiz);
        $(d).on('click', '.quiz-card-option[data-quizid="' + this.quizId + '"]', this.bindedQuizCardOptionClick);
        $(d).on('click', '.show-answer', this.bindedShowAnswerClick);
    },

    handleTakeQuizClick: function () {
        $('.quiz-card-parent[data-quizid="' + this.quizId + '"]').removeClass('hidden');
        $('.take-quiz[data-quizid="' + this.quizId + '"]').addClass('hidden');
        $('html').addClass('ovr-hidden');
        $('body').addClass('ovr-hidden');
        $('html').addClass('touch-action-none');
        $('body').addClass('touch-action-none');

        track({
            'event':'take_quiz',
            'url': w.location.href
        });

        if (typeof(partner) != 'undefined' && partner) {
            let quiz = $('.quiz-card-parent[data-quizid="' + this.quizId + '"]');
            let advtAlreadyRenderedOnce = typeof(quiz.attr('data-advt-quiz-card')) != 'undefined' && quiz.attr('data-advt-quiz-card');
            if(w['advtsConf'][advt_unit_name]['slots']['s8'] && !advtAlreadyRenderedOnce){
                embedAds(this.quizId, [{slot : 's8', slotPos : quiz.find('.hidden-quiz-card-ad-slug').eq(0)}], (quiz.prevAll('[data-advt-permit=enable]').length > 0));

                quiz.attr('data-advt-quiz-card', true);
            }
        }

    },
    
    handleCloseQuizClick: function () {
        $('.quiz-card-parent[data-quizid="' + this.quizId + '"]').addClass('hidden');
        $('.take-quiz[data-quizid="' + this.quizId + '"]').removeClass('hidden');
        $('html').removeClass('ovr-hidden');
        $('body').removeClass('ovr-hidden');
        $('html').removeClass('touch-action-none');
        $('body').removeClass('touch-action-none');
    },

    handleQuizCardOptionClick: function () {
        var args = arguments;
        var optionSelected = $(args[1].target).closest(args[0]);
        var optionsParent = optionSelected.parents('.slide')
        if (optionsParent.hasClass('answered')) {
            return;
        }
        optionsParent.addClass('answered');
        var rVal = optionSelected.attr('data-answer');
        this.resultsValue.push(rVal);
        this.currentQuestion++;

        track({
            'event': 'quiz_card_option_click',
            'question': 'Question number ' + optionsParent.attr('data-id').split('-').pop(),
            'url': w.location.href
        });

        this.manageSlideUi(optionSelected, optionsParent);
    },

    handleShowAnswerClick: function () {
        var args = arguments;
        var selectedQuestion = $(args[1].target).closest(args[0]);

        $('.show-answer[data-quizid="' + this.quizId + '"]').each((index, question) => {
            question = $(question);
            var answer = question.find('.answer');
            var show_answer_icon = question.find('.show-answer-icon');
            var hide_answer_icon = question.find('.hide-answer-icon');

            if(question.attr('data-id') == selectedQuestion.attr('data-id')){
                if (answer.hasClass('hidden')) {
                    answer.removeClass('hidden');
                    show_answer_icon.addClass('hidden');
                    hide_answer_icon.removeClass('hidden');
                }else {
                    answer.addClass('hidden');
                    show_answer_icon.removeClass('hidden');
                    hide_answer_icon.addClass('hidden');
                }
            }else {
                answer.addClass('hidden');
                show_answer_icon.removeClass('hidden');
                hide_answer_icon.addClass('hidden');
            }
        });
    },

    getQuizHtmlState: function () {
        return $('.quiz-card-parent[data-quizid="' + this.quizId + '"]').find('.slide').toArray();
    },

    assignMaxHeight: function () {
        var max = Number.MIN_VALUE;
        for (var index = 0; index < this.quizHtmlState.length; index++) {
            var $element = $(this.quizHtmlState[index]);
            if ($element.height() > max) {
                max = $element.height();
            }
        }
        $('.quiz-card-parent[data-quizid="' + this.quizId + '"]').css({
            height: max + "px"
        });
    },

    manageSlideUi: function (optionSelected, optionsParent) {
        var _this = this;
        var correctOption = optionsParent.find('.quiz-card-option[data-answer="true"]');

        if (optionSelected.attr('data-answer') == 'true') {
            var uiPromiseChain = this.showProgressBar(optionSelected, 100, '#0f9d58');
        }else{
            var uiPromiseChain = this.showProgressBar(optionSelected, 100, '#f13c3c');
        }
        optionSelected.addClass('cl-primary');
        optionSelected.find('.index').removeClass('cl-brand');
        uiPromiseChain
        .then(function (resolvedVal, rejectedVal) {
            return _this.showOptionSelectedStatus(optionSelected, optionsParent);
        })
        .then(function (resolvedVal, rejectedVal) {
            optionsParent.addClass('hidden');
            return _this.switchToNextQuestion();
        })
        .then(function (resolvedVal, rejectedVal) {
            _this.showFinalResults();
        })
    },

    showProgressBar: function ($item, value, color) {
        return new Promise(function (resolve, reject) {
            var width = 0;
            $item.find('.progress-bar').css({'backgroundColor' : color});
            var timer = setInterval(function () {
                if (width >= value) {
                    clearInterval(timer);
                    resolve('highlight-option');
                } else {
                    width++;
                    $item.find('.progress-bar').css({width : width + '%'});
                }
            }, 4);
        });
    },

    showCountDownTimer: function ($item) {
        var timerCount = 10;
        var timer = setInterval(function () {
            if (timerCount > 0) {
                timerCount--;
                $item.css("background", "radial-gradient(closest-side, white 69%, transparent 70% 100%), conic-gradient(#f10404 " + timerCount * 10 + "%, #ffe9eb 0)");
                $item.text(timerCount + "");
            } else {
                $item.addClass('hidden');
                clearInterval(timer);
            }
        }, 1000);
    },

    showOptionSelectedStatus: function (optionSelected, optionsParent) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var correctOption = optionsParent.find('.quiz-card-option[data-answer="true"]');
            optionSelected.find('.subtitle').fadeIn(225, 'swing');
            correctOption.find('.subtitle').fadeIn(225, 'swing');
            resolve([optionSelected, correctOption]);
        })
        .then(function (resolvedVal, rejectedVal) {
            if (resolvedVal[0] == resolvedVal[1]) {
                resolvedVal[0].find('.icon-embed').html(_this.correctSvg.html());
            } else {
                resolvedVal[0].find('.icon-embed').html(_this.incorrectSvg.html());
                resolvedVal[1].find('.icon-embed').html(_this.correctSvg.html());
            }
            resolvedVal[0].find('.icon-embed').fadeIn(225, 'swing', function () {
                resolvedVal[1].find('.icon-embed').fadeIn(225, 'swing', function () {
                    return 'icon-shown';
                });
            });
            resolvedVal[0].find('.icon-embed').fadeIn(225, 'swing')
            resolvedVal[1].find('.icon-embed').fadeIn(225, 'swing')
            return _this.addDelay(1350);
        })
        .then(function (resolvedVal, rejectedVal) {
            return 'status-shown';
        })
    },

    addDelay: function (timer) {
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                resolve('delay added');
            }, timer);
        });
    },

    switchToNextQuestion: function () {
        var _this = this;
        var $nextQues = $(this.quizHtmlState[this.currentQuestion]);
        return new Promise(function (resolve, reject) {
            $nextQues.find('.quiz-card-intermediary').fadeIn(675, 'swing', function () {
                resolve(true);                
            });
        })
        .then(function (resolvedVal, rejectedVal) {
            return _this.addDelay(765);
        })
        .then(function (resolvedVal, rejectedVal) {
            $nextQues.find('.quiz-card-intermediary').addClass('quiz-card-intermediary-animation');
            return true;
        })
        .then(function (resolvedVal, rejectedVal) {
            $nextQues.find('.quiz-card-intermediary').fadeOut(450, 'swing', function () {
                return true;
            });
        }).then(function (resolvedVal, rejectedVal) {
            return _this.addDelay(450);
        })
        .then(function (resolvedVal, rejectedVal) {
            $nextQues.find('.final').fadeIn(900, 'swing', function () {
                return true;                
            });
        })
        .then(function (resolvedVal, rejectedVal) {
            return 'question-switched';
        })
    },

    showFinalResults: function () {
        var length = this.quizHtmlState.length;
        if (this.currentQuestion == (length - 1)) {
            var $title = $(this.quizHtmlState[length-1]).find('.title');
            var $subTitle = $(this.quizHtmlState[length-1]).find('.subtitle');
            var $resultsBar = $(this.quizHtmlState[length-1]).find('.results-bar');
            var $resultsTimer = $(this.quizHtmlState[length-1]).find('.results-timer');
            var $questionResult = $('.question-result[data-quizid="' + this.quizId + '"]');
            var correctAnsCount = 0;
            for (var index = 0; index < this.resultsValue.length; index++) {
                var elem = this.resultsValue[index];
                if (elem === 'true') {
                    correctAnsCount++;
                    $questionResult.eq(index).append(this.correctSvg.html());
                } else {
                    $questionResult.eq(index).append(this.incorrectSvg.html());
                }
            }
            let remarkStr = "";
            let resultsPer = (correctAnsCount * 100)/this.resultsValue.length;
            if(resultsPer >= 70){
                remarkStr = $subTitle.text().replace('-remarks-', "Awesome, you rock!");
            }else if(resultsPer >= 30){
                remarkStr = $subTitle.text().replace('-remarks-', "Well, that is impressive!");
            }else{
                remarkStr = $subTitle.text().replace('-remarks-', "Check out the answers");
            }
            $subTitle.text(remarkStr);

            $resultsBar.css("background", "radial-gradient(closest-side, white 69%, transparent 70% 100%),conic-gradient(#f10404 " + resultsPer + "%, #ffe9eb 0)");
            var resultStr = "" + correctAnsCount + "/" + this.resultsValue.length + " correct";
            resultStr = $title.text().replace('-results-', resultStr);
            $title.text(resultStr);

            this.showCountDownTimer($resultsTimer);
            setTimeout(() => {
                this.handleCloseQuizClick();
            }, 10*1000);
        }
    },

    assignCompletedStatus: function () {
        var item = $('.quiz-card-parent[data-quizid="' + this.quizId + '"]')
        var $item = $(item);
        var startsAt = $item.attr('data-start');
        var endsAt = $item.attr('data-end');
        if (typeof startsAt === 'undefined' || typeof endsAt === 'undefined') {
            return;
        }
        var time = new Date().getTime();
        if (time >= new Date(parseInt(endsAt)).getTime()) {
            $item.addClass('completed');
        }
    },

    showCompletedQuizState: function () {
        var item = $('.quiz-card-parent[data-quizid="' + this.quizId + '"]')
        var $item = $(item);
        if ($item.hasClass('completed')) {
            $item.siblings('.quiz-siblings').remove();
            $item.remove();
            return true;
        }
        return false;
    }

}

var ampStoryPlayer = {
    player : w.document.querySelector("amp-story-player"),

    events : function () {
        this.player.addEventListener("storyEnd", (data) => {});

        this.player.addEventListener("amp-story-player-close", () => {
            w.document.querySelector(".web-stories-list").classList.remove("show");
        });
    },

    initializePlayer : function () {
        if(this.player){
            this.player = w.document.querySelector("amp-story-player");
            if (this.player.isReady) {
                this.events();
                return this.player;
            }

            this.player.addEventListener("ready", () => {
                this.events();
            });
        }

        return;
    },

    show: function (story) {
        this.player.show(story).then(data => {
            w.document.querySelector(".web-stories-list").classList.add("show");
        });
    },

    addStories: function (stories) {
        this.player.add(stories);
    },

    nextStory: function () {
        this.player.go(1);
    },

    getMoreStories: function(url) {

        successCb = function (result, status, xhr) {
            if (status === "success" && result['success']) {
                let data = result.data.list.data.map(item => {
                    return {href: item.url, title: item.title};
                });
                this.addStories(data);
            }else{
                return;
            }
        },
    
        errorCb = function (xhr, status, error) {
            this.errorReporter();
        },

        $.ajax({
            async: true,
            type: "GET",
            url: url,
            success: callBackBinder(successCb, this),
            error: callBackBinder(errorCb, this),
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", this.token);
            }
        });
    },
}

/* Polyfill EventEmitter. */
var EventEmitter = function () {
    this.events = {};
};

EventEmitter.prototype.on = function (event, listener) {
    if (typeof this.events[event] !== 'object') {
        this.events[event] = [];
    }

    this.events[event].push(listener);
};

EventEmitter.prototype.removeListener = function (event, listener) {
    var idx;
    var findIdx = function (stack, key) {
        return stack.indexOf(key);
    }
    if (typeof this.events[event] === 'object') {
        idx = findIdx(this.events[event], listener);

        if (idx > -1) {
            this.events[event].splice(idx, 1);
        }
    }
};

EventEmitter.prototype.emit = function (event) {
    var i, listeners, length, args = [].slice.call(arguments, 1);

    if (typeof this.events[event] === 'object') {
        listeners = this.events[event].slice();
        length = listeners.length;

        for (i = 0; i < length; i++) {
            listeners[i].apply(this, args);
        }
    }
};

EventEmitter.prototype.once = function (event, listener) {
    this.on(event, function g() {
        this.removeListener(event, g);
        listener.apply(this, arguments);
    });
};

var notifPermission = "notAllowed";

var inPrivateMode = function () {
    return new Promise(function (resolve) {
        var db;
        var on = function () { resolve(true) },
            off = function () { resolve(false) },
            tryls = function tryls() {
                try {
                    localStorage.length ? off() : (localStorage.x = 1, localStorage.removeItem("x"), off());
                } catch (e) {
                    // Safari only enables cookie in private mode
                    // if cookie is disabled then all client side storage is disabled
                    // if all client side storage is disabled, then there is no point
                    // in using private mode
                    n.cookieEnabled ? on() : off();
                }
            }

        // Blink (chrome & opera)
        w.webkitRequestFileSystem ? webkitRequestFileSystem(0, 0, off, on)
            // FF
            : "MozAppearance" in d.documentElement.style ? (db = indexedDB.open("test"), db.onerror = on, db.onsuccess = off)
                // Safari
                : /constructor/i.test(w.HTMLElement) ? tryls()
                    // IE10+ & edge
                    : !w.indexedDB && (w.PointerEvent || w.MSPointerEvent) ? on()
                        // Rest
                        : off();
    });
}

var oneSignalManager = {

    loadScript: function () {
        return new Promise(this.checkOneSignalIsPresent.bind(this)).then(function (result) {
            if (!result) {
                var as = d.createElement('script');
                as.type = 'text/javascript';
                as.async = true;
                as.src = 'https://o.cdn.newsbytesapp.com/assets/js/OneSignalSDK.js';
                var s = d.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(as, s);
                return new Promise(function (resolve, reject) {
                    as.onload = function (event) {
                        resolve('loaded');
                    }
                })
            } else {
                return 'already present';
            }
        })

    },

    notify: function () {
        var ls = this.loadScript()
        ls.then(function (result) {
            if (!OneSignal.initialized) OneSignal.push(["init", OneSignalConfig]);
            return inPrivateMode();
        })
            .then(function (isPrivateMode) {
                if (!isPrivateMode) {
                    OneSignal.push(function () {
                        // If we're on an unsupported browser, do nothing
                        if (!OneSignal.isPushNotificationsSupported()) {
                            return;
                        }
                        OneSignal.getNotificationPermission(function (permission) {
                            notifPermission = permission;
                            if (permission == "default") {
                                var popup = $('.subscribePopup');
                                if (!checkNotifCookie()) {
                                    popup.removeClass('hidden');
                                    var src = popup.find('.image').attr('data-src');
                                    popup.find('.image').attr('src', src);
                                    setTimeout(function () {
                                        popup.find('.image').addClass('pulse');
                                    }, 500)
                                } else {
                                    // displaySubscribeInset('block');
                                    showNotifBellIcon(true);
                                }
                            }
                            else if (permission == "granted") {
                                OneSignal.isPushNotificationsEnabled(function (isEnabled) {
                                    if (!isEnabled) {
                                        // displaySubscribeInset('block');
                                        showNotifBellIcon(true);
                                    } else {
                                        OneSignal.getTags(function (tags) {
                                            if (tags != null && Object.keys(tags).length != 0) {
                                                jQuery.each(tags, function (id, val) {
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                            else if (permission == "denied") {
                            }
                        });
                        OneSignal.on('notificationPermissionChange', function (permissionChange) {
                            var currentPermission = permissionChange.to;
                            notifPermission = currentPermission;
                            if (currentPermission == "granted") {
                                OneSignal.push(["setSubscription", true]);
                            }
                            else if (currentPermission == "denied") {
                                OneSignal.push(["setSubscription", false]);
                            }
                            else {
                                OneSignal.push(["setSubscription", false]);
                            }
                        });
                    });
                }
            })
            .catch(function (err) {
                console.error(err.message);
            });
    },

    checkOneSignalIsPresent: function (resolve, reject) {
        if (typeof w.OneSignal === 'function') {
            resolve(true);
        } else {
            resolve(false);
        }
    }
}

var checkNotifCookie = function () {
    var name = "notifInteract=";
    var decodedCookie = decodeURIComponent(d.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) {
            return true
        }
    }
    return false;
}

var allowNotif = function (event) {
    var d = new Date();
    d.setTime(d.getTime() + (24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    d.cookie = "notifInteract=allow;" + expires + ";path=/";
    $('.subscribePopup').addClass('hidden');
    /* displaySubscribeInset('none');
      $('.notif-overlay').removeClass('enabled'); */
    showNotifBellIcon(false);
    OneSignal.push(["registerForPushNotifications"]);
    OneSignal.push(["setSubscription", true]);
    /* $('#unsubscribe').removeClass('hidden');
    $('#subscribe').addClass('hidden'); */
    // validateGlobals('ga') ? ga('send', 'event', "webNotification", "subscribe", w.location.href) : null;
    track('send', 'event', 'webNotification', 'subscribe', w.location.href);
}


var cancelNotif = function () {
    // $('body,html').removeClass('scroll-y-hidden');
    var d = new Date();
    d.setTime(d.getTime() + (24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    d.cookie = "notifInteract=later;" + expires + ";path=/";
    $('.subscribePopup').addClass('hidden');
    showNotifBellIcon(true);
    /* displaySubscribeInset('block');
    $('.notif-overlay').removeClass('enabled'); */
}

var showNotifBellIcon = function (display) {
    if (display) {
        var icon = $('.notifBell .bellIcon');
        var html = '<img src="' + icon.attr('src') + '" alt="' + icon.attr('alt') + '" class="' + icon.attr('class') + '"/>';
        $(icon).replaceWith(html);
        $('.notifBell').removeClass('hidden');
    } else {
        $('.notifBell').addClass('hidden')
    }
}


var copyToClipboard = function (elem) {
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        target = d.getElementById(targetId);
        if (!target) {
            var target = d.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            d.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = d.activeElement;
    target.focus();
    target.focus({ preventScroll: true });
    target.setSelectionRange(0, target.value.length);
    // copy the selection
    var succeed;
    try {
        succeed = d.execCommand("copy");
    } catch (e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}

var randomId = function (length) {
    var id = '';
    var charString = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var stringLength = charString.length;
    for (var i = 0; i < length; i++) {
        id += charString.charAt(Math.floor(Math.random() * stringLength));
    }
    return id;
}

w.fbAsyncInit = function () {
    FB.init({
        appId: '349043958860717',
        cookie: true,
        xfbml: true,
        version: 'v4.0'
    });
}
var launchFb = function (callb) {
    var d = document;
    var s = 'script';
    var id = 'facebook-jssdk';
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
    if (typeof callb != 'undefined') {
        js.onload = function () { callb() };
    }
}

var loadAsyncAnalyticsScripts = function () {
    if (typeof loadFbAnalytics === 'function') {
        loadFbAnalytics();
    }
}

var getPageUrl = function() {
    var url = w.location.protocol+'//'+w.location.host+w.location.pathname;
}

var evtEmitter = new EventEmitter();
evtEmitter.once('initLoginFrame', initLoginBackground);
evtEmitter.once('initAnalyticsScripts', loadAsyncAnalyticsScripts);

var slideShowMaker = function ($item) {
    var query = '#' + $($item).attr('id');
    var config = { appendDots: $(query).closest('.slide-show-parent').find('.append-slide-show-dots'),};
    if ($($item).hasClass('with-adaptiveHeight')) {
        config = Object.assign(Object.assign({}, config), { adaptiveHeight: true });
    }
    if ($($item).hasClass('with-centerMode')) {
        config = Object.assign(Object.assign({}, config), { centerMode: true, variableWidth: true,});
    }
    var slideshowInstance = Object.create(slideshowModule);
    slideshowInstance.init(query, config);
    slideshowInstance.activateSlideshow();
    slideshowInstance.handleEvents();
    evtEmitter.on('pauseSlideshowOnDropdownOpen', slideshowInstance.config.pauseSlideshow.bind(slideshowInstance));
    evtEmitter.on('resumeSlideshowOnDropdownClose', slideshowInstance.config.resumeSlideshow.bind(slideshowInstance));
}

$.fn.isInViewport = function () {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
};

var pollCardModuleInstance;

(function urlHistoryModifier() {
    var stateObj = {id : 'nb-1', url : w.location.href, explicit : true}
    w.history.pushState(stateObj, 'Current page', '');
})();

function getFilteredQueryParams(url) {
    let filteredQueryParams;
    if(url === void 0 || !url.includes("?")){
        filteredQueryParams = w.location.search;
    }else{
        filteredQueryParams = '?' + url.split("?").splice(-1)[0];
    }
    if(filteredQueryParams){
        filteredQueryParams = "?" + filteredQueryParams.substring(1).split("&").filter(item => {
            if(item.split("=")[0] == 'utm_medium' || item.split("=")[0] == 'utm_source' || item.split("=")[0] == 'partner'){
                return true;
            }
            return false;
        }).join("&");
    }
    return filteredQueryParams;
}

function updateUrlQueries () {
    let filteredQueryUrl = w.location.origin + '/curated-news' + getFilteredQueryParams();
    let clickableLogos = $('.clickable-logo');
    clickableLogos.each(function (i, item) {
        let $item = $(item);
        $item.attr('href', filteredQueryUrl);
    })
    let accordionItems = $('.accordion-item');
    accordionItems.each(function (i, item) {
        let $item = $(item);
        let accordianCategory = '#' + $item.attr('href').split("#")[1];
        $item.attr('href', filteredQueryUrl + accordianCategory);
    })
}

var removeExternalLinks = function(id){
    $('article#' + id).find('p').each(function() {
        $(this).find('a').each(function() {
            $(this).replaceWith( "<span>" + $(this).html() + "</span>" );
        })
    });

    $('article#' + id).find('ul').each(function() {
        $(this).find('a').each(function() {
            $(this).replaceWith( "<span>" + $(this).html() + "</span>" );
        })
    });
}

var fallbackAdModule = {
    init: function(slots, title, url){
        this.slots = slots;
        this.title = title,
        this.logo = $('#_svg_logo_' + nblocale),
        this.currentEventIndex = 0;
        this.nextEvents = [];
        this.nextEventLoadUrl = url;
        this.allowLoadAd = false;
        this.loadAd = this.loadAd.bind(this);
        this.loadNextEvents();

        $(d).on('click', '.fallback-ad', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var url = $(this).attr('href');
            if (url.indexOf('http') !== 0) {
                url = w.location.protocol+'//'+w.location.host+url;
            }
            var title = $(this).text();
            if (typeof url !== 'undefined') {
                track({
                    'event':'fallback_ad_click',
                    'fallback_ad_url':url,
                    'article_id': w.location.href
                });
                w.setTimeout(function() {
                    w.location.href = url;
                }, 200);
            }
        });
    },

    loadAd: function (slot) {
        if(!this.allowLoadAd && this.currentEventIndex >= this.nextEvents.length - 2){
            this.slots.push(slot);
            return;
        }
        let slotPos = $('#' + slot['id']);

        let events = [];
        events.push(this.nextEvents[this.currentEventIndex]);
        events.push(this.nextEvents[this.currentEventIndex+1]);

        this.currentEventIndex += 2;

        let title = this.title;
        let logo = this.logo;
        slot = {
            ...slot,
            title: title,
            logo: logo,
            events: events,
        }

        if(slot.type == 'multi-size'){
            slot = {
                ...slot,
                width: slot['size'][0][0],
                height: slot['size'][0][1]
            }
        }else{
            slot = {
                ...slot,
                width: slot['size'][0],
                height: slot['size'][1]
            }
        }
    
        if(slot['placementName'] == 'InArticleMedium'){
            var slotHtml = $.render.fallbackAdvt(slot);
            slotPos.html(slotHtml);
        }
        slotPos.find('.icon-embed').html(logo.html());

        if(this.currentEventIndex >= this.nextEvents.length - 7){
            this.loadNextEvents();
            return;
        }
    },

    loadNextEvents: function () {
        this.allowLoadAd = false;
        $.ajax({
            async: true,
            type: "GET",
            url: this.nextEventLoadUrl,
            success: callBackBinder(this.successCbForEventsLoading, this),
            error: callBackBinder(this.errorCbForEventsLoading, this),
        });
    },

    successCbForEventsLoading: function (result, status, xhr) {
        if (status === "success" && result['success']) {
            this.renderManagerForEventsLoading(result['data']);
        } else {
            return;
        }
    },

    errorCbForEventsLoading: function (xhr, status, error) {
        console.log('failed to load events');
    },

    renderManagerForEventsLoading: function (resp) {
        var eventsResp = Object.assign({}, resp);
        eventsResp['events'] = eventsResp['events'].filter(function (item, index) {
            return ($('#wrapper-events-list-' + item['htmlId']).length == 0);
        }).map(function (item) {
            if (typeof(item['clickbaityTitle']) != 'undefined' && item['clickbaityTitle']) {
                item['title'] = item['clickbaityTitle'];
            }
            item['url'] = item['url'] + getFilteredQueryParams();

            return item;
        });

        eventsResp['events'].forEach(event => {
            this.nextEvents.push(event);
        });

        this.allowLoadAd = true;

        while (this.slots.length > 0) {
            let slot = this.slots.pop();
            this.loadAd(slot);
        }

        if (typeof(partner) != 'undefined' && partner) {
            this.nextEventLoadUrl = eventsResp['load']['nextPartnerEventsApi'];
        }else{
            this.nextEventLoadUrl = eventsResp['load']['nextEventsApi'];
        }
        return;
    },
}

var eventListCards = {
    init: function () {
        if ($('.events-list-card').length == 0) {
            return;
        }

        this.loadingNextEvents = false;
        this.nextArticleLoadUrl = ((typeof $('.events-list-card').attr('data-next-latest') !== 'undefined') ? $('.events-list-card').attr('data-next-latest') : $('.events-list-card').attr('data-next'));
        this.onScrollEventHandler = this.onScrollEventHandler.bind(this);
        $(window).scroll(this.onScrollEventHandler);
    },

    onScrollEventHandler: function () {
        if ($(window).scrollTop() + $(window).height() + 500 > $(d).height() - (0.5 * $(window).height()) && !this.loadingNextEvents) {
            this.loadNextEvents();
        }
    },

    loadNextEvents: function () {
        this.loadingNextEvents = true;

        $.ajax({
            async: true,
            type: "GET",
            url: this.nextArticleLoadUrl,
            success: callBackBinder(this.successCbForEventsLoading, this),
            error: callBackBinder(this.errorCbForEventsLoading, this),
        });
    },

    successCbForEventsLoading: function (result, status, xhr) {
        if (status === "success" && result['success']) {
            this.renderManagerForEventsLoading(result['data']);
        } else {
            return;
        }
    },

    errorCbForEventsLoading: function (xhr, status, error) {
        console.log("Failed to get events");
    },

    renderManagerForEventsLoading: function (resp) {
        var eventsResp = Object.assign({}, resp);

        if (typeof(partner) != 'undefined' && partner) {
            eventsResp['events'] = eventsResp['events'].map(function (item) {
                if (typeof(partnerLogo) != 'undefined' && partnerLogo) {
                    item['partnerLogo'] = partnerLogo;
                }
                if (typeof(item['clickbaityTitle']) != 'undefined' && item['clickbaityTitle']) {
                    item['title'] = item['clickbaityTitle'];
                }
                
                item['url'] = item['url'] + getFilteredQueryParams();
                return item;
            });
        }
        var html = $.render.listItemTitleWithSqrImg(eventsResp);
        $('.events-list-card').append(html);
        
        if (typeof(partner) != 'undefined' && partner) {
            var embedAdSlots = [];
            let eventId = eventsResp['events'].pop()['eventId'];

            $('.events-list-card').find('.hidden-ad-slug').each(function (i, item) {
                embedAdSlots.push({slot : 's2', slotPos : $(item)});
                $(item).removeClass('hidden-ad-slug');
            });

            if(embedAdSlots.length > 0){
                embedAds(eventId, embedAdSlots, ($('.events-list-card').prevAll('[data-advt-permit=enable]').length > 0));
            }
        }

        this.delayImageLoading();
        this.loadingNextEvents = false;
        this.nextArticleLoadUrl = eventsResp['load']['partnerapi'];
    },

    delayImageLoading: function () {
        var lazyLoader = Object.create(lazyLoadModule);
        lazyLoader.init('.lazy', { ios: { query: '.inset-img-hidden', device: isIos() } });
        lazyLoader.lazy();
    },

}

function embedAds (eventId, slots, advtsAlreadyRenderedOnce) {
    if(typeof(partner_ads_type) == 'undefined' || typeof(advt_unit_name) == 'undefined'){
        return;
    }
    if(partner_ads_type == 'meson'){
        for (var index = 0; index < slots.length; index++) {
            var slotElementDefaultConfig = {type : advt_unit_name};
            var slotElement = Object.assign(Object.assign({}, slotElementDefaultConfig), slots[index]);

            var slotElementRenderingConfig = {
                id : slotElement['slot'] + '-' + eventId + '-' + index,
                slot : slotElement['slot'],
                type : slotElement['type'],
            }
            var slotHtml = $.render.advt(slotElementRenderingConfig);
            $(slotHtml).insertBefore(slotElement['slotPos']);

            var advtTypeId = w['advtsConf'][slotElement['type']]['id'];
            var advtTypeSlotConfig = w['advtsConf'][slotElement['type']]['slots'][slotElement['slot']];
            advtTypeSlotConfig['id'] = slotElement['slot'] + '-' + eventId + '-' + index;

            let slot = JSON.parse(JSON.stringify(advtTypeSlotConfig));
            slotLoaderDelayed(slot);
        }
    }
    else if(typeof w.adsbygoogle != 'undefined' && partner_ads_type == 'adsense'){
        for (var index = 0; index < slots.length; index++) {
            var slotElementDefaultConfig = {type : advt_unit_name};
            var slotElement = Object.assign(Object.assign({}, slotElementDefaultConfig), slots[index]);

            var slotElementRenderingConfig = {
                id : slotElement['slot'] + '-' + eventId + '-' + index,
                slot : slotElement['slot'],
                slotId: w['advtsConf'][slotElement['type']]['slots'][slotElement['slot']]['id'],
                clientId : w['advtsConf'][slotElement['type']]['slots'][slotElement['slot']]['clientId'],
                width: w['advtsConf'][slotElement['type']]['slots'][slotElement['slot']]['size']['0']['0'],
                height: w['advtsConf'][slotElement['type']]['slots'][slotElement['slot']]['size']['0']['1'],
                type : slotElement['type'],
            }
            var slotHtml = $.render.adsenseAdvt(slotElementRenderingConfig);
            $(slotHtml).insertBefore(slotElement['slotPos']);
            (adsbygoogle = w.adsbygoogle || []).push({});
        }
    }
    else if(typeof googletag != 'undefined'){
        googletag.cmd.push(function() {
            for (var index = 0; index < slots.length; index++) {
                var slotElementDefaultConfig = {type : advt_unit_name};
                var slotElement = Object.assign(Object.assign({}, slotElementDefaultConfig), slots[index]);

                var slotElementRenderingConfig = {
                    id : slotElement['slot'] + '-' + eventId + '-' + index,
                    slot : slotElement['slot'],
                    type : slotElement['type'],
                }
                var slotHtml = $.render.advt(slotElementRenderingConfig);
                $(slotHtml).insertBefore(slotElement['slotPos']);

                var advtTypeId = w['advtsConf'][slotElement['type']]['id'];
                var advtTypeSlotConfig = w['advtsConf'][slotElement['type']]['slots'][slotElement['slot']];

                if(typeof fetchAmazonBids != 'undefined'){
                    apstag.fetchBids({slots: [{
                        slotID : slotElementRenderingConfig['id'],
                        slotName : advtTypeSlotConfig['name'],
                        sizes : advtTypeSlotConfig['size'],
                    }],}, function(bids) {
                        googletag.cmd.push(function() {
                            apstag.setDisplayBids();
                            googletag.pubads().refresh();
                        });
                    });
                }

                googletag.defineSlot(advtTypeId + advtTypeSlotConfig['name'], advtTypeSlotConfig['size'], slotElementRenderingConfig['id']).addService(googletag.pubads());
                if (! advtsAlreadyRenderedOnce) {
                    googletag.display(slotElementRenderingConfig['id']);
                }
            }
            if(typeof fetchAmazonBids != 'undefined'){
                googletag.pubads().disableInitialLoad();
            }

            if (advtsAlreadyRenderedOnce) {
                googletag.pubads().disableInitialLoad();
                googletag.pubads().collapseEmptyDivs();
                googletag.enableServices();
            }
        });
    }
}

function showInterstialAd (url) {
    if(w['advtsConf'][advt_unit_name]['slots']['s6']){
        let article = $('article').first();
        let interstialAdSlug = article.find('.hidden-interstial-ad-slug');

        if(!interstialAdSlug.length){
            w.location.href = url;
            return;
        }

        embedAds(article.attr('id'), [{slot : 's6', slotPos : interstialAdSlug.eq(0)}], article.prevAll('[data-advt-permit=enable]').length > 0);

        interstialAdSlug.eq(0).removeClass('hidden-interstial-ad-slug');

        article.find('.interstial-ad-parent').eq(0).removeClass('hidden');
        $('html').addClass('ovr-hidden');
        $('body').addClass('ovr-hidden');
        $('html').addClass('touch-action-none');
        $('body').addClass('touch-action-none');

        $(d).on('click', '.interstial-ad-parent', function(e) {
            $('.interstial-ad-parent').addClass('hidden');
            $('html').removeClass('ovr-hidden');
            $('body').removeClass('ovr-hidden');
            $('html').removeClass('touch-action-none');
            $('body').removeClass('touch-action-none');

            w.location.href = url;
        });
    }else{
        w.location.href = url;
    }
};

w.readyFunction = function() {

    d.documentElement.className += " fonts-loaded";

    if(w.location.href.indexOf('/story') !== -1) {
        const elements = d.querySelectorAll('script[type="application/ld+json"]');
        for (var i = elements.length - 1; i >= 0; i--) {
            var element = JSON.parse(elements[i].textContent);
            if((element['@type'] == 'NewsArticle') && (typeof element['datePublished'] != undefined)) {
                track({
                    'published_date': element['datePublished'],
                });
                break;
            }
        }
    }

    $('.nbrh').each(function(key, item){
        $(item).removeClass('nbrh');
    });
    var layout = new htmlLayout('.extensible-layout');
    // var stickSidebar = new stickySidebar('.sidebar.sticky-nature');

    var ddInstance = Object.create(dropDown);
    ddInstance.init('.drdown-selector', '.drdown-parent', '.drdown-box');
    var tabSelector = Object.create(verticalTabSelector);
    tabSelector.init();

    $.ajax({
        async: true,
        type: "GET",
        url: webComponentTemplateRoute,
        success: function (result, status, xhr) {
            if (status === "success") {
                $(result).filter('template').each(function (index, item) {
                    $.templates($(item).attr('id'), item);
                });
                var loadMoreInstance = Object.create(loadMore);
                loadMoreInstance.init();
                var navigationInstance = Object.create(navigation);
                navigationInstance.init(tabSelector.active);
                var videoModuleInstance = Object.create(videoModule)
                videoModuleInstance.init();
                var paginatedResponsesComponentInstance = Object.create(paginatedResponsesComponent);
                paginatedResponsesComponentInstance.init();
                paginatedResponsesComponentInstance.handlePanelEvents();
                if (typeof widgetsConf !== 'undefined') {
                    var widget = Object.create(liveScoreWidget);
                    widget.init();
                    widget.regulate();
                }

                if (typeof fallbackAdsData != 'undefined' && typeof fallBackAdSlots != 'undefined'){
                    w.nb_fallbackAd = Object.create(fallbackAdModule);
                    w.nb_fallbackAd.init(fallBackAdSlots, fallbackAdsData.title, fallbackAdsData.url);
                }
            }
        },
        error: function (xhr, status, error) {

        }
    });

    var querySubmit = Object.create(querySubmitModule);
    querySubmit.init();
    /*var lazyLoader = Object.create(lazyLoadModule);
    lazyLoader.init('.lazy', { ios: { query: '.inset-img-hidden', device: isIos() } });
    lazyLoader.lazy();
    $('.tabs label').click(function (event) {
        lazyLoader.lazyOnEvent($(event.target).closest('.tabs').find('.tab-content .lazy'), { bind: 'event', delay: 0 });
    });*/
    w.lazzyLoader = Object.create(lazyLoadModule);
    w.lazzyLoader.init('.lazy', { ios: { query: '.inset-img-hidden', device: isIos() } });
    w.lazzyLoader.lazy();
    $('.tabs label').click(function (event) {
        w.lazzyLoader.lazyOnEvent($(event.target).closest('.tabs').find('.tab-content .lazy'), { bind: 'event', delay: 0 });
    });

    $('.slider-init').slick(sliderConf[$('.slider-init').attr('data-sliderconf')]);

    $('.slide-show.default').each(function (i, item) {
        slideShowMaker(item);
    })

    var ajaxTemplateInstance = Object.create(ajaxTemplates)
    ajaxTemplateInstance.init();

    var eventCardsInstance = Object.create(eventCards)
    eventCardsInstance.init();

    var tableInstance = Object.create(tableModule)
    tableInstance.init();
    tableInstance.handleEvents();

    pollCardModuleInstance = Object.create(pollCardModule);
    pollCardModuleInstance.init();

    var quizCardModuleInstance = Object.create(quizCardModule);
    quizCardModuleInstance.init($('.quiz-card-parent').first().attr('data-quizid'));

    var ampStoryPlayerInstance = Object.create(ampStoryPlayer);
    ampStoryPlayerInstance.player = ampStoryPlayerInstance.initializePlayer();

    var eventListCardsInstance = Object.create(eventListCards)
    eventListCardsInstance.init();

    $('.slider-news').each(function (i, item) {
        let id = $(item).children('.tabs').attr('id');
        var sliderModuleInstance = Object.create(sliderModule);
        sliderModuleInstance.init(id);
    });


    $('.pagination-parent').each(function (i, item) {
        var paginateInstance = Object.create(paginationModule);
        paginateInstance.init($(item));
    })

    $(d).on('click', '.collapse-bttn', function (e) {
        var id = $(this).attr('data-id');
        var section = $('#collapsible-' + id);
        if ($(this).hasClass('open')) {
            section.animate({ height: section.get(0).scrollHeight }, 500);
        } else if ($(this).hasClass('close')) {
            section.animate({ height: 0 }, 500);
        }
        $('.collapse-bttn[data-id=' + id + ']').toggleClass('hidden');
    });

    $(d).on('click', '.tab-item', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var url = getPageUrl();
        var title = $(this).text();
        track({
            'event':'suggested_category_navigation',
            'suggestion_category':title,
            'article_category': undefined,
            'article_name': d.title,
            'article_id': url,
            'User_ID': undefined
        });
    });
    $(d).on('click', 'a.clickable-item', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var url = $(this).attr('href');
        if (url.indexOf('http') !== 0) {
            url = w.location.protocol+'//'+w.location.host+url;
        }
        var title = $(this).text();
        if (typeof url !== 'undefined') {
            track({
                'event':'article_click',
                'article_name':title,
                'article_category':undefined,
                'author_name':undefined,
                'article_id': url,
                'User_ID':undefined
            });
            w.setTimeout(function() {
                w.location.href = url;
            }, 200);
        }
    });
    $(d).on('click', '.clickable-target', function (e) {
        var url = $(this).attr('data-url');
        if (url.indexOf('http') !== 0) {
            url = w.location.protocol+'//'+w.location.host+url;
        }
        var category = $(this).find('a').eq(0).text();
        var title = $(this).find('a').eq(1).text();
        if (typeof url !== 'undefined') {
            track({
                'event':'article_click',
                'article_name':title,
                'article_category':category,
                'author_name':undefined,
                'article_id': url,
                'User_ID':undefined
            });
            w.setTimeout(function() {
                w.location.href = url;
            }, 200);
        }
    });

    $(d).on('click', '.modal-opener', function (e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        var title = $(this).attr('title');
        if ((typeof url === 'undefined') || ($('.full-screen-modal').length === 0)) {
            return;
        }
        if (typeof title === 'undefined') {
            title = 'Iframe for ' + url;
        }
        $('html').addClass('hide-native-scrollbar');
        var iframeElem = '<iframe src="' + url + '" title="' + title + '" frameborder="0" class="wd-full fx-grow-1" allowFullScreen></iframe>';
        $(iframeElem).insertAfter('.full-screen-modal .insert-content');
        $('.full-screen-modal').removeClass('hidden');
    });

    $(d).on('click', '.start-player', function (e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        var title = $(this).attr('title');
        if ((typeof url === 'undefined') || ($('.full-screen-modal').length === 0)) {
            return;
        }
        if (typeof title === 'undefined') {
            title = 'Iframe for ' + url;
        }
        $('html').addClass('hide-native-scrollbar');
        ampStoryPlayer.show(url);
        
    });

    $(d).on('click', '.add-more-stories', function (e) {
        e.preventDefault();
        var url = $(this).attr('data-api');
        ampStoryPlayer.getMoreStories(url);
    })

    $(d).on('click', '.close-full-screen-modal', function (e) {
        $('.full-screen-modal iframe').remove();
        $('.full-screen-modal').addClass('hidden');
        $('html').removeClass('hide-native-scrollbar');
    });

    $('.cover-card-img').each(function () {
        var item = $(this)
        item.removeClass("hidden");
    })
    
    if($(d).find('twitter-card-embed')){
        (function() {
            var cads = d.createElement("script");
            cads.async = true;
            cads.type = "text/javascript";
            cads.src = "//platform.twitter.com/widgets.js";
            var node = d.getElementsByTagName("script")[0];
            node.parentNode.insertBefore(cads, node);
        })();
    }

    if($(d).find('instagram-card-embed')){
        (function() {
            var cads = d.createElement("script");
            cads.async = true;
            cads.type = "text/javascript";
            cads.src = "//platform.instagram.com/en_US/embeds.js";
            var node = d.getElementsByTagName("script")[0];
            node.parentNode.insertBefore(cads, node);
        })();
    }
    
    $.views.helpers({
        compareGT: function (val, threshold) {
            return val > threshold;
        },
        compareLT: function (val, threshold) {
            return val < threshold;
        },
        addQueryToUrl: function (url, query) {
            if (url.indexOf('?') > -1)
                return url.replace('?', '?' + query + '&');
            else
                return url + '?' + query;
        },
        eql: function (val1, val2) {
            return val1 === val2;
        },
        neql: function (val1, val2) {
            return val1 != val2;
        },
        eqllen: function (val1, val2) {
            var len = [], total = 0;
            for (var i = 0; i < arguments.length; i++) {
                if (typeof arguments[i] == 'object') {
                    len[i] = Object.keys(arguments[i]).length;
                }
                else if (typeof arguments[i] == 'array') {
                    len[i] = arguments[i].length;
                }
                else if (typeof arguments[i] == 'string') {
                    len[i] = arguments[i].length;
                }
                else if (typeof arguments[i] == 'number') {
                    len[i] = arguments[i];
                }
                else
                    len[i] = 0;
            }
            for (i = 0; i < len.length; i++) {
                total += len[i];
            }
            return total / len.length == len[0];
        },
        _and: function () {
            var result = true;
            for (var i = 0; i < arguments.length; i++) {
                result = result && arguments[i];
            }
            return result;
        },
        _nand: function () {
            var result = true;
            for (var i = 0; i < arguments.length; i++) {
                result = result && arguments[i];
            }
            return !result;
        },
        noop: function (text) {
            if (typeof text == 'undefined') {
                text = '';
            }
        },
        capitalize: function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        },
        randomId: function (length) {
            var id = '';
            var charString = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var stringLength = charString.length;
            for (var i = 0; i < length; i++) {
                id += charString.charAt(Math.floor(Math.random() * stringLength));
            }
            return id;
        },
        tagColor: function (key) {
            return colorMap[key.toLowerCase()];
        },
        lengthOfArr: function (arr) {
            return arr.length;
        },
        rawUrlEncode: function (url) {
            return encodeURIComponent(url).replace(/[!'()*]/g, function (c) {
                return '%' + c.charCodeAt(0).toString(16);
            });
        },
        incrementChar: function (c, val) {
            return String.fromCharCode(c.charCodeAt(0) + val);
        },
        parseInt: function (val) {
            return parseInt(val);
        },
    });

    if (typeof hasFb !== 'undefined' && hasFb) {
        launchFb();
    }
    OneSignal = w.OneSignal || [];
    $('.allowNotif').click(allowNotif);
    $('.cancelNotif').click(cancelNotif);

    if (checkForLoginModule()) {
        $(d).on('click', '.login', function (e) {
            evtEmitter.emit('initLoginFrame');
            renderLoginModal();
        });

        w.addEventListener('message', receiveMessage, false);

        $(d).on('click', '.login-dismiss', closeLogin);

        $(d).keyup(function (e) {
            if (e.keyCode == 27 && ($('.login-overlay').data('overlay') === 'true')) {
                $('.login-dismiss').click();
            }
        });

        $(d).on('click', '.formButton', function (e) {
            var action = $(this).data('action');
            $('#loginZone').find('form').eq(0).data('action', action);
            $('#loginZone').find('#submitForm').text($('#loginZone').find('form').eq(0).data('text-' + action));
            $('#loginZone').find('.top-text').text($('#loginZone').find('form').eq(0).data('title-' + action));
            $('#loginZone').find('input,.formButton,.error,.alt-login').each(function (e) {
                if ($(this).hasClass('p' + action.capitalize())) {
                    $(this).removeClass('hidden');
                }
                else {
                    $(this).addClass('hidden');
                }
            });
        });
        $(d).on('click', '.logout', logoutInit);

        $(d).on('click', '.submitQuestion', function (e) {
            var form = $(this).parents('.askQuestion'), timelineId = $(this).attr('data-timelineId'), _this = $(this);
            var api = $(this).data('api');
            if ($('.login').data('status') != 'in') {
                $('.login').click();
                return;
            }
            var flashMessage = form.find('.flashMessage'), loader = form.find('.loader').eq(0), question = form.find('.userQuestion');
            $(this).attr('disabled', true);
            if (question.val().length > parseInt(question.data('max'))) {
                flashMessage.html('You have typed more characters than allowed.');
                flashMessage.addClass('error');
                flashMessage.removeClass('hidden');
                return;
            }
            if (question.val().length < 1) {
                flashMessage.html('You cannot leave the question empty.');
                flashMessage.addClass('error');
                flashMessage.removeClass('hidden');
                return;
            }
            loader.html('<img src="https://o.cdn.newsbytesapp.com/assets/images/loader.3.gif" class="loading wd-ht-px-32" alt="loader gif">');
            var formData = {
                'question': (Object.create(utilityModule)).cleanInputField(question.val()),
                'timelineId': timelineId,
                'locale': w.nblocale
            };
            form.addClass('marked' + timelineId);
            postUserQuestion(formData);
        });
        $(d).on('keyup', '.userQuestion', function () {
            var loader = $(this).parents('.askQuestion').find('.loader').eq(0), text = $(this).val().trim(), maxCount = parseInt($(this).data('max'));
            loader.text((maxCount - text.length) + " characters left.");
            $(this).parents('.askQuestion').find('.submitQuestion').attr('disabled', false);
            loader.removeClass('hidden');
            loader.removeClass((text.length <= maxCount) ? "error" : "success");
            loader.addClass((text.length <= maxCount) ? "success" : "error");
        });
        $(d).on('focus', '.userQuestion', function () {
            $(this).attr('rows', 4);
            $(this).parents('.askQuestion').children('.controls').removeClass('hidden');
            $(this).parents('.askNewsbytes').find('.questionSlider').addClass('active');
        });
        $(d).on('blur', '.userQuestion', function () {
            if ($(this).val() == "") {
                $(this).attr('rows', 1);
                $(this).parents('.askQuestion').children('.controls').addClass('hidden');
            }
        });
        $(d).on('click', '.cancelSubmitQuestion', function () {
            $(this).parents('.askQuestion').children().find('.userQuestion').val('');
            $(this).parents('.askQuestion').children().find('.userQuestion').blur();
        });

    }

    if(typeof(partner) != 'undefined'){
        updateUrlQueries();
        removeExternalLinks($('article').first().attr('id'));

        $(d).on('click', 'header a', function (e) {
            e.preventDefault();
            e.stopPropagation();

            if(!($(e.currentTarget).hasClass('clickable-logo') || $(e.currentTarget).hasClass('menu-item'))){
                showInterstialAd(e.currentTarget.href);
            }
        });
    }

    w.blogPanel && (w.blogPanel.init(),w.blogPanel.regulate());
}
})(window, document, navigator)
