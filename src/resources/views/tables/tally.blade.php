@foreach($data as $item)
<?php
    $sectionHeaderPresent = array_get($item, 'sectionHeader', false);
?>
@include('web-components::tables.filterTable', [ 'data' => $item, 'classList' => $classList, 'type' => $type, 'sectionHeader' => $sectionHeaderPresent])
@endforeach