<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
<div class="bg-primary  pd-b-10 table-section {{isset($classList) ? $classList : ''}} {{isset($sectionHeaderPresent) ? ($sectionHeaderPresent ? '' : 'pd-t-10') : 'pd-t-10'}}" id="table-ft-{{$componentId}}">
    @if(isset($sectionHeaderPresent) && $sectionHeaderPresent)
    @include('web-components::sectionHeaders.listItemSmSqrImg', ['data' => $sectionHeader, 'classList' => 'dp-fx fx-al-ct ft-pr bd-4 ft-pr md-bd-4 br-rd-tl-tr-md-10px br-rd-0 bg-lt-grey pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 section-header', 'type' => $type])
    @endif
    <?php
        $searchPresent = false;
        $paginationPresent = false;
        if(!is_null($data)){
            foreach ($data['header'] as $key => $value) {
                if(array_key_exists('search', $value) && $value['search'])
                    $searchPresent = true;
            }
            if(array_get($data, 'pagination', []) && (sizeof($data['pagination']) > 0)){
                $paginationPresent = true;
            }
        }
        $pgTotal = '';
        $pgPerView = '';
        if($paginationPresent){
            $pgTotal = 'data-pg-total=' . $data['pagination']['total'] . '';
            $pgPerView = 'data-pg-view=' . $data['pagination']['perView'] . '';
            $pgCountPerView = (($data['pagination']['total'] - ($data['pagination']['total'] % $data['pagination']['perView'])) / $data['pagination']['perView']);
        }
    ?>
    @switch($type)
    @case('amp')
    <div class="dp-fx fx-al-ct fx-js-bw pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20">
        @if(!empty(array_get($data, 'title', '')))
        <div class="ft-pr bd-4 ft-pr md-bd-4">{!! $data['title'] !!}</div>
        @endif
        @if(sizeof(array_get($data, 'dropdown', [])) > 0)
            @include('web-components::dropdowns.statefull', ['data'=> $data['dropdown'], 'type'=> $type, 'classList' => ''])
        @endif
    </div>
    @break
    @default
    @if(!$searchPresent)
    <div class="dp-fx fx-al-ct fx-js-bw pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20">
        @if(!empty(array_get($data, 'title', '')))
            @if(isset($data['tag']) && $data['tag']=='widget')
            <div class="ft-ter md-ter-bd-2 ter-bd-3 cl-link">{!! $data['title'] !!}</div>
            @else           
             <div class="ft-pr bd-4 ft-pr md-bd-4">{!! $data['title'] !!}</div>
            @endif
        @endif
        @if(sizeof(array_get($data, 'dropdown', [])) > 0)
            @include('web-components::dropdowns.statefull', ['data'=> $data['dropdown'], 'type'=> $type, 'classList' => ''])
        @endif
    </div>
    @else
    <div class="dp-fx fx-al-ct-md-ac fx-dr-col-ac fx-dr-col-md-dac fx-js-bw pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20">
        @if(!empty(array_get($data, 'title', '')))
            @if(isset($data['tag']) && $data['tag']=='widget')
            <div class="ft-ter md-ter-bd-2 ter-bd-3 cl-link">{!! $data['title'] !!}</div>
            @else           
            <div class="ft-pr bd-4 ft-pr md-bd-4">{!! $data['title'] !!}</div>
            @endif
        @endif
        <div class="dp-fx mg-t-10 fx-js-bw fx-al-ct">
            <div class="dp-fx fx-al-ct col-6 bg-lavender cl-acadia pd-t-14 pd-b-14 pd-l-20 pd-r-10 br-rd-10">
                <span class="fz-16 nb-icon-search dp-ib mg-t-4"></span>
                <input type="text" class="search-in-table wd-full reg-3 md-reg-3 cl-ink-dk mg-l-8" />
            </div>
            <div class="mg-l-0 mg-l-md-20">
                @if(sizeof(array_get($data, 'dropdown', [])) > 0)
                    @include('web-components::dropdowns.statefull', ['data'=> $data['dropdown'], 'type'=> $type, 'classList' => ''])
                @endif
            </div>
        </div>
    </div>
    @endif
    @break
    @endswitch
    @if(!is_null($data))
    <div class="hz-scroll-ac {{$paginationPresent ? 'pagination-parent' : ''}}" id="paginate-{{$componentId}}">
        <table class="wd-full table {{$paginationPresent ? 'pagination-present' : ''}}" cellspacing="0" cellpadding="0" {{$pgTotal}} {{$pgPerView}}>
            <tbody class="wht-sp-normal {{(isset($data['tag']) && $data['tag']=='widget') ? 'ft-ter br-1p-pale-lavender ' : 'ft-pr'}} ">
                <tr class=" wht-sp-normal {{(isset($data['tag']) && $data['tag']=='widget') ? 'ft-ter md-ter-reg-2 ter-reg-3 br-b-1p-pale-lavender' : 'bd-6 md-bd-6'}}">
                    @foreach($data['header'] as $tableColHeader)
                    @switch($type)
                    @case('amp')
                    <td class="pd-l-10 pd-r-10 pd-t-20 pd-b-20" data-cell="{{$tableColHeader['key']}}">{!! $tableColHeader['name'] !!}</td>
                    @break
                    @default
                    @if(array_get($tableColHeader, 'sort', false))
                    <td class="dp-fx fx-al-ct pd-l-10 pd-r-10 pd-t-20 pd-b-20 {{array_get($tableColHeader, 'imp', false) ? 'imp' : ''}} {{array_get($tableColHeader, 'show', false) ? '' : 'show-not'}} cs-ptr sort-rows sort-by-{{$tableColHeader['key']}} {{array_get($tableColHeader, 'search', false) ? 'search-rows' : ''}}" data-cell="{{$tableColHeader['key']}}">
                        <span>{!! $tableColHeader['name'] !!}</span>
                        <span class="fz-14 nb-icon-drop-more dp-ib rotate-180"></span>
                    </td>
                    @else
                    <td class="pd-l-10 pd-r-10 pd-t-20 pd-b-20 {{array_get($tableColHeader, 'imp', false) ? 'imp' : ''}} {{array_get($tableColHeader, 'show', false) ? '' : 'show-not'}}" data-cell="{{$tableColHeader['key']}}">
                    @if(isset($tableColHeader['show']) && ($tableColHeader['show']))
                    <b>{!! $tableColHeader['name'] !!}</b>
                    @else
                    {!! $tableColHeader['name'] !!}
                    @endif
                    </td>
                    @endif
                    @break
                    @endswitch
                    @endforeach
		        </tr>
                @foreach($data['rowData'] as $rowItem)
                <tr class="reg-2 md-reg-2 sortable searchable pg-row {{$paginationPresent ? (($loop->index) < $pgCountPerView ? '' : 'row-hidden') : ''}}">
                    @foreach($rowItem as $item)
                    @switch($type)
                    @case('amp')
                    <td class="pd-l-10 pd-r-10 {{ isset($item['url']) ? 'pd-t-8 pd-b-8' : 'pd-t-20 pd-b-20'}}" data-cell="{{$data['header'][$loop->index]['key']}}">
                    @break
                    @default
                    <td class="pd-l-10 pd-r-10 {{ isset($item['url']) ? 'pd-t-8 pd-b-8' : 'pd-t-20 pd-b-20'}} {{array_get($item, 'imp', false) ? 'imp' : ''}} {{array_get($item, 'show', false) ? '' : 'show-not'}}" data-cell="{{$data['header'][$loop->index]['key']}}"> 
                    @break
                    @endswitch
                        @if(isset($item['url']))
                        <a class="dp-fx fx-al-ct" href="{!! $item['url'] !!}">
                        @switch($type)
                        @case('amp')
                        @if($item['icon'] != '')
                        <amp-img src="{{$item['icon']}}" class="mg-r-10 fx-basis-40px" alt="{{$item['content']}}" width="40" height="40" layout="fixed"></amp-img>
                        @endif
                        @break
                        @default
                        @if($item['icon'] != '')
                        <img data-src="{{$item['icon']}}" class="lazy wd-ht-px-40 mg-r-10 fx-basis-40px" alt="{{$item['content']}}" title="{{$item['content']}}">
                        @endif
                        @break
                        @endswitch
                        @endif
                        @if(isset($item['fontIcon']) && $item['fontIcon'] != null)
                        <div class="dp-fx fx-al-ct {{$item['fontIcon']['classList']}}">
                            <span class="mg-t-4 nb-icon-{{$item['fontIcon']['icon']}}"></span>
                            <span class="mg-l-6">{!! $item['content'] !!}</span>
                        </div>
                        @else
                        <span>{!! $item['content'] !!}</span>
                        @endif
                        </a>
                    </td>
                    @endforeach
                </tr>
                @endforeach
                @if((isset($data['extraData'])) && (count($data['extraData']) > 0))
                @foreach($data['extraData'] as $item)
                <tr class="bg-inherit">
                   <td colspan="{{count($data['header'])}}" class="imp">
                    @if(isset($data['tag']) && $data['tag']=='widget')
                       <div class="dp-fx fx-js-bw wd-full pd-t-20 pd-b-20 extra-data wht-sp-normal {{ $loop->first ? 'ft-ter bd-6 ft-pr md-bd-6' : 'ft-pr reg-2 ft-ter md-reg-2'}}">
                    @else
                       <div class="dp-fx fx-js-bw wd-full pd-t-20 pd-b-20 extra-data wht-sp-normal {{ $loop->first ? 'ft-pr bd-6 ft-pr md-bd-6' : 'ft-pr reg-2 ft-pr md-reg-2'}}">
                    @endif
                           @foreach($item as $subItem)
                           <span class="pd-l-6 pd-r-6">{{$subItem}}</span>
                           @endforeach
                        </div>
                    </td>
                </tr>
                @endforeach
                @endif
                @if(isset($data['link']['size']))
                <tr class="bg-inherit">
                    <td colspan="{{count($data['header'])}}" class="imp">
                        <div class="dp-fx wd-full pd-t-20 pd-b-20">
                            @include('web-components::links.link', ['link' => $data['link'], 'classList' => ''])
                        </div>
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
        @if($paginationPresent)
        <div class="dp-fx fx-js-ct">
            @include('web-components::pagination.withinComponent', ['data' => array_get($data, 'pagination', []), 'classList' => 'bg-body'])
        </div>
        @endif
    </div>
    @else
    <div class="dp-fx fx-js-ct pd-b-30">
        <div class="col-6">
            <img src="{{array_get($data, 'void.img', config('web-components.services.'.config('web-components.services.assetSource').'.url', '').'/assets/images/no-data.png')}}" alt="No Data Available Image" class="obj-fit-cover wd-full">
            <div class="bd-5 cl-ink-dk md-bd-5 mg-t-10 tx-ct">{!! array_get($data, 'void.title', 'Data not available yet.') !!}</div>
        </div>
    </div>
    @endif
</div>

@if(isset($data['tag']) && $data['tag']=='widget')
@section('css')
@parent
 <style type="text/css">
    .table tbody tr:nth-child(2n) {
        background-color: white;
    }
    .table tbody tr:nth-child(n+3):nth-child(odd) {
	    background-color:#f5f5f5;
	}
    table {
        display: flex;
        flex-direction: column;
        flex: 1;
    }
    tr {
        flex: 1;
        display: flex;
        flex-direction: row;
        align-items: center;
    }
    td{
        flex: 1;
        align-items: center;
    }
    tbody tr td:first-child {
        flex-grow: 2;
        flex-shrink: 1;
    }
  </style>
@stop
@endif
