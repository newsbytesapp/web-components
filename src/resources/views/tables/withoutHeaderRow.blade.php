<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
<div class="bg-primary  pd-b-10 table-section {{isset($classList) ? $classList : ''}} {{isset($sectionHeaderPresent) ? ($sectionHeaderPresent ? '' : 'pd-t-10') : 'pd-t-10'}}" id="table-whr-{{$componentId}}">
    @if(isset($sectionHeaderPresent) && $sectionHeaderPresent)
    @include('web-components::sectionHeaders.listItemSmSqrImg', ['data' => $sectionHeader, 'classList' => 'dp-fx fx-al-ct ft-pr bd-4 ft-pr md-bd-4 br-rd-tl-tr-md-10px br-rd-0 bg-lt-grey pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20', 'type' => $type])
    @endif
    <div class="dp-fx fx-al-ct fx-js-bw pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20">
        @if(!empty(array_get($data, 'title', '')))
        <div class="ft-pr bd-4 ft-pr md-bd-4">{!! $data['title'] !!}</div>
        @endif
        @if(sizeof(array_get($data, 'dropdown', [])) > 0)
            @include('web-components::dropdowns.statefull', ['data'=> $data['dropdown'], 'type'=> $type, 'classList' => ''])
        @endif
    </div>
    <?php
        if(isset($data['firstRowFeatured']) && $data['firstRowFeatured']){
            $highlight = 'highlighted-first-row';
        }elseif(isset($data['lastRowFeatured']) && $data['lastRowFeatured']){
            $highlight = 'highlighted-last-row';
        }else{
            $highlight = '';
        }
    ?>
    @if(!is_null($data))
    <div class="hz-scroll-ac">
        <table class="wd-full table {{$highlight}}" cellspacing="0" cellpadding="0">
            <tbody class="wht-sp-normal ft-pr">
                @foreach($data['rowData'] as $rowItem)
                <tr class="br-t-1p-white-smoke reg-2 md-reg-2">
                    @foreach($rowItem as $item)
                    @switch($type)
                    @case('amp')
                    <td class="pd-l-10 pd-r-10 {{ isset($item['url']) ? 'pd-t-8 pd-b-8' : 'pd-t-20 pd-b-20'}} imp">
                    @break
                    @default
                    <td class="pd-l-10 pd-r-10 {{ isset($item['url']) ? 'pd-t-8 pd-b-8' : 'pd-t-20 pd-b-20'}} {{$item['imp'] ? 'imp' : ''}}"> 
                    @break
                    @endswitch
                        @if(isset($item['url']))
                        <a class="dp-fx fx-al-ct wht-sp-nowrap" href="{!! $item['url'] !!}">
                            @switch($type)
                            @case('amp')
                            @if($item['icon'] != '')
                            <amp-img src="{{$item['icon']}}" class="mg-r-10" alt="{{$item['content']}}" width="40" height="40" layout="fixed"></amp-img>
                            @endif
                            @break
                            @default
                            @if($item['icon'] != '')
                            <img data-src="{{$item['icon']}}" class="lazy wd-ht-px-40 mg-r-10" alt="{{$item['content']}}" title="{{$item['content']}}">
                            @endif
                            @break
                            @endswitch
                        @endif
                            @if(isset($item['fontIcon']) && $item['fontIcon'] != null)
                            <div class="dp-fx fx-al-ct {{$item['fontIcon']['classList']}}">
                                <span class="mg-t-4 nb-icon-{{$item['fontIcon']['icon']}}"></span>
                                <span class="mg-l-6 {{$loop->first ? 'tx-ct' : ''}}">{!! $item['content'] !!}</span>
                            </div>
                            @else
                            <span class="{{$loop->first ? 'tx-ct' : ''}}">{!! $item['content'] !!}</span>
                            @endif
                        </a>
                    </td>
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @else
    <div class="dp-fx fx-js-ct pd-b-30">
        <div class="col-6">
            <img src="{{array_get($data, 'void.img', config('web-components.services.'.config('web-components.services.assetSource').'.url', '').'/assets/images/no-data.png')}}" alt="No Data Available Image" class="obj-fit-cover wd-full">
            <div class="bd-5 cl-ink-dk md-bd-5 mg-t-10 tx-ct">{!! array_get($data, 'void.title', 'Data not available yet.') !!}</div>
        </div>
    </div>
    @endif
</div>