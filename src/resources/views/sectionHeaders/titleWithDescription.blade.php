<?php
    $tag = array_get($data, 'tag', 'default');
?>
<section class="bg-primary pd-t-20 pd-b-20 pd-t-md-30 pd-b-md-30 pd-l-md-20 pd-r-md-20  pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac ft-pr {{isset($classList) ? $classList : ''}}">
    <h1 class="cl-ink-dk cl-@nbColor($tag)-dk ft-ter md-ter-bd-6 ter-bd-6">{!! $data['title'] !!}</h1>
    @if(! empty($data['description']))
    <p class="cl-lt mg-t-6 ft-sec md-sec-reg-2 sec-reg-2">{!! $data['description'] !!}</p>
    @endif
</section>