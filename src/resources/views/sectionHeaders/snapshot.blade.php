<div class="dp-fx fx-dr-col-rev-ac fx-dr-row-md-ac fx-al-ct fx-js-bw {{isset($classList) ? $classList : ''}}">
    <div class="pd-r-0 pd-r-md-20">
        <h1 class="cl-ink-dk ft-ter md-ter-bd-7 ter-bd-7 mg-t-20 mg-t-md-0 tx-ct tx-md-lt">{!! $data['title'] !!}</h1>
        <p class="cl-ink-dk ft-ter md-ter-reg-3 ter-reg-3 tx-ct tx-md-lt pd-t-10">{!! $data['subtitle'] !!}</p>
        <div class="cl-lt ft-sec md-sec-reg-3 sec-reg-3 tx-lt mg-t-10">{!! $data['description'] !!}</div>
    </div>
    @switch($type)
    @case('amp')
    <div class="pd-r-md-10">
        <amp-img src="{{$data['thumb']}}" alt="{!! $data['title'] !!}" width="140" height="140" layout="fixed"></amp-img>
    </div>
    @break
    @default
    <div class="wd-px-180 fx-basis-180px dp-fx fx-dr-col">
        <img class="wd-full ht-full lazy" data-src="{{$data['thumb']}}" alt="{!! $data['title'] !!}" title="{!! $data['title'] !!}">
    </div>
    @break
    @endswitch
</div>
@if(isset($data['profiles']) && (count($data['profiles']) > 0))
    <div class="dp-fx fx-js-ct fx-js-end-md-dac">
        @include('web-components::lists.socialProfiles', ['data' =>  $data['profiles'], 'classList' => 'dp-fx wd-px-180 fx-js-ct-md-dac fx-js-ct'])
    </div>
@endif