<a class="{{$classList}}" href="{!! $data['url'] !!}">
    @switch($type)
    @case('amp')
    <amp-img src="{{$data['icon']}}" class="mg-r-10" alt="{{$data['title']}}" width="40" height="40" layout="fixed"></amp-img>
    @break
    @default
    <img data-src="{{$data['icon']}}" class="lazy wd-ht-px-40 mg-r-10" alt="{{$data['title']}}" title="{{$data['title']}}">
    @break
    @endswitch
    <span>{!! $data['title'] !!}</span>
</a>