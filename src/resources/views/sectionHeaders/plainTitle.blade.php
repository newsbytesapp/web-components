<section class="bg-primary pd-t-20 pd-b-20 pd-t-md-30 pd-b-md-30 pd-l-md-20 pd-r-md-20  pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac {{isset($classList) ? $classList : ''}}">
    <h1 class="cl-ink-dk ft-ter md-ter-bd-6 ter-bd-6">{!! $data['title'] !!}</h1>
</section>