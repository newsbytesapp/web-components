<section class="bg-primary pd-t-20 pd-b-20 pd-t-md-30 pd-b-md-30 pd-l-md-20 pd-r-md-20  pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac {{isset($classList) ? $classList : ''}}">
    <p class="cl-ink-dk ft-ter md-ter-reg-3 ter-reg-3">{!! $data['subtitle'] !!}</p>
    <h1 class="cl-ink-dk ft-ter md-ter-bd-5 ter-bd-5 mg-t-2">{!! $data['title'] !!}</h1>
</section>