<?php
    $tag = array_get($data, 'tag', 'default');
?>
<section class="bg-primary ft-pr br-1p-pale-lavender mg-t-20 {{isset($classList) ? $classList : ''}}">    
    <div class="dp-fx fx-al-bs fx-js-ct wd-full ht-px-100 gd-tint-@nbColor($tag)-lt-@nbColor($tag)0-@nbColor($tag)100">
        <img src="{{ $data['image'] }}" alt="{{ $data['title'] }}" class="wd-ht-px-100 br-rd-pr-50 mg-t-50 br-1p-pale-lavender">
    </div>
    <div class="dp-fx fx-al-ct fx-dr-col fx-al-ct-ac mg-t-50 pd-t-10 pd-b-20">
        <h1 class="ft-ter md-ter-bd-7 ter-bd-6 pd-b-4">{!! $data['title'] !!}</h1>
        <h1 class="ft-ter md-ter-reg-2 ter-reg-2">{!! $data['subtitle'] !!}</h1>
    </div>
    @if(isset($data['list'])) 
    <div class="fx-row br-t-1p-pale-lavender pd-t-20 pd-b-20 {{isset($classList) ? $classList : ''}}">
        @foreach($data['list'] as $key => $item)
        <div class="col-6 col-md-3 pd-t-md-0 pd-t-14 fx-grow-md-1">
            <div class="dp-fx fx-dr-col fx-al-ct {{($key==0 )? " " : "br-l-1p-pale-lavender"}}">
                <span class="cl-lt ft-ter md-sec-reg-2 sec-reg-2 tx-ct  mg-b-4">{!!$item['title']!!}</span>
                <span class="ft-ter md-sec-bd-2 sec-bd-2 tx-ct ">{!!$item['data']!!}</span>
            </div>
        </div>
        @endforeach
    </div>
    @endif
</section>
