<?php
    $tag = array_get($data, 'tag', 'default');
?>
<section class="bg-primary bg-@nbColor($tag)-lt pd-t-20 pd-b-20 pd-t-md-30 pd-b-md-30 pd-l-md-20 pd-r-md-20  pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac {{isset($classList) ? $classList : ''}}">
    <h1 class="cl-lt cl-@nbColor($tag)-dk ft-pr md-bd-2 bd-2">{!! $data['title'] !!}</h1>
</section>