
<section class="bg-primary pd-t-20 pd-b-20 pd-t-md-30 pd-b-md-30 pd-l-md-20 pd-r-md-20  pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac {{isset($classList) ? $classList : ''}}">
    <h1 class="cl-ink-dk ft-pr md-bd-2 bd-2">{!! $data['title'] !!}</h1>
    <div class="dp-fx fx-js-ct-ac fx-js-ct-md-dac">
        @include('web-components::share.social-share', [ 'data' => $data['socialShare'], 'classList' => 'mg-t-20 mg-t-md-30', 'type' => ''])
    </div>
</section>