<?php
    $matomoId = array_get($data, 'id', config('web-components.services.analytics.matomo.id'));
?>
@if(($matomoId) && (config('web-components.services.analytics.matomo.active') == 'on'))
    {{-- <script type="text/javascript"> --}}
        var _paq = _paq || [];
        _paq.push(["setCookieDomain", "*.newsbytesapp.com"]);
        _paq.push(["setDomains", ["*.newsbytesapp.com"]]);
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
        var u="https://a.newsbytesapp.com/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', '{{$matomoId}}']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
        })();
    {{-- </script>
    <noscript><p><img src="https://a.newsbytesapp.com/piwik.php?idsite=1&amp;rec=1" style="border:0;" alt="" /></p></noscript> --}}
@endif