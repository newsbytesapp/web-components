<?php
    $alexaId = array_get($data, 'id', config('web-components.services.analytics.alexa'));
?>
@if($alexaId)
@section('pageScript')
@parent
    {{-- <script type="text/javascript"> --}}
    _atrk_opts = { atrk_acct:"{{$alexaId}}", domain:"newsbytesapp.com",dynamic: true};
    (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.defer = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
    {{-- </script>
    <noscript><img src="https://certify.alexametrics.com/atrk.gif?account={{$alexaId}}" style="display:none" height="1" width="1" alt="" /></noscript> --}}
@stop
@endif