<?php
    $comscoreId = array_get($data, 'id', config('web-components.services.analytics.comscore'));
?>
@if($comscoreId)
@section('pageScript')
@parent
{{-- <script> --}}
    var _comscore = _comscore || [];
    _comscore.push({ c1: "2", c2: "{{$comscoreId}}" });
    {{-- (function() {
        var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.defer = true;
        s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
        el.parentNode.insertBefore(s, el);
    })(); --}}
{{-- </script> --}}
{{-- <noscript><img src="https://sb.scorecardresearch.com/p?c1=2&c2={{$comscoreId}}&cv=2.0&cj=1"/></noscript> --}}
@stop
@endif