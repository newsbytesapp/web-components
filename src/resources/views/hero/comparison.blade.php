<div class="bg-lt-grey  pd-t-30 pd-b-30 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 hero-comparison ft-pr reg-3 md-reg-3 {{isset($classList) ? $classList : ''}}">
    <div class="tx-ct mg-t-4">{!! $data['comparisionTitle'] !!}</div>
    @if(isset($data['comparisionSubtitle']) && ($data['comparisionSubtitle'] != ''))
    <div class="tx-ct mg-t-6 cl-lt">{!! $data['comparisionSubtitle'] !!}</div>
    @endif
    <div class="mg-t-10 dp-fx fx-js-ct">
        <div class="dp-fx fx-dr-col fx-al-ct">
            <a href="{!! $data['left']['url'] !!}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!} class="">
                @switch($type)
                @case('amp')
                <amp-img src="{{$data['left']['icon']}}" alt="{{$data['left']['title']}}" width="50" height="50" layout="fixed"></amp-img>
                @break
                @default
                <img class="lazy wd-ht-px-50 mg-l-10" data-src="{{$data['left']['icon']}}" alt="{{$data['left']['title']}}" title="{{$data['left']['title']}}"/>
                @break
                @endswitch
            </a>
            <div class="bd-5 md-bd-5">{!! $data['left']['title'] !!}</div>
            <div>{!! $data['left']['subtitle'] !!}</div>
            <div class="mg-t-6">
                @foreach($data['left']['comparision'] as $comparisionParam)
                    <div class="bd-5 md-bd-5 tx-ct">{!! $comparisionParam['title'] !!}</div>
                    <div class="cl-lt mg-t-2 tx-ct">{!! $comparisionParam['subtitle'] !!}</div>
                @endforeach
            </div>    
        </div>
        <span class="pd-l-26 pd-r-26 bd-6 md-bd-6 mg-t-16">@t('vs')</span>
        <div class="dp-fx fx-dr-col fx-al-ct">
            <a href="{!! $data['right']['url'] !!}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!} class="">
                @switch($type)
                @case('amp')
                <amp-img src="{{$data['right']['icon']}}" alt="{{$data['right']['title']}}" width="50" height="50" layout="fixed"></amp-img>
                @break
                @default
                <img class="lazy wd-ht-px-50 mg-l-10" data-src="{{$data['right']['icon']}}" alt="{{$data['right']['title']}}" title="{{$data['right']['title']}}"/>
                @break
                @endswitch
            </a>
            <div class="bd-5 md-bd-5">{!! $data['right']['title'] !!}</div>
            <div>{!! $data['right']['subtitle'] !!}</div>
            <div class="mg-t-6">
                @foreach($data['right']['comparision'] as $comparisionParam)
                    <div class="bd-5 md-bd-5 tx-ct">{!! $comparisionParam['title'] !!}</div>
                    <div class="cl-lt mg-t-2 tx-ct">{!! $comparisionParam['subtitle'] !!}</div>
                @endforeach
            </div>    
        </div>
    </div>
    @if(isset($data['conclusion']) && ($data['conclusion'] != ''))
    <div class="reg-2 md-reg-2 tx-ct mg-t-20">{!! $data['conclusion'] !!}</div>
    @endif
    <div class="mg-t-10 fx-row fx-js-ct neg-mg-lr-px-10-ac">
        @foreach($data['links'] as $linkItem)
        <div class="pd-l-10 pd-r-10">
            @include('web-components::links.link', [
                'link' => $linkItem
            ])
        </div>
        @endforeach
    </div>
    <div class="mg-t-30 dp-fx fx-js-ct">
        @include('web-components::share.social-share', ['data' => $data['socialShare'], 'type' => $type, 'classList' => ''])
    </div>
</div>