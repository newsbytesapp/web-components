<div class="bg-lt-grey br-rd-0 br-rd-bl-br-md-10px pd-t-30 pd-b-30 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 hero-iconsLeftRightImgCenterDropdown ft-pr {{isset($classList) ? $classList : ''}}">
    <div class="tx-ct md-bd-2 bd-2">{!! $data['hero']['title'] !!}</div>
    <div class="fx-row">
        <div class="col-6 col-lg-4">
            @foreach($data['hero']['left']['list']['data'] as $item)
            <a href="{!! $item['url'] !!}" class="dp-fx list-item-sqrImg mg-t-30 fx-al-ct mg-l-lg-20">
                <div class="wd-ht-px-40">
                    @switch($type)
                    @case('amp')
                    <amp-img src="{{$item['img']}}" alt="{{$item['title']}}" width="40" height="40" layout="fixed"></amp-img>
                    @break
                    @default
                    <img data-src="{{$item['img']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-full ht-full"/>
                    @break
                    @endswitch
                </div>
                <div class="tx-container pd-l-10">
                    <div class="md-bd-6 bd-6">{!! $item['title'] !!}</div>
                    @if(isset($item['auxiliaryText']))
                    <p class="md-reg-3 reg-3 cl-lt">{!! $item['auxiliaryText'] !!}</p>
                    @endif
                </div>
            </a>
            @endforeach
        </div>
        @switch($type)
        @case('amp')
        @break
        @default
        <div class="col-6 col-lg-4 hidden-in-touch">
            <img data-src="{{$data['hero']['img']}}" alt="{{$data['hero']['title']}}" title="{{$data['hero']['title']}}" class="lazy wd-full"/>
        </div>
        @break
        @endswitch
        <div class="col-6 col-lg-4 ">
            @foreach($data['hero']['right']['list']['data'] as $item)
            <a href="{!! $item['url'] !!}" class="dp-fx list-item-sqrImg mg-t-30 fx-al-ct mg-l-lg-20">
                <div class="wd-ht-px-40">
                    @switch($type)
                    @case('amp')
                    <amp-img src="{{$item['img']}}" alt="{{$item['title']}}" width="40" height="40" layout="fixed"></amp-img>
                    @break
                    @default
                    <img data-src="{{$item['img']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-full ht-full"/>
                    @break
                    @endswitch
                </div>
                <div class="tx-container pd-l-10">
                    <div class="md-bd-6 bd-6">{!! $item['title'] !!}</div>
                    @if(isset($item['auxiliaryText']))
                    <p class="md-reg-3 reg-3 cl-lt">{!! $item['auxiliaryText'] !!}</p>
                    @endif
                </div>
            </a>
            @endforeach
        </div>
    </div>
    @if(count($data['dropdown']['content']['data']) > 0)
    <div class="mg-t-30 dp-fx fx-js-ct">
        <div class="col-12 col-lg-4">
            @include('web-components::dropdowns.stateless',
            ['data'=> $data['dropdown'], 'type'=> $type, 'classList' => ''])
        </div>
    </div>
    @endif
    <div class="mg-t-20 dp-fx fx-js-ct">
        @include('web-components::share.social-share', ['data' => $data['socialShare'], 'type' => $type, 'classList' => ''])
    </div>
</div>
