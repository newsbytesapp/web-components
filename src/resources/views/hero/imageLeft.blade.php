<div class="bg-lt-grey  pd-t-30 pd-b-30 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 hero-imageLeft ft-pr {{isset($classList) ? $classList : ''}}">
    <div class="fx-row fx-al-st tx-ct tx-md-lt">
        <div class="col-12 col-md-3 pd-r-md-10 dp-fx fx-js-ct">
            <div class="col-4 col-md-12 pos-rel">
                @switch($type)
                @case('amp')
                <amp-img src="{{$data['thumb']}}" alt="{{$data['title']}}" width="1080" height="1080" layout="responsive" class="mg-lr-auto"></amp-img>
                @break
                @default
                <img class="lazy mx-wd-full mg-lr-auto dp-bl" data-src="{{$data['thumb']}}" alt="{{$data['title']}}" title="{{$data['title']}}"/>
                @break
                @endswitch
                <div class="{{ $tint ? 'ht-pr-30 pos-abs bt-init lt-init wd-full gd-tint-lavender-pl-lavender0-lavender100' : 'hidden'}}"></div>
            </div>
        </div>
        <div class="col-12 col-md-9 mg-t-20 mg-t-md-0 pd-l-md-10">
            <div class="tx-ct tx-md-lt md-bd-2 bd-2">{!! $data['title'] !!}</div>
            @if(isset($data['subtitle']))
            <div class="tx-ct tx-md-lt md-reg-2 reg-2">{!! $data['subtitle'] !!}</div>
            @endif
            @if(array_get($data, 'longText') && (!empty($data['longText'])))
            <p class="mg-t-10 md-reg-2 reg-2">{!! $data['longText'] !!}</p>
            @endif
            @if(isset($data['links']) && (count($data['links']) > 0))
            <div class="mg-t-20 tx-ct tx-md-lt">
                @foreach($data['links'] as $linkItem)
                @include('web-components::links.link', ['link' => $linkItem, 'classList' => ''])
                @endforeach
            </div>
            @endif
            @if(isset($data['socialProfiles']) && (count($data['socialProfiles']) > 0))
            <div class="mg-t-10 dp-fx fx-js-ct-ac fx-js-ct-md-dac">
                @include('web-components::lists.socialProfiles', ['data' =>  $data['socialProfiles'], 'classList' => 'neg-mg-lr-px-15-ac dp-fx'])
            </div>
            @endif
            @if(isset($data['primaryInfo']) && (count($data['primaryInfo']) > 0))
            <div class="mg-t-20 dp-fx fx-wrap tx-ct tx-md-lt neg-mg-lr-px-10-ac fx-js-ct-ac fx-js-ct-md-dac">
                @foreach ($data['primaryInfo'] as $item)
                <div class="pd-l-10 pd-r-10">
                    <p class="md-bd-2 bd-2 tx-ct tx-md-lt">{!! $item['title'] !!}</p>
                    <p class="md-reg-3 reg-3 tx-ct tx-md-lt">{!! $item['subtitle'] !!}</p>
                </div>
                @endforeach
            </div>
            @endif
            @if(isset($data['secondaryInfo']) && (count($data['secondaryInfo']) > 0))
            <div class="dp-fx fx-wrap neg-mg-lr-px-10-ac fx-js-ct-ac fx-js-ct-md-dac">
                @foreach ($data['secondaryInfo'] as $item)
                <div class="pd-l-10 pd-r-10 mg-t-20">
                    <p class="md-bd-5 bd-5 tx-ct tx-md-lt">{!! $item['title'] !!}</p>
                    <p class="md-reg-3 reg-3 tx-ct tx-md-lt">{!! $item['subtitle'] !!}</p>
                </div>
                @endforeach
            </div>
            @endif
            @if(array_get($data, 'auxiliaryImage') && (!empty($data['auxiliaryImage'])))
            <div class="mg-t-30 dp-fx fx-dr-col-ac fx-dr-col-md-dac fx-al-ct-ac fx-al-ct-md-dac fx-js-bw">
                @include('web-components::share.social-share', ['data' => $data['socialShare'], 'type' => $type, 'classList' => ''])
                <div class="mg-t-10 mg-t-md-0 dp-fx fx-al-end">
                    @switch($type)
                    @case('amp')
                    <amp-img src="{{$data['auxiliaryImage']}}" height="18" layout="fixed-height" class="min-wd-px-120"></amp-img>
                    @break
                    @default
                    <img src="{{$data['auxiliaryImage']}}" height="18" class="min-wd-px-120"/>
                    @break
                    @endswitch
                </div>
            </div>
            @else
            <div class="mg-t-30 dp-fx fx-js-ct-ac fx-js-ct-md-dac">
                @include('web-components::share.social-share', ['data' => $data['socialShare'], 'type' => $type, 'classList' => ''])
            </div>
            @endif
        </div>
    </div>
</div>
