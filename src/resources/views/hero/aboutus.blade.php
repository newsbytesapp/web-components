<div class="bg-lt-grey  pd-t-30 {{isset($classList) ? $classList : ''}}">
    <h1 class="tx-ct ft-ter md-ter-bd-6 ter-bd-6">{!! $data['title'] !!}</h1>
    <div class="mg-t-20 hero-img"></div>
    <style type="text/css">
        .hero-img {
            background-image: url({{$data['ximg']}});
            background-repeat: no-repeat;
            image-rendering: -webkit-optimize-contrast;
            image-rendering: -moz-optimize-contrast;
            image-rendering: -o-optimize-contrast;
            height: 220px;
            background-position: 50%;
            -o-object-fit: cover;
            object-fit: cover;
            background-size: cover;
            background-repeat: no-repeat
        }
    </style>
</div>