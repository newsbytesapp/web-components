@if(array_get($data, 'status', false))
    <script type="text/javascript">
        window.addEventListener("jsuseraction", function(event) {
            var sl = new scriptloader(window, document);

            sl.seq([
                "{{\Vite::asset('vendor/newsbytesapp/web-components/src/resources/assets/js/widgets/ticker.js')}}"
                ]);
        });
    </script>
@endif