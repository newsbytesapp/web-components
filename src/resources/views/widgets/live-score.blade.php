@section('css')
@parent
<style type="text/css">
	.wd-px-25{
		width: 25px;
	}
	.fx-basis-25px{
		flex: 0 0 25px;
	}
	.wd-px-28{
		width: 28px;
	}
	.wd-ht-px-28{
		width: 28px;
		height: 28px;
	}
	.nested-tabs.selected{
		color: #000000;
	}
	.nested-tabs.selected.level-1{
		border-bottom: 1px solid #000000;
	}
	.br-t-1p-pale-lavender{
		border-top: 1px solid #edecee;
	}
	.br-b-1p-pale-lavender{
		border-bottom: 1px solid #edecee;
	}
	.br-rd-30{
		border-radius: 30px;
	}
	.bg-pale-lavender{
		background-color: #edecee;
	}
	.img-shdw{
		box-shadow: 0 23px 30px 0 rgba(0,0,0,0.15);
	}
	.live-widget .live-blog .header{
		background: linear-gradient(135deg, #361EE9 0%, #100DB0 100%);
	}
	.live-widget .live-blog .bar{
		width: 40px;
		height: 4px;
		opacity: 0.5;
		border-radius: 2.5px;
	}
	.live-widget .live-blog .container{
		height: 600px;
		overflow-y: auto;
		background-color: #f7f7f7;
	}
	.live-widget .live-blog .container-tint{
		height: 30px;
		background: linear-gradient(180deg, rgba(247,247,247,0.00) 0%, rgba(247,247,247,0.90) 48%, #F7F7F7 100%);
	}
	.live-widget .live-blog .card .description a{
		font-weight: 700;
		text-decoration: underline;
		-webkit-text-decoration-color: inherit;
		text-decoration-color: inherit;
	}

	.live-widget .live-cricket-test .tab-content .table-row:first-child{
		padding-top: 8px;
	}
	.live-widget .live-cricket-test .tab-content .table-row:first-child .table-cell{
		font-weight: 700;
	}
	.live-widget .live-cricket-test .tab-content .table-row:first-child{
		padding-top: 8px;
	}
	.live-widget .live-cricket-test .table-cell{
		width: 16.66%;
	}
	.live-widget .live-cricket-test .table-cell:first-child{
		padding-left: 20px;
		width: 33.32%;
	}
	.live-widget .live-cricket-test .table-cell:nth-child(3){
		width: 33.32%;
	}
	.live-widget .live-cricket-test .tab-content .table-rows:last-child .table-cell{
		width: 16.66%;
	}
	.live-widget .live-cricket-test .tab-content .table-rows:last-child .table-cell:first-child{
		width: 49.98%;
	}
	.live-widget .live-cricket-test .table-cell:last-child{
		padding-right: 20px;
	}

	@media screen and (max-width: 1024.9px) {
		.live-widget .live-blog{
/*			position: fixed;*/
			bottom: 0;
			left: 0;
			right: 0;
			max-width: 420px;
			margin: auto;
			cursor: pointer;
			z-index: 3;
		}
		.live-widget .live-blog .container{
/*			height: 0px;*/
			/* height: 350px; */
		}
	}
	@media screen and (min-width: 1025px) {
		.pointer-events-none-lg{
			pointer-events: none;
		}
	}
</style>
@stop