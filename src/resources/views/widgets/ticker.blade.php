@if(array_get($data, 'status', false))
    @switch(array_get($data, 'invitee', 'default'))
        @case('touch')
            <div class="ticker-module v1 bg-primary hidden hidden-lg" id="ticker-touch" data-url="{{array_get($data, 'route', '')}}">
                <div class="pd-t-16 pd-b-16 ticker-chart v1 hidden">
                    <a class="ticker-chart-link ht-px-50 dp-fx fx-al-ct fx-js-bw mx-wd-px-400 wrapper" href="#">
                        <p class="ticker-more hidden cl-brand bd-7">More</p>
                    </a>
                </div>
                <div class="ticker-news pd-t-10 pd-b-10 hidden bg-body">
                    <div class="live-data-ticker mx-wd-px-400 wrapper dp-fx fx-al-ct">
                        <div class="dp-fx fx-al-ct">
                            <p class="live-status cl-brand bd-7">Live</p>
                        </div>
                        <div class="mg-l-10 cl-ink-dk">
                            <a href="#" class="ticker-news-link">
                                <u class="bd-6 ticker-text"></u>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @break
        @default
            <div class="ticker-module v1 bg-primary hidden right-bottom" id="ticker-ntouch" data-url="{{array_get($data, 'route', '')}}">
                <div class="ticker-chart v1">
                    {{-- <a class="ticker-chart-link ht-px-50 dp-fx fx-al-ct fx-js-bw wd-full" href="#">
                        <div class="hidden ticker-template v1 cl-ink-dk ft-pr">
                            <div class="template-item dp-fx fx-al-ct">
                                <img src="https://o.cdn.newsbytesapp.com/assets/people/na.2.jpg" class="wd-ht-px-20"/>
                                <div class="mg-l-10">
                                    <p class="bd-6"><span class="left"></span><span class="right"></span></p>
                                    <p class="label mg-t-2 bd-7"></p>
                                </div>
                            </div>
                        </div>
                        <p class="ticker-more hidden cl-brand bd-7">More</p>
                    </a> --}}
                    <div class="pd-t-20 pd-l-20 pd-r-20 pd-b-20 pd-b-20 ticker-slideshow-parent">
                        <div class="ticker-slide-show cs-ptr">
                            <div class="ticker-slide mg-l-20 template">
                                <div class="title cl-suva-grey ft-ter md-ter-bd-4 ter-bd-4"></div>
                                <div class="items-list dp-fx fx-js-bw mg-t-10">
                                    <div class="item template">
                                        <div class="cl-ink-dk ft-ter md-ter-bd-2 ter-bd-2 dp-in-fx">
                                            <span class="left"></span>
                                            <span class="right"></span>
                                        </div>
                                        <div class="mg-t-6 dp-fx fx-al-ct">
                                            <img src="" class="wd-ht-px-20 br-rd-pr-50">
                                            <div class="mg-l-6 cl-ink-dk label ft-ter md-ter-bd-2 ter-bd-2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
                    </div>
                    <div class="pd-t-20 pd-b-20 pd-l-20 pd-r-20 expandible-chart hidden animation-dur-scale-4 ent-slide-up">
                        <div class="dp-fx fx-js-bw fx-al-ct">
                            <div class="cl-ink-dk charts-title ft-ter md-ter-bd-4 ter-bd-4"></div>
                            <div class="wd-ht-px-40 br-rd-pr-50 dp-fx fx-js-ct fx-al-ct bg-white-smoke cancel-expandible-chart cs-ptr">
                                <span class="nb-icon-cancel fz-22 mg-t-6"></span>
                            </div>
                        </div>
                        <div class="listicle-holder mg-t-10">
                            <div class="listicle mg-t-20 template">
                                <div class="title cl-suva-grey ft-ter md-ter-bd-4 ter-bd-4"></div>
                                <div class="items-list dp-fx fx-js-bw mg-t-10">
                                    <div class="item template">
                                        <div class="cl-ink-dk ft-ter md-ter-bd-2 ter-bd-2">
                                            <span class="left"></span>
                                            <span class="right"></span>
                                        </div>
                                        <div class="mg-t-6 dp-fx fx-al-ct">
                                            <img src="" class="wd-ht-px-20 br-rd-pr-50">
                                            <div class="mg-l-6 cl-ink-dk label ft-ter md-ter-bd-2 ter-bd-2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ticker-news pd-t-10 pd-b-10 pd-l-20 pd-r-20 bg-body hidden">   
                    <div class="live-data-ticker dp-fx fx-al-ct wd-full">
                        <div class="dp-fx fx-al-ct">
                            <p class="live-status cl-brand bd-7">Live</p>
                        </div>
                        <div class="mg-l-10 cl-ink-dk">
                            <a href="#" class="ticker-news-link">
                                <u class="bd-6 ticker-text"></u>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ticker-module v2 hidden" data-url="{{array_get($data, 'route', '')}}">
                <div class="mg-t-20 ticker-chart v2 hidden pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">
                    <div class="bd-1 md-bd-1 cl-ink-dk tx-ct title"></div>
                    <div class="mg-t-30 mg-t-md-20 dp-fx fx-js-ct">
                        <a class="ticker-chart-link dp-fx fx-al-ct fx-js-bw col-12 col-md-8 col-lg-6 neg-mg-lr-px-10-ac" href="#">
                            <div class="hidden ticker-template v2 cl-ink-dk ft-pr neg-mg-lr-px-10-ac">
                                <div class="template-item dp-fx fx-dr-col fx-al-ct pd-l-10 pd-r-10">
                                    <img src="https://o.cdn.newsbytesapp.com/assets/people/na.2.jpg" class="wd-ht-px-20"/>
                                    <div class="label mg-t-10 bd-5 md-bd-5 tx-ct cl-sunset-orange"></div>
                                    <div class="left bd-1 md-bd-1 tx-ct"></div>
                                    <div class="right bd-5 md-bd-5 tx-ct cl-sunset-orange"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="pos-fix ticker-floating-bttn tk-floating-bttn bg-primary pd-r-10 hidden hidden-md">
                <a class="dp-fx fx-al-ct link" href="#" target="_blank">
                    <div class="dp-fx fx-js-ct fx-al-ct pd-l-14 pd-r-14 pd-t-12 pd-b-10 br-rd-pr-50 mg-r-12 icon-container">
                        <span class="fz-20 cl-primary nb-icon-whatsapp"></span>
                    </div>
                    <div>
                        <div class="ft-ter md-ter-bd-2 ter-bd-2 title"></div>
                    </div>
                </a>
            </div>
        @break
    @endswitch
    @section('css')
    @parent
    <style type="text/css" data-styling="ticker">
        .tk-animate{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-fill-mode:both;animation-fill-mode:both}@-webkit-keyframes tk-flipInX{from{-webkit-transform:perspective(400px) rotate3d(1,0,0,90deg);transform:perspective(400px) rotate3d(1,0,0,90deg);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in;opacity:0}40%{-webkit-transform:perspective(400px) rotate3d(1,0,0,-20deg);transform:perspective(400px) rotate3d(1,0,0,-20deg);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}60%{-webkit-transform:perspective(400px) rotate3d(1,0,0,10deg);transform:perspective(400px) rotate3d(1,0,0,10deg);opacity:1}80%{-webkit-transform:perspective(400px) rotate3d(1,0,0,-5deg);transform:perspective(400px) rotate3d(1,0,0,-5deg)}to{-webkit-transform:perspective(400px);transform:perspective(400px)}}@keyframes tk-flipInX{from{-webkit-transform:perspective(400px) rotate3d(1,0,0,90deg);transform:perspective(400px) rotate3d(1,0,0,90deg);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in;opacity:0}40%{-webkit-transform:perspective(400px) rotate3d(1,0,0,-20deg);transform:perspective(400px) rotate3d(1,0,0,-20deg);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}60%{-webkit-transform:perspective(400px) rotate3d(1,0,0,10deg);transform:perspective(400px) rotate3d(1,0,0,10deg);opacity:1}80%{-webkit-transform:perspective(400px) rotate3d(1,0,0,-5deg);transform:perspective(400px) rotate3d(1,0,0,-5deg)}to{-webkit-transform:perspective(400px);transform:perspective(400px)}}.tk-flipInX{-webkit-backface-visibility:visible!important;backface-visibility:visible!important;-webkit-animation-name:tk-flipInX;animation-name:tk-flipInX}@-webkit-keyframes tk-flipOutX{from{-webkit-transform:perspective(400px);transform:perspective(400px)}30%{-webkit-transform:perspective(400px) rotate3d(1,0,0,-20deg);transform:perspective(400px) rotate3d(1,0,0,-20deg);opacity:1}to{-webkit-transform:perspective(400px) rotate3d(1,0,0,90deg);transform:perspective(400px) rotate3d(1,0,0,90deg);opacity:0}}@keyframes tk-flipOutX{from{-webkit-transform:perspective(400px);transform:perspective(400px)}30%{-webkit-transform:perspective(400px) rotate3d(1,0,0,-20deg);transform:perspective(400px) rotate3d(1,0,0,-20deg);opacity:1}to{-webkit-transform:perspective(400px) rotate3d(1,0,0,90deg);transform:perspective(400px) rotate3d(1,0,0,90deg);opacity:0}}.tk-flipOutX{-webkit-animation-duration:.75s;animation-duration:.75s;-webkit-animation-name:tk-flipOutX;animation-name:tk-flipOutX;-webkit-backface-visibility:visible!important;backface-visibility:visible!important}.mx-wd-px-400{max-width:400px}.wd-ht-px-20{width:20px;height:20px}.live-status{padding:6px 10px;border-radius:50%;border:1px solid #f30017;border-radius:10px}.ticker-module.right-bottom{position:fixed;right:0;bottom:0;min-height:40px;width:410px;z-index:6;box-shadow:0 10px 24px 0 rgba(0,0,0,0.1);border-top-left-radius:10px;border-top-right-radius:10px}.bd-7{font-size:12px;line-height:16px;font-weight:700;letter-spacing:0}.pd-b-24{padding-bottom:24px}.ht-px-50{height:50px}.live-data-ticker{min-height:50px}.tk-floating-bttn{min-width:190px;max-width:300px;box-shadow:0 2px 22px 0 rgba(62,64,63,0.2);border-radius:25px;left:50%;right:50%;bottom:50px;transform:translateX(-50%)}.tk-floating-bttn .icon-container{margin-left:-4px;background:linear-gradient(180deg,#00D48F 0%,#0AC663 100%)}
        .pd-l-14{padding-left:14px;} .pd-r-14{padding-right: 14px;} .pd-t-12{padding-top: 12px;}.mg-r-8{margin-right: 8px;}.ticker-slide:first-child{margin-left: 0;}.visibility-hidden{visibility: hidden;}.ticker-slide{min-width: 81vw;}@media screen and (min-width:1025px){.ticker-slide{max-width:280px; min-width: 262px; width: 25.6vw;}}@media screen and (max-width:320px){.ticker-slide{min-width:79.6vw;}}
    </style>
    @stop
@endif