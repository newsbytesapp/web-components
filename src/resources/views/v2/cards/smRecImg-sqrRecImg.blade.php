@if(sizeof(array_get($sum, 'data.list.data', [])) > 0)
    <?php $data = $sum['data']; ?>
    <div class="bg-primary pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac cd-smRecImgs-sqrRecImgs {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
        <div class="ft-pr bd-3 md-bd-3 cl-lt c-tt">{!! $data['title'] !!}</div>
        @include('web-components::v2.list.smRecImgs', ['sum' => ['data' => array_splice($data['list']['data'], 0, 4), 'extraParams' => array_merge(array_get($sum, 'extraParams', []), ['classActions' => ['css' => '', 'cname' => 'smRecImg-hz']])]])

        @include('web-components::v2.list.sqrRecImgsHz', ['sum' => ['data' => $data['list']['data'], 'extraParams' => array_merge(array_get($sum, 'extraParams', []), ['classActions' => ['css' => 'list-sqrRecImgs', 'cname' => 'sqrRecImg-hz']])]])

        @if(sizeof(array_get($data, 'list.button', [])))
            @include('web-components::v2.buttons.basic', ['sum' => ['data' => $data['list']['button'], 'extraParams' => array_merge(array_get($sum, 'extraParams', []), ['classActions' => ['css' => '', 'cname' => ''], 'searchFor' => '.cd-smRecImgs-sqrRecImgs', 'appendTo' => '.list-sqrRecImgs', 'template' => 'v2sqrRecImgHz' ])]])
        @endif
    </div> 
@endif