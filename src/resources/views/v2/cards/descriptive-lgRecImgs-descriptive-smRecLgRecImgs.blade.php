@if(sizeof(array_get($sum, 'data.list.data', [])) > 0)
    <?php $data = $sum['data']; ?>
    <div class="bg-primary pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac cd-descriptive-lgRecImgs-descriptive-smRecLgRecImgs {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
        <div class="ft-pr bd-3 md-bd-3 cl-lt c-tt">{!! $data['title'] !!}</div>
        @include('web-components::v2.list.descriptive-lgRecImgs', ['sum' => ['data' => array_splice($data['list']['data'], 0, 2), 'extraParams' => array_merge(array_get($sum, 'extraParams', []), ['classActions' => ['css' => '', 'cname' => 'descriptive-lgRecImg']])]])

        @include('web-components::v2.list.descriptive-smRecLgRecImgs', ['sum' => ['data' => $data['list']['data'], 'extraParams' => array_merge(array_get($sum, 'extraParams', []), ['classActions' => ['css' => 'list-descriptive-smRecLgRecImgs', 'cname' => 'descriptive-smRecLgRecImg']])]])

        @if($sum['extraParams']['pageType'] != 'amp')
            @if(sizeof(array_get($data, 'list.button', [])))
                @include('web-components::v2.buttons.basic', ['sum' => ['data' => $data['list']['button'], 'extraParams' => array_merge(array_get($sum, 'extraParams', []), ['classActions' => ['css' => '', 'cname' => ''], 'searchFor' => '.cd-descriptive-lgRecImgs-descriptive-smRecLgRecImgs', 'appendTo' => '.list-descriptive-smRecLgRecImgs', 'template' => 'v2descriptivesmRecLgRecImg' ])]])
            @endif
        @endif
        
    </div> 
@endif