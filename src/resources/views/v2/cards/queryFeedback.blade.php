<?php $data = $sum['data']; ?>
<section class="bg-primary pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac card {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
    @if(array_key_exists('postbox', $data))
        @include('web-components::v2.entities.postbox', ['sum' => ['data' => $data['postbox'], 'extraParams' => ['classActions' => ['css' => '', 'cname' => ''], 'pageType' => array_get($sum, 'extraParams.pageType', 'page')]]])
    @endif
    @if((sizeof(array_get($data, 'list.link', [])) > 0) && (sizeof(array_get($data, 'list.data', [])) > 2))
        @include('web-components::v2.links.simple', ['sum' => ['data' => $data['list']['link'], 'extraParams' => ['classActions' => ['css' => 'in-lk mg-t-10', 'cname' => ''], 'pageType' => array_get($sum, 'extraParams.pageType', 'page')]]])
    @endif
</section>