@if(sizeof(array_get($sum, 'data.list.data', [])) > 0)
    <?php $data = $sum['data']; ?>
    <div class="bg-primary pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
        <div class="ft-pr md-bd-4 bd-4 c-tt">{!! $data['title'] !!}</div>
        @include('web-components::v2.list.sqrRecImgsNumbered', ['sum' => ['data' => $data['list']['data'], 'extraParams' => array_merge(array_get($sum, 'extraParams', []), ['classActions' => ['css' => '', 'cname' => '']])]])
    </div> 
@endif