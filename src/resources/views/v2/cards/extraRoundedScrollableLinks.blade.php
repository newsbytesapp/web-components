@if(sizeof(array_get($sum, 'data.list.data', [])) > 0)
<?php $data = $sum['data']; ?>
<section class="card {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
    <div class="ft-pr bd-4 md-bd-4 c-tt pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">{!! $data['title'] !!}</div>
    <div class="pos-rel">
        <div class="pos-abs col-2 col-lg-1 z-index-1 gd-tint-white-smoke-white-smoke0-white-smoke100-neg-half tp-init bt-init lt-init"></div>
        @include('web-components::v2.list.extraRoundedScrollableLinks', ['sum' => ['data' => $data['list']['data'], 'extraParams' => ['classActions' => ['css' => '', 'cname' => ''], 'pageType' => array_get($sum, 'extraParams.pageType', 'page')]]])
        <div class="pos-abs col-2 col-lg-1 z-index-1 gd-tint-white-smoke-white-smoke0-white-smoke100-half tp-init bt-init rt-init"></div>
    </div>
</section>
@endif