@if(sizeof(array_get($sum, 'data.list.data', [])) > 0)
    <?php $data = $sum['data']; ?>
    <div class="bg-primary pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
        @if(isset($data['tagUrl']) && !empty($data['tagUrl']))
        <h2 class="ft-pr md-bd-4 bd-4 c-tt">
            <a href="{!! $data['tagUrl'] !!}" title="{{$data['title']}}">{!! $data['title'] !!}</a>
        </h2>
        @else
        <h2 class="ft-pr md-bd-4 bd-4 c-tt">{!! $data['title'] !!}</h2>
        @endif
        @include('web-components::v2.list.sqrRecImgs', ['sum' => ['data' => $data['list']['data'], 'extraParams' => array_merge(array_get($sum, 'extraParams', []), ['classActions' => ['css' => '', 'cname' => '']])]])
    </div> 
@endif