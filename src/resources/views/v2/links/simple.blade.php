<?php
    $data = $sum['data'];
    $linkType = array_get($data, 'type', 'internal');
    $linkTarget = ($linkType == 'external') ? 'target="_blank"' : '';
?>
<div class="{{array_get($data, 'size', 'free') == 'fixed' ? 'dp-ib' : ''}} {{array_get($sum, 'extraParams.classActions.css', '')}} cl-link s-s-lk" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
    <a class="dp-fx" href="{!! $data['url'] !!}" aria-label="Link to {!! $data['title'] !!}" {!! $linkTarget !!} {!! $linkType == "external" ? 'rel="_noopener"' : '' !!}>
        <span class="ft-ter md-ter-bd-2 ter-bd-2">{!! $data['title'] !!}</span>
        <span class="{{$linkType == 'external' ? 'nb-icon-external-link' : 'nb-icon-arrow-forward'}} icon"></span>
    </a>
</div>