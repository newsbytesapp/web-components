<?php 
    $data = $sum['data']; 
    $extraParams = $sum['extraParams'];
    $searchFor = array_get($extraParams, 'searchFor', '');
    $appendTo = array_get($extraParams, 'appendTo', '');
    $template = array_get($extraParams, 'template', '');
    $api = array_get($data, 'api', '');
    $componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>

<div class="load-more-parent">
    <button id="load-more-basic-{{$componentId}}" class="ft-pr bd-6 md-bd-6 ff-primary load-more" data-search-for="{{$searchFor}}" data-append-to="{{$appendTo}}" data-api="{{$api}}" data-template="{{$template}}">{!! $data['title'] !!}</button>
    @include('web-components::toasts.dotsLoader')
    @include('web-components::toasts.error')
</div>