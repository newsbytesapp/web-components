<?php
$data = $sum['data'];
$linkType = array_get($data, 'type', 'internal');
$fixedSizeId = floor(time()/(30*86400)); //str_random(6)
$componentId = array_get($data, 'id', $fixedSizeId);
?>
@switch($type)
    @case('amp')
    <div class="ovr-initial ft-pr c-drdown">
        <input type="checkbox" id="fxd-p-amp-{{$componentId}}" name="stateless-dropdown-amp" class="drdown-stateless-amp hidden" />
        <label for="fxd-p-amp-{{$componentId}}" class="dp-ib bg-transparent cl-ink-dk ff-primary cs-ptr">
            <div class="ft-ter md-ter-reg-2 ter-reg-2 ff-primary cl-lt {{$data['selector']['label'] == '' ? 'hidden' : ''}}">{!! $data['selector']['label'] !!}</div>
            <div class="dp-fx">
                <span class="ft-ter md-ter-reg-2 ter-reg-2">{!! $data['selector']['title'] !!}</span>
                <span class="nb-icon-drop-more d-icon"></span>
            </div>
        </label>
        <div class="hidden mg-t-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-2 drdown-box d-box {{$data['content']['alignment'] == 'right' ? 'rt-init' : ''}}">
            @foreach($data['content']['data'] as $item)
            <a class="ft-ter md-ter-reg-2 ter-reg-2" href="{!! $item['url'] !!}" {!! $data['linkTargetAttr'] == "_blank" ? 'target="_blank"' : '' !!} {!! isset($item['lang_code']) ? 'hreflang="' . $item['lang_code'] . '"' : '' !!} title="{!! $item['name'] !!}">{!! $item['name'] !!}</a>
            @endforeach
        </div>
    </div>
    @break
    @default
    <div class="ovr-initial drdown-parent ft-pr c-drdown">
        <button class="cl-ink-dk ff-primary drdown-selector drdown-selector-plain b-sel" id="fxd-p-{{$componentId}}">
            <div class="ft-ter md-ter-reg-2 ter-reg-2 ff-primary cl-lt {{$data['selector']['label'] == '' ? 'hidden' : ''}}">{!! $data['selector']['label'] !!}</div>
            <div class="dp-fx">
                <span class="ft-ter md-ter-reg-2 ter-reg-2">{!! $data['selector']['title'] !!}</span>
                <span class="nb-icon-drop-more d-icon"></span>
            </div>
        </button>
        <div class="hidden wd-full ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-2 drdown-box d-box {{$data['content']['alignment'] == 'right' ? 'rt-init' : ''}}" data-status="off" data-animation-entrance="ent-slide-down-mg" data-animation-exit="ext-slide-up-mg" data-type="stateless">
            @foreach($data['content']['data'] as $item)
            <a class="ft-ter md-ter-reg-2 ter-reg-2" href="{!! $item['url'] !!}" target="{{$data['linkTargetAttr']}}" {!! $linkType == "external" ? 'rel="_noopener"' : '' !!} {!! isset($item['lang_code']) ? 'hreflang="' . $item['lang_code'] . '"' : '' !!}  title="{!! $item['name'] !!}">{!! $item['name'] !!}</a>
            @endforeach
        </div>
    </div>
    @break
@endswitch
