@switch($type)
@case('amp')
<amp-accordion disable-session-states animate>
    <section>
        <h6 class="accordion-header">
            @if(array_get($data, 'linkable', true))
            <a class="md-bd-5 bd-5 {{isset($classList) ? $classList : ''}}" href="{!! $data['url'] !!}">{!! $data['title'] !!}</a>
            @else
            <div class="sel accordion-selector {{isset($classList) ? $classList : ''}}">
                <span class="md-bd-5 bd-5">{!! $data['title'] !!}</span>
                <span class="nb-icon-drop-more d-icon"></span>
            </div>
            @endif
        </h6>
        <div class="accordion-content items">
            @if(array_get($data, 'list.data', []))
            @foreach($data['list']['data'] as $item)
            <a class="md-reg-2 reg-2" href="{!! $item['url'] !!}">{!! $item['title'] !!}</a>
            @endforeach
            @endif
        </div>
    </section>
</amp-accordion>
@break
@default
@if(array_get($data, 'linkable', true))
<a class="ft-ter md-ter-bd-3 menu-item ter-bd-3 pd-b-20 pd-l-10 pd-t-10 ter-bd-3 wd-full dp-ib slab" href="{!! $data['url'] !!}">{!! $data['title'] !!}</a>
@else
<div class="slab">
    <div class="sel accordion-selector {{isset($classList) ? $classList : ''}}">
        <span class="ft-ter md-ter-bd-3 ter-bd-3">{!! $data['title'] !!}</span>
        <span class="nb-icon-drop-more d-icon"></span>
    </div>
    <div class="list hidden items">
        @if(array_get($data, 'list.data', []))
        @foreach($data['list']['data'] as $item)
        <a class="ft-ter menu-item md-ter-reg-2 ter-reg-2 " href="{!! $item['url'] !!}">{!! $item['title'] !!}</a>
        @endforeach
        @endif
    </div>
</div>
@endif
@break
@endswitch
