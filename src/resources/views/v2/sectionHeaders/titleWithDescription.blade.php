@if(sizeof(array_get($sum, 'data', [])) > 0)
    <?php $data = $sum['data']; ?>
    <section class="bg-primary pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac ft-pr {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
        @if(array_get($data, 'title', ''))
        <h1 class="cl-ink-dk md-bd-2 bd-2">{!! $data['title'] !!}</h1>
        @endif
        @if(array_get($data, 'description', ''))
        <p class="cl-lt mg-t-2 md-reg-2 reg-2">{!! $data['description'] !!}</p>
        @endif
    </section>
@endif