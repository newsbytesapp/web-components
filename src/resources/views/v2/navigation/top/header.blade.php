<?php
$headerData = [];
if(isset($data) && !empty(array_get($data, 'menu', []))){
    $headerData = $data['menu'];
}else{
    $headerData = $menu;
}
$type = $pageType;
$searchModule = array_get($headerData, 'searchModule', false);
$loginModule = array_get($headerData, 'loginModule', false);
$loginFunctionality = $loginModule ? 'enabled' : 'disabled';
$langVariation = array_get($headerData, 'langVariation', []);
if ($loginModule) {
    $firebaseAppId = config('web-components.services.firebase.projectId', '');
    $firebaseAppKey = config('web-components.services.firebase.projectKey');
    $firebaseMessagingId = config('web-components.services.firebase.messagingId');
}

if (!function_exists('primaryHeaderPresenter')) {
    function primaryHeaderPresenter($headerData = [])
    {
        $headerPresentable = [];
        if (isset($headerData['menu']['list']['data']) && (sizeof($headerData['menu']['list']['data']) > 0)) {
            foreach ($headerData['menu']['list']['data'] as $headerItem) {
                $item = [];
                $item['size'] = 'fixed';
                $item['selected'] = $headerItem['selected'];
                $item['selector'] = ['label' => $headerItem['title'], 'title' => '', 'url' => $headerItem['url'], 'linkable' => true];
                $item['content']['data'] = [];
                if (array_get($headerItem, 'list', false) && array_get($headerItem, 'list.data', false)) {
                    foreach ($headerItem['list']['data'] as $listItem) {
                        $drdownItem = [];
                        $drdownItem['name'] = $listItem['title'];
                        $drdownItem['url'] = $listItem['url'];
                        array_push($item['content']['data'], $drdownItem);
                    }
                }
                array_push($headerPresentable, $item);
            }
        }
        return $headerPresentable;
    }
}

if (!function_exists('secondaryHeaderPresenter')) {
    function secondaryHeaderPresenter($header = [])
    {
        $headerPresentable = [];
        $skipAttrs = ['list'];
        if (sizeof($header) > 0) {
            $fItem = [];
            foreach ($header['routePath'][0] as $key => $value) {
                if (!in_array($key, $skipAttrs)) {
                    $fItem[$key] = $value;
                }
            }
            $headerPresentable['root'] = $fItem;
            $headerPresentable['list'] = [];
            if (isset($header['list'])) {
                foreach ($header['list'] as $item) {
                    $dItem = [];
                    foreach ($item as $key => $value) {
                        if (!in_array($key, $skipAttrs)) {
                            $dItem[$key] = $value;
                        }
                    }
                    array_push($headerPresentable['list'], $dItem);
                }
            }
        }
        return $headerPresentable;
    }
}

if (!function_exists('secondaryHeaderItems')) {
    function secondaryHeaderItems($data, $rPath = [], $secondaryHeader = [])
    {
        if (isset($data['list'])) {
            foreach ($data['list']['data'] as $item) {
                if ($item['selected']) {
                    array_push($rPath, $item);
                    $secondaryHeader['routePath'] = $rPath;
                    if (isset($item['list']))
                        $secondaryHeader['list'] = $item['list']['data'];
                    $secondaryHeader = secondaryHeaderItems($item, $rPath, $secondaryHeader);
                }
            }
        }
        return $secondaryHeader;
    }
}

/* Populating Primary Header and transforming it */
$primaryHeaderPresentable = primaryHeaderPresenter($headerData);

/* Populating Secondary Header and transforming it */
$secondaryHeader = secondaryHeaderItems($headerData['menu']);
$secondaryHeaderPresentable = secondaryHeaderPresenter($secondaryHeader);
?>
@if($type != 'amp')
@section('pageScript')
@parent
@if($loginModule)
var loginModule = "{!! $loginFunctionality !!}";
@endif
@if(array_get($headerData, 'notifSupport', false))
var OneSignalConfig = {
    appId: '{{config("web-components.oneSignalWebPushId")}}',
    allowLocalhostAsSecureOrigin: true,
    autoRegister: false,
    autoResubscribe: true,
    notifyButton: {
        enable: false
    },
    safari_web_id: "{{config('web-components.oneSignalSafariPushId')}}",
    persistNotification: false,
    welcomeNotification: {
        "title": "Awesome!",
        "message": "You will now receive notifications on important stories."
    }
};
@endif
@stop
@endif

@switch($type)
@case('amp')
<div class="top-nav">
    <div class="box-shdw-header">
        <div class="wrapper-full-wd-amp-ac">
            <div class="top-bar touch-screen">
                <div class="slab-logo first-tier">
                    <div class="dp-fx ht-full fx-al-ct pos-rel">
                        <div class="nb-icon-menu m-icon" on="tap:sidebar.toggle" role="button" tabindex="0" aria-label="Sidebar toggle"></div>
                        <a href="{!! array_get($headerData, 'logoUrl', '/') !!}" class="mg-l-10">
                            <amp-img src="{{$headerData['thumb']}}" alt="{!! $headerData['title'] !!}" height="36" layout="fixed-height" class="{{$headerData['classList'] or ''}} min-wd-px-120"></amp-img>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if((sizeof($secondaryHeaderPresentable) > 0) && (sizeof($secondaryHeaderPresentable['list']) > 0))
    <div class="second-tier pos-rel br-b-1p-white-smoke">
        <div class="pd-b-10 pd-l-r-full-wd-ac pd-t-10 wrapper-full-wd-amp-ac">
            <div class="dp-fx fx-al-ct">
                <a href="{!! $secondaryHeaderPresentable['root']['url'] !!}" class="reg-2 mg-r-14">{!! $secondaryHeaderPresentable['root']['title']!!}</a>
                <div class="reg-2">|</div>
                <div class="hz-scroll-ac">
                    @foreach($secondaryHeaderPresentable['list'] as $item)
                    <a href="{!! $item['url'] !!}" class="{{($item['selected']) ? 'bd-6' : 'reg-2'}} {{$loop->first ? 'mg-l-14' : 'mg-l-30'}} {{$loop->last ? 'mg-r-30' : ''}}">{!! $item['title']!!}</a>
                    @endforeach
                </div>
                <div class="gd-tint-white-white0-white100-half col-2 rt-init tp-init bt-init pos-abs"></div>
            </div>
        </div>
    </div>
    @endif
</div>
@include('web-components::v2.navigation.top.side', ['sum' => ['data' => $headerData, 'extraParams' => ['classActions' => ['css' => '', 'cname' => ''], 'pageType' => $type]]])
@break
@default
<header id="header-nav" class="bg-primary {{isset($classList) ? $classList : ''}} c-nt-h">
    <nav class="top-nav">
        <div class="box-shdw-header">
            <div class="wrapper hidden-in-touch">
                <div class="top-bar lg-screen" data-search="off">
                    <div class="first-tier ft-pr reg-2">
                        <div class="nb-icon-menu side-drawer-toggle m-icon"></div>
                        @if(isset($headerData['includeSvg']))
                        <a href="{!! array_get($headerData, 'logoUrl', '/') !!}" class="svg-inclusion mg-l-30 clickable-logo" data-svg="{{$headerData['includeSvg']}}" aria-label="{!! $headerData['title'] !!}"><span class="dp-bl wd-px-120 visibility-hidden">{!! $headerData['title'] !!}</span></a>
                        @else
                        <a href="{!! array_get($headerData, 'logoUrl', '/') !!}" class="mg-l-30 clickable-logo">
                            <img src="{{$headerData['thumb']}}" alt="{!! $headerData['title'] !!}" title="{!! $headerData['title'] !!}" class="{{$headerData['classList'] or ''}} ht-px-36" /><span class="hidden">{!! $headerData['title'] !!}</span>
                        </a>
                        @endif
                        @include('web-components::v2.list.hoverableDropdownButtonsContainer', ['sum' => ['data' => $primaryHeaderPresentable, 'extraParams' => ['classActions' => ['css' => 'overflow-check visibility-hidden', 'cname' => 'list-hdBttns'], 'pageType' => $type]]])
                        @if($searchModule || $langVariation)
                        <div class="last-box lt-box {{($searchModule && $langVariation) ? 'fx-js-bw' : 'fx-js-ct'}}">
                            @if($searchModule)
                            <span class="nb-icon-search-mobile-header s-icon search-for"></span>
                            @endif
                            @if((array_get($langVariation, 'content.data', false)) && sizeof($langVariation['content']['data']) > 0)
                            @include('web-components::v2.dropdowns.plain', ['sum' => ['data' => $langVariation, 'extraParams' => ['classActions' => ['css' => '', 'cname' => 'c-sidebar'], 'pageType' => $type]]])
                            @endif
                        </div>
                        @endif
                    </div>
                    <div class="search-bar">
                        <input type="text" class="search-query ft-ter md-ter-reg-3 ter-reg-3 wd-full" placeholder="{{array_get($headerData['searchInfo'], 'text', 'Search')}}" value="" id="searchLgView" />
                        <span class="cl-lt nb-icon-cancel cancel-search cr-icon"></span>
                    </div>
                    <span class="aux-text hidden">{!! array_get($headerData, 'auxText', '')!!}</span>
                </div>
            </div>
            @if(!empty(array_get($headerData, 'supportingSecondaryHeaderData.list.data', [])))
            <div class="pd-t-14 pd-b-14 br-t-1p-white-smoke hidden-in-touch">
                <div class="wrapper">
                    <div class="dp-fx fx-al-ct neg-mg-lr-px-15-ac cl-acadia">
                        @if(!empty(array_get($headerData, 'supportingSecondaryHeaderData.title', '')))
                        <div class="pd-l-14 pd-r-14 ft-ter md-ter-bd-2 ter-bd-2 cl-ink-dk">{!! $headerData['supportingSecondaryHeaderData']['title'] !!}</div>
                        @endif
                        @foreach($headerData['supportingSecondaryHeaderData']['list']['data'] as $item)
                        <div class="pd-l-14 pd-r-14">
                            <a href="{!! $item['url'] !!}" class="ft-ter md-ter-reg-1 ter-reg-1">{!! $item['title'] !!}</a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
            <div class="hidden-lg">
                <div class="top-bar touch-screen" data-search="off">
                    <div class="slab-logo first-tier">
                        <div class="dp-fx ht-full fx-al-ct pos-rel">
                            <div class="nb-icon-menu m-icon side-drawer-toggle"></div>
                            @if(isset($headerData['includeSvg']))
                            <a href="{!! array_get($headerData, 'logoUrl', '/') !!}" class="mg-l-10 pd-b-6 pd-t-6" data-svg="{{$headerData['includeSvg']}}" aria-label="{!! $headerData['title'] !!}">{!! $iconSVG !!}</a>
                            @else
                            <a href="{!! array_get($headerData, 'logoUrl', '/') !!}" class="mg-l-10 pd-b-6 pd-t-6">
                                <img src="{{$headerData['thumb']}}" alt="{!! $headerData['title'] !!}" title="{!! $headerData['title'] !!}" class="{{$headerData['classList'] or ''}} ht-px-36" /><span class="hidden">{!! $headerData['title'] !!}</span>
                            </a>
                            @endif
                            @if($searchModule || $langVariation)
                            <div class="lt-box {{($searchModule && $langVariation) ? 'fx-js-bw' : 'fx-js-ct'}}">
                                @if($searchModule)
                                <span class="nb-icon-search-mobile-header s-icon search-for"></span>
                                @endif
                                @if((array_get($langVariation, 'content.data', false)) && sizeof($langVariation['content']['data']) > 0)
                                {{-- @include('web-components::v2.dropdowns.plain', ['data'=> $langVariation, 'type'=> $type, 'classList' => 'mg-l-8']) --}}
                                @include('web-components::v2.dropdowns.plain', ['sum' => ['data' => $langVariation, 'extraParams' => ['classActions' => ['css' => '', 'cname' => ''], 'pageType' => $type]]])
                                @endif
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="search-bar">
                        <input type="text" class="search-query ft-ter md-ter-reg-3 ter-reg-3 s-query" placeholder="{{array_get($headerData['searchInfo'], 'text', 'Search')}}" value="" id="searchTouchView" />
                        <span class="cl-lt nb-icon-cancel cr-icon cancel-search"></span>
                    </div>
                </div>
            </div>
        </div>
        @if((sizeof($secondaryHeaderPresentable) > 0) && (sizeof($secondaryHeaderPresentable['list']) > 0))
        <div class="second-tier pos-rel br-b-1p-white-smoke">
            <div class="wrapper pd-t-10 pd-b-10">
                <div class="dp-fx fx-al-ct">
                    <a href="{!! $secondaryHeaderPresentable['root']['url'] !!}" class="ft-ter md-ter-reg-2 ter-reg-2 left-most">{!! $secondaryHeaderPresentable['root']['title']!!}</a>
                    <div class="ft-ter md-ter-reg-2 ter-reg-2">|</div>
                    <div class="hz-scroll-ac secondary-header">
                        @foreach($secondaryHeaderPresentable['list'] as $item)
                        <a href="{!! $item['url'] !!}" class="link-item {{($item['selected']) ? 'ft-ter md-ter-bd-2 ter-bd-2' : 'ft-ter md-ter-reg-2 ter-reg-2'}}">{!! $item['title']!!}</a>
                        @endforeach
                    </div>
                    <div class="gd-tint-white-white0-white100-half col-2 rt-init tp-init bt-init pos-abs"></div>
                </div>
            </div>
        </div>
        @endif
        @if(isset($data) && (array_get($data, 'widgets.ticker', []) && (sizeof($data['widgets']['ticker']) > 0)))
        @include('web-components::widgets.ticker', ['data' => $data['widgets']['ticker']])
        @endif
    </nav>
    <nav class="side-nav pos-fix wd-full mx-wd-px-380 tp-init bt-init lt-init bg-primary z-index-4 ovr-scroll ovr-x-hidden hidden animation-dur-scale-3 hide-native-scrollbar m-sidebar">
        @include('web-components::v2.navigation.top.side', ['sum' => ['data' => $headerData, 'extraParams' => ['classActions' => ['css' => '', 'cname' => ''], 'pageType' => $type]]])
    </nav>
</header>
@include('web-components::v2.tints.completeTint', ['type' => $type])
@break
@endswitch
