<?php
    $data = $sum['data'];
    function accordionPresenter($accordion = [])
    {
        $accordionPresentable = [];
        if(isset($accordion['list'])){
            $accordionPresentable = array_map(function ($item)
            {
                $item['linkable'] = true;
                if(array_key_exists('list', $item))
                    $item['linkable'] = false;
                return $item;
            }, $accordion['list']['data']);
        }
        return $accordionPresentable;
    }
    $accordionPresentable = accordionPresenter($data['menu']);
?>
@switch($type)
    @case('amp')
    <amp-sidebar id="sidebar" layout="nodisplay" side="left" class="m-sidebar side-nav wd-full mx-wd-px-380 bg-primary ovr-scroll ovr-x-hidden hide-native-scrollbar ft-pr">
        <div class="ft-pr sd-upper">
            <div class="dp-fx fx-al-ct fx-js-bw">
                <a href="{!! array_get($data, 'logoUrl', '/') !!}" class="pd-t-10 pd-b-10">
                    <amp-img src="{{$data['thumb']}}" alt="{{$data['title']}}" height="36" layout="fixed-height" class="{{$data['classList'] or ''}} min-wd-px-120"></amp-img>
                </a>
                <span class="nb-icon-cancel c-icon" on="tap:sidebar.toggle" role="button" tabindex="0" aria-label="Sidebar toggle"></span>
            </div>
        </div>
        @if(sizeof($accordionPresentable) > 0)
        <hr class="br-t-1p-white-smoke"/>
        <div class="c-accordion accordion accordion-side-drawer">
            @foreach($accordionPresentable as $item)
                @include('web-components::v2.accordion.simple', ['type' => $type, 'classList' => 'mg-t-10', 'data' => $item])
            @endforeach
        </div>
        @endif
        @if(sizeof($data['otherLinks']['list']) > 0)
        <hr class="br-t-1p-white-smoke"/>
        <div class="pd-l-20 pd-r-20 pd-t-30 pd-b-30">
            <div class="cl-lt reg-2">{!! $data['otherLinks']['title'] !!}</div>
            @include('web-components::v2.list.simpleLinks', ['sum' => ['data' => $data['otherLinks']['list'], 'extraParams' => ['classActions' => ['css' => 'dp-fx fx-dr-col', 'cname' => 'list-sLinks'], 'pageType' => $type]]])
        </div>
        @endif
        @if((sizeof($data['appStore']) > 0) && (array_get($data['appStore'], 'size', 'free')))
        <hr class="br-t-1p-white-smoke"/>
        <div class="pd-l-20 pd-r-20 pd-t-30 pd-b-30">
            @include('web-components::v2.links.simple', ['sum' => ['data' => $data['appStore'], 'extraParams' => ['classActions' => ['css' => 'd-link', 'cname' => ''], 'pageType' => $type]]])
        </div>
        @endif
        @if(sizeof($data['socialProfiles']['list']) > 0)
        <hr class="br-t-1p-white-smoke"/>
        <div class="pd-t-30 pd-b-30 pd-l-20 pd-r-20">
            <div class="cl-lt reg-2">{!! $data['socialProfiles']['title'] !!}</div>
            @include('web-components::v2.list.socialProfiles', ['sum' => ['data' => $data['socialProfiles']['list'], 'extraParams' => ['classActions' => ['css' => 'neg-mg-lr-px-15-ac dp-fx pd-t-24 list-sprofiles', 'cname' => ''], 'pageType' => $type]]])
        </div>
        @endif
    </amp-sidebar>
    @break
    @default
        <div class="ft-pr sd-upper">
            <div class="dp-fx fx-al-ct fx-js-bw">
                @if(isset($data['includeSvg']))
                <a href="{!! array_get($data, 'logoUrl', '/') !!}" class="svg-inclusion clickable-logo pd-t-10 pd-b-10" data-svg="{{$data['includeSvg']}}" aria-label="{{$data['title']}}"><span class="hidden">{{$data['title']}}</span>
                </a>
                @else
                <a href="{!! array_get($data, 'logoUrl', '/') !!}" class="pd-t-10 pd-b-10 clickable-logo">
                    <img src="{{$data['thumb']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="{{$data['classList'] or ''}} ht-px-36"/>
                </a>
                @endif
                <span class="nb-icon-cancel side-drawer-toggle c-icon"></span>
            </div>
            @if($loginModule)
            <div class="mg-t-20">
                <button class="ft-ter md-ter-bd-2 ter-bd-2 login lg-bttn" data-status="out">Log in/Sign up</button>
                <div class="signed-in-user dp-fx fx-al-st text-logout hidden">
					<img class="user-signed-in-img userIcon sg-img" src="" alt="User Placeholder" title="User Placeholder"/>
					<div class="mg-l-10 dp-fx fx-dr-col ft-ter md-ter-bd-2 ter-bd-2">
						<p class="cl-ink-dk">Hi, <span class="userText cl-ink-dk text-capitalize"></span></p>
						<p class="cl-link cs-ptr ft-ter md-ter-bd-1 ter-bd-1 logout mg-t-2 text-logout">Logout</p>
					</div>
				</div>
            </div>
            @endif
        </div>
        @if(sizeof($accordionPresentable) > 0)
        <hr class="br-t-1p-white-smoke"/>
        <div class="accordion accordion-side-drawer c-accordion">
            @foreach($accordionPresentable as $item)
                @include('web-components::v2.accordion.simple', ['type' => $type, 'classList' => '', 'data' => $item])
            @endforeach
        </div>
        @endif
        @if(sizeof($data['otherLinks']['list']) > 0)
        <hr class="br-t-1p-white-smoke"/>
        <div class="pd-l-20 pd-r-20 pd-t-30 pd-b-30">
            <div class="cl-lt ft-ter md-ter-reg-2 ter-reg-2">{!! $data['otherLinks']['title'] !!}</div>
            @include('web-components::v2.list.simpleLinks', ['sum' => ['data' => $data['otherLinks']['list'], 'extraParams' => ['classActions' => ['css' => 'dp-fx fx-dr-col', 'cname' => 'list-sLinks'], 'pageType' => $type]]])
        </div>
        @endif
        @if((sizeof($data['appStore']) > 0) && (array_get($data['appStore'], 'size', 'free')))
        <hr class="br-t-1p-white-smoke"/>
        <div class="pd-l-20 pd-r-20 pd-t-30 pd-b-30">
            @include('web-components::v2.links.simple', ['sum' => ['data' => $data['appStore'], 'extraParams' => ['classActions' => ['css' => 'd-link', 'cname' => ''], 'pageType' => $type]]])
        </div>
        @endif
        @if(sizeof($data['socialProfiles']['list']) > 0)
        <hr class="br-t-1p-white-smoke"/>
        <div class="pd-t-30 pd-b-30 pd-l-20 pd-r-20">
            <div class="cl-lt ft-ter md-ter-reg-2 ter-reg-2">{!! $data['socialProfiles']['title'] !!}</div>
            @include('web-components::v2.list.socialProfiles', ['sum' => ['data' => $data['socialProfiles']['list'], 'extraParams' => ['classActions' => ['css' => 'neg-mg-lr-px-15-ac dp-fx pd-t-24 list-sprofiles', 'cname' => ''], 'pageType' => $type]]])
        </div>
        @endif
    @break
@endswitch