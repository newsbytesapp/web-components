<?php
    $links = $footer['links'];
    if(isset($footer['text']) && ($footer['text'] != '')){
        $displayStatic = false;
    }else{
        $displayStatic = true;
    }
?>
<footer class="bg-black pd-t-46 pd-b-50 mg-t-50 bg-ink-dk ft-pr reg-2" id="footer">
    <div class="wrapper cl-primary">
        <div class="fx-row fx-js-bw">
            <div class="fx-row neg-mg-lr-px-10-ac">
                @foreach($links as $linkItem)
                <a href="{!! $linkItem['url'] !!}" target="_blank" class="mg-l-12 {{$loop->last ? 'mg-r-20' : 'mg-r-12'}} pd-t-18 pd-b-18 mg-t-6 mg-b-6 tx-ct min-wd-px-50">{!! $linkItem['title'] !!}</a>
                @endforeach
            </div>
            @if($displayStatic)
            <span class="pd-t-18 pd-b-18 mg-t-6 mg-b-6 tx-ct mg-l-12">All rights reserved &copy; NewsBytes {{date("Y")}}</span>
            @else
            <span class="pd-t-18 pd-b-18 mg-t-6 mg-b-6 tx-ct mg-l-12">{!! $footer['text'] !!}</span>
            @endif
        </div>
    </div>
</footer>