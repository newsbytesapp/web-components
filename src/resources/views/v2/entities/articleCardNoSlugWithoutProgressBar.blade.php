<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
$renderOptClass = array_get($data, 'render_optimized', false) ? 'nbrh' : '';
$tagEn = (isset($tagEn) && !empty($tagEn)) ? $tagEn : array_get($data, 'tagEn', '');
?>
@switch($card)
    @case('coverCard')
    <div class="hidden-slug" data-id="{{$data['htmlId']}}"></div>
    <?php $data['video'] = array_merge($data['video'], array_map(function ($item) {return $item;}, ['title' => $data['title']])); ?>
    @if(!empty(array_get($data, 'video.src', '')))
        @include('web-components::v2.entities.videoTileWithImg', ['sum' => ['data' => $data['video']],'classList' =>'mg-t-10 pd-l-md-18 mg-b-6'])
    @endif
    @if(!empty(array_get($data, 'slides', [])))
    <section class="slide-show-parent visibility-hidden mg-t-10 pd-l-md-18 mg-b-6">
        <section class="slide-show default with-centerMode" id="slideshow-{!! $data['htmlId'] !!}-{{$componentId}}">
            @foreach($data['slides'] as $item)
            <figure class="mg-r-20">
                <img alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="dp-bl wd-full {{$loop->first ? 'cover-card-img' : ''}}" src="{{$item['img']}}">
                @if(!empty(array_get($item, 'caption', '')))
                <figcaption class="cl-lt pd-t-4 ft-sec md-sec-reg-2 sec-reg-2 bg-primary {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">{!! $item['caption'] !!}</figcaption>
                @endif
            </figure>
            @endforeach
        </section>
        <div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
    </section>
    @elseif((!empty($data['ximg'])) && (empty($data['video']['src'])))
    @if(!(isset($hideCoverImg) && $hideCoverImg))
        <figure class="mg-b-6">
            <div class="asp-ratio r-16-9">
                @if(isset($data['imageFallback']) && $data['imageFallback'])
                <picture>
                    <source
                        media="(max-width: 599px) and (min-device-pixel-ratio: 3),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 3),(max-width: 599px) and (min-resolution: 3dppx),(max-width: 599px) and (min-resolution: 288dpi),(min-width: 768px) and (min-device-pixel-ratio: 2),(min-width: 768px) and (-webkit-min-device-pixel-ratio: 2),(min-width: 768px) and (min-resolution: 3dppx),(min-width: 768px) and (min-resolution: 288dpi)"
                        srcset="{{$data['ximg']}}"/>
                    <source
                        media="(min-width: 1080px) and (min-device-pixel-ratio: 1),(min-width: 1080px) and (-webkit-min-device-pixel-ratio: 1),(min-width: 1080px) and (min-resolution: 2dppx),(min-width: 1080px) and (min-resolution: 192dpi)"
                        srcset="{{array_get($data, 'limg', array_get($data, 'ximg', ''))}}"/>
                    <source
                        media="(max-width: 599px) and (min-device-pixel-ratio: 2),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 2),(max-width: 599px) and (min-resolution: 2dppx),(max-width: 599px) and (min-resolution: 192dpi)"
                        srcset="{{$data['img']}}"/>
                    <source
                        media="(max-width: 599px) and (min-device-pixel-ratio: 1),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 1),(max-width: 599px) and (min-resolution: 1dppx),(max-width: 599px) and (min-resolution: 96dpi)"
                        srcset="{{$data['img']}}"/>
                    <img alt="{{$data['title']}}" title="{{$data['title']}}" class="dp-bl wd-full cover-card-img hidden" src="{{$data['ximg']}}"/>
                    @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
                    @include('web-components::v2.entities.imageCredits', ['sum' => ['data' => $data['imagecredit']]])
                    @endif
                    @if(!(isset($partner) || !empty($partner)) && !empty(array_get($data, 'caption', '')))
                    <figcaption class="cl-primary pd-t-6 pd-b-6 ft-sec md-sec-reg-2 sec-reg-2 bg-ink-dk pos-abs bt-init opacity-8 wd-full {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">{!! $data['caption'] !!}</figcaption>
                    @endif
                </picture>
                @if(isset($partner_ads) && $partner_ads && !(isset($sponsored) && $sponsored) && (isset($partner) && !empty($partner)) && isset($defineSlots[config('web-components.nbAdvtConf.Newsbytes_InImage')]))
                <div class="{{$advt_unit_name}} {{config('web-components.nbAdvtConf.Newsbytes_InImage')}} dp-fx fx-js-ct lt-init rt-init wd-full">
                    @include('web-components::advt.embed', ['data' => ['adType' => $advt_unit_name, 'slot' => config('web-components.nbAdvtConf.Newsbytes_InImage')], 'type' => '', 'fireat' => '', 'firedelay' => 500, 'smart' => false, 'position' => 3])
                </div>
                @endif
                @else
                @if(isset($delayImgLoading) && !empty($delayImgLoading))
                <img alt="{{$data['title']}}" title="{{$data['title']}}" class="dp-bl wd-full cover-card-img {{(isset($delayImgLoading) && !empty($delayImgLoading)) ? 'hidden' : ''}}" data-src="{{$data['img']}}">
                @else
                <img alt="{{$data['title']}}" title="{{$data['title']}}" class="dp-bl wd-full cover-card-img" src="{{$data['img']}}">
                @endif
                @if((isset($partner) && !empty($partner)))
                <div class="tag-container dp-fx fx-al-ct mg-b-6 cl-primary pd-t-6 pd-b-6 ft-sec md-sec-reg-2 sec-reg-2 bg-ink-dk pos-abs bt-init opacity-8 {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">
                    <div class="dp-fx fx-row ft-ter md-ter-bd-1 ter-bd-1">{!! $data['tag']!!}</div>
                    <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-primary opacity-50"></div>
                    @if(isset($data['creatorSnapshot']) && !empty($data['creatorSnapshot']))
                    <div class="dp-fx fx-al-ct fx-js-ct ft-ter ter-reg-1">
                        @if(count(explode('.', $data['creatorSnapshot']['timestamp'])) > 1)
                            {!! head(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                            @if(!(isset($partner) || !empty($partner)))
                            <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-primary opacity-50"></div>
                            {!! last(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                            @endif
                        @else
                            {!! head(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                            @if(!(isset($partner) || !empty($partner)))
                            <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-primary opacity-50"></div>
                            {!! last(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                            @endif
                        @endif
                    </div>
                    @endif
                </div>
                @endif
                @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
                @include('web-components::v2.entities.imageCredits', ['sum' => ['data' => $data['imagecredit']]])
                @endif
                @endif
            </div>
            @if(isset($partner_ads) && $partner_ads && !(isset($sponsored) && $sponsored) && (isset($partner) && !empty($partner)) && isset($defineSlots[config('web-components.nbAdvtConf.Newsbytes_InImage')]))
            <div class="{{$advt_unit_name}} {{config('web-components.nbAdvtConf.Newsbytes_InImage')}} dp-fx fx-js-ct lt-init rt-init wd-full">
                @include('web-components::advt.embed', ['data' => ['adType' => $advt_unit_name, 'slot' => config('web-components.nbAdvtConf.Newsbytes_InImage')], 'type' => '', 'fireat' => '', 'firedelay' => 500, 'smart' => false, 'position' => 3])
            </div>
            @endif
        </figure>
        @else
        <div class="mg-t-16 dp-fx fx-al-ct fx-row cl-dove-grey {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">
            <div class="dp-fx fx-al-ct fx-row wd-full">
                @if(!(isset($partner) || !empty($partner)) && !empty(array_get($data, 'creators', [])))
                    @foreach($data['creators'] as $item)
                    <div class="dp-fx fx-al-ct tx-decoration-none mg-r-6 {{($loop->index > 0) ? 'mg-l-6' : ''}}">
                        <span class="md-ter-reg-1 ter-reg-1 tx-ct ft-ter">{!! $item['subtitle'] !!}</span>
                        <a href="{!! $item['url'] !!}" target="_blank"><span class="md-ter-reg-1 ter-reg-1 mg-l-4 tx-ct ft-ter"><u>{!! $item['title'] !!}</u></span></a>
                    </div>
                    <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                    @endforeach
                @endif
                <div class="dp-fx fx-js-bw {{(isset($partnerLogo) && $partnerLogo) ? 'wd-full':''}}">
                    <div class="dp-fx fx-al-ct">
                        @if(isset($partner) && !empty($partner))
                        <div class="dp-fx fx-row ft-ter md-ter-bd-1 ter-bd-1">{!! $data['tag']!!}</div>
                        <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                        @endif
                        @if(isset($data['creatorSnapshot']) && !empty($data['creatorSnapshot']))
                        <div class="dp-fx fx-al-ct fx-js-ct ft-ter ter-reg-1">
                            @if(count(explode('.', $data['creatorSnapshot']['timestamp'])) > 1)
                                {!! head(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                                @if(!(isset($partner) || !empty($partner)))
                                <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                                {!! last(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                                @endif
                            @else
                                {!! head(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                                @if(!(isset($partner) || !empty($partner)))
                                <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                                {!! last(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                                @endif
                            @endif
                        </div>
                        @endif
                    </div>
                    @if(isset($partnerLogo) && $partnerLogo)
                        @if(isset($partnerLogo['includeSvg']))
                            <div class="svg-inclusion partner-logo" data-svg="{{$partnerLogo['includeSvg']}}" aria-label="{!! $partnerLogo['title'] !!}"><span class="hidden">{!! $partnerLogo['title'] !!}</span></div>
                        @else
                            <img src="{!! $partnerLogo['src'] !!}" alt="{!! $partnerLogo['title'] !!}" class="partner-logo" />
                        @endif
                    @endif
                </div>
            </div>
        </div>
        @endif
    @endif
    <h1 class="cover-card-title ft-sec bd-9 md-bd-8 mg-t-10 sticky-title {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">{!! $data['title'] !!}</h1>
    @if(! (isset($partner) && !empty($partner)) && !(isset($hideCoverImg) && $hideCoverImg) || !empty($data['video']['src']))
    <div class="mg-t-10 dp-fx fx-al-ct fx-row cl-dove-grey {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">
        <div class="mg-b-6 dp-fx fx-al-ct fx-row wd-full">
            @if(!(isset($partner) || !empty($partner)) && !empty(array_get($data, 'creators', [])))
                @foreach($data['creators'] as $item)
                <div class="dp-fx fx-al-ct tx-decoration-none mg-r-6 {{($loop->index > 0) ? 'mg-l-6' : ''}}">
                    <span class="md-ter-reg-1 ter-reg-1 tx-ct ft-ter">{!! $item['subtitle'] !!}</span>
                    <a href="{!! $item['url'] !!}" target="_blank"><span class="md-ter-reg-1 ter-reg-1 mg-l-4 tx-ct ft-ter"><u>{!! $item['title'] !!}</u></span></a>
                </div>
                <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                @endforeach
            @endif
            <div class="dp-fx fx-js-bw {{(isset($partnerLogo) && $partnerLogo) ? 'wd-full':''}}">
                <div class="dp-fx fx-al-ct">
                    @if((isset($partner) && !empty($partner)))
                    <div class="dp-fx fx-row ft-ter md-ter-bd-1 ter-bd-1">{!! $data['tag']!!}</div>
                    <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                    @endif
                    @if(isset($data['creatorSnapshot']) && !empty($data['creatorSnapshot']))
                    <div class="dp-fx fx-al-ct fx-js-ct ft-ter ter-reg-1">
                        @if(count(explode('.', $data['creatorSnapshot']['timestamp'])) > 1)
                            {!! head(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                            @if(!(isset($partner) || !empty($partner)))
                            <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                            {!! last(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                            @endif
                        @else
                            {!! head(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                            @if(!(isset($partner) || !empty($partner)))
                            <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                            {!! last(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                            @endif
                        @endif
                    </div>
                    @endif
                </div>
                @if(isset($partnerLogo) && $partnerLogo)
                    @if(isset($partnerLogo['includeSvg']))
                        <div class="svg-inclusion partner-logo" data-svg="{{$partnerLogo['includeSvg']}}" aria-label="{!! $partnerLogo['title'] !!}"><span class="hidden">{!! $partnerLogo['title'] !!}</span></div>
                    @else
                        <img src="{!! $partnerLogo['src'] !!}" alt="{!! $partnerLogo['title'] !!}" class="partner-logo" />
                    @endif
                @endif
            </div>
        </div>
    </div>
    @endif
    {{-- <div class="dp-fx fx-al-ct fx-js-bw cl-dove-grey {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">
        <div class="dp-fx fx-al-ct fx-row ft-ter md-ter-reg-1 ter-reg-1">
            <div class="dp-fx fx-al-ct fx-row ter-bd-1">{!! $data['tag']!!}</div>
            <span class="pd-l-8 pd-r-8">|</span>
            @if(isset($data['creatorSnapshot']) && !empty($data['creatorSnapshot']))
            <div class="dp-fx fx-al-ct fx-js-ct">
                @if(count(explode('.', $data['creatorSnapshot']['timestamp'])) > 1)
                    {!! head(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                    @if(!(isset($partner) || !empty($partner)))
                    <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                    {!! last(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                    @endif
                @else
                    {!! head(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                    @if(!(isset($partner) || !empty($partner)))
                    <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                    {!! last(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                    @endif
                @endif
            </div>
            @endif
        </div>
        @if(isset($partnerLogo) && $partnerLogo)
            @if(isset($partnerLogo['includeSvg']))
                <div class="svg-inclusion partner-logo" data-svg="{{$partnerLogo['includeSvg']}}" aria-label="{!! $partnerLogo['title'] !!}"><span class="hidden">{!! $partnerLogo['title'] !!}</span></div>
            @else
                <img src="{!! $partnerLogo['src'] !!}" alt="{!! $partnerLogo['title'] !!}" class="partner-logo" />
            @endif
        @endif
    </div> --}}

    <div class="{{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">
        <div class="mg-t-16 br-t-1p-grey wd-full ht-px-0"></div>
    </div>

    {{-- <div class="hidden-slug mg-t-20" data-id="{{$data['htmlId']}}"></div>
    <span class="br-l-2p-gainsboro vt-line v-ln" style="{{isset($cardCount) && $cardCount == 1 ? "" : 'height: 150vh'}}"></span>
    <span class="vt-line-progress-bar bg-@nbColor($tagEn) z-index-1 lt-init"></span>
    <div class="bg-primary lt-init tp-px-6 pd-l-10 pd-r-24 mg-b-16 bd-solid-venetian-red">
        <div id="{{$data['htmlId']}}" data-id="{{$data['htmlId']}}" class="cl-venetian-red evt-connector wd-full fx-ht fx-al-ct dp-fx fx-js-bw br-r-rd-4 ft-sec sec-bd-4">
            <span data-id="{{$data['htmlId']}}" class="cl-venetian-red cover-card ft-sec sec-bd-4">
                {!! $data['tag'] !!}
                @if(!empty(array_get($data, 'stats', [])))
                    <span class="mg-l-6 pd-l-6 stats-bar cl-dove-grey sec-bd-3">{!! $data['stats']['subtitle'] !!}</span>
                @endifF
            </span>
            <i>{{$index}}</i>
        </div>
    </div> --}}
    
    @break
    @case('coverCardTitleInset')
    <div class="hidden-slug" data-id="{{$data['htmlId']}}"></div>
    <?php $data['video'] = array_merge($data['video'], array_map(function ($item) {return $item;}, ['title' => $data['title']])); ?>
    @if(!empty(array_get($data, 'video.src', '')))
        @include('web-components::v2.entities.videoTileWithImg', ['sum' => ['data' => $data['video']],'classList' =>'mg-t-10 pd-l-md-18 mg-b-6'])
    @endif
    @if(!empty(array_get($data, 'slides', [])))
    <section class="slide-show-parent visibility-hidden mg-t-10 pd-l-md-18 mg-b-6">
        <section class="slide-show default with-centerMode" id="slideshow-{!! $data['htmlId'] !!}-{{$componentId}}">
            @foreach($data['slides'] as $item)
            <figure class="mg-r-20">
                <img alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="dp-bl wd-full {{$loop->first ? 'cover-card-img' : ''}}" src="{{$item['img']}}">
                @if(!empty(array_get($item, 'caption', '')))
                <figcaption class="cl-lt pd-t-4 ft-sec md-sec-reg-2 sec-reg-2 bg-primary {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">{!! $item['caption'] !!}</figcaption>
                @endif
            </figure>
            @endforeach
        </section>
        <div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
    </section>
    @elseif((!empty($data['ximg'])) && (empty($data['video']['src'])))
    @if(!(isset($hideCoverImg) && $hideCoverImg))
        <figure class="mg-b-6 asp-ratio r-16-9 pos-abs">
            @if(isset($data['imageFallback']) && $data['imageFallback'])
            <picture>
                <source
                    media="(max-width: 599px) and (min-device-pixel-ratio: 3),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 3),(max-width: 599px) and (min-resolution: 3dppx),(max-width: 599px) and (min-resolution: 288dpi),(min-width: 768px) and (min-device-pixel-ratio: 2),(min-width: 768px) and (-webkit-min-device-pixel-ratio: 2),(min-width: 768px) and (min-resolution: 3dppx),(min-width: 768px) and (min-resolution: 288dpi)"
                    srcset="{{$data['ximg']}}"/>
                <source
                    media="(min-width: 1080px) and (min-device-pixel-ratio: 1),(min-width: 1080px) and (-webkit-min-device-pixel-ratio: 1),(min-width: 1080px) and (min-resolution: 2dppx),(min-width: 1080px) and (min-resolution: 192dpi)"
                    srcset="{{array_get($data, 'limg', array_get($data, 'ximg', ''))}}"/>
                <source
                    media="(max-width: 599px) and (min-device-pixel-ratio: 2),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 2),(max-width: 599px) and (min-resolution: 2dppx),(max-width: 599px) and (min-resolution: 192dpi)"
                    srcset="{{$data['img']}}"/>
                <source
                    media="(max-width: 599px) and (min-device-pixel-ratio: 1),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 1),(max-width: 599px) and (min-resolution: 1dppx),(max-width: 599px) and (min-resolution: 96dpi)"
                    srcset="{{$data['img']}}"/>
                <img alt="{{$data['title']}}" title="{{$data['title']}}" class="dp-bl wd-full cover-card-img hidden" src="{{$data['ximg']}}"/>
                @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
                @include('web-components::v2.entities.imageCredits', ['sum' => ['data' => $data['imagecredit']]])
                @endif
                @if(!(isset($partner) || !empty($partner)) && !empty(array_get($data, 'caption', '')))
                <figcaption class="cl-primary pd-t-6 pd-b-6 ft-sec md-sec-reg-2 sec-reg-2 bg-ink-dk pos-abs bt-init opacity-8 wd-full {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">{!! $data['caption'] !!}</figcaption>
                @endif
            </picture>
                @if(isset($partner_ads) && $partner_ads && !(isset($sponsored) && $sponsored) && (isset($partner) && !empty($partner)) && isset($defineSlots[config('web-components.nbAdvtConf.Newsbytes_InImage')]))
            <div class="{{$advt_unit_name}} {{config('web-components.nbAdvtConf.Newsbytes_InImage')}} dp-fx fx-js-ct lt-init rt-init wd-full">
                @include('web-components::advt.embed', ['data' => ['adType' => $advt_unit_name, 'slot' => config('web-components.nbAdvtConf.Newsbytes_InImage')], 'type' => '', 'fireat' => '', 'firedelay' => 500, 'smart' => false, 'position' => 3])
            </div>
                @endif
            @else
                @if(isset($delayImgLoading) && !empty($delayImgLoading))
            <img alt="{{$data['title']}}" title="{{$data['title']}}" class="dp-bl wd-full cover-card-img {{(isset($delayImgLoading) && !empty($delayImgLoading)) ? 'hidden' : ''}}" data-src="{{$data['img']}}">
                @else
            <img alt="{{$data['title']}}" tsitle="{{$data['title']}}" class="dp-bl wd-full cover-card-img" src="{{$data['img']}}">
                @endif
            @endif
            @if(isset($partner_ads) && $partner_ads && !(isset($sponsored) && $sponsored) && (isset($partner) && !empty($partner)) && isset($defineSlots[config('web-components.nbAdvtConf.Newsbytes_InImage')]))
            <div class="{{$advt_unit_name}} {{config('web-components.nbAdvtConf.Newsbytes_InImage')}} dp-fx fx-js-ct lt-init rt-init wd-full">
                @include('web-components::advt.embed', ['data' => ['adType' => $advt_unit_name, 'slot' => config('web-components.nbAdvtConf.Newsbytes_InImage')], 'type' => '', 'fireat' => '', 'firedelay' => 500, 'smart' => false, 'position' => 3])
            </div>
            @endif
        </figure>
        @else
        <div class="mg-t-16 dp-fx fx-al-ct fx-row cl-dove-grey {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">
            <div class="dp-fx fx-al-ct fx-row wd-full">
                @if(!(isset($partner) || !empty($partner)) && !empty(array_get($data, 'creators', [])))
                    @foreach($data['creators'] as $item)
                    <div class="dp-fx fx-al-ct tx-decoration-none mg-r-6 {{($loop->index > 0) ? 'mg-l-6' : ''}}">
                        <span class="md-ter-reg-1 ter-reg-1 tx-ct ft-ter">{!! $item['subtitle'] !!}</span>
                        <a href="{!! $item['url'] !!}" target="_blank"><span class="md-ter-reg-1 ter-reg-1 mg-l-4 tx-ct ft-ter"><u>{!! $item['title'] !!}</u></span></a>
                    </div>
                    <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                    @endforeach
                @endif
                <div class="dp-fx fx-js-bw {{(isset($partnerLogo) && $partnerLogo) ? 'wd-full':''}}">
                    <div class="dp-fx fx-al-ct">
                        @if(isset($partner) && !empty($partner))
                        <div class="dp-fx fx-row ft-ter md-ter-bd-1 ter-bd-1">{!! $data['tag']!!}</div>
                        <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                        @endif
                        @if(isset($data['creatorSnapshot']) && !empty($data['creatorSnapshot']))
                        <div class="dp-fx fx-al-ct fx-js-ct ft-ter ter-reg-1">
                            @if(count(explode('.', $data['creatorSnapshot']['timestamp'])) > 1)
                                {!! head(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                                @if(!(isset($partner) || !empty($partner)))
                                <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                                {!! last(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                                @endif
                            @else
                                {!! head(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                                @if(!(isset($partner) || !empty($partner)))
                                <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                                {!! last(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                                @endif
                            @endif
                        </div>
                        @endif
                    </div>
                    @if(isset($partnerLogo) && $partnerLogo)
                        @if(isset($partnerLogo['includeSvg']))
                            <div class="svg-inclusion partner-logo" data-svg="{{$partnerLogo['includeSvg']}}" aria-label="{!! $partnerLogo['title'] !!}"><span class="hidden">{!! $partnerLogo['title'] !!}</span></div>
                        @else
                            <img src="{!! $partnerLogo['src'] !!}" alt="{!! $partnerLogo['title'] !!}" class="partner-logo" />
                        @endif
                    @endif
                </div>
            </div>
        </div>
        @endif
    @endif

    @if(! (isset($partner) && !empty($partner)) && !(isset($hideCoverImg) && $hideCoverImg) || !empty($data['video']['src']))
    <div class="mg-t-10 dp-fx fx-al-ct fx-row cl-dove-grey {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">
        <div class="mg-b-6 dp-fx fx-al-ct fx-row wd-full">
            @if(!(isset($partner) || !empty($partner)) && !empty(array_get($data, 'creators', [])))
                @foreach($data['creators'] as $item)
                <div class="dp-fx fx-al-ct tx-decoration-none mg-r-6 {{($loop->index > 0) ? 'mg-l-6' : ''}}">
                    <span class="md-ter-reg-1 ter-reg-1 tx-ct ft-ter">{!! $item['subtitle'] !!}</span>
                    <a href="{!! $item['url'] !!}" target="_blank"><span class="md-ter-reg-1 ter-reg-1 mg-l-4 tx-ct ft-ter"><u>{!! $item['title'] !!}</u></span></a>
                </div>
                <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                @endforeach
            @endif
            <div class="dp-fx fx-js-bw {{(isset($partnerLogo) && $partnerLogo) ? 'wd-full':''}}">
                <div class="dp-fx fx-al-ct">
                    @if((isset($partner) && !empty($partner)))
                    <div class="dp-fx fx-row ft-ter md-ter-bd-1 ter-bd-1">{!! $data['tag']!!}</div>
                    <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                    @endif
                    @if(isset($data['creatorSnapshot']) && !empty($data['creatorSnapshot']))
                    <div class="dp-fx fx-al-ct fx-js-ct ft-ter ter-reg-1">
                        @if(count(explode('.', $data['creatorSnapshot']['timestamp'])) > 1)
                            {!! head(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                            @if(!(isset($partner) || !empty($partner)))
                            <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                            {!! last(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                            @endif
                        @else
                            {!! head(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                            @if(!(isset($partner) || !empty($partner)))
                            <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
                            {!! last(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                            @endif
                        @endif
                    </div>
                    @endif
                </div>
                @if(isset($partnerLogo) && $partnerLogo)
                    @if(isset($partnerLogo['includeSvg']))
                        <div class="svg-inclusion partner-logo" data-svg="{{$partnerLogo['includeSvg']}}" aria-label="{!! $partnerLogo['title'] !!}"><span class="hidden">{!! $partnerLogo['title'] !!}</span></div>
                    @else
                        <img src="{!! $partnerLogo['src'] !!}" alt="{!! $partnerLogo['title'] !!}" class="partner-logo" />
                    @endif
                @endif
            </div>
        </div>
    </div>
    @endif

    <div class="bg-ink-dk asp-ratio r-16-9"  style="background: linear-gradient(to bottom, #00000000, #000000a0);">
        <div></div>
        <div class="{{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}} pos-abs bt-init cl-primary">
            @if(!(isset($hideCoverImg) && $hideCoverImg))
                @if(!(isset($data['imageFallback']) && $data['imageFallback']))
                @if((isset($partner) && !empty($partner)))
                <div class="tag-container dp-fx fx-al-ct mg-b-6 pd-t-6 pd-b-6 ft-sec md-sec-reg-2 sec-reg-2">
                    <div class="dp-fx fx-row ft-ter md-ter-bd-1 ter-bd-1">{!! $data['tag']!!}</div>
                    <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-primary opacity-50"></div>
                    @if(isset($data['creatorSnapshot']) && !empty($data['creatorSnapshot']))
                    <div class="dp-fx fx-al-ct fx-js-ct ft-ter ter-reg-1">
                        @if(count(explode('.', $data['creatorSnapshot']['timestamp'])) > 1)
                            {!! head(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                            @if(!(isset($partner) || !empty($partner)))
                            <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-primary opacity-50"></div>
                            {!! last(explode('.', $data['creatorSnapshot']['timestamp'])) !!}
                            @endif
                        @else
                            {!! head(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                            @if(!(isset($partner) || !empty($partner)))
                            <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-primary opacity-50"></div>
                            {!! last(explode('|', $data['creatorSnapshot']['timestamp'])) !!}
                            @endif
                        @endif
                    </div>
                    @endif
                </div>
                @endif
                @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
                @include('web-components::v2.entities.imageCredits', ['sum' => ['data' => $data['imagecredit']]])
                @endif
                @endif
            @endif
            <h1 class="cover-card-title ft-sec bd-9 md-bd-8 pd-b-10 sticky-title">{!! $data['title'] !!}</h1>
        </div>
    </div>
    
    @break
    @case('coverCardText')
        <div class="pd-t-16">
            @if(isset($data['subtitle']) && !empty($data['subtitle']))
            <div class="fx-al-ct {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">
                <span class="ft-ter ter-reg-2 cl-venetian-red">{!! $data['subtitle'] !!}</span>
            </div>
            @endif

            @if(!empty(array_get($data, 'html', [])))
            <p class="ft-sec md-sec-reg-4 sec-reg-5 card-point cl-lt {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}} {{$renderOptClass}}">
                @foreach($data['html'] as $key => $item)
                @if(isset($adsData) && !empty($adsData) && ($key == $adsData['atLineIndex']))
            </p>
                <div class="{{$adsData['data']['adType']}} {{$adsData['classList']}}">
                    @include('web-components::advt.embed', $adsData)
                </div>
            <p class="ft-sec md-sec-reg-4 sec-reg-5 card-point cl-lt {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}} {{$renderOptClass}}">
                @endif
                <span class="dp-bl pd-t-10">{!! $item !!}</span>
                @endforeach
            </p>
            @endif

            @if(isset($seperator) && !empty($seperator))
            <div class="dp-fx fx-al-ct fx-js-ct mg-t-16 {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}" id="articleScrollDownButtonParent">
                <div class="br-1p-pale-lavender ht-px-0 fx-grow-1"></div>
                <div class="cs-ptr pd-t-10 pd-l-16 pd-r-16 pd-t-12 pd-b-12 dp-fx fx-al-ct fx-js-bw br-rd-px-6" id="articleScrollDownButton" onclick="scrollToPosition()">
                    <span class="ter-bd-3 dp-fx fx-al-fs cl-acadia">{{$data['readMore']['title']}}</span>
                    {{-- <span class="ter-reg-1 cl-lt dp-fx fx-al-fs mg-t-3">{{$data['readMore']['subtitle']}}</span> --}}
                    <div class="bg-primary br-rd-pr-50 dp-fx fx-js-ct wd-ht-px-40 pd-t-20 mg-l-10 br-1p-solid-smoke">
                        <span class="pos-abs nb-icon-angle-down fz-18 cl-venetian-red slide-down"></span>
                        <span class="pos-abs nb-icon-angle-down fz-18 cl-venetian-red slide-down" style="animation-delay: 0.3s;margin-top: -4px"></span>
                    </div>
                </div>
                <div class="br-1p-pale-lavender ht-px-0 fx-grow-1"></div>
            </div>
            @endif

        </div>
    @break
    @case('eventCard')
        @switch($data['conf']['cardtype'])
            @case('fact')
            @if(!empty(array_get($data, 'traits.type', '')))
                @if($data['traits']['type'] == 'scorecard')
                @if(array_get($data, 'traits.sport', 'cricket') == 'tennis')
                <div class="pd-t-10 pd-l-16 pd-r-16 card-border {{$renderOptClass}}">
                    <div class="pd-t-30 pd-b-30 pd-l-16 pd-r-16 ft-pr br-rd-20 cl-primary solid-tint-san-juan">
                        @if($data['subtitle'] != '')
                            @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                            <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                            @endif
                                <div class="dp-fx fx-al-ct">
                                    <div class="cl-primary ft-pr md-bd-7 bd-7">{!! $data['subtitle'] !!}</div>
                                </div>
                            @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                            </div>
                            @endif
                        @endif
                        @if(!empty($data['traits']['subtitle']))
                        <div class="mg-t-2 ft-ter md-ter-reg-2 ter-reg-2">{!! $data['traits']['subtitle'] !!}</div>
                        @endif
                        @if(!empty($data['traits']['title']))
                        <div class="ft-ter md-ter-reg-2 ter-reg-2">{!! $data['traits']['title'] !!}</div>
                        @endif
                        @if(!empty($data['traits']['comparison']['items']))
                            @foreach($data['traits']['comparison']['items'] as $item)
                            <div class="dp-fx fx-al-ct fx-js-bw mg-t-20">
                                <div class="dp-fx fx-al-ct">
                                    @if(!empty($item['img']))
                                    <img data-src="{!! $item['img'] !!}" alt="{!! $item['subtitle'] !!}" title="{!! $item['subtitle'] !!}" class="lazy wd-ht-px-40 br-rd-pr-50"/>
                                    @endif
                                    <div class="mg-l-10 {{$item['highlight'] ? '' : 'opacity-8'}}">
                                        <div class="ft-ter md-ter-bd-5 ter-bd-5">{!! $item['title'] !!}</div>
                                        <div class="ft-ter md-ter-reg-1 ter-reg-1">{!! $item['subtitle'] !!}</div>
                                    </div>
                                </div>
                                @if(!empty(array_get($item, 'params', [])))
                                <div class="dp-fx fx-al-ct ft-ter md-ter-bd-5 ter-bd-5 {{$item['highlight'] ? '' : 'opacity-8'}}">
                                    @foreach($item['params'] as $param)
                                    <div class="pd-l-6 pd-r-6 pd-l-md-10 pd-r-md-10">{!! $param['title'] !!}</div>
                                    @endforeach
                                </div>
                                @endif
                            </div>
                            @endforeach
                        @endif
                        <div class="dp-fx fx-js-end">
                            <div class="collapse-bttn open dp-fx fx-al-ct fx-js-bw br-rd-10 pd-t-14 pd-b-14 pd-l-16 pd-r-16 br-1p-pale-lavender cs-ptr wd-full wd-full-md-dac mg-t-20" data-id="{{$data['htmlId']}}">
                                <div class="ft-ter md-ter-bd-2 ter-bd-2">{!! $data['traits']['buttons'][0]['title'] !!}</div>
                                <div class="mg-l-20 nb-icon-angle-down fz-20 rotate-neg-90"></div>
                            </div>
                        </div>
                        @if(!empty($data['traits']['list']))
                        <div id="collapsible-{{$data['htmlId']}}" class="ht-px-0 ovr-hidden">
                            <div class="sc-stats-separator mg-t-20"></div>
                            @foreach($data['traits']['list'] as $item)
                            @if($loop->first)
                            <div class="dp-fx fx-js-bw fx-al-ct mg-t-20">
                                @foreach($item['data'] as $dItem)
                                @if(($loop->index % 2) == 1)
                                @if(!empty($item['title']))
                                <div class="ft-ter md-ter-reg-3 ter-reg-3 tx-ct">{!! $item['title'] !!}</div>
                                @endif
                                @endif
                                <div class="ft-ter md-ter-bd-2 ter-bd-2 {{($loop->index % 2) == 0 ? 'tx-lt' : 'tx-rt'}}">{!! $dItem['title'] !!}</div>
                                @endforeach
                            </div>
                            @else
                            <div class="dp-fx fx-js-bw fx-al-ct mg-t-20">
                                @foreach($item['data'] as $dItem)
                                @if(($loop->index % 2) == 1)
                                @if(!empty($item['title']))
                                <div class="ft-ter md-ter-reg-3 ter-reg-3 tx-ct">{!! $item['title'] !!}</div>
                                @endif
                                @endif
                                <div class="ft-ter md-ter-bd-5 ter-bd-5 {{($loop->index % 2) == 0 ? 'tx-lt' : 'tx-rt'}}">{!! $dItem['title'] !!}</div>
                                @endforeach
                            </div>
                            @endif
                            @endforeach
                        </div>
                        @endif
                        <div class="dp-fx fx-js-end">
                            <div class="collapse-bttn close dp-fx fx-al-ct fx-js-bw br-rd-10 pd-t-14 pd-b-14 pd-l-16 pd-r-16 br-1p-pale-lavender cs-ptr wd-full wd-full-md-dac hidden mg-t-20" data-id="{{$data['htmlId']}}">
                                <div class="ft-ter md-ter-bd-2 ter-bd-2">{!! $data['traits']['buttons'][1]['title'] !!}</div>
                                <div class="mg-l-20 nb-icon-angle-down fz-20 rotate-180"></div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="pd-l-16 pd-r-16 card-border {{$renderOptClass}}">
                    <div class="pd-t-20 pd-b-20 pd-l-16 pd-r-16 ft-pr br-rd-20 cl-primary solid-tint-san-juan">
                        @if($data['subtitle'] != '')
                            @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                            <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                            @endif
                                <div class="dp-fx fx-al-ct">
                                    <div class="cl-primary ft-ter md-ter-bd-2 ter-bd-2">{!! $data['subtitle'] !!}</div>
                                </div>
                            @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                            </div>
                            @endif
                        @endif
                        @if(!empty($data['traits']['title']))
                        <div class="tx-ct mg-t-10 ft-ter md-ter-reg-2 ter-reg-2">{!! $data['traits']['title'] !!}</div>
                        @endif
                        @if(!empty($data['traits']['subtitle']))
                        <div class="tx-ct ft-ter md-ter-reg-2 ter-reg-2">{!! $data['traits']['subtitle'] !!}</div>
                        @endif
                        @if(!empty($data['traits']['comparison']['items']))
                        <div class="fx-row mg-t-10 fx-js-bw">
                            @foreach($data['traits']['comparison']['items'] as $item)
                            <div class="dp-fx">
                                @if(!empty($item['img']) && (($loop->index % 2) == 0))
                                <div class="dp-fx fx-dr-col fx-al-ct mg-r-10 mg-r-md-20">
                                    <img data-src="{!! $item['img'] !!}" alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="lazy wd-ht-px-20 br-rd-pr-50 sc-comp-img"/>
                                    <div class="ft-ter md-ter-bd-6 ter-bd-6 mg-t-16">{!! $item['title'] !!}</div>
                                </div>
                                @endif
                                <div>
                                    <div class="ft-ter md-ter-bd-3 ter-bd-3 {{(($loop->index % 2) == 0) ? 'tx-rt' : 'tx-lt'}}">{!! $item['params']['title'] !!}</div>
                                    <div class="ft-ter md-ter-reg-2 ter-reg-2 {{(($loop->index % 2) == 0) ? 'tx-rt' : 'tx-lt'}}">{!! $item['params']['subtitle'] !!}</div>
                                </div>
                                @if(!empty($item['img']) && (($loop->index % 2) != 0))
                                <div class="dp-fx fx-dr-col fx-al-ct mg-l-10 mg-l-md-20">
                                    <img data-src="{!! $item['img'] !!}" alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="lazy wd-ht-px-20 br-rd-pr-50 sc-comp-img"/>
                                    <div class="ft-ter md-ter-bd-6 ter-bd-6 mg-t-16">{!! $item['title'] !!}</div>
                                </div>
                                @endif
                            </div>
                            @if(($loop->index % 2) == 0)
                            <div class="ft-ter md-ter-bd-2 ter-bd-2 mg-t-16">{!! $data['traits']['comparison']['connector'] !!}</div>
                            @endif
                            @endforeach
                        </div>
                        @endif
                        @if(!empty($data['traits']['conclusion']))
                        <div class="ft-ter md-ter-reg-3 ter-reg-3 mg-t-20 tx-ct">{!! $data['traits']['conclusion'] !!}</div>
                        @endif
                        @if(!empty($data['traits']['list']))
                            @foreach($data['traits']['list'] as $item)
                            <div class="ft-ter md-ter-bd-4 ter-bd-4 mg-t-20 tx-ct">{!! $item['title'] !!}</div>
                            <div class="fx-row neg-mg-lr-px-10-ac">
                                @foreach($item['data'] as $lItem)
                                <div class="mg-t-6 col-6 dp-fx fx-al-ct pd-l-10 pd-r-10 {{(($loop->index % 2) == 0) ? '' : 'fx-js-end'}}">
                                    @if(!empty($lItem['img']) && (($loop->index % 2) == 0))
                                    <img data-src="{!! $lItem['img'] !!}" alt="{!! $lItem['title'] !!}" title="{!! $lItem['title'] !!}" class="lazy wd-ht-px-20 br-rd-pr-50 mg-r-10"/>
                                    @endif
                                    <div class="ft-ter md-ter-reg-2 ter-reg-2 dp-fx fx-dr-col {{(($loop->index % 2) == 0) ? '' : 'fx-al-end'}}">
                                        <div>{!! $lItem['title'] !!}</div>
                                        <div>{!! $lItem['subtitle'] !!}</div>
                                    </div>
                                    @if(!empty($lItem['img']) && (($loop->index % 2) != 0))
                                    <img data-src="{!! $lItem['img'] !!}" alt="{!! $lItem['title'] !!}" title="{!! $lItem['title'] !!}" class="lazy wd-ht-px-20 br-rd-pr-50 mg-l-10"/>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                            @endforeach
                        @endif
                        @if(!empty($data['traits']['card']))
                            <div class="ft-ter md-ter-bd-4 ter-bd-4 mg-t-20">{!! $data['traits']['card']['title'] !!}</div>
                            @foreach($data['traits']['card']['text'] as $item)
                            <div class="mg-t-10 ft-ter md-ter-reg-3 ter-reg-3">{!! $item !!}</div>
                            @endforeach
                        @endif
                        @if(!empty($data['traits']['links']))
                            <div class="fx-row neg-mg-lr-px-10-ac">
                                @foreach($data['traits']['links'] as $item)
                                <div class="pd-l-10 pd-r-10 col-12 mg-t-30 {{(sizeof($data['traits']['links']) > 1) ? 'col-md-6' : ''}}">
                                    <a class="dp-bl wd-full ft-ter md-ter-bd-2 ter-bd-2 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{!! $item['url'] !!}" target="_blank" aria-label="Button Link">
                                        <div>{!! $item['title'] !!}</div>
                                        <span class="nb-icon-external-link fz-20 mg-l-6"></span>
                                    </a>
                                </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                @endif
                @elseif($data['traits']['type'] == 'rating')
                <div class="pd-l-16 pd-r-16 card-border {{$renderOptClass}}">
                    <div class="pd-t-20 pd-b-20 pd-l-16 pd-r-16 ft-pr cl-primary solid-tint-@nbColor($data['tagEn'] . 'tint')">
                        @if($data['subtitle'] != '')
                            @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                            <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                            @endif
                                <div class="dp-fx fx-al-ct">
                                    <div class="cl-primary ft-ter md-ter-bd-3 ter-bd-3">{!! $data['subtitle'] !!}</div>
                                </div>
                            @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                            </div>
                            @endif
                        @endif
                        @if(!empty($data['traits']['title']))
                        <div class="ft-sec md-sec-bd-5 sec-bd-5 mg-t-6 cl-primary">{!! $data['traits']['title'] !!}</div>
                        @endif
                        @if(!empty(array_get($data, 'traits.ratings.keys', [])))
                        <div class="pd-t-10" title="{!! $data['traits']['ratings']['value'] !!}">
                            <div class="cl-primary ft-ter md-ter-reg-2 ter-reg-2">{!! $data['traits']['ratings']['title'] !!}</div>
                            <div class="fx-row">
                                @foreach($data['traits']['ratings']['keys'] as $item)
                                    <div class="svg-inclusion mg-r-4" data-svg="{{$item}}"></div>
                                @endforeach
                            </div>
                        </div>
                        @endif
                        @if(!empty($data['traits']['img']))
                        <div class="dp-fx fx-js-ct mg-t-20">
                            <img data-src="{!! $data['traits']['img'] !!}" alt="{!! $data['traits']['title'] !!}" title="{!! $data['traits']['title'] !!}" class="lazy wd-ht-px-200"/>
                        </div>
                        @endif
                        @if(!empty($data['traits']['card']['text']))
                        @foreach($data['traits']['card']['text'] as $item)
                        <div class="ft-sec md-sec-reg-4 sec-reg-4 cl-primary mg-t-10">{!! $item !!}</div>
                        @endforeach
                        @endif
                        @if(!empty($data['traits']['list']))
                        <div class="fx-row neg-mg-lr-px-10-ac cl-primary">
                            @foreach($data['traits']['list'] as $item)
                            <div class="col-12 col-md-6 pd-l-10 pd-r-10 mg-t-20">
                                <div class="ft-ter md-ter-bd-4 ter-bd-4">{!! $item['title'] !!}</div>
                                @foreach($item['data'] as $dItem)
                                <div class="pos-rel mg-t-10">
                                    <div class="ft-sec md-sec-reg-4 sec-reg-4 pd-t-6 pd-b-6 pd-l-20">{!! $dItem !!}</div>
                                    <div class="gd-tint-white-white0-white100-half-1 pos-abs lt-init bt-init tp-init wd-px-90"></div>
                                </div>
                                @endforeach
                            </div>
                            @endforeach
                        </div>
                        @endif
                        @if(!empty($data['traits']['links']))
                            <div class="fx-row neg-mg-lr-px-10-ac">
                                @foreach($data['traits']['links'] as $item)
                                <div class="pd-l-10 pd-r-10 col-12 mg-t-30 {{(sizeof($data['traits']['links']) > 1) ? 'col-md-6' : ''}}">
                                    <a class="dp-bl wd-full ft-ter md-ter-bd-2 ter-bd-2 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{!! $item['url'] !!}" target="_blank" aria-label="Button Link">
                                        <div>{!! $item['title'] !!}</div>
                                        <span class="nb-icon-external-link fz-20 mg-l-6"></span>
                                    </a>
                                </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                @endif
            @else
                @if(array_get($data, 'tag', '') == 'whydoesitmatter')
                <div class="hidden-slug" data-hooked="{{$data['htmlId']}}"></div>
                <div class="lt-init tp-px-6 pd-r-16 pd-b-6 dp-fx card-border">
                    <div class="bd-solid-venetian-red"></div>
                    <div id="{{$data['htmlId']}}" data-id="{{$data['htmlId']}}" class="cl-venetian-red evt-connector wd-full fx-ht fx-al-ct dp-fx br-r-rd-4 ft-ter ter-reg-2 fx-js-bw pd-l-16">
                        <span>{!! $data['subtitle'] !!}</span>
                    </div>
                </div>
                <h2 id="{!! $data['jumpLinkId'] !!}" data-id="{{$data['htmlId']}}" class="card-border cl-night-rider ft-sec jump-at sec-bd-5 md-sec-bd-4 pd-l-16 pd-r-16 {{$renderOptClass}}">{!! $data['title'] !!}</h2>
                @if(!empty(array_get($data, 'html', [])))
                <p class="pd-l-16 pd-r-16 ft-sec md-sec-reg-4 sec-reg-5 card-point cl-lt card-border {{$renderOptClass}}">
                    @foreach($data['html'] as $item)
                    <span class="dp-bl pd-t-10">{!! $item !!}</span>
                    @endforeach
                </p>
                @endif
                @else
                    @if(!empty($data['subtitle']) || !empty($data['title']))
                    <div class="hidden-slug" data-hooked="{{$data['htmlId']}}"></div>
                    <div class="lt-init tp-px-6 pd-r-16 pd-b-6 dp-fx card-border">
                        <div class="bd-solid-venetian-red"></div>
                        <div id="{{$data['htmlId']}}" data-id="{{$data['htmlId']}}" class="cl-venetian-red evt-connector wd-full fx-ht fx-al-ct dp-fx br-r-rd-4 ft-ter ter-reg-2 fx-js-bw pd-l-16">
                            <span>{!! $data['subtitle'] !!}</span>
                        </div>
                    </div>
                    <h2 id="{!! $data['jumpLinkId'] !!}" data-id="{{$data['htmlId']}}" class="cl-night-rider ft-sec jump-at sec-bd-5 md-sec-bd-4 pd-l-16 pd-r-16 card-border {{$renderOptClass}}">{!! $data['title'] !!}</h2>
                    @endif
                    @if(!empty(array_get($data, 'html', [])))
                    <p class="ft-sec md-sec-reg-4 sec-reg-5 pd-l-16 pd-r-16 card-point cl-lt card-border {{$renderOptClass}}">
                        @foreach($data['html'] as $item)
                        <span class="dp-bl pd-t-10">{!! $item !!}</span>
                        @endforeach
                    </p>
                    @endif
                @endif
            @endif
            @break
            @default
                @if(isset($index) && $index == 1)
                <div class="pd-t-16">
                    @if(isset($data['subtitle']) && !empty($data['subtitle']))
                    <div class="fx-al-ct {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">
                        <span class="ft-ter ter-reg-2 cl-venetian-red">{!! $data['subtitle'] !!}</span>
                    </div>
                    @endif

                    @if(!empty(array_get($data, 'html', [])))
                    <p class="ft-sec md-sec-reg-4 sec-reg-5 card-point cl-lt {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}} {{$renderOptClass}}">
                        @foreach($data['html'] as $item)
                        <span class="dp-bl pd-t-10">{!! $item !!}</span>
                        @endforeach
                    </p>
                    @endif

                    @if($data['conf']['cardtype'] == 'embed' && isset($data['conf']['type']))
                    <div class="mg-t-20 {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}} {{strtolower($data['conf']['type'])}}-card-embed {{$renderOptClass}}">
                    @include('web-components::v2.embeddedPosts.' . $data['conf']['type'], ['sum' => ['data' => $data['conf']]])
                    </div>
                    @endif

                    @if(isset($partner) && !empty($partner))
                    <div class="dp-fx fx-al-ct fx-js-ct mg-t-16 {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}" id="articleScrollDownButtonParent">
                        <div class="br-1p-pale-lavender ht-px-0 fx-grow-1"></div>
                        <div class="cs-ptr pd-t-10 pd-l-16 pd-r-16 pd-t-12 pd-b-12 dp-fx fx-al-ct fx-js-bw br-rd-px-6" id="articleScrollDownButton" onclick="scrollToPosition()">
                            <span class="ter-bd-3 dp-fx fx-al-fs cl-acadia">{{$data['readMore']['title']}}</span>
                            {{-- <span class="ter-reg-1 cl-lt dp-fx fx-al-fs mg-t-3">{{$data['readMore']['subtitle']}}</span> --}}
                            <div class="bg-primary br-rd-pr-50 dp-fx fx-js-ct wd-ht-px-40 pd-t-20 mg-l-10 br-1p-solid-smoke">
                                <span class="pos-abs nb-icon-angle-down fz-18 cl-venetian-red slide-down"></span>
                                <span class="pos-abs nb-icon-angle-down fz-18 cl-venetian-red slide-down" style="animation-delay: 0.3s;margin-top: -4px"></span>
                            </div>
                        </div>
                        <div class="br-1p-pale-lavender ht-px-0 fx-grow-1"></div>
                    </div>
                    @endif

                </div>
                @else
                    @if(!empty($data['subtitle']) || !empty($data['title']))
                    <div class="hidden-slug" data-hooked="{{$data['htmlId']}}"></div>
                    <div class="lt-init tp-px-6 pd-r-16 pd-b-6 dp-fx card-border">
                        <div class="bd-solid-venetian-red"></div>
                        <div id="{{$data['htmlId']}}" data-id="{{$data['htmlId']}}" class="cl-venetian-red evt-connector wd-full fx-ht fx-al-ct dp-fx br-r-rd-4 ft-ter ter-reg-2 fx-js-bw pd-l-16">
                            <span>{!! $data['subtitle'] !!}</span>
                        </div>
                    </div>
                    <h2 id="{!! $data['jumpLinkId'] !!}" data-id="{{$data['htmlId']}}" class="cl-night-rider ft-sec jump-at sec-bd-5 md-sec-bd-4 pd-l-16 pd-r-16 card-border {{$renderOptClass}}">{!! $data['title'] !!}</h2>
                    @endif

                    @if(!empty($data['title']))
                    @if(!empty(array_get($data, 'slides', [])))
                    <div class="pd-t-10 pd-l-16 pd-r-16 card-border {{$renderOptClass}}">
                        <section class="slide-show-parent visibility-hidden">
                            <section class="slide-show default with-centerMode" id="slideshow-{!! $data['jumpLinkId'] !!}-{{$componentId}}">
                                @foreach($data['slides'] as $item)
                                <figure class="mg-r-20">
                                    <img alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="dp-bl wd-full" src="{{$item['img']}}">
                                    @if(!empty(array_get($item, 'caption', '')))
                                    <figcaption class="cl-lt pd-t-4 ft-sec md-sec-reg-2 sec-reg-2 bg-primary {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">{!! $item['caption'] !!}</figcaption>
                                    @endif
                                </figure>
                                @endforeach
                            </section>
                            <div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
                        </section>
                    </div>
                    {{-- @elseif((!empty($data['ximg'])) && (empty($data['video']['src'])))
                    <figure class="mg-t-10 pd-l-md-18">
                        <picture class="asp-ratio r-16-9">
                            <source
                                media="(max-width: 599px) and (min-device-pixel-ratio: 3),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 3),(max-width: 599px) and (min-resolution: 3dppx),(max-width: 599px) and (min-resolution: 288dpi)"
                                srcset="{{$data['ximg']}}">
                            <source
                                media="(max-width: 599px) and (min-device-pixel-ratio: 2),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 2),(max-width: 599px) and (min-resolution: 2dppx),(max-width: 599px) and (min-resolution: 192dpi)"
                                srcset="{{$data['ximg']}}">
                            <source
                                media="(max-width: 599px) and (min-device-pixel-ratio: 1),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 1),(max-width: 599px) and (min-resolution: 1dppx),(max-width: 599px) and (min-resolution: 96dpi)"
                                srcset="{{$data['img']}}">
                            <img alt="{{$data['title']}}" title="{{$data['title']}}" class="dp-bl wd-full lazy" data-src="{{$data['ximg']}}">
                            @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
                            @include('web-components::v2.entities.imageCredits', ['sum' => ['data' => $data['imagecredit']]])
                            @endif
                        </picture>
                        @if(!empty(array_get($data, 'caption', '')))
                        <figcaption class="cl-lt pd-t-4 pd-l-16 pd-r-16 ft-sec md-sec-reg-2 sec-reg-2 bg-primary {{isset($classList) ? $classList : 'pd-l-10 pd-r-10'}}">{!! $data['caption'] !!}</figcaption>
                        @endif
                    </figure>
                    --}}
                    @endif

                    <?php $data['video'] = array_merge($data['video'], array_map(function ($item) {return $item;}, ['title' => $data['title']])); ?>
                    @if(!empty(array_get($data, 'video.src', '')))
                    <div class="pd-t-20 pd-b-10 pd-l-md-14 card-border {{$renderOptClass}}">
                        @include('web-components::v2.entities.videoTileWithLazyImg', ['sum' => ['data' => $data['video']]])
                    </div>
                    @endif
                    @endif

                    @if(!empty(array_get($data, 'html', [])))
                    <p class="ft-sec md-sec-reg-4 sec-reg-5 pd-l-16 pd-r-16 card-point cl-lt card-border {{$renderOptClass}}">
                        @foreach($data['html'] as $item)
                        <span class="dp-bl pd-t-10">{!! $item !!}</span>
                        @endforeach
                    </p>
                    @endif

                    @if($data['conf']['cardtype'] == 'embed' && isset($data['conf']['type']))
                    <div class="pd-t-20 pd-l-16 pd-r-16 card-border pd-b-10 embedded-links {{strtolower($data['conf']['type'])}}-card-embed {{$renderOptClass}}">
                    @include('web-components::v2.embeddedPosts.' . $data['conf']['type'], ['sum' => ['data' => $data['conf']]])
                    </div>
                    @endif
                @endif
            @break
        @endswitch
    @break
@endswitch