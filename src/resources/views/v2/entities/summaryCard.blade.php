<div class="dp-fx fx-al-ct fx-js-ct pd-l-14 pd-r-14 pd-t-14 pd-b-14 br-rd-px-10 cl-primary bx-shdw wd-max-cont ht-px-50 sticky-button simplify opacity-8 cs-ptr pe-none" onclick="handleSimplifyClick()">
    <span class="fz-22 dp-fx fx-al-ct fx-js-ct">
        <svg width="20" height="20" viewBox="0 0 24 24" fill="none" class="simplify-icon">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M12 21.5a9.5 9.5 0 1 0 0-19 9.5 9.5 0 0 0 0 19zm0 2.5c6.627 0 12-5.373 12-12S18.627 0 12 0 0 5.373 0 12s5.373 12 12 12z" fill="url(#awj716x0sa)"/>
            <defs>
                <linearGradient id="awj716x0sa" x1="20.5" y1="2.5" x2="4" y2="21" gradientUnits="userSpaceOnUse">
                    <stop stop-color="#24B0C7"/>
                    <stop offset=".5" stop-color="#FCFEFC"/>
                    <stop offset="1" stop-color="#F937DA"/>
                </linearGradient>
            </defs>
        </svg>
    </span>
    <span class="ft-ter ter-reg-2 sticky-button-title">{!!$data['button']['title']!!}</span>
</div>

<div class="hidden tp-init summary-card-parent" data-id="{{ $htmlId }}">
    <div class="z-index-2 solid-tint-black-black60 backdrop-blur-px-5 tp-init lt-init wd-vw-100 ht-vh-100 pos-fix close-summary" data-id="{{ $htmlId }}" onclick="handleCloseSummaryClick()"></div>
    <div class="pos-fix lt-init rt-init tp-init bt-init mg-lr-auto mg-tb-auto z-index-6 ht-max-content wd-px-360 summary-card bg-primary pd-t-16 pd-b-16 br-rd-10">
        <div class="pd-l-14 pd-r-14">
            <div class="dp-fx fx-js-bw fx-al-ct mg-b-4 gd-lr-tint-red-white-red100-white0 pd-l-8 br-rd-6">
                <div class="dp-fx fx-js-ct fx-al-ct">
                    <span class="fz-18 dp-fx fx-al-ct fx-js-ct">
                        <svg class="wd-px-18" fill="none" viewBox="0 0 17 16" xmlns="http://www.w3.org/2000/svg">
                            <path d="m11.273 1.4592c0.1488 0.04005 0.2649 0.15621 0.305 0.30492 0.1148 0.42637 0.7197 0.42637 0.8345 0 0.04-0.14871 0.1562-0.26487 0.3049-0.30492 0.4264-0.11479 0.4264-0.71972 0-0.83452-0.1487-0.04004-0.2649-0.1562-0.3049-0.30492-0.1148-0.42637-0.7197-0.42637-0.8345 0-0.0401 0.14871-0.1562 0.26488-0.305 0.30492-0.4263 0.1148-0.4263 0.71973 0 0.83452zm-5.9443 7.8664c0.68828 0.18532 1.2259 0.72294 1.4112 1.4112l0.98495 3.6582c0.13283 0.4933 0.83278 0.4933 0.96562 0l0.98495-3.6582c0.18532-0.6883 0.72292-1.2259 1.4112-1.4112l3.6582-0.98496c0.4933-0.13283 0.4933-0.83278 0-0.96561l-3.6582-0.98495c-0.6883-0.18532-1.2259-0.72296-1.4112-1.4112l-0.98495-3.6582c-0.13284-0.49334-0.83279-0.49334-0.96562 0l-0.98495 3.6582c-0.18532 0.68828-0.72296 1.2259-1.4112 1.4112l-3.6582 0.98495c-0.49334 0.13283-0.49334 0.83278 0 0.96561l3.6582 0.98496zm7.4909 5.4251c0.2974 0.08 0.5298 0.3124 0.6098 0.6098 0.2296 0.8527 1.4395 0.8527 1.6691 0 0.0801-0.2974 0.3124-0.5298 0.6098-0.6098 0.8528-0.2296 0.8528-1.4395 0-1.6691-0.2974-0.0801-0.5297-0.3124-0.6098-0.6098-0.2296-0.8527-1.4395-0.8527-1.6691 0-0.08 0.2974-0.3124 0.5297-0.6098 0.6098-0.8527 0.2296-0.8527 1.4395 0 1.6691zm-11.987-1.3863c-0.053387-0.1983-0.20827-0.3532-0.40655-0.4065-0.56849-0.1531-0.56849-0.9597 0-1.1127 0.19828-0.0534 0.35317-0.2083 0.40655-0.4066 0.15306-0.5685 0.95963-0.5685 1.1127 0 0.05339 0.1983 0.20827 0.3532 0.40655 0.4066 0.56849 0.153 0.56849 0.9596 0 1.1127-0.19828 0.0533-0.35316 0.2082-0.40655 0.4065-0.15307 0.5685-0.95964 0.5685-1.1127 0z" clip-rule="evenodd" fill="url(#a)" fill-rule="evenodd"/>
                            <defs>
                            <linearGradient id="a" x1="13.5" x2="-4.736e-7" y1=".5" y2="18.5" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#C7091B" offset=".26752"/>
                            <stop stop-color="#064893" offset=".55417"/>
                            </linearGradient>
                            </defs>
                        </svg>
                    </span>
                    <span class="simplify-loading-title ft-ter ter-reg-3 ter-reg-bd-3 pd-l-12 pd-r-12 pd-t-12 pd-b-12 br-rd-6">{!! $data['subtitle'] !!}</span></span>
                    <span class="simplify-content-title ft-ter ter-reg-3 ter-reg-bd-3 pd-l-12 pd-r-12 pd-t-12 pd-b-12 br-rd-6 hidden">{!! explode(" ", $data['title'])[0] !!}<span class="mg-l-6 cl-brand">{!! explode(" ", $data['title'])[1] !!}</span></span>
                </div>
                <span class="fz-18 nb-icon-cancel cs-ptr" onclick="handleCloseSummaryClick()"></span>
            </div>

            <ul class="simplify-content hide-native-scrollbar hidden">
                <div class="pd-l-16">
                    @foreach($data['list'] as $item)
                        @if(!empty($item['title']))
                        <span class="cl-venetian-red evt-connector fx-al-ct dp-fx wd-full ft-ter ter-reg-2">{!! $item['title'] !!}</span>
                        @endif
                        @foreach($item['description'] as $point)
                        <li class="pd-t-16">
                            <div class="ft-sec sec-reg-4 line-height-4">{!! $point !!}</div>
                        </li>
                        @endforeach
                    @endforeach
                </div>

                @if(isset($partner_ads) && $partner_ads && !(isset($sponsored) && $sponsored) && (isset($partner) && !empty($partner)) && isset($defineSlots[config('web-components.nbAdvtConf.Newsbytes_InArticleMedium_Mid3')]))
                <div class="{{$advt_unit_name}} ft-ter ter-reg-1 cl-dove-grey tx-ct mg-t-24 pd-t-6 pd-b-6">ADVERTISEMENT</div>
                <div class="{{$advt_unit_name}} dp-fx fx-js-ct fx-al-ct lt-init rt-init wd-full pd-b-10 fx-dr-col simplify-advt">
                    @include('web-components::advt.embed', ['data' => ['adType' => $advt_unit_name, 'slot' => config('web-components.nbAdvtConf.Newsbytes_InArticleMedium_Mid3')], 'type' => '', 'fireat' => '', 'firedelay' => 500, 'smart' => false, 'position' => 'simplify', 'loadEvent' => 'summaryCardAd'])
                </div>
                @endif
            </ul>

            <div class="simplify-loading dp-fx fx-al-ct fx-js-ct">
                <div class="ft-ter ter-reg-2 mg-t-30">{!! $data['description'] !!}</div>
            </div>
        </div>

        <div class="br-t-1p-grey wd-full ht-px-0 pd-b-14"></div>

        @if(isset($next['data']['url']))
        <div class="pd-l-14 pd-r-14">
            <a href="{!! isset($next['data']['url']) ? $next['data']['url'] : '' !!}" title="{!! isset($next['data']['title']) ? $next['data']['title'] : '' !!}" tag="{!! isset($next['data']['tag']) ? $next['data']['tag'] : '' !!}" class="dp-fx fx-js-ct fx-al-ct wd-full br-rd-6 pd-t-14 pd-b-10 bg-ink-dk opacity-8 cl-primary">
                <span class="ft-ter ter-reg-2 sticky-button-title">{!!$next['title']!!}</span>
                <span class="arrow-right fz-22 dp-fx fx-al-ct fx-js-ct mg-l-6"></span>
            </a>
        </div>
        @endif
    </div>
</div>

@section('pageScript')
@parent
const expandSimplifyTimer15 = window.setTimeout(() => {
	document.getElementsByClassName('simplify')[0].classList.add('expanded');
}, 15000);

const expandSimplifyTimer3 = window.setTimeout(() => {
	document.getElementsByClassName('simplify')[0].classList.add('expanded');
}, 3000);

window.addEventListener("jsuseraction", function(event) {
    console.log("jsuseraction");
    clearTimeout(expandSimplifyTimer3);
});


function handleSimplifyClick(){
	document.getElementsByClassName('summary-card-parent')[0].classList.remove('hidden');
    document.getElementsByTagName("html")[0].classList.add('touch-action-none');
    document.getElementsByTagName("html")[0].classList.add('ovr-hidden');
    if(! (window.hasFired && window.hasFired.includes(this.id))){
        document.getElementsByClassName('simplify-loading')[0].appendChild(
            document.getElementsByClassName('page-loader-svg')[0]
        );

        window.setTimeout(() => {
            document.getElementsByClassName('simplify-loading-title')[0].classList.toggle('hidden');
            document.getElementsByClassName('simplify-content-title')[0].classList.toggle('hidden');
            document.getElementsByClassName('simplify-loading')[0].classList.toggle('hidden');
            document.getElementsByClassName('simplify-content')[0].classList.toggle('hidden');
        }, 400);
        
        let url = document.getElementsByClassName('events-list-holder')[0].getAttribute('data-url');
        let tag = document.getElementsByClassName('events-list-holder')[0].getAttribute('data-tag');
        let title = document.getElementsByClassName('events-list-holder')[0].getAttribute('data-title');

        window.dispatchEvent(summaryCardAd);
        if(! window.hasFired){
            window.hasFired = [this.is];
        }else{
            window.hasFired.push(this.id);
        }
    }
    
}
function handleCloseSummaryClick(){
	document.getElementsByClassName('summary-card-parent')[0].classList.add('hidden');
    document.getElementsByTagName("html")[0].classList.remove('touch-action-none');
    document.getElementsByTagName("html")[0].classList.remove('ovr-hidden');
}
@stop