<?php $data = $sum['data']; ?>
<div class="askQuestion {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
    <div class="ft-pr post-box">
        @if(isset($data['includeSvg']))
        <span class="svg-inclusion userIcon" data-svg="{{$data['includeSvg']}}" data-fallback-icon="{{$data['icon']}}"></span>
        @else
        <img src="{{$data['icon']}}" class="lazy userIcon" data-fallback-icon="{{$data['icon']}}" alt="Fallback Icon" title="Fallback Icon"/>
        @endif
        <div class="br-1p-pale-lavender t-box">
            <div class="fx-al-end dp-fx">
                <textarea class="mg-r-10 wd-full hide-native-scrollbar ft-pr md-reg-1 reg-1 placeholder-query-packet userQuestion" data-min-rows='1' data-max="180" rows="1" aria-label="User Question Box" placeholder="{!! $data['placeholder'] !!}"></textarea>
                <span class="nb-icon-post cl-link p-icon submitQuestion" data-timelineId="{{$data['params']['static']['timelineId']}}" data-api="{{$data['api']}}"></span>
            </div>
            <div class="dp-fx mg-t-10 fx-js-end">
                <div class="cl-lt reg-2 loader"></div>
            </div>
        </div>
    </div>
    <div class="flashMessage cl-link md-bd-6 bd-6 f-mssg"></div>
</div>