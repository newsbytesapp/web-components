<?php
    $data = array_get($sum, 'data', []);
?>
@if(sizeof($data) > 0)
    <div class="bg-primary pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac br-rd-10 pd-t-20 pd-b-20 pd-l-md-20 pd-r-md-20 {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
        <div class="fx-row">
            <span class="cl-lt reg-3 md-reg-3">{!! $data['subtitle'] !!}</span>
            <span class="mg-l-4 bd-6 md-bd-6">{!! $data['timestamp'] !!}</span>
        </div>
        @foreach($data['creators'] as $item)
        <div class="hz-scroll-ac hide-native-scrollbar">
            <div class="dp-ib">
                <div class="dp-fx mg-t-10 fx-al-ct">
                    <a href="{!! $item['url'] !!}" class="dp-fx fx-al-st">
                        @switch($sum['extraParams']['pageType'])
                        @case('amp')
                        <amp-img src="{{$item['icon']}}" alt="{{$item['title']}}" width="36" height="36" layout="fixed" class="br-rd-pr-50"></amp-img>
                        @break
                        @default
                        <img data-src="{{$item['icon']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy br-rd-pr-50 wd-ht-px-36"/>
                        @break
                        @endswitch
                        <div class="pd-l-10">
                            <div class="cl-lt reg-3 md-reg-3">{!! $item['subtitle'] !!}</div>
                            <div class="bd-6 md-bd-6">{!! $item['title'] !!}</div>
                        </div>
                    </a>
                    @if(sizeof(array_get($item, 'profiles', [])) > 0)
                        <div class="dp-fx mg-l-10">
                            @foreach($item['profiles'] as $pItem)
                            <a target="_blank" href="{!! $pItem['url'] !!}" rel="nofollow noreferrer" aria-label="Link to {{$pItem['key']}} profile" class="dp-fx wd-ht-px-50 fx-al-ct mg-l-10">
                                <span class="hidden">{{ucfirst($pItem['key'])}}</span>
                                <div class="nb-icon-{{$pItem['key']}} fz-14"></div>
                            </a>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endif