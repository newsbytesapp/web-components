<div class="n-bell notifBell hidden ft-pr {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
    <div class="wrapper-sm-variant">
        <section class="fx-row fx-al-st">
            <main class="col-12 col-lg-8 pos-rel">
                <div class="allowNotif b-holder">
                    <div src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/images/notif-bell.png" alt="Notif Bell Icon" class="icon bellIcon"></div>
                </div>
            </main>
        </section>
    </div>
</div>
