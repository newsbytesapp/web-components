<?php 
    $data = $sum['data'];
    $reactionManager = json_encode($data['reactions']['master']);
?>
@section('pageScript')
@parent
<script type="text/javascript">
    var reactionManager = JSON.parse(JSON.stringify({!! $reactionManager !!}));
</script>
@stop
@section('css')
@parent
<style type="text/css">
.sticker-sprite {background-image: url('{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/images/emotion-stickers.2.png');background-repeat:no-repeat;image-rendering: -webkit-optimize-contrast;image-rendering: -moz-optimize-contrast;image-rendering: -o-optimize-contrast;}
.sticker-sprite-props{background-size:40px auto;height:40px;width:40px}.sticker-sprite-props-lg{background-size:100px auto;height:100px;width:100px}.reaction-sticker[data-reaction=enraged]{background-position:0 -320px}.reaction-sticker[data-reaction=pride]{background-position:0 -360px}.reaction-sticker[data-reaction=disgust]{background-position:0 -400px}.reaction-sticker[data-reaction=enlightened]{background-position:0 -440px}.reaction-sticker[data-reaction=sowhat]{background-position:0 -480px}.reaction-sticker[data-reaction=amused]{background-position:0 -520px}.reaction-sticker[data-reaction=weird]{background-position:0 -560px}.reaction-sticker[data-reaction=empathy]{background-position:0 -600px}.active.reaction-sticker[data-reaction=enraged]{background-position:0 -0}.active.reaction-sticker[data-reaction=pride]{background-position:0 -40px}.active.reaction-sticker[data-reaction=disgust]{background-position:0 -80px}.active.reaction-sticker[data-reaction=enlightened]{background-position:0 -120px}.active.reaction-sticker[data-reaction=sowhat]{background-position:0 -160px}.active.reaction-sticker[data-reaction=amused]{background-position:0 -200px}.active.reaction-sticker[data-reaction=weird]{background-position:0 -240px}.active.reaction-sticker[data-reaction=empathy]{background-position:0 -280px}.active.reaction-sticker-lg[data-reaction=enraged]{background-position:0 -0}.active.reaction-sticker-lg[data-reaction=pride]{background-position:0 -100px}.active.reaction-sticker-lg[data-reaction=disgust]{background-position:0 -200px}.active.reaction-sticker-lg[data-reaction=enlightened]{background-position:0 -300px}.active.reaction-sticker-lg[data-reaction=sowhat]{background-position:0 -400px}.active.reaction-sticker-lg[data-reaction=amused]{background-position:0 -500px}.active.reaction-sticker-lg[data-reaction=weird]{background-position:0 -600px}.active.reaction-sticker-lg[data-reaction=empathy]{background-position:0 -700px}
</style>
@stop
<div class="ent-slide-up reaction-widget ft-pr r-con {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
    <div class="dp-fx fx-js-bw fx-al-ct reaction-initiator stack-item stack-item-1">
        <div class="dp-fx">
            @foreach($data['reactions']['list']['data'] as $key => $item)
            <div class="cs-ptr active sticker-sprite sticker-sprite-props reaction-select reaction-sticker {{$loop->first ? '' : 'mg-l-24'}}" data-reaction="{{$item}}"></div>
            @endforeach
        </div>
        <div class="dp-fx cl-link fx-al-ct bottom-share cs-ptr">
            <span class="fz-24 mg-t-8 nb-icon-social-share mg-t-3 bottom-share"></span>
            <span class="mg-l-6 bd-6 bottom-share">Share</span>
        </div>
    </div>
    <div class="cs-ptr tx-rt cl-link md-bd-6 bd-6 reaction-cancel hidden stack-item stack-item-2 mg-t-10">Cancel</div>
    <div class="mg-t-30 hidden stack-item stack-item-3">
        <div class="dp-fx fx-js-ct">
            <span class="active sticker-lg-version sticker-sprite sticker-sprite-props-lg reaction-sticker-lg ent-slide-up animation-dur-scale-2" data-reaction=""></span>
        </div>
        <p class="mg-t-10 tx-ct md-bd-3 bd-3 sticker-text ent-slide-up animation-dur-scale-2"></p>
        <p class="mg-t-4 mg-b-20 tx-ct md-reg-2 reg-2 cl-lt ent-slide-up animation-dur-scale-2">{!! $data['reactions']['subtitle'] !!}</p>
    </div>
    <div class="dp-fx fx-js-ct fx-al-end hidden stack-item stack-item-4">
        <a class="modify-url r-lk nb-icon-facebook ent-slide-up animation-dur-scale-1" target="_blank" data-url="https://www.facebook.com/sharer/sharer.php?u=<url>" href="https://www.facebook.com/sharer/sharer.php?u={{$data['socialShare']['url']}}" rel="nofollow noreferrer" aria-label="Facebook"><span class="hidden">Facebook</span></a>
        <a class="modify-url r-lk nb-icon-whatsapp ent-slide-up animation-dur-scale-2 mg-l-10 hidden-md" target="_blank" data-url="whatsapp://send?text=<text>%20<url>" href="whatsapp://send?text={{rawurlencode($data['socialShare']['text'])}}%20{{$data['socialShare']['url']}}" rel="nofollow noreferrer" aria-label="Whatsapp" data-share-text="{{rawurlencode($data['socialShare']['text'])}}"><span class="hidden">Whatsapp</span></a>
        <a class="modify-url r-lk nb-icon-twitter ent-slide-up animation-dur-scale-3 mg-l-10 has-sharable-txt" target="_blank" data-url="https://twitter.com/intent/tweet?via=newsbytesapp&text=<text>&url=<url>" href="https://twitter.com/intent/tweet?via=newsbytesapp&text={{rawurlencode($data['socialShare']['text'])}}&url={{$data['socialShare']['url']}}" rel="nofollow noreferrer" aria-label="Twitter" data-share-text="{{rawurlencode($data['socialShare']['text'])}}"><span class="hidden">Twitter</span></a>
        <a class="modify-url r-lk nb-icon-linkedin ent-slide-up animation-dur-scale-4 mg-l-10" target="_blank" data-url="https://www.linkedin.com/shareArticle?mini=true&source=newsbytes&url=<url>" href="https://www.linkedin.com/shareArticle?mini=true&source=newsbytes&url={{$data['socialShare']['url']}}" rel="nofollow noreferrer" aria-label="Linkedin"><span class="hidden">Linkedin</span></a>
        <div class="mg-l-10 hidden">
            <div class="cl-lt md-reg-3 reg-3 link-flag visibility-hidden ent-slide-up animation-dur-scale-1">Copied</div>
            <button class="r-lk nb-icon-link-pwa cs-ptr ent-slide-up animation-dur-scale-5 copy-link" aria-label="Copy link to clipboard"></button>
            <input type="text" value="{{$data['socialShare']['url']}}" data-url="<url>" class="modify-url pos-abs tp-init link-val" style="left: -9999px;" readonly aria-label="Link copied" />
        </div>
    </div>
</div>