@php
$renderOptClass = array_get($data, 'render_optimized', false) ? 'nbrh' : '';
@endphp
@if((!empty($data['options'])) && (!empty($data['subtitle']) || !empty($data['title'])))
    <div class="hidden-slug card-border" data-hooked="{{$data['htmlId']}}"></div>
    <div class="bg-primary lt-init tp-px-6 pd-r-16 pd-b-6 dp-fx card-border">
        <div class="bd-solid-venetian-red"></div>
        <div id="{{$data['htmlId']}}" data-id="{{$data['htmlId']}}" class="cl-venetian-red evt-connector wd-full fx-ht fx-al-ct dp-fx br-r-rd-4 ft-ter ter-reg-2 fx-js-bw pd-l-16">
            <span>{!! $data['subtitle'] !!}</span>
        </div>
    </div>
    <div class="tx-decoration-none card-border {{$renderOptClass}}" data-id="{{$data['htmlId']}}">
        <div class="pd-l-16 pd-r-16">
            <div class="ft-sec md-sec-bd-5 sec-bd-5">{!! $data['title'] !!}</div>
        </div>
    </div>
    <div class="pd-l-16 pd-r-16 pd-b-10 options-event-parent card-border {{$renderOptClass}}" data-apiurl="{!! $data['apiUrl'] !!}" data-id="{{$data['htmlId']}}" {{!empty($data['startsAt']) ? 'data-start=' . $data['startsAt'] : ''}} {{!empty($data['endsAt']) ? 'data-end=' . $data['endsAt'] : ''}}>
        <div class="dp-fx fx-dr-col ft-ter md-ter-bd-3 ter-bd-3 options-list">
            @foreach($data['options'] as $key => $item)
                <div class="pd-t-14 pd-b-14 pd-l-16 pd-r-16 dp-fx wd-dull fx-js-bw fx-al-ct mg-t-10 br-1p-pale-lavender cl-link br-rd-5 cs-ptr pos-rel poll-card-option" data-id="{!! $item['id'] !!}" data-score="{!! $item['score'] !!}" data-pollid="{!! $data['pollId'] !!}" data-choice="{!! $key !!}">
                    <div class="pos-abs lt-init ht-full tp-init bt-init progress-bar br-rd-5"></div>
                    <div class="pd-r-10 z-index-1 title" data-classlist="md-ter-reg-3 ter-reg-3 cl-ink-dk">{!! $item['title'] !!}</div>
                    <div class="pd-l-10 z-index-1 cl-ink-dk hidden subtitle md-ter-reg-3 ter-reg-3">{!! $item['subtitle'] !!}</div>
                </div>
            @endforeach
        </div>
        <div class="mg-t-10 cl-suva-grey ft-ter md-ter-reg-3 ter-reg-3 hidden poll-result">{!! $data['pollResults']['title'] !!}</div>
        @foreach($data['options'] as $item)
            @if(!empty($item['event']['title']))
            <div class="br-1p-pale-lavender event hidden mg-t-20 pd-b-10 pd-l-16 pd-r-16 pd-t-10" data-id="{!! $item['id'] !!}">
                @if(!empty($item['auxText']))
                <div class="cl-link ft-ter md-ter-reg-3 ter-reg-3">{!! $item['auxText'] !!}</div>
                @endif
                <a href="{!! $item['event']['url'] !!}" class="dp-fx wd-full mg-t-6 fx-al-ct" target="_blank" title="{!! $item['event']['title'] !!}">
                    <div class="fx-grow-1 pd-r-10 dp-fx">
                        <div class="col-4 col-lg-1 fig-container--wd-ht-lg-60">
                            <figure class="fig fig--bg-img-ct-cov-props ht-full-dac ht-full-lg-ac asp-ratio-rect-ac asp-ratio-rect-lg-dac" style="background-image:url({{$item['event']['thumb']}})" title="{{$item['event']['title']}}">
                            </figure>
                        </div>
                        <div class="col-8 col-lg-10 pd-l-10 ft-pr bd-3 md-bd-3">
                            <h3 class="ft-ter ter-reg-3 md-bd-4 bd-4">{!! $item['event']['title'] !!}</h3>
                        </div>
                    </div>
                    <div class="pd-l-10">
                        <span class="fz-20 nb-icon-external-link"></span>
                    </div>
                </a>
            </div>
            @endif
        @endforeach
        <div class="hidden-poll-card-ad-slug hidden"></div>
    </div>
@endif