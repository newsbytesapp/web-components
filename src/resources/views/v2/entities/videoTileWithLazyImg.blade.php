<?php $data = $sum['data']; ?>
<div class="wd-full pos-rel asp-ratio-rect-ac v-obj {{isset($classList) ? $classList : ''}}">
    @if(array_get($data, 'id'))
        <div class="pos-abs tp-init lt-init rt-init wd-full ht-full cs-ptr play-video embed-in-place" data-video="{{$data['src']}}">
            <div class="asp-ratio r-16-9">
                <img alt="{!! $data['title'] !!}" title="{!! $data['title'] !!}" class="lazy wd-full dp-bl" data-src="{{$data['img']}}">
                <div class="pos-abs tp-init lt-init rt-init bt-init solid-tint-black-black40"></div>
                <div class="cl-primary nb-icon-video-play-button tf-x-y-middle lt-half tp-half pos-abs fz-50"></div>
            </div>
        </div>
    @else
        <iframe src="{{$data['src']}}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen title="YouTube Frame"></iframe>
    @endif
    @if(isset($partner_ads) && $partner_ads && !(isset($sponsored) && $sponsored) && (isset($partner) && !empty($partner)) && isset($defineSlots[config('web-components.nbAdvtConf.Newsbytes_InImage')]))
    <div class="lt-init rt-init wd-full z-index-3">
        <div class="{{$advt_unit_name}} {{config('web-components.nbAdvtConf.Newsbytes_InImage')}} dp-fx fx-js-ct">
            @include('web-components::advt.embed', ['data' => ['adType' => $advt_unit_name, 'slot' => config('web-components.nbAdvtConf.Newsbytes_InImage')], 'type' => '', 'fireat' => '', 'firedelay' => 500, 'smart' => false, 'position' => 3])
        </div>
    </div>
    @endif
</div>