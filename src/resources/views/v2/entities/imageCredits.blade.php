@if(sizeof(array_get($sum, 'data', [])) > 0)
<?php $data = $sum['data']; ?>
<a class="pos-abs tx-decoration-none image-credits" href="{!! $data['url'] !!}" target="_blank" rel="nofollow noopener">
    <div class="dp-fx fx-al-ct">
        <div class="cl-primary ft-ter md-ter-reg-1 ter-reg-1 mg-r-4">{!! $data['title'] !!}</div>
        <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 -2 17.52 16" height="10"><defs></defs><circle class="svg" cx="8.76" cy="8.26" r="2.87"/><path class="svg" d="M15.78,2.52H13.5L13,1a1.48,1.48,0,0,0-1.41-1H6.39A1.48,1.48,0,0,0,5,1L4.5,2.52H2.22a2,2,0,0,0-2,2V12a2,2,0,0,0,2,2H15.78a2,2,0,0,0,2-2V4.49A2,2,0,0,0,15.78,2.52ZM9,12.32a4.06,4.06,0,1,1,4.06-4.06A4.06,4.06,0,0,1,9,12.32Z" transform="translate(-0.24)"/></svg>
    </div>
</a>
@endif