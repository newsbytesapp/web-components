<?php $data = $sum['data']; ?>
<div class="s-popup hidden subscribePopup ft-pr {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
    <div class="wrapper-sm-variant">
        <section class="neg-mg-lr-px-10-lg-ac fx-row fx-al-st">
            <main class="col-12 col-lg-8 pd-l-lg-10 pd-r-lg-10">
                <div class="s-box pop-up-box ent-slide-up animation-dur-scale-4">
                    <div class="i-holder cross-icon cancelNotif">
                        <span class="cl-lt nb-icon-cancel-thin fz-22"></span>
                    </div>
                    <div class="dp-fx fx-js-bw fx-al-ct">
                        <div>
                            <p class="ft-pr md-bd-6 bd-6">{!! $data['title'] !!}</p>
                            <p class="cl-lt ft-ter md-ter-reg-1 ter-reg-1">{!! $data['subtitle'] !!}</p>
                            <button class="b-tt ft-ter md-ter-bd-2 ter-bd-2 allowNotif">{!! $data['button']['title'] !!}</button>
                        </div>
                        <img data-src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/images/categories/{{strtolower($data['tag'])}}.png" alt="{{$data['tag']}} Thumbnail" class="wd-ht-px-140 animation-dur-scale-4 image"/>
                    </div>
                </div>
            </main>
            <aside class="col-12 col-lg-4 pd-l-lg-10 pd-r-lg-10"></aside>
        </section>
    </div>
</div>