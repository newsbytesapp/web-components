<?php
    $data = array_get($downloadBox, 'data', []);
?>
@if(sizeof($data) > 0)
    <div class="bg-primary pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac {{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
        <div class="box">
            <div>
                <p class="ft-pr md-bd-3 bd-3">{!! $data['title'] !!}<br>{!! $data['subtitle'] !!}</p>
                <div class="mg-t-30">
                    @include('web-components::links.link', ['link' => $data['link'], 'classList' => ''])
                </div>
            </div>
            <img class="lazy ht-px-200" data-src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/images/download-app-newsbytes.2.png" alt="{{$data['title']}} Thumb" title="{{$data['title']}} Thumb"/>
        </div>
    </div>
@endif