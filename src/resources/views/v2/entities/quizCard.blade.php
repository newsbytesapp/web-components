@php
$renderOptClass = array_get($data, 'render_optimized', false) ? 'nbrh' : '';
@endphp
@if(!empty($data['options']))
    <div class="pos-sticky z-index-1 rt-px-160 bt-px-90 wd-ht-0 ht-px-36 neg-mg-t-36 hidden-md take-quiz cs-ptr {{$renderOptClass}}" data-quizid="{!! $data['quizId'] . '-' . $data['htmlId'] !!}">
        <div class="br-rd-15 cl-primary dp-fx fx-al-ct fx-js-ct ht-px-60 pd-l-10 pd-r-10 pd-t-10 share-link solid-tint-bright-red wd-px-150">
            <span class="fz-24 mg-r-12 nb-icon-quiz"></span>
            <span class="ft-ter pd-b-10 fz-14">{{$data['title']}}</span>
        </div>
    </div>
    {{-- <div class="quiz-siblings svgs hidden">
        <div class="svg correct">
            <div class="bg-accept-green br-rd-pr-50">
                <span class="nb-icon-accept fz-18 cl-primary">
            </div>
        </div>
        <div class="svg incorrect">
            <div class="bg-incorrect-red br-rd-pr-50">
                <span class="nb-icon-accept fz-18 cl-primary">
            </div>
        </div>
    </div> --}}
    <div class="quiz-siblings svgs hidden">
        <div class="svg correct">
            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="mg-l-6 mg-r-6">
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="quiz-end" transform="translate(-249.000000, -281.000000)">
                        <g id="Question-1" transform="translate(164.000000, 100.000000)">
                            <g id="Option-1" transform="translate(20.000000, 141.000000)">
                                <g id="Correct-answer" transform="translate(65.000000, 40.000000)">
                                    <circle id="Oval" fill="#30D158" cx="12" cy="12" r="12"></circle>
                                    <g id="check-good-yes" transform="translate(8.000000, 9.000000)" stroke="#FFFFFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                        <polyline id="Path" points="0 3 2.66666667 6 8 0"></polyline>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        </div>
        <div class="svg incorrect">
            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="mg-l-6 mg-r-6">
                <title>Group 2</title>
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="quiz-end" transform="translate(-293.000000, -281.000000)">
                        <g id="Question-1" transform="translate(164.000000, 100.000000)">
                            <g id="Option-1" transform="translate(20.000000, 141.000000)">
                                <g id="Group-3" transform="translate(65.000000, 40.000000)">
                                    <g id="Group-2" transform="translate(44.000000, 0.000000)">
                                        <circle id="Oval" fill="#FF453A" cx="12" cy="12" r="12"></circle>
                                        <path d="M15,9 L12,12 M12,12 L9,15 M12,12 L9,9 M12,12 L15,15" id="Shape" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        </div>
    </div>
    <div class="hidden tp-init quiz-card-parent wd-ht-0 {{$renderOptClass}}" data-id="{{$data['htmlId']}}" data-quizid="{!! $data['quizId'] . '-' . $data['htmlId'] !!}" {{!empty($data['startsAt']) ? 'data-start=' . $data['startsAt'] : ''}} {{!empty($data['endsAt']) ? 'data-end=' . $data['endsAt'] : ''}}>
        <div class="z-index-2 solid-tint-black-black60 backdrop-blur-px-5 tp-init lt-init wd-vw-100 ht-vh-100 pos-fix close-quiz" data-quizid="{!! $data['quizId'] . '-' . $data['htmlId'] !!}"></div>
        <div class="pos-fix lt-pr-50-px-180 tp-pr-50-px-250 z-index-2">
            <div class="pos-rel wd-px-360 ht-px-500 bg-body pd-l-30 pd-r-30 br-rd-14" data-quizid="{!! $data['quizId'] . '-' . $data['htmlId'] !!}">
                <div class="hidden-quiz-card-ad-slug hidden"></div>
                @foreach($data['options']['questions'] as $key => $item)
                <div class="pd-t-20 pd-b-20 slide question ht-full ht-full-px-50" data-id="{!! $item['id'] !!}">
                    <div class="dp-fx ovr-scroll hide-scrollbar ht-full">
                        <div class="quiz-card-intermediary pos-abs lt-half hidden">
                            <div class="ft-ter md-ter-bd-5 ter-bd-5 tx-ct">{!! $item['auxText'] !!}</div>
                        </div>
                        <div class="final {{$loop->first ? '' : 'hidden'}}">
                            <div class="mg-t-6 mg-b-10 ft-ter md-ter-bd-1 ter-bd-1 cl-brand">{!! strtoupper($item['subtitle']) !!}</div>
                            <div class="dp-fx wd-full mg-t-6 mg-b-10 fx-al-ct">
                                <div class="br-rd-3 wd-full bg-spark-red-lt">
                                    <div class="br-rd-3 pd-b-6 bg-brand" style="width: {!! ($key + 1) * 100/count($data['options']['questions']) !!}%"></div>
                                </div>
                                <div class="pd-l-8 ft-ter md-ter-bd-1 ter-bd-1">{!! ($key + 1) . "/" .count($data['options']['questions']) !!}</div>
                            </div>
                            <div class="ft-ter md-ter-bd-5 ter-bd-5 mg-t-20">{!! $item['title'] !!}</div>
                            <div class="dp-fx fx-dr-col ft-ter md-ter-bd-3 ter-bd-3 mg-t-40 answers-list">
                                <?php
                                    $index = 'a';
                                ?>
                                @foreach($item['options'] as $ansKey => $ans)
                                    <div class="pd-t-14 pd-b-14 pd-l-20 pd-r-20 dp-fx wd-dull fx-js-bw fx-al-ct mg-t-10 br-1p-pale-lavender br-rd-5 cs-ptr pos-rel quiz-card-option bg-primary" data-id="{!! $ans['id'] !!}" data-answer="{!! $ans['answer'] !!}" data-quizid="{!! $data['quizId'] . '-' . $data['htmlId'] !!}">
                                        <div class="pos-abs lt-init ht-full tp-init bt-init progress-bar br-rd-5"></div>
                                        <div class="dp-fx fx-al-ct fx-js-bw wd-full">
                                            <div class="dp-fx">
                                                <span class="z-index-1 cl-brand ft-ter md-ter-reg-2 ter-reg-2 pd-r-10 index">{!! $index++ . '.' !!}</span>
                                                <div class="pd-r-10 z-index-1 title">{!! $ans['title'] !!}</div>
                                            </div>
                                            <div class="dp-fx fx-al-ct">
                                                {{-- <div class="pd-l-10 pd-r-10 z-index-1 cl-ink-dk hidden subtitle md-ter-reg-3 ter-reg-3">{!! $ans['subtitle'] !!}</div> --}}
                                                <div class="z-index-1 icon-embed hidden"></div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="slide result ht-full ht-full-px-50 pos-rel" data-id="{!! $data['options']['results']['id'] !!}">
                    <div class="quiz-card-intermediary pos-abs lt-half hidden">
                        <div class="ft-ter md-ter-bd-5 ter-bd-5 tx-ct">{!! $data['options']['results']['auxText'] !!}</div>
                    </div>
                    <div class="final hidden dp-fx fx-dr-col ovr-scroll hide-scrollbar ht-full pd-t-20 pd-b-20">
                        <div class="dp-fx fx-js-bw fx-al-ct">
                            <div class="dp-fx fx-al-ct pd-b-20">
                                <div class="mg-r-20">
                                    <div class="results-bar wd-ht-px-50 br-rd-pr-50"></div>
                                </div>
                                <div class="dp-fx fx-dr-col">
                                    <div class="mg-t-6 ft-ter md-ter-bd-1 ter-bd-1 cl-spark-red title">-results-</div>
                                    <div class="mg-t-4 ft-ter md-ter-bd-5 ter-bd-5 subtitle">-remarks-</div>
                                </div>
                            </div>
                            <div class="mg-l-20">
                                <div class="results-timer wd-ht-px-32 br-rd-pr-50 dp-fx fx-al-ct fx-js-ct ft-ter md-ter-bd-1 ter-bd-1 cl-spark-red">10</div>
                            </div>
                        </div>
                        @if(!empty($data['options']['results']['linkWithImg']['title']))
                        <div class="mg-t-40 linkWithImg" data-id="{!! $data['options']['results']['id'] !!}">
                            <a href="{!! $data['options']['results']['linkWithImg']['url'] !!}" class="dp-fx wd-full mg-t-6 fx-al-ct" target="_blank" title="{!! $data['options']['results']['linkWithImg']['title'] !!}">
                                <div class="fx-grow-1 pd-r-10 dp-fx">
                                    <div class="col-4 col-lg-1 fig-container--wd-ht-lg-60">
                                        <figure class="fig fig--bg-img-ct-cov-props ht-full-dac ht-full-lg-ac asp-ratio-sq-ac asp-ratio-sq-lg-dac" style="background-image:url({{$data['options']['results']['linkWithImg']['thumb']}})" title="{{$data['options']['results']['linkWithImg']['title']}}">
                                        </figure>
                                    </div>
                                    <div class="col-8 col-lg-10 pd-l-10 ft-pr bd-3 md-bd-3">
                                        <h3 class="ft-pr md-bd-4 bd-4">{!! $data['options']['results']['linkWithImg']['title'] !!}</h3>
                                    </div>
                                </div>
                                <div class="pd-l-10">
                                    <span class="fz-20 nb-icon-external-link"></span>
                                </div>
                            </a>
                        </div>
                        @endif

                        @foreach($data['options']['questions'] as $key => $item)
                        <div class="dp-fx wd-full mg-t-16">
                            <div class="mg-r-10 pd-t-16 question-result" data-quizid="{!! $data['quizId'] . '-' . $data['htmlId'] !!}"></div>
                            <div class="pd-t-10 pd-l-10 pd-r-10 pd-b-10 bg-primary br-rd-10 wd-full show-answer" data-id="{!! $item['id'] !!}" data-quizid="{!! $data['quizId'] . '-' . $data['htmlId'] !!}">
                                <div class="dp-fx fx-js-bw fx-al-ct cs-ptr">
                                    <div class="">
                                        <div class="ft-ter md-ter-bd-2 ter-bd-2 cl-suva-grey">{!! $item['subtitle'] !!}</div>
                                        <div class="ft-ter md-ter-bd-3 ter-bd-3 mg-t-4">{!! $item['title'] !!}</div>
                                    </div>
                                    <div class="nb-icon-smallright show-answer-icon fz-20"></div>
                                    <div class="nb-icon-down hide-answer-icon fz-20 hidden"></div>
                                </div>
                                <div class="mg-t-10 answer hidden">
                                    <hr class="br-t-1p-white-smoke"/>
                                    @if(isset($item['description']) && !empty($item['description']))
                                    <div class="ft-ter md-ter-reg-3 ter-reg-3 mg-t-10">{!! $item['description'] !!}</div>
                                    @else
                                    @foreach($item['options'] as $ansKey => $ans)
                                    @if($ans['answer'] == 'true')
                                    <div class="ft-ter md-ter-reg-3 ter-reg-3 mg-t-10">{!! $ans['title'] !!}</div>
                                    @endif
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="dp-fx wd-full fx-js-ct pd-t-20 pos-rel">
                <div class="wd-ht-px-50 br-rd-pr-50 bg-ink-dk dp-fx fx-al-ct fx-js-ct close-quiz cs-ptr"  data-quizid="{!! $data['quizId'] . '-' . $data['htmlId'] !!}">
                    <span class="nb-icon-cancel fz-24 cl-primary mg-t-8"></span>
                </div>
            </div>
        </div>
    </div>
@endif