<?php
$data = $sum['data'];
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
<li class="e-cd cv tn-fw cover-card ft-pr">

    <div class="slug">
        <span class="dot bg-@nbColor($data['tag'])-dk evt-connector" id="{{$data['htmlId']}}"></span>
        <span class="cl-lt cl-@nbColor($data['tag'])-dk tt">{!! $data['timestamp'] !!}</span>
    </div>

    <span class="br-l-2p-@nbColor($data['tag']) vt-line v-ln"></span>

    <h1 class="md-bd-1 bd-1 mg-t-6">{!! $data['title'] !!}</h1>

    <div class="fx-row fx-al-ct mg-t-10">
        <p class="md-reg-3 reg-3 cl-lt">{!! $data['creators'][0]['subtitle'] !!}</p>
        <a class="mg-l-4 tx-decoration-none md-bd-6 bd-6" href="{!! $data['creators'][0]['url'] !!}">{!! $data['creators'][0]['title'] !!}</a>
        <div class="bg-lt bg-@nbColor($data['tag'])-dk mg-l-4 mg-r-4 br-rd-pr-50 wd-ht-px-3 fx-basis-3px"></div>
        <h2 class="cl-lt cl-@nbColor($data['tag'])-dk md-bd-6 bd-6">
            @if(empty(array_get($data, 'tagUrl', '')))
            <span>{!! $data['tag'] !!}</span>
            @else
            <a href="{!! $data['tagUrl'] !!}" class="tx-decoration-none">{!! $data['tag'] !!}</a>
            @endif
        </h2>
    </div>

    @if(!empty(array_get($data, 'slides', [])))
    <section class="slide-show-parent visibility-hidden mg-t-20">
        <section class="slide-show default" id="slideshow-{{componentId}}">
            @foreach($data['slides'] as $item)
            <div class="min-wd-px-100 mg-r-20">
                <img alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="dp-bl wd-full br-rd-10" src="{{$item['ximg']}}">
            </div>
            @endforeach
        </section>
        <div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
    </section>
    @elseif((!empty($data['ximg'])) && (empty($data['video']['src'])))
    <div class="pd-b-20 mg-t-20">
        <div class="pos-rel asp-ratio r-16-9">
            <img src="{{$data['ximg']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="br-rd-10 dp-bl wd-full"/>
            @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
            @include('web-components::v2.entities.imageCredits', ['sum' => ['data' => $data['imagecredit'], 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => '', 'cname' => '']]]])
            @endif
        </div>
    </div>
    @endif

    <?php $data['video'] = array_merge($data['video'], array_map(function ($item) {return $item;}, ['title' => $data['title']])); ?>
    @include('web-components::v2.entities.videoTileWithImg', ['sum' => ['data' => $data['video'], 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => 'pd-b-20', 'cname' => '']]]])

</li>
