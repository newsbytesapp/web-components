<?php
$data = $sum['data'];
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
<li class="e-cd cv tn-fw cover-card pd-t-30 ft-pr">

    <div class="slug">
        <span class="dot bg-@nbColor($data['tag'])-dk evt-connector" id="{{$data['htmlId']}}"></span>
        <span class="cl-lt cl-@nbColor($data['tag'])-dk tt">
            @if(empty(array_get($data, 'tagUrl', '')))
            <span>{!! $data['tag'] !!}</span>
            @else
            <a href="{!! $data['tagUrl'] !!}" class="tx-decoration-none">{!! $data['tag'] !!}</a>
            @endif
        </span>
    </div>

    <span class="br-l-2p-@nbColor($data['tag']) vt-line v-ln"></span>

    <h1 class="md-bd-1 bd-1 mg-t-6">{!! $data['title'] !!}</h1>
    @if(!empty(array_get($data, 'creators', [])))
    <div class="mg-t-10">
        <div class="dp-fx fx-al-ct">
            @foreach($data['creators'] as $item)
            <a href="{!! $item['url'] !!}" class="dp-fx fx-al-st tx-decoration-none {{($loop->index > 0) ? 'mg-l-30' : ''}}">
                @if(sizeof($data['creators']) == 1)
                <img src="{{$item['icon']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="br-rd-pr-50 wd-ht-px-36 mg-r-10" />
                @endif
                <div>
                    <div class="cl-lt reg-3 md-reg-3">{!! $item['subtitle'] !!}</div>
                    <div class="bd-6 md-bd-6">{!! $item['title'] !!}</div>
                </div>
            </a>
            @if((sizeof($data['creators']) == 1) && (sizeof(array_get($item, 'profiles', [])) > 0))
            <div class="dp-fx mg-l-10">
                @foreach($item['profiles'] as $pItem)
                <a target="_blank" href="{!! $pItem['url'] !!}" rel="nofollow noreferrer" aria-label="Link to {{$pItem['key']}} profile" class="dp-fx wd-ht-px-50 fx-al-ct mg-l-10 tx-decoration-none">
                    <span class="hidden">{{ucfirst($pItem['key'])}}</span>
                    <div class="nb-icon-{{$pItem['key']}} fz-14"></div>
                </a>
                @endforeach
            </div>
            @endif
            @endforeach
        </div>
    </div>
    @endif
    @if(isset($data['creatorSnapshot']) && !empty($data['creatorSnapshot']))
    <div class="fx-row mg-t-10">
        <span class="cl-lt reg-3 md-reg-3">{!! $data['creatorSnapshot']['subtitle'] !!}</span>
        <span class="mg-l-4 bd-6 md-bd-6">{!! $data['creatorSnapshot']['timestamp'] !!}</span>
    </div>
    @endif

    @if(!empty(array_get($data, 'slides', [])))
    <section class="slide-show-parent visibility-hidden mg-t-10">
        <section class="slide-show default" id="slideshow-{!! $data['htmlId'] !!}-{{str_random(6)}}">
            @foreach($data['slides'] as $item)
            <div class="min-wd-px-100 mg-r-20">
                <img alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="dp-bl wd-full br-rd-10" src="{{$item['ximg']}}">
            </div>
            @endforeach
        </section>
        <div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
    </section>
    @elseif((!empty($data['ximg'])) && (empty($data['video']['src'])))
    <div class="mg-t-10">
        <div class="pos-rel asp-ratio r-16-9">
            <img src="{{$data['ximg']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="br-rd-10 dp-bl wd-full" />
            @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
            @include('web-components::v2.entities.imageCredits', ['sum' => ['data' => $data['imagecredit'], 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => '', 'cname' => '']]]])
            @endif
        </div>
    </div>
    @endif

    <?php $data['video'] = array_merge($data['video'], array_map(function ($item) {
        return $item;
    }, ['title' => $data['title']])); ?>
    @include('web-components::v2.entities.videoTileWithImg', ['sum' => ['data' => $data['video'], 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => 'pd-b-20', 'cname' => '']]]])

</li>