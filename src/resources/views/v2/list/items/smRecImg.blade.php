<?php 
    $data = $sum['data'];
    $target = array_get($sum, 'extraParams.targetAttr', '')
?>
<li class="col-12 col-md-6">
    @if(array_get($sum, 'extraParams.targetAttr', '') != '')
    <a href="{!! $data['url'] !!}" class="fx-row" target="{{$sum['extraParams']['targetAttr']}}">
    @else
    <a href="{!! $data['url'] !!}" class="fx-row">
    @endif
        <div class="col-6">
            <div class="asp-ratio r-16-9">
                <img data-src="{{$data['thumb']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="lazy"/>
            </div>
        </div>
        <div class="col-6 pd-l-10 ft-pr bd-6 md-bd-6">
            <div class="cl-lt">{!! $data['timestamp'] !!}</div>
            <div class="bd-5 md-bd-5 cl-ink-dk tx">{!! $data['title'] !!}</div>
            @if(array_get($sum, 'extraParams.tag', ''))
            <div class="cl-lt cl-@nbColor($data['tag'])-dk tag">{!! $data['tag'] !!}</div>
            @endif
        </div>
    </a>
</li>