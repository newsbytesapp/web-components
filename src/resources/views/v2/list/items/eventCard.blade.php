<?php
$data = $sum['data'];
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
@switch($data['conf']['cardtype'])
    @case('fact')
        @if(!empty(array_get($data, 'traits.type', '')))
            @if($data['traits']['type'] == 'scorecard')
            <li class="e-cd ft-pr pd-t-50 pd-b-40 mg-t-10 br-rd-20 cl-primary solid-tint-@nbColor($data['tag'] . 'tint')">
                @if($data['subtitle'] != '')
                    @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                    <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                    @endif
                        <div class="slug">
                            <span class="dot bg-primary evt-connector" data-hooked="{{$data['htmlId']}}"></span>
                            <div class="cl-primary tt">{!! $data['subtitle'] !!}</div>
                        </div>
                    @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                    </div>
                    @endif
                @endif
                @if(!empty($data['traits']['title']))
                <div class="tx-ct mg-t-10 reg-3 md-reg-3">{!! $data['traits']['title'] !!}</div>
                @endif
                @if(!empty($data['traits']['subtitle']))
                <div class="tx-ct reg-3 md-reg-3">{!! $data['traits']['subtitle'] !!}</div>
                @endif
                @if(!empty($data['traits']['comparison']['items']))
                <div class="fx-row mg-t-10 fx-js-bw">
                    @foreach($data['traits']['comparison']['items'] as $item)
                    <div class="dp-fx">
                        @if(!empty($item['img']) && (($loop->index % 2) == 0))
                        <div class="dp-fx fx-dr-col fx-al-ct mg-r-10 mg-r-md-20">
                            <img data-src="{!! $item['img'] !!}" alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="lazy wd-ht-px-50 br-rd-pr-50"/>
                            <div class="bd-2 md-bd-2 mg-t-6">{!! $item['title'] !!}</div>
                        </div>
                        @endif
                        <div>
                            <div class="bd-5 md-bd-5 {{(($loop->index % 2) == 0) ? 'tx-rt' : 'tx-lt'}}">{!! $item['params']['title'] !!}</div>
                            <div class="reg-3 md-reg-3 {{(($loop->index % 2) == 0) ? 'tx-rt' : 'tx-lt'}}">{!! $item['params']['subtitle'] !!}</div>
                        </div>
                        @if(!empty($item['img']) && (($loop->index % 2) != 0))
                        <div class="dp-fx fx-dr-col fx-al-ct mg-l-10 mg-l-md-20">
                            <img data-src="{!! $item['img'] !!}" alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="lazy wd-ht-px-50 br-rd-pr-50"/>
                            <div class="bd-2 md-bd-2 mg-t-6">{!! $item['title'] !!}</div>
                        </div>
                        @endif
                    </div>
                    @if(($loop->index % 2) == 0)
                    <div class="bd-6 md-bd-6 mg-t-16">{!! $data['traits']['comparison']['connector'] !!}</div>
                    @endif
                    @endforeach
                </div>
                @endif
                @if(!empty($data['traits']['conclusion']))
                <div class="reg-2 mg-t-6 tx-ct">{!! $data['traits']['conclusion'] !!}</div>
                @endif
                @if(!empty($data['traits']['list']))
                    @foreach($data['traits']['list'] as $item)
                    <div class="bd-4 md-bd-4 mg-t-6 tx-ct">{!! $item['title'] !!}</div>
                    <div class="fx-row neg-mg-lr-px-10-ac">
                        @foreach($item['data'] as $lItem)
                        <div class="mg-t-6 col-6 dp-fx fx-al-ct pd-l-10 pd-r-10 {{(($loop->index % 2) == 0) ? '' : 'fx-js-end'}}">
                            @if(!empty($lItem['img']) && (($loop->index % 2) == 0))
                            <img data-src="{!! $lItem['img'] !!}" alt="{!! $lItem['title'] !!}" title="{!! $lItem['title'] !!}" class="lazy wd-ht-px-20 br-rd-pr-50 mg-r-10"/>
                            @endif
                            <div class="reg-2 dp-fx fx-dr-col {{(($loop->index % 2) == 0) ? '' : 'fx-al-end'}}">
                                <div>{!! $lItem['title'] !!}</div>
                                <div>{!! $lItem['subtitle'] !!}</div>
                            </div>
                            @if(!empty($lItem['img']) && (($loop->index % 2) != 0))
                            <img data-src="{!! $lItem['img'] !!}" alt="{!! $lItem['title'] !!}" title="{!! $lItem['title'] !!}" class="lazy wd-ht-px-20 br-rd-pr-50 mg-l-10"/>
                            @endif
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                @endif
                @if(!empty($data['traits']['card']))
                    <div class="bd-4 md-bd-4 mg-t-20">{!! $data['traits']['card']['title'] !!}</div>
                    @foreach($data['traits']['card']['text'] as $item)
                    <div class="mg-t-10 reg-1">{!! $item !!}</div>
                    @endforeach
                @endif
                @if(!empty($data['traits']['links']))
                    <div class="fx-row neg-mg-lr-px-10-ac">
                        @foreach($data['traits']['links'] as $item)
                        <div class="pd-l-10 pd-r-10 col-12 mg-t-30 {{(sizeof($data['traits']['links']) > 1) ? 'col-md-6' : ''}}">
                            <a class="dp-bl wd-full ft-pr bd-6 md-bd-6 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{!! $item['url'] !!}" target="_blank" aria-label="Button Link">
                                <div>{!! $item['title'] !!}</div>
                                <span class="nb-icon-external-link fz-20 mg-l-6"></span>
                            </a>
                        </div>
                        @endforeach
                    </div>
                @endif
            </li>
            @elseif($data['traits']['type'] == 'rating')
            <li class="e-cd ft-pr pd-t-50 pd-b-40 mg-t-10 br-rd-20 solid-tint-@nbColor($data['tag'] . 'tint')">
                @if($data['subtitle'] != '')
                    @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                    <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                    @endif
                        <div class="slug">
                            <span class="dot bg-primary evt-connector" data-hooked="{{$data['htmlId']}}"></span>
                            <div class="cl-primary tt">{!! $data['subtitle'] !!}</div>
                        </div>
                    @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                    </div>
                    @endif
                @endif
                @if(!empty($data['traits']['title']))
                <div class="bd-1 md-bd-2 mg-t-6 cl-primary">{!! $data['traits']['title'] !!}</div>
                @endif
                @if(!empty(array_get($data, 'traits.ratings.keys', [])))
                <div class="pd-t-10" title="{!! $data['traits']['ratings']['value'] !!}">
                    <div class="cl-primary reg-3">{!! $data['traits']['ratings']['title'] !!}</div>
                    <div class="fx-row">
                        @foreach($data['traits']['ratings']['keys'] as $item)
                            <div class="svg-inclusion mg-r-4" data-svg="{{$item}}"></div>
                        @endforeach
                    </div>
                </div>
                @endif
                @if(!empty($data['traits']['img']))
                <div class="dp-fx fx-js-ct mg-t-20">
                    <img data-src="{!! $data['traits']['img'] !!}" alt="{!! $data['traits']['title'] !!}" title="{!! $data['traits']['title'] !!}" class="lazy wd-ht-px-200"/>
                </div>
                @endif
                @if(!empty($data['traits']['card']['text']))
                @foreach($data['traits']['card']['text'] as $item)
                <div class="reg-1 cl-primary mg-t-10">{!! $item !!}</div>
                @endforeach
                @endif
                @if(!empty($data['traits']['list']))
                <div class="fx-row neg-mg-lr-px-10-ac cl-primary">
                    @foreach($data['traits']['list'] as $item)
                    <div class="col-12 col-md-6 pd-l-10 pd-r-10 mg-t-20">
                        <div class="bd-4 md-bd-4">{!! $item['title'] !!}</div>
                        @foreach($item['data'] as $dItem)
                        <div class="pos-rel mg-t-10">
                            <div class="reg-1 pd-t-6 pd-b-6 pd-l-20">{!! $dItem !!}</div>
                            <div class="gd-tint-white-white0-white100-half-1 pos-abs lt-init bt-init tp-init wd-px-90"></div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                </div>
                @endif
                @if(!empty($data['traits']['links']))
                    <div class="fx-row neg-mg-lr-px-10-ac">
                        @foreach($data['traits']['links'] as $item)
                        <div class="pd-l-10 pd-r-10 col-12 mg-t-30 {{(sizeof($data['traits']['links']) > 1) ? 'col-md-6' : ''}}">
                            <a class="dp-bl wd-full ft-pr bd-6 md-bd-6 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{!! $item['url'] !!}" target="_blank" aria-label="Button Link">
                                <div>{!! $item['title'] !!}</div>
                                <span class="nb-icon-external-link fz-20 mg-l-6"></span>
                            </a>
                        </div>
                        @endforeach
                    </div>
                @endif
            </li>
            @endif
        @else
            <li class="e-cd ft-b pd-t-20 ft-pr mg-t-10">
                @if($data['subtitle'] != '')
                @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                @endif
                <div class="slug">
                    <span class="dot bg-@nbColor($data['tag']) evt-connector" data-hooked="{{$data['htmlId']}}"></span>
                    <div class="cl-lt cl-@nbColor($data['tag']) tt">{!! $data['subtitle'] !!}</div>
                </div>
                @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                </div>
                @endif
                @endif
                @if(!empty(array_get($data, 'title', '')))
                <h2 class="c-tl">{!! $data['title'] !!}</h2>
                @endif
            </li>
            <li class="e-cd ft-e ft-pr pd-b-20">
                @foreach($data['html'] as $point)
                    <p class="ft-sec pt">{!! $point !!}</p>
                @endforeach
            </li>
        @endif
    @break
    @default
        @if(($data['subtitle'] != '') || ($data['title'] != '') || ($data['img'] != '') || ($data['video']['src'] != ''))
        <li class="e-cd ev md-asset tn-fw ft-pr pd-t-20 mg-t-10">
            @if($data['subtitle'] != '')
            @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
            <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
            @endif
            <div class="slug">
                <span class="dot bg-@nbColor($data['tag'])-dk evt-connector" data-hooked="{{$data['htmlId']}}"></span>
                <div class="cl-lt cl-@nbColor($data['tag'])-dk tt">{!! $data['subtitle'] !!}</div>
            </div>
            @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
            </div>
            @endif
            @endif

            @if($data['title'] != '')
            <h2 class="c-tl">{!! $data['title'] !!}</h2>
            @endif

            @if(!empty(array_get($data, 'slides', [])))
            <section class="slide-show-parent visibility-hidden mg-t-10">
                <section class="slide-show default" id="slideshow-{!! $data['jumpLinkId'] !!}-{{$componentId}}">
                    @foreach($data['slides'] as $item)
                    <div class="min-wd-px-100 mg-r-20">
                        <img alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="dp-bl wd-full br-rd-10" src="{{$item['ximg']}}">
                    </div>
                    @endforeach
                </section>
                <div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
            </section>
            @elseif((!empty($data['ximg'])) && (empty($data['video']['src'])))
            <div class="asp-ratio r-16-9 mg-t-10">
                <img data-src="{{$data['ximg']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="i-obj lazy"/>
                @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
                @include('web-components::v2.entities.imageCredits', ['sum' => ['data' => $data['imagecredit'], 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => '', 'cname' => '']]]])
                @endif
            </div>
            @endif

            <?php $data['video'] = array_merge($data['video'], array_map(function ($item) {return $item;}, ['title' => $data['title']])); ?>
            @include('web-components::v2.entities.videoTileWithLazyImg', ['sum' => ['data' => $data['video'], 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => '', 'cname' => '']]]])
            
        </li>
        @endif

        @if(sizeof($data['html']) > 0)
            @if($loop->first)
            <li class="e-cd ev ft-sec pd-b-20">
                @foreach($data['html'] as $point)
                    <p class="pt">{!! $point !!}</p>
                @endforeach
            </li>
            @else
            <li class="e-cd ev r-bt ft-sec pd-b-20">
                @foreach($data['html'] as $point)
                <p class="pt">{!! $point !!}</p>
                @endforeach
            </li>
            @endif
        @endif

        @if($data['conf']['cardtype'] == 'embed' && isset($data['conf']['type']))
        <li class="e-cd ev r-bt pd-t-10 pd-b-20">
            @include('web-components::v2.embeddedPosts.' . $data['conf']['type'], ['sum' => ['data' => $data['conf'], 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => '', 'cname' => '']]]])
        </li>
        @endif
    @break
@endswitch