<?php
    $data = $sum['data'];
?>
<li>
    <a class="nb-icon-{{$data['key']}}" href="{!! array_get($data, 'url', array_get($data, 'link', '')) !!}" target="_blank" rel="nofollow noreferrer" aria-label="Link to {{$data['key']}} profile"><span class="hidden">{{ucfirst($data['key'])}}</span></a>
</li>