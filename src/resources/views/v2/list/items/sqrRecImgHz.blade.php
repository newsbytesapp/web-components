<?php 
    $data = $sum['data'];
    $target = array_get($sum, 'extraParams.targetAttr', '')
?>
<li class="col-12 col-md-4">
    @if(array_get($sum, 'extraParams.targetAttr', '') != '')
    <a href="{!! $data['url'] !!}" class="dp-fx" target="{{$sum['extraParams']['targetAttr']}}">
    @else
    <a href="{!! $data['url'] !!}" class="dp-fx">
    @endif
        <div class="col-4 col-lg-1 fig-container--wd-ht-lg-60">
            <figure class="lazy fig fig--bg-img-ct-cov-props ht-full-dac ht-full-lg-ac asp-ratio-rect-ac asp-ratio-rect-lg-dac" data-src="{{$data['thumb']}}" title="{{$data['title']}}">
                <img data-src="{{$data['thumb']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="lazy hidden inset-img-hidden"/>
            </figure>
        </div>
        <div class="col-8 col-lg-10 pd-l-10 ft-pr bd-6 md-bd-6">
            <div>{!! $data['title'] !!}</div>
            @if(array_get($sum, 'extraParams.tag', ''))
            <div class="cl-lt cl-@nbColor($data['tag'])-dk tag">{!! $data['tag'] !!}</div>
            @endif
        </div>
    </a>
</li>