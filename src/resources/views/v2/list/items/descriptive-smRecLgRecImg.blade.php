<?php 
    $data = $sum['data'];
    $target = array_get($sum, 'extraParams.targetAttr', '')
?>
<li class="col-12">
    @if(array_get($sum, 'extraParams.targetAttr', '') != '')
    <a href="{!! $data['url'] !!}" class="fx-row" target="{{$sum['extraParams']['targetAttr']}}">
    @else
    <a href="{!! $data['url'] !!}" class="fx-row">
    @endif
        <div class="col-12 col-md-4">
        @switch($sum['extraParams']['pageType'])
        @case('amp')
        <amp-img src="{{$data['thumb']}}" srcset="{{$data['img']}} 960w, {{$data['ximg']}} 1280w" alt="{{$data['title']}}" width="1080" height="610" layout="responsive"></amp-img>
        @break
        @default
        <div class="asp-ratio r-16-9">
            <img data-src="{{$data['thumb']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="lazy"/>
        </div>
        @endswitch
        </div>
        <div class="ft-pr bd-6 md-bd-6 col-12 col-md-8 pd-l-md-20">
            <div class="fx-row fx-al-ct mg-t-14 mg-t-md-0">
                <div class="cl-lt">{!! $data['timestamp'] !!}</div>
                @if(array_get($sum, 'extraParams.tag', ''))
                <div class="bg-lt bg-@nbColor($data['tag'])-dk mg-l-4 mg-r-4 br-rd-pr-50 wd-ht-px-3 fx-basis-3px"></div>
                <div class="cl-lt cl-@nbColor($data['tag'])-dk tag">{!! $data['tag'] !!}</div>
                @endif
            </div>
            <div class="bd-3 md-bd-3 cl-ink-dk mg-t-4">{!! $data['title'] !!}</div>
            <p class="reg-2 md-reg-2 cl-lt mg-t-4">{!! $data['description'] !!}</p>
        </div>
    </a>
</li>