<?php $data = $sum['data']; ?>
<li class="{{empty($data['icon']) ? '' : 'h-icon'}}">
    <a href="{!! $data['url'] !!}" class="l-con">
        @if(!empty($data['icon']))
        <img src="{{$data['icon']}}" alt="{{$data['title']}}" title="{{$data['title']}}" width="36" height="36" class="br-rd-pr-50"/>
        @endif
        <div class="i-con">
            <p class="md-reg-3 reg-3 cl-lt">{!! $data['subtitle'] !!}</p>
            <p class="md-bd-6 bd-6 dp-ib">{!! $data['title'] !!}</p>
        </div>
    </a>
    @if(sizeof(array_get($data, 'profiles', [])) > 0)
    <div class="icons">
        @foreach($data['profiles'] as $item)
            <a target="_blank" href="{!! $item['url'] !!}" rel="nofollow noreferrer" aria-label="Link to {{$item['key']}} profile">
                <span class="hidden">{{ucfirst($item['key'])}}</span>
                <div class="nb-icon-{{$item['key']}} fz-14" ></div>
            </a>
        @endforeach
    </div>
    @endif
</li>