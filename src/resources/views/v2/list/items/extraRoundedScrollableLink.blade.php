<?php $data = $sum['data']; ?>
<li class="sc-child scr-lk">
    <a href="{!! $data['url'] !!}">
    @if(isset($data['icon']) && (!empty($data['icon'])))
        <img data-src="{{$data['icon']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="lazy"/>
    @endif
        <span class="reg-2 md-reg-2 tt">{!! $data['title'] !!}</span>
    </a>
</li>