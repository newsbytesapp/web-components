<?php $data = $sum['data']; ?>
<li>
    <div class="q-box">
        <img data-src="{{$data['query']['icon']}}" alt="{{$data['query']['title']}}" title="{{$data['query']['title']}}" class="lazy i-holder"/>
        <div class="mg-l-10 ft-pr">
            <p class="md-reg-2 reg-2 dp-ib">{!! $data['query']['title'] !!}</p>
            <span class="cl-lt md-l-10 md-reg-3 reg-3">{!! $data['query']['timestamp'] !!}</span>
            <p class="md-bd-5 bd-5 mg-t-4">{!! $data['query']['text'] !!}</p>
        </div>
    </div>
    <div class="f-box">
        <div class="i-holder">
            <img data-src="{{$data['feedback']['icon']}}" alt="{{$data['feedback']['title']}}" title="{{$data['feedback']['title']}}" class="lazy ht-full wd-full"/>
        </div>
        <div class="mg-l-10 ft-pr">
            <p class="cl-lt md-reg-3 reg-3">{!! $data['feedback']['subtitle'] !!}</p>
            <p class="md-reg-2 reg-2">{!! $data['feedback']['title'] !!}</p>
            <p class="ft-sec sec-md-reg-2 sec-reg-2 mg-t-10">{!! $data['feedback']['text'] !!}</p>
        </div>
    </div>
</li>