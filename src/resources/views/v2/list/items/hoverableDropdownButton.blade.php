<?php
    $data = $sum['data'];
?>
<li class="{{array_get($sum, 'extraParams.classActions.css', '')}}">
    @if(array_get($data, 'selector.linkable', false))
    <a class="sel selector-url {{(array_get($data, 'selected', false)) ? 'selected' : ''}}" href="{!! $data['selector']['url'] !!}">
    @else
    <div class="sel {{(array_get($data, 'selected', false)) ? 'selected' : ''}}">
    @endif
        @if(array_get($data, 'selector.label', '') && (!empty($data['selector']['label'])))
            <div class="ft-ter md-ter-reg-2 ter-reg-2 selector-label">{!! $data['selector']['label'] !!}</div>
        @endif
        @if(array_get($data, 'selector.title', '') && (!empty($data['selector']['title'])))
            <div class="dp-fx">
                <span class="ft-ter md-ter-reg-2 ter-reg-2">{!! $data['selector']['title'] !!}</span>
                <span class="nb-icon-drop-more d-icon"></span>
            </div>
        @endif
    @if(array_get($data, 'selector.linkable', false))
    </a>
    @else
    </div>
    @endif
    @if(sizeof(array_get($data, 'content.data', [])) > 0)
    <div class="hidden box-shdw-drdown-content-box ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-1 ent-slide-down-pd drdown-box d-box {{(array_get($data, 'content.alignment', 'left')) == 'right' ? 'rt-init' : ''}}">
        @foreach($data['content']['data'] as $item)
        <a class="ft-ter md-ter-reg-2 ter-reg-2" href="{!! $item['url'] !!}" {!! (array_get($data, 'linkTargetAttr', '')) == '' ? '' : 'target="_blank"' !!} {!! (array_get($data, 'linkType', 'internal')) == 'external' ? 'rel="_noopener"' : '' !!}>{!! $item['name'] !!}</a>
        @endforeach
    </div>
    @endif
</li>