<?php $data = $sum['data']; ?>
<ul class="slider-init hz-scroll-ac scroll-menu pos-rel hz-scroll-lg-dac" data-sliderconf="variableWidth">
    @foreach($data as $item)
        @include('web-components::v2.list.items.extraRoundedScrollableLink', ['sum' => ['data' => $item, 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => '', 'cname' => '']]]])
    @endforeach
    <span>&nbsp</span>
</ul>