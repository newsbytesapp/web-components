@if(sizeof(array_get($sum, 'data.list.data', [])) > 0)
    <ul class="{{array_get($sum, 'extraParams.classActions.css', '')}} @nbColor($sum['data']['list']['data'][0]['tag'])" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
        @foreach($sum['data']['list']['data'] as $item)
            @include('web-components::v2.list.items.eventCoverCard', ['sum' => ['data' => $item, 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => '', 'cname' => '']]]])
        @endforeach
    </ul>
@endif