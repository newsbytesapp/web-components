@if(sizeof(array_get($sum, 'data', [])) > 0)
    <ul class="{{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
        @foreach($sum['data'] as $item)
            @include('web-components::v2.list.items.hoverableDropdownButton', ['sum' => ['data' => $item, 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => 'ovr-elem', 'cname' => '']]]])
        @endforeach
    </ul>
@endif