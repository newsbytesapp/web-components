@if(sizeof(array_get($sum, 'data.events.list.data', [])) > 0)
    <ul class="{{array_get($sum, 'extraParams.classActions.css', '')}} @nbColor($sum['data']['events']['list']['data'][0]['tag'])" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
        @foreach($sum['data']['events']['list']['data'] as $item)
            @if($loop->index == 1)
            <li>
            @include('web-components::v2.cards.eventsJumpLinksCard', ['sum' => ['data' => array_merge($sum['data']['jumpLinks'], ['htmlId' => $item['htmlId']]), 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => 'mg-t-10', 'cname' => '']]]])
            </li>
            @endif
            @include('web-components::v2.list.items.eventCard', ['sum' => ['data' => $item, 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => '', 'cname' => '']]]])
        @endforeach
    </ul>
@endif