<ul class="{{array_get($sum, 'extraParams.classActions.css', '')}}" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
    @foreach($sum['data'] as $item)
        @include('web-components::v2.list.items.sqrRecImgHz', ['sum' => ['data' => $item, 'extraParams' => array_merge(array_get($sum, 'extraParams', []), ['classActions' => ['css' => '', 'cname' => '']])]])
    @endforeach
</ul>