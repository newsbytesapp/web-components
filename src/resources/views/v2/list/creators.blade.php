@if(sizeof(array_get($sum, 'data', [])) > 0)
    <ul class="{{array_get($sum, 'extraParams.classActions.css', '')}} dp-fx mg-t-20 neg-mg-lr-px-10-ac" data-cname="{{array_get($sum, 'extraParams.classActions.cname', '')}}">
        @foreach($sum['data'] as $item)
            @include('web-components::v2.list.items.creator', ['sum' => ['data' => $item, 'extraParams' => ['pageType' => array_get($sum, 'extraParams.pageType', 'page'), 'classActions' => ['css' => '', 'cname' => '']]]])
        @endforeach
    </ul>
@endif