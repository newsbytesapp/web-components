<?php $data = $sum['data']; ?>
@section('pageScript')
@parent
<script type="text/javascript">var hasFb = true;</script>
@endsection
<div id="fb-root"></div>
<?php 
    switch($data['embedtype']){
        case 'comment':
            echo '<div class="fb-comment-embed" data-href="'.$data['link'].'" data-width="'.$data['width'].'" data-include-parent="'.$data['includeparent'].'"></div>';
        break;
        case 'video':
            echo '<div class="fb-video" data-href="'.$data['link'].'" data-width="'.$data['width'].'" data-show-text="'.$data['showtext'].'"></div>';
        break;
        case 'post':
            echo '<div class="fb-post" data-href="'.$data['link'].'" data-width="'.$data['width'].'" data-show-text="'.$data['showtext'].'"></div>';
        break;
    }
?>