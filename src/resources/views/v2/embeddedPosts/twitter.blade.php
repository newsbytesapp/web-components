<?php $data = $sum['data']; ?>
@if ($data['hideMedia'])
<blockquote class="twitter-tweet" data-cards="hidden" data-lang="en">{!! $data['text'] !!}</blockquote>
@else
<blockquote class="twitter-tweet" data-lang="en">{!! $data['text'] !!}</blockquote>
@endif