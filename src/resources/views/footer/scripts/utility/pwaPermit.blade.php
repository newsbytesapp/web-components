@if(isset($data['pwaAccess']))
    var newWorker;
    document.addEventListener('DOMContentLoaded',function(){
        pwa = [];
        if(typeof window.PWA !== 'undefined'){
            pwa = new window.PWA;
        }
        if ('serviceWorker' in navigator) {
            var deferredPrompt;
            function auditPwaBttn(){
                var element = document.getElementById('install-pwa');
                if (typeof(element) !== 'undefined' && element != null) return true;
                else return false;
            }              
            navigator.serviceWorker.register("{{$data['pwaAccess']}}", {scope:'/'}).then(
                function (reg) {
                    console.log('ServiceWorker registration successful with scope: ', reg.scope);
                    if (reg.installing) {
                        newWorker = reg.installing;
                        console.log('Service worker installing');
                    } else if (reg.waiting) {
                        newWorker = reg.waiting;
                        console.log('Service worker installed & waiting');
                    } else if (reg.active) {
                        newWorker = reg.active;
                        console.log('Service worker active');
                        pwa.push(['init']);
                    }

                    reg.onupdatefound = function(){
                        var installingWorker = reg.installing;
                        installingWorker.onstatechange = function(){
                            switch (installingWorker.state) {
                                case 'installed':
                                    if (navigator.serviceWorker.controller) {
                                        console.log('show the prompt');
                                        pwa.push(['updateNotif']);
                                        newWorkerInstalled();
                                    } else {
                                        console.log('no update available');
                                    }
                                    break;
                            }
                        };

                        function newWorkerInstalled(){
                            setTimeout(function(){
                                pwa.push(['hideUpdateNotif']);
                            }, 10000);
                            $('#reload').click(function(){
                                reg.update();
                                pwa.push(['hideUpdateNotif']);
                                location.reload();
                            });
                            $('#dismiss-app-update').click(function(){
                                pwa.push(['hideUpdateNotif']);
                            });
                        }
                    }
                },
                function (err) {
                    console.error('unsuccessful registration with ', err);
                }
            );
            window.addEventListener('beforeinstallprompt', function (e) {
                e.preventDefault();
                deferredPrompt = e;
                if(auditPwaBttn()){
                    document.getElementById('install-pwa').removeAttribute("style");
                }
            });            
            if(auditPwaBttn()){
                var btnAdd = document.getElementById('install-pwa');
                btnAdd.addEventListener('click', function(e){
                    deferredPrompt.prompt();
                    ga("send", "event", "pwa", "click", window.location.href);
                    deferredPrompt.userChoice
                    .then(function(choiceResult){
                        if (choiceResult.outcome === 'accepted') {
                            console.log('User accepted the add-to-homescreen prompt');
                            ga("send", "event", "pwa", "accepted", window.location.href);
                            btnAdd.parentNode.removeChild(btnAdd);
                        } else {
                            console.log('User dismissed the add-to-homescreen prompt');
                            ga("send", "event", "pwa", "rejected", window.location.href);
                            btnAdd.parentNode.removeChild(btnAdd);
                        }
                        deferredPrompt = null;
                    });
                });
            }
        }
    });
@endif