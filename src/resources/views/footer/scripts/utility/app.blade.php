@include('web-components::footer.scripts.utility.initJs', ['data' => $data])
{{-- <script defer type="text/javascript" src="{{mix('/js/components.js')}}"></script> --}}

	window.scriptloader = function (w, d) {
		this.init(w, d);
	};

	scriptloader.prototype = {
		init: function (w, d) {
			this.w = w;
			this.d = d;
		},

		push: function(i) {
			console.log('Loading via scriptloader: ' + i.s);
			let f = this.d.getElementsByTagName('script')[0], j = this.d.createElement('script');
			if(i.d) {
				j.defer = true;
			} else {
				j.async = true;
			}
			j.src = i.s;
			if(typeof i.o != 'undefined') {
				console.log('defining scriptloader onload and defer: ', i.d)
				if(i.d) {
					j.onload = i.o;
				} else {
					i.o();
				}
			}
			// f.parentNode.insertBefore(j, f);
			this.d.body.appendChild(j)
		},

		seq: function (list, last) {
			console.log('Sequenced in scriptloader: ' + list.length);
			if(list.length == 1) {
				var src = (typeof list[0] == 'string') ? list[0] : list[0].s;
				var defer = (typeof list[0] == 'string') ? true : list[0].d;
				this.push({s: src, d: defer, o: last});
				return;
			}else if(list.length > 1) {
				var l = list.splice(-1), _this = this;
				var src = (typeof l[0] == 'string') ? l[0] : l[0].s;
				var defer = (typeof l[0] == 'string') ? true : l[0].d;
				this.seq(list, function(){_this.push({s: src, d: defer, o: last});});
			}
		}
	}

	window.setjs = function(fn) {
		var sl = new scriptloader(window, document);
		sl.seq([
			"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js",
			"{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/packageMaster.6.js",
			"https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js",
			"https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"
			], fn);
			// ], window.readyFunction);
	}

	var svgInclusion = function () {
		var items = document.getElementsByClassName('svg-inclusion');
		for (var i = 0; i < items.length; i++) {
			var selectSvg = items[i].getAttribute('data-svg');
			var ih = document.getElementById('_svg_' + selectSvg).innerHTML;
			switch (selectSvg) {
				case 'avatar':
					items[i].insertAdjacentHTML('afterend', ih);
					break;
				default:
					items[i].innerHTML = ih;
					break;
			}
		}
	}

	var isIos = function () {
		var iDevices = [
			'iPad Simulator',
			'iPhone Simulator',
			'iPod Simulator',
			'iPad',
			'iPhone',
			'iPod'
		];
		if (!!navigator.platform) {
			while (iDevices.length) {
				if (navigator.platform === iDevices.pop()) { return true && !window.MSStream; }
			}
		}
		return false;
	}

	function detectUserInteraction() {
		document.getElementsByTagName('html')[0].classList.add('js-user-action');
		/*setjs();
		window.dispatchEvent(jsuseraction);*/
		@if(! (isset($partner) && !empty($partner)))
		setjs(function(){onJqueryLoad();window.dispatchEvent(jsuseraction);window.dispatchEvent(load_event);});
		@else
		window.dispatchEvent(jsuseraction);window.dispatchEvent(load_event);
		@endif

		'keydown mouseover touchmove touchstart wheel touchend touchcancel'.split(" ").forEach(function(e){
			window.removeEventListener(e,detectUserInteraction,false);
		});
	}
	function touchHandler(event)
	{
		var touches = event.changedTouches,
			first = touches[0],
			type = "";

		switch(event.type)
		{
		   case "touchstart": type = "mousedown"; break;
		   case "touchmove":  type = "mousemove"; break;        
		   case "touchend":   type = "mouseup"; break;
		   default: return;
		}

		//initMouseEvent(type, canBubble, cancelable, view, clickCount, 
		//           screenX, screenY, clientX, clientY, ctrlKey, 
		//           altKey, shiftKey, metaKey, button, relatedTarget);

		var simulatedEvent = document.createEvent("MouseEvent");
		simulatedEvent.initMouseEvent(type, true, true, window, 1, 
							  first.screenX, first.screenY, 
							  first.clientX, first.clientY, false, 
							  false, false, false, 0/*left*/, null);

		first.target.dispatchEvent(simulatedEvent);
		// event.preventDefault();
	}

	(function() {
		window.addEventListener("load", function(event) {
			svgInclusion();
		});
@if(array_get($data, 'viatouch', true))
@if(array_get($data, 'findtouch', false))
		if ( window.touchSupport ) {
@endif
			if(isIos()) {
				document.addEventListener("touchstart", touchHandler, true);
				document.addEventListener("touchmove", touchHandler, true);
				document.addEventListener("touchend", touchHandler, true);
				document.addEventListener("touchcancel", touchHandler, true);
			}
			'keydown mouseover touchmove touchstart wheel touchend touchcancel'.split(" ").forEach(function(e){
				window.addEventListener(e, detectUserInteraction, false);
			});
@if(array_get($data, 'findtouch', false))
		} else {
			document.getElementsByTagName('html')[0].classList.add('js-user-action');
			@if(! (isset($partner) && !empty($partner)))
			setjs(function(){onJqueryLoad();window.dispatchEvent(jsuseraction);});
			@endif
		}
@endif
@else
		document.getElementsByTagName('html')[0].classList.add('js-user-action');
		setjs(function(){onJqueryLoad();window.dispatchEvent(jsuseraction);});
@endif
	})();

@if(array_get($data, 'allowNavigation', false))
document.addEventListener('DOMContentLoaded', function () {
	window.dataLayer = window.dataLayer || [];
	document.getElementsByClassName('navigation-events')[0]?.addEventListener('click', (e) => {
		e.preventDefault();
		e.stopPropagation();
		let url = e.currentTarget.getAttribute('href');
		let tag = e.currentTarget.getAttribute('tag');
		let title = e.currentTarget.getAttribute('title');
		let curUrl = window.location.origin + window.location.pathname;
		let isNext = e.currentTarget.classList.contains('prev-article-button');
		
		if (url.indexOf('http') !== 0) {
			url = window.location.protocol+'//'+window.location.host+url;
		}
	
		if (typeof url !== 'undefined') {
			let sessionNavigationInfo = sessionStorage?.getItem('sessionNavigationInfo');
			if(sessionNavigationInfo){
				sessionNavigationInfo = JSON.parse(sessionNavigationInfo);
				sessionNavigationInfo.visitedArticleList = sessionNavigationInfo.visitedArticleList || [curUrl];
			}else{
				sessionNavigationInfo = {
					latestCategoryArticleChecked: [],
					visitedArticleList: [],
					sessionExpTimestamp: Date.now() + 15 * 60 * 1000
				}
			}

			if(sessionNavigationInfo.latestCategoryArticleChecked.indexOf(tag.toLowerCase()) == -1 && window.readyFunctionExecuted){
				sessionNavigationInfo.latestCategoryArticleChecked.push(tag.toLowerCase());
			}
	
			if(sessionNavigationInfo.visitedArticleList.indexOf(curUrl) == -1){
				sessionNavigationInfo.visitedArticleList.push(curUrl);
			}
	
			sessionStorage.setItem('sessionNavigationInfo', JSON.stringify(sessionNavigationInfo));

			window.dataLayer.push({
                'event': isNext ? 'article_nav_click_previous' : 'article_nav_click_next',
                'article_name':title,
                'article_category':tag,
                'author_name':undefined,
                'article_id': url,
                'User_ID':undefined
            });

			window.location.href = url;
			{{-- window.setTimeout(function() {
			}, 200); --}}
		}
	})
});
@endif
@include('web-components::footer.scripts.utility.pwaPermit', ['data' => $data])
{{-- @include('web-components::footer.scripts.utility.performanceMetrics', ['data' => $data]) --}}