
	function onJqueryLoad() {
		jQuery.event.special.touchstart = {
			setup: function(_, ns, handle) {
				this.addEventListener("touchstart", handle, {
					passive: !ns.includes("noPreventDefault")
				});
			}
		};
		jQuery.event.special.touchmove = {
			setup: function(_, ns, handle) {
				this.addEventListener("touchmove", handle, {
					passive: !ns.includes("noPreventDefault")
				});
			}
		};
		jQuery.event.special.wheel = {
			setup: function(_, ns, handle) {
				this.addEventListener("wheel", handle, {
					passive: true
				});
			}
		};
		jQuery.event.special.mousewheel = {
			setup: function(_, ns, handle) {
				this.addEventListener("mousewheel", handle, {
					passive: true
				});
			}
		};
	}
{{--<script defer type="text/javascript" src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/jquery.min.js" onload="onJqueryLoad()"></script> --}}
{{--<script defer type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js"></script> --}}
{{-- <script defer src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/OneSignalSDK.js"></script> --}}
{{--<script defer type="text/javascript" src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/packageMaster.6.js"></script> --}}
{{--<script defer type="text/javascript" src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/slick.min.js"></script> --}}
{{-- <script defer type="text/javascript" src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/jq.scrollify.min.js"></script> --}}