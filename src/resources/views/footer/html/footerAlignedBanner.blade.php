<section class="bg-jungle-mist pd-t-20 pd-b-20 pd-t-md-0 pd-b-md-0 mg-t-50" id="footer-aligned-banner">
    <div class="wrapper-full-wd-ac wrapper-full-wd-md-dac {{$pageType == 'amp' ? 'wrapper-full-wd-amp-ac' : ''}}">
        <div class="cl-ink-dk dp-fx fx-al-ct fx-js-bw pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">
            <div class="dp-fx fx-grow-1 fx-dr-col-ac fx-dr-col-md-dac fx-js-ar-md-ac">
                @foreach($footerAlignedBanner['data']['text'] as $item)
                <div class="ft-pr md-bd-4 bd-4 {{$loop->first ? '' : 'mg-t-10'}} mg-t-md-0">{!! $item !!}</div>
                @endforeach
            </div>
            <div class="col-4 col-md-2 ht-px-100">
                @switch($pageType)
                @case('amp')
                <amp-img src="{{$footerAlignedBanner['data']['thumb']}}" alt="Category all illustration" width="300" height="300" layout="responsive"></amp-img>
                @break
                @default
                <figure class="lazy ht-full fig--bg-img-ct-cov-props" data-src="{{$footerAlignedBanner['data']['thumb']}}" alt="Category all illustration" title="Category all illustration"></figure>
                @endswitch
            </div>
        </div>
    </div>
</section>