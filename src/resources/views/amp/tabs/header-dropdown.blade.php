<div class="bg-primary br-rd-0 pd-t-10 pd-b-10 {{isset($classList) ? $classList : ''}}">
    <div>

        <?php
            if(isset($data['selectedTab'])){
                $privileged = $data['selectedTab'] - 1;
            }else{
                $privileged = 0;
            }
        ?>

        <amp-selector class="tabs-selector" role="tablist" on="select:{{$data['id']}}.toggle(index=event.targetOption, value=true)">
        @foreach ($data['tabularSection'] as $item)
            @if($item['tab']['linkable'])
            <a href="{!! $item['tab']['url'] !!}" id="tab-{{$loop->iteration}}-{{$data['id']}}" role="tab" aria-controls="tabpanel-{{$loop->iteration}}-{{$data['id']}}" option="{{$loop->index}}" class="cs-ptr tx-ct cl-lt ft-pr bd-6 pd-t-6 pd-b-16" {{$loop->index == $privileged ? 'selected' : ''}}>{!! $item['tab']['title'] !!}</a>
            @else
            <div id="tab-{{$loop->iteration}}-{{$data['id']}}" role="tab" aria-controls="tabpanel-{{$loop->iteration}}-{{$data['id']}}" option="{{$loop->index}}" class="cs-ptr tx-ct cl-lt ft-pr bd-6 pd-t-6 pd-b-16" {{$loop->index == $privileged ? 'selected' : ''}}>{!! $item['tab']['title'] !!}</div>
            @endif
        @endforeach
        </amp-selector>
        
        <amp-selector id="{{$data['id']}}" class="tabpanels">
            @foreach ($data['tabularSection'] as $item)
            <div id="tabpanel-{{$loop->iteration}}-{{$data['id']}}" role="tabpanel" aria-labelledby="tab-{{$loop->iteration}}-{{$data['id']}}" option {{$loop->index == $privileged ? 'selected' : ''}}>
                <div class="{{$item['content']['header']['classList']}}">

                    <div class="ft-pr bd-4">{!! $item['content']['header']['title'] !!}</div>

                    @if((isset($item['content']['header']['dropdown'])) && isset($item['content']['header']['dropdown']['size']))
                        @include('web-components::dropdowns.stateless',  ['data'=> $item['content']['header']['dropdown'], 'type'=> $type, 'classList' => ''])
                    @endif

                </div>
                <div class="render-{{$loop->iteration}}">
                    <?php
                        $listicalData = $item['content'];
                    ?>
                    @if(isset($listicalData['list']))
                        @if((isset($listicalData['list']['button'])) && (count($listicalData['list']['button']) > 0))

                            @include('web-components::' . $item['component'], ['classList' => '', 'type' => $type, 'data' => $listicalData['list']['data'], 'linkTargetAttr' => $listicalData['list']['linkTargetAttr'], 'button' => $listicalData['list']['button']])

                        @elseif((isset($listicalData['list']['link'])) && (count($listicalData['list']['link']) > 0))

                            @include('web-components::' . $item['component'], ['classList' => '', 'type' => $type, 'data' => $listicalData['list']['data'], 'linkTargetAttr' => $listicalData['list']['linkTargetAttr'], 'link' => $listicalData['list']['link']])

                        @else

                            @include('web-components::' . $item['component'], ['classList' => '', 'type' => $type, 'data' => $listicalData['list']['data'], 'linkTargetAttr' => $listicalData['list']['linkTargetAttr']])

                        @endif
                    @endif
                </div>
            </div>
            @endforeach
        </amp-selector>
        
    </div>
</div>

@section('css')
@parent

amp-selector[role=tablist].tabs-selector {
	display: flex;
}

amp-selector[role=tablist].tabs-selector [role=tab][selected] {
	outline: none;
	border-bottom: 1px solid #000000;
}

amp-selector[role=tablist].tabs-selector {
	display: flex;
}

amp-selector[role=tablist].tabs-selector [role=tab] {
	width: 100%;
	text-align: center;
}

amp-selector.tabpanels [role=tabpanel] {
	display: none;
}

amp-selector.tabpanels [role=tabpanel][selected] {
	outline: none;
	display: block;
}

@stop
