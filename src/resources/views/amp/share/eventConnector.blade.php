<div class="{{isset($classList) ? $classList : ''}}">
    <div class="dp-fx">
        <span class="wd-ht-px-8 br-rd-pr-50 bg-ink-dk bg-lt evt-connector z-index-1 mg-l-10 mg-t-4"></span>
        <div class="mg-l-10">
            <div class="cl-lt cl-ink-dk cl-lt md-bd-6 bd-6">{!! $data['reactions']['title'] !!}</div>
            <div class="dp-fx neg-mg-lr-px-10-ac mg-t-10">
                <amp-social-share type="facebook" data-target="_blank" data-param-url="{{$data['socialShare']['url']}}" class="mg-l-10 mg-r-10 custom-style br-rd-pr-50" width="37" height="37" data-param-app_id="479402082226194"></amp-social-share>
                <amp-social-share type="whatsapp" data-target="_blank" data-param-via="newsbytesapp" data-param-text="{{$data['socialShare']['text']}} {{$data['socialShare']['url']}}" class="mg-l-10 mg-r-10 custom-style br-rd-pr-50" width="37" height="37"></amp-social-share>
                <amp-social-share type="twitter" data-target="_blank" data-param-via="newsbytesapp" data-param-url="{{$data['socialShare']['url']}}" data-param-text="{{$data['socialShare']['text']}}" class="mg-l-10 mg-r-10 custom-style br-rd-pr-50" width="37" height="37"></amp-social-share>
                <amp-social-share type="linkedin" data-target="_blank" data-param-via="newsbytesapp" data-param-url="{{$data['socialShare']['url']}}" class="mg-l-10 mg-r-10 custom-style br-rd-pr-50" width="37" height="37"></amp-social-share>
            </div>
        </div>
    </div>
</div>