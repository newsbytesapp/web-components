<section class="pos-rel {{isset($classList) ? $classList : ''}}">
    <section class="hz-scroll-ac scroll-menu dp-fx">
        @if(count($data['list']['data']) > 0)
            @foreach($data['list']['data'] as $item)
                @include('web-components::entities.capsule', [ 'data' => $item, 'classList' => 'mg-l-6 mg-r-6 pd-t-14 pd-b-14 pd-l-20 pd-r-20 br-1p-ink-dk br-rd-px-25 ft-pr reg-2', 'type' => $type])
            @endforeach
            <span>&nbsp</span>
        @endif

        @if(isset($tint) && $tint)
            <div class="pos-abs gd-tint-white-white0-white100-neg-half lt-init tp-init bt-init col-1"></div>
            <div class="pos-abs gd-tint-white-white0-white100-half rt-init tp-init bt-init col-1"></div>
        @endif
    </section>
</section>