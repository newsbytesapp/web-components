@if(sizeof(array_get($footer, 'links.list.data', [])) > 0)
<footer class="bg-ink-dk pd-b-50 mg-t-50" id="footer">
    <div class="wrapper">
        @if(sizeof(array_get($footer, 'supportingLinks.list.data', [])) > 0)
        <div class="pd-t-32 fx-row neg-mg-lr-px-10-ac">
            @foreach($footer['supportingLinks']['list']['data'] as $slot)
            <div class="pd-l-10 pd-r-10 ft-ter md-ter-reg-2 wd-full ter-reg-2 cl-suva-grey fx-row">
                @foreach($slot as $item)
                <a href="{!! $item['url'] !!}" class="mg-t-18 dp-bl pd-l-10 pd-r-10 min-wd-px-50">{!! $item['title'] !!}</a>
                @endforeach
            </div>
            @endforeach
        </div>
        @endif
        <div class="pd-t-30 fx-row fx-js-bw">
            <div class="fx-row neg-mg-lr-px-10-ac cl-primary pd-t-10">
                @foreach($footer['links']['list']['data'] as $item)
                <?php
                    $rel = '';
                    if (array_get($item, 'type', 'internal') != 'internal') {
                        $rel = 'rel="nofollow noreferrer"';
                    }
                ?>
                <a href="{!! $item['url'] !!}" class="mg-t-10 min-wd-px-50 pd-l-10 pd-r-10 ft-ter md-ter-reg-2 ter-reg-2" target="_blank" {!! $rel !!}>{!! $item['title'] !!}</a>
                @endforeach
            </div>
            @if(sizeof(array_get($footer, 'socialProfiles.list', [])) > 0)
            <div class="fx-row neg-mg-lr-px-10-ac cl-primary pd-t-10">
                @if(!empty(array_get($footer, 'socialProfiles.title', '')))
                <div class="mg-t-10 pd-l-10 pd-r-10 ft-ter md-ter-reg-2 ter-reg-2">{!! $footer['socialProfiles']['title'] !!}</div>
                @endif
                @foreach($footer['socialProfiles']['list'] as $item)
                {{-- <a class="pd-l-10 pd-r-10 nb-icon-{{$item['key']}} fz-18 mg-t-10" href="{!! $item['url'] !!}" target="_blank" rel="nofollow noreferrer" aria-label="Link to {{$item['key']}} profile"><span class="hidden">{{ucfirst($item['key'])}}</span></a> --}}
                <a href="{!! $item['url'] !!}" target="_blank" rel="nofollow noreferrer" aria-label="Link to {{$item['key']}} profile" class="pd-l-10 pd-r-10 mg-t-10 dp-fx fx-al-ct">
                    <span class="hidden">{{ucfirst($item['key'])}}</span>
                    @switch($item['key'])
                    @case('facebook')
                    <svg width="28px" height="28px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="cl-primary">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Group-31">
                                <rect id="Rectangle" fill-opacity="0" fill="#D8D8D8" x="0" y="0" width="24" height="24"></rect>
                                <g id="iconfinder_social-facebook_216078" transform="translate(7.200000, 3.200000)" fill="currentColor">
                                    <path d="M6.6,6.6 L9.9,6.6 L9.9,9.9 L6.6,9.9 L6.6,17.6 L3.3,17.6 L3.3,9.9 L0,9.9 L0,6.6 L3.3,6.6 L3.3,5.2195 C3.3,3.9116 3.7114,2.2594 4.5298,1.3563 C5.3482,0.451 6.3701,0 7.5944,0 L9.9,0 L9.9,3.3 L7.59,3.3 C7.0422,3.3 6.6,3.7422 6.6,4.2889 L6.6,6.6 Z" id="Path"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                    @break
                    @case('twitter')
                    <svg width="28px" height="28px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="cl-primary">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Group-34">
                                <rect id="Rectangle-Copy-43" fill-opacity="0" fill="#D8D8D8" x="0" y="0" width="24" height="24"></rect>
                                <path d="M20.7812026,6.41252373 C20.1642942,6.6864384 19.5003768,6.8712032 18.8037574,6.953888 C19.5150245,6.52855467 20.0603974,5.85482667 20.3175845,5.0511168 C19.6523045,5.44548587 18.9158297,5.73165013 18.1313239,5.8861312 C17.5035148,5.21784747 16.60864,4.8 15.6183845,4.8 C13.7172439,4.8 12.1758245,6.33970667 12.1758245,8.2390752 C12.1758245,8.50822613 12.2061419,8.77057173 12.2650735,9.02270933 C9.40365419,8.8791168 6.86652903,7.510224 5.1684129,5.42949333 C4.87205161,5.9371712 4.70241032,6.52787413 4.70241032,7.15872853 C4.70241032,8.35170347 5.31012129,9.40448853 6.23395097,10.0210517 C5.66984258,10.0030176 5.13877677,9.84819627 4.67447742,9.5906144 C4.67413677,9.6049056 4.67413677,9.61953707 4.67413677,9.63416853 C4.67413677,11.3001141 5.86094452,12.6897632 7.43608774,13.0062112 C7.14722065,13.0844725 6.84302452,13.1266656 6.52894968,13.1266656 C6.30684903,13.1266656 6.09122065,13.1052288 5.88104258,13.0647371 C6.3194529,14.4309077 7.59074065,15.4255072 9.09707355,15.4530688 C7.91878194,16.3755317 6.43459097,16.9254027 4.82129548,16.9254027 C4.54366968,16.9254027 4.26945032,16.9090699 4,16.8770848 C5.52404645,17.8533099 7.33355355,18.4222357 9.27761548,18.4222357 C15.6105497,18.4222357 19.0732077,13.1821291 19.0732077,8.63752747 C19.0732077,8.48849067 19.0701419,8.33979413 19.0636697,8.1921184 C19.7361032,7.7082592 20.319969,7.10224427 20.7812026,6.41252373 Z" id="Path" fill="currentColor"></path>
                            </g>
                        </g>
                    </svg>
                    @break
                    @case('linkedin')
                    <svg width="28px" height="28px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="cl-primary">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Group-35">
                                <rect id="Rectangle-Copy-44" fill-opacity="0" fill="#D8D8D8" x="0" y="0" width="24" height="24"></rect>
                                <g id="iconfinder_Linkedin_Solid_1435169" transform="translate(4.000000, 4.000000)" fill="currentColor">
                                    <path d="M0.276923077,5.29230769 L3.58769231,5.29230769 L3.58769231,15.9384615 L0.276923077,15.9384615 L0.276923077,5.29230769 Z M1.93846154,0 C2.99692308,0 3.85846154,0.861538462 3.85846154,1.92 C3.85846154,2.97846154 2.99692308,3.84 1.93846154,3.84 C0.873846154,3.84 0.0184615385,2.97846154 0.0184615385,1.92 C0.0184615385,0.861538462 0.873846154,0 1.93846154,0 L1.93846154,0 Z" id="XMLID_195_" fill-rule="nonzero"></path>
                                    <path d="M5.66769231,5.29230769 L8.83692308,5.29230769 L8.83692308,6.75076923 L8.88,6.75076923 C9.32307692,5.91384615 10.4,5.03384615 12.0123077,5.03384615 C15.36,5.03384615 15.9815385,7.23692308 15.9815385,10.1046154 L15.9815385,15.9384615 L12.6769231,15.9384615 L12.6769231,10.7630769 C12.6769231,9.52615385 12.6584615,7.93846154 10.96,7.93846154 C9.23692308,7.93846154 8.97846154,9.28615385 8.97846154,10.6707692 L8.97846154,15.9384615 L5.67384615,15.9384615 L5.66769231,5.29230769 L5.66769231,5.29230769 Z" id="XMLID_192_"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                    @break
                    @case('youtube')
                    <svg width="28px" height="28px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="cl-primary">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="youtube-video" transform="translate(3.000000, 6.000000)">
                                <path d="M0.385491429,1.70786571 C0.496345714,0.935571429 1.13027143,0.355765714 1.90850571,0.300231429 C3.56909143,0.181722857 6.45975429,0 8.57142857,0 C10.6830857,0 13.5738,0.181722857 15.2343429,0.300231429 C16.0125429,0.355765714 16.6464857,0.935571429 16.7574,1.70786571 C16.9277143,2.89473429 17.1428571,4.65908571 17.1428571,6.00008571 C17.1428571,7.34117143 16.9277143,9.10542857 16.7574,10.2923143 C16.6464857,11.0646 16.0125429,11.6444571 15.2342571,11.6999143 C13.5737143,11.8183714 10.6830857,12 8.57142857,12 C6.45978,12 3.56916857,11.8183714 1.90856571,11.6999143 C1.13030571,11.6444571 0.496337143,11.0646 0.385482857,10.2923143 C0.215125714,9.10542857 0,7.34117143 0,6.00008571 C0,4.65908571 0.215134286,2.89473429 0.385491429,1.70786571 Z" id="Path" fill="currentColor"></path>
                                <polygon id="Path" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="12 6 6.85714286 8.96922857 6.85714286 3.03077143"></polygon>
                                <polygon id="Path" fill="#FFFFFF" points="6.85714286 8.57142857 6.85714286 3 12 6"></polygon>
                            </g>
                        </g>
                    </svg>
                    @break
                    @endswitch
                </a>
                @endforeach
            </div>
            @elseif(empty(array_get($footer, 'text', '')))
            <div class="cl-suva-grey ft-ter md-ter-reg-2 ter-reg-2 pd-t-20">All rights reserved &copy; NewsBytes {{date("Y")}}</div>
            @else
            <div class="cl-suva-grey ft-ter md-ter-reg-2 ter-reg-2 pd-t-20">{!! $footer['text'] !!}</div>
            @endif
        </div>
        @if(sizeof(array_get($footer, 'socialProfiles.list', [])) > 0)
        @if(empty(array_get($footer, 'text', '')))
        <div class="cl-suva-grey ft-ter md-ter-reg-2 ter-reg-2 pd-t-50 pd-t-md-30">All rights reserved &copy; NewsBytes {{date("Y")}}</div>
        @else
        <div class="cl-suva-grey ft-ter md-ter-reg-2 ter-reg-2 pd-t-50 pd-t-md-30">{!! $footer['text'] !!}</div>
        @endif
        @endif
    </div>
</footer>
@endif