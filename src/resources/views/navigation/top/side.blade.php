<?php
    function accordionPresenter($accordion = [])
    {
        $accordionPresentable = [];
        if(isset($accordion['list'])){
            foreach ($accordion['list']['data'] as $item) {
                $aItem = [];
                $aItem['linkable'] = true;
                foreach ($item as $ikey => $ival) {
                    $aItem[$ikey] = $ival;
                    if($ikey == 'list'){
                        $aItem['linkable'] = false;
                    }
                }
                array_push($accordionPresentable, $aItem);
            }
        }
        return $accordionPresentable;
    }
    $accordionPresentable = accordionPresenter($data['menu']);
?>
@switch($type)
    @case('amp')
    <amp-sidebar id="sidebar" layout="nodisplay" side="left" class="side-nav wd-full mx-wd-px-380 bg-primary ovr-scroll ovr-x-hidden hide-native-scrollbar ft-pr">
        <div class="pd-l-20 pd-r-20 pd-t-16 pd-b-30">
            <div class="dp-fx fx-al-ct fx-js-bw">
                <a href="{!! array_get($data, 'logoUrl', '/') !!}" class="pd-t-10 pd-b-10">
                    <amp-img src="{{$data['thumb']}}" alt="{{$data['title']}}" height="36" layout="fixed-height" class="{{$data['classList'] or ''}} min-wd-px-120"></amp-img>
                </a>
                <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="cl-ink-dk cs-ptr mg-t-16 mg-b-16 mg-l-16 mg-r-16" on="tap:sidebar.toggle" role="button" tabindex="0" aria-label="Sidebar toggle">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="Cross-icon" transform="translate(5.636039, 5.636039)" fill="#000000">
                            <rect id="Rectangle-5" transform="translate(6.363961, 6.363961) rotate(45.000000) translate(-6.363961, -6.363961) " x="5.36396103" y="-1.63603897" width="2" height="16"></rect>
                            <rect id="Rectangle-5-Copy" transform="translate(6.363961, 6.363961) scale(-1, 1) rotate(45.000000) translate(-6.363961, -6.363961) " x="5.36396103" y="-1.63603897" width="2" height="16"></rect>
                        </g>
                    </g>
                </svg>
                {{-- <span class="nb-icon-cancel fz-22 pd-t-14 pd-b-14 pd-l-14 pd-r-14 cs-ptr" on="tap:sidebar.toggle" role="button" tabindex="0" aria-label="Sidebar toggle"></span> --}}
            </div>
        </div>
        @if(sizeof($accordionPresentable) > 0)
        <hr class="br-t-1p-white-smoke"/>
        <div class="pd-l-20 pd-r-20 pd-t-20 pd-b-30 accordion accordion-side-drawer">
            @foreach($accordionPresentable as $item)
                @include('web-components::accordion.simple', ['type' => $type, 'classList' => 'mg-t-10', 'data' => $item])
            @endforeach
        </div>
        @endif
        @if(sizeof($data['otherLinks']['list']) > 0)
        <hr class="br-t-1p-white-smoke"/>
        <div class="pd-l-20 pd-r-20 pd-t-30 pd-b-30">
            <div class="cl-lt ft-ter md-ter-reg-2 ter-reg-2">{!! $data['otherLinks']['title'] !!}</div>
            <div class="dp-fx fx-dr-col">
                @foreach($data['otherLinks']['list'] as $item)
                    @if(array_get($item, 'size', 'free'))
                        @include('web-components::links.svglink', ['link' => $item, 'classList' => 'pd-t-30'])
                    @endif
                @endforeach
            </div>
        </div>
        @endif
        @if((sizeof($data['appStore']) > 0) && (array_get($data['appStore'], 'size', 'free')))
        <hr class="br-t-1p-white-smoke"/>
        @include('web-components::links.svglink', ['link' => $data['appStore'], 'classList' => 'pd-t-30 pd-b-30 pd-l-20 pd-r-20'])
        @endif
        @if(sizeof($data['socialProfiles']['list']) > 0)
        <hr class="br-t-1p-white-smoke"/>
        <div class="pd-t-30 pd-b-30 pd-l-20 pd-r-20">
            <div class="cl-lt ft-ter md-ter-reg-2 ter-reg-2">{!! $data['socialProfiles']['title'] !!}</div>
            @include('web-components::lists.socialProfiles', ['data' =>  $data['socialProfiles']['list'], 'classList' => 'neg-mg-lr-px-15-ac dp-fx pd-t-8'])
        </div>
        @endif
    </amp-sidebar>
    @break
    @default
        <div class="pd-l-20 pd-r-20 pd-t-16 pd-b-30 ft-pr">
            <div class="dp-fx fx-al-ct fx-js-bw">
                @if(isset($data['includeSvg']))
                <a href="{!! array_get($data, 'logoUrl', '/') !!}" class="svg-inclusion pd-t-10 pd-b-10" data-svg="{{$data['includeSvg']}}" aria-label="{{$data['title']}}"><span class="hidden">{{$data['title']}}</span>
                </a>
                @else
                <a href="{!! array_get($data, 'logoUrl', '/') !!}" class="pd-t-10 pd-b-10">
                    <img src="{{$data['thumb']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="{{$data['classList'] or ''}} ht-px-36"/>
                </a>
                @endif
                <span class="nb-icon-cancel fz-22 pd-t-14 pd-b-14 pd-l-14 pd-r-14 side-drawer-toggle cs-ptr"></span>
            </div>
            @if($loginModule)
            <div class="mg-t-20">
                <button id="login" class="dp-bl bg-accent cl-primary br-rd-10 tx-ct bd-6 md-bd-6 ff-primary pd-t-14 pd-b-14 pd-l-30 pd-r-30 cs-ptr login" data-status="out">Log in/Sign up</button>
                <div class="signed-in-user dp-fx fx-al-st text-logout hidden">
					<img class="br-rd-pr-50 user-signed-in-img userIcon wd-ht-px-50" src="" alt="User Placeholder" title="User Placeholder"/>
					<div class="mg-l-10 dp-fx fx-dr-col bd-4 md-bd-4">
						<p class="cl-ink-dk">Hi, <span class="userText cl-ink-dk text-capitalize"></span></p>
						<p class="cl-link cs-ptr bd-6 md-bd-6 logout mg-t-2 text-logout">Logout</p>
					</div>
				</div>
            </div>
            @endif
        </div>
        @if(sizeof($accordionPresentable) > 0)
        <hr class="br-t-1p-white-smoke"/>
        <div class="pd-l-20 pd-r-20 pd-t-20 pd-b-30 accordion accordion-side-drawer">
            @foreach($accordionPresentable as $item)
                @include('web-components::accordion.simple', ['type' => $type, 'classList' => 'mg-t-10', 'data' => $item])
            @endforeach
        </div>
        @endif
        @if(sizeof($data['otherLinks']['list']) > 0)
        <hr class="br-t-1p-white-smoke"/>
        <div class="pd-l-20 pd-r-20 pd-t-30 pd-b-30">
            <div class="cl-lt ft-pr reg-2">{!! $data['otherLinks']['title'] !!}</div>
            <div class="dp-fx fx-dr-col">
                @foreach($data['otherLinks']['list'] as $linkItem)
                @if(array_get($item, 'size', 'free'))
                    @include('web-components::links.svglink', ['link' => $linkItem, 'classList' => 'pd-t-30'])
                @endif
                @endforeach
            </div>
        </div>
        @endif
        @if((sizeof($data['appStore']) > 0) && (array_get($data['appStore'], 'size', 'free')))
        <hr class="br-t-1p-white-smoke"/>
        @include('web-components::links.svglink', ['link' => $data['appStore'], 'classList' => 'pd-t-30 pd-b-30 pd-l-20 pd-r-20'])
        @endif
        {{-- @if(sizeof($data['socialProfiles']['list']) > 0)
        <hr class="br-t-1p-white-smoke"/>
        <div class="pd-t-30 pd-b-30 pd-l-20 pd-r-20">
            <div class="cl-lt ft-pr reg-2">{!! $data['socialProfiles']['title'] !!}</div>
            @include('web-components::lists.socialProfiles', ['data' =>  $data['socialProfiles']['list'], 'classList' => 'neg-mg-lr-px-15-ac dp-fx pd-t-24'])
        </div>
        @endif --}}
    @break
@endswitch