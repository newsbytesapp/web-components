<?php
$headerData = $menu;
$type = $pageType;
$searchModule = array_get($headerData, 'searchModule', false);
$loginModule = array_get($headerData, 'loginModule', false);
$loginFunctionality = $loginModule ? 'enabled' : 'disabled';
$langVariation = array_get($headerData, 'langVariation', []);

if (!function_exists('primaryHeaderPresenter')) {
    function primaryHeaderPresenter($headerData = [])
    {
        $headerPresentable = [];
        if (isset($headerData['menu']['list']['data']) && (sizeof($headerData['menu']['list']['data']) > 0)) {
            foreach ($headerData['menu']['list']['data'] as $headerItem) {
                $item = [];
                $item['size'] = 'fixed';
                $item['selected'] = $headerItem['selected'];
                $item['selector'] = ['label' => $headerItem['title'], 'title' => '', 'url' => $headerItem['url'], 'linkable' => true];
                $item['content']['data'] = [];
                if (array_get($headerItem, 'list', false) && array_get($headerItem, 'list.data', false)) {
                    foreach ($headerItem['list']['data'] as $listItem) {
                        $drdownItem = [];
                        $drdownItem['name'] = $listItem['title'];
                        $drdownItem['url'] = $listItem['url'];
                        array_push($item['content']['data'], $drdownItem);
                    }
                }
                array_push($headerPresentable, $item);
            }
        }
        return $headerPresentable;
    }
}

if (!function_exists('secondaryHeaderPresenter')) {
    function secondaryHeaderPresenter($header = [])
    {
        $headerPresentable = [];
        $skipAttrs = ['list'];
        if (sizeof($header) > 0) {
            $fItem = [];
            foreach ($header['routePath'][0] as $key => $value) {
                if (!in_array($key, $skipAttrs)) {
                    $fItem[$key] = $value;
                }
            }
            $headerPresentable['root'] = $fItem;
            $headerPresentable['list'] = [];
            if (isset($header['list'])) {
                foreach ($header['list'] as $item) {
                    $dItem = [];
                    foreach ($item as $key => $value) {
                        if (!in_array($key, $skipAttrs)) {
                            $dItem[$key] = $value;
                        }
                    }
                    array_push($headerPresentable['list'], $dItem);
                }
            }
        }
        return $headerPresentable;
    }
}

if (!function_exists('secondaryHeaderItems')) {
    function secondaryHeaderItems($data, $rPath = [], $secondaryHeader = [])
    {
        if (isset($data['list'])) {
            foreach ($data['list']['data'] as $item) {
                if ($item['selected']) {
                    array_push($rPath, $item);
                    $secondaryHeader['routePath'] = $rPath;
                    if (isset($item['list']))
                        $secondaryHeader['list'] = $item['list']['data'];
                    $secondaryHeader = secondaryHeaderItems($item, $rPath, $secondaryHeader);
                }
            }
        }
        return $secondaryHeader;
    }
}

/* Populating Primary Header and transforming it */
$primaryHeaderPresentable = primaryHeaderPresenter($headerData);

/* Populating Secondary Header and transforming it */
$secondaryHeader = secondaryHeaderItems($headerData['menu']);
$secondaryHeaderPresentable = secondaryHeaderPresenter($secondaryHeader);
?>
@if($type != 'amp')
@section('pageScript')
@parent
<script>
    @if($loginModule)
    var loginModule = "{!! $loginFunctionality !!}";
    @endif
    @if(array_get($headerData, 'notifSupport', false))
    var OneSignalConfig = {
        appId: '{{config("web-components.oneSignalWebPushId")}}',
        allowLocalhostAsSecureOrigin: true,
        autoRegister: false,
        autoResubscribe: true,
        notifyButton: {
            enable: false
        },
        safari_web_id: "{{config('web-components.oneSignalSafariPushId')}}",
        persistNotification: false,
        welcomeNotification: {
            "title": "Awesome!",
            "message": "You will now receive notifications on important stories."
        }
    };
    @endif
</script>
@stop
@endif

@switch($type)
@case('amp')
<div class="top-nav wd-full z-index-3 bg-primary">
    <div class="box-shdw-header">
        <div class="wrapper-full-wd-amp-ac">
            <div class="ht-px-56 pos-rel top-bar">
                <div class="pos-abs tp-init bt-init lt-pr-4 rt-pr-4 slab-logo first-tier">
                    <div class="dp-fx ht-full fx-al-ct pos-rel">
                        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="cl-ink-dk cs-ptr mg-t-16 mg-b-16 mg-l-16 mg-r-16" on="tap:sidebar.toggle" role="button" tabindex="0" aria-label="Sidebar toggle">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="Menu-Copy" transform="translate(4.000000, 4.000000)" fill="#111111">
                                    <rect id="Rectangle-23" x="8" y="0" width="8" height="2.02222222"></rect>
                                    <rect id="Rectangle-23-Copy" x="0" y="6.06666667" width="16" height="2.02222222"></rect>
                                    <rect id="Rectangle-23-Copy-2" x="0" y="13.1444444" width="8" height="2.02222222"></rect>
                                </g>
                            </g>
                        </svg>
                        {{-- <div class="nb-icon-menu fz-18 pd-t-16 pd-b-16 pd-l-16 pd-r-16 cs-ptr" on="tap:sidebar.toggle" role="button" tabindex="0" aria-label="Sidebar toggle"></div> --}}
                        <a href="{!! array_get($headerData, 'logoUrl', '/') !!}" class="mg-l-10">
                            <amp-img src="{{$headerData['thumb']}}" alt="{{$headerData['title']}}" height="36" layout="fixed-height" class="{{$headerData['classList'] or ''}} min-wd-px-120"></amp-img>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if((sizeof($secondaryHeaderPresentable) > 0) && (sizeof($secondaryHeaderPresentable['list']) > 0))
    <div class="second-tier pos-rel br-b-1p-white-smoke">
        <div class="pd-b-10 pd-l-r-full-wd-ac pd-t-10 wrapper-full-wd-amp-ac">
            <div class="dp-fx fx-al-ct">
                <a href="{!! $secondaryHeaderPresentable['root']['url'] !!}" class="ft-ter md-ter-reg-2 ter-reg-2 mg-r-14">{!! $secondaryHeaderPresentable['root']['title']!!}</a>
                <div class="ft-ter md-ter-reg-2 ter-reg-2">|</div>
                <div class="hz-scroll-ac">
                    @foreach($secondaryHeaderPresentable['list'] as $item)
                    <a href="{!! $item['url'] !!}" class="ft-ter {{($item['selected']) ? 'md-ter-bd-2 ter-bd-2' : 'md-ter-reg-2 ter-reg-2'}} {{$loop->first ? 'mg-l-14' : 'mg-l-30'}} {{$loop->last ? 'mg-r-30' : ''}}">{!! $item['title']!!}</a>
                    @endforeach
                </div>
                <div class="gd-tint-white-white0-white100-half col-2 rt-init tp-init bt-init pos-abs"></div>
            </div>
        </div>
    </div>
    @endif
</div>
@include('web-components::navigation.top.side', ['type' => $type, 'data' => $headerData])
@break
@default
<header id="header-nav" class="bg-primary {{isset($classList) ? $classList : ''}}">
    <nav class="top-nav wd-full z-index-3 bg-primary">
        <div class="box-shdw-header">
            <div class="wrapper hidden-in-touch">
                <div class="ht-px-70 dp-fx top-bar lg-screen" data-search="off">
                    <div class="wd-full ht-full first-tier pos-rel dp-fx fx-al-ct ft-pr reg-2">
                        <div class="nb-icon-menu fz-18 mg-t-4 cs-ptr side-drawer-toggle"></div>
                        @if(isset($headerData['includeSvg']))
                        <a href="{!! array_get($headerData, 'logoUrl', '/') !!}" class="svg-inclusion mg-l-30" data-svg="{{$headerData['includeSvg']}}" aria-label="{{$headerData['title']}}"><span class="hidden">{{$headerData['title']}}</span>
                        </a>
                        @else
                        <a href="{!! array_get($headerData, 'logoUrl', '/') !!}" class="mg-l-30">
                            <img src="{{$headerData['thumb']}}" alt="{{$headerData['title']}}" title="{{$headerData['title']}}" class="{{$headerData['classList'] or ''}} ht-px-36" /><span class="hidden">{{$headerData['title']}}</span>
                        </a>
                        @endif
                        <div class="dp-fx overflow-check visibility-hidden">
                            @foreach($primaryHeaderPresentable as $item)
                            @include('web-components::dropdowns.hoverableButton', ['data' => $item, 'type'=> $type, 'classList' => ($loop->first ? 'mg-l-14' : '') . ' ovr-elem'])
                            @endforeach
                        </div>
                        @if($searchModule || $langVariation)
                        <div class="min-wd-px-120 pos-abs rt-init dp-fx last-box {{($searchModule && $langVariation) ? 'fx-js-bw' : 'fx-js-ct'}}">
                            @if($searchModule)
                            <span class="nb-icon-search-mobile-header mg-t-2 mg-r-10 pd-b-14 pd-l-16 pd-r-16 pd-t-14 cs-ptr search-for"></span>
                            @endif
                            @if((array_get($langVariation, 'content.data', false)) && sizeof($langVariation['content']['data']) > 0)
                            @include('web-components::dropdowns.plain', ['data'=> $langVariation, 'type'=> $type, 'classList' => ''])
                            @endif
                        </div>
                        @endif
                    </div>
                    <div class="search-bar dp-fx fx-js-bw fx-alt-ct pd-t-20 pd-b-20 wd-full">
                        <input type="text" class="search-query ft-pr md-bd-5 wd-full" placeholder="{{array_get($headerData['searchInfo'], 'text', 'Search')}}" value="" id="searchLgView" />
                        <span class="cl-lt nb-icon-cancel fz-24 cancel-search cs-ptr"></span>
                    </div>
                    <span class="aux-text hidden">{!! array_get($headerData, 'auxText', '')!!}</span>
                </div>
            </div>
            <div class="hidden-lg">
                <div class="ht-px-56 pos-rel top-bar" data-search="off">
                    <div class="pos-abs tp-init bt-init lt-pr-4 rt-pr-4 slab-logo first-tier">
                        <div class="dp-fx ht-full fx-al-ct pos-rel">
                            <div class="nb-icon-menu fz-18 pd-t-16 pd-b-16 pd-l-16 pd-r-16 cs-ptr side-drawer-toggle"></div>
                            @if(isset($headerData['includeSvg']))
                            <a href="{!! array_get($headerData, 'logoUrl', '/') !!}" class="svg-inclusion mg-l-10 pd-b-6 pd-t-6" data-svg="{{$headerData['includeSvg']}}" aria-label="{{$headerData['title']}}"><span class="hidden">{{$headerData['title']}}</span>
                            </a>
                            @else
                            <a href="{!! array_get($headerData, 'logoUrl', '/') !!}" class="mg-l-10 pd-b-6 pd-t-6">
                                <img src="{{$headerData['thumb']}}" alt="{{$headerData['title']}}" title="{{$headerData['title']}}" class="{{$headerData['classList'] or ''}} ht-px-36" /><span class="hidden">{{$headerData['title']}}</span>
                            </a>
                            @endif
                            @if($searchModule || $langVariation)
                            <div class="min-wd-px-110 pos-abs rt-init dp-fx {{($searchModule && $langVariation) ? 'fx-js-bw' : 'fx-js-ct'}}">
                                @if($searchModule)
                                <span class="nb-icon-search-mobile-header mg-t-2 mg-r-10 pd-b-14 pd-l-16 pd-r-16 pd-t-14 cs-ptr search-for"></span>
                                @endif
                                @if((array_get($langVariation, 'content.data', false)) && sizeof($langVariation['content']['data']) > 0)
                                @include('web-components::dropdowns.plain', ['data'=> $langVariation, 'type'=> $type, 'classList' => 'mg-l-8'])
                                @endif
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="search-bar dp-fx fx-js-bw fx-alt-ct pd-l-r-full-wd-ac wd-full">
                        <input type="text" class="search-query ft-pr md-bd-5 wd-full pd-t-12 pd-b-12" placeholder="{{array_get($headerData['searchInfo'], 'text', 'Search')}}" value="" id="searchTouchView" />
                        <span class="cl-lt nb-icon-cancel fz-24 cancel-search cs-ptr pd-t-12 pd-b-12 pd-l-12 pd-r-12 mg-l-12"></span>
                    </div>
                </div>
            </div>
        </div>
        @if((sizeof($secondaryHeaderPresentable) > 0) && (sizeof($secondaryHeaderPresentable['list']) > 0))
        <div class="second-tier pos-rel br-b-1p-white-smoke">
            <div class="wrapper pd-t-10 pd-b-10">
                <div class="dp-fx fx-al-ct">
                    <a href="{!! $secondaryHeaderPresentable['root']['url'] !!}" class="md-reg-2 reg-2 mg-r-14">{!! $secondaryHeaderPresentable['root']['title']!!}</a>
                    <div class="md-reg-2 reg-2">|</div>
                    <div class="hz-scroll-ac">
                        @foreach($secondaryHeaderPresentable['list'] as $item)
                        <a href="{!! $item['url'] !!}" class="{{($item['selected']) ? 'md-bd-6 bd-6' : 'md-reg-2 reg-2'}} {{$loop->first ? 'mg-l-14' : 'mg-l-30'}} {{$loop->last ? 'mg-r-30' : ''}}">{!! $item['title']!!}</a>
                        @endforeach
                    </div>
                    <div class="gd-tint-white-white0-white100-half col-2 rt-init tp-init bt-init pos-abs"></div>
                </div>
            </div>
        </div>
        @endif
        @if(isset($data) && (array_get($data, 'widgets.ticker', []) && (sizeof($data['widgets']['ticker']) > 0)))
        @include('web-components::widgets.ticker', ['data' => $data['widgets']['ticker']])
        @endif
    </nav>
    <nav class="side-nav pos-fix wd-full mx-wd-px-380 tp-init bt-init lt-init bg-primary z-index-4 ovr-scroll ovr-x-hidden hidden animation-dur-scale-3 hide-native-scrollbar">
        @include('web-components::navigation.top.side', ['type' => $type, 'data' => $headerData])
    </nav>
</header>
@include('web-components::tints.completeTint', ['type' => $type])
@break
@endswitch