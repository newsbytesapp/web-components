<?php
    $bannerData = json_encode($data);
?>
@section('pageScript')
@parent
<script type="text/javascript">
    var bannersPresent = true;
    var bannerData = JSON.parse(JSON.stringify({!! $bannerData !!}))
</script>
@stop