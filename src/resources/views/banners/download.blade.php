<?php
$classList = array_get($downloadBox, 'classList', '');
$data = array_get($downloadBox, 'data', []);
?>
<div class="bg-primary pd-t-30 pd-l-md-20 pd-r-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac comp-cards-lists-sqrImg {{$classList}}">
    <div class="dp-fx wd-full fx-al-ct fx-js-bw">
        <div>
            <p class="ft-pr md-bd-6 bd-6">{!! $data['title'] !!}<br>{!! $data['subtitle'] !!}</p>
            <div class="mg-t-30">
                @include('web-components::links.link', ['link' => $data['link'], 'classList' => ''])
            </div>
        </div>
        @switch($pageType)
        @case('amp')
        <amp-img layout="fixed" width="124" height="200" src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/images/download-app-newsbytes.2.png" alt="{{$data['title']}} Thumb"></amp-img>
        @break
        @default
        <img class="lazy" width="124" height="200" data-src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/images/download-app-newsbytes.2.png" alt="{{$data['title']}} Thumb" title="{{$data['title']}} Thumb"/>
        @endswitch
    </div>
</div>