<div class="mg-t-50 ft-ter md-ter-bd-6 ter-bd-6 mg-t-50">{!! $data['title'] !!}</div>
@foreach($data['text'] as $item)
    <p class="{{$loop->first ? 'mg-t-20' : 'mg-t-2'}} ft-sec md-sec-reg-4 sec-reg-4">{!! $item !!}</p>
@endforeach
        