<section class="bg-primary pd-t-20 pd-b-20 pd-t-md-30 pd-b-md-30 pd-l-md-20 pd-r-md-20  pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac ft-pr {{isset($classList) ? $classList : ''}}">
    <h1 class="cl-ink-dk ft-ter md-ter-bd-6 ter-bd-6">{!! $data['title'] !!}</h1>
    @foreach($data['info'] as $item)
        <div class="mg-t-20">
            <p class="cl-ink-dk ft-ter md-ter-bd-2 ter-bd-2">{!! $item['key'] !!}</p>
            <p class="cl-ink-dk ft-ter md-ter-reg-1 ter-reg-1">{!! $item['val'] !!}</p>
        </div>
    @endforeach
</section>