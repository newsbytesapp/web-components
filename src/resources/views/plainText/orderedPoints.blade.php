<div class="mg-t-20 ft-sec md-sec-reg-4 sec-reg-4">{!! $data['title'] !!}</div>
@switch($data['type'])
    @case('numbered')
        <ol type="1" class="pd-l-12 ft-sec md-sec-reg-4 sec-reg-4">
        @foreach($data['text'] as $item)
            <li class="{{$loop->first ? 'mg-t-20' : 'mg-t-10'}}">{!! $item !!}</li>
        @endforeach
        </ol>
    @break
@endswitch