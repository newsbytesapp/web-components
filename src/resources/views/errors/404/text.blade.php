<div class="cl-ink-dk ft-pr md-bd-9 bd-9">{!! $data['title'] !!}</div>
<div class="cl-lt ft-ter md-ter-reg-2 ter-reg-2 mg-t-10">{!! $data['subtitle'] !!}</div>
@include('web-components::links.link', ['link' => $data['link'], 'classList' => 'mg-t-40'])