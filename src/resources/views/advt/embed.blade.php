@if(!empty(array_get($data, 'slot', [])))
    @switch($type)
        @case('amp')
        @break
        @default
        @if((strpos($data['adType'], 'meson') !== false) || (strpos($data['adType'], 'adx') !== false))
            <?php
                $slotStyle = 'width:auto;height:auto';
                // if (!empty(array_get($data['slot'], 'size', []))) {
                //     if (array_get($data['slot'], 'type', 'single-size') == 'multi-size') {
                //         $slotStyle.='width:auto;height:auto;';
                //     } else {
                //         $slotStyle.='width:'.$data['slot']['size'][0].'px;height:'.$data['slot']['size'][1].'px;';
                //     }
                // }
                $commonSlot = isset($common) && $common;
                $loadEvent = isset($loadEvent) ? $loadEvent : '';
                $slotElementRenderingConfig = [
                    'id' => $data['slot'] . (isset($htmlId) && !empty($htmlId) ? '_' . $htmlId : '') . (isset($position) && !empty($position) ? '_' . $position : ''),
                    'name' => $data['slot'],
                    'slot' => $data['slot'],
                    'type' => $data['adType'],
                    'width' => $defineSlots[$data['slot']]['multiSize']['0']['0'],
                    'height' => $defineSlots[$data['slot']]['multiSize']['0']['1']
                ];
            ?>
            <div id="{{$slotElementRenderingConfig['id']}}" style="{{$slotStyle}}">
                @if($commonSlot)
                @section('advtEmbedScript')
                @parent
                @if(!empty($loadEvent))
                    window.addEventListener("{{$loadEvent}}", function(event) {
                        slotLoaderOpen();
                        slotLoaderDelayed(JSON.parse('{!!json_encode($slotElementRenderingConfig);!!}'));
                    });
                @else
                    slotLoaderDelayed(JSON.parse('{!!json_encode($slotElementRenderingConfig);!!}'));
                @endif
                @endsection
                @else
                <script>
                    @if(!empty($loadEvent))
                        window.addEventListener("{{$loadEvent}}", function(event) {
                            slotLoaderOpen();
                            slotLoaderDelayed(JSON.parse('{!!json_encode($slotElementRenderingConfig);!!}'));
                        });
                    @else
                        slotLoaderDelayed(JSON.parse('{!!json_encode($slotElementRenderingConfig);!!}'));
                    @endif
                </script>
                @endif
            </div>
        @elseif(strpos($data['adType'], 'adsense') !== false)
            <?php
                $slotStyle = '';
                if (!empty(array_get($data['slot'], 'size', []))) {
                    if (array_get($data['slot'], 'type', 'single-size') == 'multi-size') {
                        $slotStyle.='display:inline-block;width:auto;height:auto;';
                    } else {
                        $slotStyle.='display:inline-block;width:'.$data['slot']['size'][0][0].'px;height:'.$data['slot']['size'][0][1].'px;';
                    }
                }
            ?>
            @if((!empty($data['slot']['id'])) && (!empty($data['slot']['clientId'])))
            <ins class="adsbygoogle" style="{{$slotStyle}}" data-ad-client="{{array_get($data['slot'], 'clientId', '')}}" data-ad-slot="{{array_get($data['slot'], 'id', '')}}"></ins>
            <script type="text/javascript">
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
            @endif
        @endif
        @break
    @endswitch
@endif