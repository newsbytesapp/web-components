@if(isset($data['fallbackAds']) && !empty($data['fallbackAds']))
    var fallbackAdsData = JSON.parse(`{!!json_encode($data['fallbackAds'])!!}`);
    var fallBackAdSlots = [];
    window.nb_fallBackAdSlots = [];
@endif
    window.nb_adSlots = [];
    window.nb_adLoad = false;
    window.nb_loaded = 0;
    window._max_ad_count = window._max_ad_count || 3;


@if(sizeof($data) > 0)
    @switch($type)
    @case('amp')
    @break
    @default
            window.googletag = window.googletag || {cmd:[]};
            window.defineSlots = JSON.parse('{!! json_encode(array_get($data, "defineSlots", [])) !!}');
            window.nbAdvtParams = JSON.parse('{!! json_encode(array_get($data, "nbAdvtParams", [])) !!}');
            var nbAdvtConf = JSON.parse('{!!json_encode(array_get($data, "nbAdvtConf", []));!!}'), gt = window.googletag;
        @if(strpos($data['adType'], 'meson') !== false || strpos($data['adType'], 'adx') !== false)
        (function (w,d,n) {
            w.googletag = w.googletag || {cmd:[]};
            w.PWT = w.PWT || {bidsReceived: {}};
            w.defineSlots = w.defineSlots || {};
            var gt = w.googletag, p = w.PWT;
            class DeviceInfo {
                constructor() {
                    this.memory = ('deviceMemory' in n) ? n.deviceMemory : 0;
                    this.threads = ('hardwareConcurrency' in n) ? n.hardwareConcurrency : 0;
                    this.batteryLevel = 100;
                    this.batteryCharging = false;
                    this.est = 100; //Effective Speed Threshold
                    this.getBattery();
                    this.getNetwork(true);
                    this.intervalId = null;
                }
            
                effectiveSpeed() {
                    return this.logTime - this.startingTime;
                }
            
                refresh() {
                    this.getBattery();
                    this.getNetwork(false);
                }
            
                getNetwork(init) {
                    const c = n.connection || n.mozConnection || n.webkitConnection;
                    this.networkSpeed = c ? c.downlink : 5;
                    this.networkType = c ? c.effectiveType : '4g';
                    this.networkRTT = c ? c.rtt : -1;
                    {{-- if(init) c.onchange = this.refresh.bind(this); --}}
                }
            
                getBattery() {
                    if ('getBattery' in n) {
                        var _this = this;
                        n.getBattery().then(battery => {
                            _this.batteryLevel = battery.level * 100;
                            _this.batteryCharging = battery.charging;
                            console.log(`Battery is ${this.batteryCharging ? 'charging' : 'not charging'} and has ${this.batteryLevel} units of charge`);
                        });
                    }
                }
            
                isSlow() {
                    return (this.threads < 8 || this.memory < 3 || (!this.batteryCharging && this.batteryLevel < 20) || this.effectiveSpeed() > this.est);
                }
            
                monitor() {
                    if (this.intervalId) return;
                    var _this = this;
                    this.intervalId = w.setInterval(function(){_this.refresh();}, 5000)
                }
            
                speedTestInit(startingTime, logTime, est) {
                    this.startingTime = startingTime;
                    this.logTime = logTime;
                    this.est = est;
                }
            }
            class AdLoader {
                constructor(initObj) {
                    this.v = "2.2.1";
                    this.delay = 0;
                    this.cascadeDelay = 0;
                    this.vpMap = {};
                    this.slotMap = {};
                    this.bidSlots = [];
                    this.deviceCharsEnabled = false;
                    this.isPWTLoaded = this.isOWEnabled = false;
                    this.releaseSlotFlag = {};
                    this.targeting = {};
                    this.device = new DeviceInfo;
                    this.defaultState = {isTamEnabled: true, isTamParallelized: true};
                    this.loadState();
                    console.log(`Adloader version ${this.v}`);
                    console.log(`TAM is ${this.isTamEnabled ? 'enabled' : 'disabled'} and ${this.isTamParallelized ? 'parallelized' : 'FIFO'}`);
                    this.createTracker([], this.display.bind(this));
                    if(Array.isArray(initObj) && initObj.length)
                        w.setTimeout(() => {
                            initObj.forEach((obj) => {w.adLoader.push(obj)});
                        }, 5);
                }
            
                addTargeting(slot, gslot) {
                    for (let k in this.targeting) {
                        if (String(this.targeting[k]) == '') continue;
                        slot.setTargeting(k, String(this.targeting[k]));
                    }
                    return slot.setTargeting('surface', gslot.surface)
                        .setTargeting('refreshRate', gslot.refreshRate)
                        .setTargeting('pageName', gslot.pageName)
                        .setTargeting('adUnitName', gslot.au)
                        .setTargeting('tamEnabled', String(gslot?.enableTAM))
                        .setTargeting('sizes', JSON.stringify(gslot.multiSize))
                        .setTargeting('adFormat', gslot.adFormat);
                }
            
                createTracker(e, t) {
                    this.tracker = new Proxy(e, {
                        set: (e, i, n) => {
                            e[i] = n;
                            t(n);
                            return true;
                        },
                        deleteProperty: (e, i) => {
                            delete e[i];
                            t(i);
                            return true;
                        }
                    });
                }
            
                debug() {
                    gt.cmd.push(() => {
                        gt.pubads().setForceSafeFrame(true);
                        gt.openConsole();
                    });
                }
            
                dispatcher(name, payload) {
                    const ev = new CustomEvent(name, { detail: payload, bubbles: true, cancelable: true, composed: false });
                    try {
                        w.dispatchEvent(ev);
                        const ps = payload.data.slot;
                    } catch(e) {
                        console.error(e);
                    }
                }
            
                display(mslot) {
                    if (typeof mslot === 'number') return;
                    if (typeof w.defineSlots[mslot.name] == 'undefined') return;
                    if (!this.isOpen && this.deviceCharsEnabled && this.device.isSlow() && (this.tracker.length > this._max_ad_count) && (void 0 === mslot.retry))
                        this.delay = this.delay + this.cascadeDelay;
                    else
                        this.delay = 0;
                    const gslot = w.defineSlots[mslot.name];
                    if ((gslot.enabled === false) || gslot.client !== 'google') return;
            
                    console.log(`${(void 0 === mslot.retry ? "display" : "retry")} gslot called for ${mslot.name}`, mslot, gslot);
                    gslot.containerID = mslot.id;
            
                    const sm = this.slotMap, bs = this.bidSlots;
                    const _fetchBids = this.fetchBids.bind(this);
                    const addTargeting = this.addTargeting.bind(this);
                    const isTamEnabled = this.isTamEnabled;
                    const delay = this.delay;
            
                    gt.cmd.push(() => {
                        const au = gslot.au.startsWith('/') ? gslot.au : `/${gslot.au}`;
                        const slot = gt.defineSlot(au, gslot.multiSize, gslot.containerID);
                        addTargeting(slot, gslot);
                        slot.addService(gt.pubads());
            
                        sm[gslot.containerID] = { slot, gslot };
                        if(!isTamEnabled && delay > 0)
                            w.setTimeout(() => {
                                gt.cmd.push(() => {
                                    gt.display(gslot.containerID);
                                    gt.enableServices();
                                });
                            }, delay);
                        else {
                            gt.display(gslot.containerID);
                            gt.enableServices();
                        }
                        if(!gslot.enableTAM && isTamEnabled) {
                            gt.pubads().refresh([slot])
                            return;
                        }
            
                        bs.push({
                            bidSlot: {
                                slotID: gslot.containerID,
                                slotName: au,
                                sizes: gslot.multiSize
                            },
                            googleSlot: slot
                        });
            
                        console.log(`setting displayBids with length: ${bs.length}`);
                        w.setTimeout(() => { _fetchBids(slot); }, 20 + bs.length);
                    });
                }
            
                fetchBids(slot, isRefresh) {
                    var bids, slots;
                    isRefresh = isRefresh ?? false;
                    if(this.isTamParallelized && !isRefresh) {
                        if(this.bidSlots.length < 1) return;
                        console.log(`Fetching Bids with length: ${this.bidSlots.length}`);
                        bids = this.bidSlots.map(s => s.bidSlot);
                        slots = this.bidSlots.map(s => s.googleSlot);
                        this.bidSlots = [];
                    } else {
                        const slotID = slot.getSlotElementId();
                        const slotName = slot.getAdUnitPath();
                        const sizes = this.slotMap[slotID].gslot.multiSize;
                        console.log(`Fetching Bids for: ${slotID}`);
                        bids = [{slotID,slotName,sizes}];
                        slots = [slot];
                    }
                    var rsf = this.releaseSlotFlag;
                    bids.forEach( b => {
                        p.bidsReceived[b.slotID] = p.bidsReceived[b.slotID] || {};
                        rsf[b.slotID] = rsf[b.slotID] || false;
                    });
                    var _releaseSlot = this.releaseSlot.bind(this);
                    if(this.isTamEnabled) {
                        const r = w.apstag;
                        r.fetchBids({ slots: bids }, (i) => {
                            console.log(`TAM bidding completed`);
                            gt.cmd.push(() => {
                                r.setDisplayBids();
                            });
                            slots.forEach(s => {
                                p.bidsReceived[s.getSlotElementId()].TAM_BidsReceived = true;
                            });
                            _releaseSlot(false, slots);
                        });
                    }
                    if(this.isOWEnabled) {
                        var bidOpenWrap = this.bidOpenWrap.bind(this);
                        if ('undefined' === typeof p.requestBids) {
                            d.addEventListener('ads_event', () => bidOpenWrap(slots));
                        }
                        else
                            bidOpenWrap(slots);
                        const PWT_FAILSAFE_TIMEOUT = 1200;
                        setTimeout(function() {
                            console.log(`Force releasing slots`);
                            _releaseSlot(true, slots);
                        }, PWT_FAILSAFE_TIMEOUT);
                    }
                }
            
                bidOpenWrap(slots) {
                    var _releaseSlot = this.releaseSlot.bind(this);
                    p.requestBids(p.generateConfForGPT(slots), function(a) {
                        p.addKeyValuePairsToGPTSlots(a);
                        console.log(`OW bidding completed`, a);
            
                        slots.forEach((s) => {
                            p.bidsReceived[s.getSlotElementId()].OW_BidsReceived = true;
                        });
                        _releaseSlot(false, slots);
                    });
                }
            
                lazy(params) {
                    if(Array.isArray(params) || (Object.entries(params).length === 0))
                        return;
            
                    gt.cmd.push(() => {
                        gt.pubads().enableLazyLoad(params);
                    });
                }
            
                loadState() {
                    const state = JSON.parse(w.localStorage.getItem('nb_adloader')) || this.defaultState;
                    state.isTamEnabled = state.isTamEnabled && !(this.deviceCharsEnabled && this.device.isSlow())
                    Object.assign(this, state);
                }
            
                onSlotRenderEnded(e) {
                    if (e.isEmpty) {
                        const i = { requestId: e.slot.getAdUnitPath(), data: e };
                        const adId = e.slot.getSlotElementId();
                        console.log(`ad failed for: ${e.slot.getAdUnitPath()}`, e.slot);
                        p.removeKeyValuePairsFromGPTSlots && p.removeKeyValuePairsFromGPTSlots([e.slot]);
                        gt.destroySlots([e.slot]);
                        this.dispatcher("onAdLoadFailed", i);
                        const gslot = w.defineSlots[this.slotMap[adId].gslot.adUnitName];
                        if (typeof gslot.retry !== 'undefined' && gslot.retry < gslot.retryMax) {
                            w.defineSlots[this.slotMap[adId].gslot.adUnitName].retry = gslot.retry + 1;
                            delete this.slotMap[adId].slot;
                            w.setTimeout(() => {
                                this.display({ id: adId, name: gslot.adUnitName, retry: w.defineSlots[this.slotMap[adId].gslot.adUnitName].retry });
                            }, parseInt(gslot.retryRate));
                        }
                    } else {
                        const t = { requestId: e.slot.getAdUnitPath(), data: e };
                        console.log(`ad succeeded for: ${e.slot.getAdUnitPath()}`, e.slot);
                        this.dispatcher("onAdLoadSucceed", t);
                    }
                }
            
                onSlotVisibilityChanged(t) {
                    const i = t.inViewPercentage;
                    const vp = this.vpMap[t.slot.getAdUnitPath()] || 50;
                    const n = { requestId: t.slot.getAdUnitPath(), data: t };
            
                    if (i > vp) {
                        this.dispatcher("onAdExpanded", n);
                    } else if (i < vp) {
                        this.dispatcher("onAdCollapsed", n);
                    }
            
                    this.vpMap[t.slot.getAdUnitPath()] = i;
                }
            
                onSlotRequested(e) {
                    const t = { requestId: e.slot.getAdUnitPath(), data: e };
                    this.dispatcher("onLoadStarted", t);
                }
            
                onImpressionViewable(e) {
                    const t = { requestId: e.slot.getAdUnitPath(), data: e, detail: { adFormat: "banner" } };
                    console.log(`impression of ad: ${e.slot.getAdUnitPath()}`, e.slot);
                    this.dispatcher("onAdImpression", t);
                    this.dispatcher("onAdDisplayed", t);
                    this.refresh(e.slot);
                }
            
                onRewardedSlotReady(e) {
                    const t = { requestId: e.slot.getAdUnitPath(), data: e };
                    this.dispatcher("GPTSlotReady", t);
                }
            
                onRewardedSlotGranted(e) {
                    const t = { requestId: e.slot.getAdUnitPath(), data: e };
                    this.dispatcher("onRewardsUnlocked", t);
                }
            
                onRewardedSlotClosed(e) {
                    const t = { requestId: e.slot.getAdUnitPath(), data: e };
                    gt.destroySlots([e.slot]);
                    this.dispatcher("onAdClosed", t);
                }
            
                open() {this.isOpen = true;}
            
                presetTam() {
                    function q(c, r) {w.apstag._Q.push([c, r]);}
                    w.apstag = {init: function () {q('i', arguments);},fetchBids: function () {q('f', arguments);},setDisplayBids: function () {},targetingKeys: function () {return [];},_Q: []};
                }
            
                push(mslot) {
                    if(typeof mslot === 'function') {mslot();return;}
                    this.tracker.push(mslot);
                }
            
                refresh(slot) {
                    const currentSlotID = slot.getSlotElementId();
                    const gslot = this.slotMap[currentSlotID].gslot;
                    if (typeof gslot.refreshRate === 'undefined' || gslot.refreshRate < 20000) return;
                    console.log(`Refresh set for slot: ${currentSlotID}`);
                    var isTamEnabledForSlot = gslot.enableTAM;
                    const _fetchBids = this.fetchBids.bind(this);
            
                    gt.cmd.push(() => {
                        setTimeout(() => {
                            console.log(`Refreshing slot: ${currentSlotID} and slot TAM status ${isTamEnabledForSlot ? 'enabled' : 'disabled'}`);
                            p.removeKeyValuePairsFromGPTSlots && p.removeKeyValuePairsFromGPTSlots([slot]);
                            if (isTamEnabledForSlot) {
                                _fetchBids(slot, true);
                            } else {
                                gt.cmd.push(() => gt.pubads().refresh([slot]));
                            }
                        }, Number(gslot.refreshRate));
                    });
                }
            
                releaseSlot(forced, slots) {
                    var rsf = this.releaseSlotFlag;
                    const isOWEnabled = this.isOWEnabled;
                    const isTamEnabled = this.isTamEnabled;
                    var googleSlots = slots.filter(s => {
                        const slotId = s.getSlotElementId();
                        var bid = p.bidsReceived[slotId];
                        if (
                            (forced === true && rsf[slotId] !== true) ||
                            (isTamEnabled && isOWEnabled && bid?.TAM_BidsReceived && bid?.OW_BidsReceived) ||
                            (isTamEnabled && !isOWEnabled && bid?.TAM_BidsReceived) ||
                            (!isTamEnabled && isOWEnabled && bid?.OW_BidsReceived)
                        ) {
                            rsf[slotId] = true;
                            bid.TAM_BidsReceived = bid.OW_BidsReceived = false;
                            return true;
                        }
                        return false;
                    });
                    googleSlots.forEach((s) => gt.cmd.push(() => gt.pubads().refresh([s])));
                    console.log(`Releasing slots with length: ${googleSlots.length}`, JSON.parse(JSON.stringify(slots)), JSON.parse(JSON.stringify(googleSlots)));
                }
            
                saveState() {
                    w.localStorage.setItem('nb_adloader', JSON.stringify({isTamEnabled: this.isTamEnabled, isTamParallelized: this.isTamParallelized}));
                }
            
                setDeviceChars(deviceCharsEnabled) {
                    this.deviceCharsEnabled = deviceCharsEnabled ?? false;
                    this.loadState();
                }
            
                setEvents() {
                    const eventListeners = [
                        { event: "slotRenderEnded", listener: this.onSlotRenderEnded.bind(this) },
                        { event: "slotVisibilityChanged", listener: this.onSlotVisibilityChanged.bind(this) },
                        { event: "slotRequested", listener: this.onSlotRequested.bind(this) },
                        { event: "impressionViewable", listener: this.onImpressionViewable.bind(this) },
                        { event: "rewardedSlotReady", listener: this.onRewardedSlotReady.bind(this) },
                        { event: "rewardedSlotGranted", listener: this.onRewardedSlotGranted.bind(this) },
                        { event: "rewardedSlotClosed", listener: this.onRewardedSlotClosed.bind(this) }
                    ];
            
                    gt.cmd.push(() => {
                        eventListeners.forEach(({ event, listener }) => {
                            gt.pubads().addEventListener(event, listener);
                        });
            
                        gt.enableServices();
                    });
                }
            
                setTam() {
                    console.log(`Setup TAM is ${this.isTamEnabled ? 'enabled' : 'disabled'}`);
                    if (!this.isTamEnabled) return;
                    if (!w.apstag) this.presetTam();
            
                    w.apstag.init({
                        pubID: '32f695ba-fd00-4206-9c9f-753fcc4b7f5e',
                        adServer: 'googletag',
                        bidTimeout: 1200
                    });
                }
            
                setOpenWrap() {
                    console.log(`Setup OpenWrap is ${this.isOWEnabled ? 'enabled' : 'disabled'}`);
                    if(void 0 === p.bidsReceived) p.bidsReceived = {};
                    if (!this.isOWEnabled) return;
                    var _this = this;
                    p.jsLoaded = function() {
                        console.log('PWT js loaded');
                        _this.isPWTLoaded = true;
                        d.dispatchEvent(
                            new CustomEvent('ads_event', {
                                detail: {
                                    event: 'pwt_script_loaded'
                                }
                            })
                        );
                    };
                }
            
                setup(targeting, advtParams) {
                    this.isOpen = false;
                    this.targeting = targeting;
                    this.device.speedTestInit(advtParams?.fst, Date.now(), advtParams?.est);
                    this.setDeviceChars(advtParams?.dce);
                    this.isTamEnabled = (advtParams?.isTamEnabled ?? this.isTamEnabled) && !(this.deviceCharsEnabled && this.device.isSlow());
                    this.isOWEnabled = (advtParams?.ow.enabled && !(this.deviceCharsEnabled && this.device.isSlow())) ?? false;
                    this._max_ad_count = advtParams?.max_ad_count ?? 2;
                    this.cascadeDelay = advtParams?.cascadeDelay ?? 0;
                    this.setTam();
                    this.setOpenWrap();
                    const url = advtParams?.url;
                    var _this = this;
                    console.log(`Device characteristics: ${JSON.stringify(this.device)}`);
                    gt.cmd.push(() => {
                        if (_this.isTamEnabled) {
                            gt.pubads().disableInitialLoad();
                            gt.pubads().enableSingleRequest();
                        }
                        if(url) gt.pubads().set("page_url", url);
                        console.log(`URL is set as ${url}`);
                        gt.enableServices();
                    });
                    if (!(this.deviceCharsEnabled && this.device.isSlow()))
                        this.lazy(advtParams?.lazy);
                    this.setEvents();
                }
            
                toggleTAM() {
                    this.isTamEnabled = !this.isTamEnabled;
                    console.log(`TAM is now ${this.isTamEnabled ? 'enabled' : 'disabled'}`);
                    this.saveState();
                }
            
                toggleTAMParallel() {
                    this.isTamParallelized = !this.isTamParallelized;
                    console.log(`TAM is ${this.isTamParallelized ? 'parallelized' : 'FIFO'}`);
                    this.saveState();
                }
            }
            w.adLoader = new AdLoader(w.adLoader);
        })(window, document, navigator);
window.adLoader.push(() => {
            nbAdvtParams['fst']=window.log[0][1];
            if(fullurl) nbAdvtParams['url'] = fullurl;
            window.adLoader.setup({
                "categoryName":"{!! array_get($data, "targetParams.category", []) !!}",
                'oem':window.partner,
                'PAW':String(window.issupportpaw ?? false),
                'sdkVersion':window.sdkversion,
                'media_version':`${window.media_version ?? 'null'}_${window.page_from ?? 'null'}`,
            }, nbAdvtParams);
});
function slotLoaderDelayed(slot){window.adLoader.push(slot);}
function slotLoaderOpen(){window.adLoader.push(function(){window.adLoader.open()});}
window.addEventListener('onAdLoadFailed', function(e){ced(ce('jsAdLoad', e));});
window.addEventListener('onAdLoadSucceed', function(e){ced(ce('jsAdLoad', e));});
window.addEventListener('onAdImpression', function(e){ced(ce('jsAdImpression', e));});
        @endif
        @if(strpos($data['adType'], 'adsense') !== false)
            const addScript = (url) => new Promise((resolve) => {
                const script = document.createElement('script');
                script.src = url;
                script.async = true;
                script.crossorigin="anonymous"
                document.head.appendChild(script);
                script.addEventListener('load', resolve)
            })

            let URL = 'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=' + window['advtsConf']["{{$data['adType']}}"]['id'];

            let p = addScript(URL).then(() => {console.log('Adsense Loaded')});
        @elseif($data['adType'] == 'taboola')
            window._taboola = window._taboola || [];
                _taboola.push({article:'auto'});
                !function (e, f, u, i) {
                    if (!document.getElementById(i)){
                        e.async = true;
                        e.src = u;
                        e.id = i;
                        f.parentNode.insertBefore(e, f);
                    }
                }(document.createElement('script'), document.getElementsByTagName('script')[0], '//cdn.taboola.com/libtrc/newsbytes/loader.js', 'tb_loader_script');
                if(window.performance && typeof window.performance.mark == 'function'){window.performance.mark('tbl_ic');}
        @endif
    @break
    @endswitch
@endif
