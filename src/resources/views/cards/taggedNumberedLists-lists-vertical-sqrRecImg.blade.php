@if(count($data['list']['data']) > 0)
<?php
    $tag = array_get($data, 'tag', 'default');
?>
<section {!! isset($data['htmlid'])?'id="'.$data['htmlid'].'"':'' !!} class="bg-primary  pd-b-30 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac comp-cards-lists-sqrImg {{isset($classList) ? $classList : ''}}">
    @if(isset($data['tagUrl']) && !empty($data['tagUrl']))
    <h2 class="ft-ter md-ter-bd-4 ter-bd-4 pd-l-md-20 pd-r-md-20 pd-t-20">
        <a href="{!! $data['tagUrl'] !!}" title="{{$data['title']}}">{!! $data['title'] !!}</a>
    </h2>
    @else
    <h2 class="ft-ter md-ter-bd-4 ter-bd-4 pd-l-md-20 pd-r-md-20 pd-t-20">{!! $data['title'] !!}</h2>
    @endif
    <div class="pd-l-md-10 pd-r-md-10">
        @include('web-components::lists.taggedNumbered-sqrRecImgVertical', ['data' => $data['list']['data'], 'type' => $type, 'linkTargetAttr' => array_get($data, 'list.linkTargetAttr', '')])
    </div>
</section>
@endif