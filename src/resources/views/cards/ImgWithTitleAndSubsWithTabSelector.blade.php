<div class="br-1p-pale-lavender mg-t-10  {{isset($data['type']) && $data['type'] =='product' ? ' ' : 'mg-b-20'}}">
    @if (array_get($data,"type",false) && $data['type'] =='product')
        @include('web-components::lists.titleWithSubsIcons',['data' => $data['content']])  
    @else
        @include('web-components::lists.items.descrpitive-ImgWithTitleAndSubs',['data' => $data['content']])
    @endif

    @include('web-components::tabs.listWithVerticalTabSelector',['data' => $data['tab']])
</div>