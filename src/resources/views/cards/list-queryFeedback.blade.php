<section class="bg-primary pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 comp-cards-list-queryFeedback {{isset($classList) ? $classList : ''}}">
    @if(sizeof(array_get($data, 'header', [])) > 0)
    <div class="pd-t-20 pd-l-md-10 pr-r-md-10">
    @switch($type)
    @case('amp')
        <amp-img src="{{$data['header']['thumb']}}" alt="{{$data['header']['title']}}" layout="fixed" width="120" height="45"></amp-img>
    @break
    @default
        <img data-src="{{$data['header']['thumb']}}" alt="{{$data['header']['title']}}" title="{{$data['header']['title']}}" class="lazy ht-px-45"/>
    @break
    @endswitch
    </div>
    @endif

    @if(isset($data['postbox']))
        @include('web-components::lists.items.postbox', ['data' => $data['postbox'], 'type' => $type, 'classList' => 'mg-t-24'])
    @endif

    @if(sizeof(array_get($data, 'list.data', [])) > 0)
        @include('web-components::lists.queryFeedback', ['data' => $data['list']['data'], 'type' => $type, 'classList' => ''])
    @endif

    @if((sizeof(array_get($data, 'list.link', [])) > 0) && (sizeof(array_get($data, 'list.data', [])) > 2))
    <div class="pd-t-20 pd-b-20 pd-l-md-10">
        @include('web-components::links.link', ['link' => $data['list']['link'], 'classList' => ''])
    </div>
    @endif
</section>