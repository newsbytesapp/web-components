@if(count($data['list']['data']) > 0)
    @include('web-components::lists.eventCards', ['data' => $data['list']['data'], 'type' => $type, 'advt' => array_get($data, 'advt', null)])
@endif