@if(array_get($data, 'list.data', []) && (sizeof($data['list']['data']) > 0))
<section class="bg-primary ft-pr  pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 {{isset($classList) ? $classList : ''}}">
    <div class="bd-3 md-bd-3">{!! $data['title'] !!}</div>
    <?php
        $htmlTargetAttr = (array_get($data, 'list.linkTargetAttr', '') ? (!empty($data['list']['linkTargetAttr']) ? 'target=' . $data['list']['linkTargetAttr'] : '') : '');
    ?>
    @foreach($data['list']['data'] as $item)
        <div class="dp-fx mg-t-20 fx-al-ct">
            @if(array_get($item, 'thumb', '') && (!empty($item['thumb'])))
            @switch($type)
            @case('amp')
            <amp-img src="{{$item['thumb']}}" srcset="{{$item['img']}} 960w, {{$item['ximg']}} 1280w" alt="{!! $item['title'] !!}" width="40" height="40" layout="fixed" class="fx-basis-40px"></amp-img>
            @break
            @default
            <img data-src="{{$item['thumb']}}" alt="{!! $item['title'] !!}" title="{{$item['title']}}" class="lazy wd-ht-px-40 fx-basis-40px"/>
            @break
            @endswitch
            @endif
            <div class="{{array_get($item, 'thumb', '') && (!empty($item['thumb'])) ? 'mg-l-10' : ''}}">
                <a href="{!! $item['url'] !!}" class="dp-bl bd-5 md-bd-5 txt-decor-underline" {{$htmlTargetAttr}}>{!! $item['title'] !!}</a>
                <div class="reg-3 md-reg-3">{!! $item['subtitle'] !!}</div>
            </div>
        </div>
    @endforeach
</section>
@endif