@if(count($data['list']['data']) > 0)
<section class="bg-primary pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 comp-cards-lists-smRecImg-sqrRecImg {{isset($classList) ? $classList : ''}}">
    <div class="cl-lt ft-pr bd-3 md-bd-3 pd-t-10 pd-l-md-10 pd-r-md-10">{!! $data['title'] !!}</div>

    @include('web-components::lists.smRecImg', ['data' => array_splice($data['list']['data'], 0, 4), 'linkTargetAttr' => $data['list']['linkTargetAttr']])

    @include('web-components::lists.sqrRecImg', ['data' => $data['list']['data'], 'linkTargetAttr' => $data['list']['linkTargetAttr']])

    @if(array_get($data, 'list.button', []) && (sizeof($data['list']['button']) > 0))
    <div class="pd-t-30 pd-b-20 pd-l-md-10 pd-r-md-10">
        @include('web-components::buttons.basic', [ 'button' => $data['list']['button'], 'type' => $type, 'searchFor' => '.comp-cards-lists-smRecImg-sqrRecImg', 'appendTo' => '.list-sqrRecImg', 'template' => 'listItemSqrRecImg' ])
    </div>
    @else
        @if(array_get($data, 'list.link', []) && (sizeof($data['list']['link']) > 0))
        <div class="pd-t-30 pd-b-20 pd-l-md-10 pd-r-md-10">
            @include('web-components::links.link', ['link' => $data['list']['link'], 'classList' => ''])
        </div>
        @endif
    @endif
   
</section>
@endif