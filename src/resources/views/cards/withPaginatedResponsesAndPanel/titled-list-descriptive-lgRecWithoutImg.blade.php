@if(count($data['list']['data']) > 0)
<?php
    $prevApi = array_get($data, 'list.panel.prev.api', null);
    $nextApi = array_get($data, 'list.panel.next.api', null);
    $prevUrl = array_get($data, 'list.panel.prev.url', null);
    $nextUrl = array_get($data, 'list.panel.next.url', null);
    $nextAmpUrl = array_get($data, 'list.panel.next.ampurl', null);
    $text = array_get($data, 'list.panel.text', null);
?>
<section>
    @foreach($data['list']['data'] as $list)
    <div class="bg-primary pd-t-10 pd-b-10 {{isset($classList) ? $classList : ''}}" data-template="paginatedResponseListDescriptiveLgRecImg">
        {{-- <div class="cl-ink-dk ft-ter md-ter-bd-4 ter-bd-4 pd-t-10 pd-l-md-20 pd-r-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">
            @if(empty(array_get($data, 'tagUrl', '')))
            <span>{!! $data['title'] !!}</span>
            @elseif(($type == 'amp') && (isset($data['tagAmpUrl'])))
            <a href="{!! $data['tagAmpUrl'] !!}" title="{{$data['title']}}">{!! $data['title'] !!}</a>
            @else
            <a href="{!! $data['tagUrl'] !!}" title="{{$data['title']}}">{!! $data['title'] !!}</a>
            @endif
        </div> --}}
        <div class="dp-fx fx-al-ct wd-full cl-ink-dk ft-ter md-ter-bd-4 ter-bd-4 pd-t-10 pd-l-md-20 pd-r-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">
            <span class="wd-px-180 neg-mg-l-pr-6">{{ $list['title'] }}</span>
            <hr class="wd-full bg-body ht-px-4">
        </div>
        <div class="fx-row paginated-response-item" id="{{$data['list']['data'][0]['data'][0]['id']}}" {{!is_null($text) ? 'data-text=' . $text : ''}} {{!is_null($prevApi) ? 'data-prev-api=' . $prevApi : ''}} {{!is_null($nextApi) ? 'data-next-api=' . $nextApi : ''}} data-prev-url="{{$prevUrl}}" data-next-url="{{$nextUrl}}">
            @foreach ($list['data'] as $item)
            <div class="col-12 dp-fx fx-al-st fx-wrap mg-t-10 clickable-target cs-ptr pd-l-md-20 pd-r-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-t-20 pd-b-20 pd-t-md-18 pd-b-md-30 bg-body" title="{{$item['title']}}" data-url="{!! $item['url'] !!}">
                <div class="tx-container col-12 pd-l-md-20">
                    @if(!empty(array_get($item, 'tag', '')) && !empty(array_get($item, 'tagUrl', '')))
                    <div class="dp-fx fx-al-ct">
                        {{-- <p class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt">{!! $item['timestamp'] !!}</p> --}}
                        {{-- <div class="wd-ht-px-4 bg-acadia br-rd-pr-50 mg-l-4 mg-r-4"></div> --}}
                        <a class="cl-ink-dk cl-@nbColor((isset($item['tagColor']))?$item['tagColor']:$item['tag'])-dk ft-ter md-ter-bd-2 ter-bd-2 dp-fx fx-al-ct min-ht-px-50 min-wd-px-50" href="{!! $item['tagUrl'] !!}"><span>{!! $item['tag'] !!}</span></a>
                    </div>
                    @else
                    <div class="min-ht-px-50 min-wd-px-50 dp-fx fx-al-ct">
                        {{-- <p class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt">{!! $item['timestamp'] !!}</p> --}}
                    </div>
                    @endif
                    <h2><a href="{!! $item['url'] !!}" class="ft-pr md-bd-5 bd-5 cl-ink-dk dp-bl">{!! $item['title'] !!}</a></h2>
                    <p class="ft-sec md-sec-reg-3 sec-reg-3 cl-lt mg-t-2">{!! $item['description'] !!}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endforeach
    @if(!empty(array_get($data, 'list.pagination', [])))
    <div class="pd-t-20 pd-b-20 dp-fx fx-js-ct">
    @include('web-components::pagination.simple', ['data' => $data['list']['pagination'], 'classList' => 'br-1p-pale-lavender'])
    </div>
    @else
    <hr data-id="{{$data['list']['data'][0]['id']}}"/>
    <div class="panel static pd-t-30 pd-b-20">
        @if(!is_null($nextUrl))
        <a href="{!! $type == 'amp' ? (!is_null($nextAmpUrl) ? $nextAmpUrl : $nextUrl) : $nextUrl !!}" class="dp-bl wd-full bg-primary cl-link br-rd-10 tx-ct pd-t-14 pd-b-14 navy-blue-cl-hover alice-blue-bg-hover ft-ter md-ter-bd-2 ter-bd-2 next cs-ptr" title="{{$data['list']['panel']['title']}}">{!! $data['list']['panel']['title'] !!}</a>
        @endif
        @switch($type)
        @case('amp')
        @break
        @default
        @include('web-components::toasts.dotsLoader')
        @include('web-components::toasts.error')
        @break
        @endswitch
    </div>
    @endif
    @endif
</section>

@switch($type)
@case('amp')
@break
@default
@section('css')
@parent
<style type="text/css">
.bottom-panel{bottom:20px}.link-disabled{pointer-events:none}.fx-grow-1{flex-grow:1}.rotate-90{transform:rotate(90deg)}
.ht-px-4{height: 4px;}
@media screen and (min-width: 768px) {
    .neg-mg-l-pr-6{margin-left:-6%}
}
</style>
@stop
@break
@endswitch