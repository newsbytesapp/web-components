<?php
    $tag = array_get($data, 'tag', 'default');
?>
@if(count($data['list']['data']) > 0)
<section class=" bg-@nbColor($tag)-lt  mg-t-20 pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 {{isset($classList) ? $classList : ''}}">
    <div class="main-block dp-fx fx-js-bw fx-al-stretch pd-l-20 pd-r-20  pd-t-10 pd-b-10">
        <div >
            <div class="cl-ink-dk ft-ter  ter-bd-4">{!! $data['title'] !!}</div>
            <div class="cl-razzmatazz-dk ft-ter ter-reg-2">{!! $data['description'] !!}
            </div>    
        </div>
        <span class="dp-fx fx-al-ct fz-18" >
            @if(isset($data['img']))
            <img src="{{$data['img']}}" alt="{{$data['title']}}" srcset=""  class="wd-px-40">
            @endif
        </span>
    </div>    
    @include('web-components::lists.listMovies', ['data' => $data['list']['data']])
</section>
@endif