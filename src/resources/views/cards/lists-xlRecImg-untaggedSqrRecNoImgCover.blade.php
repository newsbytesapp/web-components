<?php
    if(isset($htmlTags)){
        $markupTags = $htmlTags;
    }else{
        $markupTags = [];
    }
?>
@if(count($data['list']['data']) > 0)
<section class=" ovr-hidden comp-cards-lists-xlRecImg-sqrRecImg {{isset($classList) ? $classList : ''}}">

    @include('web-components::lists.items.xlRecImg', ['data' => array_splice($data['list']['data'], 0, 1), 'linkTargetAttr' => $data['list']['linkTargetAttr'], 'htmlTags' => $markupTags])

    <div class="fx-row bg-ink-dk pd-b-30 cl-primary pd-l-20 pd-r-20 pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10">
        @include('web-components::lists.items.untaggedSqrRecNoImgCover', ['data' => $data['list']['data'], 'linkTargetAttr' => $data['list']['linkTargetAttr'], 'htmlTags' => $markupTags])
    </div>

</section>
@endif