@if(sizeof($data['list']['data'] > 0))
<section class="bg-primary pd-t-10 pd-b-10 {{isset($classList) ? $classList : ''}}">
    <div class="ft-pr bd-4 md-bd-4 pd-t-10 pd-l-md-20 pd-r-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">{!! $data['title'] !!}</div>
    <div class="pos-rel">
        <div class="pos-abs col-2 col-lg-1 z-index-1 gd-tint-white-smoke-white-smoke0-white-smoke100-neg-half tp-init bt-init lt-init"></div>
        @switch($type)
        @case('amp')
        <div class="mg-t-40 mg-b-30 hz-scroll-ac scroll-menu pos-rel">
        @break
        @default
        <div class="mg-t-40 mg-b-30 slider-init hz-scroll-ac scroll-menu pos-rel hz-scroll-lg-dac mg-l-lg-20 " data-sliderconf="variableWidth">
        @break
        @endswitch
            @foreach($data['list']['data'] as $item)
                @include('web-components::collections.links.capsuleShaped', ['data' => array_merge($item, ['classList' => 'sc-child']), 'type' => $type])
            @endforeach
            <span>&nbsp;</span>
        </div>
        <div class="pos-abs col-2 col-lg-1 z-index-1 gd-tint-white-smoke-white-smoke0-white-smoke100-half tp-init bt-init rt-init"></div>
    </div>
</section>
@endif