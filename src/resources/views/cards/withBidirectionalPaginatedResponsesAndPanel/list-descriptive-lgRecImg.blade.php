@if(count($data['list']['data']) > 0)
<?php 
    $prevApi = array_get($data, 'list.panel.prev.api', null);
    $nextApi = array_get($data, 'list.panel.next.api', null);
    $prevUrl = array_get($data, 'list.panel.prev.url', null);
    $nextUrl = array_get($data, 'list.panel.next.url', null);
?>
{{-- <section class="bg-primary  pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 {{isset($classList) ? $classList : ''}}" data-template="paginatedResponseListDescriptiveLgRecImg">
    <div class="cl-lt ft-pr bd-3 md-bd-3 pd-t-10 pd-l-md-10 pd-r-md-10">{!! $data['title'] !!}</div>
    <div class="fx-row paginated-response-item" id="{{$data['list']['data'][0]['id']}}" data-text="{{$data['list']['panel']['text']}}" {{!is_null($prevApi) ? 'data-prev-api=' . $prevApi : ''}} {{!is_null($nextApi) ? 'data-next-api=' . $nextApi : ''}} data-prev-url="{!! $prevUrl !!}" data-next-url="{!! $nextUrl !!}">
        @foreach ($data['list']['data'] as $item)
        <a href="{!! $item['url'] !!}" class="col-12 dp-fx fx-al-st fx-wrap pd-l-md-10 pd-r-md-10 mg-t-20" {!! $data['list'] == "_blank" ? 'target="_blank"' : '' !!}>
            <div class="col-12 col-md-4">
                @switch($type)
                @case('amp')
                <amp-img src="{{$item['thumb']}}" srcset="{{$item['img']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" width="1080" height="610" layout="responsive"></amp-img>
                @break
                @default
                <div class="asp-ratio r-16-9">
                    <img data-src="{{$item['thumb']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-full dp-bl"/>
                </div>
                @endswitch
            </div>
            <div class="tx-container col-12 col-md-8 pd-l-md-20 ft-pr">
                <p class="bd-6 md-bd-6 cl-lt mg-t-14 mg-t-md-0">{!! $item['timestamp'] !!}</p>
                <div class="bd-3 md-bd-3 cl-ink-dk mg-t-4">{!! $item['title'] !!}</div>
                <p class="reg-2 md-reg-2 cl-lt mg-t-4">{!! $item['description'] !!}</p>
            </div>
        </a>
        @endforeach
    </div>
    <hr data-id="{{$data['list']['data'][0]['id']}}"/>
    <div class="panel floating bottom-panel pos-fix lt-init rt-init hidden">
        <div class="wrapper">
            <div class="fx-row neg-mg-lr-px-10-lg-ac">
                <div class="col-12 col-lg-8 pd-l-lg-10 pd-r-lg-10">
                    <div class="pd-l-md-20 pd-r-md-20">
                        <div class="dp-fx fx-al-ct">
                            <div class="mg-r-10 bg-primary pd-l-20 pd-r-22 pd-b-20 pd-t-20 prev page-scroll cs-ptr br-rd-10 navy-blue-cl-hover alice-blue-bg-hover">
                                <div class="nb-icon nb-icon-back-pwa cl-link rotate-90 prev"></div>
                                <a href="{!! $prevUrl !!}" class="hidden"></a>
                            </div>
                            <div class="mg-l-10 bg-primary pd-b-20 pd-t-20 next page-scroll cs-ptr br-rd-10 navy-blue-cl-hover alice-blue-bg-hover fx-grow-1">
                                <div class="cl-link tx-ct bd-6 md-bd-6 next">{!! $data['list']['panel']['title'] !!}</div>
                                <a href="{!! $nextUrl !!}" class="hidden"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel static mg-t-60">
        <div class="dp-fx fx-al-ct">
            <div class="hidden mg-r-10 bg-primary pd-l-20 pd-r-22 pd-b-20 pd-t-20 prev page-scroll cs-ptr br-rd-10 navy-blue-cl-hover alice-blue-bg-hover">
                <div class="nb-icon nb-icon-back-pwa cl-link rotate-90 prev"></div>
                <a href="{!! $prevUrl !!}" class="hidden"></a>
            </div>
            <div class="bg-primary pd-b-20 pd-t-20 next page-scroll cs-ptr br-rd-10 navy-blue-cl-hover alice-blue-bg-hover fx-grow-1">
                <div class="cl-link tx-ct bd-6 md-bd-6 next">{!! $data['list']['panel']['title'] !!}</div>
                <a href="{!! $nextUrl !!}" class="hidden"></a>
            </div>
        </div>
        @include('web-components::toasts.dotsLoader')
        @include('web-components::toasts.error')
    </div>
</section>
@section('css')
@parent
<style type="text/css">
.bottom-panel{bottom:20px}.link-disabled{pointer-events:none}.fx-grow-1{flex-grow:1}.rotate-90{transform:rotate(90deg)}
</style>
@stop --}}
@endif