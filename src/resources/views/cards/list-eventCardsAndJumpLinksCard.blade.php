@if(count($data['events']['list']['data']) > 0)
    @include('web-components::lists.eventCardsAndJumpLinksCard', ['data' => ['events' => $data['events'], 'jumpLinks' => $data['jumpLinks']], 'type' => $type])
@endif