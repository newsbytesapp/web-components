@if(array_get($data, 'list.data', []) && (sizeof($data['list']['data']) > 0))
<?php
    $htmlTargetAttr = (array_get($data, 'list.linkTargetAttr', '') ? (!empty($data['list']['linkTargetAttr']) ? 'target=' . $data['list']['linkTargetAttr'] : '') : '');
?>
<section class="bg-primary pd-t-20 pd-b-30 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 {{isset($classList) ? $classList : ''}}">
    <div class="cl-lt ft-pr bd-3 md-bd-3 pd-l-md-10 pd-r-md-10">{!! $data['title'] !!}</div>

    @include('web-components::lists.recImgWithTitle', ['data' => $data['list']['data'], 'linkTargetAttr' => $htmlTargetAttr, 'classList' => ''])
   
</section>
@endif