@if(array_get($data, 'html', '') && (!empty($data['html'])))
<section class="bg-primary ft-pr  pd-t-30 pd-b-30 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 {{isset($classList) ? $classList : ''}}">
    <h1 class="bd-2 md-bd-2">{!! $data['title'] !!}</h1>
    @if(array_get($data, 'subtitle', '') && (!empty($data['subtitle'])))
    <p class="bd-7 md-bd-7 mg-t-2">{!! $data['subtitle'] !!}</p>
    @endif
    <div class="mg-t-20">{!! $data['html'] !!}</div>
</section>
@endif