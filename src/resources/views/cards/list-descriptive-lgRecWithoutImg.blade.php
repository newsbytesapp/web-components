@if(count($data['list']['data']) > 0)
<section class="bg-primary pd-t-10 pd-b-10 pd-l-md-10 pd-r-md-10 comp-cards-lists-descriptive-lgRecWithoutImg {{isset($classList) ? $classList : ''}}">
    <h2 class="cl-lt ft-ter ter-bd-4 md-ter-bd-4 pd-t-10 pd-l-md-10 pd-r-md-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">{!! $data['title'] !!}</h2>

    @include('web-components::lists.descriptive-lgRecWithoutImg', ['data' => $data['list']['data'], 'linkTargetAttr' => $data['list']['linkTargetAttr']])

    @if(isset($data['list']['button']['size']))
    @if(isset($data['list']['button']['url']))
    @endif
    <div class="pd-t-20 pd-b-20 pd-l-md-10 pd-r-md-10 dp-fx fx-js-ct">
        @include('web-components::buttons.solid', [ 'button' => $data['list']['button'], 'type' => $type, 'searchFor' => '.comp-cards-lists-descriptive-lgRecWithoutImg', 'appendTo' => '.list-descriptive-lgRecWithoutImg', 'template' => 'listDescriptiveLgRecWithoutImg'])
    </div>
    @endif

</section>
@endif