@if(!empty($data))
<div class="bg-lt-grey pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac comp-cards-lists-sqrImg-drdwon-sateless {{isset($classList) ? $classList : ''}}">
    <div class="ft-pr md-bd-4 bd-4 pd-l-md-20 pd-r-md-20 pd-t-20">{!! $data['card']['title'] !!}</div>
    @if(count($data['card']['list']['data']) > 0)
    <section class="pd-l-md-10 pd-r-md-10">
        @include('web-components::lists.sqrImg', 
        ['data'=> $data['card']['list']['data'], 'type' => $type, 'linkTargetAttr' =>  $data['card']['linkTargetAttr']])
    </section>
    @endif
    @if(count($data['dropdown']['content']['data']) > 0)
    <div class="mg-t-24 fx-row pd-l-md-20 pd-r-md-20">
        <div class="col-12 col-md-4 col-lg-12">
            @include('web-components::dropdowns.stateless',
            ['data'=> $data['dropdown'], 'type'=> $type, 'classList' => ''])
        </div>
    </div>
    @endif
</div>
@endif
