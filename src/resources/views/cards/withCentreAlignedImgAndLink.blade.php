<section class="bg-primary ft-pr  pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 {{isset($classList) ? $classList : ''}}">
    <?php
        $htmlTargetAttr = (array_get($data, 'link.targetAttr', '') ? (!empty($data['link']['targetAttr']) ? 'target=' . $data['link']['targetAttr'] : '') : '');
    ?>
    <div class="dp-fx fx-dr-col fx-al-ct">
        <div class="col-6 neg-mg-t-pr-15">
            <a href="{!! $data['url'] !!}" class="wd-full" {{$htmlTargetAttr}}>
                @switch($type)
                @case('amp')
                <amp-img src="{{$data['img']}}" alt="{{$data['title']}}" width="1080" height="1080" class="wd-full" layout="responsive"></amp-img>
                @break
                @default
                <img data-src="{{$data['img']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="lazy wd-full"/>
                @break
                @endswitch
            </a>
        </div>
        <div class="mg-t-10 bd-3 md-bd-5 cl-ink-dk tx-ct">{!! $data['title'] !!}</div>
        <div class="dp-fx fx-js-ct mg-t-2">
            @include('web-components::links.link', ['link' => $data['link'], 'classList' => ''])
        </div>
    </div>
</section>