@if(count($data['list']['data']) > 0)
<section class="bg-primary pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 comp-cards-lists-taggedLinks-sqrImg {{isset($classList) ? $classList : ''}}">
    <div class="ft-ter md-ter-bd-4 ter-bd-4 pd-t-10 pd-l-md-10 pd-r-md-10">{!! $data['title'] !!}</div>

    @foreach($data['list']['data'] as $item)
        @if(isset($item))
            @include('web-components::entities.sqrImgCircularWithTitleSubtitle', ['data' => $item, 'attributes' => [ 'classListIcon' => 'wd-ht-px-50 br-rd-pr-50', 'classListTitle' => 'ft-ter md-ter-bd-2 ter-bd-2', 'classListSubtitle' => 'ft-ter md-ter-reg-1 ter-reg-1 cl-lt', 'classListContainer' => 'dp-fx pd-l-md-10 pd-r-md-10 fx-al-ct mg-t-40', 'classListTextContainer' => 'pd-l-10'], 'linkable' => true, 'type' => $type])
        @endif

        @if(isset($item['taggedLinks']))
            @foreach($item['taggedLinks'] as $linkItem)
            @include('web-components::links.taggedLink', ['data' => $linkItem, 'attributes' => [ 'classListTitle' => 'ft-pr md-bd-3 bd-3', 'classListSubtitle' => '', 'classListContainer' => 'pd-l-md-10 pd-r-md-10 dp-bl mg-t-20', 'classListTextContainer' => '', 'classListTag' => 'cl-lt ft-ter md-ter-bd-2 ter-bd-2 mg-t-2']])
            @endforeach
        @endif

        @if(isset($item['link']['size']))
            <div class="mg-t-20 pd-l-md-10">
                @include('web-components::links.link', ['link' => $item['link'], 'classList' => '', 'targetAttr' => $item['link']['targetAttr']])
            </div>
        @endif
    @endforeach
       
</section>

@endif