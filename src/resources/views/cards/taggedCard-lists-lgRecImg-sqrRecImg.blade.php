@if(count($data['list']['data']) > 0)
<?php
    $tag = array_get($data, 'tag', 'default');
?>
<section {!! isset($data['htmlid'])?'id="'.$data['htmlid'].'"':'' !!} class="bg-primary bg-@nbColor($tag)-lt  pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 comp-cards-lists-lgRecImg-sqrRecImg {{isset($classList) ? $classList : ''}}">
    @if((array_get($data, 'url', '')) && (!empty($data['url'])))
    <h2>
        <a class="cl-ink-dk cl-@nbColor($tag)-dk ft-ter md-ter-bd-6 ter-bd-6 pd-t-10 pd-l-md-10 pd-r-md-10 dp-ib" href="{!! $data['url'] !!}">{!! $data['title'] !!}</a>
    </h2>
    @else
    <h2 class="cl-ink-dk cl-@nbColor($tag)-dk ft-ter md-ter-bd-6 ter-bd-6 pd-t-10 pd-l-md-10 pd-r-md-10">{!! $data['title'] !!}</h2>
    @endif

    @include('web-components::lists.lgRecImg', ['data' => array_splice($data['list']['data'], 0, 1), 'linkTargetAttr' => $data['list']['linkTargetAttr']])

    @include('web-components::lists.sqrRecImg', ['data' => $data['list']['data'], 'linkTargetAttr' => $data['list']['linkTargetAttr']])

    @if(isset($data['list']['link']['size']))
    <div class="pd-t-20 pd-b-10 pd-l-md-10">
        @include('web-components::links.link', ['link' => $data['list']['link'], 'classList' => ''])
    </div>
    @endif
    
</section>
@endif