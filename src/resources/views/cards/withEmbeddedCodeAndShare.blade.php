@if(array_get($data, 'html', '') && (!empty($data['html'])))
<section class="bg-primary  pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 {{isset($classList) ? $classList : ''}}">
    <div class="dp-fx fx-dr-col fx-dr-col-md-dac neg-mg-lr-px-10-md-ac pd-l-md-10 pd-r-md-10 fx-js-bw fx-al-ct ft-pr">
        <h1 class="cl-lt ft-pr bd-3 md-bd-3">{!! $data['title'] !!}</h1>
        <div class="dp-fx fx-js-ct-ac fx-js-ct-md-dac">
            @include('web-components::share.social-share', [ 'data' => $data['socialShare'], 'classList' => 'mg-t-20 mg-t-md-0', 'type' => ''])
        </div>
    </div>
    <div class="mg-t-20">{!! $data['html'] !!}</div>
</section>
@endif