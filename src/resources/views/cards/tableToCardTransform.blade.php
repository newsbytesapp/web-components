@if(isset($data))
<?php
$transformedData = [];
$transformedData['list'] = [];
$transformedData['list']['data'] = [];

foreach($data as $card){
    $item = [];
    if(isset($card['link'])){
        $item['link'] = $card['link'];
    }
    if(isset($card['name'])){
        $item['name'] = $card['name'];
    }
    $item['info'] = [];
    foreach($card['rowData'] as $row){
        $r = [];
        foreach($row as $cell){
            $cellInfo = [];
            if($cell['target']){
                $cellInfo = $cell;
                $cellInfo['name'] = $cell['content'];
                array_push($r, $cellInfo);
            }
        }
        if(!empty($r)) array_push($item['info'], $r);
    }
    array_push($transformedData['list']['data'], $item);
}
?>
@include('web-components::lists.infoImageRight', ['data' => $transformedData['list']['data'], 'type' => $type, 'linkTargetAttr' => ''])
@endif
