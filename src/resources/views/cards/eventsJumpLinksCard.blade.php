@if(sizeof($data['list']['data']) > 0)
    <div class="{{isset($classList) ? $classList : ''}}">
        <div class="ft-pr jump-links-parent">
            <div class="bd-4 md-bd-4 cl-ink-dk ft-pr">{!! $data['title'] !!}</div>
            <ul>
                @foreach($data['list']['data'] as $item)
                    <li class="mg-t-14">
                        <a class="dp-fx jump-link" href="{!! $item['url'] !!}" data-id="{{$data['htmlId']}}">
                            <span class="dp-ib wd-ht-px-5 br-rd-pr-50 bg-ink-dk fx-basis-5px mg-t-12"></span>
                            <span class="mg-l-10 reg-1 md-reg-1">{!! $item['title'] !!}</span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endif