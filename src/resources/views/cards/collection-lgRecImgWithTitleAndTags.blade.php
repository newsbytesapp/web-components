<?php
    $tag = array_get($data, 'tag', 'default');
?>
@if(count($data['list']['data']) > 0)
<section class=" bg-@nbColor($tag)-lt  mg-t-20 pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 {{isset($classList) ? $classList : ''}}">
    <div class="main-block dp-fx fx-js-bw fx-al-stretch  pd-l-10 pd-r-16 pd-t-10 pd-b-10">
        <div class="dp-fx fx-al-ct">
            <div class="cl-ink-dk ft-ter  ter-bd-4">{!! $data['title'] !!}</div>
            <div class="cl-razzmatazz-dk ft-ter ter-reg-2">{!! $data['description'] !!}
            </div>    
        </div>
        <span class="dp-fx fx-al-ct fz-18" >
            @if(isset($data['img']))
            <img src="{{$data['img']}}" alt="{{$data['title']}}"  class="wd-px-40">
            @endif
        </span>
    </div> 
    @foreach($data['list']['data'] as $item)
    @if(count($item['data']) > 0) 
    <div class="dp-fx fx-al-ct mg-b-8 mg-t-10 pd-l-10 pd-r-12">
        <div class="ft-ter fx-grow-1 mg-r-lg-10 ter-bd-6  mg-r-4">{!! $item['timestamp'] !!}</div> 
        <hr class=" bg-silver-lt fx-grow-2 opacity-50 mg-b-4 ht-px-1 wd-full">
    </div>    
    @include('web-components::lists.lgRecImgWithTags', ['data' => $item['data']])
    @endif 
    @endforeach   
</section>
@endif