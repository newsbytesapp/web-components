@if(count($data['list']['data']) > 0)
<div class="bg-primary pd-b-30 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac comp-cards-lists-sqrImg {{isset($classList) ? $classList : ''}}">
    <div class="ft-pr md-bd-4 bd-4 pd-l-md-20 pd-r-md-20 pd-t-20">{!! $data['title'] !!}</div>
    <section class="pd-l-md-10 pd-r-md-10">
        @include('web-components::lists.sqrRecImgVertical', ['data' => $data['list']['data'], 'type' => $type, 'linkTargetAttr' => $data['list']['linkTargetAttr']])
    </section>
</div>
@endif