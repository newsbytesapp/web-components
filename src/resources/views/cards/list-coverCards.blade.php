@if(count($data['list']['data']) > 0)
    @include('web-components::lists.coverCards', ['data' => $data['list']['data'], 'type' => $type, 'attributes' => $attributes])
@endif