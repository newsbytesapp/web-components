@if(count($data['list']['data']) > 0)
<?php
    $tag = array_get($data, 'tag', 'default');
    if(isset($htmlTags)){
        $markupTags = $htmlTags;
    }else{
        $markupTags = [];
    }
?>
<section {!! isset($data['htmlid'])?'id="'.$data['htmlid'].'"':'' !!} class="bg-primary  pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 comp-cards-lists-smRecImg-sqrRecImg {{isset($classList) ? $classList : ''}}">
    @if(isset($data['tagUrl']) && !empty($data['tagUrl']))
    <a class="cl-ink-dk ft-ter md-ter-bd-4 ter-bd-4 pd-t-10 pd-l-md-10 pd-r-md-10" href="{{$data['tagUrl']}}">{!! $data['title'] !!}</a>
    @else
    <div class="cl-ink-dk ft-ter md-ter-bd-4 ter-bd-4 pd-t-10 pd-l-md-10 pd-r-md-10">{!! $data['title'] !!}</div>
    @endif

    @include('web-components::lists.tagged-smRecImg', ['data' => array_splice($data['list']['data'], 0, 4), 'linkTargetAttr' => $data['list']['linkTargetAttr'], 'htmlTags' => $markupTags])

    @include('web-components::lists.tagged-sqrRecImg', ['data' => $data['list']['data'], 'linkTargetAttr' => $data['list']['linkTargetAttr'], 'htmlTags' => $markupTags])

    @if(!empty(array_get($data, 'list.pagination', [])))
    <div class="pd-t-20 pd-b-20 dp-fx fx-js-ct">
    @include('web-components::pagination.simple', ['data' => $data['list']['pagination'], 'classList' => ''])
    </div>
    @else

    @if(isset($data['list']['button']['size']))
    @if(isset($data['list']['button']['url']))
    <a href="{!! $data['list']['button']['url'] !!}" class="hidden disabled-link"></a>
    @endif
    <div class="pd-t-20 pd-b-20 pd-l-md-10 pd-r-md-10">
        @include('web-components::buttons.basic', [ 'button' => $data['list']['button'], 'type' => $type, 'searchFor' => '.comp-cards-lists-smRecImg-sqrRecImg', 'appendTo' => '.list-sqrRecImg', 'template' => 'taggedListItemSqrRecImg' ])
    </div>
    @endif

    @if((isset($data['list']['link']['size'])) && (isset($data['list']['link']['url'])))
    <div class="pd-t-20 pd-b-20 pd-l-md-10 pd-r-md-10">
        <a class="dp-bl wd-full bg-primary cl-link br-rd-10 tx-ct pd-t-14 pd-b-14 navy-blue-cl-hover alice-blue-bg-hover ft-ter md-ter-bd-2 ter-bd-2 load-more standard-bttn" href="{{$data['list']['link']['url']}}">{!! $data['list']['link']['title'] !!}</a>
    </div>
    @endif

    @endif
</section>
@endif