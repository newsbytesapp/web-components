<section class="bg-primary pd-t-20 pd-t-md-30 pd-r-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac ft-pr {{isset($classList) ? $classList : ''}}">
    @include('web-components::sectionHeaders.snapshot', [ 'data' => $data['sectionHeader'], 'classList' => ''])

    @include('web-components::lists.descriptiveListWithLinks', ['data' => $data['list'], 'classList' => ''])
</section>