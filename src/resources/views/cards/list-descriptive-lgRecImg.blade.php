@if(count($data['list']['data']) > 0)
<section class="bg-primary pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 {{isset($classList) ? $classList : ''}}">
    <div class="cl-lt ft-pr bd-3 md-bd-3 pd-t-10 pd-l-md-10 pd-r-md-10">{!! $data['title'] !!}</div>

    @include('web-components::lists.descriptive-lgRecImg', ['data' => $data['list']['data'], 'linkTargetAttr' => $data['list']['linkTargetAttr']])

</section>
@endif