<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
@switch($type)
    @case('amp')

    @break
    @default
        @switch($button['size'])
            @case('fixed')
                <button class="dp-ib bg-primary cl-link br-rd-10 pd-l-30 pd-r-30 pd-l-md-40 pd-r-md-40 pd-t-14 pd-b-14 navy-blue-cl-hover alice-blue-bg-hover ft-ter md-ter-bd-2 ter-bd-2">{!! $button['title'] !!}</button>
            @break
            @default
            <div class="load-more-parent">
                <button id="load-more-basic-{{$componentId}}" class="dp-bl wd-full bg-primary cl-link br-rd-10 tx-ct pd-t-14 pd-b-14 navy-blue-cl-hover alice-blue-bg-hover ft-ter md-ter-bd-2 ter-bd-2 load-more standard-bttn" data-search-for="{{ isset($searchFor) ? $searchFor : ''}}" data-append-to="{{ isset($appendTo) ? $appendTo : ''}}" data-api="{{isset($button['api']) ? $button['api'] : ''}}" data-template="{{ isset($template) ? $template : '' }}">{!! $button['title'] !!}</button>
                @include('web-components::toasts.dotsLoader')
                @include('web-components::toasts.error')
            </div>
            @break
        @endswitch
    @break
@endswitch