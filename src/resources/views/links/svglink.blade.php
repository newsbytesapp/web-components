<?php
    $linkType = array_get($link, 'type', 'internal');
    $htmlDownloadAttr = (array_get($link, 'download', false) ? 'download=' . substr($link['url'], strrpos($link['url'], '/') + 1) : '');
    $externalLinkSvg = '
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="mg-l-6">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="iconfinder_icon-external-link_2867894" transform="translate(6.000000, 6.000000)" fill="currentColor" fill-rule="nonzero">
                <path d="M10.6666667,2.27333333 L3.8,9.14 C3.63570083,9.32899768 3.37934304,9.40997717 3.13628801,9.34965644 C2.89323298,9.28933571 2.70448468,9.09789101 2.64761743,8.85400486 C2.59075019,8.61011871 2.67535764,8.35493529 2.86666667,8.19333333 L9.72,1.33333333 L7.33333333,1.33333333 C6.9651435,1.33333333 6.66666667,1.0348565 6.66666667,0.666666667 C6.66666667,0.298476833 6.9651435,0 7.33333333,0 L11.3333333,0 C11.7015232,0 12,0.298476833 12,0.666666667 L12,4.66666667 C12,5.0348565 11.7015232,5.33333333 11.3333333,5.33333333 C10.9651435,5.33333333 10.6666667,5.0348565 10.6666667,4.66666667 L10.6666667,2.27333333 Z M9.33333333,7.33333333 C9.33333333,6.9651435 9.63181017,6.66666667 10,6.66666667 C10.3681898,6.66666667 10.6666667,6.9651435 10.6666667,7.33333333 L10.6666667,10.6666667 C10.6666667,11.4030463 10.069713,12 9.33333333,12 L1.33333333,12 C0.596953667,12 0,11.4030463 0,10.6666667 L0,2.66666667 C0,1.93333333 0.6,1.33333333 1.33333333,1.33333333 L4.66666667,1.33333333 C5.0348565,1.33333333 5.33333333,1.63181017 5.33333333,2 C5.33333333,2.36818983 5.0348565,2.66666667 4.66666667,2.66666667 L1.33333333,2.66666667 L1.33333333,10.6666667 L9.33333333,10.6666667 L9.33333333,7.33333333 Z" id="Shape"></path>
            </g>
        </g>
    </svg>
    ';
    $internalLinkSvg = '
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="mg-l-6">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
            <g id="Group-5" transform="translate(2.000000, 9.000000)" stroke="#000000" stroke-width="2">
                <line x1="-2.27318164e-13" y1="3" x2="20" y2="3" id="Path-2"></line>
                <polyline id="Path-3" stroke-linejoin="round" points="17 0 20 3 17 6"></polyline>
            </g>
        </g>
    </svg>
    ';
?>
@switch($link['size'])
    @case('fixed')
        <div class="dp-ib ft-pr {{isset($classList) ? $classList : ''}}">
            <a class="dp-fx fx-al-ct cl-link navy-blue-cl-hover standard-link" href="{!! (isset($type) && $type == 'amp') ? (isset($link['ampurl']) ? $link['ampurl'] : $link['url']) : $link['url'] !!}" {!! $link['targetAttr'] == "_blank" ? 'target="_blank"' : '' !!} {!! $linkType == "external" ? 'rel="_noopener"' : '' !!} aria-label="Link to {!! $link['title'] !!}" {!! $htmlDownloadAttr !!}>
                <span class="ft-ter md-ter-bd-2 ter-bd-2">{!! $link['title'] !!}</span>
                @if($linkType == 'external')
                {!! $externalLinkSvg !!}
                @else
                {{!! $internalLinkSvg !!}}
                @endif
            </a>
        </div>
    @break
    @default
    <div class="dp-bl wd-full ft-pr {{isset($classList) ? $classList : ''}}">
        <a class="dp-fx fx-al-ct cl-link navy-blue-cl-hover standard-link" href="{!! (isset($type) && $type == 'amp') ? (isset($link['ampurl']) ? $link['ampurl'] : $link['url']) : $link['url'] !!}" {!! $link['targetAttr'] == "_blank" ? 'target="_blank"' : '' !!} {!! $linkType == "external" ? 'rel="_noopener"' : '' !!} aria-label="Link to {!! $link['title'] !!}" {!! $htmlDownloadAttr !!}>
            <span class="ft-ter md-ter-bd-2 ter-bd-2">{!! $link['title'] !!}</span>
            @if($linkType == 'external')
            {!! $externalLinkSvg !!}
            @else
            {{!! $internalLinkSvg !!}}
            @endif
        </a>
    </div>
    @break
@endswitch
   