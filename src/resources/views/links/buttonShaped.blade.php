@switch(array_get($data, 'size', 'free'))
    @case('fixed')
        <div class="dp-ib ft-pr {{isset($classList) ? $classList : ''}}">
            <a class="br-rd-10 pd-l-30 pd-r-30 pd-l-md-40 pd-r-md-40 pd-t-14 pd-b-14 bg-accent tx-ct navy-blue-bg-hover bd-6 md-bd-6 dp-bl cl-primary" href="{!! $type == 'amp' ? (isset($data['ampurl']) ? $data['ampurl'] : $data['url']) : $data['url'] !!}" {!! array_get($data, 'targetAttr', false) ? 'target="_blank"' : '' !!} aria-label="Button Link">{!! $data['title'] !!}</a>
        </div>
    @break
    @default
    <div class="dp-bl wd-full ft-pr {{isset($classList) ? $classList : ''}}">
        <a class="br-rd-10 pd-t-14 pd-b-14 bg-accent tx-ct navy-blue-bg-hover bd-6 md-bd-6 dp-bl cl-primary" href="{!! $type == 'amp' ? (isset($data['ampurl']) ? $data['ampurl'] : $data['url']) : $data['url'] !!}" {!! array_get($data, 'targetAttr', false) ? 'target="_blank"' : '' !!} aria-label="Button Link">{!! $data['title'] !!}</a>
    </div>
    @break
@endswitch
   