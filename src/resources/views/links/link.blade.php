<?php
    $linkType = array_get($link, 'type', 'internal');
    $htmlDownloadAttr = (array_get($link, 'download', false) ? 'download=' . substr($link['url'], strrpos($link['url'], '/') + 1) : '');
?>
@switch($link['size'])
    @case('fixed')
        <div class="dp-ib ft-pr {{isset($classList) ? $classList : ''}}">
            <a class="dp-fx fx-al-ct cl-link navy-blue-cl-hover standard-link" href="{!! (isset($type) && $type == 'amp') ? (isset($link['ampurl']) ? $link['ampurl'] : $link['url']) : $link['url'] !!}" {!! $link['targetAttr'] == "_blank" ? 'target="_blank"' : '' !!} {!! $linkType == "external" ? 'rel="_noopener"' : '' !!} aria-label="Link to {!! $link['title'] !!}" {!! $htmlDownloadAttr !!}>
                <span class="ft-ter md-ter-bd-3 ter-bd-3">{!! $link['title'] !!}</span>
                <span class="{{$linkType == 'external' ? 'nb-icon-external-link' : 'nb-icon-arrow-forward'}} fz-20 mg-t-6 mg-l-6"></span>
            </a>
        </div>
    @break
    @default
    <div class="dp-bl wd-full ft-pr {{isset($classList) ? $classList : ''}}">
        <a class="dp-fx fx-al-ct cl-link navy-blue-cl-hover standard-link" href="{!! (isset($type) && $type == 'amp') ? (isset($link['ampurl']) ? $link['ampurl'] : $link['url']) : $link['url'] !!}" {!! $link['targetAttr'] == "_blank" ? 'target="_blank"' : '' !!} {!! $linkType == "external" ? 'rel="_noopener"' : '' !!} aria-label="Link to {!! $link['title'] !!}" {!! $htmlDownloadAttr !!}>
            <span class="ft-ter md-ter-bd-3 ter-bd-3">{!! $link['title'] !!}</span>
            <span class="{{$linkType == 'external' ? 'nb-icon-external-link' : 'nb-icon-arrow-forward'}} fz-20 mg-t-6 mg-l-6"></span>
        </a>
    </div>
    @break
@endswitch
   