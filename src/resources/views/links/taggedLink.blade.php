<?php
    $tag = 'default';
    if(isset($data['tag']) && ($data['tag'] != '')){
        $tag = $data['tag'];
    }
?>
<a href="{!! $data['url'] !!}" class="{{$attributes['classListContainer']}}">
    <div class="tx-container {{$attributes['classListTextContainer']}}">
        <div class="{{$attributes['classListTitle']}}">{!! $data['title'] !!}</div>
        <div class="{{$attributes['classListTag']}} cl-@nbColor($tag)-dk">{!! $data['tag'] !!}</div>
    </div>
</a>