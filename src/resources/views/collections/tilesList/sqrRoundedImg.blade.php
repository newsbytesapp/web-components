<div class="{{isset($classList) ? $classList : ''}} ft-pr">
    <div class="cl-ink-dk bd-2 md-bd-2 tx-ct">{!! $data['list']['title'] !!}</div>
    <div class="fx-row neg-mg-lr-px-10-ac fx-js-ar-ac fx-js-ar-md-dac">
        @foreach($data['list']['data'] as $item)
        <div class="pd-l-10 pd-r-10">
            <div class="wd-ht-px-100">
                <img data-src="{{$item['thumb']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-full ht-full mg-t-20 dp-bl"/>
            </div>
            <div class="mg-t-10 tx-ct bd-6 md-bd-6 wd-px-100">{!! $item['title'] !!}</div>
            <div class="cl-lt tx-ct reg-2 md-reg-2 wd-px-100">{!! $item['subtitle'] !!}</div>
        </div>
        @endforeach
    </div>
</div>