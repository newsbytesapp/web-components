<div class="dp-ib mg-r-10 {{isset($data['classList']) ? $data['classList'] : ''}}">
    <a href="{!! $type == 'amp' ? (isset($data['ampurl']) ? $data['ampurl'] : $data['url']) : $data['url'] !!}" class="pd-t-10 pd-b-10 pd-l-10 pd-r-20 ft-pr bg-primary br-rd-px-30 dp-fx fx-al-ct min-ht-px-60">
    @if(isset($data['icon']) && (!empty($data['icon'])))
        @switch($type)
            @case('amp')
            <amp-img src="{{$data['icon']}}" alt="{{$data['title']}}" width="40" height="40" layout="fixed" class="br-rd-pr-50 fx-basis-40px"></amp-img>
            @break
        @default
            <img data-src="{{$data['icon']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="lazy wd-ht-px-40 br-rd-pr-50 fx-basis-40px"/>
            @break
        @endswitch
    @endif
        <span class="reg-2 md-reg-2 pd-l-10 wht-sp-nowrap">{!! $data['title'] !!}</span>
    </a>
</div>