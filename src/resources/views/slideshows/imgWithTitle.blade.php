<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
@if(array_get($data, 'list.data', []) && (sizeof($data['list']['data']) > 0))
<section class="bg-primary slide-show-parent visibility-hidden  pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 {{isset($classList) ? $classList : ''}}">
    <div class="cl-lt ft-pr bd-3 md-bd-3">{!! $data['title'] !!}</div>
    <section class="mg-t-20 slide-show default" id="slideshow-imt-{{$componentId}}">
        @foreach($data['list']['data'] as $item)
            <div class="min-wd-px-100 mg-r-20">
                <a href="{!! $item['url'] !!}">
                    <img alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="dp-bl wd-full" src="{{$item['img']}}">
                    <div class="bd-3 md-bd-3 cl-ink-dk mg-t-14">{!! $item['title'] !!}</div>
                </a>
            </div>
        @endforeach
    </section>
    <div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
</section>
@endif