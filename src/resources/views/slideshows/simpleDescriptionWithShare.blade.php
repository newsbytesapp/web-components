<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
@if(count($data['list']['data']) > 0)
<section class="pos-rel slide-show-parent visibility-hidden  pd-t-40 pd-b-22 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 {{isset($classList) ? $classList : ''}}">
    <section class="slide-show default with-adaptiveHeight" id="slideshow-sds-{{$componentId}}">
        @foreach($data['list']['data'] as $item)
            <div class="min-wd-px-100">
                <div class="bd-4 md-bd-4">{!! $item['title'] !!}</div>
                <div class="mg-t-10 reg-1 md-reg-1">{!! $item['description'] !!}</div>
                @if(array_get($item, 'socialShare', []) && (sizeof($item['socialShare']) > 0))
                    @include('web-components::share.horizontalList-hzCenterVtCenterIcons', ['data' => $item['socialShare'], 'type' => $type, 'classList' => 'mg-t-10 fx-js-ct'])
                @endif
            </div>
        @endforeach
    </section>
    <div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
</section>
@endif