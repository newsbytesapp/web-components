<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
@if(array_get($data, 'list.data', []) &&(sizeof($data['list']['data']) > 0))
    <?php
        $htmlTargetAttr = (array_get($data, 'list.linkTargetAttr', '') ? (!empty($data['list']['linkTargetAttr']) ? 'target=' . $data['list']['linkTargetAttr'] : '') : '');
    ?>
    <section class="slide-show-parent visibility-hidden  ovr-hidden {{isset($classList) ? $classList : ''}}">
        <div class="slide-show default" id="slideshow-recimg-{{$componentId}}">
            @foreach($data['list']['data'] as $item)
                <a href="{!! $item['url'] !!}" class="wd-full pos-rel dp-bl fig fig--tt-black-bt" {{$htmlTargetAttr}}>
                    <img src="{{$item['ximg']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="wd-full">
                    @if($loop->first)
                    <h1 class="pos-abs lt-init bt-init pd-b-10 pd-b-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 wd-full cl-primary bd-1 ft-pr md-bd-8 bd-8 bg-transparent z-index-2">{!! $item['title'] !!}</h1>
                    @else
                    <h4 class="pos-abs lt-init bt-init pd-b-10 pd-b-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 wd-full cl-primary bd-1 ft-pr md-bd-8 bd-8 bg-transparent z-index-2">{!! $item['title'] !!}</h4>
                    @endif
                </a>
            @endforeach
        </div>
        <div class="append-slide-show-dots slide-show-bars dark-theme pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 bg-ink-dk pd-t-20 pd-b-20"></div>
        <div class="fx-row bg-ink-dk pd-b-30 cl-suva-grey pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 hidden slide-items-mapper">
            @foreach($data['list']['data'] as $item)
                <a href="{!! $item['url'] !!}" class="col-12 col-md-4 dp-fx pd-l-md-10 pd-r-md-10 mg-t-10 slide-item {{$loop->first ? 'cl-primary' : 'cl-suva-grey'}}" id="slide-item-{{$loop->index}}" {{$htmlTargetAttr}}>
                    <div class="col-4 col-md-1 fig-container--wd-ht-md-60">
                        <figure class="lazy fig fig--bg-img-ct-cov-props ht-full-dac ht-full-md-ac asp-ratio-rect-ac asp-ratio-rect-md-dac" data-src="{{$item['thumb']}}" title="{{$item['title']}}">
                            <img data-src="{{$item['thumb']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy hidden inset-img-hidden"/>
                        </figure>
                    </div>
                    <div class="tx-container col-8 pd-l-10">
                        <div class="ft-pr bd-6">{!! $item['title'] !!}</div>
                    </div>
                </a>
            @endforeach
        </div>
    </section>
@endif