@switch($type)
@case('amp')
<amp-apester-media data-apester-media-id="{{$data['id']}}" height="{{$data['height']}}"></amp-apester-media>
@break
@default
<div class="apester-media" data-media-id="{{$data['id']}}" height="{{$data['height']}}"></div>
<script async src="//static.apester.com/js/sdk/v2.0/apester-javascript-sdk.min.js"></script>
@break
@endswitch