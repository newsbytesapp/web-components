@switch($type)
@case('amp')
<amp-facebook width="{{$data['width']}}" height="{{$data['height']}}" layout="responsive" data-href="{!! $data['link'] !!}" data-embed-type="{{$data['embedtype']}}" data-embed-as="{{$data['embedtype']}}" @if($data['embedtype']=='comment') data-include-comment-parent="{{$data['includeparent']}}" @endif data-align-center="true" data-show-text="{{$data['showtext']}}" ></amp-facebook>
@break
@default
<script type="text/javascript">var hasFb = true;</script>
<div id="fb-root"></div>
<?php 
    switch($data['embedtype']){
        case 'comment':
            echo '<div class="fb-comment-embed" data-href="'.$data['link'].'" data-width="'.$data['width'].'" data-include-parent="'.$data['includeparent'].'"></div>';
        break;
        case 'video':
            echo '<div class="fb-video" data-href="'.$data['link'].'" data-width="'.$data['width'].'" data-show-text="'.$data['showtext'].'"></div>';
        break;
        case 'post':
            echo '<div class="fb-post" data-href="'.$data['link'].'" data-width="'.$data['width'].'" data-show-text="'.$data['showtext'].'"></div>';
        break;
    }
?>
@break
@endswitch