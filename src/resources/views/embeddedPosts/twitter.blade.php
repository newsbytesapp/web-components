@switch($type)
@case('amp')
    @if ($data['hideMedia'])
    <amp-twitter width="1" height="1" layout="responsive" data-tweetid="{{last(explode('/', trim(explode('?', $data['link'])[0], '/')))}}" data-cards="hidden"></amp-twitter>
    @else
    <amp-twitter width="1" height="1" layout="responsive" data-tweetid="{{last(explode('/', trim(explode('?', $data['link'])[0], '/')))}}"></amp-twitter>
    @endif
@break
@default
    @if ($data['hideMedia'])
    <blockquote class="twitter-tweet" data-cards="hidden" data-lang="en">{!! $data['text'] !!}</blockquote>
    @else
    <blockquote class="twitter-tweet" data-lang="en">{!! $data['text'] !!}</blockquote>
    @endif
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
@break
@endswitch