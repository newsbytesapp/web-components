<div class="bg-primary  pd-b-10 mg-t-10 mg-t-md-20 {{isset($classList) ? $classList : ''}}">
    <div class="cl-ink-dk ft-pr md-bd-4 bd-4 pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20">{!! $data['title'] !!}</div>
    <div class="br-t-1p-white-smoke pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20">
        <div id="LB24_LIVE_CONTENT" data-url="https://embed.24liveblog.com/" data-eid="{{$data['id']}}"></div>
        <script src="https://v.24liveblog.com/24.js" ></script>
    </div>
</div>