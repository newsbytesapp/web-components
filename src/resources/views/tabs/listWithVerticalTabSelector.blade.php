<div class="vertical-tab-container dp-fx fx-dr-col fx-dr-row-md-ac" style="align-items: start">
    <div class="vertical-tab-sticky-div tp-init pos-sticky ht-0 visibility-hidden"></div>
    <div class="vertical-tabs fx-1 fx-dr-col-md-ac dp-fx fx-dr-row pos-sticky tp-init" >
        @foreach ($data['description'] as $i => $tabDescription)
            <li class="vertical-tab list-style-none {{$i==0 ? 'active':''}} ft-ter ter-bd-2 tx-md-lt tx-ct cs-ptr pd-l-md-18 pd-t-md-14 pd-b-md-14 pd-r-10 pd-b-10 pd-l-10 pd-t-10 tab-{{ $i + 1 }} br-b-1p-pale-lavender cl-dove-grey" data-tab-id="tab-{{ $i + 1 }}">{{ $tabDescription }}</li>
        @endforeach
    </div>
    <div class="vertical-tab-content  fx-2   pd-t-10 pd-b-10 pd-r-md-10 pd-b-md-16 pd-l-md-10 pd-t-md-16 ovr-y-scroll">
        @foreach ($data['content'] as $index => $tabContent)
            <div class="vtab-content mg-b-20" id="tab-{{ $index + 1 }}">
                @include('web-components::lists.titleListWithRoundImg', ['data' => $tabContent])
            </div>
        @endforeach
    </div>
</div>
@section('css')
@parent
    <style type="text/css">
        /* .vertical-tab:hover {
            background-color: #ddd;
        } */
        .vertical-tab-content {
            border-left: 1px solid #edecee;
            scrollbar-width: none;
        }
        .vertical-tab-content::-webkit-scrollbar {
            width: none;
            background-color: transparent;
            display: none;
        }
    
        .vertical-tab-content::-webkit-scrollbar-track {
            width: none;
            background-color: transparent;
            display: none;
        }
        
        .vertical-tab-content::-webkit-scrollbar-thumb {
            width: none;
            background-color: transparent;
            display: none;
        }
        .vertical-tab.active {
            color: #064893;
            border-bottom: 1px solid #064893;
            /*  br-b-1p-link  cl-link */
        }
    
        /* Media Query for Mobile */
        @media screen and (max-width: 767px) {
            .vertical-tabs
            {
                overflow: auto;
                background-color:white;
                width:100%;
            }
            .vertical-tab {
                margin: 0;
                /* border: none; */
                white-space: nowrap;
                border-right: 1px solid #edecee;
                flex-grow: 1;
            }
            .vertical-tab-content {
                border:none; 
                width: 100%;           
            }
        }
    </style>
     @if(isset($data['type']) && $data['type'] == 'mobile')
        <style type="text/css">
        .vertical-tab.active {
                color:#ffffff;
                background-color: black;
                /*  br-b-1p-link  cl-link */
            }
            @media screen and (max-width: 767px) {
                .vertical-tab.active {
                    color: #064893;
                    border-bottom: 1px solid #064893;
                    background-color: transparent;
                /*  br-b-1p-link  cl-link */
                }
            }
        </style>
   @endif
@stop