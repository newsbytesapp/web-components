<div class="bg-primary pd-t-10 pd-b-10 {{isset($classList) ? $classList : ''}}">
    <div class="tabs wht-sp-nowrap pos-rel fz-0" id="{{$data['id']}}">
        @switch($type)
        @case('amp')
            @foreach ($data['tabularSection'] as $item)
            <input type="radio" id="tab-{{$loop->iteration}}-{{$data['id']}}" name="primary-tab-{{$data['id']}}" class="tab-switch-{{$loop->iteration}} hidden" {{$loop->first ? 'checked' : ''}} />
            <label for="tab-{{$loop->iteration}}-{{$data['id']}}" class="cs-ptr dp-ib tab-label tx-ct cl-suva-grey ft-ter md-ter-bd-2 ter-bd-2 pd-t-8 pd-b-8">{!! $item['tab']['title'] !!}</label>
            @endforeach
            <div class="slider pos-abs"></div>
            <div class="tab-content pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac wht-sp-normal">
                @foreach($data['tabularSection'] as $tabularSection)
                <div class="hidden content-{{$loop->iteration}}">
                    @if(isset($tabularSection['tab']['url']))
                    <a class="ft-ter md-ter-bd-4 ter-bd-4 pd-l-md-20 pd-r-md-20 pd-t-20 dp-bl" href="{{$tabularSection['tab']['url']}}">{!! $tabularSection['tab']['title'] !!}</a>
                    @else
                    <div class="ft-ter md-ter-bd-4 ter-bd-4 pd-l-md-20 pd-r-md-20 pd-t-20">{!! $tabularSection['tab']['title'] !!}</div>
                    @endif
                    <div class="pd-l-md-20 pd-r-md-20">
                        @include('web-components::lists.tagged-sqrRecImgVertical', ['data' => $tabularSection['content']['data']['list']['data'], 'type' => $type, 'linkTargetAttr' => $tabularSection['content']['data']['list']['linkTargetAttr']])
                    </div>
                    @if(isset($tabularSection['content']['link']))
                    <a href="{!! $tabularSection['content']['link']['url'] !!}" class="dp-fx fx-js-ct pd-t-10 pd-b-10 mg-t-10" aria-label="Button Link">
                        <span class="cl-link bd-6 md-bd-6">{!! $tabularSection['content']['link']['title'] !!}</span>
                    </a>
                    @endif
                </div>
                @endforeach
            </div>
        @break
        @default
            <div class="pos-rel">
                <div class="hz-scroll-ac scroll-menu dp-fx tabs-list slider-tabs">
                    @foreach ($data['tabularSection'] as $item)
                    <div class="cs-ptr cl-suva-grey ft-ter md-ter-bd-2 ter-bd-2 pd-t-8 pd-b-8 pd-l-24 pd-r-24 tab-item {{$loop->first ? 'active' : ''}}" data-id="tbc-{{$loop->index}}">{!! $item['tab']['title'] !!}</div>
                    @endforeach
                </div>
            </div>
            <div class="pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac wht-sp-normal">
                @foreach($data['tabularSection'] as $tabularSection)
                <div class="{{$loop->first ? '' : 'hidden'}} tab-content" data-id="tbc-{{$loop->index}}">
                    @if(isset($tabularSection['tab']['url']))
                    <a class="ft-ter md-ter-bd-4 ter-bd-4 pd-l-md-20 pd-r-md-20 pd-t-20 dp-bl" href="{{$tabularSection['tab']['url']}}">{!! $tabularSection['tab']['title'] !!}</a>
                    @else
                    <div class="ft-ter md-ter-bd-4 ter-bd-4 pd-l-md-20 pd-r-md-20 pd-t-20">{!! $tabularSection['tab']['title'] !!}</div>
                    @endif
                    <div class="pd-l-md-10 pd-r-md-10">
                        @include('web-components::lists.tagged-sqrRecImgVertical', ['data' => $tabularSection['content']['data']['list']['data'], 'type' => $type, 'linkTargetAttr' => $tabularSection['content']['data']['list']['linkTargetAttr']])
                    </div>
                    @if(isset($tabularSection['content']['link']))
                    <a href="{!! $tabularSection['content']['link']['url'] !!}" class="dp-fx fx-js-ct pd-t-10 pd-b-10 mg-t-10" aria-label="Button Link">
                        <span class="cl-link ft-ter md-ter-bd-2 ter-bd-2">{!! $tabularSection['content']['link']['title'] !!}</span>
                    </a>
                    @endif
                </div>
                @endforeach
            </div>
        @break
        @endswitch
    </div>
</div>
@section('css')
@parent
    @if($type == 'amp')
        .tabs#{{$data['id']}} label, .tabs#{{$data['id']}} .slider {
            width: calc(100% / {{count($data['tabularSection'])}});
            white-space: normal;
        }

        @for ($i = 1; $i <= count($data['tabularSection']); $i++)
        .tabs#{{$data['id']}} input:nth-of-type({{$i}}):checked ~ .slider {
            left: calc((100% / {{count($data['tabularSection'])}}) * {{$i - 1}});
        }
        @endfor
    @else
        <style type="text/css">
        .tabs#{{$data['id']}} .tab-item.active {
            color: #000000;
            border-bottom: 1px solid #000000;
        }
        </style>
    @endif
@stop