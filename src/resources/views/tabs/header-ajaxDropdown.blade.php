<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
if(isset($data['selectedTab'])){
    $privileged = $data['selectedTab'] - 1;
}else{
    $privileged = 0;
}
?>
<div class="bg-primary  pd-t-10 pd-b-10 load-template-parent ft-pr {{isset($classList) ? $classList : ''}}" data-component="ajax-tabs-header-dropdown">
    <div class="tabs wht-sp-nowrap pos-rel fz-0" id="{{$data['id']}}">
        @foreach ($data['tabularSection'] as $item)
            <?php
                if($loop->index == $privileged){
                    $ajaxLabel = false;
                }else{
                    $ajaxLabel = true;
                }
            ?>
            <input type="radio" id="tab-{{$loop->iteration}}-{{$data['id']}}" name="primary-tab-{{$data['id']}}" class="tab-switch-{{$loop->iteration}} hidden" {{$loop->index == $privileged ? 'checked' : ''}} />
            
            @if($ajaxLabel)
            <label for="tab-{{$loop->iteration}}-{{$data['id']}}" id="load-{{componentId}}-{{$loop->iteration}}" class="cs-ptr dp-ib tab-label tx-ct cl-lt md-bd-6 bd-6 pd-t-6 pd-b-16 load-template template-tab search-for" data-url="{{$item['tab']['api']}}" data-template="{{$item['ajaxRenderInfo']['template']}}" data-loading-practise ="{{isset($item['ajaxRenderInfo']['loadingPractise']) ? $item['ajaxRenderInfo']['loadingPractise'] : ''}}" data-mapped-to="content-{{$loop->iteration}}"><a href="{!! $item['tab']['url'] !!}" class="link-disabled {{$item['tab']['linkable'] ? '' : 'hidden'}}">{!! $item['tab']['title'] !!}</a></label>
            @else
            <label for="tab-{{$loop->iteration}}-{{$data['id']}}" class="cs-ptr dp-ib tab-label tx-ct cl-lt md-bd-6 bd-6 pd-t-6 pd-b-16 search-for template-tab" data-mapped-to="content-{{$loop->iteration}}"><a href="{!! $item['tab']['url'] !!}" class="link-disabled {{$item['tab']['linkable'] ? '' : 'hidden'}}">{!! $item['tab']['title'] !!}</a></label>
            @endif
        @endforeach

        <div class="slider pos-abs"></div>

        <section class="tab-content wht-sp-normal">
            @foreach ($data['tabularSection'] as $item)
            <div class="content content-{{$loop->iteration}} search-for hidden" data-mapped-to="content-{{$loop->iteration}}">

               
                <div class="{{$item['content']['header']['classList']}}">

                    <div class="md-bd-4 bd-4 header-title">{!! $item['content']['header']['title'] !!}</div>

                    @if((isset($item['content']['header']['dropdown'])) && isset($item['content']['header']['dropdown']['size']))
                        @include('web-components::dropdowns.ajax',  ['data'=> $item['content']['header']['dropdown'], 'type'=> $type, 'classList' => '', 'ajaxRenderInfo' => $item['content']['header']['ajaxRenderInfo']]) 
                    @endif

                </div>

                <div class="render">
                    @if(!isset($item['ajaxRenderInfo']))
                    
                        <?php
                            $listicalData = $item['content'];
                        ?>

                        @if((isset($listicalData['list']['button'])) && (count($listicalData['list']['button']) > 0))

                            @include('web-components::' . $item['component'], ['classList' => '', 'type' => $type, 'data' => $listicalData['list']['data'], 'linkTargetAttr' => $listicalData['list']['linkTargetAttr'], 'button' => $listicalData['list']['button']])

                        @elseif((isset($listicalData['list']['link'])) && (count($listicalData['list']['link']) > 0))

                            @include('web-components::' . $item['component'], ['classList' => '', 'type' => $type, 'data' => $listicalData['list']['data'], 'linkTargetAttr' => $listicalData['list']['linkTargetAttr'], 'link' => $listicalData['list']['link']])

                        @else

                            @include('web-components::' . $item['component'], ['classList' => '', 'type' => $type, 'data' => $listicalData['list']['data'], 'linkTargetAttr' => $listicalData['list']['linkTargetAttr']])

                        @endif

                    @endif
                </div>

                @include('web-components::toasts.dotsLoader')
                @include('web-components::toasts.error')
            </div>
            @endforeach
        </section>
    </div>
</div>

<style>

    .tabs#{{$data['id']}} label, .tabs#{{$data['id']}} .slider {
        width: calc(100% / {{count($data['tabularSection'])}});
        white-space: normal;
    }

    @for ($i = 1; $i <= count($data['tabularSection']); $i++)
    .tabs#{{$data['id']}} input:nth-of-type({{$i}}):checked ~ .slider {
        left: calc((100% / {{count($data['tabularSection'])}}) * {{$i - 1}});
    }
    @endfor

</style>
