<div class="bg-primary  pd-b-30 pd-t-10 comp-tabs-simpleTitle-listsSqrImg ft-pr {{isset($classList) ? $classList : ''}}">
    @if(isset($data['title']))
    <div class="md-bd-4 bd-4 pd-l-md-20 pd-r-md-20 pd-t-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">{!! $data['title'] !!}</div>
    @endif
    <div class="tabs {{ isset($data['title']) ? 'mg-t-20' : 'mg-t-6' }} wht-sp-nowrap pos-rel fz-0" id="{{$data['id']}}">
        @foreach ($data['tabularSection'] as $item)
        <input type="radio" id="tab-{{$loop->iteration}}-{{$data['id']}}" name="primary-tab-{{$data['id']}}" class="tab-switch-{{$loop->iteration}} hidden" {{$loop->first ? 'checked' : ''}} />
        @if(isset($item['tab']['linkable']) && $item['tab']['linkable'])
        <label for="tab-{{$loop->iteration}}-{{$data['id']}}" class="cs-ptr dp-ib tab-label tx-ct cl-lt md-bd-6 bd-6 pd-t-10 pd-b-10"><a href="{!! $item['tab']['url'] !!}" class="link-disabled {{$item['tab']['linkable'] ? '' : 'hidden'}}">{!! $item['tab']['title'] !!}</a></label>
        @else
        <label for="tab-{{$loop->iteration}}-{{$data['id']}}" class="cs-ptr dp-ib tab-label tx-ct cl-lt md-bd-6 bd-6 pd-t-10 pd-b-10">{!! $item['tab']['title'] !!}</label>
        @endif
        @endforeach
        <div class="slider pos-abs"></div>
        <section class="tab-content wht-sp-normal">
            @foreach ($data['tabularSection'] as $tabularSection)
            <div class="pd-l-md-10 pd-r-md-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac content-{{$loop->iteration}} hidden">
                @if(sizeof(array_get($tabularSection, 'list.data', [])) > 0)
                @if(isset($largerVersion) && $largerVersion)
                @include('web-components::lists.sqrImg', 
                ['data'=> $tabularSection['list']['data'], 'type' => $type, 'linkTargetAttr' => $tabularSection['list']['linkTargetAttr'], 'largerVersion' => $largerVersion])
                @else
                @include('web-components::lists.sqrImg', 
                ['data'=> $tabularSection['list']['data'], 'type' => $type, 'linkTargetAttr' => $tabularSection['list']['linkTargetAttr']])
                @endif
                @else
                <div class="dp-fx fx-js-ct pd-t-30 pd-b-30">
                    <div class="col-6">
                        <img src="{{array_get($tabularSection, 'void.img', config('web-components.services.'.config('web-components.services.assetSource').'.url', '').'/assets/images/no-data.png')}}" alt="No Data Available Image" class="obj-fit-cover wd-full">
                        <div class="bd-5 cl-ink-dk md-bd-5 mg-t-10 tx-ct">{!! array_get($tabularSection, 'void.title', 'Data not available yet.') !!}</div>
                    </div>
                </div>
                @endif
            </div>
            @endforeach
        </section>
    </div>
</div>

<style>

    .tabs#{{$data['id']}} label, .tabs#{{$data['id']}} .slider {
        width: calc(100% / {{count($data['tabularSection'])}});
        white-space: normal;
    }

    @for ($i = 1; $i <= count($data['tabularSection']); $i++)
    .tabs#{{$data['id']}} input:nth-of-type({{$i}}):checked ~ .slider {
        left: calc((100% / {{count($data['tabularSection'])}}) * {{$i - 1}});
    }
    @endfor

</style>
