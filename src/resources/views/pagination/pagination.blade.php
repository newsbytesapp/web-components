<?php
    function validateKey($key){
        if($key != null){
            return true;
        }else{
            return false;
        }
    }
     
    $prevPgLink = validateKey($data['prev_page_url']);
    $nextPgLink = validateKey($data['next_page_url']);

    $current = $data['current_page'];
    $last = $data['last_page'];
    $path = $data['path'];
    $urlColl = [];

    if($current <= $windowSize){
        for($i=0; $i < $windowSize; $i++){
            $item = [];
            $item['url'] = $path . '?page=' . ($i+1); 
            $item['val'] = $i + 1;
            array_push($urlColl, $item);
        }
        $extremeStartPoint = false;
        $extremeEndPoint = true;
    }else if(($current + $windowSize) >= $last){
        $start = $last - $windowSize;
        for($i=$start; $i <= $last; $i++){
            $item = [];
            $item['url'] = $path . '?page=' . ($i); 
            $item['val'] = $i;
            array_push($urlColl, $item);
        }
        $extremeStartPoint = true;
        $extremeEndPoint = false;
    }else {
        $start = $current - (($windowSize - ($windowSize%2))/2);
        $final = $current + (($windowSize - ($windowSize%2))/2);
        for($i=$start; $i <= $final; $i++){
            $item = [];
            $item['url'] = $path . '?page=' . ($i); 
            $item['val'] = $i;
            array_push($urlColl, $item);
        }
        $extremeStartPoint = true;
        $extremeEndPoint = true;
    }
    
?>
@if(count($urlColl) > 0)
<div class="bg-primary dp-fx fx-al-ct pd-t-20 pd-b-20 pd-l-40 pd-r-40 br-rd-10 ft-pr reg-2 md-reg-2 {{isset($classList) ? $classList : ''}}">
    <a class="md-bd-6 bd-6 mg-r-10 {{$prevPgLink ? '' : 'cl-lt link-disabled'}}" {{$prevPgLink ? 'href=' .$data['prev_page_url'] : ''}}>Prev</a>
    <a class="mg-l-10 mg-r-10 cl-lt {{$extremeStartPoint ? '' : 'hidden'}}" href="{!! $data['first_page_url'] !!}">1 ...</a>
    <div class="dp-fx cl-lt">
        @foreach($urlColl as $item)
        <a class="mg-l-6 mg-r-6 mg-l-md-10 mg-r-md-10 {{$item['val'] == $current ? 'cl-ink-dk' : ''}}" href="{!! $item['url'] !!}">{{$item['val']}}</a>
        @endforeach
    </div>
    <a class="mg-l-10 mg-r-10 cl-lt {{$extremeEndPoint ? '' : 'hidden'}}" href="{!! $data['last_page_url'] !!}">... {{$last}}</a>
    <a class="md-bd-6 bd-6 mg-l-10 {{$nextPgLink ? '' : 'cl-lt link-disabled'}}" {{$nextPgLink ? 'href=' . $data['next_page_url'] : ''}}>Next</a>
</div>
@endif