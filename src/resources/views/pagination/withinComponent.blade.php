<div class="bg-primary paginator hidden dp-fx fx-al-ct pd-t-20 pd-b-20 pd-l-40 pd-r-40 br-rd-10 ft-pr reg-2 md-reg-2 {{isset($classList) ? $classList : ''}}">
    <div class="md-bd-6 bd-6 mg-r-10 cs-ptr prev hidden">Prev</div>
    <div class="dp-fx cl-suva-grey template">
        <div class="mg-l-6 mg-r-6 mg-l-md-10 mg-r-md-10 item cs-ptr hidden"></div>
    </div>
    <div class="md-bd-6 bd-6 mg-l-10 cs-ptr next hidden">Next</div>
</div>