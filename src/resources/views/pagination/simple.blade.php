@if(!empty(array_get($data, 'list.data', [])))
<div class="bg-primary br-rd-10 dp-fx fx-al-ct pd-t-10 pd-b-10 pd-l-10 pd-r-10 {{isset($classList) ? $classList : ''}}">
	@if(!empty(array_get($data, 'first.url', '')))
	<a href="{!! $data['first']['url'] !!}" class="ft-ter fz-14 nb-icon-back-pwa mg-t-4 mg-r-10 cl-ink-dk" title="first"></a>
	@else
	<div class="ft-ter fz-14 nb-icon-back-pwa mg-t-4 mg-r-10 cl-suva-grey"></div>
	@endif
	@if(!empty(array_get($data, 'prev.url', '')))
	<a href="{!! $data['prev']['url'] !!}" class="ft-ter md-ter-bd-2 ter-bd-2 pd-l-10 pd-r-10 cl-ink-dk">{!! $data['prev']['title'] !!}</a>
	@else
	<div class="ft-ter md-ter-bd-2 ter-bd-2 pd-l-10 pd-r-10 cl-suva-grey">{!! $data['prev']['title'] !!}</div>
	@endif
	<?php
		$selectedIndex = -1;
		foreach ($data['list']['data'] as $key => $value) {
			if ($value['selected']) {
				$selectedIndex = $key;
			}
		}
	?>
	<div class="dp-fx">
		@foreach($data['list']['data'] as $key => $item)
		<a href="{!! $item['url'] !!}" class="ft-ter md-ter-bd-2 ter-bd-2 pd-l-10 pd-r-10 {{$item['selected'] ? 'cl-ink-dk' : 'cl-suva-grey'}} {{((abs($selectedIndex - $key) <= 2)) ? '' : 'hidden-in-touch'}}">{!! $item['title'] !!}</a>
		@endforeach
	</div>
	@if(!empty(array_get($data, 'next.url', '')))
	<a href="{!! $data['next']['url'] !!}" class="ft-ter md-ter-bd-2 ter-bd-2 pd-l-10 pd-r-10 cl-ink-dk">{!! $data['next']['title'] !!}</a>
	@else
	<div class="ft-ter md-ter-bd-2 ter-bd-2 pd-l-10 pd-r-10 cl-suva-grey">{!! $data['next']['title'] !!}</div>
	@endif
	@if(!empty(array_get($data, 'last.url', '')))
	<a href="{!! $data['last']['url'] !!}" class="ft-ter fz-14 rotate-180 nb-icon-back-pwa mg-b-4 mg-r-16 cl-ink-dk" title="last"></a>
	@else
	<div class="ft-ter fz-14 rotate-180 nb-icon-back-pwa mg-b-4 mg-r-16 cl-suva-grey"></div>
	@endif
</div>
@endif