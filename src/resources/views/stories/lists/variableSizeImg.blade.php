<section class="pos-rel {{isset($classList) ? $classList : ''}}">
    <section class="fx-row neg-mg-lr-px-10-ac neg-mg-lr-px-10-md-dac stories-list-variable-size">
        @foreach($data as $item)
            @include('web-components::stories.items.variableSizeImg', [ 'data' => $item, 'classList' => 'col-6 col-md-2 pd-l-10 pd-r-10 mg-t-20', 'type' => $type])
        @endforeach
    </section>
</section>
@include('web-components::toasts.fullScreenModal')