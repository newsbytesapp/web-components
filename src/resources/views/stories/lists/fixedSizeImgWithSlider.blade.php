<section class="pos-rel {{isset($classList) ? $classList : ''}}">
    <section class="img-slider scroll-menu dp-fx fx-dr-col slider-init pd-l-md-10 pd-r-md-10 stories-list" data-sliderconf="variableWidth">
        @foreach($data as $key => $item)
            <div class="dp-fx">
                @foreach($item as $storyData)
                @include('web-components::stories.items.fixedSizeImg', [ 'data' => $storyData, 'classList' => 'mg-r-6 mg-l-6 mg-t-6 mg-b-6', 'type' => $type])
                @endforeach
            </div>
        @endforeach
        @if(isset($listLink) && !empty($listLink) && (sizeof($data) >= 3))
            <a href="{!! $listLink['url'] !!}" class="mg-r-10 pos-rel loadmore" >
                <div class="item fixed-size pos-rel asp-ratio-4-3 bg-white-smoke">
                    <div class="pos-abs tp-init bt-init lt-init rt-init">
                        <div class="dp-fx fx-js-ct fx-al-ct wd-full ht-full">
                            <div class="bg-primary wd-ht-px-50 br-rd-pr-50 dp-fx fx-js-ct fx-al-ct">
                                <div class="ft-ter md-ter-bd-7 ter-bd-5">+</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        @endif
    </section>
</section>
@include('web-components::toasts.fullScreenModal')
@section('css')
@parent
<style type="text/css">
    .asp-ratio-4-3{
        padding-top: 133.33%;
    }
    .slick-initialized .slick-slide{
        display:flex;
    }
    @media only screen and (max-width: 768px) {
        .loadmore{
            display: none;
        }
    }
    .imgSize{
        max-width: 180px;
        width: 43vw;
    }
    .story-item-text-area{
        background: linear-gradient(180deg,transparent,#000 99%);
    }
</style>
@stop