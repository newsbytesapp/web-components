@if(array_get($data, 'list.data', []) && (sizeof($data['list']['data']) > 0))
<?php
    $prevApi = array_get($data, 'list.panel.prev.api', null);
    $nextApi = array_get($data, 'list.panel.next.api', null);
    $prevUrl = array_get($data, 'list.panel.prev.url', null);
    $nextUrl = array_get($data, 'list.panel.next.url', null);
    $nextAmpUrl = array_get($data, 'list.panel.next.ampurl', null);
    $text = array_get($data, 'list.panel.text', null);
?>

<section class="bg-primary pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 stories-card-list-variableSizeImg {{isset($classList) ? $classList : ''}}">

    <div class="cl-lt ft-ter md-ter-bd-6 ter-bd-6 pd-t-10 pd-l-md-10 pd-r-md-10"><h1>{!! $data['title'] !!}</h1></div>

    @include('web-components::stories.lists.variableSizeImg', ['data' => $data['list']['data'], 'classList' => '', 'linkTargetAttr' => array_get($data, 'list.linkTargetAttr', '')])

    @if(!empty(array_get($data, 'list.pagination', [])))
    <div class="pd-t-20 pd-b-20 dp-fx fx-js-ct">
    @include('web-components::pagination.simple', ['data' => $data['list']['pagination'], 'classList' => 'br-1p-pale-lavender'])
    </div>
    @else
    <hr data-id="{{$data['list']['data'][0]['id']}}"/>
    <div class="panel static pd-t-30 pd-b-20">
        @if(!is_null($nextUrl))
        <a href="{!! $type == 'amp' ? (!is_null($nextAmpUrl) ? $nextAmpUrl : $nextUrl) : $nextUrl !!}" class="dp-bl wd-full bg-primary cl-link br-rd-10 tx-ct pd-t-14 pd-b-14 navy-blue-cl-hover alice-blue-bg-hover ft-ter md-ter-bd-2 ter-bd-2 next cs-ptr" title="{{$data['list']['panel']['title']}}">{!! $data['list']['panel']['title'] !!}</a>
        @endif
        @switch($type)
        @case('amp')
        @break
        @default
        @include('web-components::toasts.dotsLoader')
        @include('web-components::toasts.error')
        @break
        @endswitch
    </div>
    @endif

</section>
@endif