@if(array_get($data, 'list.data', []) && (sizeof($data['list']['data']) > 0))
    <section class="bg-primary pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 stories-card-list-variableSizeImg {{isset($classList) ? $classList : ''}}">

        <div class="cl-lt ft-ter md-ter-bd-6 ter-bd-6 pd-t-10 pd-l-md-10 pd-r-md-10">{!! $data['title'] !!}</div>

        @include('web-components::stories.lists.variableSizeImg', ['data' => $data['list']['data'], 'classList' => '', 'linkTargetAttr' => array_get($data, 'list.linkTargetAttr', '')])

        @if(array_get($data, 'list.button', []) && (sizeof($data['list']['button']) > 0))
            <div class="pd-t-20 pd-b-20 pd-l-md-10 pd-r-md-10">
                @include('web-components::buttons.outline', [ 'button' => $data['list']['button'], 'type' => $type, 'searchFor' => '.stories-card-list-variableSizeImg', 'appendTo' => '.stories-list-variable-size', 'template' => 'storyListItemVariableSizeImg', 'classList' => 'add-more-stories' ])
            </div>
        @endif

    </section>
@endif