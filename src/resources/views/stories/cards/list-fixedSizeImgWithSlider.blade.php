@if(array_get($data, 'list.data', []) && (sizeof($data['list']['data']) > 0))
    <section class="bg-primary pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 {{isset($classList) ? $classList : ''}}">

        @if(!empty(array_get($data, 'list.link', [])))
        <a href="{!! $data['list']['link']['url'] !!}" class="cl-lt ft-ter md-ter-bd-6 ter-bd-6 pd-t-10 pd-l-md-10 pd-r-md-10 dp-ib">{!! $data['title'] !!}</a>
        @else
        <div class="cl-lt ft-ter md-ter-bd-6 ter-bd-6 pd-t-10 pd-l-md-10 pd-r-md-10">{!! $data['title'] !!}</div>
        @endif

        @include('web-components::stories.lists.fixedSizeImgWithSlider', ['data' => $data['list']['data'], 'listLink' => array_get($data, 'list.link', []), 'classList' => 'mg-t-20', 'linkTargetAttr' => array_get($data, 'list.linkTargetAttr', '')])

        @if(!empty(array_get($data, 'list.link', [])))
            <div class="pd-t-20 pd-b-10 pd-l-md-10">
                @include('web-components::links.link', ['link' => $data['list']['link'], 'classList' => ''])
            </div>
        @endif

    </section>
@endif