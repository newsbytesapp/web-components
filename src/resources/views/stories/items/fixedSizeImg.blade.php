<div data-url="{!! $data['url'] !!}" class="{{isset($classList) ? $classList : ''}} pos-rel cs-ptr modal-opener full-size" title="{!! $data['title'] !!}" >
    <div class="asp-ratio item r-3-4 item imgSize"><img data-src="{{$data['img']}}" class="lazy wd-full" alt="{!! $data['title'] !!}"/></div>
    <div class="pos-abs bt-init ht-half wht-sp-normal wd-full story-item-text-area imgSize">
        <div class="ht-full pos-rel">
            <a href="{!! $data['url'] !!}" class="bt-init cl-primary pd-b-10 pd-l-10 pd-r-10 pos-abs ft-pr md-bd-4 bd-4">{!! $data['title'] !!}</a>
        </div>
    </div>
</div>