<div data-url="{!! $data['url'] !!}" class="{{isset($classList) ? $classList : ''}} pos-rel cs-ptr start-player full-size">
    <img src="{{$data['img']}}" class="item variable-size wd-full" alt="{!! $data['title'] !!}"/>
    <div class="pos-abs bt-init ht-half story-item-text-area variable-size">
        <div class="ht-full pos-rel">
            <a href="{!! $data['url'] !!}" class="bt-init cl-primary pd-b-10 pd-l-10 pd-r-10 pos-abs ft-pr md-bd-3 bd-3">{!! $data['title'] !!}</a>
        </div>
    </div>
</div>