<?php
$w = (isset($width))?$width:70;
$h = (isset($height))?$height:70;
$isLazy = in_array('lazy', explode(' ', (isset($classList) ? $classList : '')));
$imgsrc = ($isLazy)?'data-src':'src';
?>
@switch($type)
@case('amp')
<amp-img data-src="{{$data['icon']}}" alt="{!! $data['title'] !!}" title="{!! $data['title'] !!}" class="br-rd-10 {{isset($classList) ? $classList : ''}}" width="{{$w}}" height="{{$h}}" layout="fixed"></amp-img>
@break
@default
<img {{$imgsrc}}="{{$data['icon']}}" alt="{!! $data['title'] !!}" title="{!! $data['title'] !!}" class="br-rd-10 {{isset($classList) ? $classList : ''}}"/>
@endswitch