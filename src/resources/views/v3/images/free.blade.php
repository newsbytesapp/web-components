<?php
$w = (isset($width))?$width:1080;
$h = (isset($height))?$height:610;
$isLazy = in_array('lazy', explode(' ', (isset($classList) ? $classList : '')));
$imgsrc = ($isLazy)?'data-src':'src';
?>
@switch($type)
@case('amp')
<amp-img data-src="{{$data['img']}}" alt="{!! $data['title'] !!}" title="{!! $data['title'] !!}" class="br-rd-10 {{isset($classList) ? $classList : ''}}" width="{{$w}}" height="{{$h}}" layout="responsive"></amp-img>
@break
@default
<img {{$imgsrc}}="{{$data['img']}}" alt="{!! $data['title'] !!}" title="{!! $data['title'] !!}" class="br-rd-10 {{isset($classList) ? $classList : ''}}"/>
@endswitch