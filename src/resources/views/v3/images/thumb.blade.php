<?php
$w = (isset($width))?$width:235;
$h = (isset($height))?$height:133;
$isLazy = in_array('lazy', explode(' ', (isset($classList) ? $classList : '')));
$imgsrc = ($isLazy)?'data-src':'src';
?>
@switch($type)
@case('amp')
<amp-img data-src="{{$data['thumb']}}" alt="{!! $data['title'] !!}" title="{!! $data['title'] !!}" class="br-rd-10 {{isset($classList) ? $classList : ''}}" width="{{$w}}" height="{{$h}}" layout="responseive"></amp-img>
@break
@default
<img {{$imgsrc}}="{{$data['thumb']}}" alt="{!! $data['title'] !!}" title="{!! $data['title'] !!}" class="br-rd-10 {{isset($classList) ? $classList : ''}}"/>
@endswitch