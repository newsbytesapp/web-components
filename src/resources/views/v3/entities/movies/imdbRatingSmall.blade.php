@if(isset($data['imdB']) )
<div class="mg-b-6">
    <span class="mg-t-2  ft-ter   mg-r-2 ter-reg-1">IMDb</span>
    <span class="mg-t-2  ft-ter cl-ink-dk ter-bd-1 bg-safety-yellow br-rd-4 mg-r-2 pd-l-4 pd-r-4 pd-t-2 pd-b-2"> {!! $data['imdb'] !!}</span>
</div>
@endif