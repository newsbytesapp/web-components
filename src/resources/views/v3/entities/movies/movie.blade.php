<?php
    $tag = array_get($data, 'tag', 'default');
?>
<div class="bg-primary  mg-b-10 mg-r-4 mg-l-4 mg-t-4 col-12  col-md-6 col-lg-5 dp-fx fx-js-bw fx-dr-col fx-basis-48pr br-rd-4 {{isset($classList) ? $classList : ''}}" >
    <div class="dp-fx fx-js-bw fx-al-stretch mg-b-10 mg-l-10 mg-r-10 mg-t-10">
        <div class="dp-fx fx-al-ct fx-js-bw">
            <div class="mg-r-10 wd-px-100 " >
                @if(isset($data['img']))
                <img data-src="{!! $data['img'] !!}" alt="{{$data['title']}}" class="lazy wd-full obj-fit-cover asp-r-2-3">                
                @endif
            </div>
            <div class="dp-fx fx-dr-col"  style="max-width: calc(100% - 110px);">
                <p class="cl-ink-dk ft-ter ter-bd-3 mg-r-2 mg-b-6 tx-lt">{!! $data['title'] !!}</p>
                @if(isset($data['genres']) && !empty($data['genres']))
                <span class="mg-t-2  ft-ter mg-b-8   tx-lt ter-reg-1 cl-lt"> {!! $data['genres'] !!}</span> 
                @endif
                @include('web-components::v3.entities.movies.imdbRatingSmall',['data' =>$data,'classList' =>''])
                @if(isset($data['director']) && !empty($data['director']))
                <span class="mg-t-2  ft-ter mg-b-6  ter-reg-1 cl-cerulean-blue-dk"> {!!$data['director']!!}</span>
                @endif
                @if(isset($data['timestamp']) && !empty($data['timestamp']))
                <span class="cl-ink-dk ter-bd-2 dp-fx ft-ter fx-al-ct ter-reg-1"> {!!$data['timestamp'] !!}</span>
                @endif
                @if(isset($data['money']) && !empty($data['money']))
                <span class="cl-ink-dk ter-bd-2 dp-fx ft-ter fx-al-ct  ter-bd-2"> {!!$data['money'] !!}</span>
                @endif
            </div>
        </div>
    </div>
    @if(isset($data['footer']) && !empty($data['footer']['data']))
    <div class="ft-ter sec-reg-1 cl-lt mg-b-10 mg-l-10 mg-r-10 mg-t-10" >
        <hr class="bg-silver-lt opacity-50 mg-b-4 ht-px-1">
        <div class="dp-fx fx-al-ct mg-l-2 fz-12  {{ !isset($data['footer']['data']['cast']) ? 'mg-b-md-16' : ''}}">
        @if (!empty($data['footer']['data']['platform']))
            <div class="nb-icon-video-play-square fz-12 mg-r-10"> </div>
            <div class="tx-lt sec-bd-2  ft-ter  cl-cerulean-blue-dk" >{!!$data['footer']['data']['platform'] !!}</div>
        @endif
        </div>        
        <div  class="mg-l-2 {{ !isset($data['footer']['data']['platform']) ? 'mg-t-md-20' : ''}}">
            @if((!empty($data['footer']['data']['cast'])) && (!empty($data['footer']['data']['cast'][0])))

            <span class="nb-icon-starcast fz-12  mg-r-6"> </span>
            @foreach ($data['footer']['data']['cast'] as $cast)
            <span class="ft-ter "> {{$cast}}</span>
                @if (!$loop->last)
            <span class="mg-l-4 mg-r-4 cl-acadia br-rd-pr-50 wd-ht-px-4">&bull;</span>
                @endif
            @endforeach
        @endif
        </div>
    </div>
    @endif
</div>
