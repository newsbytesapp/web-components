@include('web-components::v3.footer.scripts.external.app', ['data' => $data])
@include('web-components::v3.footer.scripts.utility.app', ['data' => $data])
@yield('web-components-js')