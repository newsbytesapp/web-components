@include('web-components::v3.footer.scripts.utility.initJs', ['data' => $data])
<script defer type="text/javascript" src="{{mix('/js/components.js')}}"></script>
@include('web-components::v3.footer.scripts.utility.pwaPermit', ['data' => $data])
@include('web-components::v3.footer.scripts.utility.performanceMetrics', ['data' => $data])