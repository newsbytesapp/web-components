<script defer type="text/javascript" src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/jquery.min.js"></script>
<script defer src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/OneSignalSDK.js"></script>
<script defer type="text/javascript" src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/packageMaster.6.js"></script>
<script defer type="text/javascript" src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/slick.min.js"></script>
@if(!empty(array_get($data, 'libs.chartjs', false)))
<script defer type="text/javascript" src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/chart.2.9.3.min.js" onload="_nbchartready=true"></script>
@endif
@if(!empty(array_get($data, 'libs.table', false)))
<script type="text/javascript">const filterTemplate='<label class="dp-bl pd-l-44 pd-t-12 pd-b-12 pd-r-20 filter-checkbox drdown-checkbox cl-lt reg-3 md-reg-3 black-cl-hover alice-blue-bg-hover pos-rel cs-ptr" data-filter-for="{key}">{colname}<input type="checkbox" name="filter-checkboxes" checked="{checked}" class="wd-ht-0 cs-ptr pos-abs opacity-0" /><span class="checkmark br-rd-3"></span></label>',headerCellTemplate='<td class="pd-l-10 pd-r-10 pd-t-20 pd-b-20 {imp}" data-cell="{key}">{colname}</td>',headerTemplate='<tr class="br-t-1p-white-smoke bd-6 md-bd-6 wht-sp-normal">{headerCellValuesString}</tr>',cellTemplate='<td class="pd-l-10 pd-r-10 pd-t-20 pd-b-20 {imp}" data-cell="{key}"><span class="tx-ct">{val}</span></td>',rowTemplate='<tr class="br-t-1p-white-smoke reg-2 md-reg-2 filterable" data-filter-key="{filterkey}" data-filter-val="{filterval}">{cellValuesString}</tr>',tableTemplate='<div class="bg-primary br-rd-0 br-rd-md-10 pd-b-10 table-section mg-t-10 mg-t-md-20 pd-t-10" id="hd-table-{randomIdStr}"><div class="dp-fx fx-al-ct fx-js-bw pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20"><h6 class="ft-pr bd-4 ft-pr md-bd-4"><input id="input-{randomIdStr}" type="text" placeholder="Search..." onKeyUp="tableFilter(\'{randomIdStr}\')" /></h6><div class="dp-ib pos-rel ovr-initial drdown-parent ft-pr"><button class="dp-ib bg-primary cl-ink-dk br-rd-10 pd-l-20 pd-r-20 pd-l-md-30 pd-r-md-30 pd-t-12 pd-b-12 ff-primary alice-blue-bg-hover drdown-selector cs-ptr" id="hdfb-{randomIdStr}"><div class="dp-fx"><span class="bd-6 md-bd-6">View by</span><span class="nb-icon-drop-more fz-16 mg-l-6"></span></div></button><div id="hdfo-{randomIdStr}" class="hidden pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 wd-full box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-2 drdown-box rt-init" data-status="off" data-animation-entrance="ent-slide-down-mg" data-animation-exit="ext-slide-up-mg" data-type="statefull">{filterValuesString}</div></div></div><div class="hz-scroll-ac"><table id="table-{randomIdStr}" class="wd-full table pagination-present"  cellspacing="0" cellpadding="0"><tbody class="wht-sp-normal ft-pr">{headerValuesString}{rowValuesString}</tbody></table></div></div>';function json2table(e){if(0==e.rows.length)return"";let l="",a="",t="",r="",d="";var p,i,s=e.header;let c=Math.random().toString(36).replace(/[^a-z]+/g,"").substr(0,5),n=!1;for(let e=0;e<s.length;e++)p=headerCellTemplate,t+=filterTemplate.replace(/{checked}/g,s[e].checked).replace(/{key}/g,s[e].key).replace(/{colname}/g,s[e].name),l+=p.replace(/{imp}/g,s[e].imp).replace(/{key}/g,s[e].key).replace(/{colname}/g,s[e].name),n=s[e].filter?s[e].key:n;r=headerTemplate.replace(/{headerCellValuesString}/g,l);for(let l=0;l<e.rows.length;l++){i=rowTemplate,a="";for(let t=0;t<s.length;t++)a+=cellTemplate.replace(/{key}/g,s[t].key).replace(/{val}/g,e.rows[l][s[t].key]).replace(/{imp}/g,s[t].imp);i=i.replace(/{cellValuesString}/g,a),n&&(i=i.replace(/{filterval}/g,e.rows[l][n]).replace(/{filterkey}/g,n)),d+=i}return tableTemplate.replace(/{filterValuesString}/g,t).replace(/{headerValuesString}/g,r).replace(/{rowValuesString}/g,d).replace(/{randomIdStr}/g,c)}function tableFilter(e){var l,a,t;for(l=document.getElementById("input-"+e).value.toUpperCase().trim(),a=document.getElementById("table-"+e).getElementsByClassName("filterable"),i=0;i<a.length;i++)if((t=a[i]).attributes["data-filter-val"].value.toUpperCase().trim().indexOf(l)>-1||""==l){let e=t.className.split(" "),l=e.indexOf("hidden");l>-1&&(e.splice(l,1),t.className=e.join(" "))}else{-1==t.className.split(" ").indexOf("hidden")&&(t.className+=" hidden")}};</script>
@elseif(!empty(array_get($data, 'libs.tablepg', false)))
<script>

const filterTemplate = '<label class="dp-bl pd-l-44 pd-t-12 pd-b-12 pd-r-20 filter-checkbox drdown-checkbox cl-lt reg-3 md-reg-3 black-cl-hover alice-blue-bg-hover pos-rel cs-ptr" data-filter-for="{key}">{colname}<input type="checkbox" name="filter-checkboxes" checked="{checked}" class="wd-ht-0 cs-ptr pos-abs opacity-0" /><span class="checkmark br-rd-3"></span></label>';

const headerCellTemplate = '<td class="pd-l-10 pd-r-10 pd-t-20 pd-b-20 {imp} {sortclass}" data-cell="{key}">{colname}</td>';

const headerTemplate = '<tr class="br-t-1p-white-smoke bd-6 md-bd-6 wht-sp-normal">{headerCellValuesString}</tr>';

const cellTemplate = '<td class="pd-l-10 pd-r-10 pd-t-20 pd-b-20 {imp}" data-cell="{key}"><span class="tx-ct">{val}</span></td>';

const rowTemplate = '<tr class="br-t-1p-white-smoke reg-2 md-reg-2 paginated sortable filterable {rowState}" data-filter-key="{filterkey}" data-filter-val="{filterval}" data-page="{page}">{cellValuesString}</tr>';

const tableTemplate = '<div class="bg-primary br-rd-0 br-rd-md-10 pd-b-10 table-section mg-t-10 mg-t-md-20 pd-t-10" id="hd-table-{randomIdStr}"><div class="dp-fx fx-al-ct fx-js-bw pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20"><div class="dp-fx fx-al-ct col-6 bg-lavender cl-acadia pd-t-14 pd-b-14 pd-l-20 pd-r-10 br-rd-10 {showstate}"><span class="fz-16 nb-icon-search dp-ib mg-t-4"></span><input id="input-{randomIdStr}" type="text" placeholder="" onKeyUp="tableFilter(\'{randomIdStr}\')" /></div><div class="dp-ib pos-rel ovr-initial drdown-parent ft-pr"><button class="dp-ib bg-primary cl-ink-dk br-rd-10 pd-l-20 pd-r-20 pd-l-md-30 pd-r-md-30 pd-t-12 pd-b-12 ff-primary alice-blue-bg-hover drdown-selector cs-ptr" id="hdfb-{randomIdStr}"><div class="dp-fx"><span class="bd-6 md-bd-6">View by</span><span class="nb-icon-drop-more fz-16 mg-l-6"></span></div></button><div id="hdfo-{randomIdStr}" class="hidden pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 wd-full box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-2 drdown-box rt-init" data-status="off" data-animation-entrance="ent-slide-down-mg" data-animation-exit="ext-slide-up-mg" data-type="statefull">{filterValuesString}</div></div></div><div id="paginate-{randomIdStr}" class="hz-scroll-ac pagination-parent"><table id="table-{randomIdStr}" class="wd-full table pagination-present" cellspacing="0" cellpadding="0"><tbody class="wht-sp-normal ft-pr">{headerValuesString}{rowValuesString}</tbody></table>{paginateValuesString}</div></div>';

const paginateTemplate = '<div class="dp-fx fx-js-ct mg-t-10 {showstate}"><div id="paginator-{randomIdStr}" class="bg-primary paginator dp-fx fx-al-ct pd-t-20 pd-b-20 pd-l-40 pd-r-40 br-rd-10 ft-pr reg-2 md-reg-2 bg-body"><div class="md-bd-6 bd-6 mg-r-10 cs-ptr prev hidden">Prev</div><div class="dp-fx cl-suva-grey">{indicatorValuesString}</div><div class="md-bd-6 bd-6 mg-l-10 cs-ptr next hidden">Next</div></div></div>';
const indicatorTemplate = '<div class="mg-l-6 mg-r-6 mg-l-md-10 mg-r-md-10 item cs-ptr {state}" onClick="showpage(\'{randomIdStr}\', {page})">{page}</div>';


function json2table(json) {
	if(json.rows.length == 0) return "";
	let headerCellValuesString = "", cellValuesString = "", filterValuesString = "", headerValuesString = "", rowValuesString = "", paginateValuesString = "", indicatorValuesString = "";
	var filterTemplateString, headerTemplateString, headerCellTemplateString, cellTemplateString, rowTemplateString, paginateTemplateString, indicatorTemplateString;
	var cols = json.header, val;
	let rstr = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
	let filterkey = false;
	let pagesize=5;
	var indicatorState = 'hidden', rowState = '';
	for (let j=0; j < cols.length; j++) {
		filterTemplateString = filterTemplate;
		headerCellTemplateString = headerCellTemplate;
		filterValuesString += filterTemplateString.replace(/\{checked\}/g, cols[j].checked).replace(/\{key\}/g, cols[j].key).replace(/\{colname\}/g, cols[j].name);
		let sortclass='';
		if(typeof cols[j].format == 'undefined') cols[j].format = 'string';
		if((typeof cols[j].sort != 'undefined') && (cols[j].sort)) sortclass='sort-rows sort-by-'+cols[j].key.toLowerCase().trim();
		headerCellValuesString += headerCellTemplateString.replace(/\{imp\}/g, cols[j].imp).replace(/\{key\}/g, cols[j].key).replace(/\{colname\}/g, cols[j].name).replace(/\{sortclass\}/g, sortclass);
		filterkey = (cols[j].filter) ? cols[j].key : filterkey;
	}
	headerValuesString = headerTemplate.replace(/\{headerCellValuesString\}/g, headerCellValuesString);
	for (let i=0; i<json.rows.length; i++) {
		page = Math.ceil((i+1)/pagesize);
		rowTemplateString = rowTemplate;
		cellValuesString = "";
		for (let j=0; j < cols.length; j++) {
			val = json.rows[i][cols[j].key];
			if(cols[j].format == 'number') {val = parseInt(val);val = val.toLocaleString();}
			else if(cols[j].format == 'float') {val = parseFloat(val);val = val.toLocaleString();}
			cellValuesString += cellTemplate.replace(/\{key\}/g, cols[j].key).replace(/\{val\}/g, val).replace(/\{imp\}/g, cols[j].imp);
		}
		if(page>1) rowState = 'hidden';
		rowTemplateString = rowTemplateString.replace(/\{cellValuesString\}/g, cellValuesString).replace(/\{rowState\}/g, rowState);
		if(filterkey) rowTemplateString = rowTemplateString.replace(/\{filterval\}/g, json.rows[i][filterkey]).replace(/\{filterkey\}/g, filterkey).replace(/\{page\}/, page);
		rowValuesString += rowTemplateString;
		if((i)%pagesize == 0) {
			indicatorState = (page<=5)?((page==1)?'cl-ink-dk active':'active'):'hidden';
			indicatorValuesString += indicatorTemplate.replace(/\{state\}/g, indicatorState).replace(/\{page\}/g, page)
		}
	}
	var showstate = (json.rows.length > 5)?'':'hidden';
	paginateValuesString = paginateTemplate.replace(/\{indicatorValuesString\}/, indicatorValuesString);
	return tableTemplate.replace(/\{filterValuesString\}/g, filterValuesString).replace(/\{headerValuesString\}/g, headerValuesString).replace(/\{rowValuesString\}/g, rowValuesString).replace(/\{paginateValuesString\}/g, paginateValuesString).replace(/\{showstate\}/g, showstate).replace(/\{randomIdStr\}/g, rstr);
}
function tableFilter(rsid) {
  // Declare variables
  var input, filter, table, paginator, trs, txtValue, element;
  input = document.getElementById("input-"+rsid);
  filter = input.value.toUpperCase().trim();
  paginator = document.getElementById('paginator-'+rsid);
  table = document.getElementById('table-'+rsid);
  trs = table.getElementsByClassName('filterable');

  for (i = 0; i < trs.length; i++) {
    element = trs[i];
    txtValue = element.attributes['data-filter-val'].value.toUpperCase().trim();
	if ((txtValue.indexOf(filter) > -1) || filter=="") {
		removeClass(element, 'hidden');
	} else {
		addClass(element, "hidden");
	}
  }
  if(filter == '') {
  	removeClass(paginator, 'hidden');
  	showpage(rsid);
  } else {
  	addClass(paginator, 'hidden');
  }
}
function showpage(rsid, page) {
  // Declare variables
  var table, trs, indicators, pageValue, element;
  table = document.getElementById('table-'+rsid);
  trs = table.getElementsByClassName('paginated');
  indicators = document.getElementById('paginator-'+rsid).getElementsByClassName('item');

  page = ((typeof page == 'undefined') && (typeof table.attributes['data-page'] == 'undefined')) ? 1 : ((typeof page == 'undefined') ? parseInt(table.attributes['data-page'].value) : page);
  table.setAttribute('data-page', page);

  for (var i = trs.length-1; i >= 0; i--) {
    element = trs[i];
    pageValue = parseInt(element.attributes['data-page'].value);
	if (pageValue == page) {
		removeClass(element, 'hidden');
	} else {
		addClass(element, "hidden");
	}
  }
  var totalPages = indicators.length;
  for (var i = totalPages; i > 0; i--) {
  	element = indicators[i-1];
  	console.log('pages' , ((i <= page+2) && (i >= page-2)) , ((page-2 < 1) && (i <= 5)) , ((page+2 > totalPages) && (i >= totalPages-5)));
  	if(((i <= page+2) && (i >= page-2)) || ((page-2 < 1) && (i <= 5)) || ((page+2 > totalPages) && (i > totalPages-5))) {
  		addClass(element, 'active');
  		removeClass(element, 'hidden');
  		removeClass(element, 'cl-ink-dk');
  		if(i==page)
  			addClass(element, 'cl-ink-dk');
  	} else {
  		addClass(element, 'hidden');
  		removeClass(element, 'active');
  		removeClass(element, 'cl-ink-dk');
  	}
  }
}
function addClass(element, classStr) {
	let arr = element.className.split(" ");
	if (arr.indexOf(classStr) == -1)
		element.className += " "+classStr;
}
function removeClass(element, classStr) {
	let arr = element.className.split(" "), index = arr.indexOf(classStr);
	if (index > -1) {
		arr.splice(index, 1);
		element.className = arr.join(" ");
	}
}
</script>
@endif
{{-- <script defer type="text/javascript" src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/jq.scrollify.min.js"></script> --}}
@if(!empty(array_get($data, 'libs.d3js', false)))
<script type="text/javascript" src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/d3.5.15.1.min.js" defer></script>
<script type="text/javascript">
	function embedMap(data, mapId) {
		var d3Interval = setInterval(function () {
			if(typeof d3 == 'undefined') return;
			clearInterval(d3Interval);
			var w = 3000;
			var h = 1250;
			// Variables for catching min and max zoom factors
			var minZoom;
			var maxZoom;

			var group;
			/* var zoomed = function () {
				t = d3.event.transform;
				group.attr("transform", "translate(" + [t.x, t.y] + ")scale(" + t.k + ")");
			} */

			// var zoom = d3.zoom().on("zoom", zoomed);

			var getTextBox = function (list) {
				list.each(function (d) {
					d.bbox = this.getBBox();
				});
			}

			// Function that calculates zoom/pan limits and sets zoom to default value 
			var initiateZoom = function () {
				// Define a "minzoom" whereby the "Countries" is as small possible without leaving white space at top/bottom or sides
				minZoom = Math.max($('#' + mapId).width() / w, $('#' + mapId).height() / h);
				// set max zoom to a suitable factor of this value
				maxZoom = 20 * minZoom;
				// set extent of zoom to chosen values
				// set translate extent so that panning can't cause map to move out of viewport
				zoom.scaleExtent([minZoom, maxZoom]).translateExtent([[0, 0],[w, h]]);
				// define X and Y offset for centre of map to be shown in centre of holder
				midX = ($('#' + mapId).width() - minZoom * w) / 2;
				midY = ($('#' + mapId).height() - minZoom * h) / 2;
				// change zoom transform to min zoom and centre offsets
				svg.call(zoom.transform, d3.zoomIdentity.translate(midX, midY).scale(minZoom));
			}

			// var svg = d3.select('#' + mapId).append("svg").attr("width", $('#' + mapId).width()).attr("height", $('#' + mapId).height()).call(zoom);
			var svg = d3.select('#' + mapId).append("svg").attr("width", $('#' + mapId).width()).attr("height", $('#' + mapId).height());

			d3.json(data.geojson)
			.then(function (json) {
				group = svg.append("g").attr("id", "id" + mapId).attr("width", $('#' + mapId).width()).attr("height", "100%");
				var projection = d3.geoMercator().fitSize([$('#' + mapId).width(), $('#' + mapId).height()], json);
				var path = d3.geoPath().projection(projection);
				group.append("rect").attr("x", 0).attr("y", 0).attr("width", $('#' + mapId).width()).attr("height", "100%");

				// draw a path for each feature/item
				var countList = [];

				var items = group.selectAll("path").data(json.features).enter().append("path").attr("d", path).attr("class", "item")
				.attr("id", function (d, i) {
					var id = d['id'];
					if(typeof data['data'][id] == 'undefined')
						countList.push(0);
					else
						countList.push(data['data'][id]['count']);
					return "item-" + i;
				})
				.on("mouseover", function (d, i) {
					d3.selectAll(".item").classed("item-on", false);
					d3.select(this).classed("item-on", true);
					// d3.select("#label" + i).style("display", "block");
					updateMapToolTip(mapId, '#label' + i);
				})
				.on("mouseout", function (d, i) {
					d3.selectAll(".item").classed("item-on", false);
					d3.select(this).classed("item-on", true);
					// d3.select("#label" + i).style("display", "none");
				})
				.on("click", function (d, i) {
					d3.selectAll(".item").classed("item-on", false);
					d3.select(this).classed("item-on", true);
					// boxZoom(path.bounds(d), path.centroid(d), 20);
					updateMapToolTip(mapId, '#label' + i);
				});

				var sortedCountList = countList.sort(function (a, b) {
					return a - b;
				});

				var colorSet = ["#FEE5D8", "#A61B1B"]
				if(typeof data['options']['colorset'] !== 'undefined')
					colorSet = data['options']['colorset'];
				var colorScale = d3.scaleLinear().domain([0, (sortedCountList.length - 1)]).range(colorSet);

				var itemsPerColor = ((sortedCountList[sortedCountList.length - 1] - (sortedCountList[sortedCountList.length - 1] % sortedCountList.length)) / sortedCountList.length)
				group.selectAll("path").data(json.features).style("fill", function (d, i) {
					var id = d['id'];
					var fillIndex;
					if(typeof data['data'][id] == 'undefined' || (itemsPerColor == 0))
						fillIndex = 0;
					else
						fillIndex = ((data['data'][id]['count'] - (data['data'][id]['count'] % itemsPerColor)) / itemsPerColor);
					return colorScale(fillIndex);
				});

				var bucketCount = 5;
					if(typeof data['options']['bucketCount'] !== 'undefined')
						bucketCount = data['options']['bucketCount'];

				var bucketRanges = [];
				var lastValue = -1;
				var itemsPerBucketRange = (sortedCountList[sortedCountList.length - 1] - (sortedCountList[sortedCountList.length - 1] % bucketCount))/bucketCount
				for (var index = 0; index < bucketCount; index++) {
					if (index == (bucketCount - 1)) {
						bucketRanges.push([(lastValue + 1), Number.MAX_VALUE]);
					} else {
						bucketRanges.push([(lastValue == 0 ? lastValue : (lastValue + 1)), (itemsPerBucketRange * (index + 1))])
					}
					lastValue = (itemsPerBucketRange * (index + 1));
				}
				if (bucketRanges[0][0] == bucketRanges[1][0]) {
					bucketRanges = [bucketRanges.slice(0, 1)[0], bucketRanges.slice(-1)[0]];
				}
				addLegend(mapId, sortedCountList, bucketRanges, colorScale)
				// Add a label group to each feature/item. This will contain the item name and a background box
				var itemLabels = group.selectAll("g").data(json.features).enter().append("g").attr("class", "label")
				.attr("id", function (d, i) {
					return "label" + i;
				})
				/* .attr("transform", function (d) {
					return (
						"translate(" + path.centroid(d)[0] + "," + path.centroid(d)[1] + ")"
					);
				})
				.on("mouseover", function (d, i) {
					d3.select(this).style("display", "block");
				})
				.on("mouseout", function (d, i) {
					d3.select(this).style("display", "none");
				}) */
				/* .on("click", function (d, i) {
					d3.selectAll(".item").classed("item-on", false);
					d3.select("#item" + i).classed("item-on", true);
					// boxZoom(path.bounds(d), path.centroid(d), 20);
				}); */
				

				// add the text to the label group showing item details
				itemLabels.append("text").attr("class", "text").style("text-anchor", "middle").attr("dx", 0).attr("dy", 0).insert('tspan').attr("class", "title")
				.text(function (d) {
					var id = d['id'];
					if(typeof data['data'][id] == 'undefined')
						return '';
					if(typeof data['data'][id]['title'] == 'undefined')
						return '';
					return data['data'][id]['title'];
				}).call(getTextBox);

				itemLabels.selectAll("text").append('tspan').attr("y", 20).attr("x", 0).attr("class", "subtitle")
				.text(function (d) {
					var id = d['id'];
					if(typeof data['data'][id] == 'undefined')
						return '';
					if(typeof data['data'][id]['subtitle'] == 'undefined')
						return '';
					return data['data'][id]['subtitle'];
				}).call(getTextBox);

				itemLabels.selectAll("text").append('tspan').attr("y", 40).attr("x", 0).attr("class", "ltext")
				.text(function (d) {
					var id = d['id'];
					if(typeof data['data'][id] == 'undefined')
						return '';
					if(typeof data['data'][id]['text'] == 'undefined')
						return '';
					return data['data'][id]['text'];
				}).call(getTextBox);

				// add a background box the same size as the text
				/* itemLabels.insert("rect", "text").attr("class", "box")
				.attr("transform", function (d) {
					return "translate(" + (d.bbox.x - 10) + "," + (d.bbox.y - 10) + ")";
				})
				.attr("width", function (d) {
					return d.bbox.width + 20;
				})
				.attr("height", function (d) {
					return d.bbox.height + 20;
				})
				.attr("rx", 10)
				.attr("ry", 10); */
				
				// initiateZoom();
			});
		}, 500);
	}
	function updateMapToolTip(mapId, query) {
		var tooltip = $('#' + mapId).closest('.map-parent').find('.tooltip');
		tooltip.removeClass('hidden');
		var elem = $('#' + mapId).find(query);
		var defaultFieldValue = 'NA';
		var updateFields = ['.title', '.ltext'];
		updateFields.forEach(function (field, i) {
			tooltip.find(field).text(elem.find(field).text());
			if (tooltip.find(field).text() == '') {
				tooltip.find(field).text(defaultFieldValue)
			}
		});
	}

	function addLegend(mapId, countList, bucketRanges, colorScale) {
		var legend = $('#' + mapId).closest('.map-parent').find('.legend');
		var clonedRampItem = legend.find('.ramp .item').clone();
		var clonedMarkerItem = legend.find('.marker .item').clone();
		legend.find('.ramp').empty();
		legend.find('.marker').empty();

		countList.forEach(function (count, index){
			var item = clonedRampItem.clone().css({backgroundColor : colorScale(index)});
			legend.find('.ramp').append(item);
		});
		for (var index = 0; index < bucketRanges.length; index++) {
			var item = clonedMarkerItem.clone();
			item.find('.text').text(bucketRanges[index][0]);
			if (index == (bucketRanges.length - 1)) {
				item.find('.text').text(item.find('.text').text() + '+');
			}
			legend.find('.marker').append(item);
		}
	}
</script>
@endif