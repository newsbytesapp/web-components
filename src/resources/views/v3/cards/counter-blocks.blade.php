@if(empty(array_get($data, 'list.data', [])))
<?php $_nb_items = array_get($data, 'config.data', []); $len = count($_nb_items); ?>
<div {!! isset($data['htmlid'])?'id="'.$data['htmlid'].'"':'' !!} class="br-rd-0 br-rd-md-10 mg-t-20 v2 {{isset($classList) ? $classList : ''}}">
	@if(isset($data['footnotes']) && !empty($data['footnotes']['data']))
	<div class="footer">
	@foreach($data['footnotes']['data'] as $footnote)
	@include('web-components::v3.texts.footnote', ['data' => $footnote, 'classList' => '', 'type' => ''])
	@endforeach
	</div>
	@endif
	<div class="mg-t-20 dp-fx fx-js-ct">
		<div class="dp-fx fx-al-ct fx-js-bw col-12 neg-mg-lr-px-10-ac">
			@foreach($_nb_items as $key => $item)
			<div class="item dp-fx fx-dr-col pd-l-10 pd-r-10 bg-primary pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 wd-full {{($key == $len-1)?'':'mg-r-20'}} br-rd-md-10" data-label="{{$item['label']}}">
				@if(!empty($item['img']))
				<img src="{{$item['img']}}" @if(isset($item['imgsize'])) width="{{$item['imgsize']['width']}}" height="{{$item['imgsize']['height']}}" @endif >
				@elseif(!empty($item['canvas']))
				<canvas id="{{$item['canvas']['id'].'_nbid_'.uniqid(6)}}" @if(isset($item['canvas']['width'])) width="{{$item['canvas']['width']}}" height="{{$item['canvas']['height']}}" @endif ></canvas>
				@endif
				<div class="dp-fx fx-al-ct {{(empty($item['img']) && empty($item['canvas'])) ? '' : 'mg-t-10'}}">
					<div class="bg-primary bg-@nbColor((isset($item['tagColor']))?$item['tagColor']:$item['tag']) br-rd-10">&nbsp;</div>
					<div class="label mg-l-10 bd-5 md-bd-5">{!! $item['label'] !!}</div>
				</div>
				<div class="mg-l-14 dp-fx fx-al-ct bd-1 md-bd-1">
					<span class="left">{!! $item['left'] !!}</span>
					<span class="right">{!! $item['right'] !!}</span>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
@endif