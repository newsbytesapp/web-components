@if(isset($data) && !empty($data))
<?php
$_nb_map_id = '_nb_chart_'.uniqid(6);
$templates = [
	'd3js' => '<div id="'.$_nb_map_id.'" class="map-box"></div>',
];
$js = [
	'd3js' => '<script type="text/javascript">document.addEventListener("DOMContentLoaded", function() {var data = {config}; embedMap(data,"'.$_nb_map_id.'");});</script>',
];
$data['html'] = str_replace('{config}', ''.json_encode($data['config']).'',$templates[$data['lib']]);
$data['js'] = str_replace('{config}', ''.json_encode($data['config']).'',$js[$data['lib']]);
?>

<section {!! isset($data['htmlid'])?'id="'.$data['htmlid'].'"':'' !!} class="bg-primary br-rd-0 br-rd-md-10 pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 map-parent {{isset($classList) ? $classList : ''}}">
	@if(isset($data['title']) || isset($data['socialShare']))
	<div class="dp-fx fx-dr-col fx-dr-col-md-dac neg-mg-lr-px-10-md-ac pd-l-md-10 pd-r-md-10 fx-js-bw fx-al-ct ft-pr">
		<div class="cl-lt ft-pr">
			@if(array_get($data, 'title', '') && (!empty($data['title'])))
			<div class="bd-2 md-bd-2">{!! $data['title'] !!}</div>
			@endif
			@if(array_get($data, 'subtitle', '') && (!empty($data['subtitle'])))
			<div class="bd-7 md-bd-7 mg-t-2">{!! $data['subtitle'] !!}</div>
			@endif
		</div>
		@if(isset($data['socialShare']))
		<div class="dp-fx fx-js-ct-ac fx-js-ct-md-dac">
			@include('web-components::share.social-share', [ 'data' => $data['socialShare'], 'classList' => 'mg-t-20 mg-t-md-0', 'type' => ''])
		</div>
		@endif
	</div>
    @endif
    <div class="mg-t-20 tooltip pd-t-20 pd-b-20 pd-l-20 pd-r-20 br-rd-10 box-shdw-header hidden">
        <div class="fx-row fx-js-ar bd-5 md-bd-5">
            <div class="tx-ct title"></div>
            <div class="tx-ct ltext"></div>
        </div>
    </div>
	<div class="mg-t-20">{!! $data['html'] !!}</div>
<div class="legend mg-t-20 dp-fx fx-js-end">
	<div class="col-12 col-lg-8">
		<div class="ramp dp-fx ht-px-10">
			<div class="item ht-full fx-grow-1">&nbsp;</div>
		</div>
		<div class="marker dp-fx fx-js-bw">
			<div class="item dp-fx fx-dr-col">
				<div class="bg-ink-dk line"></div>
				<div class="reg-2 md-reg-2 cl-ink-dk text"></div>
			</div>
		</div>
	</div>
</div>
	@if(isset($data['footnotes']) && !empty($data['footnotes']['data']))
	<div class="bd-1 md-bd-1 tx-rt footer">
	@foreach($data['footnotes']['data'] as $footnote)
	@include('web-components::v3.texts.footnote', ['data' => $footnote, 'classList' => '', 'type' => ''])
	@endforeach
	</div>
	@endif
</section>
@section('web-components-js')
@parent
@if(!empty($data['js'])) {!! $data['js'] !!} @endif
@endsection
@section('css')
@parent
<style type="text/css">
.ht-px-10{height:10px;}.fx-grow-1{flex-grow:1;}.legend .marker .line{width:1px; height:12px;}
</style>
@stop
@endif