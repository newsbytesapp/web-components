@if(isset($data) && !empty($data))
<?php
$_nb_chart_id = '_nb_chart_'.uniqid(6);
$templates = [
	'chartjs' => '<canvas id="'.$_nb_chart_id.'"></canvas>',
	'table' => '<div id="filteredTable'.$_nb_chart_id.'">Loading...</div>',
	'tablepg' => '<div id="filteredTable'.$_nb_chart_id.'">Loading...</div>',
];
$js = [
	'chartjs' => '<script type="text/javascript">var _nbchartready=false;var '.$_nb_chart_id.'_interval = window.setInterval(function(){if(! _nbchartready) return; clearInterval('.$_nb_chart_id.'_interval); Chart.defaults.global.defaultFontSize = 16; new Chart("'.$_nb_chart_id.'", {config});}, 300);</script>',
	'table' => '<script type="text/javascript">document.addEventListener("DOMContentLoaded", function() {var data = {config};document.getElementById("filteredTable'.$_nb_chart_id.'").innerHTML = json2table(data.data);});</script>',
	'tablepg' => '<script type="text/javascript">document.addEventListener("DOMContentLoaded", function() {var data = {config};document.getElementById("filteredTable'.$_nb_chart_id.'").innerHTML = json2table(data.data);});</script>',
];
$data['html'] = str_replace('{config}', ''.json_encode($data['config']).'',$templates[$data['lib']]);
$data['js'] = str_replace('{config}', ''.json_encode($data['config']).'',$js[$data['lib']]);
?>
@include('web-components::v3.cards.embed', ['data' => $data, 'classList' => $classList, 'type' => ''])
@endif
