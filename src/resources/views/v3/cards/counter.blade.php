@if(empty(array_get($data, 'list.data', [])))
<div {!! isset($data['htmlid'])?'id="'.$data['htmlid'].'"':'' !!} class="br-rd-0 br-rd-md-10 mg-t-20 v2 pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 {{isset($classList) ? $classList : ''}}">
	@if(isset($data['title']))
    <div class="bd-1 md-bd-1 cl-ink-dk tx-ct title">{!! $data['title'] !!}</div>
    @endif
    <div class="mg-t-30 mg-t-md-20 dp-fx fx-js-ct">
        <div class="dp-fx fx-al-ct fx-js-bw col-12 col-md-8 col-lg-6 neg-mg-lr-px-10-ac">
        	@foreach(array_get($data, 'config.data', []) as $item)
            <div class="item dp-fx fx-dr-col fx-al-ct pd-l-10 pd-r-10 cl-@nbColor((isset($item['tagColor']))?$item['tagColor']:$item['tag'])-dk" data-label="{{$item['label']}}">
            	@if(!empty($item['img']))
                <img src="{{$item['img']}}" @if(isset($item['imgsize'])) width="{{$item['imgsize']['width']}}" height="{{$item['imgsize']['height']}}" @endif >
                @elseif(!empty($item['canvas']))
                <canvas id="{{$item['canvas']['id'].'_nbid_'.uniqid(6)}}" @if(isset($item['canvas']['width'])) width="{{$item['canvas']['width']}}" height="{{$item['canvas']['height']}}" @endif ></canvas>
                @endif
                <div class="label mg-t-10 bd-5 md-bd-5 tx-ct">{!! $item['label'] !!}</div>
                <div class="left bd-1 md-bd-1 tx-ct">{!! $item['left'] !!}</div>
                <div class="right bd-5 md-bd-5 tx-ct">{!! $item['right'] !!}</div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif