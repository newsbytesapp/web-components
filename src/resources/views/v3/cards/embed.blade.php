<section {!! isset($data['htmlid'])?'id="'.$data['htmlid'].'"':'' !!} class="bg-primary br-rd-0 br-rd-md-10 pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 {{isset($classList) ? $classList : ''}}">
	@if(isset($data['title']) || isset($data['socialShare']))
	<div class="dp-fx fx-dr-col fx-dr-col-md-dac neg-mg-lr-px-10-md-ac pd-l-md-10 pd-r-md-10 fx-js-bw fx-al-ct ft-pr">
		<div class="cl-lt ft-pr">
			@if(array_get($data, 'title', '') && (!empty($data['title'])))
			<div class="bd-2 md-bd-2">{!! $data['title'] !!}</div>
			@endif
			@if(array_get($data, 'subtitle', '') && (!empty($data['subtitle'])))
			<div class="bd-7 md-bd-7 mg-t-2">{!! $data['subtitle'] !!}</div>
			@endif
		</div>
		@if(isset($data['socialShare']))
		<div class="dp-fx fx-js-ct-ac fx-js-ct-md-dac">
			@include('web-components::share.social-share', [ 'data' => $data['socialShare'], 'classList' => 'mg-t-20 mg-t-md-0', 'type' => ''])
		</div>
		@endif
	</div>
	@endif
	<div class="mg-t-20">{!! $data['html'] !!}</div>
	@if(isset($data['footnotes']) && !empty($data['footnotes']['data']))
	<div class="bd-1 md-bd-1 tx-rt footer">
	@foreach($data['footnotes']['data'] as $footnote)
	@include('web-components::v3.texts.footnote', ['data' => $footnote, 'classList' => '', 'type' => ''])
	@endforeach
	</div>
	@endif
</section>
@section('web-components-js')
@parent
@if(!empty($data['js'])) {!! $data['js'] !!} @endif
@endsection