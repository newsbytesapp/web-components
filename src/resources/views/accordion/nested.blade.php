@switch($type)
@case('amp')
@if($linkable)
<amp-accordion disable-session-states animate>
    @foreach($data as $accordionEntity)
    <section>
        <h6 class="accordion-header">
            <div class="fx-row fx-js-bw alice-blue-bg-hover br-rd-10 depth-indicator accordion-depth-{{$loop->depth - 1}} {{isset($classList) ? $classList : ''}}">
                <a href="{!! $accordionEntity['url'] !!}" class="col-6 title">{!! $accordionEntity['title'] !!}</a>
                @if(isset($accordionEntity['list']) && ($loop->depth < 2))
                <div class="col-3 cs-ptr dp-fx fx-js-end accordion-selector">
                    <span class="nb-icon-drop-more fz-18 accordion-selector"></span>
                </div>
                @endif
            </div> 
        </h6>
        @if(isset($accordionEntity['list']['data']))
        <div class="accordion-content">
            @foreach($accordionEntity['list']['data'] as $listItem)
                <div class="fx-row fx-js-bw alice-blue-bg-hover depth-indicator br-rd-10 accordion-depth-{{$loop->depth - 1}}">
                    <a href="{!! $listItem['url'] !!}" class="col-6 title">{!! $listItem['title'] !!}</a>
                    @if(isset($listItem['list']) && ($loop->depth < 2))
                    <div class="col-3 cs-ptr dp-fx fx-js-end accordion-selector">
                        <span class="nb-icon-drop-more fz-18 accordion-selector"></span>
                    </div>
                    @endif
                </div>
            @endforeach
        </div>
        @else
        <div></div>
        @endif
    </section>
  @endforeach
</amp-accordion>
@endif
@break
@default
    @if($linkable)
    <div class="accordion-slab">
        <div class="fx-row fx-js-bw alice-blue-bg-hover br-rd-10 depth-indicator accordion-depth-{{$loop->depth - 1}} {{isset($classList) ? $classList : ''}} pos-rel">
            <a href="{!! $data['url'] !!}" class="col-6 title">{!! $data['title'] !!}</a>
            @if(isset($data['list']) && ($loop->depth < 3))
            <div class="col-3 cs-ptr dp-fx fx-js-end">
                <span class="nb-icon-drop-more fz-18"></span>
	    </div>
            <div class="accordion-selector ht-full pos-abs wd-full" style="left: 0;top: 0;"></div>
            @endif
        </div>
        @if(isset($data['list']['data']))
        <div class="item-list hidden">
            @foreach($data['list']['data'] as $listItem)
                <div class="fx-row fx-js-bw alice-blue-bg-hover depth-indicator br-rd-10 accordion-depth-{{$loop->depth - 1}} pos-rel">
                    <a href="{!! $listItem['url'] !!}" class="col-6 title">{!! $listItem['title'] !!}</a>
                    @if(isset($listItem['list']) && ($loop->depth < 3))
                    <div class="col-3 cs-ptr dp-fx fx-js-end">
                        <span class="nb-icon-drop-more fz-18"></span>
		    </div>
                    <div class="accordion-selector ht-full pos-abs wd-full" style="left:0;top:0;"></div>
                    @endif
                </div>
                @if(isset($listItem['list']))
                    @foreach($listItem['list']['data'] as $recursiveItem)
                    @include('web-components::accordion.accordion', ['linkable' => true, 'type' => $type, 'classList' => 'hidden', 'data' => $recursiveItem])
                    @endforeach
                @endif
            @endforeach
        </div>
        @endif
    </div>
    @endif
@break
@endswitch
