<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
@if(array_get($data, 'list', []) && (sizeof($data['list']['data']) > 0))
<div class="bd-3 md-bd-3 cl-acadia pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 {{isset($classList) ? $classList : ''}}">{!! $data['title'] !!}</div>
<section class="bg-body ft-pr br-rd-0 br-rd-md-10 pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 accordion {{isset($classList) ? $classList : ''}}" id="accordion-{{$componentId}}">
    @foreach($data['list']['data'] as $item)
    <div class="{{$loop->first ? '' : 'pd-t-10'}} pd-b-10 {{$loop->last ? '' : 'br-b-1p-white-smoke'}} accordion-slab acc-slab">
        <div class="dp-fx fx-al-ct fx-js-bw cs-ptr accordion-selector selector">
            <div class="bd-6 md-bd-6 pd-r-10">{!! $item['title'] !!}</div>
            <div class="cl-link bd-5 md-bd-5 pd-l-10 status-indicator">
                <div class="close">+</div>
                <div class="open hidden">-</div>
            </div>
        </div>
        <div class="list hidden">
            <div class="reg-2 md-reg-2 mg-t-10">{!! $item['description'] !!}</div>
            @if(array_get($item, 'socialShare', []) && (sizeof($item['socialShare']) > 0))
                @include('web-components::share.horizontalList-hzCenterVtCenterIcons', ['data' => $item['socialShare'], 'type' => $type, 'classList' => 'mg-t-10'])
            @endif
        </div>
    </div>
    @endforeach
</section>
@endif