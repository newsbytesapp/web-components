@switch($type)
@case('amp')
<amp-accordion disable-session-states animate>
    <section>
        <h6 class="accordion-header">
            @if(array_get($data, 'linkable', true))
            <a class="dp-bl alice-blue-bg-hover ft-ter md-ter-bd-3 ter-bd-3 br-rd-10 cs-ptr pd-t-10 pd-b-10 pd-l-10 pd-r-10 {{isset($classList) ? $classList : ''}}" href="{!! isset($data['ampurl']) ? $data['ampurl'] : $data['url'] !!}">{!! $data['title'] !!}</a>
            @else
            <div class="fx-row fx-js-bw alice-blue-bg-hover br-rd-10 cs-ptr accordion-selector pd-t-10 pd-b-10 pd-l-10 pd-r-10 {{isset($classList) ? $classList : ''}}">
                <span class="ft-ter md-ter-bd-3 ter-bd-3">{!! $data['title'] !!}</span>
                <span class="nb-icon-drop-more fz-18"></span>
            </div>
            @endif
        </h6>
        <div class="accordion-content">
            @if(array_get($data, 'list.data', []))
            @foreach($data['list']['data'] as $item)
            <a class="dp-bl alice-blue-bg-hover ft-ter md-ter-reg-2 ter-reg-2 br-rd-10 cs-ptr depth-indicator pd-t-10 pd-b-10 pd-l-10 pd-r-10" href="{!! $item['url'] !!}">{!! $item['title'] !!}</a>
            @endforeach
            @endif
        </div>
    </section>
</amp-accordion>
@break
@default
<div class="accordion-slab">
    @if(array_get($data, 'linkable', true))
    <a class="dp-bl alice-blue-bg-hover md-bd-5 bd-5 br-rd-10 cs-ptr pd-t-10 pd-b-10 pd-l-10 pd-r-10 {{isset($classList) ? $classList : ''}}" href="{!! $data['url'] !!}">{!! $data['title'] !!}</a>
    @else
    <div class="fx-row fx-js-bw alice-blue-bg-hover br-rd-10 cs-ptr accordion-selector pd-t-10 pd-b-10 pd-l-10 pd-r-10 {{isset($classList) ? $classList : ''}}">
        <span class="md-bd-5 bd-5">{!! $data['title'] !!}</span>
        <span class="nb-icon-drop-more fz-18"></span>
    </div>
    <div class="list hidden">
        @if(array_get($data, 'list.data', []))
        @foreach($data['list']['data'] as $item)
        <a class="dp-bl alice-blue-bg-hover md-reg-2 reg-2 br-rd-10 cs-ptr depth-indicator pd-t-10 pd-b-10 pd-l-10 pd-r-10" href="{!! $item['url'] !!}">{!! $item['title'] !!}</a>
        @endforeach
        @endif
    </div>
    @endif
</div>
@break
@endswitch
