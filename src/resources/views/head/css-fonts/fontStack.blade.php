@if(array_get($data, 'type', 'google') == 'google')
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
@endif
<?php
$fonturl = '';
$datatype = array_get($data, 'type', 'google');
$event = array_get($data, 'event', '');
switch($data['lang'])
{
    case 'hi':
    $fonturl = ($datatype == 'self') ? config('web-components.services.'.config('web-components.services.assetSource').'.url', '').'/assets/css/fonts-hi.4.1.css' : 'https://fonts.googleapis.com/css2?family=Mukta:wght@400;600&family=Noto+Sans:wght@400;700&display=swap';
    break;
    case 'ta':
    $fonturl = ($datatype == 'self') ? config('web-components.services.'.config('web-components.services.assetSource').'.url', '').'/assets/css/fonts-ta.4.5.css' : 'https://fonts.googleapis.com/css2?family=Noto+Sans+Tamil:wght@400;600&display=swap';
    break;
    case 'te':
    $fonturl = ($datatype == 'self') ? config('web-components.services.'.config('web-components.services.assetSource').'.url', '').'/assets/css/fonts-te.4.5.css' : 'https://fonts.googleapis.com/css2?family=Noto+Sans+Telugu:wght@400;600&display=swap';
    break;
    case 'en':
    default:
    $fonturl = ($datatype == 'self') ? config('web-components.services.'.config('web-components.services.assetSource').'.url', '').'/assets/css/fonts-en.4.5.css' : 'https://fonts.googleapis.com/css2?family=Caladea:wght@400;700&family=Frank+Ruhl+Libre:wght@500&family=Inter:wght@400;600&display=swap';
    break;
}
?>
@if(!empty($event))
<script>
    window.addEventListener({{$event}}, function(event) {
        let link = document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = "{{$fonturl}}";

        document.getElementsByTagName('HEAD')[0].appendChild(link);
        window.removeEventListener({{$event}}, this);
    });
</script>
@else
    <link rel="preload" href="{{$fonturl}}" as="style" onload="this.onload=null;this.rel='stylesheet'" />
    <noscript><link rel="stylesheet" href="{{$fonturl}}" /></noscript>
@endif