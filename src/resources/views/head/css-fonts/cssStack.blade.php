@if(isset($event) && !empty($event))
<script>
    window.addEventListener({{$event}}, function(event) {
        let link = document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = "{!! $data['path'] !!}";

        document.getElementsByTagName('HEAD')[0].appendChild(link);
        window.removeEventListener({{$event}}, this);
    });
</script>
@else
<link rel="{{$data['preload'] ? 'preload' : 'stylesheet'}}" href="{!! $data['path'] !!}" name="{!! $data['name'] !!}" as="style" @if($data['self_onload']) onload="this.onload=null;this.rel='stylesheet';window.css_loaded=window.css_loaded||[];window.css_loaded.push(this.name)" @endif><noscript><link rel="stylesheet" href="{!! $data['path'] !!}"></noscript>
@endif