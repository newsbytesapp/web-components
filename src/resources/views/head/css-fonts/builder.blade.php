@if(!empty(array_get($data, 'loadCss.path', '')))
@include('web-components::head.css-fonts.cssStack', ['data' => array_merge(array_get($data, 'loadCss', []), ['name' => array_get($data, 'loadCss.name', array_get($data, 'loadCss.path', bin2hex(random_bytes(6/2)))),'path' => array_get($data, 'loadCss.path', ''), 'event' => array_get($data, 'event', ''), 'preload' => array_get($data, 'loadCss.preload', false), 'self_onload' => array_get($data, 'loadCss.self_onload', false)])])
@endif
@if(!empty(array_get($data, 'loadFonts', [])))
@include('web-components::head.css-fonts.fontStack', ['data' => array_merge(array_get($data, 'loadFonts', []), ['lang' => array_get($data, 'loadFonts.lang', 'en'), 'type' => array_get($data, 'loadFonts.type', 'google'), 'event' => array_get($data, 'event', '')])])
@endif