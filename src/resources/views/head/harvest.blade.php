@include('web-components::head.seo', ['data' => $seoData])
@include('web-components::head.meta', ['data' => $seoData])
@include('web-components::head.favicon')
@include('web-components::head.analytics')
@include('web-components::head.js')