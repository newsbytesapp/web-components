<?php
if (!function_exists('seoReady')) {
    function seoReady($key){
        return strip_tags(str_replace('"', "", str_replace("\n", " ", $key)));
    }
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>{!! $data['title'] !!}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
@if(isset($data['description']) && !empty($data['description']))
<meta name="description" content="{!! seoReady($data['description']) !!}" />
@endif
@if(isset($data['keywords']) && !empty($data['keywords']))
<meta name="keywords" content="{!! seoReady($data['keywords']) !!}" />
@endif