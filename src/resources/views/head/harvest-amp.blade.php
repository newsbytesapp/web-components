@include('web-components::head.seo', ['data' => $seoData])
@include('web-components::head.meta', ['data' => $seoData])
@include('web-components::head.favicon')