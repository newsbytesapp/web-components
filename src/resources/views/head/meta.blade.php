@if(isset($data['news_keywords']) && !empty($data['news_keywords'])) <meta name="news_keywords" content="{!! seoReady($data['keywords']) !!}"> @endif
<meta property="og:site_name" content="NewsBytes">
<meta property="og:type" content="article">
@if(isset($data['title']) && !empty($data['title'])) <meta property="og:title" content="{!! isset($data['ogTitle']) ? seoReady($data['ogTitle']) : seoReady($data['title']) !!}"> @endif
@if(isset($data['description']) && !empty($data['description'])) <meta property="og:description" content="{!! seoReady($data['description']) !!}"> @endif
@if(isset($data['url']) && !empty($data['url'])) <meta property="og:url" content="{!! $data['url'] !!}"> @endif
@if(isset($data['image'])) 
<meta property="og:image" content="{!! array_get($data, 'image.src', '') !!}"> 
<meta property="og:image:width" content="{{array_get($data, 'image.width', 470)}}" />
<meta property="og:image:height" content="{{array_get($data, 'image.height', 246)}}" /> 
@endif
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:site" content="@newsbytesapp">
@if(isset($data['title']) && !empty($data['title'])) <meta property="twitter:title" content="{!! seoReady($data['title']) !!}"> @endif
@if(isset($data['description']) && !empty($data['description'])) <meta property="twitter:description" content="{!! seoReady($data['description']) !!}"> @endif
@if(isset($data['url']) && !empty($data['url'])) <meta property="twitter:url" content="{!! $data['url'] !!}"> @endif
@if(isset($data['image'])) <meta property="twitter:image" content="{!! array_get($data, 'image.src', '') !!}"> @endif
@if(isset($data['lastModified']) && !empty($data['lastModified'])) <meta http-equiv="Last-Modified" content="{!! $data['lastModified'] !!}"> @endif
@if(isset($data['lastModified']) && !empty($data['lastModified'])) <meta name="Last-Modified" content="{!! $data['lastModified'] !!}"> @endif
@if(isset($data['lastModified']) && !empty($data['lastModified'])) <meta name="Last-Modified-Date" content="{!! $data['lastModified'] !!}"> @endif
@if(isset($data['lastModifiedTime']) && !empty($data['lastModifiedTime'])) <meta name="Last-Modified-Time" content="{!! $data['lastModifiedTime'] !!}"> @endif
<meta property="fb:pages" content="681371421973844" />
<meta name="pocket-site-verification" content="c9aeb5661f69e37caab618602e7539" />
{{-- <meta id="#token" name="csrf-token" content="{{csrf_token()}}" /> --}}
<meta name="application-name" content="NewsBytes - Your Daily News Digest">
{{-- <link rel="preload" href="/css/fonts/webpage-icons.6.woff" as="font"  crossorigin="" /> --}}
<link rel="preconnect" href="https://i.cdn.newsbytesapp.com"  crossorigin="" />
{{-- <link rel="dns-prefetch" href="https://www.facebook.com"  crossorigin="" /> --}}
<link rel="dns-prefetch" href="https://www.google-analytics.com"  crossorigin="" />
<link rel="dns-prefetch" href="https://storage.googleapis.com"  crossorigin="" />
<link rel="dns-prefetch" href="https://stats.g.doubleclick.net"  crossorigin="" />