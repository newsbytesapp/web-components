<link rel="preload" type="text/javascript" as="script" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" />
<link rel="preload" type="text/javascript" as="script" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js" />
<link rel="preload" type="text/javascript" as="script" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/js/packageMaster.6.js" />
<link rel="preload" type="text/javascript" as="script" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js" />
@if(isset($partner) && !empty($partner))
<link rel="preload" as="script" href="{!! Common::mix_assets('/js/components-partner.js') !!}">
@else
<link rel="preload" as="script" href="{!! Common::mix_assets('/js/components.js') !!}">
@endif