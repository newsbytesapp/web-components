<link rel="manifest" href="/manifest.json" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" sizes="57x57" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="60x60" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon" sizes="72x72" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/apple-touch-icon-152x152.png" />
<link rel="apple-touch-icon" sizes="180x180" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/apple-touch-icon-180x180.png" />
<link rel="apple-touch-startup-image" media="(-webkit-device-pixel-ratio: 1)" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/apple-touch-icon-512x512.png" />
<link rel="apple-touch-startup-image" media="(-webkit-device-pixel-ratio: 2)" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/apple-touch-icon-1024x1024.png" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<meta name="mobile-web-app-capable" content="yes" />
<link rel="icon" type="image/png" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/android-chrome-192x192.png" sizes="192x192" />
<link rel="icon" type="image/png" sizes="240x240" href="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/android-chrome-240x240.png" sizes="240x240" />
<meta name="msapplication-TileColor" content="#ffffff" />
<meta name="msapplication-TileImage" content="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/icons/2/apple-touch-icon-144x144.png" />
<meta name="msapplication-config" content="/browserconfig.xml" />
<meta name="msapplication-square70x70logo" content="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/icons/smalltile.png" />
<meta name="msapplication-square150x150logo" content="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/icons/mediumtile.png" />
<meta name="msapplication-square310x310logo" content="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/icons/largetile.png" />
<meta name="theme-color" content="#ffffff"/>
<meta name="msapplication-navbutton-color" content="#ffffff" />
