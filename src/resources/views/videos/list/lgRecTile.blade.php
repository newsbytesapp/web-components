<div class="fx-row list-lgRecTile">
    <?php 
        $itemClassList = 'col-' . (12 / (array_get($list, 'layout.sm', 1))) . ' col-md-' . (12 / (array_get($list, 'layout.md', 1))) . ' col-lg-' . (12 / (array_get($list, 'layout.lg', 1)));
    ?>
    @foreach($list['data'] as $item)
        @include('web-components::videos.items.lgRecTile', ['data' => array_merge($item, ['classList' => $itemClassList])])
    @endforeach
</div>