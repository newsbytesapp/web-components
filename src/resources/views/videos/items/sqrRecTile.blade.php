<div class="dp-fx pd-l-md-10 pd-r-md-10 mg-t-20 mg-t-md-30 cs-ptr play-video {{array_get($data, 'classList', '')}}" data-video="{{$data['video']['src']}}">
    <div class="col-4 col-md-1 fig-container--wd-ht-md-60 pos-rel">
        <figure class="lazy fig fig--bg-img-ct-cov-props ht-full-dac ht-full-md-ac asp-ratio-rect-ac asp-ratio-rect-md-dac" data-src="{{$data['thumb']}}" title="{!! $data['title'] !!}">
            <img data-src="{{$data['thumb']}}" alt="{!! $data['title'] !!}" title="{!! $data['title'] !!}" class="lazy hidden inset-img-hidden"/>
            <div class="pos-abs tp-init lt-init rt-init bt-init solid-tint-black-black40"></div>
            <div class="cl-primary nb-icon-video-play-button tf-x-y-middle lt-half tp-half pos-abs fz-20"></div>
        </figure>
    </div>
    <div class="tx-container col-8 pd-l-10">
        <div class="ft-pr bd-6 md-bd-6 cl-primary">{!! $data['title'] !!}</div>
    </div>
</div>