<div class="fx-row pd-l-md-10 pd-r-md-10 mg-t-20 cs-ptr play-video {{array_get($data, 'classList', '')}}" data-video="{{$data['video']['src']}}">
    <div class="col-6 pos-rel">
        <img alt="{!! $data['title'] !!}" title="{!! $data['title'] !!}" class="lazy wd-full dp-bl" data-src="{{$data['thumb']}}">
        <div class="pos-abs tp-init lt-init rt-init bt-init solid-tint-black-black40"></div>
        <div class="cl-primary nb-icon-video-play-button tf-x-y-middle lt-half tp-half pos-abs fz-30"></div>
    </div>
    <div class="tx-container col-6 pd-l-10 ft-pr">
        <div class="bd-5 md-bd-5 cl-primary">{!! $data['title'] !!}</div>
    </div>
</div>