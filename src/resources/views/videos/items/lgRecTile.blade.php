<div class="fx-row fx-al-ct pd-l-md-10 pd-r-md-10 mg-t-20 cs-ptr play-video {{array_get($data, 'classList', '')}}" data-video="{{$data['video']['src']}}">
    <div class="col-12 col-lg-6 pos-rel">
        <img alt="{!! $data['title'] !!}" title="{!! $data['title'] !!}" class="lazy wd-full dp-bl" data-src="{{$data['img']}}">
        <div class="pos-abs tp-init lt-init rt-init bt-init solid-tint-black-black40"></div>
        <div class="cl-primary nb-icon-video-play-button tf-x-y-middle lt-half tp-half pos-abs fz-50"></div>
    </div>
    <div class="tx-container col-12 col-lg-6 pd-l-md-20 ft-pr">
        <div class="bd-3 md-bd-3 cl-primary mg-t-10 mg-t-lg-0">{!! $data['title'] !!}</div>
    </div>
</div>