<section class="solid-tint-black-black90 pos-fix wd-full ht-full tp-init bt-init lt-init rt-init z-index-3 ent-fade-in animation-dur-scale-3 video-player hidden" tabindex="0">
    <div class="pos-rel ht-full wrapper">
        <span class="nb-icon-cancel-thin cs-ptr cl-primary rt-init pos-abs cross-icon close-video-player"></span>
        <div class="wd-full ht-full dp-fx fx-js-ct fx-al-ct">
            <div class="col-12 col-md-10 col-lg-8">
                <div class="pos-rel asp-ratio-rect-ac" id="frame-container">
                </div>
            </div>
        </div>
    </div>
</section>
@section('css')
@parent
<style type="text/css">
    .video-player.active{display:block;}
    .video-player .cross-icon{top:30px;font-size:40px;}
</style>
@stop
@section('pageScript')
@parent
    if (!window['YT']) {var YT = {loading: 0,loaded: 0};}if (!window['YTConfig']) {var YTConfig = {'host': 'http://www.youtube.com'};}if (!YT.loading) {YT.loading = 1;(function(){var l = [];YT.ready = function(f) {if (YT.loaded) {f();} else {l.push(f);}};window.onYTReady = function() {YT.loaded = 1;for (var i = 0; i < l.length; i++) {try {l[i]();} catch (e) {}}};YT.setConfig = function(c) {for (var k in c) {if (c.hasOwnProperty(k)) {YTConfig[k] = c[k];}}};var a = document.createElement('script');a.type = 'text/javascript';a.id = 'www-widgetapi-script';a.src = 'https://s.ytimg.com/yts/jsbin/www-widgetapi-vflBs9Ibw/www-widgetapi.js';a.async = true;var c = document.currentScript;if (c) {var n = c.nonce || c.getAttribute('nonce');if (n) {a.setAttribute('nonce', n);}}var b = document.getElementsByTagName('script')[0];b.parentNode.insertBefore(a, b);})();}
@stop