@if(sizeof($data['list']['data']) > 0)
<section class="bg-primary  pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 {{isset($classList) ? $classList : ''}} card-lgRecTile-sqrRecTile">
    <div class="cl-primary ft-pr bd-3 md-bd-3 pd-t-10 pd-l-md-10 pd-r-md-10">{!! $data['title'] !!}</div>

    @include('web-components::videos.list.lgRecTile', ['list' => array_merge(['data' => array_splice($data['list']['data'], 0, 1)], ['layout' => ['lg' => 1, 'md' => 1, 'sm' => 1]])])

    @include('web-components::videos.list.sqrRecTile', ['list' => array_merge(['data' => array_splice($data['list']['data'], 0, 3)], ['layout' => ['lg' => 3, 'md' => 3, 'sm' => 1]])])

    @if((array_has($data, 'list.link')) && (sizeof($data['list']['link']) > 0))
    <div class="pd-t-30 pd-b-20 pd-l-md-10">
        @include('web-components::links.link', ['link' => $data['list']['link'], 'classList' => ''])
    </div>
    @endif
    
</section>
@endif