@if(sizeof($data['list']['data']) > 0)
<section class="bg-primary  pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 {{isset($classList) ? $classList : ''}} card-xlRecTile-smRecTile">

    @include('web-components::videos.list.xlRecTile', ['list' => array_merge(['data' => array_splice($data['list']['data'], 0, 1)], ['layout' => ['lg' => 1, 'md' => 1, 'sm' => 1]])])

    <div class="cl-lt ft-pr bd-3 md-bd-3 mg-t-30 pd-l-md-10 pd-r-md-10">{!! $data['title'] !!}</div>

    @include('web-components::videos.list.smRecTile', ['list' => array_merge(['data' => $data['list']['data']], ['layout' => ['lg' => 3,'md' => 2, 'sm' => 1]])])

    @if((array_has($data, 'list.button')) && (sizeof($data['list']['button']) > 0))
    <div class="pd-t-30 pd-b-20 pd-l-md-10 pd-r-md-10">
        @include('web-components::buttons.outline', [ 'button' => $data['list']['button'], 'type' => $type, 'searchFor' => '.card-xlRecTile-smRecTile', 'appendTo' => '.list-smRecTile', 'template' => 'listItemSmRecTile' ])
    </div>
    @endif

</section>
@endif