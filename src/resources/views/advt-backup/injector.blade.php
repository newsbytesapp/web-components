@switch($type)
@case('amp')
    @if(array_get($data, 'active', false))
        <?php
            $size = config('web-components.advt.slotIdsDfp.'.array_get($data, 'slotId', 's3'). '.size');
        ?>
        <div class="{{isset($classList) ? $classList : ''}} slot-size-{{config('web-components.advt.slotIds.'.array_get($data, 'slotId', 's3').'.size')}}">
            <amp-ad class="{{isset($classList) ? $classList : ''}} slot-size-{{config('web-components.advt.slotIds.'.array_get($data, 'slotId', 's3').'.size')}}" layout="responsive" type="adsense" width="{{config('web-components.advt.slotAttrs.'.$size.'.width')}}" height="{{config('web-components.advt.slotAttrs.'.$size.'.height')}}" data-ad-client="{{config('web-components.advt.clientId')}}" data-ad-slot="{{config('web-components.advt.slotIdsDfp.'.array_get($data, 'slotId', 's3').'.id')}}"></amp-ad>
        </div>
    @endif
@break
@default
    @if(array_get($data, 'active', false))
        <div class="{{isset($classList) ? $classList : ''}} slot-size-{{config('web-components.advt.slotIds.'.array_get($data, 'slotId', 's3').'.size')}}">
            <div id="{{config('web-components.advt.slotIds.'.array_get($data, 'slotId', 's3').'.id')}}"></div>
        </div>
    @endif
@break
@endswitch