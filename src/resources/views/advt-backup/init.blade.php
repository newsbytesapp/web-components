@switch($type)
@case('amp')
    @section('ampScripts')
    @parent
        @if(array_get($data, 'script.active', false))
        <script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-latest.js"></script>
        @endif
    @stop
@break
@default
    @section('pageScript')
    @parent
        @if(array_get($data, 'script.active', false))
        <script type="text/javascript">
            var advtModule = {script : 'https://go.automatad.com/geo/1kcCU0/afihbs.js'};
            @if(array_get($data, 'moreAdvts.active', false))
            advtModule['slotIds'] = JSON.parse('{!! json_encode(config('web-components.advt.slotIds')) !!}');
            advtModule['slotAttrs'] = JSON.parse('{!! json_encode(config('web-components.advt.slotAttrs')) !!}');
            advtModule['moreAdvts'] = JSON.parse('{!! json_encode($data['moreAdvts']) !!}');
            @endif
        </script>
        @endif
    @stop
@break
@endswitch