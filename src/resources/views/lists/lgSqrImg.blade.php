@include('web-components::containers.simpleDiv', [
    'child' => 'web-components::lists.items.lgSqrImg',
    'class' =>  $classList,
    'info' => ['data' => $data, 'type' => $type, 'linkTargetAttr' => $linkTargetAttr, 'tint' => true]
])