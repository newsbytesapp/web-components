<div class="pd-b-20">
    @if(isset($data['title']))
    <div class="ft-ter ter-bd-2  pd-b-16 pd-l-16 pd-l-md-4  br-b-1p-dotted-weathered-stone  ter-bd-3">{!!$data['title']!!}</div>
    @endif
    @if(isset($data['type']) && $data['type']=='images')
    <div class="dp-fx fx-wrap fx-js-ar pd-t-20">
    @foreach($data['list'] as $item)
    @include('web-components::lists.items.titleWithRoundedImg', ['data' => $item,'classList' => ''])
    @endforeach
    </div>
    @else
    @foreach($data['list'] as $item)
    @include('web-components::lists.items.titleWithSubs', ['data' => $item,'classList' => ''])
    @endforeach
    @endif
</div>