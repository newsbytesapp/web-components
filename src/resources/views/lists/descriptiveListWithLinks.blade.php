<div class="fx-row {{isset($classList) ? $classList : ''}}">
    @foreach($data as $item)
        @include('web-components::lists.items.descriptiveListWithLinks', ['data' => $item, 'classList' => ''])
    @endforeach
</div>