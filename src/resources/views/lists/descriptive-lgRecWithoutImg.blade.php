@include('web-components::containers.simpleDiv', [
    'child' => 'web-components::lists.items.descriptive-lgRecWithoutImg',
    'class' => 'fx-row list-descriptive-lgRecWithoutImg',
    'info' => ['data' => $data, 'type' => $type, 'linkTargetAttr' => $linkTargetAttr]
])