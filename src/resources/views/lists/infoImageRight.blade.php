@include('web-components::containers.simpleDiv', [
    'child' => 'web-components::lists.items.infoImageRight',
    'class' => 'fx-row neg-mg-lr-px-10-dac neg-mg-lr-px-10-md-ac',
    'info' => ['data' => $data, 'type' => $type, 'linkTargetAttr' => $linkTargetAttr]
])