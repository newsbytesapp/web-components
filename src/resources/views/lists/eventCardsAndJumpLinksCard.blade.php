@foreach($data['events']['list']['data'] as $item)
    @if($loop->index == 1)
    @include('web-components::cards.eventsJumpLinksCard', ['data' => array_merge($data['jumpLinks'], ['htmlId' => $item['htmlId']]), 'type' => $type, 'classList' => ''])
    @endif
    @include('web-components::lists.items.eventCard', ['data' => $item, 'type' => $type, 'classList' => ''])
@endforeach