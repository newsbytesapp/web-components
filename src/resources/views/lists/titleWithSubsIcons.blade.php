<?php
    $tag = array_get($data, 'tagEn', 'default');
?>
<div class="dp-fx fx-js-bw pd-b-10 pd-l-10 pd-r-10 pd-t-10  br-b-1p-pale-lavender {{isset($classList) ? $classList : ''}}">
    <div class="dp-fx fx-dr-col fx-js-ct  {{isset($data['list']) ?  'pd-b-10 pd-t-10 pd-l-10' : 'pd-b-30 pd-t-30' }}">
        <span class="ft-ter ter-bd-4">{{$data['title']}}</span>
        <span class="cl-link mg-t-4 ter-bd-2">{{$data['subtitle']}}</span>
    </div>
</div>
@if(isset($data['list']))
<div class="bg-@nbColor($tag)-lt">
    @if(isset($data['description']))
        <div class="ft-ter  pd-b-16 pd-l-20  pd-t-16 br-b-1p-dotted-weathered-stone ter-bd-3">{!!$data['description']!!}</div>
    @endif
    <div class="dp-fx mg-l-20 mg-t-4 fx-wrap">
        @foreach($data['list'] as $item)
            @include('web-components::lists.items.titleWithSubsIcons', ['data' => $item,'classList' => ''])
        @endforeach
    </div>
</div>
@endif
