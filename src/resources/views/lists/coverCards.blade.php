@foreach($data as $cardItem)
    @include('web-components::lists.items.coverCard', ['data' => $cardItem, 'type' => $type, 'classList' => '', 'attributes' => $attributes])
@endforeach