@include('web-components::containers.simpleDiv', [
    'child' => 'web-components::lists.items.tagged-sqrRecImgVertical',
    'class' => '',
    'info' => ['data' => $data, 'type' => $type, 'linkTargetAttr' => $linkTargetAttr]
])