@include('web-components::containers.simpleDiv', [
    'child' => 'web-components::lists.items.taggedSqrRecNoImg',
    'class' => 'fx-row list-sqrRecImg',
    'info' => ['data' => $data, 'type' => $type, 'linkTargetAttr' => $linkTargetAttr]
])