@include('web-components::containers.simpleDiv', [
    'child' => 'web-components::lists.items.lgRecImg',
    'class' => 'fx-row',
    'info' => ['data' => $data, 'type' => $type, 'linkTargetAttr' => $linkTargetAttr]
])