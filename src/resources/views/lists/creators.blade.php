<div class="{{isset($classList) ? $classList : ''}}">
    @foreach($data as $creator)
        @include('web-components::lists.items.creator', ['data' => $creator, 'type' => $type, 'classList' => 'pd-l-10 pd-r-10'])
    @endforeach
</div>