<?php $index = 0; ?>
@foreach($data as $cardItem)
    @if(isset($advt) && (!is_null($advt)))
        @if(array_key_exists($index, $advt['data']['after']))
            @if($loop->index == ($advt['data']['after'][$index]))
                @include('web-components::advt.injector', ['data' => $advt['data'], 'classList' => $advt['data']['classList'], 'type' => $type])
                <?php $index++; ?>
            @endif
        @endif
    @endif
    @include('web-components::lists.items.eventCard', ['data' => $cardItem, 'type' => $type, 'classList' => ''])
@endforeach