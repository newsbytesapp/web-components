@foreach($data as $item)
    @include('web-components::lists.items.queryFeedback', ['data' => $item, 'type' => $type, 'classList' => 'mg-t-20'])
@endforeach