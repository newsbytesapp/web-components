@include('web-components::containers.simpleDiv', [
    'child' => 'web-components::lists.items.smRecImg',
    'class' => 'fx-row',
    'info' => ['data' => $data, 'type' => $type, 'linkTargetAttr' => $linkTargetAttr]
])