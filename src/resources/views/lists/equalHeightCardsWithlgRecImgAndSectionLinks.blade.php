<div class="fx-row neg-mg-lr-px-10-md-ac">
    @foreach($data as $item)
        @include('web-components::lists.items.equalHeightCardsWithlgRecImgAndSectionLinks', ['data' => $item, 'classList' => '', 'type' => $pageType])
    @endforeach
</div>