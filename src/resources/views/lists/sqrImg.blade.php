@if(isset($largerVersion) && $largerVersion)
@include('web-components::containers.simpleDiv', [
    'child' => 'web-components::lists.items.sqrImg',
    'class' => 'fx-row',
    'info' => ['data' => $data, 'type' => $type, 'linkTargetAttr' => $linkTargetAttr, 'largerVersion' => $largerVersion]
])
@else
@include('web-components::containers.simpleDiv', [
    'child' => 'web-components::lists.items.sqrImg',
    'class' => 'fx-row',
    'info' => ['data' => $data, 'type' => $type, 'linkTargetAttr' => $linkTargetAttr]
])
@endif