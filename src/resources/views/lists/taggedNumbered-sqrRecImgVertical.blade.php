@include('web-components::containers.simpleDiv', [
    'child' => 'web-components::lists.items.taggedNumbered-sqrRecImgVertical',
    'class' => '',
    'info' => ['data' => $data, 'type' => $type, 'linkTargetAttr' => $linkTargetAttr]
])