<div class=" dp-fx  fx-dr-col fx-dr-row-md-ac  col-12 fx-wrap fx-js-bw  {{isset($classList) ? $classList : ''}}" >
    @foreach($data as $item)
        @include('web-components::lists.items.lgRecImgWithTitleAndTags',['data'=>$item])
    @endforeach
</div>