@include('web-components::containers.simpleDiv', [
    'child' => 'web-components::lists.items.comparison',
    'class' => '',
    'info' => ['data' => $data, 'type' => $type, 'linkTargetAttr' => $linkTargetAttr]
])