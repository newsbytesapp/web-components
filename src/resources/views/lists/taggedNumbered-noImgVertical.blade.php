@include('web-components::containers.simpleDiv', [
    'child' => 'web-components::lists.items.taggedNumbered-noImgVertical',
    'class' => '',
    'info' => ['data' => $data, 'type' => $type, 'linkTargetAttr' => $linkTargetAttr, 'withTagUrl' => $withTagUrl]
])