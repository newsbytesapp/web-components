<div class="dp-fx fx-dr-col fx-dr-row-md-ac col-12 fx-wrap fx-js-ar {{isset($classList) ? $classList : ''}}" >
    @foreach($data as $index => $event)
        @if($loop->first)
            @include('web-components::lists.items.lgRecImgWithTitle', ['data' => $event, 'classList' => 'col-12'])
        @else
            @include('web-components::lists.items.titleWithTag', ['data' => $event, 'classList' => 'col-12'])
            @if(!$loop->last)
            <div class="pd-l-20 pd-r-20 wd-full">
                <div class="mg-t-20 wd-full br-t-1p-gainsboro"></div>
            </div>
            @endif
        @endif
    @endforeach
</div>