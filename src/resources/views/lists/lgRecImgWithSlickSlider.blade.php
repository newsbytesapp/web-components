<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
@if(array_get($data, 'list.data', []) && (sizeof($data['list']['data']) > 0))
<section class="bg-primary slide-show-parent visibility-hidden pd-t-20 pd-b-20 pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 {{isset($classList) ? $classList : ''}}" id="slideshow-parent-{{$componentId}}">
    <div class="dp-fx fx-js-bw pd-b-10 pd-l-16 pd-r-16 pd-t-10 br-b-1p-pale-lavender">
        <div class="ft-ter ter-bd-6 md-ter-bd-6 cl-venetian-red">{!! $data['title'] !!}</div>
        <div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct" id="slideshow-imt-{{$componentId}}-dots"></div>
    </div>
    <section class="mg-t-20 slide-show default" id="slideshow-imt-{{$componentId}}">
        @foreach($data['list']['data'] as $item)
            <div class="min-wd-px-100 mg-r-16 mg-l-16">
                <a class="dp-bl wd-inherit" href="{!! $item['url'] !!}">
                    <img alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="dp-bl br-rd-10 wd-full" src="{{$item['img']}}">
                    <div class="pos-fix tp-init ht-full wd-inherit br-rd-10 ovr-hidden">
                        <div class="ht-full wd-inherit" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), #000);"></div>
                    </div>
                    <div class="pos-fix bt-init cl-primary pd-b-20 pd-l-12 pd-r-12 wd-inherit ovr-hidden">
                        <div class="dp-fx fx-al-ct">
                            <span class="ft-ter ter-bd-1 md-ter-bd-1">{!! $item['tag'] !!}</span>
                            <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-primary opacity-50"></div>
                            <span class="ft-ter ter-reg-1 md-ter-reg-1">{!! $item['timestamp'] !!}</span>
                        </div>
                        <h2 class="ft-sec sec-bd-6 md-sec-bd-6 pd-t-6">{!! $item['title'] !!}</h2>
                    </div>
                </a>
            </div>
        @endforeach
    </section>
</section>
@endif

@section('css')
<style type="text/css">
#slideshow-imt-{{$componentId}}-dots.slide-show-dots.light-theme .slick-dots li[role=presentation].slick-active:before {
    width:10px;
    display: inline-flex;
    background-color: #c7091b;
    height: 6px;
    border-radius: 43px;
    font-size: 0;
    margin-bottom: 10px;
    align-items: end;
}
#slideshow-parent-{{$componentId}}.visibility-hidden{
    display: none;
}
</style>
@stop