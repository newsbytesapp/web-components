@if(count($data) > 0)
<section class="bg-primary  pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 lists-composite-smRecImg-sqrRecImg {{isset($classList) ? $classList : ''}}">

    @include('web-components::lists.smRecImg', ['data' => array_splice($data, 0, 4), 'type' => $type, 'linkTargetAttr' => $linkTargetAttr])

    @include('web-components::lists.sqrRecImg', ['data' => array_splice($data, 0, 12), 'type' => $type, 'linkTargetAttr' => $linkTargetAttr])

    @if(isset($button['size']))
    <div class="pd-t-30 pd-b-20">
        @include('web-components::buttons.plain', [ 'button' => $button, 'type' => $type, 'searchFor' => '.lists-composite-smRecImg-sqrRecImg', 'appendTo' => '.list-sqrRecImg', 'template' => 'listItemSqrRecImg' ])
    </div>
    @endif
   
</section>
@endif