<div class="fx-row">
    @foreach($data as $item)
        @include('web-components::lists.items.recImgWithTitle', ['data' => $item, 'linkTargetAttr' => $htmlTargetAttr, 'classList' => 'col-12 col-md-6'])
    @endforeach
</div>