<div class=" dp-fx  fx-dr-col fx-dr-row-md-ac  fx-js-bw pd-l-4 pd-r-16 col-12 fx-wrap {{isset($classList) ? $classList : ''}}" >
    @foreach($data as $item)
        @include('web-components::lists.items.lgRecImgWithTags',['data'=>$item])
    @endforeach
</div>