@foreach($data as $cardItem)
    @include('web-components::lists.items.coverCardWithCreators', ['data' => $cardItem, 'type' => $type, 'classList' => '', 'attributes' => $attributes])
@endforeach