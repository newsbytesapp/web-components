<div class=" dp-fx  fx-dr-col fx-dr-row-md-ac  col-12 fx-wrap fx-js-bw  {{isset($classList) ? $classList : ''}}" >
    @foreach($data as $item)
        @include('web-components::v3.entities.movies.movie',['data'=>$item])
    @endforeach
</div>