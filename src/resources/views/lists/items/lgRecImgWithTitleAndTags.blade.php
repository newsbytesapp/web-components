<?php
    $tag = array_get($data, 'tag', 'default');
?>
<div class="bg-primary  mg-b-10 mg-r-4 mg-l-4 mg-t-4 col-12  col-md-6 col-lg-5 dp-fx fx-js-bw fx-dr-col fx-basis-48pr br-rd-4 {{isset($classList) ? $classList : ''}}" >
    <div class="dp-fx fx-js-bw fx-al-stretch mg-b-10 mg-l-10 mg-r-10 mg-t-10">
        <div class=" dp-fx fx-al-ct fx-js-bw">
            <div class="mg-r-10 wd-px-100 " >
                @if(isset($data['img']))
                <img data-src="{!! $data['img'] !!}" alt="{{$data['title']}}" class="lazy wd-full obj-fit-cover asp-r-2-3">
                @endif
            </div>
            <div class="dp-fx fx-dr-col"  style="max-width: calc(100% - 110px);">
                <p class="cl-ink-dk ft-ter ter-bd-3 mg-r-2 mg-b-6 tx-lt">{!! $data['title'] !!}</p>
                @if(is_array($data['subtitle']) && count($data['subtitle']) > 1)
                <span class="mg-t-2  ft-ter mg-b-8   tx-lt ter-reg-1 cl-lt"> {!! $data['subtitle'][2] !!}</span> 
                <div class="mg-b-6">
                    <span class="mg-t-2  ft-ter   mg-r-2 ter-reg-1"> {!! $data['subtitle'][0] !!}</span>
                    <span class="mg-t-2  ft-ter cl-ink-dk ter-bd-1 bg-safety-yellow    br-rd-4 mg-r-2 pd-l-4 pd-r-4 pd-t-2 pd-b-2"> {!! $data['subtitle'][1] !!}</span>
                </div>
                @else
                    @if(is_array($data['subtitle']))
                <span class="mg-t-2  mg-b-6 ft-ter tx-lt ter-reg-1 cl-lt"> {!! implode(", ", $data['subtitle'])!!}</span>
                    @else
                <span class="mg-t-2  ft-ter mg-b-6  ter-reg-1 cl-cerulean-blue-dk"> {!!$data['subtitle']!!}</span>
                    @endif
                @endif
                <span class="cl-ink-dk ter-bd-2 dp-fx ft-ter fx-al-ct {{is_array($data['subtitle']) ? 'ter-reg-1' : 'ter-bd-2'}}"> {!!$data['timestamp'] !!}</span>
            </div>
        </div>
    </div>
    @if(isset($data['description']))
    <div class="ft-ter sec-reg-1 cl-lt mg-b-10 mg-l-10 mg-r-10 mg-t-10" >
        <hr class=" bg-silver-lt opacity-50 mg-b-4 ht-px-1 ">
        @if (!empty($data['description'][0]))
        <div class="dp-fx fx-al-ct mg-l-2 fz-12  {{empty($data['description'][1]) ? 'mg-b-md-16' : ''}}">
            <div class="nb-icon-video-play-square fz-12 mg-r-10"> </div>
            <div class="tx-lt sec-bd-2  ft-ter  cl-cerulean-blue-dk" >{{$data['description'][0]}}</div>
        </div>
        @endif
        @if(!empty($data['description'][1]))
            <div  class=" mg-l-2 {{empty($data['description'][0]) ? 'mg-t-md-20' : ''}}">
                <span class="nb-icon-starcast fz-12  mg-r-6"> </span>
                @foreach ($data['description'] as $cast)
                    @if (!$loop->first)
                        <span class=" ft-ter "> {{$cast}}</span>
                        @if (!$loop->last)
                        <span class="mg-l-4 mg-r-4 cl-acadia br-rd-pr-50 wd-ht-px-4">&bull;</span>
                        @endif
                    @endif
                @endforeach
            </div>
        @endif
    </div>
    @endif
</div>
