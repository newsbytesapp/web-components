<div class="ft-pr {{isset($classList) ? $classList : ''}}">
    <a href="{!! $data['url'] !!}" class="dp-fx min-wd-px-50 pd-t-10 pd-b-10">
        @if($data['icon'] != '')
        @switch($type)
        @case('amp')
        <amp-img src="{{$data['icon']}}" alt="{{$data['title']}}" class="br-rd-pr-50" layout="fixed" width="36" height="36"></amp-img>
        @break
        @default
        <img src="{{$data['icon']}}" alt="{{$data['title']}}" title="{{$data['title']}}"  width="36" height="36" class="br-rd-pr-50"/>
        @break
        @endswitch
        @endif
        <div class="{{$data['icon'] != '' ? 'pd-l-10' : ''}}">
            <p class="md-reg-3 reg-3 cl-lt">{!! $data['subtitle'] !!}</p>
            <p class="md-bd-6 bd-6 dp-ib">{!! $data['title'] !!}</p>
        </div>
    </a>
    @if(isset($data['profiles']))
    <div class="{{$data['icon'] != '' ? 'mg-l-36' : ''}} dp-fx mg-t-12">
        @foreach($data['profiles'] as $item)
            <a class="wd-ht-px-50 mg-l-12" target="_blank" href="{!! $item['url'] !!}" rel="nofollow noreferrer" aria-label="Link to {{$item['key']}} profile">
                <span class="hidden">{{ucfirst($item['key'])}}</span>
                <div class="nb-icon-{{$item['key']}} fz-14" ></div>
            </a>
        @endforeach
    </div>
    @endif
</div>