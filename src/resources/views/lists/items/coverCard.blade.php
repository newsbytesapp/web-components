<?php
    $showConnector = array_get($attributes, 'showConnector', false);
    $fullHeightInTouch = array_get($attributes, 'fullHeightInTouch', false);
    $linkedTitle = array_get($attributes, 'linkedTitle', false);
    $creatorClassList = 'dp-fx mg-t-20 neg-mg-lr-px-10-ac';
?>
<div class="event-list-card pd-l-28 pd-r-20 pd-t-30 pd-l-md-48 pd-r-md-30 pd-t-md-40 gd-tint-@nbColor($data['tag'])-lt-@nbColor($data['tag'])0-@nbColor($data['tag'])100 {{$fullHeightInTouch ? 'cover-card' : ''}} event-card {{isset($classList) ? $classList : ''}} ft-pr">

    <div class="dp-fx fx-al-ct {{$showConnector ? 'neg-mg-l-px-18 neg-mg-l-px-28-md' : ''}}">
        <span class="wd-ht-px-8 br-rd-pr-50 bg-@nbColor($data['tag'])-dk evt-connector z-index-1" id="{{$data['htmlId']}}"></span>
        <span class="cl-lt cl-@nbColor($data['tag'])-dk md-bd-6 bd-6 {{$showConnector ? 'mg-l-10 mg-l-md-20' : ''}}">{!! $data['timestamp'] !!}</span>
    </div>

    <span class="pos-abs br-l-2p-@nbColor($data['tag']) neg-mg-l-px-14 neg-mg-l-px-24-md neg-mg-t-px-10 vt-line"></span>

    <h1 class="md-bd-1 bd-1 mg-t-6">@if($linkedTitle)<a href="{!! $type == 'amp' ? (isset($data['ampurl']) ? $data['ampurl'] : $data['url']) : $data['url'] !!}">@endif{!! $data['title'] !!}@if($linkedTitle)</a>@endif</h1>
    
    <div class="fx-row fx-al-ct mg-t-10">
        <p class="md-reg-3 reg-3 cl-lt">{!! $data['creators'][0]['subtitle'] !!}</p>
        <a class="mg-l-4 tx-decoration-none md-bd-6 bd-6" href="{!! $data['creators'][0]['url'] !!}">{!! $data['creators'][0]['title'] !!}</a>
        <div class="bg-lt bg-@nbColor($data['tag'])-dk mg-l-4 mg-r-4 br-rd-pr-50 wd-ht-px-3 fx-basis-3px"></div>
        <h2 class="cl-lt cl-@nbColor($data['tag'])-dk md-bd-6 bd-6">
            @if(empty(array_get($data, 'tagUrl', '')))
            <span>{!! $data['tag'] !!}</span>
            @elseif(($type == 'amp') && (isset($data['tagAmpUrl'])))
            <a href="{!! $data['tagAmpUrl'] !!}">{!! $data['tag'] !!}</a>
            @else
            <a href="{!! $data['tagUrl'] !!}">{!! $data['tag'] !!}</a>
            @endif
        </h2>
    </div>

    @if($data['img'] != '')
    <div class="pos-rel asp-ratio r-16-9 mg-t-20">
    @switch($type)
    @case('amp')
        <amp-img src="{{$data['img']}}" alt="{{$data['title']}}" class="br-rd-10 cover-media" layout="responsive" width="1080" height="610"></amp-img>
    @break
    @default
        <img src="{{$data['img']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="br-rd-10 dp-bl wd-full cover-media"/>
    @break
    @endswitch
    @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
    @include('web-components::entities.imageCredits', ['data' => $data['imagecredit'], 'type' => $type, 'classList' => ''])
    @endif
    </div>
    @endif

    @include('web-components::entities.videoTileWithImg', ['data' => $data['video'], 'type' => $type, 'classList' => 'cover-media'])
    
</div>
