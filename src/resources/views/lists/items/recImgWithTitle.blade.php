<a href="{!! $item['url'] !!}" class="col-12 pd-l-md-10 pd-r-md-10 mg-t-20 {{isset($classList) ? $classList : ''}}" {{$linkTargetAttr}}>
    <div class="col-12">
        @switch($type)
        @case('amp')
        <amp-img src="{{$item['img']}}" srcset="{{$item['img']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" width="1080" height="610" layout="responsive"></amp-img>
        @break
        @default
        <div class="asp-ratio r-16-9">
            <img data-src="{{$item['img']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-full dp-bl"/>
        </div>
        @break
        @endswitch
    </div>
    <div class="mg-t-10 bd-3 md-bd-3 cl-ink-dk">{!! $item['title'] !!}</div>
</a>