<div class="dp-fx  pd-r-10 pd-l-10  pd-r-md-0 pd-l-md-0 fx-js-bw pd-t-14 pd-b-14 br-b-1p-dotted-weathered-stone  ft-ter ter-reg-2">
    <span class=" pd-r-16 cl-dove-grey  fx-basis-48pr  pd-l-4 pd-r-md-16 pd-l-md-4 tx-lt">{!!$data['description']!!}</span>
    <span class=" pd-r-2  pd-l-4 pd-r-md-4 pd-l-md-4 tx-rt">{!!$data['subtitle']!!}</span>
</div>