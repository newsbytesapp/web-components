
@foreach ($data as $item)
<a href="{!! $item['url'] !!}" class="col-12 dp-fx fx-al-st fx-wrap pd-l-md-10 pd-r-md-10 mg-t-20"  {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!}>
    <div class="col-12 col-md-4">
        @switch($type)
        @case('amp')
        <amp-img src="{{$item['thumb']}}" srcset="{{$item['img']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" width="1080" height="610" layout="responsive"></amp-img>
        @break
        @default
        <div class="asp-ratio r-16-9">
            <img data-src="{{$item['thumb']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-full dp-bl"/>
        </div>
        @endswitch
    </div>
    <div class="tx-container col-12 col-md-8 pd-l-md-20 ft-pr">
        <p class="bd-6 md-bd-6 cl-lt mg-t-14 mg-t-md-0">{!! $item['timestamp'] !!}</p>
        <div class="bd-3 md-bd-3 cl-ink-dk mg-t-4">{!! $item['title'] !!}</div>
        <p class="reg-2 md-reg-2 cl-lt mg-t-4">{!! $item['description'] !!}</p>
    </div>
</a>
@endforeach