@if(sizeof(array_get($data, 'list.data', [])) > 0)
    <div class="mg-t-10 mg-t-md-20 pd-l-md-10 pd-r-md-10 col-12 col-md-6 col-lg-4">
        <div class="br-1p-pale-lavender br-rd-md-10 wd-full pd-t-20 pd-b-30 pd-l-md-10 pd-r-md-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">
            <div class="fx-row fx-al-bs fx-js-bw pd-l-md-10 pd-r-md-10">
                <a href="{!! ($type == 'amp') ? (empty(array_get($data, 'ampurl', '')) ? $data['url'] : $data['ampurl']) : $data['url'] !!}" class="cl-ink-dk ft-ter md-ter-bd-6 ter-bd-6">{!! $data['title'] !!}</a>
                @if(sizeof(array_get($data, 'list.link', [])) > 0)
                    @include('web-components::links.link', ['link' => $data['list']['link'], 'classList' => '', 'type' => $type])
                @endif
            </div>
            <a href="{!! ($type == 'amp') ? (empty(array_get($data['list']['data'][0], 'ampurl', '')) ? $data['list']['data'][0]['url'] : $data['list']['data'][0]['ampurl']) : $data['list']['data'][0]['url'] !!}" class="mg-t-10 dp-bl pd-l-md-10 pd-r-md-10" {!! empty(array_get($data, 'list.linkTargetAttr', '')) ? '' : 'target='.$data['list']['linkTargetAttr'] !!}>
                <div>
                    @switch($type)
                    @case('amp')
                    <amp-img src="{{$data['list']['data'][0]['thumb']}}" alt="{{$data['list']['data'][0]['title']}}" width="1080" height="610" layout="responsive"></amp-img>
                    @break
                    @default
                    <div class="asp-ratio r-16-9">
                        <img data-src="{{$data['list']['data'][0]['thumb']}}" alt="{{$data['list']['data'][0]['title']}}" title="{{$data['list']['data'][0]['title']}}" class="lazy wd-full dp-bl"/>
                    </div>
                    @endswitch
                    <div class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt mg-t-10">{!! $data['list']['data'][0]['timestamp'] !!}</div>
                    <div class="ft-pr md-bd-6 bd-6 cl-ink-dk mg-t-4">{!! $data['list']['data'][0]['title'] !!}</div>
                </div>
            </a>
            <?php
                $data['list']['data'] = array_slice($data['list']['data'], 1);
            ?>
            @foreach($data['list']['data'] as $item)
            <a href="{!! $type == 'amp' ? (isset($item['ampurl']) ? $item['ampurl'] : $item['url']) : $item['url'] !!}" class="dp-bl pd-l-md-10 pd-r-md-10 mg-t-10 ft-pr md-bd-3 bd-3" title="{{$item['title']}}">{!! $item['title'] !!}</a>
            @endforeach
            @if(isset($data['sectionLinks']) && !empty($data['sectionLinks']))
                @foreach($data['sectionLinks'] as $section)
                    @if(!empty(array_get($section, 'list.data', [])))
                    <div class="br-t-1p-white-smoke mg-t-20">
                        <div class="pd-l-md-10 pd-r-md-10">
                            <div class="dp-fx fx-js-bw fx-al-bs mg-t-20">
                                <div class="cl-ink-dk ft-ter md-ter-bd-6 ter-bd-6 col-md-8 col-6">{!! $section['title'] !!}</div>
                                @if(!empty($section['link']))
                                    @include('web-components::links.link', ['link' => $section['link'], 'classList' => 'col-md-4 col-6 dp-fx fx-js-end', 'type' => $type])
                                @endif
                            </div>
                            @foreach($section['list']['data'] as $item)
                            <a href="{!! $item['url'] !!}" class="dp-bl mg-t-10 ft-pr md-bd-3 bd-3" title="{{$item['title']}}">{!! $item['title'] !!}</a>
                            @endforeach
                        </div>
                    </div>
                    @endif
                @endforeach
            @endif
        </div>
    </div>
@endif