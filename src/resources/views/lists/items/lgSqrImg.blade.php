@foreach ($data as $item)
<a href="{!! $item['url'] !!}" class="dp-fx wd-full list-item-lgsqrImg pd-l-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac br-t-1p-white-smoke pd-t-10 pd-b-10 fx-al-ct" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!}>
    <div class="wd-ht-px-70 pos-rel">
        @switch($type)
        @case('amp')
        <amp-img src="{{$item['icon']}}" srcset="{{$item['img']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" width="70" height="70" layout="fixed"></amp-img>
        @break
        @default
        <img data-src="{{$item['icon']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-full ht-full"/>
        @break
        @endswitch
        <div class="ht-px-40 pos-abs bt-init lt-init wd-full gd-tint-white-white0-white100 {{$tint ? '' : 'hidden'}}"></div>
    </div>
    <div class="tx-container pd-l-8 ft-pr">
        <div class="md-bd-5 bd-5">{!! $item['title'] !!}</div>
        <p class="md-reg-3 reg-3 cl-lt">{!! $item['auxiliaryText'] !!}</p>
    </div>
</a>
@endforeach