@switch($data['conf']['cardtype'])
@case('fact')
    <div class="event-list-card pd-l-28 pd-r-20 pd-t-30 pd-l-md-48 pd-r-md-30 pd-t-md-30 br-rd-tl-tr-10px event-card solid-tint-@nbColor($data['tag'] . 'tint') {{isset($classList) ? $classList : ''}} mg-t-20 ft-pr">
        @if($data['subtitle'] != '')
        @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
        <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
        @endif
        <div class="dp-fx fx-al-ct neg-mg-l-px-18 neg-mg-l-px-28-md">
            <span class="wd-ht-px-8 br-rd-pr-50 bg-@nbColor($data['tag']) evt-connector z-index-1" data-hooked="{{$data['htmlId']}}"></span>
            <h3 class="cl-lt cl-@nbColor($data['tag']) md-bd-6 bd-6 mg-l-10 mg-l-md-20">{!! $data['subtitle'] !!}</h3>
        </div>
        @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
        </div>
        @endif
        @endif
        @if($data['title'] != '')
            <h2 class="md-bd-4 bd-4 mg-t-10">{!! $data['title'] !!}</h2>
        @endif
    </div>
    <div class="event-list-card pd-l-28 pd-r-20 pd-b-30 pd-l-md-48 pd-r-md-30 pd-b-md-30 br-rd-bl-br-10px embedded-links event-card solid-tint-@nbColor($data['tag'] . 'tint') ft-pr">
        @foreach($data['html'] as $point)
            <p class="{{$loop->first ? 'pd-t-20' : 'pd-t-10'}} ft-sec sec-md-reg-1 sec-reg-1">{!! $point !!}</p>
        @endforeach
    </div>
@break
@default
    @if(($data['subtitle'] != '') || ($data['title'] != '') || ($data['img'] != '') || ($data['video']['src'] != ''))
    <div class="event-list-card pd-l-28 pd-r-20 pd-t-30 pd-b-30 pd-l-md-48 pd-r-md-30 pd-t-md-30 pd-b-md-20 br-rd-tl-tr-md-10px event-card gd-tint-@nbColor($data['tag'])-lt-@nbColor($data['tag'])0-@nbColor($data['tag'])100 solid-tint-@nbColor($data['tag']) {{isset($classList) ? $classList : ''}} mg-t-20 ft-pr">
        @if($data['subtitle'] != '')
        @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
        <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
        @endif
        <div class="dp-fx fx-al-ct neg-mg-l-px-18 neg-mg-l-px-28-md">
            <span class="wd-ht-px-8 br-rd-pr-50 bg-@nbColor($data['tag'])-dk evt-connector z-index-1" data-hooked="{{$data['htmlId']}}"></span>
            <h3 class="cl-lt cl-@nbColor($data['tag'])-dk md-bd-6 bd-6 mg-l-10 mg-l-md-20">{!! $data['subtitle'] !!}</h3>
        </div>
        @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
        </div>
        @endif
        @endif

        @if($data['title'] != '')
        <h2 class="md-bd-4 bd-4 mg-t-10">{!! $data['title'] !!}</h2>
        @endif

        @if($data['img'] != '')
        <div class="pos-rel">
        @switch($type)
        @case('amp')
        <amp-img src="{{$data['img']}}" srcset="{{$data['img']}} 960w, {{$data['ximg']}} 1280w" alt="{{$data['title']}}" class="br-rd-10 mg-t-20" layout="responsive" width="1080" height="610"></amp-img>
        @break
        @default
        <div class="asp-ratio r-16-9 mg-t-20">
            <img data-src="{{$data['img']}}" alt="{{$data['title']}}" title="{{$data['title']}}" class="lazy dp-bl wd-full br-rd-10"/>
        </div>
        @break
        @endswitch
        @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
        @include('web-components::entities.imageCredits', ['data' => $data['imagecredit'], 'type' => $type, 'classList' => ''])
        @endif
        </div>
        @endif

        @include('web-components::entities.videoTileWithImg', ['data' => $data['video'], 'type' => $type, 'classList' => ''])
        
    </div>
    @endif

    @if(sizeof($data['html']) > 0)
    @if($loop->first)
    <div class="event-list-card pd-l-28 pd-r-20 pd-t-30 pd-b-30 pd-l-md-48 pd-r-md-30 pd-t-md-20 pd-b-md-30 embedded-links event-card gd-tint-rev-@nbColor($data['tag'])-lt-@nbColor($data['tag'])0-@nbColor($data['tag'])100 solid-tint-@nbColor($data['tag']) {{isset($classList) ? $classList : ''}} ft-sec">
        @foreach($data['html'] as $point)
            <p class="{{$loop->first ? '' : 'mg-t-10'}} sec-md-reg-1 sec-reg-1">{!! $point !!}</p>
        @endforeach
    </div>
    @else
    <div class="event-list-card pd-l-28 pd-r-20 pd-t-30 pd-b-30 pd-l-md-48 pd-r-md-30 pd-t-md-20 pd-b-md-30 embedded-links event-card gd-tint-rev-@nbColor($data['tag'])-lt-@nbColor($data['tag'])0-@nbColor($data['tag'])100 solid-tint-@nbColor($data['tag']) {{isset($classList) ? $classList : ''}} br-rd-bl-br-md-10px ft-sec">
        @foreach($data['html'] as $point)
        <p class="{{$loop->first ? '' : 'mg-t-10'}} sec-md-reg-1 sec-reg-1">{!! $point !!}</p>
        @endforeach
    </div>
    @endif
    @endif

    @if($data['conf']['cardtype'] == 'embed' && isset($data['conf']['type']))
    <div class="event-list-card pd-l-28 pd-r-20 pd-t-30 pd-b-30 pd-l-md-48 pd-r-md-30 pd-t-md-20 pd-b-md-30 br-rd-bl-br-md-10px embedded-links event-card gd-tint-rev-@nbColor($data['tag'])-lt-@nbColor($data['tag'])0-@nbColor($data['tag'])100 solid-tint-@nbColor($data['tag']) {{isset($classList) ? $classList : ''}}">
        @include('web-components::embeddedPosts.' . $data['conf']['type'], ['data' => $data['conf'], 'type' => $type, 'classList' => ''])
    </div>
    @endif
@break
@endswitch
