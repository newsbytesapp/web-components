<div class="pd-l-16 pd-r-16 {{isset($classList) ? $classList : ''}}">
    <a href="{!! $data['url'] !!}" class="dp-bl wd-full pos-rel">
        <div class="asp-ratio r-16-9">
            @if(!empty($data['video']['id']))
            <img alt="{!! $data['title'] !!}" class="dp-bl wd-full br-rd-10" src="{!! $data['video']['img'] !!}" loading="lazy"/>
            @else
            <img alt="{!! $data['title'] !!}" class="dp-bl wd-full br-rd-10" src="{!! $data['img'] !!}" loading="lazy"/>
            @endif
        </div>
        <div class="pos-abs tp-init ht-full wd-full">
            <div class="ht-full br-rd-10" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), #000);"></div>
        </div>
        <div class="pos-abs bt-init cl-primary pd-b-20 pd-l-12 pd-r-16 wd-full">
            @if(!empty(array_get($data, 'tag', '')))
            <div class="dp-fx fx-al-ct">
                <span class="ft-ter ter-bd-1 md-ter-bd-1">{!! $data['tag'] !!}</span>
                <div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-primary opacity-50"></div>
                <span class="ft-ter ter-reg-1 md-ter-reg-1">{!! $data['timestamp'] !!}</span>
            </div>
            @else
            <div class="dp-fx fx-al-ct">
                <span class="ft-ter ter-bd-1 md-ter-bd-1">{!! $data['timestamp'] !!}</span>
            </div>
            @endif
            <h1 class="ft-pr md-bd-6 bd-6 pd-t-6">{!! $data['title'] !!}</h1>
        </div>
    </a>
</div>