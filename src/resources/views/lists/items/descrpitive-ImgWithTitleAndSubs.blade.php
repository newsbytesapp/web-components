<?php
    $tag = array_get($data, 'tag', 'default');
?>
<div class="pd-t-20 pd-b-20 pd-l-20 pd-r-20 dp-fx fx-js-bw  bg-@nbColor($tag)-lt {{isset($classList) ? $classList : ''}}">
    <div class="dp-fx fx-al-stretch fx-dr-col fx-js-ct">
        @if(isset($data['description']) && !empty($data['description']))
            <div class="mg-b-20">
                <span class="mg-t-2  ft-ter   mg-r-2 ter-reg-1">{!!$data['description'][0]!!}</span>
                <span class="mg-t-2  ft-ter cl-ink-dk ter-bd-1 bg-safety-yellow br-rd-4 mg-r-2 pd-l-4 pd-r-4 pd-t-2 pd-b-2"> {!!$data['description'][2]!!}</span>
            </div>
        @endif
        @if(isset($data['title']) && !empty($data['title']))
            <div class="cl-ink-dk cl-default-dk ft-ter ter-bd-6">{!!$data['title']!!}</div>
        @endif
        @if(isset($data['subtitle']) && !empty($data['subtitle']))
        <div class="">
        <span class="ft-ter cl-dove-grey ter-reg-1">{!!$data['subtitle']!!}</span>
        </div>    
        @endif
    </div>
    <div class="mg-r-10 wd-px-100 ">
        @if(isset($data['img']))
        <img alt="{{$data['title']}}"  class="lazy wd-full obj-fit-cover asp-r-2-3" data-src="{{$data['img']}}">
        @endif                
    </div>
</div>