@foreach ($data as $item)
<div class="col-12 col-md-6 pd-l-md-10 pd-r-md-10 mg-t-10 mg-t-md-20 list-item-lgsqrImgBackground">
    <a href="{!! $item['url'] !!}" class="dp-fx fx-al-ct wd-full pd-l-md-20 pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac bg-primary " {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!}>
        <div class="wd-ht-px-70">
            @switch($type)
            @case('amp')
            <amp-img src="{{$item['icon']}}" srcset="{{$item['img']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" width="70" height="70" layout="fixed"></amp-img>
            @break
            @default
            <img data-src="{{$item['icon']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-full ht-full"/>
            @break
            @endswitch
        </div>
        <div class="tx-container pd-l-20 ft-pr">
            <div class="md-bd-4 bd-4">{!! $item['title'] !!}</div>
            <p class="md-reg-3 reg-3 cl-lt">{!! $item['auxiliaryText'] !!}</p>
        </div>
    </a>
</div>
@endforeach