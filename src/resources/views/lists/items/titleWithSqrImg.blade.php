<div class="mg-t-20 pd-l-16 pd-r-16 {{isset($classList) ? $classList : ''}}">
    <a href="{!! $data['url'] !!}" class="dp-fx wd-full">
        <div class="pd-r-10">
            <div class="ft-pr md-bd-3 bd-3">{!! $data['title'] !!}</div>
            @if(!empty(array_get($data, 'tag', '')))
            <div class="dp-fx fx-al-ct pd-t-10">
                <span class="cl-venetian-red ft-ter md-ter-reg-1 ter-reg-1 dp-fx fx-al-ct">{!! $data['tag'] !!}</span>
                <span class="wd-ht-px-4 bg-acadia br-rd-pr-50 mg-l-8 mg-r-8 opacity-50"></span>
                <span class="ft-ter md-ter-reg-1 ter-reg-1 cl-lt">{!! $data['timestamp'] !!}</span>
            </div>
            @else
            <div class="dp-fx fx-al-ct pd-t-10">
                <span class="ft-ter md-ter-reg-1 ter-reg-1 cl-lt">{!! $data['timestamp'] !!}</span>
            </div>
            @endif
        </div>
        <div class="pd-l-10">
            <div class="wd-ht-px-80">
                @if(!empty($data['video']['id']))
                <figure class="lazy fig fig--bg-img-ct-cov-props ht-full br-rd-4" data-src="{!! $data['video']['img'] !!}" alt="{!! $data['title'] !!}">
                    <img src="{!! $data['video']['img'] !!}" alt="{!! $data['title'] !!}" class="inset-img-hidden hidden" loading="lazy"/>
                </figure>
                @else
                <figure class="lazy fig fig--bg-img-ct-cov-props ht-full br-rd-4" data-src="{!! $data['img'] !!}" alt="{!! $data['title'] !!}">
                    <img src="{!! $data['img'] !!}" alt="{!! $data['title'] !!}" class="inset-img-hidden hidden" loading="lazy"/>
                </figure>
                @endif
            </div>
        </div>
    </a>
</div>