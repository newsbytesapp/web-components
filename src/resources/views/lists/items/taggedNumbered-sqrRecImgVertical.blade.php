
@foreach ($data as $item)
<?php
    if(isset($item['tagColor']) && $item['tagColor'] != ''){
        $tagColor = $item['tagColor'];
    }elseif(isset($item['tag']) && $item['tag'] != ''){
        $tagColor = $item['tag'];
    }else{
        $tagColor = 'default';
    }
    if(isset($item['tag']) && $item['tag'] != ''){
        $tag = $item['tag'];
    }else{
        $tag = '';
    }
    $targetAttr = ($linkTargetAttr == '_blank') ? 'target="_blank"' : '';
?>
<div class="col-12 dp-fx pd-l-md-10 pd-r-md-10 mg-t-20 list-item-sqrRecImgVertical">
    <a class="col-4 col-lg-1 fig-container--wd-ht-lg-60" title="{{$item['title']}}" href="{!! $item['url'] !!}" {!! $targetAttr !!}>
        @switch($type)
        @case('amp')
        <amp-img src="{{$item['thumb']}}" srcset="{{!empty($item['img']) ? $item['img'] : $item['thumb']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" width="1080" height="610" layout="responsive"></amp-img>
        @break
        @default
        <figure class="lazy fig fig--bg-img-ct-cov-props ht-full-dac ht-full-lg-ac asp-ratio-rect-ac asp-ratio-rect-lg-dac" data-src="{{$item['thumb']}}" title="{{$item['title']}}">
            <img data-src="{{$item['thumb']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy hidden inset-img-hidden"/>
        </figure>
        @break
        @endswitch
    </a>
    <div class="col-8 col-lg-11 tx-container pd-l-10">
        <a class="ft-pr md-bd-3 bd-3 dp-bl clickable-item" title="{{$item['title']}}" href="{!! $item['url'] !!}" {!! $targetAttr !!}>{!! $item['title'] !!}</a>
        @if(!empty(array_get($item, 'tagUrl', '')))
        <a class="cl-lt cl-@nbColor($tagColor)-dk mg-t-2 ft-ter md-ter-bd-2 ter-bd-2 dp-ib clickable-item" href="{!! $item['tagUrl'] !!}">{!! $tag !!}</a>
        @else
        <div class="cl-lt cl-@nbColor($tagColor)-dk mg-t-2 ft-ter md-ter-bd-2 ter-bd-2">{!! $tag !!}</div>
        @endif
    </div>
    <div class="cl-ink-dk ft-pr md-bd-9 bd-9 pd-l-10 tx-rt">{{($loop->index + 1)}}</div>
</div>
@endforeach