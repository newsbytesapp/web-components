<div class="dp-fx fx-al-ct pd-t-10 pd-b-10 br-t-1p-pale-lavender br-b-1p-pale-lavender mg-t-20">
    <div>
        <img src={!!$data['img']!!} alt={!!$data['title']!!} class="wd-ht-px-50 br-rd-pr-50">
    </div>
    <div class="dp-fx fx-dr-col fx-al-bs pd-l-20 pd-t-6 pd-b-6">
        <span class="cl-lt ft-ter md-ter-bd-3 ter-bd-3 tx-ct tx-md-lt mg-b-4">{!!$data['title']!!}</span>
        <span class="ft-ter md-ter-reg-2 ter-reg-2 tx-ct tx-md-lt cl-link">{!!$data['data']!!}</span>
    </div>
</div>