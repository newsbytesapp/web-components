<div class="dp-fx mg-r-60 mg-b-44 mg-t-20 fx-grow-1">
    <div class="pd-r-10">
        <span class="nb-icon-{{$data['icon']}} fz-28">  </span>
    </div>
    <div class="dp-fx fx-dr-col">
        <span class="ft-ter ter-bd-2 ">{!!$data['description']!!}</span>
        <span class="ft-ter ter-reg-2 ">{!!$data['subtitle']!!}</span>
    </div>
</div>