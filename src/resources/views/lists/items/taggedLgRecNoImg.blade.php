
@foreach ($data as $item)
<?php
    if(isset($item['tagColor']) && $item['tagColor'] != ''){
        $tagColor = $item['tagColor'];
    }elseif(isset($item['tag']) && $item['tag'] != ''){
        $tagColor = $item['tag'];
    }else{
        $tagColor = 'default';
    }
    if(isset($item['tag']) && $item['tag'] != ''){
        $tag = $item['tag'];
    }else{
        $tag = '';
    }
    $targetAttr = ($linkTargetAttr == '_blank') ? 'target="_blank"' : '';
?>
<div class="col-12 dp-fx fx-al-ct fx-wrap pd-l-md-10 pd-r-md-10 mg-t-20 list-item-lgRecImg clickable-target cs-ptr" data-url="{!! $item['url'] !!}">
    <a class="col-12 col-md-6" href="{!! $item['url'] !!}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!}>
        @switch($type)
        @case('amp')
        <amp-img src="{{$item['thumb']}}" srcset="{{$item['img']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" width="1080" height="610" layout="responsive"></amp-img>
        @break
        @default
        @if(!empty(array_get($item, 'video.src', '')))
        <div class="asp-ratio r-16-9">
            <img data-src="{{$item['video']['img']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-full dp-bl"/>
        </div>
        @else
        <div class="asp-ratio r-16-9">
            <img data-src="{{$item['img']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-full dp-bl"/>
        </div>
        @endif
        @endswitch
    </a>
    <div class="tx-container col-12 col-md-6 pd-l-md-20 mg-t-20">
        @if(!empty(array_get($item, 'tagUrl', '')))
        <div class="dp-fx fx-al-ct">
            <span class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt  mg-t-md-0">{!! $item['timestamp'] !!}</span>
            <div class="wd-ht-px-4 bg-acadia br-rd-pr-50 mg-l-4 mg-r-4"></div>
            <a class="cl-lt cl-@nbColor($tagColor)-dk ft-ter md-ter-bd-2 ter-bd-2 dp-ib clickable-item" href="{!!$item['tagUrl'] !!}" title="{!! $tag !!}">{!! $tag !!}</a>
        </div>
        @else
        <div class="dp-fx fx-al-ct">
            <span class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt  mg-t-md-0">{!! $item['timestamp']!!}</span>
            <div class="wd-ht-px-4 bg-acadia br-rd-pr-50 mg-l-4 mg-r-4"></div>
            <div class="cl-lt cl-@nbColor($tagColor)-dk ft-ter md-ter-bd-2 ter-bd-2">{!!$tag !!}</div>
        </div>
        @endif
        <a class="ft-pr md-bd-6 bd-6 cl-ink-dk mg-t-4 dp-bl clickable-item" title="{!! $item['title'] !!}" href="{!! $item['url'] !!}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!}>{!! $item['title'] !!}</a>
        <p class="ft-sec md-sec-reg-3 sec-reg-3 cl-lt mg-t-2">{!! $item['description'] !!}</p>
    </div>
</div>
@endforeach