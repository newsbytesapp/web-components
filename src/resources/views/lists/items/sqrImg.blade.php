@foreach ($data as $item)
<?php
    $imgSrc = "";
    if((isset($item['icon'])) && ($item['icon'] != '')){
        $imgSrc = $item['icon'];
    }elseif((isset($item['thumb'])) && ($item['thumb'] != '')){
        $imgSrc = $item['thumb'];
    }else{
        $imgSrc = $item['img'];
    }

    if(isset($largerVersion) && $largerVersion){
        $getLargeImage = true;
    }else{
        $getLargeImage = false;
    }
?>
@if($getLargeImage)
<a href="{!! $item['url'] !!}" class="col-12 col-md-6 dp-fx list-item-sqrImg pd-l-md-10 pd-r-10 mg-t-10 mg-t-md-20 fx-al-ct" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!}>
    <div class="wd-ht-px-70">
        @switch($type)
        @case('amp')
        <amp-img src="{{$imgSrc}}" alt="{{$item['title']}}" width="70" height="70" layout="fixed"></amp-img>
        @break
        @default
        <img data-src="{{$imgSrc}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-full ht-full"/>
        @break
        @endswitch
    </div>
    <div class="tx-container pd-l-20 ft-pr">
        <div class="md-bd-4 bd-4">{!! $item['title'] !!}</div>
        @if(isset($item['auxiliaryText']))
        <p class="md-reg-3 reg-3 cl-lt">{!! $item['auxiliaryText'] !!}</p>
        @endif
    </div>
</a>
@else
<a href="{!! $item['url'] !!}" class="col-6 col-md-4 col-lg-6 dp-fx list-item-sqrImg pd-l-md-10 pd-r-10 mg-t-20 {{ isset($item['auxiliaryText']) ? '' : 'fx-al-ct'}}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!}>
    @switch($type)
    @case('amp')
    <amp-img src="{{$imgSrc}}" alt="{{$item['title']}}" width="40" height="40" layout="fixed" class="fx-basis-40px"></amp-img>
    @break
    @default
    <img data-src="{{$imgSrc}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-ht-px-40 fx-basis-40px"/>
    @break
    @endswitch
    <div class="tx-container pd-l-10 ft-pr">
        <div class="md-bd-6 bd-6">{!! $item['title'] !!}</div>
        @if(isset($item['auxiliaryText']))
        <p class="md-reg-3 reg-3 cl-lt">{!! $item['auxiliaryText'] !!}</p>
        @endif
    </div>
</a>
@endif

@endforeach