
@foreach ($data as $item)
<div class="col-12 dp-fx fx-al-st fx-wrap mg-t-10 clickable-target cs-ptr pd-l-md-20 pd-r-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-t-20 pd-b-20 pd-t-md-18 pd-b-md-30 bg-body" title="{{$item['title']}}" data-url="{!! $item['url'] !!}">
    <div class="tx-container col-12 pd-l-md-20">
        @if(!empty(array_get($item, 'tag', '')) && !empty(array_get($item, 'tagUrl', '')))
        <div class="dp-fx fx-al-ct">
            <p class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt">{!! $item['timestamp'] !!}</p>
            <div class="wd-ht-px-4 bg-acadia br-rd-pr-50 mg-l-4 mg-r-4"></div>
            <a class="cl-ink-dk cl-@nbColor((isset($item['tagColor']))?$item['tagColor']:$item['tag'])-dk ft-ter md-ter-bd-2 ter-bd-2 dp-fx fx-al-ct min-ht-px-50 min-wd-px-50" href="{!! $item['tagUrl'] !!}"><span>{!! $item['tag'] !!}</span></a>
        </div>
        @else
        <div class="min-ht-px-50 min-wd-px-50 dp-fx fx-al-ct">
            <p class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt">{!! $item['timestamp'] !!}</p>
        </div>
        @endif
        <h3><a href="{!! $item['url'] !!}" class="ft-pr md-bd-5 bd-5 cl-ink-dk dp-bl">{!! $item['title'] !!}</a></h3>
        <p class="ft-sec md-sec-reg-3 sec-reg-3 cl-lt mg-t-2">{!! $item['description'] !!}</p>
    </div>
</div>
@endforeach