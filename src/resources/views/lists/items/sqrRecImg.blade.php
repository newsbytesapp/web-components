
@foreach ($data as $item)
<div class="col-12 col-md-4 dp-fx pd-l-md-10 pd-r-md-10 mg-t-20 list-item-sqrRecImg clickable-target cs-ptr" data-url="{!! $item['url'] !!}">
    <a class="col-4 col-md-1 fig-container--wd-ht-md-60" href="{!! $type == 'amp' ? (isset($item['ampurl']) ? $item['ampurl'] : $item['url']) : $item['url'] !!}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!} title="{{$item['title']}}">
        @switch($type)
        @case('amp')
        <amp-img src="{{$item['thumb']}}" srcset="{{!empty($item['img']) ? $item['img'] : $item['thumb']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" width="1080" height="610" layout="responsive"></amp-img>
        @break
        @default
        <figure class="lazy fig fig--bg-img-ct-cov-props ht-full-dac ht-full-md-ac asp-ratio-rect-ac asp-ratio-rect-md-dac" data-src="{{$item['thumb']}}" title="{{$item['title']}}">
            <img data-src="{{$item['thumb']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy hidden inset-img-hidden"/>
        </figure>
        @break
        @endswitch
    </a>
    <div class="tx-container col-8 pd-l-10">
        <a class="ft-pr md-bd-3 bd-3 dp-bl" href="{!! $type == 'amp' ? (isset($item['ampurl']) ? $item['ampurl'] : $item['url']) : $item['url'] !!}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!} title="{{$item['title']}}">{!! $item['title'] !!}</a>
    </div>
</div>
@endforeach