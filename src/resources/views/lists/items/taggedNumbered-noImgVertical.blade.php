@foreach ($data as $item)
<?php
    if(isset($item['tagColor']) && $item['tagColor'] != ''){
        $tagColor = $item['tagColor'];
    }elseif(isset($item['tag']) && $item['tag'] != ''){
        $tagColor = $item['tag'];
    }else{
        $tagColor = 'default';
    }
    if(isset($item['tag']) && $item['tag'] != ''){
        $tag = $item['tag'];
    }else{
        $tag = '';
    }
    $targetAttr = ($linkTargetAttr == '_blank') ? 'target="_blank"' : '';
    $item['tagUrl'] = isset($withTagUrl) && $withTagUrl ? array_get($item, 'tagUrl', '') : '';
?>
<div class="col-12 dp-fx fx-js-bw fx-al-ct bg-primary br-rd-10 pd-l-12 pd-t-12 pd-b-12 mg-t-16 box-shadow list-item-sqrRecImgVertical">
    <div class="col-12 pd-r-16 col-lg-11 tx-container">
        <a class="ft-pr md-bd-3 bd-3 dp-bl clickable-item" title="{{$item['title']}}" href="{!! $item['url'] !!}" {!! $targetAttr !!}>{!! $item['title'] !!}
            <div class="cl-venetian-red mg-t-2 ft-ter md-ter-reg-1 ter-reg-1">{!! $tag !!}</div>
        </a>
    </div>
    <div class="ft-pr pd-l-16 pd-r-16 pd-t-16 pd-b-16 br-rd-4 cl-venetian-red md-bd-9 bd-9 tx-rt bg-lt-red br-r-0 ht-full">{{($loop->index + 1)}}</div>
</div>
@endforeach