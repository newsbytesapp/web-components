<div class="dp-fx fx-dr-col fx-al-ct pd-t-10 pd-b-10 pd-r-10 pd-l-10 br-rd-8 fx-basis-180px" >
  <img  data-src="{!! $data['img'] !!}" alt="{{$data['description']}}" class="lazy wd-ht-px-100 br-rd-pr-50 mg-b-10 obj-fit-cover">
  <div class="">
    <p class=" ft-ter ter-reg-2 tx-ct">{!!$data['description']!!} </p>
    <p class="ft-ter ter-reg-1 cl-suva-grey tx-ct">{!!$data['subtitle']!!}</p>
  </div>
</div> 