<?php
    $tag = array_get($data, 'tag', 'default');
?>
<div class="bg-primary  mg-b-10 mg-r-4 mg-l-4 mg-t-4 col-12  col-md-6 col-lg-5 dp-fx fx-js-bw fx-dr-col fx-basis-48pr br-rd-4 {{isset($classList) ? $classList : ''}}" >
    <div class="dp-fx fx-js-bw fx-al-stretch mg-b-10 mg-l-10 mg-r-10 mg-t-10">
        <div class=" dp-fx fx-al-ct fx-js-bw">
            <div class="mg-r-10 wd-px-100" >
                @if(isset($data['img']))
                    <img data-src="{!! $data['img'] !!}" alt="{{$data['title']}}" class="lazy wd-full obj-fit-cover asp-r-2-3">                
                @endif
            </div>
            <div class="dp-fx fx-dr-col"  style="max-width: calc(100% - 110px);">
                <p class="cl-ink-dk ft-ter ter-bd-3 mg-r-2 mg-b-2 tx-lt">{!! $data['title'] !!}</p>
                @if(isset($data['description']) && count($data['description']) > 1)
                    <div class="mg-b-6">
                        <span class="mg-t-2  ft-ter   mg-r-2 ter-reg-1"> {!! $data['description'][0] !!}</span>
                        <span class="mg-t-2  ft-ter cl-ink-dk ter-bd-1 bg-safety-yellow    br-rd-4 mg-r-2 pd-l-4 pd-r-4 pd-t-2 pd-b-2"> {!! $data['description'][1] !!}</span>
                    </div> 
                @endif
                @if(isset($data['subtitle']))
                    <span class="mg-t-2  ft-ter mg-b-6  ter-reg-1 cl-dove-grey"> {!!$data['subtitle']!!}</span>
                @endif
            </div>
        </div>
    </div>
</div> 