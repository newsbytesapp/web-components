
@foreach ($data as $item)
<?php
    if(isset($item['tagColor']) && $item['tagColor'] != ''){
        $tagColor = $item['tagColor'];
    }elseif(isset($item['tag']) && $item['tag'] != ''){
        $tagColor = $item['tag'];
    }else{
        $tagColor = 'default';
    }
    if(isset($item['tag']) && $item['tag'] != ''){
        $tag = $item['tag'];
    }else{
        $tag = '';
    }
    $targetAttr = ($linkTargetAttr == '_blank') ? 'target="_blank"' : '';
?>
<div class="col-12 col-md-4 dp-fx pd-l-md-10 pd-r-md-10 mg-t-20 list-item-sqrRecImg clickable-target cs-ptr" data-url="{!! $item['url'] !!}">
    
    @switch($type)
    @case('amp')
    <a class="col-4 col-md-1 fig-container--wd-ht-md-60" href="{!! $type == 'amp' ? (isset($item['ampurl']) ? $item['ampurl'] : $item['url']) : $item['url'] !!}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!} title="{{$item['title']}}">
        <amp-img src="{{$item['thumb']}}" srcset="{{!empty($item['img']) ? $item['img'] : $item['thumb']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" width="1080" height="610" layout="responsive"></amp-img>
    </a>
    @break
    @default
    @break
    @endswitch
    
    <div class="tx-container">
        @if(!empty(array_get($item, 'tagUrl', '')))
        <div class="dp-fx fx-al-ct">
            <p class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt-grey mg-t-md-0">{!! $item['timestamp'] !!}</p>
            <div class="wd-ht-px-4 bg-acadia br-rd-pr-50 mg-l-4 mg-r-4 "></div>
            <a class="cl-lt-grey ft-ter md-ter-bd-2 ter-bd-2 dp-ib clickable-item" href="{!!$item['tagUrl'] !!}">{!! $tag !!}</a>
        </div>
        @else
        <div class="dp-fx fx-al-ct">
            <p class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt-grey mg-t-md-0">{!! $item['timestamp']!!}</p>
        </div>
        @endif
        <a class="ft-pr md-bd-3 bd-3 dp-bl mg-t-4 clickable-item" href="{!! $type == 'amp' ? (isset($item['ampurl']) ? $item['ampurl'] : $item['url']) : $item['url'] !!}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!} title="{{$item['title']}}">{!! $item['title'] !!}</a>
    </div>
</div>
@endforeach