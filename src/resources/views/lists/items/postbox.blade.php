@switch($type)
@case('amp')
@break
@default
<div class="askQuestion">
    <div class="dp-fx ft-pr pd-l-md-10 pd-r-md-10 {{isset($classList) ? $classList : ''}}">
        @if(isset($data['includeSvg']))
        <span class="svg-inclusion userIcon" data-svg="{{$data['includeSvg']}}" data-fallback-icon="{{$data['icon']}}"></span>
        @else
        <img src="{{$data['icon']}}" class="lazy wd-ht-px-36 br-rd-pr-50 userIcon" data-fallback-icon="{{$data['icon']}}" alt="Fallback Icon" title="Fallback Icon"/>
        @endif
        <div class="bg-primary pd-l-20 pd-r-20 pd-t-14 pd-b-14 mg-l-10 wd-full br-rd-10 br-1p-pale-lavender">
            <div class="fx-al-end dp-fx">
                <textarea class="mg-r-10 wd-full hide-native-scrollbar ft-pr md-reg-1 reg-1 placeholder-query-packet userQuestion" data-min-rows='1' data-max="180" rows="1" aria-label="User Question Box" placeholder="{!! $data['placeholder'] !!}"></textarea>
                <span class="mg-l-10 nb-icon-post fz-18 cl-link cs-ptr submitQuestion" data-timelineId="{{$data['params']['static']['timelineId']}}" data-api="{{$data['api']}}"></span>
            </div>
            <div class="dp-fx mg-t-10 fx-js-end">
                <div class="cl-lt reg-2 loader"></div>
            </div>
        </div>
    </div>
    <div class="flashMessage width-100 mg-t-10 mg-l-46 pd-l-20 cl-link md-bd-6 bd-6"></div>
</div>
@break
@endswitch