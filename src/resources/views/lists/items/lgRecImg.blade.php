
@foreach ($data as $item)
<div class="col-12 dp-fx fx-al-ct fx-wrap pd-l-md-10 pd-r-md-10 mg-t-20 list-item-lgRecImg clickable-target cs-ptr" data-url="{!! $item['url'] !!}">
    <a class="col-12 col-md-6" href="{!! $item['url'] !!}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!}>
        @switch($type)
        @case('amp')
        <amp-img src="{{$item['thumb']}}" srcset="{{$item['img']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" width="1080" height="610" layout="responsive"></amp-img>
        @break
        @default
        <div class="asp-ratio r-16-9">
            <img data-src="{{$item['thumb']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy wd-full dp-bl"/>
        </div>
        @endswitch
    </a>
    <div class="tx-container col-12 col-md-6 pd-l-md-20">
        <p class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt mg-t-20 mg-t-md-0">{!! $item['timestamp'] !!}</p>
        <a class="ft-pr md-bd-6 bd-6 cl-ink-dk mg-t-4 dp-bl" href="{!! $item['url'] !!}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!}>{!! $item['title'] !!}</a>
        <p class="ft-sec md-sec-reg-3 sec-reg-3 cl-lt mg-t-2">{!! $item['description'] !!}</p>
    </div>
</div>
@endforeach