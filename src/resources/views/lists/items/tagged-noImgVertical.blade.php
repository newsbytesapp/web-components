
@foreach ($data as $item)
<?php
    if(isset($item['tagColor']) && $item['tagColor'] != ''){
        $tagColor = $item['tagColor'];
    }elseif(isset($item['tag']) && $item['tag'] != ''){
        $tagColor = $item['tag'];
    }else{
        $tagColor = 'default';
    }
    if(isset($item['tag']) && $item['tag'] != ''){
        $tag = $item['tag'];
    }else{
        $tag = '';
    }
    $targetAttr = ($linkTargetAttr == '_blank') ? 'target="_blank"' : '';
?>
<div class="col-12 dp-fx pd-l-md-10 pd-r-md-10 mg-t-20 list-item-sqrRecImgVertical">
    <div class="col-12 tx-container">
        <a class="ft-pr md-bd-3 bd-3 dp-bl clickable-item" title="{{$item['title']}}" href="{!! $item['url'] !!}" {!! $targetAttr !!}>{!! $item['title'] !!}</a>
        @if(!empty(array_get($item, 'tagUrl', '')))
        <a class="cl-venetian-red mg-t-2 ft-ter md-ter-bd-2 ter-bd-2 dp-ib clickable-item" href="{!! $item['tagUrl'] !!}">{!! $tag !!}</a>
        @else
        <div class="cl-venetian-red mg-t-2 ft-ter md-ter-bd-2 ter-bd-2 dp-bl">{!! $tag !!}</div>
        @endif
    </div>
</div>
@endforeach