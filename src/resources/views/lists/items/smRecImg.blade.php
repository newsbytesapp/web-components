
@foreach ($data as $item)
<a href="{!! $item['url'] !!}" class="col-12 col-md-6 fx-row pd-l-md-10 pd-r-md-10 mg-t-20 list-item-smRecImg" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!}>
    <div class="col-6">
        @switch($type)
        @case('amp')
        <amp-img src="{{$item['thumb']}}" srcset="{{$item['img']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" class="dp-bl wd-full" width="1080" height="610" layout="responsive"></amp-img>
        @break
        @default
        <div class="asp-ratio r-16-9">
            <img data-src="{{$item['thumb']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy dp-bl wd-full"/>
        </div>
        @break
        @endswitch
    </div>
    <div class="tx-container col-6 pd-l-10 ft-pr">
        <p class="bd-6 bd-md-6 cl-lt">{!! $item['timestamp'] !!}</p>
        <div class="bd-5 md-bd-5 cl-ink-dk mg-t-4">{!! $item['title'] !!}</div>
    </div>
</a>
@endforeach