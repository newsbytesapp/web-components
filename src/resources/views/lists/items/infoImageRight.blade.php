<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
@foreach ($data as $item)
<div class="col-12 col-md-6 pd-l-md-10 pd-r-md-10 mg-t-10 mg-t-md-20 ft-pr">
    <div class="bg-primary  pd-t-10 pd-b-10 ht-full" id="iir-{{$componentId}}">
        <div class="pd-t-10 pd-l-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac md-bd-4 bd-4">{{array_get($item, 'name', '')}}</div>
        @foreach($item['info'] as $index => $row)
        @if(!empty($row[0]['icon']))
        <a class="dp-fx fx-al-ct fx-js-bw {{($index)?'br-t-1p-white-smoke':'mg-t-10 mg-t-md-20 pd-l-md-20 pd-r-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac'}}" href="{!! array_get($row[0], 'url', '') !!}" target="{{!empty($linkTargetAttr) ? $linkTargetAttr : '_self'}}">
        @endif
            @if($index == 0) <div> @endif
        @foreach($row as $key => $infoPiece)
        @php $fontSize=($key==0)?6:(($index==0)?3:4) @endphp
            <div class="md-bd-{{$fontSize}} bd-{{$fontSize}} dp-fx fx-al-ct pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac {{($index > 0)?'pd-t-14 pd-b-14 '.(($key>0)?'':'pd-l-md-20'):''}} pd-r-md-20">
                <span>{{array_get($infoPiece, 'name', '')}}</span>
                <span class="cl-lt mg-l-6 md-reg-3 reg-3">{{array_get($infoPiece, 'dim', '')}}</span>
            </div>
        @endforeach
        @if($index == 0)
        </div>
        @endif
        @if(($index == 0) && !empty($row[0]['icon']))
        <div class="wd-ht-px-100 pos-rel">
            @switch($type)
            @case('amp')
            <amp-img src="{{array_get($row[0], 'icon', '')}}" alt="{{array_get($row[0], 'name', '')}}" width="100" height="100" layout="fixed"></amp-img>
            @break
            @default
            <img data-src="{{array_get($row[0], 'icon', '')}}" alt="{{array_get($row[0], 'name', '')}}" title="{{array_get($row[0], 'name', '')}}" class="lazy wd-full ht-full"/>
            @endswitch
            <div class="ht-px-40 pos-abs bt-init lt-init wd-full gd-tint-white-white0-white100"></div>
        </div>
        @endif
        </a>
        @endforeach
        @if(isset($item['link']) && isset($item['link']['size']))
        <div class="dp-fx fx-js-ct br-t-1p-white-smoke pd-t-20 pd-b-20">
            @include('web-components::links.link', ['link' => $item['link'], 'classList' => ''])
        </div>
        @endif
    </div>
</div>
@endforeach