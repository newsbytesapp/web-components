@if(isset($data['type']) && $data['type'] == 'link')
<div class="mg-t-30 col-12">
    <span class="cl-ink-dk ft-ter md-ter-bd-2 ter-bd-2 mg-t-20 mg-t-md-0 tx-ct tx-md-lt">{!!$data['title']!!}</span>
    <div class="fx-row">
        @foreach($data['data'] as $item)
        <a href="{!!$item['url']!!}" class="cl-lt cl-cerulean-blue-dk mg-r-10 mg-t-10 dp-ib">
            <span class="ft-ter md-sec-reg-2 sec-reg-2 pd-r-10">{!!$item['title']!!}</span>
        </a>
        @endforeach
    </div>
</div>
@else
<div class="mg-t-30 {{isset($data['type']) && $data['type'] == 'multi' ? 'col-12 col-md-6' : 'col-6'}}">
    <span class="cl-ink-dk ft-ter md-ter-bd-2 ter-bd-2 mg-t-20 mg-t-md-0 tx-ct tx-md-lt">{!!$data['title']!!}</span>
    @foreach($data['data'] as $item)
    <div class="dp-fx fx-dr-col fx-al-bs mg-t-6">
        <span class="ft-ter md-sec-reg-2 sec-reg-2 tx-ct tx-md-lt">{!!$item['title']!!}</span>
        @if(isset($item['subtitle']))
        <span class="cl-lt ft-ter md-sec-reg-1 sec-reg-1 tx-ct tx-md-lt mg-b-10">{!!$item['subtitle']!!}</span>
        @endif
    </div>
    @endforeach
</div>
@endif