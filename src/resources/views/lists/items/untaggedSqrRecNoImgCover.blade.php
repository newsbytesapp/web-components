
@foreach ($data as $item)
<div class="col-12 col-md-4 dp-fx pd-l-md-10 pd-r-md-10 mg-t-20 list-item-sqrRecImg clickable-target cs-ptr" data-url="{!! $item['url'] !!}">
    
    @switch($type)
    @case('amp')
    <a class="col-4 col-md-1 fig-container--wd-ht-md-60" href="{!! $type == 'amp' ? (isset($item['ampurl']) ? $item['ampurl'] : $item['url']) : $item['url'] !!}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!} title="{{$item['title']}}">
        <amp-img src="{{$item['thumb']}}" srcset="{{!empty($item['img']) ? $item['img'] : $item['thumb']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" width="1080" height="610" layout="responsive"></amp-img>
    </a>
    @break
    @default
    @break
    @endswitch
    
    <div class="tx-container">
        <div class="dp-fx fx-al-ct">
            <span class="ft-ter md-ter-reg-2 ter-reg-2 cl-lt-grey mg-t-md-0">{!! $item['timestamp'] !!}</span>
        </div>
        <a class="ft-pr md-bd-3 bd-3 dp-bl mg-t-4 clickable-item break-word" href="{!! $type == 'amp' ? (isset($item['ampurl']) ? $item['ampurl'] : $item['url']) : $item['url'] !!}" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!} title="{{$item['title']}}">{!! $item['title'] !!}</a>
    </div>
</div>
@endforeach