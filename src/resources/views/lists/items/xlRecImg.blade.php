@foreach ($data as $item)
<a href="{!! $type == 'amp' ? (isset($item['ampurl']) ? $item['ampurl'] : $item['url']) : $item['url'] !!}" class="wd-full pos-rel dp-bl fig fig--tt-black-bt list-item-xlRecImg"  {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!} title="{{$item['title']}}">
    @switch($type)
    @case('amp')
    <amp-img src="{{$item['ximg']}}" alt="{{$item['title']}}" width="1080" height="610" layout="responsive"></amp-img>
    @break
    @default
    <figure>
        <div class="asp-ratio r-16-9">
            @if(! empty($item['video']['src']))
            <img src="{{$item['video']['img']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="wd-full ht-full cover-card-img hidden"/>
            @elseif(isset($data['imageFallback']) && $data['imageFallback'])
            <picture>
                <source
                    media="(max-width: 599px) and (min-device-pixel-ratio: 3),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 3),(max-width: 599px) and (min-resolution: 3dppx),(max-width: 599px) and (min-resolution: 288dpi),(min-width: 768px) and (min-device-pixel-ratio: 2),(min-width: 768px) and (-webkit-min-device-pixel-ratio: 2),(min-width: 768px) and (min-resolution: 3dppx),(min-width: 768px) and (min-resolution: 288dpi)"
                    srcset="{{$data['ximg']}}">
                <source
                    media="(min-width: 1080px) and (min-device-pixel-ratio: 1),(min-width: 1080px) and (-webkit-min-device-pixel-ratio: 1),(min-width: 1080px) and (min-resolution: 2dppx),(min-width: 1080px) and (min-resolution: 192dpi)"
                    srcset="{{array_get($data, 'limg', array_get($data, 'ximg', ''))}}">
                <source
                    media="(max-width: 599px) and (min-device-pixel-ratio: 2),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 2),(max-width: 599px) and (min-resolution: 2dppx),(max-width: 599px) and (min-resolution: 192dpi)"
                    srcset="{{$data['img']}}">
                <source
                    media="(max-width: 599px) and (min-device-pixel-ratio: 1),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 1),(max-width: 599px) and (min-resolution: 1dppx),(max-width: 599px) and (min-resolution: 96dpi)"
                    srcset="{{$data['img']}}">
                <img alt="{{$data['title']}}" title="{{$data['title']}}" class="dp-bl wd-full cover-card-img hidden" src="{{$data['thumb']}}">
            </picture>
            @else
            <img src="{{$item['img']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="wd-full ht-full cover-card-img hidden"/>
            @endif
        </div>
    </figure>
    @break
    @endswitch
    <{{array_get($htmlTags, 'items.xlRecImg', 'h1')}} class="pos-abs lt-init bt-init pd-b-10 pd-b-md-20 pd-l-20 pd-r-20 wd-full cl-primary bd-8 ft-pr md-bd-8 bg-transparent z-index-2">
    {!! $item['title'] !!}
    </{{array_get($htmlTags, 'items.xlRecImg', 'h1')}}>
</a>
@endforeach