
@foreach ($data as $item)
<a href="{!! $type == 'amp' ? (isset($item['ampurl']) ? $item['ampurl'] : $item['url']) : $item['url'] !!}" class="col-12 dp-fx pd-l-md-10 pd-r-md-10 mg-t-20 list-item-sqrRecImgVertical" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!} title="{{$item['title']}}">
    <div class="col-4 col-lg-1 fig-container--wd-ht-lg-60">
        @switch($type)
        @case('amp')
        <amp-img src="{{$item['thumb']}}" srcset="{{!empty($item['img']) ? $item['img'] : $item['thumb']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" width="1080" height="610" layout="responsive"></amp-img>
        @break
        @default
        <figure class="lazy fig fig--bg-img-ct-cov-props ht-full-dac ht-full-lg-ac asp-ratio-rect-ac asp-ratio-rect-lg-dac" data-src="{{$item['thumb']}}" title="{{$item['title']}}">
            <img data-src="{{$item['thumb']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy hidden inset-img-hidden"/>
        </figure>
        @break
        @endswitch
    </div>
    <div class="col-8 col-lg-10 tx-container pd-l-10">
        <div class="ft-pr md-bd-3 bd-3">{!! $item['title'] !!}</div>
    </div>   
</a>
@endforeach