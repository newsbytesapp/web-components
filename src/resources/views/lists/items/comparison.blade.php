@foreach ($data as $item)
<div class="bg-primary dp-fx fx-dr-col list-item-comparison wd-full br-t-1p-white-smoke pd-t-20 pd-b-20 fx-al-ct ft-pr reg-3 md-reg-3 {{isset($classList) ? $classList : ''}}">
    <div class="col-12 col-md-8 col-lg-6 dp-fx fx-js-ct">
        <div class="col-4 dp-fx fx-js-end">
            <div class="mg-t-38">
                <a href="{!! $item['left']['url'] !!}" class="dp-fx fx-al-st" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!}>
                    <div>
                        <div class="bd-6 md-bd-6 tx-rt">{!! $item['left']['title'] !!}</div>
                        <div class="tx-rt cl-lt">{!! $item['left']['subtitle'] !!}</div>
                        @foreach($item['left']['comparision'] as $param)
                        <div class="{{ $loop->first ? 'mg-t-4' : 'mg-t-6' }}">
                            <div class="bd-6 md-bd-6 tx-rt">{!! $param['title'] !!}</div>
                            <div class="tx-rt cl-lt">{!! $param['subtitle'] !!}</div>
                        </div>
                        @endforeach
                    </div>
                    @switch($type)
                    @case('amp')
                    <amp-img src="{{$item['left']['icon']}}" alt="{{$item['left']
                    ['title']}}" width="32" height="32" layout="fixed" class="mg-l-10"></amp-img>
                    @break
                    @default
                    <img class="lazy wd-ht-px-32 mg-l-10" data-src="{{$item['left']['icon']}}" alt="{{$item['left']['title']}}" title="{{$item['left']['title']}}"/>
                    @break
                    @endswitch
                </a>
            </div>
        </div>
        <div>
            <div class="br-rd-15 pd-l-10 pd-r-10 pd-t-4 pd-b-4 bg-lt-grey wht-sp-nowrap">{!! $item['comparisionTitle'] !!}</div>
            <div class="bd-6 md-bd-6 mg-t-18 tx-ct">@t('vs')</div>
        </div>
        <div class="col-4 dp-fx fx-js-st">
            <div class="mg-t-38">
                <a href="{!! $item['right']['url'] !!}" class="dp-fx fx-al-st" {!! $linkTargetAttr == "_blank" ? 'target="_blank"' : '' !!}>
                    @switch($type)
                    @case('amp')
                    <amp-img src="{{$item['right']['icon']}}" alt="{{$item['right']['title']}}" width="32" height="32" layout="fixed"></amp-img>
                    @break
                    @default
                    <img class="lazy wd-ht-px-32" data-src="{{$item['right']['icon']}}" alt="{{$item['right']['title']}}" title="{{$item['right']['title']}}"/>
                    @break
                    @endswitch
                    <div class="mg-l-10">
                        <div class="bd-6 md-bd-6">{!! $item['right']['title'] !!}</div>
                        <div class="cl-lt">{!! $item['right']['subtitle'] !!}</div>
                        @foreach($item['right']['comparision'] as $param)
                        <div class="{{ $loop->first ? 'mg-t-4' : 'mg-t-6' }}">
                            <div class="bd-6 md-bd-6">{!! $param['title'] !!}</div>
                            <div class="cl-lt">{!! $param['subtitle'] !!}</div>
                        </div>
                        @endforeach
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="mg-t-10 tx-ct">{!! $item['conclusion'] !!}</div>
    <div class="mg-t-10 fx-row neg-mg-lr-px-10-ac">
        @if(isset($item['link']))
        <div class="pd-l-10 pd-r-10">
            @include('web-components::links.link', [
                'link' => $item['link'], 'classList' => ''
            ])
        </div>
        @endif
        @foreach($item['links'] as $linkItem)
        <div class="pd-l-10 pd-r-10">
            @include('web-components::links.link', [
                'link' => $linkItem, 'classList' => ''
            ])
        </div>
        @endforeach
    </div>
</div>
@endforeach
