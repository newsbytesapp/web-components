<div class="pd-l-md-10 pr-r-md-10 dp-fx fx-al-st {{isset($classList) ? $classList : ''}}">
    @switch($type)
    @case('amp')
    <amp-img src="{{$data['query']['icon']}}" alt="{{$data['query']['title']}}" class="br-rd-pr-50" layout="fixed" width="36" height="36"></amp-img>
    @break
    @default
    <img data-src="{{$data['query']['icon']}}" alt="{{$data['query']['title']}}" title="{{$data['query']['title']}}" class="lazy wd-ht-px-36 fx-basis-36px br-rd-pr-50"/>
    @break
    @endswitch
    <div class="mg-l-10 ft-pr">
        <p class="md-reg-2 reg-2 dp-ib">{!! $data['query']['title'] !!}</p>
        <span class="cl-lt md-l-10 md-reg-3 reg-3">{!! $data['query']['timestamp'] !!}</span>
        <p class="md-bd-5 bd-5 mg-t-4">{!! $data['query']['text'] !!}</p>
    </div>
</div>
<div class="mg-l-46 dp-fx fx-al-st mg-t-10">
    @switch($type)
    @case('amp')
    <div class="bg-primary br-rd-pr-50 wd-ht-px-36 fx-basis-36px pd-t-8 pd-b-8 pd-l-8 pd-r-8">
        <amp-img src="{{$data['feedback']['icon']}}" alt="{{$data['feedback']['title']}}" layout="fixed" width="20" height="20"></amp-img>
    </div>
    @break
    @default
    <div class="bg-primary br-rd-pr-50 wd-ht-px-36 fx-basis-36px pd-t-8 pd-b-8 pd-l-8 pd-r-8">
        <img data-src="{{$data['feedback']['icon']}}" alt="{{$data['feedback']['title']}}" title="{{$data['feedback']['title']}}" class="lazy ht-full wd-full"/>
    </div>
    @break
    @endswitch
    <div class="mg-l-10 ft-pr">
        <p class="cl-lt md-reg-3 reg-3">{!! $data['feedback']['subtitle'] !!}</p>
        <p class="md-reg-2 reg-2">{!! $data['feedback']['title'] !!}</p>
        <p class="ft-sec sec-md-reg-2 sec-reg-2 mg-t-10">{!! $data['feedback']['text'] !!}</p>
    </div>
</div>