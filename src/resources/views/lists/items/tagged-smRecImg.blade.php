
@foreach ($data as $item)
<?php
    if(isset($item['tagColor']) && $item['tagColor'] != ''){
        $tagColor = $item['tagColor'];
    }elseif(isset($item['tag']) && $item['tag'] != ''){
        $tagColor = $item['tag'];
    }else{
        $tagColor = 'default';
    }
    if(isset($item['tag']) && $item['tag'] != ''){
        $tag = $item['tag'];
    }else{
        $tag = '';
    }
    $targetAttr = ($linkTargetAttr == '_blank') ? 'target="_blank"' : '';
?>
<div class="col-12 col-md-6 fx-row pd-l-md-10 pd-r-md-10 mg-t-20 list-item-smRecImg">
    <a class="col-6" href="{!! $item['url'] !!}" {!! $targetAttr !!}>
        @switch($type)
        @case('amp')
        <amp-img src="{{$item['thumb']}}" srcset="{{$item['img']}} 960w, {{$item['ximg']}} 1280w" alt="{{$item['title']}}" class="dp-bl wd-full" width="1080" height="610" layout="responsive"></amp-img>
        @break
        @default
        <div class="asp-ratio r-16-9">
            <img data-src="{{$item['thumb']}}" alt="{{$item['title']}}" title="{{$item['title']}}" class="lazy dp-bl wd-full"/>
        </div>
        @break
        @endswitch
    </a>
    <div class="tx-container col-6 pd-l-10">
        <span class="cl-lt ft-ter md-ter-bd-2 ter-bd-2">{!! $item['timestamp'] !!}</span>
        <a href="{!! $item['url'] !!}" class="ft-pr md-bd-3 bd-3 cl-ink-dk mg-t-2 dp-bl clickable-item" title="{!! $item['title'] !!}">{!! $item['title'] !!}</a>
        @if(!empty(array_get($item, 'tagUrl', '')))
        <a class="cl-lt cl-@nbColor($tagColor)-dk mg-t-2 ft-ter md-ter-bd-2 ter-bd-2 dp-ib clickable-item" href="{!! $item['tagUrl'] !!}" title="{!! $tag !!}">{!! $tag !!}</a>
        @else
        <div class="cl-lt cl-@nbColor($tagColor)-dk mg-t-2 ft-ter md-ter-bd-2 ter-bd-2">{!! $tag !!}</div>
        @endif
    </div>
</div>
@endforeach