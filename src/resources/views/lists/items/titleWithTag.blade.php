<div class="mg-t-20 pd-l-16 pd-r-16 {{isset($classList) ? $classList : ''}}">
    <a href="{!! $data['url'] !!}" class="dp-fx wd-full">
        <div class="pd-r-10">
            <div class="ft-pr md-bd-3 bd-3">{!! $data['title'] !!}</div>
            @if(!empty(array_get($data, 'tag', '')))
            <div class="dp-fx fx-al-ct pd-t-10">
                <span class="cl-venetian-red ft-ter md-ter-reg-1 ter-reg-1 dp-fx fx-al-ct">{!! $data['tag'] !!}</span>
                <span class="wd-ht-px-4 bg-acadia br-rd-pr-50 mg-l-8 mg-r-8 opacity-50"></span>
                <span class="ft-ter md-ter-reg-1 ter-reg-1 cl-lt">{!! $data['timestamp'] !!}</span>
            </div>
            @else
            <div class="dp-fx fx-al-ct pd-t-10">
                <span class="ft-ter md-ter-reg-1 ter-reg-1 cl-lt">{!! $data['timestamp'] !!}</span>
            </div>
            @endif
        </div>
    </a>
</div>