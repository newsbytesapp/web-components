@if(array_get($data, 'list.data', []) && (sizeof($data['list']['data']) > 0))
<section class="bg-primary  pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 {{isset($classList) ? $classList : ''}}">
    <div class="cl-lt ft-pr bd-3 md-bd-3">{!! $data['title'] !!}</div>
    @foreach($data['list']['data'] as $item)
        <div class="col-12 mg-t-20">
            <a href="{!! $item['url'] !!}">
                <div class="asp-ratio r-16-9">
                    <img alt="{!! $item['title'] !!}" title="{!! $item['title'] !!}" class="lazy dp-bl wd-full" data-src="{{$item['img']}}">
                </div>
                <div class="bd-3 md-bd-3 cl-ink-dk mg-t-14">{!! $item['title'] !!}</div>
            </a>
        </div>
    @endforeach
</section>
@endif