<?php
    if(isset($data['videoId']) && ($data['videoId'] != '')){
        $parts = parse_url($data['videoId']);
        parse_str($parts['query'], $query);
        $videoIdStr = $parts['path'];
        $paramsStr = collect($query)->map(function($item, $key){if(empty($item)) return ''; return "data-param-$key='$item'"; })->implode(' ');
    }
?>
@if(array_get($data, 'src') && (!empty($data['src'])))
    @switch($type)
        @case('amp')
            @if(array_get($data, 'id'))
            <amp-youtube width="1080" height="610" layout="responsive" data-videoid="{{$data['id']}}" {{$data['qparams']}} class="mg-t-10 {{isset($classList) ? $classList : ''}}"></amp-youtube>
            @else
            <amp-youtube width="1080" height="610" layout="responsive" data-videoid="{{$videoIdStr}}" {{$paramsStr}} class="mg-t-10 {{isset($classList) ? $classList : ''}}"></amp-youtube>
            @endif
        @break
        @default
            <div class="wd-full mg-t-10 pos-rel asp-ratio-rect-ac {{isset($classList) ? $classList : ''}}">
                <iframe src="{{$data['src']}}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="pos-abs lt-init tp-init wd-full ht-full" title="YouTube Frame"></iframe>
            </div>
        @break
    @endswitch
@endif  