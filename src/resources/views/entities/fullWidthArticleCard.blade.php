@switch($type)
    @case('amp')
        @switch($card)
            @case('coverCard')
                <div class="pd-t-30 ft-pr cl-lt pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac cl-@nbColor($data['tag'])-dk bd-6 md-bd-6 bg-@nbColor($data['tag'])-lt">
                    @if(empty(array_get($data, 'tagUrl', '')))
                    <span>{!! $data['tag'] !!}</span>
                    @elseif(isset($data['tagAmpUrl']))
                    <a href="{!! $data['tagAmpUrl'] !!}">{!! $data['tag'] !!}</a>
                    @else
                    <a href="{!! $data['tagUrl'] !!}">{!! $data['tag'] !!}</a>
                    @endif
                </div>
                <h1 class="md-bd-1 bd-1 pd-t-6 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac bg-@nbColor($data['tag'])-lt">{!! $data['title'] !!}</h1>
                <div class="pd-t-10 bg-@nbColor($data['tag'])-lt pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">
                    @if(!empty(array_get($data, 'creators', [])))
                    <div class="pd-t-10">
                        <div class="dp-fx fx-al-ct">
                            @foreach($data['creators'] as $item)
                            <a href="{!! $item['url'] !!}" class="dp-fx fx-al-st tx-decoration-none {{($loop->index > 0) ? 'mg-l-30' : ''}}">
                                @if(sizeof($data['creators']) == 1)
                                <amp-img src="{{$item['icon']}}" alt="{{$item['title']}}" width="36" height="36" layout="fixed" class="br-rd-pr-50 mg-r-10"></amp-img>
                                @endif
                                <div>
                                    <div class="cl-lt reg-3 md-reg-3">{!! $item['subtitle'] !!}</div>
                                    <div class="bd-6 md-bd-6">{!! $item['title'] !!}</div>
                                </div>
                            </a>
                            @if((sizeof($data['creators']) == 1) && (sizeof(array_get($item, 'profiles', [])) > 0))
                            <div class="dp-fx mg-l-10">
                                @foreach($item['profiles'] as $pItem)
                                <a target="_blank" href="{!! $pItem['url'] !!}" rel="nofollow noreferrer" aria-label="Link to {{$pItem['key']}} profile" class="dp-fx wd-ht-px-50 fx-al-ct mg-l-10 tx-decoration-none">
                                    <span class="hidden">{{ucfirst($pItem['key'])}}</span>
                                    <div class="nb-icon-{{$pItem['key']}} fz-14"></div>
                                </a>
                                @endforeach
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    @endif
                    @if(isset($data['creatorSnapshot']) && !empty($data['creatorSnapshot']))
                    <div class="fx-row pd-t-10">
                        <span class="cl-lt reg-3 md-reg-3">{!! $data['creatorSnapshot']['subtitle'] !!}</span>
                        <span class="mg-l-4 bd-6 md-bd-6">{!! $data['creatorSnapshot']['timestamp'] !!}</span>
                    </div>
                    @endif
                </div>
                @if(!empty(array_get($data, 'slides', [])))
                <div class="pd-t-10 bg-@nbColor($data['tag'])-lt">
                    <amp-carousel width="1080" height="610" layout="responsive" type="slides" autoplay delay="5000" role="region" aria-label="{!! $data['title']!!} Slideshow">
                        @foreach($data['slides'] as $item)
                        <figure>
                            <amp-img src="{!! $item['ximg'] !!}" width="1080" height="610" layout="responsive" alt="{!! $item['title'] !!}"></amp-img>
                            @if(!empty(array_get($item, 'caption', '')))
                            <figcaption class="cl-lt pd-t-10 reg-2">{!! $item['caption'] !!}</figcaption>
                            @endif
                        </figure>
                        @endforeach
                    </amp-carousel>
                </div>
                @elseif((!empty($data['ximg'])) && (empty($data['video']['src'])))
                <div class="pd-t-10 bg-@nbColor($data['tag'])-lt">
                    <figure>
                        <div class="pos-rel">
                            <amp-img src="{{$data['ximg']}}" alt="{{$data['title']}}" layout="responsive" width="1080" height="610"></amp-img>
                            @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
                            @include('web-components::entities.imageCredits', ['data' => $data['imagecredit'], 'type' => $pageType, 'classList' => ''])
                            @endif
                        </div>
                        @if(!empty(array_get($data, 'caption', '')))
                        <figcaption class="cl-lt pd-b-10 pd-t-10 reg-2 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">{!! $data['caption'] !!}</figcaption>
                        @endif
                    </figure>
                </div>
                @endif
                <?php
                    if(isset($data['video']['videoId']) && !empty($data['video']['videoId'])){
                        $parts = parse_url($data['video']['videoId']);
                        parse_str($parts['query'], $query);
                        $videoIdStr = $parts['path'];
                        $paramsStr = collect($query)->map(function($item, $key){if(empty($item)) return ''; return "data-param-$key='$item'"; })->implode(' ');
                    }
                ?>
                @if(!empty(array_get($data, 'video.src', '')))
                <div class="pd-t-10 bg-@nbColor($data['tag'])-lt">
                    @if(!empty(array_get($data, 'video.id', '')))
                    <amp-youtube width="1080" height="610" layout="responsive" data-videoid="{{$data['video']['id']}}" {{$data['video']['qparams']}}></amp-youtube>
                    @else
                    <amp-youtube width="1080" height="610" layout="responsive" data-videoid="{{$videoIdStr}}" {{$paramsStr}}></amp-youtube>
                    @endif
                </div>
            @endif

            @break
            @case('eventCard')
                @switch($data['conf']['cardtype'])
                    @case('fact')
                    @if(!empty(array_get($data, 'traits.type', '')))
                        @if($data['traits']['type'] == 'scorecard')
                        <div class="pd-t-50 pd-b-40 mg-t-10 cl-primary solid-tint-@nbColor($data['tag'] . 'tint')">
                            <div class="pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">
                                @if($data['subtitle'] != '')
                                @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                                <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                                @endif
                                    <h3 class="cl-primary md-bd-6 bd-6">{!! $data['subtitle'] !!}</h3>
                                @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                                </div>
                                @endif
                                @endif
                                @if(!empty($data['traits']['title']))
                                <div class="tx-ct mg-t-10 reg-3 md-reg-3">{!! $data['traits']['title'] !!}</div>
                                @endif
                                @if(!empty($data['traits']['subtitle']))
                                <div class="tx-ct reg-3 md-reg-3">{!! $data['traits']['subtitle'] !!}</div>
                                @endif
                                @if(!empty($data['traits']['comparison']['items']))
                                <div class="fx-row mg-t-10 fx-js-bw">
                                    @foreach($data['traits']['comparison']['items'] as $item)
                                    <div class="dp-fx">
                                        @if(!empty($item['img']) && (($loop->index % 2) == 0))
                                        <div class="dp-fx fx-dr-col fx-al-ct mg-r-10 mg-r-md-20">
                                            <amp-img src="{!! $item['img'] !!}" alt="{!! $item['title'] !!}" width="50" height="50" layout="fixed" class="br-rd-pr-50"></amp-img>
                                            <div class="bd-2 md-bd-2 mg-t-6">{!! $item['title'] !!}</div>
                                        </div>
                                        @endif
                                        <div>
                                            <div class="bd-5 md-bd-5 {{(($loop->index % 2) == 0) ? 'tx-rt' : 'tx-lt'}}">{!! $item['params']['title'] !!}</div>
                                            <div class="reg-3 md-reg-3 {{(($loop->index % 2) == 0) ? 'tx-rt' : 'tx-lt'}}">{!! $item['params']['subtitle'] !!}</div>
                                        </div>
                                        @if(!empty($item['img']) && (($loop->index % 2) != 0))
                                        <div class="dp-fx fx-dr-col fx-al-ct mg-l-10 mg-l-md-20">
                                            <amp-img src="{!! $item['img'] !!}" alt="{!! $item['title'] !!}" width="50" height="50" layout="fixed" class="br-rd-pr-50"></amp-img>
                                            <div class="bd-2 md-bd-2 mg-t-6">{!! $item['title'] !!}</div>
                                        </div>
                                        @endif
                                    </div>
                                    @if(($loop->index % 2) == 0)
                                    <div class="bd-6 md-bd-6 mg-t-16">{!! $data['traits']['comparison']['connector'] !!}</div>
                                    @endif
                                    @endforeach
                                </div>
                                @endif
                                @if(!empty($data['traits']['conclusion']))
                                <div class="reg-2 mg-t-6 tx-ct">{!! $data['traits']['conclusion'] !!}</div>
                                @endif
                                @if(!empty($data['traits']['list']))
                                    @foreach($data['traits']['list'] as $item)
                                    <div class="bd-4 md-bd-4 mg-t-6 tx-ct">{!! $item['title'] !!}</div>
                                    <div class="fx-row neg-mg-lr-px-10-ac">
                                        @foreach($item['data'] as $lItem)
                                        <div class="mg-t-6 col-6 dp-fx fx-al-ct pd-l-10 pd-r-10 {{(($loop->index % 2) == 0) ? '' : 'fx-js-end'}}">
                                            @if(!empty($lItem['img']) && (($loop->index % 2) == 0))
                                            <amp-img src="{!! $lItem['img'] !!}" alt="{!! $lItem['title'] !!}" width="20" height="20" layout="fixed" class="br-rd-pr-50 mg-r-10"></amp-img>
                                            @endif
                                            <div class="reg-2 dp-fx fx-dr-col {{(($loop->index % 2) == 0) ? '' : 'fx-al-end'}}">
                                                <div>{!! $lItem['title'] !!}</div>
                                                <div>{!! $lItem['subtitle'] !!}</div>
                                            </div>
                                            @if(!empty($lItem['img']) && (($loop->index % 2) != 0))
                                            <amp-img src="{!! $lItem['img'] !!}" alt="{!! $lItem['title'] !!}" width="20" height="20" layout="fixed" class="br-rd-pr-50 mg-l-10"></amp-img>
                                            @endif
                                        </div>
                                        @endforeach
                                    </div>
                                    @endforeach
                                @endif
                                @if(!empty($data['traits']['card']))
                                    <div class="bd-4 md-bd-4 mg-t-20">{!! $data['traits']['card']['title'] !!}</div>
                                    @foreach($data['traits']['card']['text'] as $item)
                                    <div class="mg-t-10 reg-1">{!! $item !!}</div>
                                    @endforeach
                                @endif
                                @if(!empty($data['traits']['links']))
                                    <div class="fx-row neg-mg-lr-px-10-ac">
                                        @foreach($data['traits']['links'] as $item)
                                        <div class="pd-l-10 pd-r-10 col-12 mg-t-30 {{(sizeof($data['traits']['links']) > 1) ? 'col-md-6' : ''}}">
                                            <a class="dp-bl wd-full ft-pr bd-6 md-bd-6 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{!! $item['url'] !!}" target="_blank" aria-label="Button Link">
                                                <div>{!! $item['title'] !!}</div>
                                                <span class="nb-icon-external-link fz-20 mg-l-6"></span>
                                            </a>
                                        </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                        @elseif($data['traits']['type'] == 'rating')
                        <div class="ft-pr pd-t-50 pd-b-40 mg-t-10 solid-tint-@nbColor($data['tag'] . 'tint')">
                            <div class="pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">
                                @if($data['subtitle'] != '')
                                    @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                                    <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                                    @endif
                                        <div class="slug">
                                            <span class="dot bg-primary evt-connector" data-hooked="{{$data['htmlId']}}"></span>
                                            <div class="cl-primary tt">{!! $data['subtitle'] !!}</div>
                                        </div>
                                    @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                                    </div>
                                    @endif
                                @endif
                                @if(!empty($data['traits']['title']))
                                <div class="bd-1 md-bd-2 mg-t-6 cl-primary">{!! $data['traits']['title'] !!}</div>
                                @endif
                                @if(!empty(array_get($data, 'traits.ratings.keys', [])))
                                <div class="pd-t-10" title="{!! $data['traits']['ratings']['value'] !!}">
                                    <div class="cl-primary reg-3">{!! $data['traits']['ratings']['title'] !!}</div>
                                    <div class="fx-row">
                                        @foreach($data['traits']['ratings']['keys'] as $item)
                                            <div class="svg-inclusion mg-r-4">
                                                @if($item == 'star')
                                                <svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg star">
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="Group-3" fill="#BBC2BD">
                                                            <path d="M9.8316,0.5748 L11.7896,4.7468 C11.9316,5.0488 12.2146,5.2608 12.5436,5.3108 L17.0026,5.9918 C17.8076,6.1148 18.1356,7.0958 17.5666,7.6788 L14.2786,11.0488 C14.0576,11.2768 13.9566,11.5958 14.0076,11.9078 L14.7746,16.6138 C14.9096,17.4378 14.0346,18.0538 13.3046,17.6498 L9.4106,15.4978 C9.1086,15.3308 8.7436,15.3308 8.4426,15.4978 L4.5486,17.6498 C3.8176,18.0538 2.9426,17.4378 3.0776,16.6138 L3.8446,11.9078 C3.8956,11.5958 3.7956,11.2768 3.5736,11.0488 L0.2856,7.6788 C-0.2834,7.0958 0.0456,6.1148 0.8506,5.9918 L5.3086,5.3108 C5.6386,5.2608 5.9206,5.0488 6.0626,4.7468 L8.0206,0.5748 C8.3806,-0.1922 9.4716,-0.1922 9.8316,0.5748" id="Fill-1"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                                @elseif($item == 'filled_star')
                                                <svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg filled_star">
                                                    <defs>
                                                        <linearGradient x1="50%" y1="-1.12155278%" x2="50%" y2="98.8784472%" id="linearGradient-1">
                                                            <stop stop-color="#FDD800" offset="0%"></stop>
                                                            <stop stop-color="#EBC10E" offset="100%"></stop>
                                                        </linearGradient>
                                                    </defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <path d="M9,0 C8.588,0 8.176,0.219 7.972,0.656 L7.972,0.656 L6.099,4.665 C5.939,5.009 5.618,5.251 5.243,5.307 L5.243,5.307 L0.965,5.964 C0.051,6.105 -0.321,7.224 0.324,7.889 L0.324,7.889 L3.49,11.149 C3.742,11.408 3.857,11.773 3.798,12.13 L3.798,12.13 L3.06,16.673 C2.942,17.403 3.514,17.99 4.164,18 L4.164,18 L4.2,18 C4.376,17.997 4.557,17.952 4.73,17.855 L4.73,17.855 L8.45,15.789 C8.792,15.599 9.207,15.599 9.549,15.789 L9.549,15.789 L13.27,17.855 C13.444,17.952 13.625,17.997 13.801,18 L13.801,18 L13.836,18 C14.486,17.99 15.058,17.403 14.939,16.673 L14.939,16.673 L14.202,12.13 C14.143,11.773 14.258,11.408 14.51,11.149 L14.51,11.149 L17.676,7.889 C18.321,7.224 17.949,6.105 17.035,5.964 L17.035,5.964 L12.757,5.307 C12.382,5.251 12.062,5.009 11.901,4.665 L11.901,4.665 L10.028,0.656 C9.824,0.219 9.412,0 9,0 Z" id="Fill-1" fill="url(#linearGradient-1)"></path>
                                                    </g>
                                                </svg>
                                                @elseif($item == 'half_filled_star')
                                                <svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg half_filled_star">
                                                    <defs>
                                                        <linearGradient x1="49.9996654%" y1="-1.12155278%" x2="49.9996654%" y2="98.8784472%" id="linearGradient-1">
                                                            <stop stop-color="#FDD800" offset="0%"></stop>
                                                            <stop stop-color="#EBC10E" offset="100%"></stop>
                                                        </linearGradient>
                                                    </defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <path d="M9,0 C8.589,0 8.176,0.219 7.972,0.656 L7.972,0.656 L6.099,4.665 C5.939,5.009 5.618,5.251 5.243,5.307 L5.243,5.307 L0.965,5.964 C0.051,6.105 -0.321,7.224 0.324,7.889 L0.324,7.889 L3.49,11.149 C3.742,11.408 3.857,11.773 3.798,12.13 L3.798,12.13 L3.06,16.673 C2.942,17.403 3.514,17.99 4.164,18 L4.164,18 L4.2,18 C4.376,17.997 4.557,17.952 4.73,17.855 L4.73,17.855 L8.451,15.789 C8.621,15.694 8.811,15.646 9,15.646 L9,15.646 L9,0 Z" id="Fill-1" fill="url(#linearGradient-1)"></path>
                                                        <path d="M8.9999,15.6465 C9.1889,15.6465 9.3779,15.6935 9.5489,15.7885 L13.2699,17.8545 C14.0989,18.3155 15.0919,17.6125 14.9389,16.6725 L14.2019,12.1295 C14.1439,11.7725 14.2579,11.4085 14.5099,11.1495 L17.6759,7.8895 C18.3219,7.2245 17.9489,6.1045 17.0349,5.9645 L12.7569,5.3075 C12.3829,5.2505 12.0619,5.0095 11.8999,4.6645 L10.0279,0.6565 C9.8239,0.2185 9.4119,0.0005 8.9999,0.0005 L8.9999,15.6465 Z" id="Fill-4" fill="#BBC2BD"></path>
                                                    </g>
                                                </svg>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                                @if(!empty($data['traits']['img']))
                                <div class="dp-fx fx-js-ct mg-t-20">
                                    <amp-img src="{!! $data['traits']['img'] !!}" alt="{!! $data['traits']['title'] !!}" width="200" height="200" layout="fixed"></amp-img>
                                </div>
                                @endif
                                @if(!empty($data['traits']['card']['text']))
                                @foreach($data['traits']['card']['text'] as $item)
                                <div class="reg-1 cl-primary mg-t-10">{!! $item !!}</div>
                                @endforeach
                                @endif
                                @if(!empty($data['traits']['list']))
                                <div class="fx-row neg-mg-lr-px-10-ac cl-primary">
                                    @foreach($data['traits']['list'] as $item)
                                    <div class="col-12 col-md-6 pd-l-10 pd-r-10 mg-t-20">
                                        <div class="bd-4 md-bd-4">{!! $item['title'] !!}</div>
                                        @foreach($item['data'] as $dItem)
                                        <div class="pos-rel mg-t-10">
                                            <div class="reg-1 pd-t-6 pd-b-6 pd-l-20">{!! $dItem !!}</div>
                                            <div class="gd-tint-white-white0-white100-half-1 pos-abs lt-init bt-init tp-init wd-px-90"></div>
                                        </div>
                                        @endforeach
                                    </div>
                                    @endforeach
                                </div>
                                @endif
                                @if(!empty($data['traits']['links']))
                                    <div class="fx-row neg-mg-lr-px-10-ac">
                                        @foreach($data['traits']['links'] as $item)
                                        <div class="pd-l-10 pd-r-10 col-12 mg-t-30 {{(sizeof($data['traits']['links']) > 1) ? 'col-md-6' : ''}}">
                                            <a class="dp-bl wd-full ft-pr bd-6 md-bd-6 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{!! $item['url'] !!}" target="_blank" aria-label="Button Link">
                                                <div>{!! $item['title'] !!}</div>
                                                <span class="nb-icon-external-link fz-20 mg-l-6"></span>
                                            </a>
                                        </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                        @endif
                    @else
                        @if($data['subtitle'] != '')
                        @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                        <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                        @endif
                            <div class="cl-lt cl-@nbColor($data['tag'])-dk bd-6 md-bd-6 mg-t-10 ft-pr pd-t-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac solid-tint-@nbColor($data['tag'] . 'tint')">{!! $data['subtitle'] !!}</div>
                        @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                        </div>
                        @endif
                        @endif
                        @if($data['title'] != '')
                        <h2 class="pd-t-10 bd-4 md-bd-4 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac solid-tint-@nbColor($data['tag'] . 'tint')">{!! $data['title'] !!}</h2>
                        @endif

                        @if(!empty(array_get($data, 'html', [])))
                        <p class="ft-sec sec-md-reg-1 sec-reg-1 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac solid-tint-@nbColor($data['tag'] . 'tint') card-point pd-b-20">
                            @foreach($data['html'] as $item)
                            <span class="dp-bl pd-t-10">{!! $item !!}</span>
                            @endforeach
                        </p>
                        @endif
                    @endif
                    @break
                    @default
                        @if($data['subtitle'] != '')
                        @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                        <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                        @endif
                            <div class="cl-lt cl-@nbColor($data['tag'])-dk bd-6 md-bd-6 mg-t-10 ft-pr pd-t-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac bg-@nbColor($data['tag'])-lt">{!! $data['subtitle'] !!}</div>
                        @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                        </div>
                        @endif
                        @endif
                        @if($data['title'] != '')
                        <h2 class="pd-t-10 bd-4 md-bd-4 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac bg-@nbColor($data['tag'])-lt">{!! $data['title'] !!}</h2>
                        @endif

                        @if(!empty($data['title']))
                        @if(!empty(array_get($data, 'slides', [])))
                        <div class="pd-t-10 bg-@nbColor($data['tag'])-lt">
                            <amp-carousel width="1080" height="610" layout="responsive" type="slides" autoplay delay="5000" role="region" aria-label="{!! $data['title']!!} Slideshow">
                                @foreach($data['slides'] as $item)
                                <figure>
                                    <amp-img src="{!! $item['ximg'] !!}" width="1080" height="610" layout="responsive" alt="{!! $item['title'] !!}"></amp-img>
                                    @if(!empty(array_get($item, 'caption', '')))
                                    <figcaption class="cl-lt pd-t-10 reg-2">{!! $item['caption'] !!}</figcaption>
                                    @endif
                                </figure>
                                @endforeach
                            </amp-carousel>
                        </div>
                        @elseif((!empty($data['ximg'])) && (empty($data['video']['src'])))
                        <div class="pd-t-10 bg-@nbColor($data['tag'])-lt">
                            <figure>
                                <div class="pos-rel">
                                    <amp-img src="{{$data['ximg']}}" alt="{{$data['title']}}" layout="responsive" width="1080" height="610"></amp-img>
                                    @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
                                    @include('web-components::entities.imageCredits', ['data' => $data['imagecredit'], 'type' => $pageType, 'classList' => ''])
                                    @endif
                                </div>
                                @if(!empty(array_get($data, 'caption', '')))
                                <figcaption class="cl-lt pd-b-10 pd-t-10 reg-2 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">{!! $data['caption'] !!}</figcaption>
                                @endif
                            </figure>
                        </div>
                        @endif
                        <?php
                            if(isset($data['video']['videoId']) && !empty($data['video']['videoId'])){
                                $parts = parse_url($data['video']['videoId']);
                                parse_str($parts['query'], $query);
                                $videoIdStr = $parts['path'];
                                $paramsStr = collect($query)->map(function($item, $key){if(empty($item)) return ''; return "data-param-$key='$item'"; })->implode(' ');
                            }
                        ?>
                        @if(!empty(array_get($data, 'video.src', '')))
                        <div class="pd-t-10 bg-@nbColor($data['tag'])-lt">
                            @if(!empty(array_get($data, 'video.id', '')))
                            <amp-youtube width="1080" height="610" layout="responsive" data-videoid="{{$data['video']['id']}}" {{$data['video']['qparams']}}></amp-youtube>
                            @else
                            <amp-youtube width="1080" height="610" layout="responsive" data-videoid="{{$videoIdStr}}" {{$paramsStr}}></amp-youtube>
                            @endif
                        </div>
                        @endif
                        @endif
                        @if(!empty(array_get($data, 'html', [])))
                        <p class="ft-sec sec-md-reg-1 sec-reg-1 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac bg-@nbColor($data['tag'])-lt card-point pd-b-20">
                            @foreach($data['html'] as $item)
                            <span class="dp-bl pd-t-10">{!! $item !!}</span>
                            @endforeach
                        </p>
                        @endif
                        @if($data['conf']['cardtype'] == 'embed' && isset($data['conf']['type']))
                        <div class="pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-t-10 pd-b-20 bg-@nbColor($data['tag'])-lt">
                            @include('web-components::embeddedPosts.' . $data['conf']['type'], ['data' => $data['conf'], 'type' => $pageType, 'classList' => ''])
                        </div>
                        @endif
                    @break
                @endswitch
            @break
        @endswitch
    @break
    @default
    @break
@endswitch