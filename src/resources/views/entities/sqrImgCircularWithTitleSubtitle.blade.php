@if($linkable)
    <a href="{!! $data['url'] !!}" class="{{$attributes['classListContainer']}}">
        @switch($type)
        @case('amp')
        <amp-img src="{{$data['icon']}}" alt="{!! $data['title'] !!}" width="50" height="50" layout="fixed" class="br-rd-pr-50"></amp-img>
        @break
        @default
        <img data-src="{{$data['icon']}}" class="lazy {{$attributes['classListIcon']}}" alt="{!! $data['title'] !!}" title="{!! $data['title'] !!}"/>
        @break
        @endswitch
        <div class="tx-container {{$attributes['classListTextContainer']}}">
            <p class="{{$attributes['classListTitle']}}">{!! $data['title'] !!}</p>
            <div class="{{$attributes['classListSubtitle']}}">{!! $data['subtitle'] !!}</div>
        </div>
    </a>
@else
@endif