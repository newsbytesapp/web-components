@if(array_get($data, 'query.input', []) && (sizeof($data['query']['input']) > 0))
<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
<section class="bg-primary ft-pr  pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 card-with-query-template-response query-template-parent {{isset($classList) ? $classList : ''}}" id="nwl-{{$componentId}}">
    <div class="bd-3 md-bd-3">{!! $data['query']['title'] !!}</div>
    @if(array_get($data, 'query.subtitle', '') && (!empty($data['query']['subtitle'])))
    <div class="reg-1 md-reg-1 mg-t-10">{!! $data['query']['subtitle'] !!}</div>
    @endif
    <section class="query-section">
        <div class="dp-fx fx-dr-col fx-al-ct">
            <div class="wd-full dp-fx fx-dr-col fx-al-ct">
                @foreach($data['query']['input'] as $key => $item)
                @if($item['type'] == 'textarea')
                <textarea placeholder="{!! $item['placeholder'] !!}" rows="5" data-name="{{$item['name']}}" class="mg-t-20 cl-ink-dk wd-full pd-t-14 pd-b-14 pd-l-20 pd-r-20 bg-primary reg-2 md-reg-2 br-rd-10 query hide-native-scrollbar">{!! $item['value'] !!}</textarea>
                @else
                <input type="{{$item['type']}}" id="nwl-{{$item['type'].'-'.$key.'-'.$componentId}}" placeholder="{!! $item['placeholder'] !!}" data-name="{{$item['name']}}" value="{!! $item['value'] !!}" class="mg-t-20 cl-ink-dk wd-full pd-t-14 pd-b-14 pd-l-20 pd-r-20 bg-primary reg-2 md-reg-2 br-rd-10 query" />
                <label for="nwl-{{$item['type'].'-'.$key.'-'.$componentId}}" class="cl-transparent">{!! $item['placeholder'] !!}</label>
                @endif
                @endforeach
                <button class="cs-ptr mg-t-20 dp-ib bg-lt br-rd-10 pd-t-14 pd-b-14 pd-l-30 pd-r-30 cl-primary bd-6 md-bd-6 query-submit" id="nwl-query-submit-{{$componentId}}" data-api="{{$data['query']['button']['api']}}" data-type="{{$data['query']['button']['apiType']}}">{{$data['query']['button']['title']}}</button>
                @include('web-components::toasts.dotsLoader')
                @include('web-components::toasts.error')
            </div>
        </div>
    </section>
</section>
@endif

@section('css')
@parent
<style type="text/css">
    .query-submit{
        pointer-events: none;
    }
    .query-submit.active{
        background-color: #0053b3;
        pointer-events: auto;
    }
    .query-section input~label{
        line-height: 0;
    }
</style>
@stop