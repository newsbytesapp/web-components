@if(!empty($data['list']['data']))
<div class="cl-suva-grey ft-ter md-ter-reg-2 ter-reg-2 wrapper wht-sp-nowrap ovr-hidden breadcrumbs tx-ovr-ellipsis {{isset($classList) ? $classList : ''}}">
    @foreach($data['list']['data'] as $item)
        @if(($loop->index > 0) && ($loop->index < sizeof($data['list']['data'])))
        <span class="cl-ink-dk">/</span>
        @endif
        @if((isset($item['url']) && !empty($item['url'])) && (!$loop->last))
        <a href="{!! $item['url'] !!}" class="cl-ink-dk">{!! $item['title'] !!}</a>
        @else
        <span>{!! $item['title'] !!}</span>
        @endif
    @endforeach
</div>
@endif