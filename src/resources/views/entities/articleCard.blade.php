<?php
    $externalLinkSvg = '
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="mg-l-6">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="iconfinder_icon-external-link_2867894" transform="translate(6.000000, 6.000000)" fill="currentColor" fill-rule="nonzero">
                <path d="M10.6666667,2.27333333 L3.8,9.14 C3.63570083,9.32899768 3.37934304,9.40997717 3.13628801,9.34965644 C2.89323298,9.28933571 2.70448468,9.09789101 2.64761743,8.85400486 C2.59075019,8.61011871 2.67535764,8.35493529 2.86666667,8.19333333 L9.72,1.33333333 L7.33333333,1.33333333 C6.9651435,1.33333333 6.66666667,1.0348565 6.66666667,0.666666667 C6.66666667,0.298476833 6.9651435,0 7.33333333,0 L11.3333333,0 C11.7015232,0 12,0.298476833 12,0.666666667 L12,4.66666667 C12,5.0348565 11.7015232,5.33333333 11.3333333,5.33333333 C10.9651435,5.33333333 10.6666667,5.0348565 10.6666667,4.66666667 L10.6666667,2.27333333 Z M9.33333333,7.33333333 C9.33333333,6.9651435 9.63181017,6.66666667 10,6.66666667 C10.3681898,6.66666667 10.6666667,6.9651435 10.6666667,7.33333333 L10.6666667,10.6666667 C10.6666667,11.4030463 10.069713,12 9.33333333,12 L1.33333333,12 C0.596953667,12 0,11.4030463 0,10.6666667 L0,2.66666667 C0,1.93333333 0.6,1.33333333 1.33333333,1.33333333 L4.66666667,1.33333333 C5.0348565,1.33333333 5.33333333,1.63181017 5.33333333,2 C5.33333333,2.36818983 5.0348565,2.66666667 4.66666667,2.66666667 L1.33333333,2.66666667 L1.33333333,10.6666667 L9.33333333,10.6666667 L9.33333333,7.33333333 Z" id="Shape"></path>
            </g>
        </g>
    </svg>
    ';
    $internalLinkSvg = '
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="mg-l-6">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
            <g id="Group-5" transform="translate(2.000000, 9.000000)" stroke="#000000" stroke-width="2">
                <line x1="-2.27318164e-13" y1="3" x2="20" y2="3" id="Path-2"></line>
                <polyline id="Path-3" stroke-linejoin="round" points="17 0 20 3 17 6"></polyline>
            </g>
        </g>
    </svg>
    ';
?>
@switch($type)
    @case('amp')
        @switch($card)
            @case('coverCard')
                <div class="pd-l-pr-4 cover-card">
                    <div class="slug dp-fx fx-al-ct">
                        <span class="z-index-1 bg-lt bg-@nbColor($data['tag']) evt-connector" id="{{$data['htmlId']}}"></span>
                        <div class="cl-lt cl-@nbColor($data['tag']) mg-l-10 ft-ter md-ter-bd-2 ter-bd-2">
                            <span>{!! $data['tag'] !!}</span>
                        </div>
                    </div>
                </div>
                <h1 class="cover-card-title ft-pr bd-8 md-bd-8 pd-l-pr-4 mg-t-6">{!! $data['title'] !!}</h1>
                <div class="pd-l-pr-4">
                    @if(!empty(array_get($data, 'creators', [])))
                    <div class="pd-t-2">
                        <div class="dp-fx fx-al-ct">
                            @foreach($data['creators'] as $item)
                            <div class="dp-fx fx-al-ct tx-decoration-none mg-t-10 {{($loop->index > 0) ? 'mg-l-30' : ''}}">
                                {{-- @if(sizeof($data['creators']) == 1)
                                <amp-img src="{{$item['icon']}}" alt="{{$item['title']}}" width="36" height="36" layout="fixed" class="br-rd-pr-50 mg-r-10"></amp-img>
                                @endif --}}
                                <div class="cl-ink-dk ft-ter dp-fx fx-al-ct">
                                    <span class="md-ter-reg-1 ter-reg-1">{!! $item['subtitle'] !!}</span>
                                    <span class="md-ter-bd-2 ter-bd-2 mg-l-4">{!! $item['title'] !!}</span>
                                </div>
                            </div>
                            {{-- @if((sizeof($data['creators']) == 1) && (sizeof(array_get($item, 'profiles', [])) > 0))
                            <div class="dp-fx mg-l-10">
                                @foreach($item['profiles'] as $pItem)
                                <a href="{!! $pItem['url'] !!}" target="_blank" rel="nofollow noreferrer" aria-label="Link to {{$pItem['key']}} profile" class="dp-fx pd-l-16 pd-r-16 pd-t-16 pd-b-16 fx-al-ct mg-l-10">
                                    <span class="hidden">{{ucfirst($pItem['key'])}}</span>
                                    @switch($pItem['key'])
                                    @case('mail')
                                    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="cl-ink-dk">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="Group-35-Copy">
                                                <rect id="Rectangle-Copy-44" fill-opacity="0" fill="#D8D8D8" x="0" y="0" width="24" height="24"></rect>
                                                <g id="iconfinder_mail-24_103176-copy" transform="translate(4.000000, 6.000000)" fill="#000000" fill-rule="nonzero">
                                                    <path d="M8.30976,6.6 C7.05984,6.6 0.62016,1.7346 0.62016,1.7346 L0.62016,1.2 C0.62016,0.5376 1.1936,0 1.90144,0 L14.71808,0 C15.42656,0 16,0.5376 16,1.2 L15.98976,1.8 C15.98976,1.8 9.61984,6.6 8.30976,6.6 Z M8.30976,8.25 C9.68,8.25 15.98976,3.6 15.98976,3.6 L16,10.8 C16,11.4624 15.42656,12 14.71808,12 L1.90144,12 C1.19424,12 0.62016,11.4624 0.62016,10.8 L0.6304,3.6 C0.62976,3.6 7.05984,8.25 8.30976,8.25 Z" id="Shape"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    @break
                                    @case('facebook')
                                    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="cl-ink-dk">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="Group-31">
                                                <rect id="Rectangle" fill-opacity="0" fill="#D8D8D8" x="0" y="0" width="24" height="24"></rect>
                                                <g id="iconfinder_social-facebook_216078" transform="translate(7.200000, 3.200000)" fill="currentColor">
                                                    <path d="M6.6,6.6 L9.9,6.6 L9.9,9.9 L6.6,9.9 L6.6,17.6 L3.3,17.6 L3.3,9.9 L0,9.9 L0,6.6 L3.3,6.6 L3.3,5.2195 C3.3,3.9116 3.7114,2.2594 4.5298,1.3563 C5.3482,0.451 6.3701,0 7.5944,0 L9.9,0 L9.9,3.3 L7.59,3.3 C7.0422,3.3 6.6,3.7422 6.6,4.2889 L6.6,6.6 Z" id="Path"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    @break
                                    @case('twitter')
                                    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="cl-ink-dk">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="Group-34">
                                                <rect id="Rectangle-Copy-43" fill-opacity="0" fill="#D8D8D8" x="0" y="0" width="24" height="24"></rect>
                                                <path d="M20.7812026,6.41252373 C20.1642942,6.6864384 19.5003768,6.8712032 18.8037574,6.953888 C19.5150245,6.52855467 20.0603974,5.85482667 20.3175845,5.0511168 C19.6523045,5.44548587 18.9158297,5.73165013 18.1313239,5.8861312 C17.5035148,5.21784747 16.60864,4.8 15.6183845,4.8 C13.7172439,4.8 12.1758245,6.33970667 12.1758245,8.2390752 C12.1758245,8.50822613 12.2061419,8.77057173 12.2650735,9.02270933 C9.40365419,8.8791168 6.86652903,7.510224 5.1684129,5.42949333 C4.87205161,5.9371712 4.70241032,6.52787413 4.70241032,7.15872853 C4.70241032,8.35170347 5.31012129,9.40448853 6.23395097,10.0210517 C5.66984258,10.0030176 5.13877677,9.84819627 4.67447742,9.5906144 C4.67413677,9.6049056 4.67413677,9.61953707 4.67413677,9.63416853 C4.67413677,11.3001141 5.86094452,12.6897632 7.43608774,13.0062112 C7.14722065,13.0844725 6.84302452,13.1266656 6.52894968,13.1266656 C6.30684903,13.1266656 6.09122065,13.1052288 5.88104258,13.0647371 C6.3194529,14.4309077 7.59074065,15.4255072 9.09707355,15.4530688 C7.91878194,16.3755317 6.43459097,16.9254027 4.82129548,16.9254027 C4.54366968,16.9254027 4.26945032,16.9090699 4,16.8770848 C5.52404645,17.8533099 7.33355355,18.4222357 9.27761548,18.4222357 C15.6105497,18.4222357 19.0732077,13.1821291 19.0732077,8.63752747 C19.0732077,8.48849067 19.0701419,8.33979413 19.0636697,8.1921184 C19.7361032,7.7082592 20.319969,7.10224427 20.7812026,6.41252373 Z" id="Path" fill="currentColor"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    @break
                                    @case('linkedin')
                                    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="cl-ink-dk">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="Group-35">
                                                <rect id="Rectangle-Copy-44" fill-opacity="0" fill="#D8D8D8" x="0" y="0" width="24" height="24"></rect>
                                                <g id="iconfinder_Linkedin_Solid_1435169" transform="translate(4.000000, 4.000000)" fill="currentColor">
                                                    <path d="M0.276923077,5.29230769 L3.58769231,5.29230769 L3.58769231,15.9384615 L0.276923077,15.9384615 L0.276923077,5.29230769 Z M1.93846154,0 C2.99692308,0 3.85846154,0.861538462 3.85846154,1.92 C3.85846154,2.97846154 2.99692308,3.84 1.93846154,3.84 C0.873846154,3.84 0.0184615385,2.97846154 0.0184615385,1.92 C0.0184615385,0.861538462 0.873846154,0 1.93846154,0 L1.93846154,0 Z" id="XMLID_195_" fill-rule="nonzero"></path>
                                                    <path d="M5.66769231,5.29230769 L8.83692308,5.29230769 L8.83692308,6.75076923 L8.88,6.75076923 C9.32307692,5.91384615 10.4,5.03384615 12.0123077,5.03384615 C15.36,5.03384615 15.9815385,7.23692308 15.9815385,10.1046154 L15.9815385,15.9384615 L12.6769231,15.9384615 L12.6769231,10.7630769 C12.6769231,9.52615385 12.6584615,7.93846154 10.96,7.93846154 C9.23692308,7.93846154 8.97846154,9.28615385 8.97846154,10.6707692 L8.97846154,15.9384615 L5.67384615,15.9384615 L5.66769231,5.29230769 L5.66769231,5.29230769 Z" id="XMLID_192_"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    @break
                                    @case('youtube')
                                    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="cl-ink-dk">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="youtube-video" transform="translate(3.000000, 6.000000)">
                                                <path d="M0.385491429,1.70786571 C0.496345714,0.935571429 1.13027143,0.355765714 1.90850571,0.300231429 C3.56909143,0.181722857 6.45975429,0 8.57142857,0 C10.6830857,0 13.5738,0.181722857 15.2343429,0.300231429 C16.0125429,0.355765714 16.6464857,0.935571429 16.7574,1.70786571 C16.9277143,2.89473429 17.1428571,4.65908571 17.1428571,6.00008571 C17.1428571,7.34117143 16.9277143,9.10542857 16.7574,10.2923143 C16.6464857,11.0646 16.0125429,11.6444571 15.2342571,11.6999143 C13.5737143,11.8183714 10.6830857,12 8.57142857,12 C6.45978,12 3.56916857,11.8183714 1.90856571,11.6999143 C1.13030571,11.6444571 0.496337143,11.0646 0.385482857,10.2923143 C0.215125714,9.10542857 0,7.34117143 0,6.00008571 C0,4.65908571 0.215134286,2.89473429 0.385491429,1.70786571 Z" id="Path" fill="currentColor"></path>
                                                <polygon id="Path" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="12 6 6.85714286 8.96922857 6.85714286 3.03077143"></polygon>
                                                <polygon id="Path" fill="#FFFFFF" points="6.85714286 8.57142857 6.85714286 3 12 6"></polygon>
                                            </g>
                                        </g>
                                    </svg>
                                    @break
                                    @endswitch
                                </a>
                                @endforeach
                            </div>
                            @endif --}}
                            @endforeach
                        </div>
                    </div>
                    @endif
                    @if(isset($data['creatorSnapshot']) && !empty($data['creatorSnapshot']))
                    <div class="fx-row pd-t-6 ft-ter md-ter-reg-1 ter-reg-1">
                        <span>{!! $data['creatorSnapshot']['timestamp'] !!}</span>
                        @if(!empty(array_get($data, 'stats', [])))
                        <span class="mg-l-6 pd-l-6 stats-bar">{!! $data['stats']['subtitle'] !!}</span>
                        @endif
                    </div>
                    @endif
                </div>
                @if(!empty(array_get($data, 'slides', [])))
                <div class="mg-t-10 pd-l-pr-4">
                    <amp-carousel width="1080" height="610" layout="responsive" type="slides" autoplay delay="5000" role="region" aria-label="{!! $data['title']!!} Slideshow">
                        @foreach($data['slides'] as $item)
                        <figure>
                            <amp-img src="{!! $item['ximg'] !!}" width="1080" height="610" layout="responsive" alt="{!! $item['title'] !!}"></amp-img>
                            @if(!empty(array_get($item, 'caption', '')))
                            <figcaption class="cl-lt pd-t-4 ft-sec md-sec-reg-2 sec-reg-2">{!! $item['caption'] !!}</figcaption>
                            @endif
                        </figure>
                        @endforeach
                    </amp-carousel>
                </div>
                @elseif((!empty($data['ximg'])) && (empty($data['video']['src'])))
                <div class="mg-t-10 pd-l-pr-4">
                    <figure>
                        <div class="pos-rel">
                            <amp-img src="{{$data['ximg']}}" alt="{{$data['title']}}" layout="responsive" width="1080" height="610"></amp-img>
                            @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
                            @include('web-components::entities.imageCredits', ['data' => $data['imagecredit'], 'type' => $pageType, 'classList' => ''])
                            @endif
                        </div>
                        @if(!empty(array_get($data, 'caption', '')))
                        <figcaption class="cl-lt pd-t-4 ft-sec md-sec-reg-2 sec-reg-2">{!! $data['caption'] !!}</figcaption>
                        @endif
                    </figure>
                </div>
                @endif
                <?php
                    if(isset($data['video']['videoId']) && !empty($data['video']['videoId'])){
                        $parts = parse_url($data['video']['videoId']);
                        parse_str($parts['query'], $query);
                        $videoIdStr = $parts['path'];
                        $paramsStr = collect($query)->map(function($item, $key){if(empty($item)) return ''; return "data-param-$key='$item'"; })->implode(' ');
                    }
                ?>
                @if(!empty(array_get($data, 'video.src', '')))
                <div class="mg-t-10 pd-l-pr-4">
                    @if(!empty(array_get($data, 'video.id', '')))
                    <amp-youtube width="1080" height="610" layout="responsive" data-videoid="{{$data['video']['id']}}" {{$data['video']['qparams']}}></amp-youtube>
                    @else
                    <amp-youtube width="1080" height="610" layout="responsive" data-videoid="{{$videoIdStr}}" {{$paramsStr}}></amp-youtube>
                    @endif
                </div>
                @endif

            @break
            @case('eventCard')
                @switch($data['conf']['cardtype'])
                    @case('fact')
                    @if(!empty(array_get($data, 'traits.type', '')))
                        @if($data['traits']['type'] == 'scorecard')
                        @if(array_get($data, 'traits.sport', 'cricket') == 'tennis')
                        <div class="mg-t-10 pd-l-pr-4">
                            <div class="pd-t-30 pd-b-30 pd-l-20 pd-r-20 ft-pr br-rd-20 cl-primary solid-tint-@nbColor($data['tag'] . 'tint')">
                                @if($data['subtitle'] != '')
                                    @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                                    <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                                    @endif
                                        <div class="dp-fx fx-al-ct">
                                            <div class="cl-primary ft-pr md-bd-7 bd-7">{!! $data['subtitle'] !!}</div>
                                        </div>
                                    @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                                    </div>
                                    @endif
                                @endif
                                @if(!empty($data['traits']['subtitle']))
                                <div class="mg-t-2 ft-ter md-ter-reg-2 ter-reg-2">{!! $data['traits']['subtitle'] !!}</div>
                                @endif
                                @if(!empty($data['traits']['title']))
                                <div class="ft-ter md-ter-reg-2 ter-reg-2">{!! $data['traits']['title'] !!}</div>
                                @endif
                                @if(!empty($data['traits']['comparison']['items']))
                                    @foreach($data['traits']['comparison']['items'] as $item)
                                    <div class="dp-fx fx-al-ct fx-js-bw mg-t-20">
                                        <div class="dp-fx fx-al-ct">
                                            @if(!empty($item['img']))
                                            <amp-img src="{!! $item['img'] !!}" alt="{!! $item['subtitle'] !!}" width="40" height="40" layout="fixed" class="br-rd-pr-50 fx-basis-40px"></amp-img>
                                            @endif
                                            <div class="mg-l-10 {{$item['highlight'] ? '' : 'opacity-8'}}">
                                                <div class="ft-ter md-ter-bd-5 ter-bd-5">{!! $item['title'] !!}</div>
                                                <div class="ft-ter md-ter-reg-1 ter-reg-1">{!! $item['subtitle'] !!}</div>
                                            </div>
                                        </div>
                                        @if(!empty(array_get($item, 'params', [])))
                                        <div class="dp-fx fx-al-ct ft-ter md-ter-bd-5 ter-bd-5 {{$item['highlight'] ? '' : 'opacity-8'}}">
                                            @foreach($item['params'] as $param)
                                            <div class="pd-l-6 pd-r-6 pd-l-md-10 pd-r-md-10">{!! $param['title'] !!}</div>
                                            @endforeach
                                        </div>
                                        @endif
                                    </div>
                                    @endforeach
                                @endif
                                {{-- <div class="dp-fx fx-js-end">
                                    <div class="collapse-bttn open dp-fx fx-al-ct fx-js-bw br-rd-10 pd-t-14 pd-b-14 pd-l-20 pd-r-20 br-1p-pale-lavender cs-ptr wd-full wd-full-md-dac mg-t-20" data-id="{{$data['htmlId']}}">
                                        <div class="ft-ter md-ter-bd-2 ter-bd-2">{!! $data['traits']['buttons'][0]['title'] !!}</div>
                                        <div class="mg-l-20 nb-icon-angle-down fz-20 rotate-neg-90"></div>
                                    </div>
                                </div> --}}
                                @if(!empty($data['traits']['list']))
                                <div id="collapsible-{{$data['htmlId']}}">
                                    <div class="sc-stats-separator mg-t-20"></div>
                                    @foreach($data['traits']['list'] as $item)
                                    @if($loop->first)
                                    <div class="dp-fx fx-js-bw fx-al-ct mg-t-20">
                                        @foreach($item['data'] as $dItem)
                                        @if(($loop->index % 2) == 1)
                                        @if(!empty($item['title']))
                                        <div class="ft-ter md-ter-reg-3 ter-reg-3 tx-ct">{!! $item['title'] !!}</div>
                                        @endif
                                        @endif
                                        <div class="ft-ter md-ter-bd-2 ter-bd-2 {{($loop->index % 2) == 0 ? 'tx-lt' : 'tx-rt'}}">{!! $dItem['title'] !!}</div>
                                        @endforeach
                                    </div>
                                    @else
                                    <div class="dp-fx fx-js-bw fx-al-ct mg-t-20">
                                        @foreach($item['data'] as $dItem)
                                        @if(($loop->index % 2) == 1)
                                        @if(!empty($item['title']))
                                        <div class="ft-ter md-ter-reg-3 ter-reg-3 tx-ct">{!! $item['title'] !!}</div>
                                        @endif
                                        @endif
                                        <div class="ft-ter md-ter-bd-5 ter-bd-5 {{($loop->index % 2) == 0 ? 'tx-lt' : 'tx-rt'}}">{!! $dItem['title'] !!}</div>
                                        @endforeach
                                    </div>
                                    @endif
                                    @endforeach
                                </div>
                                @endif
                                {{-- <div class="dp-fx fx-js-end">
                                    <div class="collapse-bttn close dp-fx fx-al-ct fx-js-bw br-rd-10 pd-t-14 pd-b-14 pd-l-20 pd-r-20 br-1p-pale-lavender cs-ptr wd-full wd-full-md-dac hidden mg-t-20" data-id="{{$data['htmlId']}}">
                                        <div class="ft-ter md-ter-bd-2 ter-bd-2">{!! $data['traits']['buttons'][1]['title'] !!}</div>
                                        <div class="mg-l-20 nb-icon-angle-down fz-20 rotate-180"></div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                        @else
                        <div class="mg-t-20 pd-l-pr-4">
                            <div class="pd-t-50 pd-b-50 pd-l-20 pd-r-20 ft-pr br-rd-20 cl-primary solid-tint-@nbColor($data['tag'] . 'tint')">
                                <div class="pd-l-pr-4">
                                    @if($data['subtitle'] != '')
                                    @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                                    <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                                    @endif
                                        <h3 class="cl-primary ft-ter md-ter-bd-2 ter-bd-2">{!! $data['subtitle'] !!}</h3>
                                    @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                                    </div>
                                    @endif
                                    @endif
                                    @if(!empty($data['traits']['title']))
                                    <div class="tx-ct mg-t-10 ft-ter md-ter-reg-2 ter-reg-2">{!! $data['traits']['title'] !!}</div>
                                    @endif
                                    @if(!empty($data['traits']['subtitle']))
                                    <div class="tx-ct ft-ter md-ter-reg-2 ter-reg-2">{!! $data['traits']['subtitle'] !!}</div>
                                    @endif
                                    @if(!empty($data['traits']['comparison']['items']))
                                    <div class="fx-row mg-t-10 fx-js-bw">
                                        @foreach($data['traits']['comparison']['items'] as $item)
                                        <div class="dp-fx">
                                            @if(!empty($item['img']) && (($loop->index % 2) == 0))
                                            <div class="dp-fx fx-dr-col fx-al-ct mg-r-10 mg-r-md-20">
                                                <amp-img src="{!! $item['img'] !!}" alt="{!! $item['title'] !!}" width="20" height="20" layout="fixed" class="br-rd-pr-50"></amp-img>
                                                <div class="ft-ter md-ter-bd-6 ter-bd-6 mg-t-16">{!! $item['title'] !!}</div>
                                            </div>
                                            @endif
                                            <div>
                                                <div class="ft-ter md-ter-bd-3 ter-bd-3 {{(($loop->index % 2) == 0) ? 'tx-rt' : 'tx-lt'}}">{!! $item['params']['title'] !!}</div>
                                                <div class="ft-ter md-ter-reg-2 ter-reg-2 {{(($loop->index % 2) == 0) ? 'tx-rt' : 'tx-lt'}}">{!! $item['params']['subtitle'] !!}</div>
                                            </div>
                                            @if(!empty($item['img']) && (($loop->index % 2) != 0))
                                            <div class="dp-fx fx-dr-col fx-al-ct mg-l-10 mg-l-md-20">
                                                <amp-img src="{!! $item['img'] !!}" alt="{!! $item['title'] !!}" width="20" height="20" layout="fixed" class="br-rd-pr-50"></amp-img>
                                                <div class="ft-ter md-ter-bd-6 ter-bd-6 mg-t-16">{!! $item['title'] !!}</div>
                                            </div>
                                            @endif
                                        </div>
                                        @if(($loop->index % 2) == 0)
                                        <div class="ft-ter md-ter-bd-2 ter-bd-2 mg-t-16">{!! $data['traits']['comparison']['connector'] !!}</div>
                                        @endif
                                        @endforeach
                                    </div>
                                    @endif
                                    @if(!empty($data['traits']['conclusion']))
                                    <div class="ft-ter md-ter-reg-3 ter-reg-3 mg-t-20 tx-ct">{!! $data['traits']['conclusion'] !!}</div>
                                    @endif
                                    @if(!empty($data['traits']['list']))
                                        @foreach($data['traits']['list'] as $item)
                                        <div class="ft-ter md-ter-bd-4 ter-bd-4 mg-t-20 tx-ct">{!! $item['title'] !!}</div>
                                        <div class="fx-row neg-mg-lr-px-10-ac">
                                            @foreach($item['data'] as $lItem)
                                            <div class="mg-t-6 col-6 dp-fx fx-al-ct pd-l-10 pd-r-10 {{(($loop->index % 2) == 0) ? '' : 'fx-js-end'}}">
                                                @if(!empty($lItem['img']) && (($loop->index % 2) == 0))
                                                <amp-img src="{!! $lItem['img'] !!}" alt="{!! $lItem['title'] !!}" width="20" height="20" layout="fixed" class="br-rd-pr-50 mg-r-10"></amp-img>
                                                @endif
                                                <div class="ft-ter md-ter-reg-2 ter-reg-2 dp-fx fx-dr-col {{(($loop->index % 2) == 0) ? '' : 'fx-al-end'}}">
                                                    <div>{!! $lItem['title'] !!}</div>
                                                    <div>{!! $lItem['subtitle'] !!}</div>
                                                </div>
                                                @if(!empty($lItem['img']) && (($loop->index % 2) != 0))
                                                <amp-img src="{!! $lItem['img'] !!}" alt="{!! $lItem['title'] !!}" width="20" height="20" layout="fixed" class="br-rd-pr-50 mg-l-10"></amp-img>
                                                @endif
                                            </div>
                                            @endforeach
                                        </div>
                                        @endforeach
                                    @endif
                                    @if(!empty($data['traits']['card']))
                                        <div class="ft-ter md-ter-bd-4 ter-bd-4 mg-t-20">{!! $data['traits']['card']['title'] !!}</div>
                                        @foreach($data['traits']['card']['text'] as $item)
                                        <div class="mg-t-10 ft-ter md-ter-reg-3 ter-reg-3">{!! $item !!}</div>
                                        @endforeach
                                    @endif
                                    @if(!empty($data['traits']['links']))
                                        <div class="fx-row neg-mg-lr-px-10-ac">
                                            @foreach($data['traits']['links'] as $item)
                                            <div class="pd-l-10 pd-r-10 col-12 mg-t-30 {{(sizeof($data['traits']['links']) > 1) ? 'col-md-6' : ''}}">
                                                {{-- <a class="dp-bl wd-full ft-ter md-ter-bd-2 ter-bd-2 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{!! $item['url'] !!}" target="_blank" aria-label="Button Link">
                                                    <div>{!! $item['title'] !!}</div>
                                                    <span class="nb-icon-external-link fz-20 mg-l-6"></span>
                                                </a> --}}
                                                <a class="dp-bl wd-full ft-ter md-ter-bd-2 ter-bd-2 cl-primary dp-fx fx-js-ct fx-al-ct tx-decoration-none" href="{!! $item['url'] !!}" target="_blank" aria-label="Button Link">
                                                    <div>{!! $item['title'] !!}</div>
                                                    {!! $externalLinkSvg !!}
                                                </a>
                                            </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                        @elseif($data['traits']['type'] == 'rating')
                        <div class="mg-t-20 pd-l-pr-4">
                            <div class="pd-t-50 pd-b-50 pd-l-20 pd-r-20 ft-pr br-rd-20 cl-primary solid-tint-@nbColor($data['tag'] . 'tint')">
                                <div class="pd-l-pr-4">
                                    @if($data['subtitle'] != '')
                                        @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                                        <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at" data-id="{{$data['htmlId']}}">
                                        @endif
                                            <div class="cl-primary ft-ter md-ter-bd-3 ter-bd-3">{!! $data['subtitle'] !!}</div>
                                        @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                                        </div>
                                        @endif
                                    @endif
                                    @if(!empty($data['traits']['title']))
                                    <div class="ft-sec md-sec-bd-5 sec-bd-5 mg-t-6 cl-primary">{!! $data['traits']['title'] !!}</div>
                                    @endif
                                    @if(!empty(array_get($data, 'traits.ratings.keys', [])))
                                    <div class="pd-t-10" title="{!! $data['traits']['ratings']['value'] !!}">
                                        <div class="cl-primary ft-ter md-ter-reg-2 ter-reg-2">{!! $data['traits']['ratings']['title'] !!}</div>
                                        <div class="fx-row">
                                            @foreach($data['traits']['ratings']['keys'] as $item)
                                                <div class="svg-inclusion mg-r-4">
                                                    @if($item == 'star')
                                                    <svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg star">
                                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <g id="Group-3" fill="#BBC2BD">
                                                                <path d="M9.8316,0.5748 L11.7896,4.7468 C11.9316,5.0488 12.2146,5.2608 12.5436,5.3108 L17.0026,5.9918 C17.8076,6.1148 18.1356,7.0958 17.5666,7.6788 L14.2786,11.0488 C14.0576,11.2768 13.9566,11.5958 14.0076,11.9078 L14.7746,16.6138 C14.9096,17.4378 14.0346,18.0538 13.3046,17.6498 L9.4106,15.4978 C9.1086,15.3308 8.7436,15.3308 8.4426,15.4978 L4.5486,17.6498 C3.8176,18.0538 2.9426,17.4378 3.0776,16.6138 L3.8446,11.9078 C3.8956,11.5958 3.7956,11.2768 3.5736,11.0488 L0.2856,7.6788 C-0.2834,7.0958 0.0456,6.1148 0.8506,5.9918 L5.3086,5.3108 C5.6386,5.2608 5.9206,5.0488 6.0626,4.7468 L8.0206,0.5748 C8.3806,-0.1922 9.4716,-0.1922 9.8316,0.5748" id="Fill-1"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                    @elseif($item == 'filled_star')
                                                    <svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg filled_star">
                                                        <defs>
                                                            <linearGradient x1="50%" y1="-1.12155278%" x2="50%" y2="98.8784472%" id="linearGradient-1">
                                                                <stop stop-color="#FDD800" offset="0%"></stop>
                                                                <stop stop-color="#EBC10E" offset="100%"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <path d="M9,0 C8.588,0 8.176,0.219 7.972,0.656 L7.972,0.656 L6.099,4.665 C5.939,5.009 5.618,5.251 5.243,5.307 L5.243,5.307 L0.965,5.964 C0.051,6.105 -0.321,7.224 0.324,7.889 L0.324,7.889 L3.49,11.149 C3.742,11.408 3.857,11.773 3.798,12.13 L3.798,12.13 L3.06,16.673 C2.942,17.403 3.514,17.99 4.164,18 L4.164,18 L4.2,18 C4.376,17.997 4.557,17.952 4.73,17.855 L4.73,17.855 L8.45,15.789 C8.792,15.599 9.207,15.599 9.549,15.789 L9.549,15.789 L13.27,17.855 C13.444,17.952 13.625,17.997 13.801,18 L13.801,18 L13.836,18 C14.486,17.99 15.058,17.403 14.939,16.673 L14.939,16.673 L14.202,12.13 C14.143,11.773 14.258,11.408 14.51,11.149 L14.51,11.149 L17.676,7.889 C18.321,7.224 17.949,6.105 17.035,5.964 L17.035,5.964 L12.757,5.307 C12.382,5.251 12.062,5.009 11.901,4.665 L11.901,4.665 L10.028,0.656 C9.824,0.219 9.412,0 9,0 Z" id="Fill-1" fill="url(#linearGradient-1)"></path>
                                                        </g>
                                                    </svg>
                                                    @elseif($item == 'half_filled_star')
                                                    <svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg half_filled_star">
                                                        <defs>
                                                            <linearGradient x1="49.9996654%" y1="-1.12155278%" x2="49.9996654%" y2="98.8784472%" id="linearGradient-1">
                                                                <stop stop-color="#FDD800" offset="0%"></stop>
                                                                <stop stop-color="#EBC10E" offset="100%"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <path d="M9,0 C8.589,0 8.176,0.219 7.972,0.656 L7.972,0.656 L6.099,4.665 C5.939,5.009 5.618,5.251 5.243,5.307 L5.243,5.307 L0.965,5.964 C0.051,6.105 -0.321,7.224 0.324,7.889 L0.324,7.889 L3.49,11.149 C3.742,11.408 3.857,11.773 3.798,12.13 L3.798,12.13 L3.06,16.673 C2.942,17.403 3.514,17.99 4.164,18 L4.164,18 L4.2,18 C4.376,17.997 4.557,17.952 4.73,17.855 L4.73,17.855 L8.451,15.789 C8.621,15.694 8.811,15.646 9,15.646 L9,15.646 L9,0 Z" id="Fill-1" fill="url(#linearGradient-1)"></path>
                                                            <path d="M8.9999,15.6465 C9.1889,15.6465 9.3779,15.6935 9.5489,15.7885 L13.2699,17.8545 C14.0989,18.3155 15.0919,17.6125 14.9389,16.6725 L14.2019,12.1295 C14.1439,11.7725 14.2579,11.4085 14.5099,11.1495 L17.6759,7.8895 C18.3219,7.2245 17.9489,6.1045 17.0349,5.9645 L12.7569,5.3075 C12.3829,5.2505 12.0619,5.0095 11.8999,4.6645 L10.0279,0.6565 C9.8239,0.2185 9.4119,0.0005 8.9999,0.0005 L8.9999,15.6465 Z" id="Fill-4" fill="#BBC2BD"></path>
                                                        </g>
                                                    </svg>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @endif
                                    @if(!empty($data['traits']['img']))
                                    <div class="dp-fx fx-js-ct mg-t-20">
                                        <amp-img src="{!! $data['traits']['img'] !!}" alt="{!! $data['traits']['title'] !!}" width="200" height="200" layout="fixed"></amp-img>
                                    </div>
                                    @endif
                                    @if(!empty($data['traits']['card']['text']))
                                    @foreach($data['traits']['card']['text'] as $item)
                                    <div class="ft-sec md-sec-reg-4 sec-reg-4 mg-t-10">{!! $item !!}</div>
                                    @endforeach
                                    @endif
                                    @if(!empty($data['traits']['list']))
                                    <div class="fx-row neg-mg-lr-px-10-ac cl-primary">
                                        @foreach($data['traits']['list'] as $item)
                                        <div class="col-12 col-md-6 pd-l-10 pd-r-10 mg-t-20">
                                            <div class="ft-ter md-ter-bd-4 ter-bd-4">{!! $item['title'] !!}</div>
                                            @foreach($item['data'] as $dItem)
                                            <div class="pos-rel mg-t-10">
                                                <div class="ft-sec md-sec-reg-4 sec-reg-4 pd-t-6 pd-b-6 pd-l-20">{!! $dItem !!}</div>
                                                <div class="gd-tint-white-white0-white100-half-1 pos-abs lt-init bt-init tp-init wd-px-90"></div>
                                            </div>
                                            @endforeach
                                        </div>
                                        @endforeach
                                    </div>
                                    @endif
                                    @if(!empty($data['traits']['links']))
                                        <div class="fx-row neg-mg-lr-px-10-ac">
                                            @foreach($data['traits']['links'] as $item)
                                            <div class="pd-l-10 pd-r-10 col-12 mg-t-30 {{(sizeof($data['traits']['links']) > 1) ? 'col-md-6' : ''}}">
                                                {{-- <a class="dp-bl wd-full ft-pr bd-6 md-bd-6 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{!! $item['url'] !!}" target="_blank" aria-label="Button Link">
                                                    <div>{!! $item['title'] !!}</div>
                                                    <span class="nb-icon-external-link fz-20 mg-l-6"></span>
                                                </a> --}}
                                                <a class="dp-bl wd-full ft-ter md-ter-bd-2 ter-bd-2 cl-primary dp-fx fx-js-ct fx-al-ct tx-decoration-none" href="{!! $item['url'] !!}" target="_blank" aria-label="Button Link">
                                                    <div>{!! $item['title'] !!}</div>
                                                    {!! $externalLinkSvg !!}
                                                </a>
                                            </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                    @else
                        @if(array_get($data, 'tag', '') == 'whydoesitmatter')
                            <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at mg-t-20 pd-l-pr-4" data-id="{{$data['htmlId']}}">
                                <div class="mg-t-10">
                                    <div class="slug dp-fx">
                                        <span class="z-index-1 bg-venetian-red evt-connector" data-hooked="{{$data['htmlId']}}"></span>
                                        <div class="cl-venetian-red ft-ter md-ter-bd-3 ter-bd-3 wd-full tx-ct">{!! $data['subtitle'] !!}</div>
                                    </div>
                                    <div class="mg-t-4 ft-sec md-sec-bd-5 sec-bd-5 tx-ct"><u>{!! $data['title'] !!}</u></div>
                                </div>
                            </div>
                            @if(!empty(array_get($data, 'html', [])))
                            <ul class="pd-l-pr-4 pd-b-20 ft-sec md-sec-reg-4 sec-reg-4 card-point">
                                @foreach($data['html'] as $item)
                                <li class="mg-t-20">{!! $item !!}</li>
                                @endforeach
                            </ul>
                            @endif
                        @else
                            @if($data['subtitle'] != '')
                            <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at mg-t-20 pd-l-pr-4" data-id="{{$data['htmlId']}}">
                                <div class="slug dp-fx fx-al-ct">
                                    <span class="z-index-1 bg-lt evt-connector" data-hooked="{{$data['htmlId']}}"></span>
                                    <div class="cl-lt ft-ter md-ter-bd-2 ter-bd-2 subtitle-margin">{!! $data['subtitle'] !!}</div>
                                </div>
                            </div>
                            @endif
                            @if($data['title'] != '')
                            <div class="mg-t-4 pd-l-pr-4 ft-sec md-sec-bd-6 sec-bd-6 cl-night-rider">{!! $data['title'] !!}</div>
                            @endif

                            @if(!empty(array_get($data, 'html', [])))
                            <p class="ft-sec md-sec-reg-4 sec-reg-4 pd-l-pr-4 card-point cl-night-rider">
                                @foreach($data['html'] as $item)
                                <span class="dp-bl mg-t-10">{!! $item !!}</span>
                                @endforeach
                            </p>
                            @endif
                        @endif
                    @endif
                    @break
                    @default
                        @if($data['subtitle'] != '')
                        @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                        <div id="{!! $data['jumpLinkId'] !!}" class="tx-decoration-none jump-at mg-t-10 pd-l-pr-4" data-id="{{$data['htmlId']}}">
                        @endif
                            <div class="slug dp-fx fx-al-ct">
                                <span class="z-index-1 bg-lt evt-connector" data-hooked="{{$data['htmlId']}}"></span>
                                <div class="cl-lt ft-ter md-ter-bd-2 ter-bd-2 subtitle-margin">{!! $data['subtitle'] !!}</div>
                            </div>
                        @if((isset($data['jumpLinkId'])) && (!empty($data['jumpLinkId'])))
                        </div>
                        @endif
                        @endif
                        @if($data['title'] != '')
                        <div class="mg-t-4 pd-l-pr-4 ft-sec md-sec-bd-6 sec-bd-6 cl-night-rider">{!! $data['title'] !!}</div>
                        @endif

                        @if(!empty($data['title']))
                        @if(!empty(array_get($data, 'slides', [])))
                        <div class="mg-t-10 pd-l-pr-4">
                            <amp-carousel width="1080" height="610" layout="responsive" type="slides" autoplay delay="5000" role="region" aria-label="{!! $data['title']!!} Slideshow">
                                @foreach($data['slides'] as $item)
                                <figure>
                                    <amp-img src="{!! $item['ximg'] !!}" width="1080" height="610" layout="responsive" alt="{!! $item['title'] !!}"></amp-img>
                                    @if(!empty(array_get($item, 'caption', '')))
                                    <figcaption class="cl-lt pd-t-4 ft-sec md-sec-reg-2 sec-reg-2">{!! $item['caption'] !!}</figcaption>
                                    @endif
                                </figure>
                                @endforeach
                            </amp-carousel>
                        </div>
                        {{-- @elseif((!empty($data['ximg'])) && (empty($data['video']['src'])))
                        <div class="mg-t-10 pd-l-pr-4">
                            <figure>
                                <div class="pos-rel">
                                    <amp-img src="{{$data['ximg']}}" alt="{{$data['title']}}" layout="responsive" width="1080" height="610"></amp-img>
                                    @if(isset($data['imagecredit']) && (!is_null($data['imagecredit'])))
                                    @include('web-components::entities.imageCredits', ['data' => $data['imagecredit'], 'type' => $pageType, 'classList' => ''])
                                    @endif
                                </div>
                                @if(!empty(array_get($data, 'caption', '')))
                                <figcaption class="cl-lt pd-t-4 ft-sec md-sec-reg-2 sec-reg-2">{!! $data['caption'] !!}</figcaption>
                                @endif
                            </figure>
                        </div> --}}
                        @endif
                        <?php
                            if(isset($data['video']['videoId']) && !empty($data['video']['videoId'])){
                                $parts = parse_url($data['video']['videoId']);
                                parse_str($parts['query'], $query);
                                $videoIdStr = $parts['path'];
                                $paramsStr = collect($query)->map(function($item, $key){if(empty($item)) return ''; return "data-param-$key='$item'"; })->implode(' ');
                            }
                        ?>
                        @if(!empty(array_get($data, 'video.src', '')))
                        <div class="mg-t-10 pd-b-10 pd-l-pr-4">
                            @if(!empty(array_get($data, 'video.id', '')))
                            <amp-youtube width="1080" height="610" layout="responsive" data-videoid="{{$data['video']['id']}}" {{$data['video']['qparams']}}></amp-youtube>
                            @else
                            <amp-youtube width="1080" height="610" layout="responsive" data-videoid="{{$videoIdStr}}" {{$paramsStr}}></amp-youtube>
                            @endif
                        </div>
                        @endif
                        @endif
                        @if(!empty(array_get($data, 'html', [])))
                        <p class="ft-sec md-sec-reg-4 sec-reg-4 pd-l-pr-4 card-point cl-night-rider">
                            @foreach($data['html'] as $item)
                            <span class="dp-bl mg-t-10">{!! $item !!}</span>
                            @endforeach
                        </p>
                        @endif
                        @if($data['conf']['cardtype'] == 'embed' && isset($data['conf']['type']))
                        <div class="mg-t-20 pd-l-pr-4">
                            @include('web-components::embeddedPosts.' . $data['conf']['type'], ['data' => $data['conf'], 'type' => $pageType, 'classList' => ''])
                        </div>
                        @endif
                    @break
                @endswitch
            @break
        @endswitch
    @break
    @default
    @break
@endswitch