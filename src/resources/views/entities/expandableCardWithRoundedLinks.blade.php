<?php $componentId = array_get($data, 'id', floor(time()/(30*86400))); ?>
@if(!empty(array_get($data, 'list.data', [])))
<div class="bg-primary  pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 {{isset($classList) ? $classList : ''}}">
    <div class="ft-ter md-ter-bd-4 ter-bd-4 pd-t-20">{!! $data['title'] !!}</div>
    <div class="pos-rel">
        @if(count($data['list']['data']) > 4)
        <input type="checkbox" id="expand-{{$componentId}}" class="pos-abs bt-init lt-init rt-init hidden" />
        <label for="expand-{{$componentId}}" class="pos-abs bt-init lt-init rt-init pd-t-20 pd-b-20 dp-fx fx-al-ct fx-js-ct trigger-expandable cs-ptr">
            <div class="bg-primary br-rd-pr-50 dp-fx fx-al-ct fx-basis-40px fx-js-ct pd-b-10 pd-l-10 pd-r-10 pd-t-10 wd-ht-px-40">
                <span class="nb-icon-angle-down fz-18 mg-t-6 cl-link"></span>
            </div>
        </label>
        @endif
        <div class="mg-t-20 expandable">
            @foreach($data['list']['data'] as $item)
            @include('web-components::collections.links.rounded', [ 'data' => $item, 'classList' => 'mg-r-10 mg-t-10', 'type' => $type])
            @endforeach
        </div>
    </div>
</div>
@endif
@section('css')
@parent
@if($type != 'amp')
<style type="text/css">
@endif
    #expand-{{$componentId}}~.trigger-expandable~.expandable {
        height: 200px;
        overflow: hidden;
    }

    .trigger-expandable {
        background-image: linear-gradient(180deg, rgba(247, 247, 247, 0.00) 0%, #F7F7F7 100%);
    }

    #expand-{{$componentId}}:checked~.trigger-expandable {
        display: none;
    }

    #expand-{{$componentId}}:checked~.trigger-expandable~.expandable {
        height: auto;
        overflow: auto;
    }
@if($type != 'amp')
</style>
@endif
@stop