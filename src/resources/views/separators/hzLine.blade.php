<div class="{{isset($classList) ? $classList : ''}} ft-pr">
    <div class="ft-ter ter-bd-4 md-ter-bd-4">{!! $data['title'] !!}</div>
    <div class="mg-t-4 br-t-2p-pale-lavender"></div>
    <div class="cl-lt-grey nb-icon-drop-more" style="margin-top:-18px;margin-left:40px;font-size:50px;line-height:0"></div>
</div>