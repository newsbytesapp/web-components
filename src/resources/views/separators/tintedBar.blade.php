<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
<div class="{{isset($classList) ? $classList : ''}} ht-px-90 br-rd-tl-tr-10px gd-tint-white-solitude0-white100" data-id="{{$componentId}}">
    <div class="dp-fx fx-al-ct ht-full fx-js-bw pd-l-14 pd-l-md-24 pd-r-14 pd-r-md-14">
        <div class="cl-ink-dk bd-4">{!! array_get($data, 'title', '') !!}</div>
    </div>
</div>