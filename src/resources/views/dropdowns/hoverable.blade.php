<?php
$linkType = array_get($data, 'type', 'internal');
?>
@switch($data['size'])
@case('fixed')
<div class="dp-ib pos-rel drdown-selector-hoverable ft-pr {{isset($classList) ? $classList : ''}}">
    @if(!empty($data['selector']['url']))<a class="dp-bl bg-transparent cl-ink-dk ff-primary cs-ptr label-url" href="{!! $data['selector']['url'] !!}">@endif
        <div class="reg-2 md-reg-2 hoverable-label ff-primary {{$data['selector']['label'] == '' ? 'hidden' : ''}}">{!! $data['selector']['label'] !!}</div>
        @if($data['selector']['title'] != '')
        <div class="dp-fx">
            <span class="bd-6 hoverable-title md-bd-6">{!! $data['selector']['title'] !!}</span>
            <span class="nb-icon-drop-more fz-16 mg-l-6"></span>
        </div>
        @endif
    @if(!empty($data['selector']['url']))</a>@endif
    @if(count($data['content']['data']) > 0)
    <div class="hidden pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 wd-full box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-1 ent-slide-down-pd drdown-box {{$data['content']['alignment'] == 'right' ? 'rt-init' : ''}}">
        @foreach($data['content']['data'] as $item)
        <a class="hoverable-item dp-bl pd-l-20 pd-t-12 pd-b-12 pd-r-20 cl-lt reg-3 md-reg-3 black-cl-hover alice-blue-bg-hover" href="{!! $item['url'] !!}" target="{{$data['linkTargetAttr']}}" {!! $linkType == "external" ? 'rel="_noopener"' : '' !!}>{!! $item['name'] !!}</a>
        @endforeach
    </div>
    @endif
</div>
@break
@default
<div class="dp-bl wd-full pos-rel drdown-selector-hoverable ft-pr {{isset($classList) ? $classList : ''}}">
    <a class="dp-bl bg-transparent cl-ink-dk ff-primary cs-ptr" href="{!! $data['selector']['url'] !!}">
        <div class="reg-2 md-reg-2 ff-primary {{$data['selector']['label'] == '' ? 'hidden' : ''}}">{!! $data['selector']['label'] !!}</div>
        @if($data['selector']['title'] != '')
        <div class="dp-fx">
            <span class="bd-6 md-bd-6">{!! $data['selector']['title'] !!}</span>
            <span class="nb-icon-drop-more fz-16 mg-l-6"></span>
        </div>
        @endif
    </a>
    @if(count($data['content']['data']) > 0)
    <div class="hidden pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 wd-full box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-1 ent-slide-down-pd drdown-box {{$data['content']['alignment'] == 'right' ? 'rt-init' : ''}}">
        @foreach($data['content']['data'] as $item)
        <a class="hoverable-item dp-bl pd-l-20 pd-t-12 pd-b-12 pd-r-20 cl-lt reg-3 md-reg-3 black-cl-hover alice-blue-bg-hover" href="{!! $item['url'] !!}" target="{{$data['linkTargetAttr']}}" {!! $linkType == "external" ? 'rel="_noopener"' : '' !!}>{!! $item['name'] !!}</a>
        @endforeach
    </div>
    @endif
</div>
@break
@endswitch
