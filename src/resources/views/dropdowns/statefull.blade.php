<?php
$fixedSizeId = floor(time()/(30*86400)); //str_random(6)
$componentId = array_get($data, 'id', $fixedSizeId);
?>
@switch($type)
    @case('amp')
    @break
    @default
        @switch($data['size'])
            @case('fixed')
            <div class="dp-ib pos-rel ovr-initial drdown-parent ft-pr">
                <button class="dp-ib bg-primary cl-ink-dk br-rd-10 pd-l-20 pd-r-20 pd-l-md-30 pd-r-md-30 pd-t-12 pd-b-12 ff-primary alice-blue-bg-hover drdown-selector cs-ptr" id="fxd-sf-{{$componentId}}">
                    <div class="dp-fx">
                        <span class="bd-6 md-bd-6">{!! $data['selector']['title'] !!}</span>
                        <span class="nb-icon-drop-more fz-16 mg-l-6"></span>
                    </div>
                </button>
                <div class="hidden pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 wd-full box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-2 drdown-box {{$data['content']['alignment'] == 'right' ? 'rt-init' : ''}}" data-status="off" data-animation-entrance="ent-slide-down-mg" data-animation-exit="ext-slide-up-mg" data-type="statefull">
                    @foreach($data['content']['data'] as $item)
                    @if(isset($item['skip']) && $item['skip'])
                    @continue
                    @endif
                    <label class="dp-bl pd-l-44 pd-t-12 pd-b-12 pd-r-20 filter-checkbox drdown-checkbox cl-lt reg-3 md-reg-3 black-cl-hover alice-blue-bg-hover pos-rel cs-ptr" data-filter-for="{{$item['key']}}">{!! $item['name'] !!}
                        <input type="checkbox" name="filter-checkboxes" class="wd-ht-0 cs-ptr pos-abs opacity-0" />
                        <span class="checkmark br-rd-3"></span>
                    </label>
                    @endforeach
                </div>
            </div>
            @break
            @default
            <div class="dp-bl wd-full pos-rel ovr-initial drdown-parent ft-pr">
                <button class="dp-bl bg-primary cl-ink-dk br-rd-10 pd-l-16 wd-full pd-l-md-20 pd-t-14 pd-b-14 ff-primary drdown-selector alice-blue-bg-hover drdown-selector cs-ptr" id="fre-sf-{{$componentId}}">
                    <div class="dp-fx">
                        <span class="bd-6 md-bd-6">{!! $data['selector']['title'] !!}</span>
                        <span class="nb-icon-drop-more fz-16 mg-l-6"></span>
                    </div>
                </button>
                <div class="hidden pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-2 drdown-box {{$data['content']['alignment'] == 'right' ? 'rt-init' : ''}}" data-status="off" data-animation-entrance="ent-slide-down-mg" data-animation-exit="ext-slide-up-mg" data-type="statefull">
                    @foreach($data['content']['data'] as $item)
                    @if(isset($item['skip']) && $item['skip'])
                    @continue
                    @endif
                    <label class="dp-bl pd-l-44 pd-t-12 pd-b-12 pd-r-20 filter-checkbox drdown-checkbox cl-lt reg-3 md-reg-3 black-cl-hover alice-blue-bg-hover pos-rel cs-ptr">{!! $item['name'] !!}
                        <input type="checkbox" name="filter-checkboxes" class="wd-ht-0 cs-ptr pos-abs opacity-0" />
                        <span class="checkmark br-rd-3"></span>
                    </label>
                    @endforeach
                </div>
            </div>    
            @break
        @endswitch
    @break
@endswitch