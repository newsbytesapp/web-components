<?php
    $linkType = array_get($data, 'type', 'internal');
?>
@switch(array_get($data, 'size', 'free'))
@case('fixed')
    <div class="dp-ib pos-rel drdown-selector-hoverable ft-pr {{isset($classList) ? $classList : ''}}">
        @if(array_get($data, 'selector.linkable', false))
        <a class="dp-bl bg-primary pd-t-10 pd-b-10 pd-l-14 pd-r-14 br-rd-10 cl-ink-dk ff-primary cs-ptr selector-url alice-blue-bg-hover  {{(array_get($data, 'selected', false)) ? 'bg-alice-blue selected' : ''}}" href="{!! $data['selector']['url'] !!}">
        @else
        <div class="dp-bl bg-primary pd-t-10 pd-b-10 pd-l-14 pd-r-14 br-rd-10 cl-ink-dk ff-primary cs-ptr alice-blue-bg-hover {{(array_get($data, 'selected', false)) ? 'bg-alice-blue selected' : ''}}">
        @endif
        @if($data['selector']['label'] != '')
        <div class="reg-2 md-reg-2 selector-label">{!! $data['selector']['label'] !!}</div>
        @endif
        @if($data['selector']['title'] != '')
            <div class="dp-fx">
                <span class="bd-6 hoverable-title md-bd-6 selector-title">{!! $data['selector']['title'] !!}</span>
                <span class="nb-icon-drop-more fz-16 mg-l-6"></span>
            </div>
        @endif
        @if(array_get($data, 'selector.linkable', false))
        </a>
        @else
        </div>
        @endif
        @if(sizeof($data['content']['data']) > 0)
        <div class="hidden pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 wd-full box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-1 ent-slide-down-pd drdown-box {{(array_get($data, 'content.alignment', 'left')) == 'right' ? 'rt-init' : ''}}">
            @foreach($data['content']['data'] as $item)
            <a class="hoverable-item dp-bl pd-l-20 pd-t-12 pd-b-12 pd-r-20 cl-lt reg-3 md-reg-3 black-cl-hover alice-blue-bg-hover" href="{!! $item['url'] !!}" {!! (array_get($data, 'linkTargetAttr', '')) == '' ? '' : 'target="_blank"' !!} {!! $linkType == "external" ? 'rel="_noopener"' : '' !!}>{!! $item['name'] !!}</a>
            @endforeach
        </div>
        @endif
    </div>
@break
@default
    <div class="dp-bl wd-full drdown-selector-hoverable ft-pr {{isset($classList) ? $classList : ''}}">
        @if(array_get($data, 'selector.linkable', false))
        <a class="dp-bl bg-primary pd-t-10 pd-b-10 pd-l-14 pd-r-14 br-rd-10 cl-ink-dk ff-primary cs-ptr selector-url alice-blue-bg-hover  {{(array_get($data, 'selected', false)) ? 'bg-alice-blue selected' : ''}}" href="{!! $data['selector']['url'] !!}">
        @else
        <div class="dp-bl bg-primary pd-t-10 pd-b-10 pd-l-14 pd-r-14 br-rd-10 cl-ink-dk ff-primary cs-ptr alice-blue-bg-hover {{(array_get($data, 'selected', false)) ? 'bg-alice-blue selected' : ''}}">
        @endif
        @if($data['selector']['label'] != '')
        <div class="reg-2 md-reg-2 selector-label">{!! $data['selector']['label'] !!}</div>
        @endif
        @if($data['selector']['title'] != '')
            <div class="dp-fx">
                <span class="bd-6 hoverable-title md-bd-6 selector-title">{!! $data['selector']['title'] !!}</span>
                <span class="nb-icon-drop-more fz-16 mg-l-6"></span>
            </div>
        @endif
        @if(array_get($data, 'selector.linkable', false))
        </a>
        @else
        </div>
        @endif
        @if(sizeof($data['content']['data']) > 0)
        <div class="hidden pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 wd-full box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-1 ent-slide-down-pd drdown-box {{(array_get($data, 'content.alignment', 'left')) == 'right' ? 'rt-init' : ''}}">
            @foreach($data['content']['data'] as $item)
            <a class="hoverable-item dp-bl pd-l-20 pd-t-12 pd-b-12 pd-r-20 cl-lt reg-3 md-reg-3 black-cl-hover alice-blue-bg-hover" href="{!! $item['url'] !!}" {!! (array_get($data, 'linkTargetAttr', '')) == '' ? '' : 'target="_blank"' !!} {!! $linkType == "external" ? 'rel="_noopener"' : '' !!}>{!! $item['name'] !!}</a>
            @endforeach
        </div>
        @endif
    </div>
@break
@endswitch
