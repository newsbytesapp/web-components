<?php
$linkType = array_get($data, 'type', 'internal');
$fixedSizeId = floor(time()/(30*86400)); //str_random(6)
$componentId = array_get($data, 'id', $fixedSizeId);
?>
@switch($type)
    @case('amp')
        @switch($data['size'])
        @case('fixed')
        <div class="dp-ib pos-rel ovr-initial ft-pr">
            <input type="checkbox" id="fxd-sl-amp-{{$componentId}}" name="stateless-dropdown-amp" class="drdown-stateless-amp hidden" />
            <label for="fxd-sl-amp-{{$componentId}}" class="dp-bl wd-full bg-primary cl-ink-dk br-rd-10 pd-l-20 pd-r-20 pd-t-12 pd-b-12 ff-primary alice-blue-bg-hover cs-ptr">
                <div class="ft-pr reg-3 ft-pr md-reg-3 ff-primary cl-lt {{$data['selector']['label'] == '' ? 'hidden' : ''}}">{!! $data['selector']['label'] !!}</div>
                    <div class="dp-fx">
                        <span class="ft-pr bd-6 ft-pr md-bd-6">{!! $data['selector']['title'] !!}</span>
                        <span class="nb-icon-drop-more fz-16 mg-l-6"></span>
                    </div>
            </label>
            <div class="hidden mg-t-10 pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-2 drdown-box {{$data['content']['alignment'] == 'right' ? 'rt-init' : ''}}">
                @foreach($data['content']['data'] as $item)
                <a class="dp-bl pd-l-20 pd-t-12 pd-b-12 pd-r-20 cl-lt ft-pr reg-3 ft-pr md-reg-3 black-cl-hover alice-blue-bg-hover" href="{!! $item['url'] !!}" {!! $data['linkTargetAttr'] == "_blank" ? 'target="_blank"' : '' !!}>{!! $item['name'] !!}</a>
                @endforeach
            </div>
        </div>
        @break
        @default
        <div class="dp-bl wd-full pos-rel ovr-initial ft-pr">
            <input type="checkbox" id="fre-sl-amp-{{$componentId}}" name="stateless-dropdown-amp" class="drdown-stateless-amp hidden" />
            <label for="fre-sl-amp-{{$componentId}}" class="dp-bl wd-full bg-primary cl-ink-dk br-rd-10 pd-l-20 pd-r-20 pd-t-12 pd-b-12 ff-primary alice-blue-bg-hover cs-ptr">
                <div class="reg-3 md-reg-3 ff-primary cl-lt {{$data['selector']['label'] == '' ? 'hidden' : ''}}">{!! $data['selector']['label'] !!}</div>
                    <div class="dp-fx">
                        <span class="bd-6 md-bd-6">{!! $data['selector']['title'] !!}</span>
                        <span class="nb-icon-drop-more fz-16 mg-l-6"></span>
                    </div>
            </label>
            <div class="hidden mg-t-10 pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-2 drdown-box {{$data['content']['alignment'] == 'right' ? 'rt-init' : ''}}">
                @foreach($data['content']['data'] as $item)
                <a class="dp-bl pd-l-20 pd-t-12 pd-b-12 pd-r-20 cl-lt reg-3 md-reg-3 black-cl-hover alice-blue-bg-hover" href="{!! $item['url'] !!}" {!! $data['linkTargetAttr'] == "_blank" ? 'target="_blank"' : '' !!}>{!! $item['name'] !!}</a>
                @endforeach
            </div>
        </div>
        @break
        @endswitch
    @break
    @default
        @switch($data['size'])
            @case('fixed')
            <div class="dp-ib pos-rel ovr-initial ft-pr drdown-parent">
                <button class="dp-ib bg-primary cl-ink-dk br-rd-10 pd-l-20 pd-r-20 pd-l-md-30 pd-r-md-30 pd-t-12 pd-b-12 ff-primary alice-blue-bg-hover drdown-selector cs-ptr" id="fxd-sl-{{$componentId}}">
                    <div class="reg-3 md-reg-3 ff-primary cl-lt {{$data['selector']['label'] == '' ? 'hidden' : ''}}">{!! $data['selector']['label'] !!}</div>
                    <div class="dp-fx">
                        <span class="bd-6 md-bd-6">{!! $data['selector']['title'] !!}</span>
                        <span class="nb-icon-drop-more fz-16 mg-l-6"></span>
                    </div>
                </button>
                <div class="hidden pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 wd-full box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-2 drdown-box {{$data['content']['alignment'] == 'right' ? 'rt-init' : ''}}" data-status="off" data-animation-entrance="ent-slide-down-mg" data-animation-exit="ext-slide-up-mg" data-type="stateless">
                    @foreach($data['content']['data'] as $item)
                    <a class="dp-bl pd-l-20 pd-t-12 pd-b-12 pd-r-20 cl-lt reg-3 md-reg-3 black-cl-hover alice-blue-bg-hover" href="{!! $item['url'] !!}" target="{{$data['linkTargetAttr']}}" {!! $linkType == "external" ? 'rel="_noopener"' : '' !!}>{!! $item['name'] !!}</a>
                    @endforeach
                </div>
            </div>
            @break
            @default
            <div class="dp-bl wd-full pos-rel ovr-initial ft-pr drdown-parent">
                <button class="dp-bl bg-primary cl-ink-dk br-rd-10 pd-l-16 wd-full pd-l-md-20 pd-t-14 pd-b-14 ff-primary drdown-selector alice-blue-bg-hover" id="fre-sl-{{$componentId}}">
                    <div class="reg-3 md-reg-3 ff-primary cl-lt {{$data['selector']['label'] == '' ? 'hidden' : ''}}">{!! $data['selector']['label'] !!}</div>
                    <div class="dp-fx">
                        <span class="bd-6 md-bd-6">{!! $data['selector']['title'] !!}</span>
                        <span class="nb-icon-drop-more fz-16 mg-l-6"></span>
                    </div>
                </button>
                <div class="hidden pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-2 drdown-box {{$data['content']['alignment'] == 'right' ? 'rt-init' : ''}}" data-status="off" data-animation-entrance="ent-slide-down-mg" data-animation-exit="ext-slide-up-mg" data-type="stateless">
                    @foreach($data['content']['data'] as $item)
                    <a class="dp-bl pd-l-20 pd-t-12 pd-b-12 pd-r-20 cl-lt reg-3 md-reg-3 black-cl-hover alice-blue-bg-hover" href="{!! $item['url'] !!}" target="{{$data['linkTargetAttr']}}" {!! $linkType == "external" ? 'rel="_noopener"' : '' !!}>{!! $item['name'] !!}</a>
                    @endforeach
                </div>
            </div>    
            @break
        @endswitch
        
    @break
@endswitch