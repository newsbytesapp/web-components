<template type="text/x-jsrender" id="listItemSqrRecImg">
	{{for list['data']}}
	<div class="col-12 col-md-4 dp-fx pd-l-md-10 pd-r-md-10 mg-t-20 mg-t-md-30 list-item-sqrRecImg clickable-target cs-ptr" data-url="{{:url}}">
		<a class="col-4 col-md-1 fig-container--wd-ht-md-60" href="{{:url}}" target="{{:~root.list.linkTargetAttr}}">
			<figure class="lazy fig fig--bg-img-ct-cov-props ht-full-dac ht-full-md-ac asp-ratio-rect-ac asp-ratio-rect-md-dac" data-src="{{:thumb}}" title="{{:title}}">
				<img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="lazy hidden inset-img-hidden"/>
			</figure>
		</a>
		<div class="tx-container col-8 pd-l-10">
			<a class="ft-pr bd-6" href="{{:url}}" target="{{:~root.list.linkTargetAttr}}">{{:title}}</a>
		</div>
	</div>
	{{/for}}
</template>

<template type="text/x-jsrender" id="taggedListItemSqrRecImg">
	{{for list['data']}}
	<div class="col-12 col-md-4 dp-fx pd-l-md-10 pd-r-md-10 mg-t-20 list-item-sqrRecImg">
		<a href="{{:url}}" class="col-4 col-md-1 fig-container--wd-ht-md-60" target="{{:~root.list.linkTargetAttr}}">
			<figure class="lazy fig fig--bg-img-ct-cov-props ht-full-dac ht-full-md-ac asp-ratio-rect-ac asp-ratio-rect-md-dac" data-src="{{:thumb}}" title="{{:title}}">
				<img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="lazy hidden inset-img-hidden"/>
			</figure>
		</a>
		<div class="tx-container col-8 pd-l-10">
			<a href="{{:url}}" class="ft-pr md-bd-3 bd-3 dp-bl">{{:title}}</a>
			{{if ~_and('tagUrl', ~neql('tagUrl', ''))}}
			<a href="{{:tagUrl}}" class="mg-t-2 ft-ter md-ter-bd-2 ter-bd-2 cl-lt cl-{{:~tagColor(tag)}}-dk dp-ib">{{:tag}}</a>
			{{else}}
			<div class="mg-t-2 ft-ter md-ter-bd-2 ter-bd-2 cl-lt cl-{{:~tagColor(tag)}}-dk dp-bl">{{:tag}}</div>
			{{/if}}
		</div>
	</div>
	{{/for}}
</template>

<template type="text/x-jsrender" id="v2sqrRecImgHz">
	{{for list['data']}}
	<li class="col-12 col-md-4">
		<a href="{{:url}}" class="dp-fx" target="{{:~root.list.linkTargetAttr}}">
			<div class="col-4 col-lg-1 fig-container--wd-ht-lg-60">
				<figure class="lazy fig fig--bg-img-ct-cov-props ht-full-dac ht-full-lg-ac asp-ratio-rect-ac asp-ratio-rect-lg-dac" data-src="{{:thumb}}" title="{{:title}}">
					<img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="lazy hidden inset-img-hidden"/>
				</figure>
			</div>
			<div class="col-8 col-lg-10 pd-l-10 ft-pr bd-6 md-bd-6">
				<div>{{:title}}</div>
				<div class="cl-lt cl-{{:~tagColor(tag)}}-dk tag">{{:tag}}</div>
			</div>
		</a>
	</li>
	{{/for}}
</template>

<template type="text/x-jsrender" id="v2descriptivesmRecLgRecImg">
	{{for list['data']}}
		<li class="col-12">
			<a href="{{:url}}" class="fx-row" target="{{:~root.list.linkTargetAttr}}">
				<div class="col-12 col-md-4">
					<img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="lazy">
				</div>
				<div class="ft-pr bd-6 md-bd-6 col-12 col-md-8 pd-l-md-20">
					<div class="fx-row fx-al-ct mg-t-14 mg-t-md-0">
						<div class="cl-lt">{{:timestamp}}</div>
						{{if tag}}
						<div class="bg-lt bg-{{:~tagColor(tag)}}-dk mg-l-4 mg-r-4 br-rd-pr-50 wd-ht-px-3 fx-basis-3px"></div>
						<div class="cl-lt cl-{{:~tagColor(tag)}}-dk tag">{{:tag}}</div>
						{{/if}}
					</div>
					<div class="bd-3 md-bd-3 cl-ink-dk mg-t-4">{{:title}}</div>
					<p class="reg-2 md-reg-2 cl-lt mg-t-4">{{:description}}</p>
				</div>
			</a>
		</li>
	{{/for}}
</template>

<template type="text/x-jsrender" id="listItemLgSqrRecImg">
	{{for list['data']}}
	<a href="{{:url}}" class="dp-fx wd-full list-item-lgsqrImg pd-l-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac br-t-1p-white-smoke pd-t-10 pd-b-10 fx-al-ct" target="{{:~root.list.linkTargetAttr}}">
		<div class="wd-ht-px-70 pos-rel">
			<img data-src="{{:icon}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-full ht-full"/>
			<div class="ht-px-40 pos-abs bt-init lt-init wd-full gd-tint-white-white0-white100"></div>
		</div>
		<div class="tx-container pd-l-8">
			<div class="ft-pr md-bd-5 ft-pr bd-5">{{:title}}</div>
			<p class="ft-pr md-reg-3 ft-pr reg-3 cl-lt">{{:auxiliaryText}}</p>
		</div>
	</a>
	{{/for}}
</template>

<template type="text/x-jsrender" id="paginatedResponseListDescriptiveLgRecImg">
	{{if (~lengthOfArr(list.data))}}
	<div class="fx-row paginated-response-item pd-l-md-10 pd-r-md-10" id="{{:~root.list.data[0].id}}" data-text="{{if (list.panel.text)}}{{:list.panel.text}}{{/if}}" data-prev-api="{{if (list.panel.prev)}}{{:list.panel.prev.api}}{{/if}}" data-next-api="{{if (list.panel.next)}}{{:list.panel.next.api}}{{/if}}" data-prev-url="{{if (list.panel.prev)}}{{:list.panel.prev.url}}{{/if}}" data-next-url="{{if (list.panel.next)}}{{:list.panel.next.url}}{{/if}}">
		{{for list['data']}}
        <div class="col-12 dp-fx fx-al-st fx-wrap pd-l-md-10 pd-r-md-10 mg-t-20 clickable-target cs-ptr" data-url="{{:url}}">
            <a class="col-12 col-md-4" href="{{:url}}">
                <img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-full dp-bl"/>
			</a>
            <div class="tx-container col-12 col-md-8 pd-l-md-20">
                <p class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt mg-t-14 mg-t-md-0">{{:timestamp}}</p>
                <a class="ft-pr md-bd-5 bd-5 cl-ink-dk mg-t-4 dp-bl" href="{{:url}}">{{:title}}</a>
                <p class="ft-sec md-sec-reg-3 sec-reg-3 cl-lt mg-t-2">{{:description}}</p>
            </div>
		</div>
		{{/for}}
	</div>
	<hr data-id="{{:~root.list.data[0].id}}"/>
	{{/if}}
</template>

<template type="text/x-jsrender" id="listDescriptiveLgRecWithoutImg">
	{{for list['data']}}
	<div class="col-12 dp-fx fx-al-st fx-wrap mg-t-10 clickable-target cs-ptr pd-l-md-20 pd-r-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-t-20 pd-b-20 pd-t-md-18 pd-b-md-30 bg-body" title="{{:title}}" data-url="{{:url}}">
		<div class="tx-container col-12 pd-l-md-20">
			{{if ~_and(~_and(tag, ~neql(tag, '')), ~_and(tagUrl, ~neql(tagUrl, '')))}}
			<div class="dp-fx fx-al-ct">
				<span class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt">{{:timestamp}}</span>
				<div class="wd-ht-px-4 bg-acadia br-rd-pr-50 mg-l-4 mg-r-4"></div>
				{{if ~_and(tagColor, ~neql(tagColor, ''))}}
				<a class="cl-ink-dk cl-{{:~tagColor(tagColor)}}-dk ft-ter md-ter-bd-2 ter-bd-2 dp-fx fx-al-ct min-ht-px-50 min-wd-px-50" href="{{:tagUrl}}" title="{{:tag}}"><span>{{:tag}}</span></a>
				{{else}}
				<a class="cl-ink-dk ft-ter md-ter-bd-2 ter-bd-2 dp-fx fx-al-ct min-ht-px-50 min-wd-px-50" href="{{:tagUrl}}"><span>{{:tag}}</span></a>
				{{/if}}
			</div>
			{{else}}
			<div class="min-ht-px-50 min-wd-px-50 dp-fx fx-al-ct">
				<span class="ft-ter md-ter-bd-2 ter-bd-2 cl-lt">{{:timestamp}}</span>
			</div>
			{{/if}}
			<h2><a href="{{:url}}" class="ft-pr md-bd-5 bd-5 cl-ink-dk dp-bl" title="{{:title}}">{{:title}}</a></h2>
			<p class="ft-sec md-sec-reg-3 sec-reg-3 cl-lt mg-t-2">{{:description}}</p>
		</div>
	</div>
	{{/for}}
</template>

<template type="text/x-jsrender" id="listItemSmRecTile">
	{{for list['data']}}
	<div class="fx-row pd-l-md-10 pd-r-md-10 mg-t-20 cs-ptr play-video col-12 col-md-6 col-lg-4" data-video="{{:video.src}}">
		<div class="col-6 pos-rel">
			<img alt="{{:title}}" title="{{:title}}" class="lazy wd-full dp-bl" data-src="{{:thumb}}">
			<div class="cl-primary nb-icon-video-play-button tf-x-y-middle lt-half tp-half pos-abs fz-30"></div>
		</div>
		<div class="tx-container col-6 pd-l-10 ft-pr">
			<div class="bd-5 md-bd-5 cl-primary">{{:title}}</div>
		</div>
	</div>
	{{/for}}
</template>

<template type="text/x-jsrender" id="listItemComparison">
	{{for list.data}}
	<div class="bg-primary dp-fx fx-dr-col list-item-comparison wd-full br-t-1p-white-smoke pd-t-20 pd-b-20 fx-al-ct">
		<div class="col-12 col-md-8 col-lg-6 dp-fx fx-js-ct">
			<div class="col-4 dp-fx fx-js-end">
				<div class="mg-t-38">
					<a href="{{:left.url}}" target="{{:~root.list.linkTargetAttr}}" class="dp-fx fx-al-st">
						<div>
							<div class="ft-pr bd-6 ft-pr md-bd-6 tx-rt">{{:left.title}}</div>
							<div class="ft-pr reg-3 ft-pr reg-3 tx-rt cl-lt">{{:left.subtitle}}</div>
							{{for left['comparision']}}
							<div class="mg-t-4 {{if ~compareGT(#getIndex(), 0)}}mg-t-6{{/if}}">
								<div class="ft-pr bd-6 ft-pr md-bd-6 tx-rt">{{:title}}</div>
								<div class="ft-pr reg-3 ft-pr reg-3 tx-rt cl-lt">{{:subtitle}}</div>
							</div>
							{{/for}}
						</div>
						<img class="lazy wd-ht-px-32 mg-l-10" data-src="{{:left.icon}}" alt="{{:left.title}}" title="{{:left.title}}"/>
					</a>
				</div>
			</div>
			<div>
				<div class="br-rd-15 pd-l-10 pd-r-10 pd-t-4 pd-b-4 bg-lt-grey ft-pr reg-3 ft-pr md-reg-3 wht-sp-nowrap">{{:comparisionTitle}}</div>
				<div class="bd-6 md-bd-6 mg-t-18 tx-ct">vs</div>
			</div>
			<div class="col-4 dp-fx fx-js-st">
				<div class="mg-t-38">
					<a href="{{:right.url}}" target="{{:~root.list.linkTargetAttr}}" class="dp-fx fx-al-st">
						<img class="lazy wd-ht-px-32" data-src="{{:right.icon}}" alt="{{:right.title}}" title="{{:right.title}}"/>
						<div class="mg-l-10">
							<div class="ft-pr bd-6 ft-pr md-bd-6">{{:right.title}}</div>
							<div class="ft-pr reg-3 ft-pr reg-3 cl-lt">{{:right.subtitle}}</div>
							{{for right['comparision']}}
							<div class="mg-t-4 {{if ~compareGT(#getIndex(), 0)}}mg-t-6{{/if}}">
								<div class="ft-pr bd-6 ft-pr md-bd-6">{{:title}}</div>
								<div class="ft-pr reg-3 ft-pr reg-3 cl-lt">{{:subtitle}}</div>
							</div>
							{{/for}}
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="mg-t-10 tx-ct ft-pr reg-3 ft-pr md-reg-3">{{:conclusion}}</div>
		<div class="mg-t-10 fx-row neg-mg-lr-px-10-ac">
			{{for links}}
			<div class="pd-l-10 pd-r-10">
				{{if (size == 'fixed')}}
				<div class="dp-ib">
					<a class="dp-fx cl-link navy-blue-cl-hover" href="{{:url}}" target="{{:targetAttr}}">
						<span class="ft-pr bd-6 ft-pr md-bd-6">{{:title}}</span>
						{{if (type == 'external')}}
						<span class="nb-icon-external-link fz-20 mg-l-6"></span>
						{{else (type == 'internal')}}
						<span class="nb-icon-arrow-forward fz-20 mg-l-6"></span>
						{{/if}}
					</a>
				</div>
				{{else (size != 'fixed')}}
				<div class="dp-bl wd-full">
					<a class="dp-fx cl-link navy-blue-cl-hover" href="{{:url}}" target="{{:targetAttr}}">
						<span class="ft-pr bd-6 ft-pr md-bd-6">{{:title}}</span>
						{{if (type == 'external')}}
						<span class="nb-icon-external-link fz-20 mg-l-6"></span>
						{{else (type == 'internal')}}
						<span class="nb-icon-arrow-forward fz-20 mg-l-6"></span>
						{{/if}}
					</a>
				</div>
				{{/if}}
			</div>
			{{/for}}
		</div>
	</div>
	{{/for}}
</template>

<template type="text/x-jsrender" id="listsCompositeSmRecImgSqrRecImg">
	<section class="bg-primary  pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 lists-composite-smRecImg-sqrRecImg">
		<div class="fx-row composite-list">
			{{for list['data']}}
			{{if ~compareLT(#getIndex(), 4)}}
			<a href="{{:url}}" class="col-12 col-md-6 fx-row pd-l-md-10 pd-r-md-10 mg-t-20 list-item-smRecImg" target="{{:~root.list.linkTargetAttr}}">
				<div class="col-6">
					<img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="lazy dp-bl wd-full"/>
				</div>
				<div class="tx-container col-6 pd-l-10">
					<p class="ft-pr bd-6 ft-pr bd-md-6 cl-lt">{{:timestamp}}</p>
					<div class="ft-pr bd-5 ft-pr md-bd-5 cl-ink-dk mg-t-4">{{:title}}</div>
				</div>
			</a>
			{{/if}}
			{{if ~_and(~compareGT(#getIndex(), 3), ~compareLT(#getIndex(), 15))}}
			<a href="{{:url}}" class="col-12 col-md-4 dp-fx pd-l-md-10 pd-r-md-10 mg-t-20 mg-t-md-30 list-item-sqrRecImg" target="{{:~root.list.linkTargetAttr}}">
				<div class="col-4 col-md-1 fig-container--wd-ht-md-60">
					<figure class="lazy fig fig--bg-img-ct-cov-props ht-full asp-ratio-rect-ac asp-ratio-rect-md-dac" data-src="{{:thumb}}" title="{{:title}}">
						<img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="lazy hidden inset-img-hidden"/>
					</figure>
				</div>
				<div class="tx-container col-8 pd-l-10">
					<div class="ft-pr bd-6">{{:title}}</div>
				</div>
			</a>
			{{/if}}
			{{/for}}
		</div>
		{{if list['button']}}
		{{if list['button']['api']}}
		<div class="pd-t-30 pd-b-20">
			{{if list['button']['size'] == 'fixed'}}
				<button class="dp-ib bg-transparent cl-link br-rd-10 pd-l-30 pd-r-30 pd-l-md-40 pd-r-md-40 pd-t-14 pd-b-14 navy-blue-cl-hover ft-pr bd-6 ft-pr md-bd-6 ff-primary">{{:list['button']['title']}}</button>
			{{/if}}
			{{if list['button']['size'] == 'free'}}
				<button id="load-more-{{:~randomId(6)}}" class="dp-bl wd-full bg-transparent cl-link br-rd-10 tx-ct navy-blue-cl-hover ft-pr bd-6 ft-pr md-bd-6 ff-primary load-more" data-search-for=".lists-composite-smRecImg-sqrRecImg" data-append-to=".composite-list" data-api="{{:list['button']['api']}}" data-template="listItemSqrRecImg">{{:list['button']['title']}}</button>
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 64 64" style="enable-background:new 0 0 64 64;" xml:space="preserve" alt="Dots Wave Loader" title="Dots Wave Loader" class="dp-bl mg-lr-auto wd-ht-px-32 hidden dot-loader">
					<style type="text/css">
						.dot-loader .st0{fill:#064893}.bounce{animation:bounce 2.4s infinite}.bounce:nth-child(3n+0){animation-delay:.6s}.bounce:nth-child(3n+1){animation-delay:1.4s}.bounce:nth-child(3n+2){animation-delay:2.2s}@keyframes bounce{0%,25%,50%,75%,100%{transform:translateY(0)}40%{transform:translateY(-20px)}60%{transform:translateY(-12px)}}
					</style>
					<circle class="st0 bounce" cx="13.8" cy="32" r="5"/>
					<circle class="st0 bounce" cx="31.8" cy="32" r="5"/>
					<circle class="st0 bounce" cx="49.8" cy="32" r="5"/>
				</svg>
				<div class="tx-ct ft-pr bd-6 ft-pr md-bd-6 navy-blue-cl-hover cl-link animation-dur-scale-2 ent-slide-down flash-error hidden">Oops some error happened! Please try after some time.</div>
			{{/if}}
		</div>
		{{/if}}
    	{{/if}}
	</section>
</template>

<template type="text/x-jsrender" id="listItemTitleWithSqrImg">
	{{for events}}
	{{if ~eql((#getIndex() % 4), 0)}}
	<a href="{{:url}}" class="dp-fx mg-t-20 gd-tint-red-white-red100-white0 col-12 pd-t-20 pd-r-20 pd-l-20 pd-b-20">
		<div class="pd-r-10">
			<div class="wd-ht-px-80">
				{{if (video.id)}}
				<figure class="lazy fig fig--bg-img-ct-cov-props ht-full br-rd-4" data-src="{{:video.img}}" alt="{{:title}}">
					<img data-src="{{:video.img}}" alt="{{:title}}" class="lazy hidden inset-img-hidden"/>
				</figure>
				{{else}}
				<figure class="lazy fig fig--bg-img-ct-cov-props ht-full br-rd-4" data-src="{{:img}}" alt="{{:title}}">
					<img data-src="{{:img}}" alt="{{:title}}" class="lazy hidden inset-img-hidden"/>
				</figure>
				{{/if}}
			</div>
		</div>
		<div class="pd-l-10">
			<div class="ft-pr md-sec-bd-3 sec-bd-3">{{:title}}</div>
			{{if ~_and(tag, ~neql(tag, ''))}}
			<div class="dp-fx fx-al-ct pd-t-10">
				<span class="cl-venetian-red ft-ter md-ter-reg-1 ter-reg-1 dp-fx fx-al-ct">{{:tag}}</span>
				<span class="wd-ht-px-4 bg-acadia br-rd-pr-50 mg-l-8 mg-r-8"></span>
				<span class="ft-ter md-ter-reg-1 ter-reg-1 cl-lt">{{:timestamp}}</span>
			</div>
			{{else}}
			<div class="dp-fx fx-al-ct pd-t-10">
				<span class="ft-ter md-ter-reg-1 ter-reg-1 cl-lt">{{:timestamp}}</span>
			</div>
			{{/if}}
        </div>
	</a>
	{{else}}
	<div class="pd-l-20 pd-r-20 wd-full">
		<div class="mg-t-20 wd-full br-t-1p-gainsboro"></div>
	</div>
	<a href="{{:url}}" class="dp-fx mg-t-20 pd-l-20 pd-r-20">
		<div class="pd-r-10">
			<div class="ft-pr md-sec-bd-3 sec-bd-3">{{:title}}</div>
			{{if ~_and(tag, ~neql(tag, ''))}}
			<div class="dp-fx fx-al-ct pd-t-10">
				<span class="cl-venetian-red ft-ter md-ter-reg-1 ter-reg-1 dp-fx fx-al-ct">{{:tag}}</span>
				<span class="wd-ht-px-4 bg-acadia br-rd-pr-50 mg-l-8 mg-r-8"></span>
				<span class="ft-ter md-ter-reg-1 ter-reg-1 cl-lt">{{:timestamp}}</span>
			</div>
			{{else}}
			<div class="dp-fx fx-al-ct pd-t-10">
				<span class="ft-ter md-ter-reg-1 ter-reg-1 cl-lt">{{:timestamp}}</span>
			</div>
			{{/if}}
        </div>
	</a>
	{{/if}}
	{{/for}}
</template>

<template type="text/x-jsrender" id="eventCardsList">
	<section id="wrapper-events-list-{{:htmlId}}">
		{{if (tlseparator)}}
		<div class="mg-t-50 ft-pr">
			<a class="md-bd-3 bd-3" href="{{:tlseparator.url}}">{{:tlseparator.title}}</a>
			<div class="mg-t-4 br-t-2p-pale-lavender"></div>
			<div class="cl-lt-grey nb-icon-drop-more" style="margin-top:-18px;margin-left:40px;font-size:50px;line-height: 0px;"></div>
		</div>
		{{/if}}
		<section class="neg-mg-lr-px-10-lg-ac fx-row fx-al-st">
			<main class="col-12 col-lg-8 pd-l-lg-10 pd-r-lg-10">
				<article class="events-list-holder pos-rel banner-content">
					{{include tmpl='eventCoverCard'/}}
					{{include tmpl='eventCard'/}}
				</article>
				{{include tmpl='collectionCapsuleLinks'/}}
				{{if (questions.header)}}
				{{include tmpl='queryFeedback'/}}
				{{/if}}
			</main>
			<aside class="col-12 col-lg-4 pd-l-lg-10 pd-r-lg-10 sticky-sidebar banner-sidebar">
				{{include tmpl='listTaggedSqrRecImgVertical'/}}
				{{if (advt)}}
				<div class="hidden-in-touch wd-full">
					<div id="{{:advt.id}}"></div>
				</div>
				{{/if}}
			</aside>
		</section>
	</section>
</template>

<template type="text/x-jsrender" id="v2articleCard">
	{{for events}}
	<section id="wrapper-events-list-{{:htmlId}}">
		<div class="col-12 col-lg-8">
			<div class="embed-articleCard-banner-top hidden" data-id="{{:htmlId}}"></div>
		</div>
		{{if (tlseparator)}}
		<div class="next-article-section pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac mg-t-20" data-id="{{:htmlId}}">
			<a class="ft-ter ter-bd-4 md-ter-bd-4" href="{{:tlseparator.url}}">{{:tlseparator.title}}</a>
			<div class="mg-t-4 br-t-2p-pale-lavender"></div>
			<div class="cl-lt-grey nb-icon-drop-more" style="margin-top:-18px;margin-left:40px;font-size:50px;line-height: 0px;"></div>
		</div>
		{{/if}}
		<div class="embed-articleCard-breadcrumbs" data-id="{{:htmlId}}"></div>
		<section class="neg-mg-lr-px-10-lg-ac fx-row fx-al-st">
			<main class="col-12 col-lg-8 pd-l-lg-10 pd-r-lg-10">
				<article class="events-list-holder pos-rel banner-content" id="article-{{:htmlId}}">
					<div class="gd-tint-red-white-red100-white0 pd-b-18 mg-t-16 cover-card" data-id="{{:htmlId}}">
						{{if ~_and(slides, ~lengthOfArr(slides))}}
						<div class="mg-t-10 pd-l-20 pd-r-20">
							<section class="slide-show-parent visibility-hidden">
								<section class="slide-show default with-centerMode {{:htmlId}}" id="slideshow-{{:~randomId(6)}}">
									{{for slides}}
									<figure class="min-wd-px-100 mg-r-20">
										{{if ~eql(#getIndex(), 0)}}
										<img alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full cover-card-img" src="{{:img}}">
										{{else}}
										<img alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full" src="{{:img}}">
										{{/if}}
										{{if ~_and(caption, ~neql(caption, ''))}}
										<figcaption class="cl-lt pd-t-4 ft-sec md-sec-reg-2 sec-reg-2">{{:caption}}</figcaption>
										{{/if}}
									</figure>
									{{/for}}
								</section>
								<div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
							</section>
						</div>
						{{else ~_and(~neql(ximg, ''), ~eql(video.src, ''))}}
						<figure>
							<div class="pos-rel asp-ratio r-16-9">
							{{if ~_and(~neql(imageFallback, undefined), imageFallback)}}
							<picture>
								<source
									media="(max-width: 599px) and (min-device-pixel-ratio: 3),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 3),(max-width: 599px) and (min-resolution: 3dppx),(max-width: 599px) and (min-resolution: 288dpi),(min-width: 768px) and (min-device-pixel-ratio: 2),(min-width: 768px) and (-webkit-min-device-pixel-ratio: 2),(min-width: 768px) and (min-resolution: 3dppx),(min-width: 768px) and (min-resolution: 288dpi)"
									srcset="{{:ximg}}">
								<source
									media="(min-width: 1080px) and (min-device-pixel-ratio: 1),(min-width: 1080px) and (-webkit-min-device-pixel-ratio: 1),(min-width: 1080px) and (min-resolution: 2dppx),(min-width: 1080px) and (min-resolution: 192dpi)"
									srcset="{{:limg}}">
								<source
									media="(max-width: 599px) and (min-device-pixel-ratio: 2),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 2),(max-width: 599px) and (min-resolution: 2dppx),(max-width: 599px) and (min-resolution: 192dpi)"
									srcset="{{:img}}">
								<source
									media="(max-width: 599px) and (min-device-pixel-ratio: 2),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 2),(max-width: 599px) and (min-resolution: 2dppx),(max-width: 599px) and (min-resolution: 192dpi)"
									srcset="{{:img}}">
								<img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full lazy cover-card-img"/>
							</picture>
							<div class="lt-init rt-init wd-full">
								<div class="hidden-image-card-slug hidden"></div>
							</div>
							{{else}}
							<img data-src="{{:ximg}}" alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full lazy cover-card-img"/>
							<div class="lt-init rt-init wd-full">
								<div class="hidden-image-card-slug hidden"></div>
							</div>
							{{/if}}
								{{if (imagecredit)}}
								{{if ~_and(~neql(imagecredit, null), imagecredit.title)}}
									<a class="pos-abs tx-decoration-none image-credits" href="{{:imagecredit.url}}" target="_blank" rel="nofollow noopener">
										<div class="dp-fx fx-al-ct">
											<div class="cl-primary ft-ter md-ter-reg-1 ter-reg-1 mg-r-4">{{:imagecredit.title}}</div>
											<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 -2 17.52 16" height="10"><defs></defs><circle class="svg" cx="8.76" cy="8.26" r="2.87"/><path class="svg" d="M15.78,2.52H13.5L13,1a1.48,1.48,0,0,0-1.41-1H6.39A1.48,1.48,0,0,0,5,1L4.5,2.52H2.22a2,2,0,0,0-2,2V12a2,2,0,0,0,2,2H15.78a2,2,0,0,0,2-2V4.49A2,2,0,0,0,15.78,2.52ZM9,12.32a4.06,4.06,0,1,1,4.06-4.06A4.06,4.06,0,0,1,9,12.32Z" transform="translate(-0.24)"/></svg>
										</div>
									</a>
								{{/if}}
								{{/if}}
							</div>
							<!-- {{if ~_and(caption, ~neql(caption, ''))}}
							<figcaption class="cl-lt pd-t-4 pd-r-md-0 pd-l-md-0 ft-sec md-sec-reg-2 sec-reg-2">{{:caption}}</figcaption>
							{{/if}} -->
						</figure>
						{{/if}}
						{{if (video.src != '')}}
						<div class="wd-full pos-rel asp-ratio-rect-ac v-obj">
							{{if (video.id)}}
							<div class="pos-abs tp-init lt-init rt-init wd-full ht-full cs-ptr play-video embed-in-place" data-video="{{:video.src}}">
								<div class="pos-rel">
									<img alt="{{:title}}" title="{{:title}}" class="wd-full dp-bl" src="{{:video.img}}">
									<div class="pos-abs tp-init lt-init rt-init bt-init solid-tint-black-black40 "></div>
									<div class="cl-primary nb-icon-video-play-button tf-x-y-middle lt-half tp-half pos-abs fz-50"></div>
								</div>
							</div>
							{{else}}
							<iframe src="{{:video.src}}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen title="YouTube Frame"></iframe>
							{{/if}}
						</div>
							<div class="lt-init rt-init wd-full">
								<div class="hidden-image-card-slug hidden"></div>
							</div>
						{{/if}}

						<div class="hidden-slug" data-id="{{:htmlId}}"></div>
						<!-- <div class="lt-init tp-px-6 pd-l-10 pd-r-24 mg-b-16 bd-solid-venetian-red mg-l-8">
							<div id="{{:htmlId}}" class="cl-venetian-red evt-connector wd-full fx-ht fx-al-ct fx-js-bw dp-fx br-r-rd-4 ft-sec sec-bd-4 br-b-1p-pale-lavender">
								<div data-id="{{:htmlId}}" class="cl-venetian-red cover-card ft-sec sec-bd-4">
									<span>{{:tag}}</span>
									{{if stats}}
									<span class="mg-l-6 pd-l-6 stats-bar cl-dove-grey sec-bd-3">{{:stats.subtitle}}</span>
									{{/if}}
								</div>
								
							</div>
						</div> -->
						<h1 class="cover-card-title ft-sec bd-8 md-bd-8 pd-l-20 pd-r-20 mg-t-16 sticky-title">{{:title}}</h1>
						<div class="mg-t-10 dp-fx fx-al-ct fx-row cl-dove-grey pd-l-20 pd-r-20">
						{{for creatorSnapshot.creators ~len=creatorSnapshot.creators.length ~partnerLogo=partnerLogo}}
							<div class="mg-b-6 dp-fx fx-al-ct fx-row">
								{{if ~compareGT(#getIndex(), 0)}}
								<div class="dp-fx fx-al-ct tx-decoration-none mg-r-6 mg-l-6">
								{{else}}
								<div class="dp-fx fx-al-ct tx-decoration-none mg-r-6">
								{{/if}}
									<div class="ft-ter dp-fx fx-al-ct">
										<span class="md-ter-reg-1 ter-reg-1">{{:subtitle}}</span>
										<a href="{{:url}}" target="_blank"><span class="md-ter-reg-1 ter-reg-1 mg-l-4 tx-ct ft-ter"><u>{{:title}}</u></span></a>
									</div>
								</div>
								<!-- {{if ~_and(~eql(~len, 1), ~compareGT(~lengthOfArr(profiles), 0))}}
								<div class="dp-fx mg-l-10">
								{{for profiles}}
									<a target="_blank" class="md-ter-reg-1 ter-reg-1 mg-l-4 tx-ct ft-ter" href="{{:url}}" rel="nofollow noreferrer" aria-label="Link to {{:key}} profile">
										<span class="hidden">{{:~capitalize(key)}}</span>
										<div class="nb-icon-{{:key}} fz-14" ></div>
									</a>
								{{/for}}
								</div>
								{{/if}} -->
							</div>
						{{/for}}
						</div>

						<div class="pd-l-20 pd-r-20 dp-fx fx-al-ct fx-js-bw cl-dove-grey">
							<div class="dp-fx fx-al-ct fx-row ft-ter md-ter-reg-1 ter-reg-1">
								<span>{{:tag}}</span>
								<span class="pd-l-8 pd-r-8">|</span>
								<div class="dp-fx fx-al-ct fx-js-ct ft-ter md-ter-reg-1 ter-reg-1">
									<span>{{:creatorSnapshot.timestamp.split(",")[0]}} , {{:creatorSnapshot.timestamp.split(",")[1]}}</span>
									<div class="separator mg-l-6 mg-r-6 br-rd-pr-50 wd-ht-px-4 bg-acadia opacity-50"></div>
									<span>{{:creatorSnapshot.timestamp.split(",")[2]}}</span>
								</div>
							</div>
						</div>

						<div class="pd-l-20 pd-r-20">
							<div class="mg-t-16 br-t-1p-grey wd-full ht-px-0"></div>
						</div>

						{{if subtitle}}
						<div class="pd-l-20 pd-r-20 fx-al-ct mg-t-16">
							<span class="ft-ter ter-reg-2 cl-venetian-red">{{:subtitle}}</span>
						</div>
						{{/if}}

						<p class="ft-sec md-sec-reg-4 sec-reg-4 pd-l-20 pd-r-20 card-point cl-lt">
							{{props html}}
							<span class="dp-bl mg-t-10">{{:prop}}</span>
							{{/props}}
						</p>

						<div class="hidden-slug event-card" data-hooked="{{:htmlId}}"></div>
					</div>
					<div class="embed-event-cards" data-id="{{:htmlId}}"></div>
					{{if stats}}
					<div class="pd-b-10 cs-ptr pd-t-10 dp-fx fx-al-ct fx-dr-col fx-js-ct article-stats" data-id="{{:htmlId}}" data-load="{{:apiUrl}}">
                        <div class="ft-ter dp-fx fx-dr-col fx-al-ct fx-js-ct">
                            <span class="ter-bd-3 dp-fx fx-al-fs cl-venetian-red">{{:stats.title.replace( "Go deeper (" , 'Continue Reading</span><span class="ter-reg-1 cl-lt dp-fx fx-al-fs">(' ) }}</span>
                            <div class="dp-fx fx-js-ct mg-t-12">
                                <span class="pos-abs nb-icon-angle-down fz-18 mg-l-6 cl-venetian-red slide-down"></span>
                                <span class="pos-abs nb-icon-angle-down fz-18 mg-l-6 cl-venetian-red slide-down" style="animation-delay: 0.3s;margin-top: -4px"></span>
                            </div>
                        </div>
                    </div>
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 64 64" style="enable-background:new 0 0 64 64;" xml:space="preserve" alt="Dots Wave Loader" title="Dots Wave Loader" class="dp-bl mg-lr-auto wd-ht-px-32 hidden dot-loader">
						<style type="text/css">
						.dot-loader .st0{fill:#064893}.bounce{animation:bounce 2.4s infinite}.bounce:nth-child(3n+0){animation-delay:.6s}.bounce:nth-child(3n+1){animation-delay:1.4s}.bounce:nth-child(3n+2){animation-delay:2.2s}@keyframes bounce{0%,25%,50%,75%,100%{transform:translateY(0)}40%{transform:translateY(-20px)}60%{transform:translateY(-12px)}}
						</style>
						<circle class="st0 bounce" cx="13.8" cy="32" r="5"/>
						<circle class="st0 bounce" cx="31.8" cy="32" r="5"/>
						<circle class="st0 bounce" cx="49.8" cy="32" r="5"/>
					</svg>
					{{/if}}
				</article>
				<div class="embed-articleCard-below-items" data-id="{{:htmlId}}"></div>
			</main>
			<aside class="col-12 col-lg-4 pd-l-lg-10 pd-r-lg-10 banner-sidebar">
				<div class="embed-articleCard-side-items" data-id="{{:htmlId}}"></div>
			</aside>
		</section>
	</section>
	{{/for}}
</template>

<template type="text/x-jsrender" id="v2eventCardsList">
	{{for events.list.data}}
	{{if (conf.cardtype == 'poll')}}
	{{if ~lengthOfArr(options)}}
	<div class="hidden-slug mg-t-30 event-card" data-hooked="{{:htmlId}}"></div>
	<div class="tx-decoration-none" data-id="{{:htmlId}}">
		<div class="mg-t-20 pd-l-10 pd-l-md-14">
			<div class="slug dp-fx">
				<span class="z-index-1 bg-venetian-red evt-connector" data-hooked="{{:htmlId}}"></span>
				<div class="cl-venetian-red ft-ter md-ter-bd-3 ter-bd-3 wd-full tx-ct">{{:subtitle}}</div>
			</div>
			<div class="mg-t-4 ft-sec md-sec-bd-5 sec-bd-5 tx-ct">{{:title}}</div>
			<div class="hidden-poll-card-ad-slug hidden"></div>
		</div>
	</div>
	{{if ~_and(~neql(startsAt, ''), ~neql(endsAt, ''))}}
	<div class="pd-l-14 pd-r-14 pd-r-md-24 pd-l-md-24 pd-b-10 options-event-parent" data-apiurl="{{:apiUrl}}" data-id="{{:htmlId}}" data-start="{{:startsAt}}" data-end="{{:endsAt}}">
	{{else}}
	<div class="pd-l-14 pd-r-14 pd-r-md-24 pd-l-md-24 pd-b-10 options-event-parent" data-apiurl="{{:apiUrl}}" data-id="{{:htmlId}}">
	{{/if}}
		<div class="dp-fx fx-dr-col ft-ter md-ter-bd-3 ter-bd-3 options-list">
			{{for options ~pollId=pollId}}
				<div class="pd-t-14 pd-b-14 pd-l-20 pd-r-20 dp-fx wd-dull fx-js-bw fx-al-ct mg-t-10 br-1p-pale-lavender cl-link br-rd-5 cs-ptr pos-rel poll-card-option" data-id="{{:id}}" data-score="{{:score}}" data-pollid="{{:~pollId}}" data-choice="{{:#getIndex()}}">
					<div class="pos-abs lt-init ht-full tp-init bt-init progress-bar br-rd-5"></div>
					<div class="pd-r-10 z-index-1 title" data-classlist="md-ter-reg-3 ter-reg-3 cl-ink-dk">{{:title}}</div>
					<div class="pd-l-10 z-index-1 cl-ink-dk hidden subtitle md-ter-reg-3 ter-reg-3">{{:subtitle}}</div>
				</div>
			{{/for}}
		</div>
		<div class="mg-t-10 cl-suva-grey ft-ter md-ter-reg-3 ter-reg-3 hidden poll-result">{{:pollResults.title}}</div>
		{{for options ~pollId=pollId}}
			{{if ~neql(event.title, '')}}
			<div class="br-1p-pale-lavender event hidden mg-t-20 pd-b-10 pd-l-20 pd-r-20 pd-t-10" data-id="{{:id}}">
				{{if ~neql(auxText, '')}}
				<div class="cl-link ft-ter md-ter-reg-3 ter-reg-3">{{:auxText}}</div>
				{{/if}}
				<a href="{{:event.url}}" class="dp-fx wd-full mg-t-6 fx-al-ct" target="_blank" title="{{:event.title}}">
					<div class="fx-grow-1 pd-r-10 dp-fx">
						<div class="col-4 col-lg-1 fig-container--wd-ht-lg-60">
							<figure class="fig fig--bg-img-ct-cov-props ht-full-dac ht-full-lg-ac asp-ratio-rect-ac asp-ratio-rect-lg-dac" style="background-image:url({{:event.thumb}})" title="{{:event.title}}">
							</figure>
						</div>
						<div class="col-8 col-lg-10 pd-l-10 ft-pr bd-3 md-bd-3">
							<h3 class="ft-pr md-bd-4 bd-4">{{:event.title}}</h3>
						</div>
					</div>
					<div class="pd-l-10">
						<span class="fz-20 nb-icon-external-link"></span>
					</div>
				</a>
			</div>
			{{/if}}
		{{/for}}
	</div>
	{{/if}}
	{{/if}}
	{{if (conf.cardtype == 'quiz')}}
	{{if options}}
	<div class="pos-sticky z-index-1 rt-px-160 bt-px-90 wd-ht-0 ht-px-36 neg-mg-t-36 hidden-md take-quiz cs-ptr" data-quizid="{{:quizId}}-{{:htmlId}}">
		<div class="br-rd-15 cl-primary dp-fx fx-al-ct fx-js-ct ht-px-60 pd-l-10 pd-r-10 pd-t-10 share-link solid-tint-bright-red wd-px-150">
            <span class="fz-24 mg-r-12 nb-icon-quiz"></span>
            <span class="ft-ter pd-b-10 fz-14">{{:title}}</span>
        </div>
    </div>
	<div class="quiz-siblings svgs hidden">
		<div class="svg correct">
			<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="mg-l-6 mg-r-6">
				<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<g id="quiz-end" transform="translate(-249.000000, -281.000000)">
						<g id="Question-1" transform="translate(164.000000, 100.000000)">
							<g id="Option-1" transform="translate(20.000000, 141.000000)">
								<g id="Correct-answer" transform="translate(65.000000, 40.000000)">
									<circle id="Oval" fill="#30D158" cx="12" cy="12" r="12"></circle>
									<g id="check-good-yes" transform="translate(8.000000, 9.000000)" stroke="#FFFFFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
										<polyline id="Path" points="0 3 2.66666667 6 8 0"></polyline>
									</g>
								</g>
							</g>
						</g>
					</g>
				</g>
			</svg>
		</div>
		<div class="svg incorrect">
			<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="mg-l-6 mg-r-6">
				<title>Group 2</title>
				<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<g id="quiz-end" transform="translate(-293.000000, -281.000000)">
						<g id="Question-1" transform="translate(164.000000, 100.000000)">
							<g id="Option-1" transform="translate(20.000000, 141.000000)">
								<g id="Group-3" transform="translate(65.000000, 40.000000)">
									<g id="Group-2" transform="translate(44.000000, 0.000000)">
										<circle id="Oval" fill="#FF453A" cx="12" cy="12" r="12"></circle>
										<path d="M15,9 L12,12 M12,12 L9,15 M12,12 L9,9 M12,12 L15,15" id="Shape" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
									</g>
								</g>
							</g>
						</g>
					</g>
				</g>
			</svg>
		</div>
	</div>
	{{if ~_and(~neql(startsAt, ''), ~neql(endsAt, ''))}}
	<div class="hidden tp-init quiz-card-parent wd-ht-0" data-id="{{:htmlId}}" data-quizid="{{:quizId}}-{{:htmlId}}" data-start="{{:startsAt}}" data-end="{{:endsAt}}">
	{{else}}
	<div class="hidden tp-init quiz-card-parent wd-ht-0" data-id="{{:htmlId}}" data-quizid="{{:quizId}}-{{:htmlId}}">
	{{/if}}
        <div class="z-index-2 solid-tint-black-black60 backdrop-blur-px-5 tp-init lt-init wd-vw-100 ht-vh-100 pos-fix close-quiz" data-quizid="{{:quizId}}-{{:htmlId}}"></div>
        <div class="pos-fix lt-pr-50-px-180 tp-pr-50-px-250 z-index-2">
            <div class="pos-rel wd-px-360 ht-px-500 bg-body pd-l-30 pd-r-30 br-rd-14" data-quizid="{{:quizId}}-{{:htmlId}}">
				<div class="hidden-quiz-card-ad-slug hidden"></div>
				{{for options.questions ~quizId=quizId ~array=options.questions}}
				<div class="pd-t-20 pd-b-20 slide question ht-full" data-id="{{:id}}">
					<div class="dp-fx ovr-scroll hide-scrollbar ht-full">
						<div class="quiz-card-intermediary pos-abs lt-half hidden">
							<div class="ft-ter md-ter-bd-5 ter-bd-5 tx-ct">{{:auxText}}</div>
						</div>
						{{if ~eql(#getIndex(), 0)}}
						<div class="final">
						{{else}}
						<div class="final hidden">
						{{/if}}
							<div class="mg-t-6 mg-b-10 ft-ter md-ter-bd-1 ter-bd-1 cl-brand">{{:subtitle.toUpperCase()}}</div>
							<div class="dp-fx wd-full mg-t-6 mg-b-10 fx-al-ct">
								<div class="br-rd-3 wd-full bg-spark-red-lt">
									<div class="br-rd-3 pd-b-6 bg-brand" style="width:{{:((#getIndex()+1)*(100/~array.length))}}%"></div>
								</div>
								<div class="pd-l-8 ft-ter md-ter-bd-1 ter-bd-1">{{:(#getIndex()+1)}}/{{:~array.length}}</div>
							</div>

							<div class="ft-ter md-ter-bd-5 ter-bd-5 mg-t-20">{{:title}}</div>
							<div class="dp-fx fx-dr-col ft-ter md-ter-bd-3 ter-bd-3 mg-t-40 answers-list">
								{{for options ~quizId=~quizId}}
									<div class="pd-t-14 pd-b-14 pd-l-20 pd-r-20 dp-fx wd-dull fx-js-bw fx-al-ct mg-t-10 br-1p-pale-lavender br-rd-5 cs-ptr pos-rel quiz-card-option bg-primary" data-id="{{:id}}" data-answer="{{:answer}}" data-quizid="{{:~quizId}}-{{:~root.htmlId}}">
										<div class="pos-abs lt-init ht-full tp-init bt-init progress-bar br-rd-5"></div>
										<div class="dp-fx fx-align-ct fx-js-bw wd-full">
											<div class="dp-fx">
												<span class="z-index-1 cl-brand ft-ter md-ter-reg-2 ter-reg-2 pd-r-10 index">{{:~incrementChar('a', #getIndex())}}.</span>
												<div class="pd-r-10 z-index-1 title">{{:title}}</div>
											</div>
											<div class="dp-fx fx-al-ct">
												<div class="z-index-1 icon-embed hidden"></div>
											</div>
										</div>
									</div>
								{{/for}}
							</div>
						</div>
					</div>
				</div>
				{{/for}}

				<div class="slide result ht-full pos-rel" data-id="{{:options.results.id}}">
					<div class="quiz-card-intermediary pos-abs lt-half hidden">
						<div class="ft-ter md-ter-bd-5 ter-bd-5">{{:options.results.auxText}}</div>
					</div>
					<div class="final hidden dp-fx fx-dr-col ovr-scroll hide-scrollbar ht-full pd-t-20 pd-b-20">
						<div class="dp-fx fx-dr-col fx-al-ct">
							<div class="dp-fx fx-al-ct pd-b-20">
								<div class="mg-r-20">
									<div class="results-bar wd-ht-px-50 br-rd-pr-50"></div>
								</div>
								<div class="dp-fx fx-dr-col">
									<div class="mg-t-6 ft-ter md-ter-bd-1 ter-bd-1 cl-spark-red title">-results-</div>
									<div class="mg-t-4 ft-ter md-ter-bd-5 ter-bd-5 subtitle">-remarks-</div>
								</div>
								<div class="mg-l-20">
									<div class="results-timer wd-ht-px-32 br-rd-pr-50 dp-fx fx-al-ct fx-js-ct ft-ter md-ter-bd-1 ter-bd-1 cl-spark-red">10</div>
								</div>
							</div>
						</div>

						{{if ~_and(options.results.linkWithImg.title, ~neql(options.results.linkWithImg.title, ''))}}
						<div class="mg-t-40 linkWithImg" data-id="{{:options.results.id}}">
							<a href="{{:options.results.linkWithImg.url}}" class="dp-fx wd-full mg-t-6 fx-al-ct" target="_blank" title="{{:options.results.linkWithImg.title}}">
								<div class="fx-grow-1 pd-r-10 dp-fx">
									<div class="col-4 col-lg-1 fig-container--wd-ht-lg-60">
										<figure class="fig fig--bg-img-ct-cov-props ht-full-dac ht-full-lg-ac asp-ratio-sq-ac asp-ratio-sq-lg-dac" style="background-image:url({{:options.results.linkWithImg.thumb}})" title="{{:options.results.linkWithImg.title}}">
										</figure>
									</div>
									<div class="col-8 col-lg-10 pd-l-10 ft-pr bd-3 md-bd-3">
										<h3 class="ft-pr md-bd-4 bd-4">{{:options.results.linkWithImg.title}}</h3>
									</div>
								</div>
								<div class="pd-l-10">
									<span class="fz-20 nb-icon-external-link"></span>
								</div>
							</a>
						</div>
						{{/if}}

						{{for options.questions ~quizId=quizId ~array=options.questions}}
						<div class="dp-fx wd-full mg-t-16">
							<div class="mg-r-10 pd-t-16 question-result" data-quizid="{{:~quizId}}-{{:~root.htmlId}}"></div>
							<div class="pd-t-10 pd-l-10 pd-r-10 pd-b-10 bg-primary br-rd-10 wd-full show-answer" data-id="{{:id}}" data-quizid="{{:~quizId}}-{{:~root.htmlId}}">
								<div class="dp-fx fx-js-bw fx-al-ct cs-ptr">
									<div class="">
										<div class="ft-ter md-ter-bd-2 ter-bd-2 cl-suva-grey">{{:subtitle}}</div>
										<div class="ft-ter md-ter-bd-3 ter-bd-3 mg-t-4">{{:title}}</div>
									</div>
									<div class="nb-icon-smallright show-answer-icon fz-20"></div>
									<div class="nb-icon-down hide-answer-icon fz-20 hidden"></div>
								</div>
								<div class="mg-t-10 answer hidden">
									<hr class="br-t-1p-white-smoke"/>
									{{if ~_and(description, ~neql(description, ''))}}
									<div class="ft-ter md-ter-reg-3 ter-reg-3 mg-t-10">{{:description}}</div>
									{{else}}
									{{for options ~quizId=~quizId}}
									{{if ~eql(answer, 'true')}}
									<div class="ft-ter md-ter-reg-3 ter-reg-3 mg-t-10">{{:title}}</div>
									{{/if}}
									{{/for}}
									{{/if}}
								</div>
							</div>
						</div>
						{{/for}}
					</div>
				</div>
			</div>
			<div class="dp-fx wd-full fx-js-ct pd-t-20 pos-rel">
				<div class="wd-ht-px-50 br-rd-pr-50 bg-ink-dk dp-fx fx-al-ct fx-js-ct close-quiz cs-ptr" data-quizid="{{:quizId}}-{{:htmlId}}">
					<span class="nb-icon-cancel fz-24 cl-primary mg-t-8"></span>
				</div>
			</div>
		</div>
    </div>
	{{/if}}
	{{/if}}
	{{if (conf.cardtype == 'fact')}}
		{{if ~_and(traits, ~neql(traits.type, ''))}}
		{{if ~eql(traits.type, 'scorecard')}}
		{{if ~_and(traits.sport, ~eql(traits.sport, 'tennis'))}}
		<div class="mg-t-10 pd-l-14 pd-r-14 pd-r-md-0 pd-l-md-18">
			<div class="pd-t-30 pd-b-30 pd-l-20 pd-r-20 ft-pr br-rd-20 cl-primary solid-tint-{{:~tagColor(tag + 'tint')}}">
				{{if (jumpLinkId)}}
				{{if (jumpLinkId != '')}}
				<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
				{{/if}}
				{{/if}}
				{{if (subtitle != '')}}
				<div class="dp-fx fx-al-ct">
					<h3 class="cl-primary ft-pr md-bd-7 bd-7">{{:subtitle}}</h3>
				</div>
				{{/if}}
				{{if (jumpLinkId)}}
				{{if (jumpLinkId != '')}}
				</div>
				{{/if}}
				{{/if}}
				{{if (~neql(traits.subtitle, ''))}}
				<div class="mg-t-2 ft-ter md-ter-reg-2 ter-reg-2">{{:traits.subtitle}}</div>
				{{/if}}
				{{if (~neql(traits.title, ''))}}
				<div class="ft-ter md-ter-reg-2 ter-reg-2">{{:traits.title}}</div>
				{{/if}}
				{{if ~lengthOfArr(traits.comparison.items)}}
					{{for traits.comparison.items}}
					<div class="dp-fx fx-al-ct fx-js-bw mg-t-20">
						<div class="dp-fx fx-al-ct">
							{{if ~neql(img, '')}}
							<img data-src="{{:img}}" alt="{{:subtitle}}" title="{{:subtitle}}" class="lazy wd-ht-px-40 br-rd-pr-50"/>
							{{/if}}
							{{if highlight}}
							<div class="mg-l-10">
							{{else}}
							<div class="mg-l-10 opacity-8">
							{{/if}}
								<div class="ft-ter md-ter-bd-5 ter-bd-5">{{:title}}</div>
								<div class="ft-ter md-ter-reg-1 ter-reg-1">{{:subtitle}}</div>
							</div>
						</div>
						{{if ~lengthOfArr(params)}}
						{{if highlight}}
						<div class="dp-fx fx-al-ct ft-ter md-ter-bd-5 ter-bd-5">
						{{else}}
						<div class="dp-fx fx-al-ct ft-ter md-ter-bd-5 ter-bd-5 opacity-8">
						{{/if}}
							{{for params}}
							<div class="pd-l-6 pd-r-6 pd-l-md-10 pd-r-md-10">{{:title}}</div>
							{{/for}}
						</div>
						{{/if}}
					</div>
					{{/for}}
				{{/if}}
				<div class="dp-fx fx-js-end">
					<div class="collapse-bttn open dp-fx fx-al-ct fx-js-bw br-rd-10 pd-t-14 pd-b-14 pd-l-20 pd-r-20 br-1p-pale-lavender cs-ptr wd-full wd-full-md-dac mg-t-20" data-id="{{:htmlId}}">
						<div class="ft-ter md-ter-bd-2 ter-bd-2">{{:traits.buttons[0].title}}</div>
						<div class="mg-l-20 nb-icon-angle-down fz-20 rotate-neg-90"></div>
					</div>
				</div>
				{{if ~lengthOfArr(traits.list)}}
				<div id="collapsible-{{:htmlId}}" class="ht-px-0 ovr-hidden">
					<div class="sc-stats-separator mg-t-20"></div>
					{{for traits.list}}
					{{if ~eql((#getIndex()), 0)}}
					<div class="dp-fx fx-js-bw fx-al-ct mg-t-20">
						{{for data ~title=title}}
						{{if ~eql((#getIndex() % 2), 1)}}
						{{if (~neql(~title, ''))}}
						<div class="ft-ter md-ter-reg-3 ter-reg-3 tx-ct">{{:~title}}</div>
						{{/if}}
						{{/if}}
						{{if ~eql((#getIndex() % 2), 0)}}
						<div class="ft-ter md-ter-bd-2 ter-bd-2 tx-lt">{{:title}}</div>
						{{else}}
						<div class="ft-ter md-ter-bd-2 ter-bd-2 tx-rt">{{:title}}</div>
						{{/if}}
						{{/for}}
					</div>
					{{else}}
					<div class="dp-fx fx-js-bw fx-al-ct mg-t-20">
						{{for data ~title=title}}
						{{if ~eql((#getIndex() % 2), 1)}}
						{{if (~neql(~title, ''))}}
						<div class="ft-ter md-ter-reg-3 ter-reg-3 tx-ct">{{:~title}}</div>
						{{/if}}
						{{/if}}
						{{if ~eql((#getIndex() % 2), 0)}}
						<div class="ft-ter md-ter-bd-5 ter-bd-5 tx-lt">{{:title}}</div>
						{{else}}
						<div class="ft-ter md-ter-bd-5 ter-bd-5 tx-rt">{{:title}}</div>
						{{/if}}
						{{/for}}
					</div>
					{{/if}}
					{{/for}}
				</div>
				{{/if}}
				<div class="dp-fx fx-js-end">
					<div class="collapse-bttn close dp-fx fx-al-ct fx-js-bw br-rd-10 pd-t-14 pd-b-14 pd-l-20 pd-r-20 br-1p-pale-lavender cs-ptr wd-full wd-full-md-dac hidden mg-t-20" data-id="{{:htmlId}}">
						<div class="ft-ter md-ter-bd-2 ter-bd-2">{{:traits.buttons[1].title}}</div>
						<div class="mg-l-20 nb-icon-angle-down fz-20 rotate-180"></div>
					</div>
				</div>
			</div>
		</div>
		{{else}}
		<div class="mg-t-20 pd-l-14 pd-r-14 pd-r-md-0 pd-l-md-18">
			<div class="pd-t-50 pd-b-50 pd-l-20 pd-r-20 ft-pr br-rd-20 cl-primary solid-tint-{{:~tagColor(tag + 'tint')}}">
				{{if (jumpLinkId)}}
				{{if (jumpLinkId != '')}}
				<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
				{{/if}}
				{{/if}}
				{{if (subtitle != '')}}
				<div class="dp-fx fx-al-ct">
					<h3 class="cl-primary ft-ter md-ter-bd-2 ter-bd-2">{{:subtitle}}</h3>
				</div>
				{{/if}}
				{{if (jumpLinkId)}}
				{{if (jumpLinkId != '')}}
				</div>
				{{/if}}
				{{/if}}
				{{if (~neql(traits.title, ''))}}
				<div class="tx-ct mg-t-10 ft-ter md-ter-reg-2 ter-reg-2">{{:traits.title}}</div>
				{{/if}}
				{{if (~neql(traits.subtitle, ''))}}
				<div class="tx-ct ft-ter md-ter-reg-2 ter-reg-2">{{:traits.subtitle}}</div>
				{{/if}}
				{{if ~lengthOfArr(traits.comparison.items)}}
				<div class="fx-row mg-t-10 fx-js-bw">
					{{for traits.comparison.items ~connector=traits.comparison.connector}}
					<div class="dp-fx">
						{{if ~_and(~neql(img, ''), ~eql((#getIndex() % 2), 0))}}
						<div class="dp-fx fx-dr-col fx-al-ct mg-r-10 mg-r-md-20">
							<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-20 sc-comp-img br-rd-pr-50"/>
							<div class="ft-ter md-ter-bd-6 ter-bd-6 mg-t-16">{{:title}}</div>
						</div>
						{{/if}}
						<div>
							{{if ~eql((#getIndex() % 2), 0)}}
							<div class="ft-ter md-ter-bd-3 ter-bd-3 tx-rt">{{:params.title}}</div>
							<div class="ft-ter md-ter-reg-2 ter-reg-2 tx-rt">{{:params.subtitle}}</div>
							{{else}}
							<div class="ft-ter md-ter-bd-3 ter-bd-3 tx-lt">{{:params.title}}</div>
							<div class="ft-ter md-ter-reg-2 ter-reg-2 tx-lt">{{:params.subtitle}}</div>
							{{/if}}
						</div>
						{{if ~_and(~neql(img, ''), ~neql((#getIndex() % 2), 0))}}
						<div class="dp-fx fx-dr-col fx-al-ct mg-l-10 mg-l-md-20">
							<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-20 sc-comp-img br-rd-pr-50"/>
							<div class="ft-ter md-ter-bd-6 ter-bd-6 mg-t-16">{{:title}}</div>
						</div>
						{{/if}}
					</div>
					{{if ~eql((#getIndex() % 2), 0)}}
					<div class="ft-ter md-ter-bd-2 ter-bd-2 mg-t-16">{{:~connector}}</div>
					{{/if}}
					{{/for}}
				</div>
				{{/if}}
				{{if (~neql(traits.conclusion, ''))}}
				<div class="ft-ter md-ter-reg-3 ter-reg-3 mg-t-20 tx-ct">{{:traits.conclusion}}</div>
				{{/if}}
				{{if ~lengthOfArr(traits.list)}}
					{{for traits.list}}
					<div class="ft-ter md-ter-bd-4 ter-bd-4 mg-t-20 tx-ct">{{:title}}</div>
					<div class="fx-row neg-mg-lr-px-10-ac">
						{{for data}}
						{{if ~eql((#getIndex() % 2), 0)}}
						<div class="mg-t-6 col-6 dp-fx fx-al-ct pd-l-10 pd-r-10">
						{{else}}
						<div class="mg-t-6 col-6 dp-fx fx-al-ct pd-l-10 pd-r-10 fx-js-end">
						{{/if}}
							{{if ~_and(~neql(img, ''), ~eql((#getIndex() % 2), 0))}}
							<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-20 br-rd-pr-50 mg-r-10"/>
							{{/if}}
							{{if ~eql((#getIndex() % 2), 0)}}
							<div class="ft-ter md-ter-reg-2 ter-reg-2 dp-fx fx-dr-col">
							{{else}}
							<div class="ft-ter md-ter-reg-2 ter-reg-2 dp-fx fx-dr-col fx-al-end">
							{{/if}}
								<div>{{:title}}</div>
								<div>{{:subtitle}}</div>
							</div>
							{{if ~_and(~neql(img, ''), ~neql((#getIndex() % 2), 0))}}
							<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-20 br-rd-pr-50 mg-l-10"/>
							{{/if}}
						</div>
						{{/for}}
					</div>
					{{/for}}
				{{/if}}
				{{if ~lengthOfArr(traits.card)}}
					<div class="ft-ter md-ter-bd-4 ter-bd-4 mg-t-20">{{:traits.card.title}}</div>
					{{props traits.card.text}}
					<div class="mg-t-10 ft-ter md-ter-reg-3 ter-reg-3">{{:prop}}</div>
					{{/props}}
				{{/if}}
				{{if ~lengthOfArr(traits.links)}}
					<div class="fx-row neg-mg-lr-px-10-ac">
						{{for traits.links ~len=traits.links.length}}
						{{if ~compareGT(~len, 1)}}
						<div class="pd-l-10 pd-r-10 col-12 mg-t-30 col-md-6">
						{{else}}
						<div class="pd-l-10 pd-r-10 col-12 mg-t-30">
						{{/if}}
							<a class="dp-bl wd-full ft-ter md-ter-bd-2 ter-bd-2 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{{:url}}" target="_blank" aria-label="Button Link">
								<div>{{:title}}</div>
								<span class="nb-icon-external-link fz-20 mg-l-6"></span>
							</a>
						</div>
						{{/for}}
					</div>
				{{/if}}
			</div>
		</div>
		{{/if}}
		{{else ~eql(traits.type, 'rating')}}
		<div class="mg-t-20 pd-l-14 pd-r-14 pd-r-md-0 pd-l-md-18">
			<div class="pd-t-50 pd-b-50 pd-l-20 pd-r-20 ft-pr br-rd-20 cl-primary solid-tint-{{:~tagColor(tag + 'tint')}}">
				{{if (jumpLinkId)}}
				{{if (jumpLinkId != '')}}
				<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
				{{/if}}
				{{/if}}
				{{if (subtitle != '')}}
				<div class="dp-fx fx-al-ct">
					<h3 class="cl-primary ft-ter md-ter-bd-3 ter-bd-3">{{:subtitle}}</h3>
				</div>
				{{/if}}
				{{if (jumpLinkId)}}
				{{if (jumpLinkId != '')}}
				</div>
				{{/if}}
				{{/if}}
				{{if (~neql(traits.title, ''))}}
				<div class="ft-sec md-sec-bd-5 sec-bd-5 mg-t-6 cl-primary">{{:traits.title}}</div>
				{{/if}}
				{{if ~lengthOfArr(traits.ratings.keys)}}
				<div class="pd-t-10" title="{{:traits.ratings.value}}">
					<div class="cl-primary ft-ter md-ter-reg-2 ter-reg-2">{{:traits.title}}</div>
					<div class="fx-row">
						{{props traits.ratings.keys}}
						<div class="svg-inclusion mg-r-4">
							{{if ~eql(prop, 'star')}}
							<svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg star">
								<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<g id="Group-3" fill="#BBC2BD">
										<path d="M9.8316,0.5748 L11.7896,4.7468 C11.9316,5.0488 12.2146,5.2608 12.5436,5.3108 L17.0026,5.9918 C17.8076,6.1148 18.1356,7.0958 17.5666,7.6788 L14.2786,11.0488 C14.0576,11.2768 13.9566,11.5958 14.0076,11.9078 L14.7746,16.6138 C14.9096,17.4378 14.0346,18.0538 13.3046,17.6498 L9.4106,15.4978 C9.1086,15.3308 8.7436,15.3308 8.4426,15.4978 L4.5486,17.6498 C3.8176,18.0538 2.9426,17.4378 3.0776,16.6138 L3.8446,11.9078 C3.8956,11.5958 3.7956,11.2768 3.5736,11.0488 L0.2856,7.6788 C-0.2834,7.0958 0.0456,6.1148 0.8506,5.9918 L5.3086,5.3108 C5.6386,5.2608 5.9206,5.0488 6.0626,4.7468 L8.0206,0.5748 C8.3806,-0.1922 9.4716,-0.1922 9.8316,0.5748" id="Fill-1"></path>
									</g>
								</g>
							</svg>
							{{else ~eql(prop, 'filled_star')}}
							<svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg filled_star">
								<defs>
									<linearGradient x1="50%" y1="-1.12155278%" x2="50%" y2="98.8784472%" id="linearGradient-1">
										<stop stop-color="#FDD800" offset="0%"></stop>
										<stop stop-color="#EBC10E" offset="100%"></stop>
									</linearGradient>
								</defs>
								<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<path d="M9,0 C8.588,0 8.176,0.219 7.972,0.656 L7.972,0.656 L6.099,4.665 C5.939,5.009 5.618,5.251 5.243,5.307 L5.243,5.307 L0.965,5.964 C0.051,6.105 -0.321,7.224 0.324,7.889 L0.324,7.889 L3.49,11.149 C3.742,11.408 3.857,11.773 3.798,12.13 L3.798,12.13 L3.06,16.673 C2.942,17.403 3.514,17.99 4.164,18 L4.164,18 L4.2,18 C4.376,17.997 4.557,17.952 4.73,17.855 L4.73,17.855 L8.45,15.789 C8.792,15.599 9.207,15.599 9.549,15.789 L9.549,15.789 L13.27,17.855 C13.444,17.952 13.625,17.997 13.801,18 L13.801,18 L13.836,18 C14.486,17.99 15.058,17.403 14.939,16.673 L14.939,16.673 L14.202,12.13 C14.143,11.773 14.258,11.408 14.51,11.149 L14.51,11.149 L17.676,7.889 C18.321,7.224 17.949,6.105 17.035,5.964 L17.035,5.964 L12.757,5.307 C12.382,5.251 12.062,5.009 11.901,4.665 L11.901,4.665 L10.028,0.656 C9.824,0.219 9.412,0 9,0 Z" id="Fill-1" fill="url(#linearGradient-1)"></path>
								</g>
							</svg>
							{{else ~eql(prop, 'half_filled_star')}}
							<svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg half_filled_star">
								<defs>
									<linearGradient x1="49.9996654%" y1="-1.12155278%" x2="49.9996654%" y2="98.8784472%" id="linearGradient-1">
										<stop stop-color="#FDD800" offset="0%"></stop>
										<stop stop-color="#EBC10E" offset="100%"></stop>
									</linearGradient>
								</defs>
								<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<path d="M9,0 C8.589,0 8.176,0.219 7.972,0.656 L7.972,0.656 L6.099,4.665 C5.939,5.009 5.618,5.251 5.243,5.307 L5.243,5.307 L0.965,5.964 C0.051,6.105 -0.321,7.224 0.324,7.889 L0.324,7.889 L3.49,11.149 C3.742,11.408 3.857,11.773 3.798,12.13 L3.798,12.13 L3.06,16.673 C2.942,17.403 3.514,17.99 4.164,18 L4.164,18 L4.2,18 C4.376,17.997 4.557,17.952 4.73,17.855 L4.73,17.855 L8.451,15.789 C8.621,15.694 8.811,15.646 9,15.646 L9,15.646 L9,0 Z" id="Fill-1" fill="url(#linearGradient-1)"></path>
									<path d="M8.9999,15.6465 C9.1889,15.6465 9.3779,15.6935 9.5489,15.7885 L13.2699,17.8545 C14.0989,18.3155 15.0919,17.6125 14.9389,16.6725 L14.2019,12.1295 C14.1439,11.7725 14.2579,11.4085 14.5099,11.1495 L17.6759,7.8895 C18.3219,7.2245 17.9489,6.1045 17.0349,5.9645 L12.7569,5.3075 C12.3829,5.2505 12.0619,5.0095 11.8999,4.6645 L10.0279,0.6565 C9.8239,0.2185 9.4119,0.0005 8.9999,0.0005 L8.9999,15.6465 Z" id="Fill-4" fill="#BBC2BD"></path>
								</g>
							</svg>
							{{/if}}
						</div>
						{{/props}}
					</div>
				</div>
				{{/if}}
				{{if ~neql(traits.img, '')}}
				<div class="dp-fx fx-js-ct mg-t-20">
					<img data-src="{{:traits.img}}" alt="{{:traits.title}}" title="{{:traits.title}}" class="lazy wd-ht-px-200"/>
				</div>
				{{/if}}
				{{if ~lengthOfArr(traits.card.text)}}
				{{props traits.card.text}}
				<div class="ft-sec md-sec-reg-4 sec-reg-4 cl-primary mg-t-10">{{:prop}}</div>
				{{/props}}
				{{/if}}
				{{if ~lengthOfArr(traits.list)}}
				<div class="fx-row neg-mg-lr-px-10-ac cl-primary">
					{{for traits.list}}
					<div class="col-12 col-md-6 pd-l-10 pd-r-10 mg-t-20">
						<div class="ft-ter md-ter-bd-4 ter-bd-4">{{:title}}</div>
						{{props data}}
						<div class="pos-rel mg-t-10">
							<div class="ft-sec md-sec-reg-4 sec-reg-4 pd-t-6 pd-b-6 pd-l-20">{{:prop}}</div>
							<div class="gd-tint-white-white0-white100-half-1 pos-abs lt-init bt-init tp-init wd-px-90"></div>
						</div>
						{{/props}}
					</div>
					{{/for}}
				</div>
				{{/if}}
				{{if ~lengthOfArr(traits.links)}}
					<div class="fx-row neg-mg-lr-px-10-ac">
						{{for traits.links ~len=traits.links.length}}
						{{if ~compareGT(~len, 1)}}
						<div class="pd-l-10 pd-r-10 col-12 mg-t-30 col-md-6">
						{{else}}
						<div class="pd-l-10 pd-r-10 col-12 mg-t-30">
						{{/if}}
							<a class="dp-bl wd-full ft-ter md-ter-bd-2 ter-bd-2 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{{:url}}" target="_blank" aria-label="Button Link">
								<div>{{:title}}</div>
								<span class="nb-icon-external-link fz-20 mg-l-6"></span>
							</a>
						</div>
						{{/for}}
					</div>
				{{/if}}
			</div>
		</div>
		{{/if}}
		{{else}}
			{{if ~eql(tag, 'whydoesitmatter')}}
				<div class="hidden-slug mg-t-20 event-card" data-hooked="{{:htmlId}}"></div>
				<div id="{{:jumpLinkId}}" class="jump-at mg-t-20" data-id="{{:htmlId}}">
					<div class="mg-t-10 pd-l-10 pd-l-md-14">
						<div class="slug dp-fx">
							<span class="z-index-1 bg-venetian-red evt-connector" data-hooked="{{:htmlId}}"></span>
							<div class="cl-venetian-red ft-ter md-ter-bd-3 ter-bd-3 wd-full tx-ct">{{:subtitle}}</div>
						</div>
						<div class="mg-t-4 ft-sec md-sec-bd-5 sec-bd-5 tx-ct"><u>{{:title}}</u></div>
					</div>
				</div>
				<ul class="pd-l-28 pd-r-14 pd-r-md-0 pd-l-md-48 pd-b-20 ft-sec md-sec-reg-4 sec-reg-4 card-point">
					{{props html}}
					<li class="mg-t-20">{{:prop}}</li>
					{{/props}}
				</ul>
			{{else}}
				{{if ~neql(subtitle, '')}}
				<div class="hidden-slug mg-t-20 event-card" data-hooked="{{:htmlId}}"></div>
				<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
					<div class="mg-t-20 pd-l-10 pd-l-md-14">
						<div class="slug dp-fx">
							<span class="z-index-1 bg-lt evt-connector" data-hooked="{{:htmlId}}"></span>
							<div class="cl-lt mg-l-10 mg-l-md-14">
								<div class="ft-ter md-ter-bd-2 ter-bd-2">{{:subtitle}}</div>
							</div>
						</div>
						<div class="mg-t-4 ft-sec md-sec-bd-6 sec-bd-6 cl-night-rider mg-l-4 mg-l-md-8">{{:title}}</div>
					</div>
				</div>
				{{/if}}
				<p class="ft-sec md-sec-reg-4 sec-reg-4 pd-l-14 pd-r-14 pd-r-md-0 pd-l-md-24 card-point cl-night-rider">
					{{props html}}
					<span class="dp-bl mg-t-10">{{:prop}}</span>
					{{/props}}
				</p>
			{{/if}}
		{{/if}}
	{{else (conf.cardtype == 'event')}}
		{{if ~neql(subtitle, '')}}
		<div class="hidden-slug mg-t-20 event-card" data-hooked="{{:htmlId}}"></div>
		<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
			<div class="mg-t-20 pd-l-10 pd-l-md-14">
				<div class="slug dp-fx">
					<span class="z-index-1 bg-lt evt-connector" data-hooked="{{:htmlId}}"></span>
					<div class="cl-lt mg-l-10 mg-l-md-14">
						<div class="ft-ter md-ter-bd-2 ter-bd-2">{{:subtitle}}</div>
					</div>
				</div>
				<div class="mg-t-4 ft-sec md-sec-bd-6 sec-bd-6 cl-night-rider mg-l-4 mg-l-md-8">{{:title}}</div>
			</div>
		</div>
		{{/if}}

		{{if ~neql(title, '')}}
		{{if ~_and(slides, ~lengthOfArr(slides))}}
		<div class="mg-t-10 pd-l-md-18">
			<section class="slide-show-parent visibility-hidden">
				<section class="slide-show default with-centerMode {{:htmlId}}" id="slideshow-{{:~randomId(6)}}">
					{{for slides}}
					<figure class="min-wd-px-100 mg-r-20">
						<img alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full" src="{{:img}}">
						{{if ~_and(caption, ~neql(caption, ''))}}
						<figcaption class="cl-lt pd-t-4 ft-sec md-sec-reg-2 sec-reg-2">{{:caption}}</figcaption>
						{{/if}}
					</figure>
					{{/for}}
				</section>
				<div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
			</section>
		</div>
		{{!-- {{else ~_and(~neql(ximg, ''), ~eql(video.src, ''))}}
		<div class="mg-t-10 pd-l-md-18">
			<figure>
				<div class="pos-rel asp-ratio r-16-9">
					{{if ~_and(~neql(imageFallback, undefined), imageFallback)}}
					<picture>
						<source
							media="(max-width: 599px) and (min-device-pixel-ratio: 3),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 3),(max-width: 599px) and (min-resolution: 3dppx),(max-width: 599px) and (min-resolution: 288dpi),(min-width: 768px) and (min-device-pixel-ratio: 2),(min-width: 768px) and (-webkit-min-device-pixel-ratio: 2),(min-width: 768px) and (min-resolution: 3dppx),(min-width: 768px) and (min-resolution: 288dpi)"
							srcset="{{:ximg}}">
						<source
							media="(min-width: 1080px) and (min-device-pixel-ratio: 1),(min-width: 1080px) and (-webkit-min-device-pixel-ratio: 1),(min-width: 1080px) and (min-resolution: 2dppx),(min-width: 1080px) and (min-resolution: 192dpi)"
							srcset="{{:limg}}">
						<source
							media="(max-width: 599px) and (min-device-pixel-ratio: 2),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 2),(max-width: 599px) and (min-resolution: 2dppx),(max-width: 599px) and (min-resolution: 192dpi)"
							srcset="{{:img}}">
						<source
							media="(max-width: 599px) and (min-device-pixel-ratio: 2),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 2),(max-width: 599px) and (min-resolution: 2dppx),(max-width: 599px) and (min-resolution: 192dpi)"
							srcset="{{:img}}">
						<img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full lazy cover-card-img"/>
					</picture>
					{{else}}
					<img data-src="{{:ximg}}" alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full lazy"/>
					{{/if}}
					{{if (imagecredit)}}
					{{if ~_and(~neql(imagecredit, null), imagecredit.title)}}
						<a class="pos-abs tx-decoration-none image-credits" href="{{:imagecredit.url}}" target="_blank" rel="nofollow noopener">
							<div class="dp-fx fx-al-ct">
								<div class="cl-primary ft-ter md-ter-reg-1 ter-reg-1 mg-r-4">{{:imagecredit.title}}</div>
								<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 -2 17.52 16" height="10"><defs></defs><circle class="svg" cx="8.76" cy="8.26" r="2.87"/><path class="svg" d="M15.78,2.52H13.5L13,1a1.48,1.48,0,0,0-1.41-1H6.39A1.48,1.48,0,0,0,5,1L4.5,2.52H2.22a2,2,0,0,0-2,2V12a2,2,0,0,0,2,2H15.78a2,2,0,0,0,2-2V4.49A2,2,0,0,0,15.78,2.52ZM9,12.32a4.06,4.06,0,1,1,4.06-4.06A4.06,4.06,0,0,1,9,12.32Z" transform="translate(-0.24)"/></svg>
							</div>
						</a>
					{{/if}}
					{{/if}}
				</div>
				{{if ~_and(caption, ~neql(caption, ''))}}
				<figcaption class="cl-lt pd-t-4 pd-l-14 pd-r-14 pd-r-md-0 pd-l-md-0 ft-sec md-sec-reg-2 sec-reg-2">{{:caption}}</figcaption>
				{{/if}}
			</figure>
		</div> --}}
		{{/if}}
		{{/if}}

		{{if (video.src != '')}}
		<div class="mg-t-20 pd-b-10 pd-l-44 pd-r-24">
			<div class="wd-full pos-rel asp-ratio-rect-ac v-obj">
				{{if (video.id)}}
				<div class="pos-abs tp-init lt-init rt-init wd-full ht-full cs-ptr play-video embed-in-place" data-video="{{:video.src}}">
					<div class="pos-rel">
						<img alt="{{:title}}" title="{{:title}}" class="wd-full dp-bl" src="{{:video.img}}">
						<div class="pos-abs tp-init lt-init rt-init bt-init solid-tint-black-black40"></div>
						<div class="cl-primary nb-icon-video-play-button tf-x-y-middle lt-half tp-half pos-abs fz-50"></div>
					</div>
				</div>
				{{else}}
				<iframe src="{{:video.src}}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen title="YouTube Frame"></iframe>
				{{/if}}
			</div>
		</div>
		{{/if}}
		<p class="ft-sec md-sec-reg-4 sec-reg-4 pd-l-14 pd-r-14 pd-r-md-0 pd-l-md-24 card-point cl-night-rider">
			{{props html}}
			<span class="dp-bl mg-t-10">{{:prop}}</span>
			{{/props}}
		</p>

	{{else (conf.cardtype == 'embed')}}
		{{if ~neql(subtitle, '')}}
		<div class="hidden-slug mg-t-20 event-card" data-hooked="{{:htmlId}}"></div>
		<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
			<div class="mg-t-20 pd-l-10 pd-l-md-14">
				<div class="slug dp-fx">
					<span class="z-index-1 bg-lt evt-connector" data-hooked="{{:htmlId}}"></span>
					<div class="cl-lt mg-l-10 mg-l-md-14">
						<div class="ft-ter md-ter-bd-2 ter-bd-2">{{:subtitle}}</div>
					</div>
				</div>
				<div class="mg-t-4 ft-sec md-sec-bd-6 sec-bd-6 cl-night-rider mg-l-4 mg-l-md-8">{{:title}}</div>
			</div>
		</div>
		{{/if}}
		{{if (conf.type)}}
			<div class="mg-t-20 pd-l-14 pd-r-14 pd-r-md-0 pd-l-md-18">
				{{if (conf.type == 'apester')}}
				{{include tmpl='embedApester'/}}
				{{else (conf.type == 'facebook')}}
				{{include tmpl='embedFacebook'/}}
				{{else (conf.type == 'infogram')}}
				{{include tmpl='embedInfogram'/}}
				{{else (conf.type == 'instagram')}}
				{{include tmpl='embedInstagram'/}}
				{{else (conf.type == 'twitter')}}
				{{include tmpl='embedTwitter'/}}
				{{/if}}
			</div>
		{{/if}}
	
	{{/if}}
	{{/for}}
	<div class="hidden-slug mg-t-20 event-card" data-hooked="{{:htmlId}}"></div>
	<div class="mg-t-20 pd-l-10 pd-l-md-14">
		<div class="slug dp-fx fx-al-ct">
			<span class="z-index-1 bg-lt evt-connector fx-ht" data-hooked="{{:htmlId}}"></span>
			<div class="cl-lt mg-l-10 mg-l-md-14 ft-ter md-ter-bd-2 ter-bd-2">{{:reactionShare.reactions.title}}</div>
		</div>
	</div>
</template>

<template type="text/x-jsrender" id="v2eventCardsListNoSlug">
	{{if ~lengthOfArr(events.list.data)}}
	<div class="pd-l-20 pd-r-20">
		<div class="pd-b-24 br-1p-grey br-b-0">
			{{for events.list.data ~array=events.list.data}}
			{{if (conf.cardtype == 'poll')}}
			{{if ~lengthOfArr(options)}}
			<div class="hidden-slug mg-t-30 event-card" data-hooked="{{:htmlId}}"></div>
			<div class="tp-px-6 pd-r-16 mg-b-6 dp-fx">
				<div class="bd-solid-venetian-red"></div>
				<div class="cl-venetian-red evt-connector wd-full fx-ht fx-al-ct dp-fx br-r-rd-4 ft-ter ter-reg-2 fx-js-bw pd-l-16">{{:subtitle}}</div>
			</div>
			<div class="tx-decoration-none" data-id="{{:htmlId}}">
				<div class="pd-l-16 pd-r-16">
					<div class="slug dp-fx fx-al-ct">
						<div class="cl-lt mg-l-10">
							<h2 class="ft-sec sec-bd-4 md-sec-bd-4">{{:title}}</h2>
						</div>
					</div>
					<!-- <div class="mg-t-4 ft-sec md-sec-bd-5 sec-bd-5 tx-ct">{{:title}}</div> -->
				</div>
			</div>
			{{if ~_and(~neql(startsAt, ''), ~neql(endsAt, ''))}}
			<div class="pd-l-16 pd-r-16 pd-b-10 options-event-parent" data-apiurl="{{:apiUrl}}" data-id="{{:htmlId}}" data-start="{{:startsAt}}" data-end="{{:endsAt}}">
			{{else}}
			<div class="pd-l-16 pd-r-16 pd-b-10 options-event-parent" data-apiurl="{{:apiUrl}}" data-id="{{:htmlId}}">
			{{/if}}
				<div class="dp-fx fx-dr-col ft-ter md-ter-bd-3 ter-bd-3 options-list">
					{{for options ~pollId=pollId}}
						<div class="pd-t-14 pd-b-14 pd-l-10 pd-r-10 dp-fx wd-dull fx-js-bw fx-al-ct mg-t-10 br-1p-pale-lavender cl-link br-rd-5 cs-ptr pos-rel poll-card-option" data-id="{{:id}}" data-score="{{:score}}" data-pollid="{{:~pollId}}" data-choice="{{:#getIndex()}}">
							<div class="pos-abs lt-init ht-full tp-init bt-init progress-bar br-rd-5"></div>
							<div class="pd-r-10 z-index-1 title" data-classlist="md-ter-reg-3 ter-reg-3 cl-ink-dk">{{:title}}</div>
							<div class="pd-l-10 z-index-1 cl-ink-dk hidden subtitle md-ter-reg-3 ter-reg-3">{{:subtitle}}</div>
						</div>
					{{/for}}
				</div>
				<div class="mg-t-10 cl-suva-grey ft-ter md-ter-reg-3 ter-reg-3 hidden poll-result">{{:pollResults.title}}</div>
				{{for options ~pollId=pollId}}
					{{if ~neql(event.title, '')}}
					<div class="br-1p-pale-lavender event hidden mg-t-20 pd-b-10 pd-l-10 pd-r-10 pd-t-10" data-id="{{:id}}">
						{{if ~neql(auxText, '')}}
						<div class="cl-link ft-ter md-ter-reg-3 ter-reg-3">{{:auxText}}</div>
						{{/if}}
						<a href="{{:event.url}}" class="dp-fx wd-full mg-t-6 fx-al-ct" target="_blank" title="{{:event.title}}">
							<div class="fx-grow-1 pd-r-10 dp-fx">
								<div class="col-4 col-lg-1 fig-container--wd-ht-lg-60">
									<figure class="fig fig--bg-img-ct-cov-props ht-full-dac ht-full-lg-ac asp-ratio-rect-ac asp-ratio-rect-lg-dac" style="background-image:url({{:event.thumb}})" title="{{:event.title}}">
									</figure>
								</div>
								<div class="col-8 col-lg-10 pd-l-10 ft-pr bd-3 md-bd-3">
									<h2 class="ft-sec sec-bd-4 md-sec-bd-4">{{:event.title}}</h2>
								</div>
							</div>
							<div class="pd-l-10">
								<span class="fz-20 nb-icon-external-link"></span>
							</div>
						</a>
					</div>
					{{/if}}
				{{/for}}
				<div class="hidden-poll-card-ad-slug hidden"></div>
			</div>
			{{/if}}
			{{/if}}
			{{if (conf.cardtype == 'quiz')}}
			{{if options}}
			<div class="pos-sticky z-index-1 rt-px-160 bt-px-90 wd-ht-0 ht-px-36 neg-mg-t-36 hidden-md take-quiz cs-ptr" data-quizid="{{:quizId}}-{{:htmlId}}">
				<div class="br-rd-15 cl-primary dp-fx fx-al-ct fx-js-ct ht-px-60 pd-l-10 pd-r-10 pd-t-10 share-link solid-tint-bright-red wd-px-150">
					<span class="fz-24 mg-r-12 nb-icon-quiz"></span>
					<span class="ft-ter pd-b-10 fz-14">{{:title}}</span>
				</div>
			</div>
			<div class="quiz-siblings svgs hidden">
				<div class="svg correct">
					<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="mg-l-6 mg-r-6">
						<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<g id="quiz-end" transform="translate(-249.000000, -281.000000)">
								<g id="Question-1" transform="translate(164.000000, 100.000000)">
									<g id="Option-1" transform="translate(20.000000, 141.000000)">
										<g id="Correct-answer" transform="translate(65.000000, 40.000000)">
											<circle id="Oval" fill="#30D158" cx="12" cy="12" r="12"></circle>
											<g id="check-good-yes" transform="translate(8.000000, 9.000000)" stroke="#FFFFFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
												<polyline id="Path" points="0 3 2.66666667 6 8 0"></polyline>
											</g>
										</g>
									</g>
								</g>
							</g>
						</g>
					</svg>
				</div>
				<div class="svg incorrect">
					<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="mg-l-6 mg-r-6">
						<title>Group 2</title>
						<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<g id="quiz-end" transform="translate(-293.000000, -281.000000)">
								<g id="Question-1" transform="translate(164.000000, 100.000000)">
									<g id="Option-1" transform="translate(20.000000, 141.000000)">
										<g id="Group-3" transform="translate(65.000000, 40.000000)">
											<g id="Group-2" transform="translate(44.000000, 0.000000)">
												<circle id="Oval" fill="#FF453A" cx="12" cy="12" r="12"></circle>
												<path d="M15,9 L12,12 M12,12 L9,15 M12,12 L9,9 M12,12 L15,15" id="Shape" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
											</g>
										</g>
									</g>
								</g>
							</g>
						</g>
					</svg>
				</div>
			</div>
			{{if ~_and(~neql(startsAt, ''), ~neql(endsAt, ''))}}
			<div class="hidden tp-init quiz-card-parent wd-ht-0" data-id="{{:htmlId}}" data-quizid="{{:quizId}}-{{:htmlId}}" data-start="{{:startsAt}}" data-end="{{:endsAt}}">
			{{else}}
			<div class="hidden tp-init quiz-card-parent wd-ht-0" data-id="{{:htmlId}}" data-quizid="{{:quizId}}-{{:htmlId}}">
			{{/if}}
				<div class="z-index-2 solid-tint-black-black60 backdrop-blur-px-5 tp-init lt-init wd-vw-100 ht-vh-100 pos-fix close-quiz" data-quizid="{{:quizId}}-{{:htmlId}}"></div>
				<div class="pos-fix lt-pr-50-px-180 tp-pr-50-px-250 z-index-2">
					<div class="pos-rel wd-px-360 ht-px-500 bg-body pd-l-30 pd-r-30 br-rd-14" data-quizid="{{:quizId}}-{{:htmlId}}">
						<div class="hidden-quiz-card-ad-slug hidden"></div>
						{{for options.questions ~quizId=quizId ~array=options.questions}}
						<div class="pd-t-20 pd-b-20 slide question ht-full ht-full-px-50" data-id="{{:id}}">
							<div class="dp-fx ovr-scroll hide-scrollbar ht-full">
								<div class="quiz-card-intermediary pos-abs lt-half hidden">
									<div class="ft-ter md-ter-bd-5 ter-bd-5 tx-ct">{{:auxText}}</div>
								</div>
								{{if ~eql(#getIndex(), 0)}}
								<div class="final">
								{{else}}
								<div class="final hidden">
								{{/if}}
									<div class="mg-t-6 mg-b-10 ft-ter md-ter-bd-1 ter-bd-1 cl-brand">{{:subtitle.toUpperCase()}}</div>
									<div class="dp-fx wd-full mg-t-6 mg-b-10 fx-al-ct">
										<div class="br-rd-3 wd-full bg-spark-red-lt">
											<div class="br-rd-3 pd-b-6 bg-brand" style="width:{{:((#getIndex()+1)*(100/~array.length))}}%"></div>
										</div>
										<div class="pd-l-8 ft-ter md-ter-bd-1 ter-bd-1">{{:(#getIndex()+2)}}/{{:~array.length+1}}</div>
									</div>

									<div class="ft-ter md-ter-bd-5 ter-bd-5 mg-t-20">{{:title}}</div>
									<div class="dp-fx fx-dr-col ft-ter md-ter-bd-3 ter-bd-3 mg-t-40 answers-list">
										{{for options ~quizId=~quizId}}
											<div class="pd-t-14 pd-b-14 pd-l-10 pd-r-10 dp-fx wd-dull fx-js-bw fx-al-ct mg-t-10 br-1p-pale-lavender br-rd-5 cs-ptr pos-rel quiz-card-option bg-primary" data-id="{{:id}}" data-answer="{{:answer}}" data-quizid="{{:~quizId}}-{{:~root.htmlId}}">
												<div class="pos-abs lt-init ht-full tp-init bt-init progress-bar br-rd-5"></div>
												<div class="dp-fx fx-align-ct fx-js-bw wd-full">
													<div class="dp-fx">
														<span class="z-index-1 cl-brand ft-ter md-ter-reg-2 ter-reg-2 pd-r-10 index">{{:~incrementChar('a', #getIndex())}}.</span>
														<div class="pd-r-10 z-index-1 title">{{:title}}</div>
													</div>
													<div class="dp-fx fx-al-ct">
														<div class="z-index-1 icon-embed hidden"></div>
													</div>
												</div>
											</div>
										{{/for}}
									</div>
								</div>
							</div>
						</div>
						{{/for}}

						<div class="slide result ht-full ht-full-px-50 pos-rel" data-id="{{:options.results.id}}">
							<div class="quiz-card-intermediary pos-abs lt-half hidden">
								<div class="ft-ter md-ter-bd-5 ter-bd-5">{{:options.results.auxText}}</div>
							</div>
							<div class="final hidden dp-fx fx-dr-col ovr-scroll hide-scrollbar ht-full pd-t-20 pd-b-20">
								<div class="dp-fx fx-dr-col fx-al-ct">
									<div class="dp-fx fx-al-ct pd-b-20">
										<div class="mg-r-20">
											<div class="results-bar wd-ht-px-50 br-rd-pr-50"></div>
										</div>
										<div class="dp-fx fx-dr-col">
											<div class="mg-t-6 ft-ter md-ter-bd-1 ter-bd-1 cl-spark-red title">-results-</div>
											<div class="mg-t-4 ft-ter md-ter-bd-5 ter-bd-5 subtitle">-remarks-</div>
										</div>
										<div class="mg-l-20">
											<div class="results-timer wd-ht-px-32 br-rd-pr-50 dp-fx fx-al-ct fx-js-ct ft-ter md-ter-bd-1 ter-bd-1 cl-spark-red">10</div>
										</div>
									</div>
								</div>

								{{if ~_and(options.results.linkWithImg.title, ~neql(options.results.linkWithImg.title, ''))}}
								<div class="mg-t-40 linkWithImg" data-id="{{:options.results.id}}">
									<a href="{{:options.results.linkWithImg.url}}" class="dp-fx wd-full mg-t-6 fx-al-ct" target="_blank" title="{{:options.results.linkWithImg.title}}">
										<div class="fx-grow-1 pd-r-10 dp-fx">
											<div class="col-4 col-lg-1 fig-container--wd-ht-lg-60">
												<figure class="fig fig--bg-img-ct-cov-props ht-full-dac ht-full-lg-ac asp-ratio-sq-ac asp-ratio-sq-lg-dac" style="background-image:url({{:options.results.linkWithImg.thumb}})" title="{{:options.results.linkWithImg.title}}">
												</figure>
											</div>
											<div class="col-8 col-lg-10 pd-l-10 ft-pr bd-3 md-bd-3">
												<h3 class="ft-pr md-bd-4 bd-4">{{:options.results.linkWithImg.title}}</h3>
											</div>
										</div>
										<div class="pd-l-10">
											<span class="fz-20 nb-icon-external-link"></span>
										</div>
									</a>
								</div>
								{{/if}}

								{{for options.questions ~quizId=quizId ~array=options.questions}}
								<div class="dp-fx wd-full mg-t-16">
									<div class="mg-r-10 pd-t-16 question-result" data-quizid="{{:~quizId}}-{{:~root.htmlId}}"></div>
									<div class="pd-t-10 pd-l-10 pd-r-10 pd-b-10 bg-primary br-rd-10 wd-full show-answer" data-id="{{:id}}" data-quizid="{{:~quizId}}-{{:~root.htmlId}}">
										<div class="dp-fx fx-js-bw fx-al-ct cs-ptr">
											<div class="">
												<div class="ft-ter md-ter-bd-2 ter-bd-2 cl-suva-grey">{{:subtitle}}</div>
												<div class="ft-ter md-ter-bd-3 ter-bd-3 mg-t-4">{{:title}}</div>
											</div>
											<div class="nb-icon-smallright show-answer-icon fz-20"></div>
											<div class="nb-icon-down hide-answer-icon fz-20 hidden"></div>
										</div>
										<div class="mg-t-10 answer hidden">
											<hr class="br-t-1p-white-smoke"/>
											{{if ~_and(description, ~neql(description, ''))}}
											<div class="ft-ter md-ter-reg-3 ter-reg-3 mg-t-10">{{:description}}</div>
											{{else}}
											{{for options ~quizId=~quizId}}
											{{if ~eql(answer, 'true')}}
											<div class="ft-ter md-ter-reg-3 ter-reg-3 mg-t-10">{{:title}}</div>
											{{/if}}
											{{/for}}
											{{/if}}
										</div>
									</div>
								</div>
								{{/for}}
							</div>
						</div>
					</div>
					<div class="dp-fx wd-full fx-js-ct pd-t-20 pos-rel">
						<div class="wd-ht-px-50 br-rd-pr-50 bg-ink-dk dp-fx fx-al-ct fx-js-ct close-quiz cs-ptr" data-quizid="{{:quizId}}-{{:htmlId}}">
							<span class="nb-icon-cancel fz-24 cl-primary mg-t-8"></span>
						</div>
					</div>
				</div>
			</div>
			{{/if}}
			{{/if}}
			{{if (conf.cardtype == 'fact')}}
				{{if ~_and(traits, ~neql(traits.type, ''))}}
				{{if ~eql(traits.type, 'scorecard')}}
				{{if ~_and(traits.sport, ~eql(traits.sport, 'tennis'))}}
				<div class="mg-t-10 pd-l-16 pd-r-16">
					<div class="pd-t-30 pd-b-30 pd-l-10 pd-r-10 ft-pr br-rd-20 cl-primary solid-tint-{{:~tagColor(tagEn + 'tint')}}">
						{{if (jumpLinkId)}}
						{{if (jumpLinkId != '')}}
						<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
						{{/if}}
						{{/if}}
						{{if (subtitle != '')}}
						<div class="dp-fx fx-al-ct">
							<h3 class="cl-primary ft-pr md-bd-7 bd-7">{{:subtitle}}</h3>
						</div>
						{{/if}}
						{{if (jumpLinkId)}}
						{{if (jumpLinkId != '')}}
						</div>
						{{/if}}
						{{/if}}
						{{if (~neql(traits.subtitle, ''))}}
						<div class="mg-t-2 ft-ter md-ter-reg-2 ter-reg-2">{{:traits.subtitle}}</div>
						{{/if}}
						{{if (~neql(traits.title, ''))}}
						<div class="ft-ter md-ter-reg-2 ter-reg-2">{{:traits.title}}</div>
						{{/if}}
						{{if ~lengthOfArr(traits.comparison.items)}}
							{{for traits.comparison.items}}
							<div class="dp-fx fx-al-ct fx-js-bw mg-t-20">
								<div class="dp-fx fx-al-ct">
									{{if ~neql(img, '')}}
									<img data-src="{{:img}}" alt="{{:subtitle}}" title="{{:subtitle}}" class="lazy wd-ht-px-40 br-rd-pr-50"/>
									{{/if}}
									{{if highlight}}
									<div class="mg-l-10">
									{{else}}
									<div class="mg-l-10 opacity-8">
									{{/if}}
										<div class="ft-ter md-ter-bd-5 ter-bd-5">{{:title}}</div>
										<div class="ft-ter md-ter-reg-1 ter-reg-1">{{:subtitle}}</div>
									</div>
								</div>
								{{if ~lengthOfArr(params)}}
								{{if highlight}}
								<div class="dp-fx fx-al-ct ft-ter md-ter-bd-5 ter-bd-5">
								{{else}}
								<div class="dp-fx fx-al-ct ft-ter md-ter-bd-5 ter-bd-5 opacity-8">
								{{/if}}
									{{for params}}
									<div class="pd-l-6 pd-r-6 pd-l-md-10 pd-r-md-10">{{:title}}</div>
									{{/for}}
								</div>
								{{/if}}
							</div>
							{{/for}}
						{{/if}}
						<div class="dp-fx fx-js-end">
							<div class="collapse-bttn open dp-fx fx-al-ct fx-js-bw br-rd-10 pd-t-14 pd-b-14 pd-l-10 pd-r-10 br-1p-pale-lavender cs-ptr wd-full wd-full-md-dac mg-t-20" data-id="{{:htmlId}}">
								<div class="ft-ter md-ter-bd-2 ter-bd-2">{{:traits.buttons[0].title}}</div>
								<div class="mg-l-20 nb-icon-angle-down fz-20 rotate-neg-90"></div>
							</div>
						</div>
						{{if ~lengthOfArr(traits.list)}}
						<div id="collapsible-{{:htmlId}}" class="ht-px-0 ovr-hidden">
							<div class="sc-stats-separator mg-t-20"></div>
							{{for traits.list}}
							{{if ~eql((#getIndex()), 0)}}
							<div class="dp-fx fx-js-bw fx-al-ct mg-t-20">
								{{for data ~title=title}}
								{{if ~eql((#getIndex() % 2), 1)}}
								{{if (~neql(~title, ''))}}
								<div class="ft-ter md-ter-reg-3 ter-reg-3 tx-ct">{{:~title}}</div>
								{{/if}}
								{{/if}}
								{{if ~eql((#getIndex() % 2), 0)}}
								<div class="ft-ter md-ter-bd-2 ter-bd-2 tx-lt">{{:title}}</div>
								{{else}}
								<div class="ft-ter md-ter-bd-2 ter-bd-2 tx-rt">{{:title}}</div>
								{{/if}}
								{{/for}}
							</div>
							{{else}}
							<div class="dp-fx fx-js-bw fx-al-ct mg-t-20">
								{{for data ~title=title}}
								{{if ~eql((#getIndex() % 2), 1)}}
								{{if (~neql(~title, ''))}}
								<div class="ft-ter md-ter-reg-3 ter-reg-3 tx-ct">{{:~title}}</div>
								{{/if}}
								{{/if}}
								{{if ~eql((#getIndex() % 2), 0)}}
								<div class="ft-ter md-ter-bd-5 ter-bd-5 tx-lt">{{:title}}</div>
								{{else}}
								<div class="ft-ter md-ter-bd-5 ter-bd-5 tx-rt">{{:title}}</div>
								{{/if}}
								{{/for}}
							</div>
							{{/if}}
							{{/for}}
						</div>
						{{/if}}
						<div class="dp-fx fx-js-end">
							<div class="collapse-bttn close dp-fx fx-al-ct fx-js-bw br-rd-10 pd-t-14 pd-b-14 pd-l-10 pd-r-10 br-1p-pale-lavender cs-ptr wd-full wd-full-md-dac hidden mg-t-20" data-id="{{:htmlId}}">
								<div class="ft-ter md-ter-bd-2 ter-bd-2">{{:traits.buttons[1].title}}</div>
								<div class="mg-l-20 nb-icon-angle-down fz-20 rotate-180"></div>
							</div>
						</div>
					</div>
				</div>
				{{else}}
				<div class="mg-t-20 pd-l-16 pd-r-16">
					<div class="pd-t-50 pd-b-50 pd-l-10 pd-r-10 ft-pr br-rd-20 cl-primary solid-tint-{{:~tagColor(tagEn + 'tint')}}">
						{{if (jumpLinkId)}}
						{{if (jumpLinkId != '')}}
						<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
						{{/if}}
						{{/if}}
						{{if (subtitle != '')}}
						<div class="dp-fx fx-al-ct">
							<h3 class="cl-primary ft-ter md-ter-bd-2 ter-bd-2">{{:subtitle}}</h3>
						</div>
						{{/if}}
						{{if (jumpLinkId)}}
						{{if (jumpLinkId != '')}}
						</div>
						{{/if}}
						{{/if}}
						{{if (~neql(traits.title, ''))}}
						<div class="tx-ct mg-t-10 ft-ter md-ter-reg-2 ter-reg-2">{{:traits.title}}</div>
						{{/if}}
						{{if (~neql(traits.subtitle, ''))}}
						<div class="tx-ct ft-ter md-ter-reg-2 ter-reg-2">{{:traits.subtitle}}</div>
						{{/if}}
						{{if ~lengthOfArr(traits.comparison.items)}}
						<div class="fx-row mg-t-10 fx-js-bw">
							{{for traits.comparison.items ~connector=traits.comparison.connector}}
							<div class="dp-fx">
								{{if ~_and(~neql(img, ''), ~eql((#getIndex() % 2), 0))}}
								<div class="dp-fx fx-dr-col fx-al-ct mg-r-10 mg-r-md-20">
									<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-20 sc-comp-img br-rd-pr-50"/>
									<div class="ft-ter md-ter-bd-6 ter-bd-6 mg-t-16">{{:title}}</div>
								</div>
								{{/if}}
								<div>
									{{if ~eql((#getIndex() % 2), 0)}}
									<div class="ft-ter md-ter-bd-3 ter-bd-3 tx-rt">{{:params.title}}</div>
									<div class="ft-ter md-ter-reg-2 ter-reg-2 tx-rt">{{:params.subtitle}}</div>
									{{else}}
									<div class="ft-ter md-ter-bd-3 ter-bd-3 tx-lt">{{:params.title}}</div>
									<div class="ft-ter md-ter-reg-2 ter-reg-2 tx-lt">{{:params.subtitle}}</div>
									{{/if}}
								</div>
								{{if ~_and(~neql(img, ''), ~neql((#getIndex() % 2), 0))}}
								<div class="dp-fx fx-dr-col fx-al-ct mg-l-10 mg-l-md-20">
									<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-20 sc-comp-img br-rd-pr-50"/>
									<div class="ft-ter md-ter-bd-6 ter-bd-6 mg-t-16">{{:title}}</div>
								</div>
								{{/if}}
							</div>
							{{if ~eql((#getIndex() % 2), 0)}}
							<div class="ft-ter md-ter-bd-2 ter-bd-2 mg-t-16">{{:~connector}}</div>
							{{/if}}
							{{/for}}
						</div>
						{{/if}}
						{{if (~neql(traits.conclusion, ''))}}
						<div class="ft-ter md-ter-reg-3 ter-reg-3 mg-t-20 tx-ct">{{:traits.conclusion}}</div>
						{{/if}}
						{{if ~lengthOfArr(traits.list)}}
							{{for traits.list}}
							<div class="ft-ter md-ter-bd-4 ter-bd-4 mg-t-20 tx-ct">{{:title}}</div>
							<div class="fx-row neg-mg-lr-px-10-ac">
								{{for data}}
								{{if ~eql((#getIndex() % 2), 0)}}
								<div class="mg-t-6 col-6 dp-fx fx-al-ct pd-l-10 pd-r-10">
								{{else}}
								<div class="mg-t-6 col-6 dp-fx fx-al-ct pd-l-10 pd-r-10 fx-js-end">
								{{/if}}
									{{if ~_and(~neql(img, ''), ~eql((#getIndex() % 2), 0))}}
									<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-20 br-rd-pr-50 mg-r-10"/>
									{{/if}}
									{{if ~eql((#getIndex() % 2), 0)}}
									<div class="ft-ter md-ter-reg-2 ter-reg-2 dp-fx fx-dr-col">
									{{else}}
									<div class="ft-ter md-ter-reg-2 ter-reg-2 dp-fx fx-dr-col fx-al-end">
									{{/if}}
										<div>{{:title}}</div>
										<div>{{:subtitle}}</div>
									</div>
									{{if ~_and(~neql(img, ''), ~neql((#getIndex() % 2), 0))}}
									<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-20 br-rd-pr-50 mg-l-10"/>
									{{/if}}
								</div>
								{{/for}}
							</div>
							{{/for}}
						{{/if}}
						{{if ~lengthOfArr(traits.card)}}
							<div class="ft-ter md-ter-bd-4 ter-bd-4 mg-t-20">{{:traits.card.title}}</div>
							{{props traits.card.text}}
							<div class="mg-t-10 ft-ter md-ter-reg-3 ter-reg-3">{{:prop}}</div>
							{{/props}}
						{{/if}}
						{{if ~lengthOfArr(traits.links)}}
							<div class="fx-row neg-mg-lr-px-10-ac">
								{{for traits.links ~len=traits.links.length}}
								{{if ~compareGT(~len, 1)}}
								<div class="pd-l-10 pd-r-10 col-12 mg-t-30 col-md-6">
								{{else}}
								<div class="pd-l-10 pd-r-10 col-12 mg-t-30">
								{{/if}}
									<a class="dp-bl wd-full ft-ter md-ter-bd-2 ter-bd-2 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{{:url}}" target="_blank" aria-label="Button Link">
										<div>{{:title}}</div>
										<span class="nb-icon-external-link fz-20 mg-l-6"></span>
									</a>
								</div>
								{{/for}}
							</div>
						{{/if}}
					</div>
				</div>
				{{/if}}
				{{else ~eql(traits.type, 'rating')}}
				<div class="mg-t-20 pd-l-16 pd-r-16">
					<div class="pd-t-50 pd-b-50 pd-l-10 pd-r-10 ft-pr br-rd-20 cl-primary solid-tint-{{:~tagColor(tagEn + 'tint')}}">
						{{if (jumpLinkId)}}
						{{if (jumpLinkId != '')}}
						<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
						{{/if}}
						{{/if}}
						{{if (subtitle != '')}}
						<div class="dp-fx fx-al-ct">
							<h3 class="cl-primary ft-ter md-ter-bd-3 ter-bd-3">{{:subtitle}}</h3>
						</div>
						{{/if}}
						{{if (jumpLinkId)}}
						{{if (jumpLinkId != '')}}
						</div>
						{{/if}}
						{{/if}}
						{{if (~neql(traits.title, ''))}}
						<div class="ft-sec md-sec-bd-5 sec-bd-5 mg-t-6 cl-primary">{{:traits.title}}</div>
						{{/if}}
						{{if ~lengthOfArr(traits.ratings.keys)}}
						<div class="pd-t-10" title="{{:traits.ratings.value}}">
							<div class="cl-primary ft-ter md-ter-reg-2 ter-reg-2">{{:traits.title}}</div>
							<div class="fx-row">
								{{props traits.ratings.keys}}
								<div class="svg-inclusion mg-r-4">
									{{if ~eql(prop, 'star')}}
									<svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg star">
										<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<g id="Group-3" fill="#BBC2BD">
												<path d="M9.8316,0.5748 L11.7896,4.7468 C11.9316,5.0488 12.2146,5.2608 12.5436,5.3108 L17.0026,5.9918 C17.8076,6.1148 18.1356,7.0958 17.5666,7.6788 L14.2786,11.0488 C14.0576,11.2768 13.9566,11.5958 14.0076,11.9078 L14.7746,16.6138 C14.9096,17.4378 14.0346,18.0538 13.3046,17.6498 L9.4106,15.4978 C9.1086,15.3308 8.7436,15.3308 8.4426,15.4978 L4.5486,17.6498 C3.8176,18.0538 2.9426,17.4378 3.0776,16.6138 L3.8446,11.9078 C3.8956,11.5958 3.7956,11.2768 3.5736,11.0488 L0.2856,7.6788 C-0.2834,7.0958 0.0456,6.1148 0.8506,5.9918 L5.3086,5.3108 C5.6386,5.2608 5.9206,5.0488 6.0626,4.7468 L8.0206,0.5748 C8.3806,-0.1922 9.4716,-0.1922 9.8316,0.5748" id="Fill-1"></path>
											</g>
										</g>
									</svg>
									{{else ~eql(prop, 'filled_star')}}
									<svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg filled_star">
										<defs>
											<linearGradient x1="50%" y1="-1.12155278%" x2="50%" y2="98.8784472%" id="linearGradient-1">
												<stop stop-color="#FDD800" offset="0%"></stop>
												<stop stop-color="#EBC10E" offset="100%"></stop>
											</linearGradient>
										</defs>
										<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<path d="M9,0 C8.588,0 8.176,0.219 7.972,0.656 L7.972,0.656 L6.099,4.665 C5.939,5.009 5.618,5.251 5.243,5.307 L5.243,5.307 L0.965,5.964 C0.051,6.105 -0.321,7.224 0.324,7.889 L0.324,7.889 L3.49,11.149 C3.742,11.408 3.857,11.773 3.798,12.13 L3.798,12.13 L3.06,16.673 C2.942,17.403 3.514,17.99 4.164,18 L4.164,18 L4.2,18 C4.376,17.997 4.557,17.952 4.73,17.855 L4.73,17.855 L8.45,15.789 C8.792,15.599 9.207,15.599 9.549,15.789 L9.549,15.789 L13.27,17.855 C13.444,17.952 13.625,17.997 13.801,18 L13.801,18 L13.836,18 C14.486,17.99 15.058,17.403 14.939,16.673 L14.939,16.673 L14.202,12.13 C14.143,11.773 14.258,11.408 14.51,11.149 L14.51,11.149 L17.676,7.889 C18.321,7.224 17.949,6.105 17.035,5.964 L17.035,5.964 L12.757,5.307 C12.382,5.251 12.062,5.009 11.901,4.665 L11.901,4.665 L10.028,0.656 C9.824,0.219 9.412,0 9,0 Z" id="Fill-1" fill="url(#linearGradient-1)"></path>
										</g>
									</svg>
									{{else ~eql(prop, 'half_filled_star')}}
									<svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg half_filled_star">
										<defs>
											<linearGradient x1="49.9996654%" y1="-1.12155278%" x2="49.9996654%" y2="98.8784472%" id="linearGradient-1">
												<stop stop-color="#FDD800" offset="0%"></stop>
												<stop stop-color="#EBC10E" offset="100%"></stop>
											</linearGradient>
										</defs>
										<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<path d="M9,0 C8.589,0 8.176,0.219 7.972,0.656 L7.972,0.656 L6.099,4.665 C5.939,5.009 5.618,5.251 5.243,5.307 L5.243,5.307 L0.965,5.964 C0.051,6.105 -0.321,7.224 0.324,7.889 L0.324,7.889 L3.49,11.149 C3.742,11.408 3.857,11.773 3.798,12.13 L3.798,12.13 L3.06,16.673 C2.942,17.403 3.514,17.99 4.164,18 L4.164,18 L4.2,18 C4.376,17.997 4.557,17.952 4.73,17.855 L4.73,17.855 L8.451,15.789 C8.621,15.694 8.811,15.646 9,15.646 L9,15.646 L9,0 Z" id="Fill-1" fill="url(#linearGradient-1)"></path>
											<path d="M8.9999,15.6465 C9.1889,15.6465 9.3779,15.6935 9.5489,15.7885 L13.2699,17.8545 C14.0989,18.3155 15.0919,17.6125 14.9389,16.6725 L14.2019,12.1295 C14.1439,11.7725 14.2579,11.4085 14.5099,11.1495 L17.6759,7.8895 C18.3219,7.2245 17.9489,6.1045 17.0349,5.9645 L12.7569,5.3075 C12.3829,5.2505 12.0619,5.0095 11.8999,4.6645 L10.0279,0.6565 C9.8239,0.2185 9.4119,0.0005 8.9999,0.0005 L8.9999,15.6465 Z" id="Fill-4" fill="#BBC2BD"></path>
										</g>
									</svg>
									{{/if}}
								</div>
								{{/props}}
							</div>
						</div>
						{{/if}}
						{{if ~neql(traits.img, '')}}
						<div class="dp-fx fx-js-ct mg-t-20">
							<img data-src="{{:traits.img}}" alt="{{:traits.title}}" title="{{:traits.title}}" class="lazy wd-ht-px-200"/>
						</div>
						{{/if}}
						{{if ~lengthOfArr(traits.card.text)}}
						{{props traits.card.text}}
						<div class="ft-sec md-sec-reg-4 sec-reg-4 cl-primary mg-t-10">{{:prop}}</div>
						{{/props}}
						{{/if}}
						{{if ~lengthOfArr(traits.list)}}
						<div class="fx-row neg-mg-lr-px-10-ac cl-primary">
							{{for traits.list}}
							<div class="col-12 col-md-6 pd-l-10 pd-r-10 mg-t-20">
								<div class="ft-ter md-ter-bd-4 ter-bd-4">{{:title}}</div>
								{{props data}}
								<div class="pos-rel mg-t-10">
									<div class="ft-sec md-sec-reg-4 sec-reg-4 pd-t-6 pd-b-6 pd-l-20">{{:prop}}</div>
									<div class="gd-tint-white-white0-white100-half-1 pos-abs lt-init bt-init tp-init wd-px-90"></div>
								</div>
								{{/props}}
							</div>
							{{/for}}
						</div>
						{{/if}}
						{{if ~lengthOfArr(traits.links)}}
							<div class="fx-row neg-mg-lr-px-10-ac">
								{{for traits.links ~len=traits.links.length}}
								{{if ~compareGT(~len, 1)}}
								<div class="pd-l-10 pd-r-10 col-12 mg-t-30 col-md-6">
								{{else}}
								<div class="pd-l-10 pd-r-10 col-12 mg-t-30">
								{{/if}}
									<a class="dp-bl wd-full ft-ter md-ter-bd-2 ter-bd-2 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{{:url}}" target="_blank" aria-label="Button Link">
										<div>{{:title}}</div>
										<span class="nb-icon-external-link fz-20 mg-l-6"></span>
									</a>
								</div>
								{{/for}}
							</div>
						{{/if}}
					</div>
				</div>
				{{/if}}
				{{else}}
					{{if ~eql(tag, 'whydoesitmatter')}}
						<div class="hidden-slug event-card mg-t-20" data-hooked="{{:htmlId}}"></div>
						<div class="tp-px-6 pd-r-16 mg-b-6 dp-fx">
							<div class="bd-solid-venetian-red"></div>
							<div class="cl-venetian-red evt-connector wd-full fx-ht fx-al-ct dp-fx br-r-rd-4 ft-ter ter-reg-2 fx-js-bw pd-l-16">{{:subtitle}}</div>
						</div>
						<div id="{{:jumpLinkId}}" class="jump-at" data-id="{{:htmlId}}">
							<div class="dp-fx fx-al-ct pd-l-16 pd-r-16">
								<h2 class="ft-sec sec-bd-4 md-sec-bd-4 cl-night-rider wd-full">{{:title}}</h2>
							</div>
						</div>
						<p class="ft-sec md-sec-reg-4 sec-reg-4 pd-l-16 pd-r-16 card-point cl-lt">
							{{props html}}
							<span class="dp-bl mg-t-10">{{:prop}}</span>
							{{/props}}
						</p>
					{{else}}
						{{if ~neql(subtitle, '')}}
						<div class="hidden-slug event-card mg-t-20" data-hooked="{{:htmlId}}"></div>
						<div class="tp-px-6 pd-r-16 mg-b-6 dp-fx">
							<div class="bd-solid-venetian-red"></div>
							<div class="cl-venetian-red evt-connector wd-full fx-ht fx-al-ct dp-fx br-r-rd-4 ft-ter ter-reg-2 fx-js-bw pd-l-16">{{:subtitle}}</div>
						</div>
						<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
							<div class="dp-fx fx-al-ct pd-l-16 pd-r-16">
								<h2 class="ft-sec sec-bd-4 md-sec-bd-4 cl-night-rider wd-full"><u>{{:title}}</u></h2>
							</div>
							<!-- <div class="mg-t-4 ft-sec md-sec-bd-5 sec-bd-5 tx-ct"><u>{{:title}}</u></div> -->
						</div>
						{{/if}}
						<p class="ft-sec md-sec-reg-4 sec-reg-4 pd-l-16 pd-r-16 card-point cl-lt">
							{{props html}}
							<span class="dp-bl mg-t-10">{{:prop}}</span>
							{{/props}}
						</p>
					{{/if}}
				{{/if}}
			{{else (conf.cardtype == 'event')}}
				{{if ~neql(subtitle, '')}}
				<div class="hidden-slug event-card mg-t-20" data-hooked="{{:htmlId}}"></div>
				<div class="tp-px-6 pd-r-16 mg-b-6 dp-fx">
					<div class="bd-solid-venetian-red"></div>
					<div class="cl-venetian-red evt-connector wd-full fx-ht fx-al-ct dp-fx br-r-rd-4 ft-ter ter-reg-2 fx-js-bw pd-l-16">{{:subtitle}}</div>
				</div>
				<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
					<div class="dp-fx fx-al-ct pd-l-16 pd-r-16">
						<div class="cl-lt">
							<h2 class="ft-sec sec-bd-4 md-sec-bd-4 cl-night-rider">{{:title}}</h2>
						</div>
					</div>
				</div>
				{{/if}}

				{{if ~neql(title, '')}}
				{{if ~_and(slides, ~lengthOfArr(slides))}}
				<div class="mg-t-10 pd-l-16 pd-r-16">
					<section class="slide-show-parent visibility-hidden">
						<section class="slide-show default with-centerMode {{:htmlId}}" id="slideshow-{{:~randomId(6)}}">
							{{for slides}}
							<figure class="min-wd-px-100 mg-r-20">
								<img alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full" src="{{:img}}">
								{{if ~_and(caption, ~neql(caption, ''))}}
								<figcaption class="cl-lt pd-t-4 ft-sec md-sec-reg-2 sec-reg-2">{{:caption}}</figcaption>
								{{/if}}
							</figure>
							{{/for}}
						</section>
						<div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
					</section>
				</div>
				{{!-- {{else ~_and(~neql(ximg, ''), ~eql(video.src, ''))}}
				<div class="mg-t-10 pd-l-md-18">
					<figure>
						<div class="pos-rel asp-ratio r-16-9">
							{{if ~_and(~neql(imageFallback, undefined), imageFallback)}}
							<picture>
								<source
									media="(max-width: 599px) and (min-device-pixel-ratio: 3),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 3),(max-width: 599px) and (min-resolution: 3dppx),(max-width: 599px) and (min-resolution: 288dpi),(min-width: 768px) and (min-device-pixel-ratio: 2),(min-width: 768px) and (-webkit-min-device-pixel-ratio: 2),(min-width: 768px) and (min-resolution: 3dppx),(min-width: 768px) and (min-resolution: 288dpi)"
									srcset="{{:ximg}}">
								<source
									media="(min-width: 1080px) and (min-device-pixel-ratio: 1),(min-width: 1080px) and (-webkit-min-device-pixel-ratio: 1),(min-width: 1080px) and (min-resolution: 2dppx),(min-width: 1080px) and (min-resolution: 192dpi)"
									srcset="{{:limg}}">
								<source
									media="(max-width: 599px) and (min-device-pixel-ratio: 2),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 2),(max-width: 599px) and (min-resolution: 2dppx),(max-width: 599px) and (min-resolution: 192dpi)"
									srcset="{{:img}}">
								<source
									media="(max-width: 599px) and (min-device-pixel-ratio: 2),(max-width: 599px) and (-webkit-min-device-pixel-ratio: 2),(max-width: 599px) and (min-resolution: 2dppx),(max-width: 599px) and (min-resolution: 192dpi)"
									srcset="{{:img}}">
								<img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full lazy cover-card-img"/>
							</picture>
							{{else}}
							<img data-src="{{:ximg}}" alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full lazy"/>
							{{/if}}
							{{if (imagecredit)}}
							{{if ~_and(~neql(imagecredit, null), imagecredit.title)}}
								<a class="pos-abs tx-decoration-none image-credits" href="{{:imagecredit.url}}" target="_blank" rel="nofollow noopener">
									<div class="dp-fx fx-al-ct">
										<div class="cl-primary ft-ter md-ter-reg-1 ter-reg-1 mg-r-4">{{:imagecredit.title}}</div>
										<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 -2 17.52 16" height="10"><defs></defs><circle class="svg" cx="8.76" cy="8.26" r="2.87"/><path class="svg" d="M15.78,2.52H13.5L13,1a1.48,1.48,0,0,0-1.41-1H6.39A1.48,1.48,0,0,0,5,1L4.5,2.52H2.22a2,2,0,0,0-2,2V12a2,2,0,0,0,2,2H15.78a2,2,0,0,0,2-2V4.49A2,2,0,0,0,15.78,2.52ZM9,12.32a4.06,4.06,0,1,1,4.06-4.06A4.06,4.06,0,0,1,9,12.32Z" transform="translate(-0.24)"/></svg>
									</div>
								</a>
							{{/if}}
							{{/if}}
						</div>
						{{if ~_and(caption, ~neql(caption, ''))}}
						<figcaption class="cl-lt pd-t-4 pd-l-14 pd-r-14 pd-r-md-0 pd-l-md-0 ft-sec md-sec-reg-2 sec-reg-2">{{:caption}}</figcaption>
						{{/if}}
					</figure>
				</div> --}}
				{{/if}}
				{{/if}}

				{{if (video.src != '')}}
				<div class="mg-t-20 pd-b-10 pd-l-16 pd-r-16">
					<div class="wd-full pos-rel asp-ratio-rect-ac v-obj">
						{{if (video.id)}}
						<div class="pos-abs tp-init lt-init rt-init wd-full ht-full cs-ptr play-video embed-in-place" data-video="{{:video.src}}">
							<div class="pos-rel">
								<img alt="{{:title}}" title="{{:title}}" class="wd-full dp-bl" src="{{:video.img}}">
								<div class="pos-abs tp-init lt-init rt-init bt-init solid-tint-black-black40"></div>
								<div class="cl-primary nb-icon-video-play-button tf-x-y-middle lt-half tp-half pos-abs fz-50"></div>
							</div>
						</div>
						{{else}}
						<iframe src="{{:video.src}}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen title="YouTube Frame"></iframe>
						{{/if}}
					</div>
				</div>
				{{/if}}
				<p class="ft-sec md-sec-reg-4 sec-reg-4 pd-l-16 pd-r-16 card-point cl-lt">
					{{props html}}
					<span class="dp-bl mg-t-10">{{:prop}}</span>
					{{/props}}
				</p>

			{{else (conf.cardtype == 'embed')}}
				{{if ~neql(subtitle, '')}}
				<div class="hidden-slug event-card mg-t-20" data-hooked="{{:htmlId}}"></div>
				<div class="tp-px-6 pd-r-16 mg-b-6 dp-fx">
					<div class="bd-solid-venetian-red"></div>
					<div class="cl-venetian-red evt-connector wd-full fx-ht fx-al-ct dp-fx br-r-rd-4 ft-ter ter-reg-2 fx-js-bw pd-l-16">{{:subtitle}}</div>
				</div>
				<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
					<div class="dp-fx fx-al-ct pd-l-16 pd-r-16">
						<div class="cl-lt">
							<!-- <div class="ft-ter md-ter-bd-2 ter-bd-2">{{:subtitle}}</div> -->
							<h2 class="ft-sec sec-bd-4 md-sec-bd-4 cl-night-rider">{{:title}}</h2>
						</div>
					</div>
				</div>
				{{/if}}
				{{if (conf.type)}}
					<div class="mg-t-20 pd-l-16 pd-r-16">
						{{if (conf.type == 'apester')}}
						{{include tmpl='embedApester'/}}
						{{else (conf.type == 'facebook')}}
						{{include tmpl='embedFacebook'/}}
						{{else (conf.type == 'infogram')}}
						{{include tmpl='embedInfogram'/}}
						{{else (conf.type == 'instagram')}}
						{{include tmpl='embedInstagram'/}}
						{{else (conf.type == 'twitter')}}
						{{include tmpl='embedTwitter'/}}
						{{/if}}
					</div>
				{{/if}}
			
			{{/if}}

			{{if ~neql(#getIndex(), ~array.length - 1)}}
			<div class="pd-l-16 pd-r-16 mg-t-16 mg-b-16 dp-fx fx-al-ct">
				<div class="br-1p-pale-lavender ht-px-0 fx-grow-1"></div>
				{{if ~neql(~root.tlseparator.articleRead, null)}}
				<div class="ft-ter ter-reg-1 cl-lt tx-ct pd-l-10 pd-r-10">{{:~root.tlseparator.articleRead.title.replace(':read_pr:', '<span class="cl-venetian-red ter-bd-1">' + ~parseInt((#getIndex()+1)*(100/~array.length)) + '%</span>')}}</div>
				{{/if}}
				<div class="br-1p-pale-lavender ht-px-0 fx-grow-1"></div>
			</div>
			{{/if}}
			{{/for}}
		</div>
		<div class="dp-fx fx-al-ct mg-t-neg-px-8">
			<div class="br-1p-pale-lavender ht-px-0 fx-grow-1"></div>
			{{if ~neql(tlseparator.articleRead, null)}}
			<div class="ft-ter tx-ct cl-venetian-red ter-bd-1 pd-l-10 pd-r-10">{{:tlseparator.articleRead.subtitle}}</div>
			{{/if}}
			<div class="br-1p-pale-lavender ht-px-0 fx-grow-1"></div>
		</div>
	</div>

	<!-- {{if ~neql(reactionShare.reactions, null)}}
	<div class="mg-t-20 event-card" data-hooked="{{:htmlId}}"></div>
	<div class="mg-t-20 pd-l-10 pd-l-md-14">
		<div class="slug dp-fx fx-al-ct">
			<div class="cl-lt mg-l-10 mg-l-md-14 ft-ter md-ter-bd-2 ter-bd-2">{{:reactionShare.reactions.title}}</div>
		</div>
	</div>
	{{/if}} -->

	{{if ~neql(reactionShare.like, null)}}
	<div class="mg-t-16" data-hooked="{{:htmlId}}"></div>
	<div class="pd-l-10 dp-fx">
		<div class="dp-fx fx-al-ct">
			<div class="cl-lt ft-ter md-ter-bd-2 ter-bd-2 mg-t-5">Like</div>
			<div id="{{$htmlId}}" class="like-button cs-ptr mg-l-10">
				<svg xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" width="22px" height="24px" viewBox="0 0 20 20" version="1.1">
					<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g transform="translate(-205.000000, -1816.000000)">
							<g transform="translate(205.000000, 1816.000000)">
								<g opacity="0.5"/>
								<path d="M5.71326365,16.5202541 L5.71333194,16.5202543 L5.80657195,16.5122884 L15.5518157,16.5208333 C15.7969476,16.5208333 16.0325089,16.4550454 16.2374401,16.3255529 C16.4318687,16.2027944 16.5851463,16.031875 16.7312347,15.754871 C16.7335798,15.7514742 16.7340402,15.7504996 16.7283256,15.7734895 L18.1167189,9.70101989 L18.1217035,9.6807781 C18.1219154,9.67997572 18.1221066,9.6777149 18.1216133,9.58037803 C18.1409489,9.31469824 18.0777629,9.05240955 17.9341104,8.82259024 C17.708647,8.46168589 17.3182617,8.2421618 16.8174174,8.21803258 C16.8146763,8.21759495 16.8149673,8.21762045 16.8181348,8.21762026 L11.5186772,8.21344368 L11.811795,7.38145699 C11.8486835,7.27677795 11.8486835,7.27677795 11.8833796,7.17669483 C12.1290542,6.46191353 12.2192917,5.8971525 12.2192917,4.66299959 C12.2192917,3.60025047 11.6405721,3.125 10.7862045,3.125 C10.1918702,3.125 9.7425251,3.62234401 9.52507629,4.50233823 C9.05565256,6.4020504 7.34641579,8.70369494 5.35870124,8.85534437 L2.31904936,8.85597601 C2.30078293,8.85412917 2.30816488,8.85471231 2.31499811,8.85471231 C2.30433285,8.85471231 2.29788567,8.8611118 2.29788547,8.86699625 L2.29166667,16.509058 C2.29166667,16.5141193 2.2978035,16.5202004 2.30909006,16.5202004 C2.31198891,16.5202004 2.31249852,16.5201611 2.41139271,16.5122881 L5.70895097,16.5202437 C5.70681272,16.5203522 5.70383489,16.5206829 5.69946485,16.521439 L5.71331303,16.5202559 C5.71329668,16.5202553 5.71328021,16.5202547 5.71326364,16.5202541 Z" id="Shape" stroke="#AAAAAA" stroke-width="1.25" fill-rule="nonzero"/>
								<path d="M5.83333333,8.75 L5.83333333,16.25" id="Line" stroke="#AAAAAA" stroke-width="1.25"/>
							</g>
						</g>
					</g>
				</svg>
				<svg xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" width="22px" height="24px" viewBox="0 0 20 20" version="1.1">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g transform="translate(-231.000000, -1816.000000)">
							<g transform="translate(231.000000, 1816.000000)">
								<g opacity="0.5"/>
								<path d="M18.2248528,11.297498 L17.1689045,15.8522523 C17.1490817,15.9248435 17.1158307,15.9910948 17.0742669,16.0497384 C16.9220796,16.344224 16.6890027,16.6054254 16.3807913,16.7949867 C16.0553149,16.9953257 15.6911523,17.0872534 15.3327446,17.0872534 C9.81981536,17.0937345 6.71787144,17.0937345 6.02691286,17.0872534 C5.61795419,17.0834173 5.34961576,16.912789 5.34961576,16.438962 C5.34961576,13.2382223 5.34961576,9.65232702 5.34961576,8.8015823 C5.34961576,8.38132847 5.5229843,8.2254723 5.7618772,8.14048383 C6.00658933,8.05342512 7.82942675,8.03962797 8.01026201,7.98469473 C8.94352109,7.70119394 9.15048857,7.34623232 9.63929933,6.62998313 C9.9560204,6.16589514 10.0941099,5.43284059 10.175168,4.86628533 C10.195154,4.72659328 10.2023152,4.32959091 10.2023152,4.27348331 C10.2023152,3.83389767 10.2724493,3.54396672 10.353519,3.22320709 C10.4192948,2.96295916 10.839118,2.50624445 11.489825,2.50021028 C11.9909011,2.49556368 12.1397169,2.56880078 12.2671689,2.63292751 C12.758309,2.88004163 13.0320362,3.32010079 13.1595985,3.85441981 C13.2772559,4.32293407 13.3360847,4.81078485 13.3360847,5.31131532 C13.3360847,6.08382497 13.1998833,6.82495241 12.950181,7.51536111 L16.6352895,7.51884803 C16.66918,7.51884803 16.7027507,7.52201795 16.7353623,7.52708982 C17.3639345,7.55118122 17.9659987,7.86840875 18.327255,8.41910548 C18.8079804,9.1519202 18.2443558,11.2255408 18.2248528,11.297498 Z" id="Shape-Copy" fill="#456FFF" fill-rule="nonzero"/>
								<path d="M3.10966097,8.1158117 L3.48465529,8.1158117 C3.80682139,8.1158117 4.06798861,8.37697893 4.06798861,8.69914503 C4.06798861,8.69975276 4.06798766,8.70036049 4.06798576,8.70096822 L4.04348417,16.5402547 C4.04247947,16.8617074 3.78160802,17.1217648 3.4601537,17.1217648 L3.08515937,17.1217648 C2.76299328,17.1217648 2.50182605,16.8605976 2.50182605,16.5384315 C2.50182605,16.5378237 2.501827,16.537216 2.5018289,16.5366083 L2.52633049,8.69732183 C2.52733519,8.37586908 2.78820664,8.1158117 3.10966097,8.1158117 Z" id="Rectangle-11" fill="#456FFF"/>
							</g>
						</g>
					</g>
				</svg>
			</div>
		</div>
	</div>
	{{/if}}
	{{/if}}
</template>

<template type="text/x-jsrender" id="v2articleCardBelowItems">
	<div class="dp-fx fx-js-ct wd-full mg-t-20 cl-primary">
		<div class="pd-l-10 pd-r-10">
			<a class="dp-fx fx-al-ct fx-js-ct wd-ht-px-40 bg-white-smoke cl-ink-dk br-rd-pr-50" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{:reactionShare.socialShare.url}}" rel="nofollow noreferrer" aria-label="Facebook">
				<span class="nb-icon-facebook fz-16 mg-t-6"></span>
				<span class="hidden">Facebook</span>
			</a>
		</div>
		<div class="pd-l-10 pd-r-10 hidden-md">
			<a class="dp-fx fx-al-ct fx-js-ct wd-ht-px-40 bg-white-smoke cl-ink-dk br-rd-pr-50" target="_blank" href="whatsapp://send?text={{:~rawUrlEncode(reactionShare.socialShare.text)}}%20{{:reactionShare.socialShare.url}}" rel="nofollow noreferrer" aria-label="Whatsapp">
				<span class="nb-icon-whatsapp fz-16 mg-t-6"></span>
				<span class="hidden">Whatsapp</span>
			</a>
		</div>
		<div class="pd-l-10 pd-r-10">
			{{if reactionShare.socialShare.source}}
			<a class="dp-fx fx-al-ct fx-js-ct wd-ht-px-40 bg-white-smoke cl-ink-dk br-rd-pr-50" target="_blank" href="https://twitter.com/intent/tweet?via={{reactionShare.socialShare.source}}&text={{:~rawUrlEncode(reactionShare.socialShare.text)}}&url={{:reactionShare.socialShare.url}}" rel="nofollow noreferrer" aria-label="Twitter">
			{{else}}
			<a class="dp-fx fx-al-ct fx-js-ct wd-ht-px-40 bg-white-smoke cl-ink-dk br-rd-pr-50" target="_blank" href="https://twitter.com/intent/tweet?via=NewsBytesApp&text={{:~rawUrlEncode(reactionShare.socialShare.text)}}&url={{:reactionShare.socialShare.url}}" rel="nofollow noreferrer" aria-label="Twitter">
			{{/if}}
				<span class="nb-icon-twitter fz-16 mg-t-6"></span>
				<span class="hidden">Twitter</span>
			</a>
		</div>
		<div class="pd-l-10 pd-r-10">
			<a class="dp-fx fx-al-ct fx-js-ct wd-ht-px-40 bg-white-smoke cl-ink-dk br-rd-pr-50" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&source=newsbytes&url={{:reactionShare.socialShare.url}}" rel="nofollow noreferrer" aria-label="Linkedin">
				<span class="nb-icon-linkedin fz-16 mg-t-6"></span>
				<span class="hidden">Linkedin</span>
			</a>
		</div>
	</div>
	<div class="pd-l-24 pd-r-24 mg-t-20">
		<div class="bg-white-smoke pd-t-10 pd-b-10 pd-l-14 pd-r-14">
			<div class="slide-show-parent visibility-hidden">
				<div class="slide-show default {{:htmlId}}" id="slideshow-{{:~randomId(6)}}">
					{{for cover.list.data[0].creators}}
					<div class="min-wd-px-100 mg-r-10">
						<div class="fx-row fx-al-ct">
							<a class="mg-t-2 dp-fx fx-al-ct mg-r-10" href="{{:url}}">
								{{if ~neql(icon, '')}}
								<img data-src="{{:icon}}" alt="{{:title}}" title="{{:title}}" class="br-rd-pr-50 wd-ht-px-36 mg-r-10 lazy" />
								{{/if}}
								<div class="cl-ink-dk ft-ter md-ter-bd-2 ter-bd-2">{{:title}}</div>
							</a>
							<div class="dp-fx mg-t-2">
								{{for profiles}}
									<a target="_blank" class="dp-fx wd-ht-px-50 fx-al-ct mg-l-10 tx-decoration-none" href="{{:url}}" rel="nofollow noreferrer" aria-label="Link to {{:key}} profile">
										<span class="hidden">{{:~capitalize(key)}}</span>
										<div class="nb-icon-{{:key}} fz-14" ></div>
									</a>
								{{/for}}
							</div>
						</div>
						{{if ~_and(description, ~neql(description, ''))}}
						<a class="mg-t-6 line-clamp-2 ft-sec md-sec-reg-2 sec-reg-2 cl-lt dp-bl" href="{{:url}}">{{:description}}</a>
						{{/if}}
					</div>
					{{/for}}
				</div>
				<div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
			</div>
		</div>
	</div>
</template>

<template type="text/x-jsrender" id="v2articleCardSideItems">
	{{if sliderNews}}
	<div class="bg-primary pd-t-10 pd-b-10 mg-t-20 bg-body slider-news">
		<div class="ft-ter md-ter-bd-4 ter-bd-4 pd-l-20 pd-r-20 pd-t-10 mg-b-20">{{:sliderNews.title}}</div>
		<div class="tabs wht-sp-nowrap pos-rel fz-0" id="sliderNews-{{:sliderNews.id}}">
			<div class="pos-rel">
				<div class="hz-scroll-ac scroll-menu dp-fx tabs-list slider-tabs">
					{{for sliderNews.tabularSection}}
					{{if ~eql(#getIndex(), 0)}}
					<div class="cs-ptr cl-suva-grey ft-ter md-ter-bd-2 ter-bd-2 pd-t-8 pd-b-8 pd-l-24 pd-r-24 tab-item active" data-id="tbc-{{:#getIndex()}}">{{:tab.title}}</div>
					{{else}}
					<div class="cs-ptr cl-suva-grey ft-ter md-ter-bd-2 ter-bd-2 pd-t-8 pd-b-8 pd-l-24 pd-r-24 tab-item" data-id="tbc-{{:#getIndex()}}">{{:tab.title}}</div>
					{{/if}}
					{{/for}}
				</div>
			</div>
			<div class="pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac wht-sp-normal">
				{{for sliderNews.tabularSection}}
				{{if ~eql(#getIndex(), 0)}}
				<div class="tab-content" data-id="tbc-{{:#getIndex()}}">
				{{else}}
				<div class="tab-content hidden" data-id="tbc-{{:#getIndex()}}">
				{{/if}}
					{{if tab.url}}
					<h3><a class="ft-ter md-ter-bd-4 ter-bd-4 pd-t-20 pd-l-md-20 pd-r-md-20 dp-bl cl-link" href="{{:tab.url}}">{{:tab.title}}</a></h3>
					{{else}}
					<h3 class="ft-ter md-ter-bd-4 ter-bd-4 pd-t-20 pd-l-md-20 pd-r-md-20">{{:tab.title}}</h3>
					{{/if}}
					<div class="pd-l-md-10 pd-r-md-10">
						{{for content.data.list.data}}
						<a href="{{:url}}" class="col-12 dp-fx pd-l-md-10 pd-r-md-10 mg-t-20 list-item-sqrRecImgVertical">
							<div class="col-4 col-lg-1 fig-container--wd-ht-lg-60">
								<figure class="lazy fig fig--bg-img-ct-cov-props ht-full-dac ht-full-lg-ac asp-ratio-rect-ac asp-ratio-rect-lg-dac" data-src="{{:thumb}}" title="{{:title}}">
									<img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="lazy hidden inset-img-hidden"/>
								</figure>
							</div>
							<div class="col-8 col-lg-10 tx-container pd-l-10">
								<h3 class="ft-pr md-bd-3 bd-3">{{:title}}</h3>
								<div class="cl-lt cl-{{:~tagColor(tagColor)}}-dk mg-t-2 ft-ter md-ter-bd-2 ter-bd-2">{{:tag}}</div>
							</div>
						</a>
						{{/for}}
					</div>
					<!-- {{if content.link}}
					<a href="{{:content.link.url}}" class="dp-fx fx-js-ct pd-t-10 pd-b-10 mg-t-10" aria-label="Button Link">
						<span class="cl-link ft-ter md-ter-bd-2 ter-bd-2">{{:content.link.title}}</span>
					</a>
					{{/if}} -->
				</div>
				{{/for}}
			</div>
		</div>
	</div>
	<style type="text/css">
		.tabs#sliderNews-{{:sliderNews.id}} .tab-item.active {
			color: #000000;
			border-bottom: 1px solid #000000;
		}
	</style>
	{{/if}}
</template>

<template type="text/x-jsrender" id="v2articleCardBreadCrumbs">
	{{if ~_and(isReviewTimeline, isReviewTimeline)}}
	{{else}}
	{{if breadcrumbs}}
	{{include tmpl='breadcrumbs'/}}
	{{/if}}
	{{/if}}
</template>

<template type="text/x-jsrender" id="v2sqrRecImg">
	{{if (~lengthOfArr(~data.list.data))}}
	<div class="bg-primary pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac news-list" data-cname="sqrRecImg">
		<div class="ft-pr md-bd-4 bd-4 c-tt">{{:~data.title}}</div>
		<ul>
			{{for ~data.list.data}}
			<li>
				<a href="{{:url}}" class="dp-fx">
				<div class="col-4 col-lg-1 fig-container--wd-ht-lg-60">
					<figure class="lazy fig fig--bg-img-ct-cov-props ht-full-dac ht-full-lg-ac asp-ratio-rect-ac asp-ratio-rect-lg-dac" data-src="{{:thumb}}" title="{{:title}}">
						<img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="lazy hidden inset-img-hidden"/>
					</figure>
					</div>
					<div class="col-8 col-lg-10 pd-l-10 ft-pr bd-6 md-bd-6">
						<div>{{:title}}</div>
						{{if tag}}
						<div class="cl-lt cl-{{:~tagColor(tag)}}-dk tag">{{:tag}}</div>
						{{/if}}
					</div>
				</a>
			</li>
			{{/for}}
		</ul>
	</div>
	{{/if}}
</template>

<template type="text/x-jsrender" id="expandableCardWithRoundedLinksEntity">
	{{if (~lengthOfArr(~data.list.data))}}
	<div class="bg-body pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 mg-t-10 mg-t-md-10">
		<div class="ft-pr md-bd-4 bd-4 pd-t-20">{{:~data.title}}</div>
		<div class="pos-rel">
			{{if ~compareGT(~lengthOfArr(~data.list.data), 4)}}
			<input type="checkbox" id="expand-{{:~id}}" class="pos-abs bt-init lt-init rt-init hidden" />
			<label for="expand-{{:~id}}" class="pos-abs bt-init lt-init rt-init pd-t-20 pd-b-20 dp-fx fx-al-ct fx-js-ct trigger-expandable cs-ptr">
				<div class="bg-primary br-rd-pr-50 dp-fx fx-al-ct fx-basis-40px fx-js-ct pd-b-10 pd-l-10 pd-r-10 pd-t-10 wd-ht-px-40">
					<span class="nb-icon-angle-down fz-18 mg-t-6 cl-link"></span>
				</div>
			</label>
			{{/if}}
			<div class="mg-t-30 expandable">
			{{for ~data.list.data}}
			<a href="{{:url}}" class="dp-ib pd-t-14 pd-b-14 pd-l-20 pd-r-20 bg-primary br-rd-10 reg-2 md-reg-2 br-1p-pale-lavender mg-r-10 mg-t-10">{{:title}}</a>
			{{/for}}
			</div>
		</div>
	</div>
	<style type="text/css">
		#expand-{{:~id}}~.trigger-expandable~.expandable {
			height: 200px;
			overflow: hidden;
		}

		.trigger-expandable {
			background-image: linear-gradient(180deg, rgba(247, 247, 247, 0.00) 0%, #F7F7F7 100%);
		}

		#expand-{{:~id}}:checked~.trigger-expandable {
			display: none;
		}

		#expand-{{:~id}}:checked~.trigger-expandable~.expandable {
			height: auto;
			overflow: auto;
		}
	</style>
	{{/if}}
</template>

<template type="text/x-jsrender" id="embedBanner">
	<div class="embed-banner" id="{{:uuid}}">
		<div class="dp-fx fx-js-ct mg-t-20 ht-px-194 show-banner">
			<a class="dp-bl ht-full wd-full ovr-hidden mx-wd-px-320 br-rd-10" href="{{:url}}" target="_blank" aria-label="{{:title}}">
				<div class="pos-rel wd-full ht-full">
					<div class="wd-full ht-full bg-{{:type}}-banner pos-abs bg-banner-size">
						<div class="pos-abs inset-right-section ht-full tp-init rt-init">
							<div class="pos-rel wd-full ht-full">
								<div class="image-banner-right ht-full pos-abs wd-full"></div>
								<div class="pos-rel image-banner-inset-left wd-full ht-full bg-size-75"></div>
							</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<style type="text/css">
			.embed-banner#{{:uuid}} .bg-image-banner,.embed-banner#{{:uuid}} .bg-slant-banner{
				background-image:url({{:backgrounds.container}});
				background-color: {{:colors.container}};
				background-size: cover;
				background-repeat: no-repeat;
			}
			.embed-banner#{{:uuid}} .image-banner-right{
				background-color: {{:colors.inset}};
			}
			.embed-banner#{{:uuid}} .image-banner-inset-left{background:url({{:backgrounds.center}}) center center no-repeat; background-size:contain;}
			.embed-banner#{{:uuid}} .image-banner-inset-right{background-image: url({{:backgrounds.right}});}
			.ht-px-194{
				height:194px;
			}
			.mx-wd-px-320{
				max-width:320px;
			}
			.inset-right-section{width:75%;min-width:100px;max-width:300px;}
		</style>
	</div>
</template>

<template type="text/x-jsrender" id="eventCoverCard">
	{{for cover.list.data}}
	<div class="event-list-card pd-l-28 pd-r-20 pd-t-30 pd-l-md-48 pd-r-md-30 pd-t-md-40 pos-rel gd-tint-{{:~tagColor(tag)}}-lt-{{:~tagColor(tag)}}0-{{:~tagColor(tag)}}100 cover-card event-card">
		<div class="dp-fx fx-al-ct neg-mg-l-px-18 neg-mg-l-px-28-md">
			<span class="wd-ht-px-8 br-rd-pr-50 bg-{{:~tagColor(tag)}}-dk evt-connector z-index-1" id="{{:htmlId}}"></span>
			<span class="cl-lt cl-{{:~tagColor(tag)}}-dk ft-pr md-bd-6 ft-pr bd-6 mg-l-10 mg-l-md-20">{{:timestamp}}</span>
		</div>
		<span class="pos-abs br-l-2p-{{:~tagColor(tag)}} ht-full neg-mg-l-px-14 neg-mg-l-px-24-md neg-mg-t-px-10 vt-line "></span>
		<h1 class="ft-pr md-bd-1 ft-pr bd-1 mg-t-6">{{:title}}</h1>
		<div class="cl-lt cl-{{:~tagColor(tag)}}-dk ft-pr md-bd-6 ft-pr bd-6 mg-t-6">{{:tag}}</div>
		
		{{if (img != '')}}
		<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy dp-bl wd-full br-rd-10 mg-t-20"/>
		{{/if}}

		{{if (video.src != '')}}
		<div class="wd-full mg-t-20 pos-rel asp-ratio-rect-ac">
			<iframe src="{{:video.src}}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="pos-abs lt-init tp-init wd-full ht-full br-rd-10" title="YouTube Frame"></iframe>
		</div>
		{{/if}}
		
		<div class="dp-fx mg-t-20 neg-mg-lr-px-10-ac">
			{{for creators}}
			<div class="pd-l-10 pd-r-10">
				<a href="{{:url}}" class="dp-fx min-wd-px-50 pd-t-10 pd-b-10">
					<img data-src="{{:icon}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-36 br-rd-pr-50 {{if ~eql(icon, '')}}hidden{{/if}}" />
					<div class="{{if ~neql(icon, '')}}pd-l-10{{/if}}">
						<p class="ft-pr md-reg-3 ft-pr reg-3 cl-lt">{{:subtitle}}</p>
						<p class="ft-pr md-bd-6 ft-pr bd-6 dp-ib">{{:title}}</p>
					</div>
				</a>
				{{if (profiles)}}
				<div class="dp-fx mg-t-12 {{if ~neql(icon, '')}}mg-l-36{{/if}}">
				{{for profiles}}
				<a class="wd-ht-px-50 mg-l-12" target="_blank" href="{{:url}}" rel="nofollow noreferrer" aria-label="Link to {{:key}} profile">
					<span class="hidden">{{~capitalize(key)}}</span>
					<div class="nb-icon-{{:key}} fz-14"></div>
				</a>
				{{/for}}
				</div>
				{{/if}}
			</div>
			{{/for}}
		</div>
	</div>
	{{/for}}	
</template>

<template type="text/x-jsrender" id="v2eventCoverCard">
	<ul class="gd-cards {{:~tagColor(cover.list.data[0].tag)}}" data-cname="list-cv-cards">
	{{for cover.list.data}}
		<li class="e-cd cv tn-fw cover-card ft-pr">
			<div class="slug">
				<span class="dot bg-{{:~tagColor(tag)}}-dk evt-connector" id="{{:htmlId}}"></span>
				<span class="cl-lt cl-{{:~tagColor(tag)}}-dk tt">{{:timestamp}}</span>
			</div>
			<span class="br-l-2p-{{:~tagColor(tag)}} vt-line v-ln"></span>
			<h1 class="md-bd-1 bd-1 mg-t-6">{{:title}}</h1>

			<div class="fx-row fx-al-ct mg-t-10">
				<p class="md-reg-3 reg-3 cl-lt">{{:creators[0].subtitle}}</p>
				<a class="mg-l-4 tx-decoration-none md-bd-6 bd-6" href="{{:creators[0].url}}">{{:creators[0].title}}</a>
				<div class="bg-lt bg-{{:~tagColor(tag)}}-dk mg-l-4 mg-r-4 br-rd-pr-50 wd-ht-px-3 fx-basis-3px"></div>
				<h2 class="cl-lt cl-{{:~tagColor(tag)}}-dk md-bd-6 bd-6">
					{{if ~_and(tagUrl, ~neql(tagUrl, ''))}}
					<a href="{{:tagUrl}}" class="tx-decoration-none">{{:tag}}</a>
					{{else}}
					<span>{{:tag}}</span>
					{{/if}}
				</h2>
			</div>
			
				{{if ~_and(slides, ~lengthOfArr(slides))}}
				<section class="slide-show-parent visibility-hidden mg-t-20">
					<section class="slide-show default {{:htmlId}}" id="slideshow-{{:~randomId(6)}}">
						{{for slides}}
						<div class="min-wd-px-100 mg-r-20">
							<img alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full br-rd-10" src="{{:ximg}}">
						</div>
						{{/for}}
					</section>
					<div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
				</section>
				{{else ~_and(~neql(ximg, ''), ~eql(video.src, ''))}}
				<div class="pd-b-20 mg-t-20">
					<div class="pos-rel asp-ratio r-16-9">
						<img data-src="{{:ximg}}" alt="{{:title}}" title="{{:title}}" class="br-rd-10 dp-bl wd-full lazy"/>
						{{if (imagecredit)}}
						{{if ~_and(~neql(imagecredit, null), imagecredit.title)}}
							<a class="pos-abs tx-decoration-none image-credits" href="{{:imagecredit.url}}" target="_blank" rel="nofollow noopener">
								<div class="dp-fx fx-al-ct">
									<div class="cl-primary reg-3 mg-r-4">{{:imagecredit.title}}</div>
									<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 -2 17.52 16" height="10"><defs></defs><circle class="svg" cx="8.76" cy="8.26" r="2.87"/><path class="svg" d="M15.78,2.52H13.5L13,1a1.48,1.48,0,0,0-1.41-1H6.39A1.48,1.48,0,0,0,5,1L4.5,2.52H2.22a2,2,0,0,0-2,2V12a2,2,0,0,0,2,2H15.78a2,2,0,0,0,2-2V4.49A2,2,0,0,0,15.78,2.52ZM9,12.32a4.06,4.06,0,1,1,4.06-4.06A4.06,4.06,0,0,1,9,12.32Z" transform="translate(-0.24)"/></svg>
								</div>
							</a>
						{{/if}}
						{{/if}}
					</div>
				</div>
			{{/if}}
			{{if (video.src != '')}}
			<div class="v-obj asp-ratio-rect-ac pd-b-20">
				{{if (video.id)}}
				<div class="pos-abs tp-init lt-init rt-init wd-full ht-full cs-ptr play-video embed-in-place" data-video="{{:video.src}}">
					<div class="pos-rel">
						<img alt="{{:title}}" title="{{:title}}" class="wd-full dp-bl br-rd-10" src="{{:video.img}}">
						<div class="pos-abs tp-init lt-init rt-init bt-init solid-tint-black-black40 br-rd-10"></div>
						<div class="cl-primary nb-icon-video-play-button tf-x-y-middle lt-half tp-half pos-abs fz-50"></div>
					</div>
				</div>
				{{else}}
				<iframe src="{{:video.src}}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen title="YouTube Frame"></iframe>
				{{/if}}
			</div>
			{{/if}}
		</li>
	{{/for}}
	</ul>
</template>

<template type="text/x-jsrender" id="breadcrumbs">
	{{if ~lengthOfArr(breadcrumbs.list.data)}}
	<div class="cl-suva-grey ft-ter md-ter-reg-2 ter-reg-2 wht-sp-nowrap pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac ovr-hidden tx-ovr-ellipsis mg-t-20">
		{{for breadcrumbs.list.data}}
		{{if ~_and(~compareGT(#getIndex(), 0), ~compareLT(#getIndex(), ~lengthOfArr(~root.breadcrumbs.list.data)))}}
		<span class="cl-ink-dk">/</span>
		{{/if}}
		{{if ~_and(url, ~neql(url, ''), ~neql(#getIndex(), (~lengthOfArr(~root.breadcrumbs.list.data)-1)))}}
		<a href="{{:url}}" class="cl-ink-dk">{{:title}}</a>
		{{else}}
		<span>{{:title}}</span>
		{{/if}}
		{{/for}}
	</div>
	{{/if}}
</template>

<template type="text/x-jsrender" id="v2eventCoverCardWithCreators">
	<ul class="gd-cards {{:~tagColor(cover.list.data[0].tag)}}" data-cname="list-cv-cards">
	{{for cover.list.data}}
		<li class="e-cd cv tn-fw cover-card ft-pr pd-t-30">
			<div class="slug">
				<span class="dot bg-{{:~tagColor(tag)}}-dk evt-connector" id="{{:htmlId}}"></span>
				<span class="cl-lt cl-{{:~tagColor(tag)}}-dk tt">
					{{if ~_and(tagUrl, ~neql(tagUrl, ''))}}
					<a href="{{:tagUrl}}" class="tx-decoration-none">{{:tag}}</a>
					{{else}}
					<span>{{:tag}}</span>
					{{/if}}
				</span>
			</div>
			<span class="br-l-2p-{{:~tagColor(tag)}} vt-line v-ln"></span>
			<h1 class="md-bd-1 bd-1 mg-t-6">{{:title}}</h1>
			<div class="mg-t-10">
				<div class="dp-fx fx-al-ct">
					{{for creators}}
						{{if ~compareGT(#getIndex(), 0)}}
						<a href="{{:url}}" class="dp-fx fx-al-st tx-decoration-none mg-l-30">
						{{else}}
						<a href="{{:url}}" class="dp-fx fx-al-st tx-decoration-none">
						{{/if}}
							{{if ~eql(~lengthOfArr(~root.cover.list.data[0].creators), 1)}}
							<img src="{{:icon}}" alt="{{:title}}" title="{{:title}}" class="br-rd-pr-50 wd-ht-px-36 mg-r-10" />
							{{/if}}
							<div>
								<div class="cl-lt reg-3 md-reg-3">{{:subtitle}}</div>
								<div class="bd-6 md-bd-6">{{:title}}</div>
							</div>
						</a>
						{{if ~_and(~eql(~lengthOfArr(~root.cover.list.data[0].creators), 1), ~compareGT(~lengthOfArr(profiles), 0))}}
						<div class="dp-fx mg-l-10">
						{{for profiles}}
							<a target="_blank" class="dp-fx wd-ht-px-50 fx-al-ct mg-l-10 tx-decoration-none" href="{{:url}}" rel="nofollow noreferrer" aria-label="Link to {{:key}} profile">
								<span class="hidden">{{:~capitalize(key)}}</span>
								<div class="nb-icon-{{:key}} fz-14" ></div>
							</a>
						{{/for}}
						</div>
						{{/if}}
					{{/for}}
				</div>
			</div>
			{{if creatorSnapshot}}
			<div class="fx-row mg-t-10">
				<span class="cl-lt reg-3 md-reg-3">{{:creatorSnapshot.subtitle}}</span>
				<span class="mg-l-4 bd-6 md-bd-6">{{:creatorSnapshot.timestamp}}</span>
			</div>
			{{/if}}
			
			{{if ~_and(slides, ~lengthOfArr(slides))}}
			<section class="slide-show-parent visibility-hidden mg-t-10">
				<section class="slide-show default {{:htmlId}}" id="slideshow-{{:~randomId(6)}}">
					{{for slides}}
					<div class="min-wd-px-100 mg-r-20">
						<img alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full br-rd-10" src="{{:ximg}}">
					</div>
					{{/for}}
				</section>
				<div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
			</section>
			{{else ~_and(~neql(ximg, ''), ~eql(video.src, ''))}}
			<div class="pos-rel asp-ratio r-16-9 mg-t-10">
				<img data-src="{{:ximg}}" alt="{{:title}}" title="{{:title}}" class="br-rd-10 dp-bl wd-full lazy"/>
				{{if (imagecredit)}}
				{{if ~_and(~neql(imagecredit, null), imagecredit.title)}}
					<a class="pos-abs tx-decoration-none image-credits" href="{{:imagecredit.url}}" target="_blank" rel="nofollow noopener">
						<div class="dp-fx fx-al-ct">
							<div class="cl-primary reg-3 mg-r-4">{{:imagecredit.title}}</div>
							<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 -2 17.52 16" height="10"><defs></defs><circle class="svg" cx="8.76" cy="8.26" r="2.87"/><path class="svg" d="M15.78,2.52H13.5L13,1a1.48,1.48,0,0,0-1.41-1H6.39A1.48,1.48,0,0,0,5,1L4.5,2.52H2.22a2,2,0,0,0-2,2V12a2,2,0,0,0,2,2H15.78a2,2,0,0,0,2-2V4.49A2,2,0,0,0,15.78,2.52ZM9,12.32a4.06,4.06,0,1,1,4.06-4.06A4.06,4.06,0,0,1,9,12.32Z" transform="translate(-0.24)"/></svg>
						</div>
					</a>
				{{/if}}
				{{/if}}
			</div>
			{{/if}}
			{{if (video.src != '')}}
			<div class="v-obj asp-ratio-rect-ac mg-t-10">
				{{if (video.id)}}
				<div class="pos-abs tp-init lt-init rt-init wd-full ht-full cs-ptr play-video embed-in-place" data-video="{{:video.src}}">
					<div class="pos-rel">
						<img alt="{{:title}}" title="{{:title}}" class="wd-full dp-bl br-rd-10" src="{{:video.img}}">
						<div class="pos-abs tp-init lt-init rt-init bt-init solid-tint-black-black40 br-rd-10"></div>
						<div class="cl-primary nb-icon-video-play-button tf-x-y-middle lt-half tp-half pos-abs fz-50"></div>
					</div>
				</div>
				{{else}}
				<iframe src="{{:video.src}}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen title="YouTube Frame"></iframe>
				{{/if}}
			</div>
			{{/if}}
		</li>
	{{/for}}
	</ul>
</template>

<template type="text/x-jsrender" id="eventCard">
	{{for events.list.data}}
	{{if (advt)}}
	<div class="hidden-lg dp-fx fx-js-ct">
		<div id="{{:advt.id}}"></div>
	</div>
	{{/if}}
	{{if (conf.cardtype == 'fact')}}
		<div class="event-list-card pd-l-28 pd-r-20 pd-t-30 pd-l-md-48 pd-r-md-30 pd-t-md-30 br-rd-tl-tr-10px embedded-links event-card solid-tint-{{:~tagColor(tag + 'tint')}} mg-t-20">
			{{if (subtitle != '')}}
			<div class="dp-fx fx-al-ct neg-mg-l-px-18 neg-mg-l-px-28-md">
				<span class="wd-ht-px-8 br-rd-pr-50 bg-{{:~tagColor(tag)}} evt-connector z-index-1" data-hooked="{{:htmlId}}"></span>
				<span class="cl-lt cl-{{:~tagColor(tag)}} ft-pr md-bd-6 ft-pr bd-6 mg-l-10 mg-l-md-20">{{:subtitle}}</span>
			</div>
			{{/if}}
			{{if (title != '')}}
				<div class="ft-pr md-bd-4 ft-pr bd-4 mg-t-10">{{:title}}</div>
			{{/if}}
		</div>
		<div class="event-list-card pd-l-28 pd-r-20 pd-b-30 pd-l-md-48 pd-r-md-30 pd-b-md-30 br-rd-bl-br-10px embedded-links event-card solid-tint-{{:~tagColor(tag + 'tint')}}">
			{{props html}}
				{{if ~compareLT(#getIndex(), 1)}}
				<p class="pd-t-20 ft-sec sec-md-reg-1 ft-sec sec-reg-1">{{:prop}}</p>
				{{else}}
				<p class="pd-t-10 ft-sec sec-md-reg-1 ft-sec sec-reg-1">{{:prop}}</p>
				{{/if}}
			{{/props}}
		</div>
	{{else}}
		{{if ((subtitle != '') || (title != '') || (img != '') || (video.src != ''))}}      
			<div class="event-list-card pd-l-28 pd-r-20 pd-t-30 pd-b-30 pd-l-md-48 pd-r-md-30 pd-t-md-30 pd-b-md-20 br-rd-tl-tr-md-10px embedded-links event-card gd-tint-{{:~tagColor(tag)}}-lt-{{:~tagColor(tag)}}0-{{:~tagColor(tag)}}100 solid-tint-{{:~tagColor(tag)}} mg-t-20">

				{{if (subtitle != '')}}
				<div class="dp-fx fx-al-ct neg-mg-l-px-18 neg-mg-l-px-28-md">
					<span class="wd-ht-px-8 br-rd-pr-50 bg-{{:~tagColor(tag)}}-dk evt-connector z-index-1" data-hooked="{{:htmlId}}"></span>
					<span class="cl-lt cl-{{:~tagColor(tag)}}-dk ft-pr md-bd-6 ft-pr bd-6 mg-l-10 mg-l-md-20">{{:subtitle}}</span>
				</div>
				{{/if}}

				{{if (title != '')}}
				<div class="ft-pr md-bd-4 ft-pr bd-4 mg-t-10">{{:title}}</div>
				{{/if}}

				{{if (img != '')}}
				<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy dp-bl wd-full br-rd-10 mg-t-20"/>
				{{/if}}

				{{if (video.src != '')}}
				<div class="wd-full mg-t-20 pos-rel asp-ratio-rect-ac">
					<iframe src="{{:video.src}}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="pos-abs lt-init tp-init wd-full ht-full br-rd-10" title="YouTube Frame"></iframe>
				</div>
				{{/if}}      
			</div>
		{{/if}}
		
		{{if ~lengthOfArr(html)}}
		{{if ~compareLT(#getIndex(), 1)}}
		<div class="event-list-card pd-l-28 pd-r-20 pd-t-30 pd-b-30 pd-l-md-48 pd-r-md-30 pd-t-md-20 pd-b-md-30 embedded-links event-card gd-tint-rev-{{:~tagColor(tag)}}-lt-{{:~tagColor(tag)}}0-{{:~tagColor(tag)}}100 solid-tint-{{:~tagColor(tag)}}">
		{{else}}
		<div class="event-list-card pd-l-28 pd-r-20 pd-t-30 pd-b-30 pd-l-md-48 pd-r-md-30 pd-t-md-20 pd-b-md-30 embedded-links event-card br-rd-bl-br-md-10px gd-tint-rev-{{:~tagColor(tag)}}-lt-{{:~tagColor(tag)}}0-{{:~tagColor(tag)}}100 solid-tint-{{:~tagColor(tag)}}">
		{{/if}}
			{{props html}}
				{{if ~compareLT(#getIndex(), 1)}}
				<p class="ft-sec sec-md-reg-1 ft-sec sec-reg-1">{{:prop}}</p>
				{{else}}
				<p class="mg-t-10 ft-sec sec-md-reg-1 ft-sec sec-reg-1">{{:prop}}</p>
				{{/if}}
			{{/props}}
		</div>
		{{/if}}

		{{if (conf.cardtype == 'embed')}}
		{{if (conf.type)}}
		<div class="event-list-card pd-l-28 pd-r-20 pd-t-30 pd-b-30 pd-l-md-48 pd-r-md-30 pd-t-md-20 pd-b-md-30 br-rd-bl-br-md-10px embedded-links event-card gd-tint-rev-{{:~tagColor(tag)}}-lt-{{:~tagColor(tag)}}0-{{:~tagColor(tag)}}100 solid-tint-{{:~tagColor(tag)}}">
			{{if (conf.type == 'apester')}}
			{{include tmpl='embedApester'/}}
			{{else (conf.type == 'facebook')}}
			{{include tmpl='embedFacebook'/}}
			{{else (conf.type == 'infogram')}}
			{{include tmpl='embedInfogram'/}}
			{{else (conf.type == 'instagram')}}
			{{include tmpl='embedInstagram'/}}
			{{else (conf.type == 'twitter')}}
			{{include tmpl='embedTwitter'/}}
			{{/if}}
		</div>
		{{/if}}
		{{/if}}
	{{/if}}
	{{/for}}
</template>

<template type="text/x-jsrender" id="v2eventCard">
	<ul class="gd-cards {{:~tagColor(events.list.data[0].tag)}}" data-cname="list-ev-cards">
	{{for events.list.data}}
		{{if ~eql(#getIndex(), 1)}}
		{{if (~root.jumpLinks)}}
		{{if ~lengthOfArr(~root.jumpLinks.list.data)}}
		<li class="mg-t-10">
			<div class="pd-l-28 pd-r-20 pd-l-md-48 pd-r-md-30">
				<div class="pd-t-20 pd-b-20 pd-l-20 pd-r-20 pd-t-md-30 pd-b-md-30 pd-l-md-30 pd-r-md-30 ft-sec br-rd-10 bg-primary bg-{{:~tagColor(~root.jumpLinks.list.data[0].tag)}}-lt jump-links-parent">
					<div class="bd-4 md-bd-4 cl-ink-dk ft-pr">{{:~root.jumpLinks.title}}</div>
					<ul>
						{{for ~root.jumpLinks.list.data}}
						<li class="mg-t-20">
							<a class="dp-fx jump-link" href="{{:url}}" data-id="{{:~root.events.list.data[0].htmlId}}">
								<span class="dp-ib wd-ht-px-5 br-rd-pr-50 bg-{{:~tagColor(tag)}}-dk fx-basis-5px mg-t-12"></span>
								<span class="mg-l-10 sec-reg-1 sec-md-reg-1 cl-{{:~tagColor(tag)}}-dk">{{:title}}</span>
							</a>
						</li>
						{{/for}}
					</ul>
				</div>
			</div>
		</li>
		{{/if}}
		{{/if}}
		{{/if}}
		{{if (conf.cardtype == 'fact')}}
			{{if ~_and(traits, ~neql(traits.type, ''))}}
			{{if ~eql(traits.type, 'scorecard')}}
			<div class="e-cd ft-pr pd-t-50 pd-b-40 mg-t-10 br-rd-20 cl-primary solid-tint-{{:~tagColor(tag + 'tint')}}">
				{{if (jumpLinkId)}}
				{{if (jumpLinkId != '')}}
				<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
				{{/if}}
				{{/if}}
				{{if (subtitle != '')}}
				<div class="slug">
					<span class="dot bg-primary evt-connector" data-hooked="{{:htmlId}}"></span>
					<h3 class="cl-primary tt">{{:subtitle}}</h3>
				</div>
				{{/if}}
				{{if (jumpLinkId)}}
				{{if (jumpLinkId != '')}}
				</div>
				{{/if}}
				{{/if}}
				{{if (~neql(traits.title, ''))}}
				<div class="tx-ct mg-t-10 reg-3 md-reg-3">{{:traits.title}}</div>
				{{/if}}
				{{if (~neql(traits.subtitle, ''))}}
				<div class="tx-ct reg-3 md-reg-3">{{:traits.subtitle}}</div>
				{{/if}}
				{{if ~lengthOfArr(traits.comparison.items)}}
				<div class="fx-row mg-t-10 fx-js-bw">
					{{for traits.comparison.items ~connector=traits.comparison.connector}}
					<div class="dp-fx">
						{{if ~_and(~neql(img, ''), ~eql((#getIndex() % 2), 0))}}
						<div class="dp-fx fx-dr-col fx-al-ct mg-r-10 mg-r-md-20">
							<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-50 br-rd-pr-50"/>
							<div class="bd-2 md-bd-2 mg-t-6">{{:title}}</div>
						</div>
						{{/if}}
						<div>
							{{if ~eql((#getIndex() % 2), 0)}}
							<div class="bd-5 md-bd-5 tx-rt">{{:params.title}}</div>
							<div class="reg-3 md-reg-3 tx-rt">{{:params.subtitle}}</div>
							{{else}}
							<div class="bd-5 md-bd-5 tx-lt">{{:params.title}}</div>
							<div class="reg-3 md-reg-3 tx-lt">{{:params.subtitle}}</div>
							{{/if}}
						</div>
						{{if ~_and(~neql(img, ''), ~neql((#getIndex() % 2), 0))}}
						<div class="dp-fx fx-dr-col fx-al-ct mg-l-10 mg-l-md-20">
							<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-50 br-rd-pr-50"/>
							<div class="bd-2 md-bd-2 mg-t-6">{{:title}}</div>
						</div>
						{{/if}}
					</div>
					{{if ~eql((#getIndex() % 2), 0)}}
					<div class="bd-6 md-bd-6 mg-t-16">{{:~connector}}</div>
					{{/if}}
					{{/for}}
				</div>
				{{/if}}
				{{if (~neql(traits.conclusion, ''))}}
				<div class="reg-2 mg-t-6 tx-ct">{{:traits.conclusion}}</div>
				{{/if}}
				{{if ~lengthOfArr(traits.list)}}
					{{for traits.list}}
					<div class="bd-4 md-bd-4 mg-t-6 tx-ct">{{:title}}</div>
					<div class="fx-row neg-mg-lr-px-10-ac">
						{{for data}}
						{{if ~eql((#getIndex() % 2), 0)}}
						<div class="mg-t-6 col-6 dp-fx fx-al-ct pd-l-10 pd-r-10">
						{{else}}
						<div class="mg-t-6 col-6 dp-fx fx-al-ct pd-l-10 pd-r-10 fx-js-end">
						{{/if}}
							{{if ~_and(~neql(img, ''), ~eql((#getIndex() % 2), 0))}}
							<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-20 br-rd-pr-50 mg-r-10"/>
							{{/if}}
							{{if ~eql((#getIndex() % 2), 0)}}
							<div class="reg-2 dp-fx fx-dr-col">
							{{else}}
							<div class="reg-2 dp-fx fx-dr-col fx-al-end">
							{{/if}}
								<div>{{:title}}</div>
								<div>{{:subtitle}}</div>
							</div>
							{{if ~_and(~neql(img, ''), ~neql((#getIndex() % 2), 0))}}
							<img data-src="{{:img}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-20 br-rd-pr-50 mg-l-10"/>
							{{/if}}
						</div>
						{{/for}}
					</div>
					{{/for}}
				{{/if}}
				{{if ~lengthOfArr(traits.card)}}
					<div class="bd-4 md-bd-4 mg-t-20">{{:traits.card.title}}</div>
					{{props traits.card.text}}
					<div class="mg-t-10 reg-1">{{:prop}}</div>
					{{/props}}
				{{/if}}
				{{if ~lengthOfArr(traits.links)}}
					<div class="fx-row neg-mg-lr-px-10-ac">
						{{for traits.links ~len=traits.links.length}}
						{{if ~compareGT(~len, 1)}}
						<div class="pd-l-10 pd-r-10 col-12 mg-t-30 col-md-6">
						{{else}}
						<div class="pd-l-10 pd-r-10 col-12 mg-t-30">
						{{/if}}
							<a class="dp-bl wd-full ft-pr bd-6 md-bd-6 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{{:url}}" target="_blank" aria-label="Button Link">
								<div>{{:title}}</div>
								<span class="nb-icon-external-link fz-20 mg-l-6"></span>
							</a>
						</div>
						{{/for}}
					</div>
				{{/if}}
			</div>
			{{else ~eql(traits.type, 'rating')}}
				<div class="e-cd ft-pr pd-t-50 pd-b-40 mg-t-10 br-rd-20 solid-tint-{{:~tagColor(tag + 'tint')}}">
					{{if (jumpLinkId)}}
					{{if (jumpLinkId != '')}}
					<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
					{{/if}}
					{{/if}}
					{{if (subtitle != '')}}
					<div class="slug">
						<span class="dot bg-primary evt-connector" data-hooked="{{:htmlId}}"></span>
						<h3 class="cl-primary tt">{{:subtitle}}</h3>
					</div>
					{{/if}}
					{{if (jumpLinkId)}}
					{{if (jumpLinkId != '')}}
					</div>
					{{/if}}
					{{/if}}
					{{if (~neql(traits.title, ''))}}
					<div class="bd-1 md-bd-2 mg-t-6 cl-primary">{{:traits.title}}</div>
					{{/if}}
					{{if ~lengthOfArr(traits.ratings.keys)}}
					<div class="pd-t-10" title="{{:traits.ratings.value}}">
						<div class="cl-primary reg-3">{{:traits.title}}</div>
						<div class="fx-row">
							{{props traits.ratings.keys}}
							<div class="svg-inclusion mg-r-4">
								{{if ~eql(prop, 'star')}}
								<svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg star">
									<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<g id="Group-3" fill="#BBC2BD">
											<path d="M9.8316,0.5748 L11.7896,4.7468 C11.9316,5.0488 12.2146,5.2608 12.5436,5.3108 L17.0026,5.9918 C17.8076,6.1148 18.1356,7.0958 17.5666,7.6788 L14.2786,11.0488 C14.0576,11.2768 13.9566,11.5958 14.0076,11.9078 L14.7746,16.6138 C14.9096,17.4378 14.0346,18.0538 13.3046,17.6498 L9.4106,15.4978 C9.1086,15.3308 8.7436,15.3308 8.4426,15.4978 L4.5486,17.6498 C3.8176,18.0538 2.9426,17.4378 3.0776,16.6138 L3.8446,11.9078 C3.8956,11.5958 3.7956,11.2768 3.5736,11.0488 L0.2856,7.6788 C-0.2834,7.0958 0.0456,6.1148 0.8506,5.9918 L5.3086,5.3108 C5.6386,5.2608 5.9206,5.0488 6.0626,4.7468 L8.0206,0.5748 C8.3806,-0.1922 9.4716,-0.1922 9.8316,0.5748" id="Fill-1"></path>
										</g>
									</g>
								</svg>
								{{else ~eql(prop, 'filled_star')}}
								<svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg filled_star">
									<defs>
										<linearGradient x1="50%" y1="-1.12155278%" x2="50%" y2="98.8784472%" id="linearGradient-1">
											<stop stop-color="#FDD800" offset="0%"></stop>
											<stop stop-color="#EBC10E" offset="100%"></stop>
										</linearGradient>
									</defs>
									<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<path d="M9,0 C8.588,0 8.176,0.219 7.972,0.656 L7.972,0.656 L6.099,4.665 C5.939,5.009 5.618,5.251 5.243,5.307 L5.243,5.307 L0.965,5.964 C0.051,6.105 -0.321,7.224 0.324,7.889 L0.324,7.889 L3.49,11.149 C3.742,11.408 3.857,11.773 3.798,12.13 L3.798,12.13 L3.06,16.673 C2.942,17.403 3.514,17.99 4.164,18 L4.164,18 L4.2,18 C4.376,17.997 4.557,17.952 4.73,17.855 L4.73,17.855 L8.45,15.789 C8.792,15.599 9.207,15.599 9.549,15.789 L9.549,15.789 L13.27,17.855 C13.444,17.952 13.625,17.997 13.801,18 L13.801,18 L13.836,18 C14.486,17.99 15.058,17.403 14.939,16.673 L14.939,16.673 L14.202,12.13 C14.143,11.773 14.258,11.408 14.51,11.149 L14.51,11.149 L17.676,7.889 C18.321,7.224 17.949,6.105 17.035,5.964 L17.035,5.964 L12.757,5.307 C12.382,5.251 12.062,5.009 11.901,4.665 L11.901,4.665 L10.028,0.656 C9.824,0.219 9.412,0 9,0 Z" id="Fill-1" fill="url(#linearGradient-1)"></path>
									</g>
								</svg>
								{{else ~eql(prop, 'half_filled_star')}}
								<svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="svg half_filled_star">
									<defs>
										<linearGradient x1="49.9996654%" y1="-1.12155278%" x2="49.9996654%" y2="98.8784472%" id="linearGradient-1">
											<stop stop-color="#FDD800" offset="0%"></stop>
											<stop stop-color="#EBC10E" offset="100%"></stop>
										</linearGradient>
									</defs>
									<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<path d="M9,0 C8.589,0 8.176,0.219 7.972,0.656 L7.972,0.656 L6.099,4.665 C5.939,5.009 5.618,5.251 5.243,5.307 L5.243,5.307 L0.965,5.964 C0.051,6.105 -0.321,7.224 0.324,7.889 L0.324,7.889 L3.49,11.149 C3.742,11.408 3.857,11.773 3.798,12.13 L3.798,12.13 L3.06,16.673 C2.942,17.403 3.514,17.99 4.164,18 L4.164,18 L4.2,18 C4.376,17.997 4.557,17.952 4.73,17.855 L4.73,17.855 L8.451,15.789 C8.621,15.694 8.811,15.646 9,15.646 L9,15.646 L9,0 Z" id="Fill-1" fill="url(#linearGradient-1)"></path>
										<path d="M8.9999,15.6465 C9.1889,15.6465 9.3779,15.6935 9.5489,15.7885 L13.2699,17.8545 C14.0989,18.3155 15.0919,17.6125 14.9389,16.6725 L14.2019,12.1295 C14.1439,11.7725 14.2579,11.4085 14.5099,11.1495 L17.6759,7.8895 C18.3219,7.2245 17.9489,6.1045 17.0349,5.9645 L12.7569,5.3075 C12.3829,5.2505 12.0619,5.0095 11.8999,4.6645 L10.0279,0.6565 C9.8239,0.2185 9.4119,0.0005 8.9999,0.0005 L8.9999,15.6465 Z" id="Fill-4" fill="#BBC2BD"></path>
									</g>
								</svg>
								{{/if}}
							</div>
							{{/props}}
						</div>
					</div>
					{{/if}}
					{{if ~neql(traits.img, '')}}
					<div class="dp-fx fx-js-ct mg-t-20">
						<img data-src="{{:traits.img}}" alt="{{:traits.title}}" title="{{:traits.title}}" class="lazy wd-ht-px-200"/>
					</div>
					{{/if}}
					{{if ~lengthOfArr(traits.card.text)}}
					{{props traits.card.text}}
					<div class="reg-1 cl-primary mg-t-10">{{:prop}}</div>
					{{/props}}
					{{/if}}
					{{if ~lengthOfArr(traits.list)}}
					<div class="fx-row neg-mg-lr-px-10-ac cl-primary">
						{{for traits.list}}
						<div class="col-12 col-md-6 pd-l-10 pd-r-10 mg-t-20">
							<div class="bd-4 md-bd-4">{{:title}}</div>
							{{props data}}
							<div class="pos-rel mg-t-10">
								<div class="reg-1 pd-t-6 pd-b-6 pd-l-20">{{:prop}}</div>
								<div class="gd-tint-white-white0-white100-half-1 pos-abs lt-init bt-init tp-init wd-px-90"></div>
							</div>
							{{/props}}
						</div>
						{{/for}}
					</div>
					{{/if}}
					{{if ~lengthOfArr(traits.links)}}
						<div class="fx-row neg-mg-lr-px-10-ac">
							{{for traits.links ~len=traits.links.length}}
							{{if ~compareGT(~len, 1)}}
							<div class="pd-l-10 pd-r-10 col-12 mg-t-30 col-md-6">
							{{else}}
							<div class="pd-l-10 pd-r-10 col-12 mg-t-30">
							{{/if}}
								<a class="dp-bl wd-full ft-pr bd-6 md-bd-6 cl-primary dp-fx fx-js-ct tx-decoration-none" href="{{:url}}" target="_blank" aria-label="Button Link">
									<div>{{:title}}</div>
									<span class="nb-icon-external-link fz-20 mg-l-6"></span>
								</a>
							</div>
							{{/for}}
						</div>
					{{/if}}
				</div>
			{{/if}}
			{{else}}
			<div class="e-cd ft-b ft-pr mg-t-10 pd-t-20">
				{{if (jumpLinkId)}}
				{{if (jumpLinkId != '')}}
				<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
				{{/if}}
				{{/if}}
				{{if (subtitle != '')}}
				<div class="slug">
					<span class="dot bg-{{:~tagColor(tag)}} evt-connector" data-hooked="{{:htmlId}}"></span>
					<h3 class="cl-lt cl-{{:~tagColor(tag)}} tt">{{:subtitle}}</h3>
				</div>
				{{/if}}
				{{if (jumpLinkId)}}
				{{if (jumpLinkId != '')}}
				</div>
				{{/if}}
				{{/if}}
				{{if ~neql(title, '')}}
					<h2 class="c-tl">{{:title}}</h2>
				{{/if}}
			</div>
			<div class="e-cd ft-e ft-pr pd-b-20">
				{{props html}}
				<p class="ft-sec pt">{{:prop}}</p>
				{{/props}}
			</div>
			{{/if}}
		{{else}}
			{{if ((subtitle != '') || (title != '') || (img != '') || (video.src != ''))}}
			<li class="e-cd ev md-asset tn-fw ft-pr mg-t-10 pd-t-20">
				{{if (jumpLinkId)}}
				{{if (jumpLinkId != '')}}
				<div id="{{:jumpLinkId}}" class="tx-decoration-none jump-at" data-id="{{:htmlId}}">
				{{/if}}
				{{/if}}
				{{if (subtitle != '')}}
				<div class="slug">
					<span class="dot bg-{{:~tagColor(tag)}}-dk evt-connector" data-hooked="{{:htmlId}}"></span>
					<h3 class="cl-lt cl-{{:~tagColor(tag)}}-dk tt">{{:subtitle}}</h3>
				</div>
				{{/if}}
				{{if (jumpLinkId)}}
				{{if (jumpLinkId != '')}}
				</div>
				{{/if}}
				{{/if}}

				{{if (title != '')}}
				<h2 class="c-tl">{{:title}}</h2>
				{{/if}}

				{{if ~_and(slides, ~lengthOfArr(slides))}}
				<section class="slide-show-parent visibility-hidden mg-t-10">
					<section class="slide-show default {{:htmlId}}" id="slideshow-{{:~randomId(6)}}">
						{{for slides}}
						<div class="min-wd-px-100 mg-r-20">
							<img alt="{{:title}}" title="{{:title}}" class="dp-bl wd-full br-rd-10" src="{{:ximg}}">
						</div>
						{{/for}}
					</section>
					<div class="append-slide-show-dots slide-show-dots light-theme dp-fx fx-js-ct"></div>
				</section>
				{{else ~_and(~neql(ximg, ''), ~eql(video.src, ''))}}
				<div class="asp-ratio r-16-9 mg-t-10">
					<img data-src="{{:ximg}}" alt="{{:title}}" title="{{:title}}" class="i-obj lazy"/>
					{{if (imagecredit)}}
					{{if ~_and(~neql(imagecredit, null), imagecredit.title)}}
						<a class="pos-abs tx-decoration-none image-credits" href="{{:imagecredit.url}}" target="_blank" rel="nofollow noopener">
							<div class="dp-fx fx-al-ct">
								<div class="cl-primary reg-3 mg-r-4">{{:imagecredit.title}}</div>
								<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 -2 17.52 16" height="10"><defs></defs><circle class="svg" cx="8.76" cy="8.26" r="2.87"/><path class="svg" d="M15.78,2.52H13.5L13,1a1.48,1.48,0,0,0-1.41-1H6.39A1.48,1.48,0,0,0,5,1L4.5,2.52H2.22a2,2,0,0,0-2,2V12a2,2,0,0,0,2,2H15.78a2,2,0,0,0,2-2V4.49A2,2,0,0,0,15.78,2.52ZM9,12.32a4.06,4.06,0,1,1,4.06-4.06A4.06,4.06,0,0,1,9,12.32Z" transform="translate(-0.24)"/></svg>
							</div>
						</a>
						{{/if}}
					{{/if}}
				</div>
				{{/if}}

				{{if (video.src != '')}}
				<div class="v-obj asp-ratio-rect-ac mg-t-10">
					{{if (video.id)}}
					<div class="pos-abs tp-init lt-init rt-init wd-full ht-full cs-ptr play-video embed-in-place" data-video="{{:video.src}}">
						<div class="pos-rel">
							<img alt="{{:title}}" title="{{:title}}" class="lazy wd-full dp-bl br-rd-10" data-src="{{:video.img}}">
							<div class="pos-abs tp-init lt-init rt-init bt-init solid-tint-black-black40 br-rd-10"></div>
							<div class="cl-primary nb-icon-video-play-button tf-x-y-middle lt-half tp-half pos-abs fz-50"></div>
						</div>
					</div>
					{{else}}
					<iframe src="{{:video.src}}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen title="YouTube Frame"></iframe>
					{{/if}}
				</div>
				{{/if}}        
			</li>
			{{/if}}

			{{if ~lengthOfArr(html)}}
				{{if ~compareLT(#getIndex(), 1)}}
				<li class="e-cd ev ft-sec pd-b-20">
					{{props html}}
						<p class="pt">{{:prop}}</p>
					{{/props}}
				</li>
				{{else}}
				<li class="e-cd ev r-bt ft-sec pd-b-20">
					{{props html}}
					<p class="pt">{{:prop}}</p>
					{{/props}}
				</li>
				{{/if}}
			{{/if}}

			{{if (conf.cardtype == 'embed')}}
			{{if (conf.type)}}
			<li class="e-cd ev r-bt pd-t-10 pd-b-20">
				{{if (conf.type == 'apester')}}
				{{include tmpl='embedApester'/}}
				{{else (conf.type == 'facebook')}}
				{{include tmpl='embedFacebook'/}}
				{{else (conf.type == 'infogram')}}
				{{include tmpl='embedInfogram'/}}
				{{else (conf.type == 'instagram')}}
				{{include tmpl='embedInstagram'/}}
				{{else (conf.type == 'twitter')}}
				{{include tmpl='embedTwitter'/}}
				{{/if}}
			</li>
			{{/if}}
			{{/if}}
		{{/if}}
	{{/for}}
	</ul>
</template>

<template type="text/x-jsrender" id="v2creatorSnapshot">
	<div class="bg-primary pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac br-rd-10 pd-t-20 pd-b-20 pd-l-md-20 pd-r-md-20 mg-t-20 bg-body">
		<div class="fx-row">
			<span class="cl-lt reg-3 md-reg-3">{{:creatorSnapshot.subtitle}}</span>
			<span class="mg-l-4 bd-6 md-bd-6">{{:creatorSnapshot.timestamp}}</span>
		</div>
		{{for creatorSnapshot.creators}}
		<div class="hz-scroll-ac hide-native-scrollbar">
			<div class="dp-ib">
				<div class="dp-fx mg-t-10 fx-al-ct">
					<a href="{{:url}}" class="dp-fx fx-al-st">
						<img data-src="{{:icon}}" alt="{{:title}}" title="{{:title}}" class="lazy wd-ht-px-36 br-rd-pr-50"/>
						<div class="pd-l-10">
							<div class="cl-lt reg-3 md-reg-3">{{:subtitle}}</div>
							<div class="bd-6 md-bd-6">{{:title}}</div>
						</div>
					</a>
					<div class="dp-fx mg-l-10">
						{{for profiles}}
						<a target="_blank" href="{{:url}}" rel="nofollow noreferrer" aria-label="Link to {{:key}} profile" class="dp-fx wd-ht-px-50 fx-al-ct mg-l-10">
							<div class="nb-icon-{{:key}} fz-14"></div>
						</a>
						{{/for}}
					</div>
				</div>
			</div>
		</div>
		{{/for}}
	</div>
</template>

<template type="text/x-jsrender" id="extraRoundedScrollableLinks">
	{{if ~compareGT(~lengthOfArr(topics.list.data), 0)}}
	<section class="card" data-cname="list-sc-links">
		<div class="ft-pr bd-4 md-bd-4 c-tt pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">{{:topics.title}}</div>
		<div class="pos-rel">
			<div class="pos-abs col-2 col-lg-1 z-index-1 gd-tint-white-smoke-white-smoke0-white-smoke100-neg-half tp-init bt-init lt-init"></div>
			<ul class="slider-init hz-scroll-ac scroll-menu pos-rel hz-scroll-lg-dac" data-sliderconf="variableWidth" id="slider-{{:htmlId}}">
				{{props topics.list.data}}
					<li class="sc-child scr-lk">
						<a href="{{:prop.url}}">
						{{if ~_and(prop.icon, ~neql(prop.icon, ''))}}
							<img data-src="{{:prop.icon}}" alt="{{:prop.title}}" title="{{:prop.title}}" class="lazy"/>
						{{/if}}
							<span class="reg-2 md-reg-2 tt">{{:prop.title}}</span>
						</a>
					</li>
				{{/props}}
				<span>&nbsp</span>
			</ul>
			<div class="pos-abs col-2 col-lg-1 z-index-1 gd-tint-white-smoke-white-smoke0-white-smoke100-half tp-init bt-init rt-init"></div>
		</div>
	</section>
	{{/if}}
</template>

<template type="text/x-jsrender" id="roundedShadowedScrollableLinksAndEmptyTitle">
	{{if ~compareGT(~lengthOfArr(topics.list.data), 0)}}
	<section class="card mg-t-10 mg-t-md-20">
		<div class="pos-rel">
			<!-- <div class="pos-abs col-2 col-lg-1 z-index-1 gd-tint-white-white0-white100-neg-half tp-init bt-init lt-init"></div> -->
			<ul class="slider-init hz-scroll-ac scroll-menu pos-rel hz-scroll-lg-dac" data-sliderconf="variableWidth" id="slider-{{:htmlId}}">
				{{props topics.list.data}}
					<li class="sc-child scr-lk">
						<a href="{{:prop.url}}">
						{{if ~_and(prop.icon, ~neql(prop.icon, ''))}}
							<img data-src="{{:prop.icon}}" alt="{{:prop.title}}" title="{{:prop.title}}" class="lazy"/>
						{{/if}}
							<span class="reg-2 md-reg-2 tt">{{:prop.title}}</span>
						</a>
					</li>
				{{/props}}
				<span>&nbsp</span>
			</ul>
			<div class="pos-abs col-2 col-lg-1 z-index-1 gd-tint-white-white0-white100-half tp-init bt-init rt-init"></div>
		</div>
	</section>
	{{/if}}
</template>

<template type="text/x-jsrender" id="embedTwitter">
	{{if conf.hideMedia}}
	<blockquote class="twitter-tweet" data-cards="hidden" data-lang="en">{{:conf.text}}</blockquote>
	{{else}}
	<blockquote class="twitter-tweet" data-lang="en">{{:conf.text}}</blockquote>
	{{/if}}
	<script>
		!function (e, t, s, i) { var n = "twttr", o = e.getElementsByTagName("script"), d = o[0], r = /^http:/.test(e.location) ? "http:" : "https:"; if (/^\/{2}/.test(i) && (i = r + i), window[n] && window[n].init) window[n].widgets.load(); else if (!e.getElementById(s)) { var a = e.createElement("script"); a.async = 1, a.id = s, a.src = i, d.parentNode.insertBefore(a, d) } }(document, 0, "Twitter-async", "//platform.twitter.com/widgets.js");
	</script>
</template>

<template type="text/x-jsrender" id="embedInstagram">
	<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; min-width: 235px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:26.25% 0; text-align:center; width:100%;"> <div style="background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <div style=" margin:8px 0 0 0; padding:0 4px;"> <a href="{{:conf.link}}" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">{{:conf.title}}</a></div> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by {{:conf.by}} on <time style="font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="{{:conf.time}}">{{:conf.strTimePST}}</time></p></div></blockquote>
	<script>
		!function (e, t, s, i) { var n = "instgrm", o = e.getElementsByTagName("script"), d = o[0], r = /^http:/.test(e.location) ? "http:" : "https:"; if (/^\/{2}/.test(i) && (i = r + i), window[n]) window[n].Embeds.process(); else if (!e.getElementById(s)) { var a = e.createElement("script"); a.async = 1, a.id = s, a.src = i, d.parentNode.insertBefore(a, d) } }(document, 0, "Instagram-async", "//platform.instagram.com/en_US/embeds.js");
	</script>
	<style>.instagram-media-rendered{min-width:235px !important;}</style>
</template>

<template type="text/x-jsrender" id="embedInfogram">
	<div class="infogram-embed" data-id="{{:conf.id}}" data-type="interactive" data-title="{{:conf.title}}"></div>
	<script>!function(e,t,s,i){var n="InfogramEmbeds",o=e.getElementsByTagName("script"),d=o[0],r=/^http:/.test(e.location)?"http:":"https:";if(/^\/{2}/.test(i)&&(i=r+i),window[n]&&window[n].initialized)window[n].process&&window[n].process();else if(!e.getElementById(s)){var a=e.createElement("script");a.async=1,a.id=s,a.src=i,d.parentNode.insertBefore(a,d)}}(document,0,"infogram-async","//e.infogr.am/js/dist/embed-loader-min.js");</script>
</template>

<template type="text/x-jsrender" id="embedFacebook">
	<div id="fb-root"></div>
	{{if ~eql(conf.embedtype, 'comment')}}
	<div class="fb-comment-embed" data-href="{{:conf.link}}" data-width="{{:conf.width}}" data-include-parent="{{:conf.includeparent}}"></div>
	{{else ~eql(conf.embedtype, 'video')}}
	<div class="fb-video" data-href="{{:conf.link}}" data-width="{{:conf.width}}" data-show-text="{{:conf.showtext}}"></div>
	{{else ~eql(conf.embedtype, 'post')}}
	<div class="fb-post" data-href="{{:conf.link}}" data-width="{{:conf.width}}" data-show-text="{{:conf.showtext}}"></div>
	{{/if}}
</template>

<template type="text/x-jsrender" id="embedApester">
	<div class="apester-media" data-media-id="{{:conf.id}}" height="{{:conf.height}}"></div>
	<script>
		!function (e, t, s, i) { var n = "APESTER", o = e.getElementsByTagName("script"), d = o[0], r = /^http:/.test(e.location) ? "http:" : "https:"; if (/^\/{2}/.test(i) && (i = r + i)) window[n].Init() && window[n].process(); else if (!e.getElementById(s)) { var a = e.createElement("script"); a.async = 1, a.id = s, a.src = i, d.parentNode.insertBefore(a, d) } }(document, 0, "Apester-async", "//static.apester.com/js/sdk/v2.0/apester-javascript-sdk.min.js");
	</script>
</template>

<template type="text/x-jsrender" id="horizontalSlider">
	<section class="pos-rel mg-t-20 event-card">
		<section class="hz-scroll-ac scroll-menu dp-fx">
			{{props topics.list.data}}
			<a href="{{:prop.url}}" class="mg-l-6 mg-r-6 pd-t-14 pd-b-14 pd-l-20 pd-r-20 br-1p-ink-dk br-rd-px-25 ft-pr reg-2">{{:prop.title}}</a>
			{{/props}}
			<span>&nbsp;</span>
			<div class="pos-abs gd-tint-white-white0-white100-neg-half lt-init tp-init bt-init col-1"></div>
			<div class="pos-abs gd-tint-white-white0-white100-half rt-init tp-init bt-init col-1"></div>
		</section>
	</section>
</template>

<template type="text/x-jsrender" id="collectionCapsuleLinks">
	{{if ~compareGT(~lengthOfArr(topics.list.data), 0)}}
	<section class="bg-primary  pd-t-10 pd-b-10 mg-t-20 bg-body  event-list-card">
		<div class="ft-pr bd-4 md-bd-4 pd-t-10 pd-l-md-20 pd-r-md-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac">{{:topics.title}}</div>
		<div class="pos-rel">
			<div class="pos-abs col-2 col-lg-1 z-index-1 gd-tint-white-smoke-white-smoke0-white-smoke100-neg-half tp-init bt-init lt-init"></div>
			<div class="mg-t-40 mg-b-30 slider-init hz-scroll-ac scroll-menu pos-rel hz-scroll-lg-dac" id="slider-{{:htmlId}}" data-sliderconf="variableWidth">
				{{props topics.list.data}}
				<div class="dp-ib mg-r-10 sc-child">
					<a href="{{:prop.url}}" class="pd-t-10 pd-b-10 pd-l-10 pd-r-20 ft-pr bg-primary br-rd-px-30 dp-fx fx-al-ct min-ht-px-60">
						{{if ~_and(prop.icon, ~neql(prop.icon, ''))}}
						<img data-src="{{:prop.icon}}" alt="{{:prop.title}}" title="{{:prop.title}}" class="lazy wd-ht-px-40 br-rd-pr-50 fx-basis-40px"/>
						{{/if}}
						<span class="reg-2 md-reg-2 pd-l-10 wht-sp-nowrap">{{:prop.title}}</span>
					</a>
				</div>
				{{/props}}
				<span>&nbsp;</span>
			</div>
			<div class="pos-abs col-2 col-lg-1 z-index-1 gd-tint-white-smoke-white-smoke0-white-smoke100-half tp-init bt-init rt-init"></div>
		</div>
	</section>
	{{/if}}
</template>

<template type="text/x-jsrender" id="listTaggedSqrRecImgVertical">
	{{if (~lengthOfArr(related.list.data))}}
	<div class="event-list-card bg-primary  pd-b-30 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac mg-t-20 bg-body event-card">
		<div class="ft-pr md-bd-4 ft-pr bd-4 pd-l-md-20 pd-r-md-20 pd-t-20">{{:related.title}}</div>
		<section class="pd-l-md-10 pd-r-md-10">
			{{for related.list.data}}
			<div class="">
				<div class="col-12 dp-fx pd-l-md-10 pd-r-md-10 mg-t-20 list-item-tagged-sqrRecImgVertical">
					<a href="{{:url}}" class="col-4 col-lg-1 fig-container--wd-ht-lg-60">
						<figure class="lazy fig fig--bg-img-ct-cov-props ht-full-dac ht-full-md-ac asp-ratio-rect-ac asp-ratio-rect-md-dac" data-src="{{:thumb}}" title="{{:title}}">
							<img data-src="{{:thumb}}" alt="{{:title}}" title="{{:title}}" class="lazy hidden inset-img-hidden">
						</figure>
					</a>
					<div class="col-8 col-lg-10 tx-container pd-l-10">
						<a href="{{:url}}" class="ft-pr bd-md-6 ft-pr bd-6 dp-bl">{{:title}}</a>
						{{if ~_and('tagUrl', ~neql('tagUrl', ''))}}
						<a href="{{:tagUrl}}" class="mg-t-2 ft-ter md-ter-bd-2 ter-bd-2 cl-lt cl-{{:~tagColor(tag)}}-dk dp-ib">{{:tag}}</a>
						{{else}}
						<div class="mg-t-2 ft-ter md-ter-bd-2 ter-bd-2 cl-lt cl-{{:~tagColor(tag)}}-dk dp-bl">{{:tag}}</div>
						{{/if}}
					</div>
				</div>
			</div>
			{{/for}}
		</section>
	</div>
	{{/if}}
</template>

<template type="text/x-jsrender" id="queryFeedback">
	<section class="event-list-card bg-primary  pd-t-10 pd-b-10 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-10 pd-r-md-10 comp-cards-list-queryFeedback mg-t-20 bg-body force-br-rd-10">
		<div class="pd-t-20 pd-l-md-10 pr-r-md-10">
			<img data-src="{{:questions.header.thumb}}" alt="{{:questions.header.title}}" title="{{:questions.header.title}}" class="lazy ht-px-45"/>
		</div>
		<div class="askQuestion">
			<div class="dp-fx pd-l-md-10 pd-r-md-10 mg-t-24">
				<img src="{{:questions.postbox.icon}}" class="wd-ht-px-36 br-rd-pr-50 userIcon userFallbackIcon" data-fallback-icon="{{:questions.postbox.icon}}" alt="Fallback Icon" title="Fallback Icon"/>
				<div class="bg-primary pd-l-20 pd-r-20 pd-t-14 pd-b-14 mg-l-10 wd-full br-rd-10 br-1p-pale-lavender">
					<div class="fx-al-end dp-fx">
						<textarea class="mg-r-10 wd-full hide-native-scrollbar ft-pr md-reg-1 reg-1 placeholder-query-packet userQuestion" data-min-rows='1' data-max="180" rows="1" aria-label="User Question Box" placeholder="{{:questions.postbox.placeholder}}"></textarea>
						<span class="mg-l-10 nb-icon-post fz-18 cl-link cs-ptr submitQuestion" data-timelineId="{{:questions.postbox.params.static.timelineId}}" data-api="{{:questions.postbox.api}}"></span>
					</div>
					<div class="dp-fx mg-t-10 fx-js-end">
						<div class="cl-lt ft-pr reg-2 loader"></div>
					</div>
				</div>
			</div>
			<div class="flashMessage width-100 mg-t-10 mg-l-46 pd-l-20 cl-link ft-pr md-bd-6 ft-pr bd-6"></div>
		</div>
		{{if ~lengthOfArr(questions.list.data)}}
		{{for questions.list.data}}
		<div class="pd-l-md-10 pr-r-md-10 dp-fx fx-alt-st mg-t-20">
			<img data-src="{{:query.icon}}" alt="{{:query.title}}" title="{{:query.title}}" class="lazy wd-ht-px-36 fx-basis-36px br-rd-pr-50"/>
			<div class="mg-l-10">
				<p class="ft-pr md-reg-2 ft-pr reg-2 dp-ib">{{:query.title}}</p>
				<span class="cl-lt md-l-10 ft-pr md-reg-3 ft-pr reg-3">{{:query.timestamp}}</span>
				<p class="ft-pr md-bd-5 ft-pr bd-5 mg-t-4">{{:query.text}}</p>
			</div>
		</div>
		<div class="mg-l-46 dp-fx fx-alt-st mg-t-10">
			<div class="bg-primary br-rd-pr-50 wd-ht-px-36 fx-basis-36px pd-t-8 pd-b-8 pd-l-8 pd-r-8">
				<img data-src="{{:feedback.icon}}" alt="{{:feedback.title}}" title="{{:feedback.title}}" class="lazy ht-full wd-full"/>
			</div>
			<div class="mg-l-10">
				<p class="cl-lt ft-pr md-reg-3 ft-pr reg-3">{{:feedback.subtitle}}</p>
				<p class="ft-pr md-reg-2 ft-pr reg-2">{{:feedback.title}}</p>
				<p class="ft-sec sec-md-reg-2 ft-sec sec-reg-2 mg-t-10">{{:feedback.text}}</p>
			</div>
		</div>
		{{/for}}
		{{/if}}
		{{if (questions.list.link)}}
		{{if ~compareGT(~lengthOfArr(questions.list.data, 2))}}
		<div class="pd-t-20 pd-b-20 pd-l-md-10">
			<div class="dp-ib">
				<a class="dp-fx cl-link navy-blue-cl-hover" href="{{:questions.list.link.url}}">
					<span class="ft-pr bd-6 ft-pr md-bd-6">{{:questions.list.link.title}}</span>
					<span class="nb-icon-arrow-forward fz-20 mg-l-6"></span>
				</a>
			</div>
		</div>
		{{/if}}
		{{/if}}
	</section>
</template>

<template type="text/x-jsrender" id="v2queryFeedback">
	<section class="bg-primary pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac card">
		<div class="askQuestion">
			<div class="ft-pr post-box">
				<img src="{{:questions.postbox.icon}}" class="lazy userIcon userFallbackIcon" data-fallback-icon="{{:questions.postbox.icon}}" alt="Fallback Icon" title="Fallback Icon"/>
					<div class="br-1p-pale-lavender t-box">
						<div class="fx-al-end dp-fx">
							<textarea class="mg-r-10 wd-full hide-native-scrollbar ft-pr md-reg-1 reg-1 placeholder-query-packet userQuestion" data-min-rows='1' data-max="180" rows="1" aria-label="User Question Box" placeholder="{{:questions.postbox.placeholder}}"></textarea>
							<span class="nb-icon-post cl-link p-icon submitQuestion" data-timelineId="{{:questions.postbox.params.static.timelineId}}" data-api="{{:questions.postbox.api}}"></span>
						</div>
						<div class="dp-fx mg-t-10 fx-js-end">
							<div class="cl-lt reg-2 loader"></div>
						</div>
					</div>
				</div>
			<div class="flashMessage cl-link md-bd-6 bd-6 f-mssg"></div>
		</div>
		{{if (questions.list.link)}}
		{{if ~compareGT(~lengthOfArr(questions.list.data, 2))}}
		<div class="dp-ib in-lk cl-link s-s-lk">
			<a class="dp-fx" href="{{:questions.list.link.url}}">
				<span class="bd-6 md-bd-6">{{:questions.list.link.title}}</span>
				<span class="nb-icon-arrow-forward icon"></span>
			</a>
		</div>
		{{/if}}
		{{/if}}
	</section>
</template>

<template type="text/x-jsrender" id="dropdownHoverableButton">
	<div class="dp-ib pos-rel drdown-selector-hoverable ft-pr">
		{{if (selector.linkable)}}
		<a class="dp-bl bg-primary pd-t-10 pd-b-10 pd-l-14 pd-r-14 br-rd-10 cl-ink-dk ff-primary cs-ptr selector-url alice-blue-bg-hover" href="{{:selector.url}}">
			{{if ~neql(selector.label, '')}}
			<div class="ft-ter md-ter-reg-2 ter-reg-2 selector-label">{{:selector.label}}</div>
			{{/if}}
			{{if ~neql(selector.title, '')}}
			<div class="dp-fx">
				<span class="ft-ter md-ter-reg-2 ter-reg-2 hoverable-title selector-title">{{:selector.title}}</span>
				<span class="nb-icon-drop-more fz-16 mg-l-6"></span>
			</div>
			{{/if}}
		</a>
		{{else}}
		<div class="dp-bl bg-primary pd-t-10 pd-b-10 pd-l-14 pd-r-14 br-rd-10 cl-ink-dk ff-primary cs-ptr alice-blue-bg-hover">
			{{if ~neql(selector.label, '')}}
			<div class="ft-ter md-ter-reg-2 ter-reg-2 selector-label">{{:selector.label}}</div>
			{{/if}}
			{{if ~neql(selector.title, '')}}
			<div class="dp-fx">
				<span class="ft-ter md-ter-reg-2 ter-reg-2 hoverable-title selector-title">{{:selector.title}}</span>
				<span class="nb-icon-drop-more fz-16 mg-l-6"></span>
			</div>
			{{/if}}
		</div>
		{{/if}}
		{{if ~lengthOfArr(content.data)}}
		<div class="hidden pos-abs z-index-1 bg-primary min-wd-px-160 mx-ht-px-230 wd-full box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 ovr-scroll hide-native-scrollbar ovr-x-hidden animation-dur-scale-1 ent-slide-down-pd drdown-box">
			{{for content.data}}
			<a class="hoverable-item dp-bl pd-l-20 pd-t-12 pd-b-12 pd-r-20 cl-lt ft-ter md-ter-reg-2 ter-reg-2 black-cl-hover alice-blue-bg-hover {{if (selected)}}bg-alice-blue selected{{/if}}" href="{{:url}}">{{:name}}</a>
			{{/for}}
		</div>
		{{/if}}
	</div>
</template>

<template type="text/x-jsrender" id="queryRespTemplateWiththImgAndShare">
	<section class="response-section mg-t-30">
		<div class="dp-fx fx-dr-col fx-al-ct">
			<div class="col-8 fx-row">
				<div class="col-12 col-md-6 pd-r-md-10">
					{{if image}}
					<img src="{{:image}}" class="wd-full" alt="{{:title}}"/>
					{{/if}}
				</div>
				<div class="col-12 col-md-6 pd-l-md-10 mg-t-20 mg-t-md-0">
					<div class="bd-3 md-bd-3">{{:title}}</div>
					<div class="wd-full bg-primary box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 drdown-box reg-3 md-reg-3 mg-t-20">
						<a class="dp-fx fx-al-ct pd-l-20 pd-t-12 pd-b-12 cl-lt black-cl-hover alice-blue-bg-hover" href="https://www.facebook.com/sharer/sharer.php?u={{:socialShare.url}}" target="_blank" rel="nofollow noreferrer" aria-label="Facebook">
							<span class="nb-icon-facebook fz-17"></span>
							<span class="pd-l-12">Facebook</span>
						</a>
						<a class="dp-fx fx-al-ct pd-l-20 pd-t-12 pd-b-12 cl-lt black-cl-hover alice-blue-bg-hover hidden-md" href="whatsapp://send?text={{:socialShare.text}}%20{{:socialShare.url}}" target="_blank" rel="nofollow noreferrer" aria-label="Whatsapp">
							<span class="nb-icon-whatsapp fz-17"></span>
							<span class="pd-l-12">Whatsapp</span>
						</a>
						<a class="dp-fx fx-al-ct pd-l-20 pd-t-12 pd-b-12 cl-lt black-cl-hover alice-blue-bg-hover" href="https://twitter.com/intent/tweet?via=newsbytesapp&text={{:socialShare.text}}&url={{:socialShare.url}}" target="_blank" aria-label="Twitter" rel="nofollow noreferrer">
							<span class="nb-icon-twitter fz-17"></span>
							<span class="pd-l-12">Twitter</span>
						</a>
						<a class="dp-fx fx-al-ct pd-l-20 pd-t-12 pd-b-12 cl-lt black-cl-hover alice-blue-bg-hover" href="https://www.linkedin.com/shareArticle?mini=true&source=newsbytes&url={{:socialShare.url}}" target="_blank" rel="nofollow noreferrer" aria-label="Linkedin">
							<span class="nb-icon-linkedin fz-17"></span>
							<span class="pd-l-12">Linkedin</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</template>

<template type="text/x-jsrender" id="queryRespTemplateWiththSuccessMssg">
	<section class="response-section mg-t-30">
		<div class="tx-ct ft-pr bd-6 md-bd-6 navy-blue-cl-hover cl-link animation-dur-scale-2 ent-slide-down">{{:title}}</div>
	</section>
</template>

<template type="text/x-jsrender" id="storyListItemVariableSizeImg">
	{{for list['data']}}
		<div data-url="{{:url}}" class="col-6 col-md-2 pd-l-10 pd-r-10 mg-t-20 pos-rel cs-ptr start-player full-size">
			<img src="{{:img}}" class="item variable-size wd-full" alt="{{:title}}"/>
			<div class="pos-abs bt-init ht-half story-item-text-area variable-size">
				<div class="ht-full pos-rel">
					<a href="{{:url}}" class="bt-init cl-primary pd-b-10 pd-l-10 pd-r-10 pos-abs ft-pr md-bd-3 bd-3">{{:title}}</a>
				</div>
			</div>
		</div>
	{{/for}}
</template>

<template type="text/x-jsrender" id="loginModal">
	<section id="loginZone" class="login-overlay bg-primary pos-fix wd-full lt-init bt-init rt-init tp-init z-index-10" data-overlay="false">
	<iframe src="https://accounts.newsbytesapp.com/login{{:locale}}?s={{:s}}&p={{:p}}" width="100%" height="100%" title="User Login"></iframe>
	</section>
</template>

<template type="text/x-jsrender" id="youtubeFrameInPlace">
	<iframe src="" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture; autoplay"></iframe>
</template>

<template type="text/x-jsrender" id="youtubeFrameFullScreen">
	<iframe src="" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture; autoplay; fullscreen" class="pos-abs lt-init tp-init wd-full ht-full"></iframe>
</template>

<template type="text/x-jsrender" id="liveScoreTennis">
	<div class="br-1p-pale-lavender br-rd-md-10">
		<div class="pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac bd-6 md-bd-6 cl-ink-dk pd-l-md-20 pd-r-md-20 pd-t-14">{{:title}}</div>
		<div class="mg-t-6 nested-tabs-container">
			<div class="dp-fx br-b-1p-pale-lavender">
				{{for tabs.list.data}}
				{{if ~eql(#getIndex(), 0)}}
				<div class="pd-t-10 pd-b-10 cs-ptr tx-ct fx-grow-1 bd-7 md-bd-7 cl-suva-grey nested-tabs level-1 selected" data-id="{{:id}}">{{:title}}</div>
				{{else}}
				<div class="pd-t-10 pd-b-10 cs-ptr tx-ct fx-grow-1 bd-7 md-bd-7 cl-suva-grey nested-tabs level-1" data-id="{{:id}}">{{:title}}</div>
				{{/if}}
				{{/for}}
			</div>
			<div class="nested-tabs-container">
				{{for tabs.list.data}}
				{{if ~eql(#getIndex(), 0)}}
				<div class="tab-content" data-mapped="{{:id}}">
				{{else}}
				<div class="tab-content hidden" data-mapped="{{:id}}">
				{{/if}}
					<div class="dp-fx fx-grow-1 fx-al-ct br-b-1p-pale-lavender">
						{{for list.data}}
						{{if ~eql(#getIndex(), 0)}}
						<div class="cs-ptr pd-t-6 pd-b-6 cl-suva-grey fx-grow-1 reg-3 md-reg-3 nested-tabs selected" data-id="{{:id}}">
						{{else}}
						<div class="cs-ptr pd-t-6 pd-b-6 cl-suva-grey fx-grow-1 reg-3 md-reg-3 nested-tabs" data-id="{{:id}}">
						{{/if}}
							<div class="tx-ct">{{:title}}</div>
							<div class="tx-ct">{{:subtitle}}</div>
						</div>
						{{/for}}
					</div>
					{{for list.data}}
					{{if ~eql(#getIndex(), 0)}}
					<div class="tab-content" data-mapped="{{:id}}">
					{{else}}
					<div class="tab-content hidden" data-mapped="{{:id}}">
					{{/if}}
						<div class="owl-carousel owl-theme carousel" id="carousel-{{:id}}">
							{{for list.data}}
							<div class="br-b-1p-pale-lavender item">
								<div class="card pd-t-20 pd-b-20 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 cs-ptr">
									<div class="br-rd-30 pd-t-6 pd-b-6 pd-l-10 pd-r-10 dp-ib cl-ink-dk reg-3 md-reg-3 bg-pale-lavender">
										<span class="dp-ib">{{:title}}</span>
										<span class="dp-ib mg-l-4">{{:subtitle}}</span>
									</div>
									{{for items}}
									<div class="dp-fx fx-al-ct fx-js-bw mg-t-10">
										<div class="dp-fx fx-al-ct">
											<img src="{{:img}}" alt="{{:subtitle}}" class="wd-ht-px-32 br-rd-pr-50"/>
											<div class="mg-l-10">
												<div class="cl-ink-dk bd-6 md-bd-6">{{:title}}</div>
												<div class="cl-suva-grey reg-3 md-reg-3">{{:subtitle}}</div>
											</div>
										</div>
										<div class="dp-fx bd-6 md-bd-6 param-row">
											{{for params}}
											<div class="pd-l-6 pd-r-6 param-item">{{:title}}</div>
											{{/for}}
										</div>
									</div>
									{{/for}}
									<div class="mg-t-20 br-b-1p-pale-lavender"></div>
									{{for list}}
									<div class="dp-fx fx-js-bw fx-al-ct mg-t-20 reg-2 md-reg-2 score-row">
										{{for data ~title=title}}
										{{if ~eql((#getIndex() % 2), 1)}}
										{{if (~neql(~title, ''))}}
										<div class="tx-ct score-title">{{:~title}}</div>
										{{/if}}
										{{/if}}
										{{if ~eql((#getIndex() % 2), 0)}}
										<div class="tx-lt score-{{:#getIndex()}}">{{:title}}</div>
										{{else}}
										<div class="tx-rt score-{{:#getIndex()}}">{{:title}}</div>
										{{/if}}
										{{/for}}
									</div>
									{{/for}}
								</div>
							</div>
							{{/for}}
						</div>
					</div>
					{{/for}}
				</div>
				{{/for}}
			</div>
		</div>
	</div>
</template>

<template type="text/x-jsrender" id="liveOlympicsTally">
	<div class="br-1p-pale-lavender br-rd-md-10">
		<div class="dp-fx fx-al-ct fx-js-bw pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 pd-t-14">
			<div class="bd-4 md-bd-4 cl-ink-dk">{{:title}}</div>
			<img src="{{:img}}" alt="{{:title}}" class="wd-ht-px-32 mg-l-10">
		</div>
		<div class="mg-t-12 nested-tabs-container">
			<div class="dp-fx br-b-1p-pale-lavender">
				{{for tabs.list.data}}
				{{if ~eql(#getIndex(), 0)}}
				<div class="pd-t-10 pd-b-10 cs-ptr tx-ct fx-grow-1 bd-7 md-bd-7 cl-suva-grey nested-tabs level-1 selected" data-id="{{:id}}">{{:title}}</div>
				{{else}}
				<div class="pd-t-10 pd-b-10 cs-ptr tx-ct fx-grow-1 bd-7 md-bd-7 cl-suva-grey nested-tabs level-1" data-id="{{:id}}">{{:title}}</div>
				{{/if}}
				{{/for}}
			</div>
			{{for tabs.list.data}}
			{{if ~eql(#getIndex(), 0)}}
			<div class="tab-content" data-mapped="{{:id}}">
			{{else}}
			<div class="tab-content hidden" data-mapped="{{:id}}">
			{{/if}}
				<div class="pd-t-6 pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 pd-b-20 card">
					<div class="dp-fx fx-al-ct fx-js-bw">
						<div class="cl-ink-dk reg-3 md-reg-3">{{:header.title}}</div>
						<div class="dp-fx fx-al-st">
							{{props header.images}}
							<img src="{{:prop}}" alt="Header Images" class="wd-ht-px-28 mg-l-10">
							{{/props}}
						</div>
					</div>
					<div class="pd-t-10">
						{{for list.data}}
						<div class="item-row dp-fx fx-al-ct fx-js-bw pd-t-10">
							<div class="dp-fx fx-al-ct">
								<div class="cl-ink-dk reg-3 md-reg-3 wd-px-25 fx-basis-25px serial">{{:serial}}</div>
								<div class="dp-fx fx-al-ct">
									<img src="{{:img}}" alt="{{:title}}" class="wd-ht-px-32 img">
									<div class="cl-ink-dk bd-6 md-bd-6 mg-l-6 title">{{:title}}</div>
								</div>
							</div>
							<div class="dp-fx fx-al-ct cl-ink-dk bd-6 md-bd-6">
								{{for params}}
								<div class="wd-px-28 tx-ct mg-l-10 param">{{:title}}</div>
								{{/for}}
							</div>
						</div>
						{{/for}}
					</div>
				</div>
			</div>
			{{/for}}
		</div>
		{{if link}}
		<a class="dp-fx fx-js-ct fx-al-ct pd-t-10 pd-b-10 wd-full br-t-1p-pale-lavender" href="{{:link.url}}">
			<span class="bd-6 md-bd-6 cl-link">{{:link.title}}</span>
		</a>
		{{/if}}
	</div>
</template>

<template type="text/x-jsrender" id="liveBlog">
	<div class="live-blog pos-rel">
		<div class="header pd-l-20 pd-r-20 pd-b-20 br-rd-tl-tr-10px cs-ptr pointer-events-none-lg">
			<div class="hidden-lg pd-t-10 dp-fx fx-js-ct">
				<div class="bar bg-primary"></div>
			</div>
			<div class="cl-primary dp-fx fx-js-bw pd-t-16 pd-t-lg-20">
				<div class="ft-pr md-bd-5 bd-5">{{:title}}</div>
				{{if img}}
				<div class="bg-primary wd-ht-px-50 mg-l-10 dp-fx fx-al-ct fx-js-ct">
					<img src="{{:img}}" alt="{{:title}}" class="wd-ht-px-40">
				</div>
				{{/if}}
			</div>
		</div>
		<div class="container ovr-hidden br-rd-bl-br-md-10px hide-native-scrollbar pos-rel close">
			{{if link}}
			<div class="pd-l-10 pd-r-10 pd-t-20 dp-fx wd-full">
				<a href="{{:link.url}}" class="dp-fx fx-al-ct pd-t-2 pd-b-2 pd-l-4 pd-r-6 br-rd-6 br-1p-brand">
					<div class="animation-dur-scale-10 bg-brand br-rd-pr-50 flicker infinite-animation wd-ht-px-4"></div>
					<span class="ft-ter md-ter-bd-1 ter-bd-1 mg-l-4 cl-brand">{{:link.title}}</span>
				</a>
			</div>
			{{/if}}
			<div class="pd-l-10 pd-r-10 pd-b-60 append-list-items"></div>
		</div>
		<div class="container-tint pos-abs bt-init lt-init rt-init wd-full hidden-in-touch"></div>
	</div>
</template>

<template type="text/x-jsrender" id="liveBlogItemsList">
	<div class="list">
	{{for items}}
	<div class="card bg-primary pd-t-10 pd-b-20 pd-l-10 pd-r-10 mg-t-10 clink-dk" data-id="{{:id}}">
		{{if ~_and(timestamp, ~neql(timestamp, ''))}}
		<div class="timestamp ft-ter md-ter-reg-1 ter-reg-1">{{:timestamp}}</div>
		{{/if}}
		{{if ~_and(link, ~neql(link, ''))}}
		<a href="{{:link}}" class="title-link">
			<div class="ft-pr md-bd-4 bd-4 mg-t-10 title">{{:title}}</div>
		</a>
		{{else}}
		<div class="ft-pr md-bd-4 bd-4 mg-t-10 title">{{:title}}</div>
		{{/if}}
		{{if ~_and(description, ~neql(description, ''))}}
		<div class="ft-sec md-sec-reg-4 sec-reg-4 mg-t-6 description">{{:description}}</div>
		{{/if}}
		{{if embed}}
		<div class="mg-t-6">
			{{if (embed.conf.type == 'apester')}}
				<div class="apester-media" data-media-id="{{:embed.conf.id}}" height="{{:embed.conf.height}}"></div>
				<script>
					!function (e, t, s, i) { var n = "APESTER", o = e.getElementsByTagName("script"), d = o[0], r = /^http:/.test(e.location) ? "http:" : "https:"; if (/^\/{2}/.test(i) && (i = r + i)) window[n].Init() && window[n].process(); else if (!e.getElementById(s)) { var a = e.createElement("script"); a.async = 1, a.id = s, a.src = i, d.parentNode.insertBefore(a, d) } }(document, 0, "Apester-async", "//static.apester.com/js/sdk/v2.0/apester-javascript-sdk.min.js");
				</script>
			{{else (embed.conf.type == 'facebook')}}
				<div id="fb-root"></div>
				{{if ~eql(embed.conf.embedtype, 'comment')}}
				<div class="fb-comment-embed" data-href="{{:embed.conf.link}}" data-width="{{:embed.conf.width}}" data-include-parent="{{:embed.conf.includeparent}}"></div>
				{{else ~eql(embed.conf.embedtype, 'video')}}
				<div class="fb-video" data-href="{{:embed.conf.link}}" data-width="{{:embed.conf.width}}" data-show-text="{{:embed.conf.showtext}}"></div>
				{{else ~eql(embed.conf.embedtype, 'post')}}
				<div class="fb-post" data-href="{{:embed.conf.link}}" data-width="{{:embed.conf.width}}" data-show-text="{{:embed.conf.showtext}}"></div>
				{{/if}}
			{{else (embed.conf.type == 'infogram')}}
				<div class="infogram-embed" data-id="{{:embed.conf.id}}" data-type="interactive" data-title="{{:embed.conf.title}}"></div>
				<script>!function(e,t,s,i){var n="InfogramEmbeds",o=e.getElementsByTagName("script"),d=o[0],r=/^http:/.test(e.location)?"http:":"https:";if(/^\/{2}/.test(i)&&(i=r+i),window[n]&&window[n].initialized)window[n].process&&window[n].process();else if(!e.getElementById(s)){var a=e.createElement("script");a.async=1,a.id=s,a.src=i,d.parentNode.insertBefore(a,d)}}(document,0,"infogram-async","//e.infogr.am/js/dist/embed-loader-min.js");</script>
			{{else (embed.conf.type == 'instagram')}}
				<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; min-width: 235px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:26.25% 0; text-align:center; width:100%;"> <div style="background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="{{:embed.conf.link}}" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">{{:embed.conf.title}}</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by {{:embed.conf.by}} on <time style="font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="{{:embed.conf.time}}">{{:embed.conf.strTimePST}}</time></p></div></blockquote>
				<script>
					!function (e, t, s, i) { var n = "instgrm", o = e.getElementsByTagName("script"), d = o[0], r = /^http:/.test(e.location) ? "http:" : "https:"; if (/^\/{2}/.test(i) && (i = r + i), window[n]) window[n].Embeds.process(); else if (!e.getElementById(s)) { var a = e.createElement("script"); a.async = 1, a.id = s, a.src = i, d.parentNode.insertBefore(a, d) } }(document, 0, "Instagram-async", "//platform.instagram.com/en_US/embeds.js");
				</script>
				<style>.instagram-media-rendered{min-width:235px !important;}</style>
			{{else (embed.conf.type == 'twitter')}}
				{{if embed.conf.hideMedia}}
				<blockquote class="twitter-tweet" data-cards="hidden" data-lang="en">{{:embed.conf.text}}</blockquote>
				{{else}}
				<blockquote class="twitter-tweet" data-lang="en">{{:embed.conf.text}}</blockquote>
				{{/if}}
				<script>
					!function (e, t, s, i) { var n = "twttr", o = e.getElementsByTagName("script"), d = o[0], r = /^http:/.test(e.location) ? "http:" : "https:"; if (/^\/{2}/.test(i) && (i = r + i), window[n] && window[n].init) window[n].widgets.load(); else if (!e.getElementById(s)) { var a = e.createElement("script"); a.async = 1, a.id = s, a.src = i, d.parentNode.insertBefore(a, d) } }(document, 0, "Twitter-async", "//platform.twitter.com/widgets.js");
				</script>
			{{/if}}
		</div>
		{{/if}}
	</div>
	{{/for}}
	</div>
</template>

<template type="text/x-jsrender" id="liveScoreCricketTestBkp">
	<div class="br-1p-pale-lavender br-rd-md-10 live-cricket-test">
		<div class="pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac ft-ter ter-bd-4 md-ter-bd-4 cl-ink-dk pd-l-md-20 pd-r-md-20 pd-t-14 pd-b-14">{{:title}}</div>
		{{if ~compareGT(~lengthOfArr(comparison.items), 0)}}
		<div class="br-t-1p-pale-lavender pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 pd-t-20 pd-b-20 cs-ptr comparison-item">
			<div class="dp-fx fx-js-bw fx-al-ct">
				<div class="ft-ter ter-reg-1 ter-md-reg-1 pd-t-4 pd-b-4 pd-l-10 pd-r-10 br-rd-15 bg-pale-lavender">{{:comparison.venue}}</div>
				<div class="dp-fx fx-al-ct pd-t-6 pd-b-6 pd-l-6 pd-r-10 br-rd-6 br-1p-brand">
					<div class="animation-dur-scale-10 bg-brand br-rd-pr-50 flicker infinite-animation wd-ht-px-4"></div>
					<span class="ft-ter ter-md-bd-1 ter-bd-1 mg-l-6 cl-brand">{{:comparison.liveStatus.title}}</span>
				</div>
			</div>
			{{if ~compareGT(~lengthOfArr(comparison.header), 0)}}
			<div class="mg-t-20 dp-fx fx-js-end">
				{{props comparison.header}}
				<div class="col-3 tx-rt ft-ter ter-md-reg-1 ter-reg-1 cl-suva-grey">
					{{:prop}}
				</div>
				{{/props}}
			</div>
			{{/if}}
			{{for comparison.items}}
			{{if ~eql(#getIndex(), 0)}}
			<div class="dp-fx fx-js-bw fx-al-ct">
			{{else}}
			<div class="mg-t-10 dp-fx fx-js-bw fx-al-ct">
			{{/if}}
				<div class="dp-fx fx-al-ct">
					<img src="{{:img}}" alt="{{:title}}" class="wd-ht-px-32 br-rd-pr-50">
					<div class="mg-l-10">
						<div class="ft-ter ter-md-bd-2 ter-bd-2">{{:title}}</div>
						<div class="ft-ter ter-md-reg-1 ter-reg-1 cl-suva-grey">{{:subtitle}}</div>
					</div>
				</div>
				<div class="dp-fx fx-al-ct col-6">
					{{for params}}
					<div class="col-6 tx-rt ft-ter ter-md-bd-2 ter-bd-2">{{:title}}</div>
					{{/for}}
				</div>
			</div>
			{{/for}}
			<div class="ft-ter ter-md-reg-2 ter-reg-2 cl-brand mg-t-10 tx-ct">{{:comparison.conclusion}}</div>
		</div>
		{{/if}}
		{{if tabularSection}}
		<div class="tabular-item hidden">
			<div class="dp-fx br-b-1p-pale-lavender br-t-1p-pale-lavender">
				{{for tabularSection ~len=tabularSection.length}}
				{{if ~eql(#getIndex(), 0)}}
				<div class="pd-t-8 pd-b-8 cs-ptr tx-ct fx-grow-1 ft-ter ter-md-reg-1 ter-reg-1 cl-suva-grey nested-tabs level-1 selected" data-id="{{:id}}">
				{{else}}
				<div class="pd-t-8 pd-b-8 cs-ptr tx-ct fx-grow-1 ft-ter ter-md-reg-1 ter-reg-1 cl-suva-grey nested-tabs level-1" data-id="{{:id}}">
				{{/if}}
					<div class="tx-ct">{{:tab.title}}</div>
					<div class="tx-ct">{{:tab.subtitle}}</div>
				</div>
				{{/for}}
			</div>
			{{for tabularSection ~len=tabularSection.length}}
			{{if ~eql(#getIndex(), 0)}}
			<div class="tab-content pd-b-20" data-mapped="{{:id}}">
			{{else}}
			<div class="tab-content hidden pd-b-20" data-mapped="{{:id}}">
			{{/if}}
				{{for tables}}
					<div class="ft-ter ter-md-bd-2 ter-bd-2 pd-l-20 pd-r-20 mg-t-20">{{:title}}</div>
					<div class="table-rows">
						{{for rows itemVar="~item" ~i=#getIndex()}}
							<div class="table-row dp-fx">
								{{props ~item}}
									<div class="pd-l-6 pd-r-6 pd-t-6 pd-b-6 table-cell ft-ter ter-md-reg-2 ter-reg-2">{{:prop}}</div>
								{{/props}}
							</div>
						{{/for}}
					</div>
					{{for extraData itemVar="~item"}}
					<div class="pd-t-6 fx-row fx-js-bw ft-ter ter-md-reg-2 ter-reg-2">
						<div class="pd-l-20 pd-r-20 pd-t-6">{{:title}}</div>
						<div class="pd-l-20 pd-r-20 pd-t-6">{{:subtitle}}</div>
					</div>
					{{/for}}
				{{/for}}
			</div>
			{{/for}}
		</div>
		{{/if}}
	</div>
</template>

<template type="text/x-jsrender" id="liveScoreCricketTest">
	<div class="br-1p-pale-lavender br-rd-md-10 live-cricket-test">
		<div class="pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac ft-ter ter-bd-4 md-ter-bd-4 cl-ink-dk pd-l-md-20 pd-r-md-20 pd-t-14 pd-b-14">{{:title}}</div>
		{{if tabularSection}}
		<div>
			<div class="dp-fx br-b-1p-pale-lavender br-t-1p-pale-lavender">
				{{for tabularSection ~len=tabularSection.length}}
				{{if ~eql(#getIndex(), 0)}}
				<div class="pd-t-8 pd-b-8 cs-ptr tx-ct fx-grow-1 ft-ter ter-md-reg-1 ter-reg-1 cl-suva-grey nested-tabs selected" data-id="{{:#getIndex()}}">
				{{else}}
				<div class="pd-t-8 pd-b-8 cs-ptr tx-ct fx-grow-1 ft-ter ter-md-reg-1 ter-reg-1 cl-suva-grey nested-tabs" data-id="{{:#getIndex()}}">
				{{/if}}
					<div class="tx-ct">{{:title}}</div>
				</div>
				{{/for}}
			</div>
			{{for tabularSection ~len=tabularSection.length}}
			{{if ~eql(#getIndex(), 0)}}
			<div class="tab-content" data-mapped="{{:#getIndex()}}">
			{{else}}
			<div class="tab-content hidden" data-mapped="{{:#getIndex()}}">
			{{/if}}
				{{if ~compareGT(~lengthOfArr(comparison.items), 0)}}
				<div class="pd-l-r-full-wd-ac pd-l-r-full-wd-md-dac pd-l-md-20 pd-r-md-20 pd-t-20 pd-b-20 cs-ptr comparison-item">
					<div class="dp-fx fx-js-bw fx-al-ct">
						<div class="ft-ter ter-reg-1 ter-md-reg-1 pd-t-4 pd-b-4 pd-l-10 pd-r-10 br-rd-15 bg-pale-lavender">{{:comparison.venue}}</div>
						<div class="dp-fx fx-al-ct pd-t-6 pd-b-6 pd-l-6 pd-r-10 br-rd-6 br-1p-brand">
							<div class="animation-dur-scale-10 bg-brand br-rd-pr-50 flicker infinite-animation wd-ht-px-4"></div>
							<span class="ft-ter ter-md-bd-1 ter-bd-1 mg-l-6 cl-brand">{{:comparison.liveStatus.title}}</span>
						</div>
					</div>
					{{if ~compareGT(~lengthOfArr(comparison.header), 0)}}
					<div class="mg-t-20 dp-fx fx-js-end">
						{{props comparison.header}}
						<div class="col-3 tx-rt ft-ter ter-md-reg-1 ter-reg-1 cl-suva-grey">
							{{:prop}}
						</div>
						{{/props}}
					</div>
					{{/if}}
					{{for comparison.items}}
					{{if ~eql(#getIndex(), 0)}}
					<div class="dp-fx fx-js-bw fx-al-ct">
					{{else}}
					<div class="mg-t-10 dp-fx fx-js-bw fx-al-ct">
					{{/if}}
						<div class="dp-fx fx-al-ct">
							<img src="{{:img}}" alt="{{:title}}" class="wd-ht-px-32 br-rd-pr-50">
							<div class="mg-l-10">
								<div class="ft-ter ter-md-bd-2 ter-bd-2">{{:title}}</div>
								<div class="ft-ter ter-md-reg-1 ter-reg-1 cl-suva-grey">{{:subtitle}}</div>
							</div>
						</div>
						<div class="dp-fx fx-al-ct col-6">
							{{for params}}
							<div class="col-6 tx-rt ft-ter ter-md-bd-2 ter-bd-2">{{:title}}</div>
							{{/for}}
						</div>
					</div>
					{{/for}}
					<div class="ft-ter ter-md-reg-2 ter-reg-2 cl-brand mg-t-10 tx-ct">{{:comparison.conclusion}}</div>
				</div>
				{{/if}}
				{{if tabs}}
				<div class="tabular-item hidden">
					<div class="dp-fx br-b-1p-pale-lavender br-t-1p-pale-lavender">
						{{for tabs ~len=tabs.length}}
						{{if ~eql(#getIndex(), 0)}}
						<div class="pd-t-8 pd-b-8 cs-ptr tx-ct fx-grow-1 ft-ter ter-md-reg-1 ter-reg-1 cl-suva-grey nested-tabs level-1 selected" data-id="{{:id}}">
						{{else}}
						<div class="pd-t-8 pd-b-8 cs-ptr tx-ct fx-grow-1 ft-ter ter-md-reg-1 ter-reg-1 cl-suva-grey nested-tabs level-1" data-id="{{:id}}">
						{{/if}}
							<div class="tx-ct">{{:tab.title}}</div>
							<div class="tx-ct">{{:tab.subtitle}}</div>
						</div>
						{{/for}}
					</div>
					{{for tabs ~len=tabs.length}}
					{{if ~eql(#getIndex(), 0)}}
					<div class="tab-content pd-b-20" data-mapped="{{:id}}">
					{{else}}
					<div class="tab-content hidden pd-b-20" data-mapped="{{:id}}">
					{{/if}}
						{{for tables}}
							<div class="ft-ter ter-md-bd-2 ter-bd-2 pd-l-20 pd-r-20 mg-t-20">{{:title}}</div>
							<div class="table-rows">
								{{for rows itemVar="~item" ~i=#getIndex()}}
									<div class="table-row dp-fx">
										{{props ~item}}
											<div class="pd-l-6 pd-r-6 pd-t-6 pd-b-6 table-cell ft-ter ter-md-reg-2 ter-reg-2">{{:prop}}</div>
										{{/props}}
									</div>
								{{/for}}
							</div>
							{{for extraData itemVar="~item"}}
							<div class="pd-t-6 fx-row fx-js-bw ft-ter ter-md-reg-2 ter-reg-2">
								<div class="pd-l-20 pd-r-20 pd-t-6">{{:title}}</div>
								<div class="pd-l-20 pd-r-20 pd-t-6">{{:subtitle}}</div>
							</div>
							{{/for}}
						{{/for}}
					</div>
					{{/for}}
				</div>
				{{/if}}
			</div>
			{{/for}}
		</div>
		{{/if}}
	</div>
</template>

<template type="text/x-jsrender" id="advt">
	<div class="{{:type}} {{:slot}} dp-fx fx-dr-col fx-js-ct fx-al-ct">
		<div id="{{:id}}"></div>
	</div>
</template>

<template type="text/x-jsrender" id="adsenseAdvt">
	<div class="mg-t-10"></div>
	<div class="{{:type}} {{:slot}} dp-fx fx-dr-col fx-js-ct fx-al-ct mg-t-10 mg-b-10">
		<ins class="adsbygoogle" style="display:inline-block;width:{{:width}}px;height:{{:height}}px" data-ad-client="{{:clientId}}" data-ad-slot="{{:slotId}}"></ins>
	</div>
</template>

<template type="text/x-jsrender" id="fallbackAdvt">
	<div class="bg-primary dp-fx fx-al-ct fx-js-ct tp-init bt-init lt-init rt-init fx-dr-col" style="width:{{:width}}px;height:{{:height}}px">
		<div class="dp-fx fx-al-ct fx-js-bw pd-t-10 pd-b-10 pd-r-10 pd-l-10 wd-full">
			<span class="ft-ter ter-md-reg-3 ter-reg-3">{{:title}}</span>
			<div class="ht-px-36 icon-embed"></div>
		</div>
		{{for events itemVar="~item"}}
		<a href="{{:url}}" alt="{{:title}}" class="dp-fx fx-js-bw wd-full pd-t-10 pd-b-10 pd-r-10 pd-l-10 fallback-ad">
			<div class="dp-fx fx-js-ct fx-dr-col">
				<span class="ft-pr md-bd-3 bd-3 dp-bl">{{:title}}</span>
				<span class="mg-t-2 ft-ter md-ter-bd-2 ter-bd-2 cl-lt cl-{{:~tagColor(tag)}}-dk dp-bl">{{:tag}}</span>
			</div>
			<span class="fz-24 nb-icon-external-link"></span>
		</a>
		{{/for}}
	</div>
</template>
