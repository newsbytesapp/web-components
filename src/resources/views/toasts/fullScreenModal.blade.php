<div class="solid-tint-black-black90 pos-fix wd-full ht-full lt-init rt-inint tp-init bt-init full-screen-modal hidden dp-fx fx-dr-col">
    <div class="dp-fx fx-al-ct fx-js-end ht-px-40 insert-content pos-fix">
        <div class="cl-primary fz-30 cs-ptr pd-r-20 mg-t-14 nb-icon-cancel close-full-screen-modal"></div>
    </div>
</div>