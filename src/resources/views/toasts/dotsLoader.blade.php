{{-- <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 64 64" style="enable-background:new 0 0 64 64;" xml:space="preserve" alt="Dots Wave Loader" title="Dots Wave Loader" class="dp-bl mg-lr-auto wd-ht-px-32 hidden dot-loader">
    <style type="text/css">
        .dot-loader .st0{fill:#064893}.bounce{animation:bounce 2.4s infinite}.bounce:nth-child(3n+0){animation-delay:.6s}.bounce:nth-child(3n+1){animation-delay:1.4s}.bounce:nth-child(3n+2){animation-delay:2.2s}@keyframes bounce{0%,25%,50%,75%,100%{transform:translateY(0)}40%{transform:translateY(-20px)}60%{transform:translateY(-12px)}}
    </style>
	<circle class="st0 bounce" cx="13.8" cy="32" r="5"/>
	<circle class="st0 bounce" cx="31.8" cy="32" r="5"/>
	<circle class="st0 bounce" cx="49.8" cy="32" r="5"/>
</svg> --}}
<img class="dp-bl mg-lr-auto wd-ht-px-32 hidden dot-loader" src="{{config('services.'.config('website.assetSource.service').'.url', '')}}/assets/icons/dot-loader.svg" />
