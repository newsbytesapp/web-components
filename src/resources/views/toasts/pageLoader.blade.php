@if(array_get($data, 'css', false))
/* body.styles-loading>*{display:none;} */ body.styles-loading .page-loader{display:flex}body .page-loader{display:none;}{{--
.page-loader-svg .st0{clip-path:url(#SVGID_2_);fill:#C8091B}
.page-loader-svg .st1{clip-path:url(#SVGID_4_);fill:#F30017}
.page-loader-svg .st2{clip-path:url(#SVGID_6_);fill:#970412}
.page-loader-svg .st3{fill:none;stroke:#D5C8C9;stroke-miterlimit:10}.page-loader-svg .st4{fill:none;stroke:#F60000;stroke-miterlimit:10}
.page-loader-svg circle.spin{animation:1.4s ease-in-out infinite both circle-animation;display:block;fill:transparent;stroke:#f60000;stroke-linecap:round;stroke-dasharray:294;stroke-dashoffset:0;stroke-width:1px;transform-origin:50% 50%}
@keyframes circle-animation{0%{stroke-dashoffset:294;transform:rotate(-90deg)}50%{stroke-dashoffset:0;transform:rotate(-90deg)}100%{stroke-dashoffset:294;transform:rotate(270deg)}} --}}
@endif
@if(array_get($data, 'html', true))
<section class="page-loader z-index-3 wd-full ht-full pos-fix tp-init bt-init lt-init rt-init dp-fx fx-al-ct fx-js-ct bg-primary ovr-hidden">
    {{-- <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" style="width:100px; height:100px; enable-background:new 0 0 100 100;" xml:space="preserve" alt="Page Loader" title="Page Loader" class="page-loader-svg">
        <g>
            <g>
                <defs>
                    <polygon id="SVGID_1_" points="53.2,34.5 51.7,41.6 48.3,57.8 50.7,57 60.2,53.6 65,30.3"></polygon>
                </defs>
                <clippath id="SVGID_2_">
                    <use xlink:href="#SVGID_1_" style="overflow:visible;"></use>
                </clippath>
                <rect x="48.1" y="30" class="st0" width="17.2" height="28.1"/>
            </g>
            <g>
                <defs>
                    <polygon id="SVGID_3_" points="39.8,45.8 35,69.1 46.9,64.9 51.7,41.6"></polygon>
                </defs>
                <clippath id="SVGID_4_">
                    <use xlink:href="#SVGID_3_"  style="overflow:visible;"></use>
                </clippath>
                <rect x="34.7" y="41.3" class="st1" width="17.2" height="28.1"/>
            </g>
            <g>
                <defs>
                    <polygon id="SVGID_5_" points="51.7,41.6 50.7,57 48.3,57.8 51.7,41.6"></polygon>
                </defs>
                <clippath id="SVGID_6_">
                    <use xlink:href="#SVGID_5_"  style="overflow:visible;"></use>
                </clippath>
                <rect x="48.1" y="41.3" class="st2" width="3.9" height="16.8"/>
            </g>
        </g>
        <circle class="st3" cx="50" cy="49.7" r="47">
        </circle>
        <circle class="st4 spin" cx="50" cy="49.7" r="47"/>
    </svg> --}}
    <img style="width:100px; height:100px; enable-background:new 0 0 100 100;" alt="Page Loader" title="Page Loader" class="page-loader-svg" src="{{config('services.'.config('website.assetSource.service').'.url', '')}}/assets/icons/logo-loader.svg" />
</section>
@endif