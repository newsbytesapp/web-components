<div class="pos-fix wd-full lt-init bt-init notif-bell notifBell hidden ft-pr {{isset($classList) ? $classList : ''}}">
    <div class="wrapper-sm-variant">
        <section class="fx-row fx-al-st">
            <main class="col-12 col-lg-8 pos-rel">
                <div class="allowNotif pos-abs cs-ptr z-index-5 bell-holder circular-holder bg-primary br-rd-pr-50 dp-fx fx-al-ct fx-js-ct">
                    <div src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/images/notif-bell.png" alt="Notif Bell Icon" class="bell-icon bellIcon"></div>
                </div>
            </main>
        </section>
    </div>
</div>
@section('css')
@parent
<style type="text/css">
.notif-bell .bell-holder{bottom:70px;right:0px}.notif-bell .circular-holder{width:52px;height:52px;box-shadow:0 2px 22px 0 rgba(0,0,0,0.06)}.notif-bell .bell-icon{width:38px;height:38px;}@media screen and (min-width: 1025px) {.notif-bell .bell-holder{bottom:20px;left:-72px;}}
</style>
@stop
