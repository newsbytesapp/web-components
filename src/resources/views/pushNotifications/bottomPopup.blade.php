@switch($type)
@case('amp')
<div class="bg-primary wd-full br-rd-tl-tr-10px pd-l-20 pd-r-10 pd-t-20 pd-b-20 pop-up-box mg-lr-auto pos-rel ent-slide-up animation-dur-scale-4 ft-pr {{isset($classList) ? $classList : ''}}">
    <label for="dismissDialog" class="wd-ht-px-50 dp-fx fx-js-end fx-al-st cross-icon cancelNotif pos-abs cs-ptr z-index-1">
        <span class="cl-lt nb-icon-cancel-thin fz-22"></span>
    </label>
    <div class="dp-fx fx-js-bw fx-al-ct">
        <div>
            <p class="bd-4">{!! $data['title'] !!}</p>
            <p class="cl-lt reg-2">{!! $data['subtitle'] !!}</p>
            <button class="dp-ib bg-accent cl-primary br-rd-10 tx-ct bd-6ff-primary pd-t-10 pd-b-10 pd-l-20 pd-r-20 cs-ptr allowNotif mg-t-20" on="tap:amp-web-push.subscribe" role="button" tabindex="0">{!! $data['button']['title'] !!}</button>
        </div>
        <amp-img src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/images/categories/{{strtolower($data['tag'])}}.png" alt="{{$data['tag']}} Thumbnail" width="140" height="140" layout="fixed"></amp-img>
    </div>
</div>
@break
@default
<div class="pos-fix wd-full lt-init bt-init z-index-1 hidden push-notif bottom-popup subscribePopup ft-pr {{isset($classList) ? $classList : ''}}">
    <div class="wrapper-sm-variant">
        <section class="neg-mg-lr-px-10-lg-ac fx-row fx-al-st">
            <main class="col-12 col-lg-8 pd-l-lg-10 pd-r-lg-10">
                <div class="bg-primary wd-full br-rd-tl-tr-10px pd-l-20 pd-r-10 pd-t-20 pd-b-20 pop-up-box mg-lr-auto pos-rel ent-slide-up animation-dur-scale-4">
                    <div class="wd-ht-px-50 dp-fx fx-js-end fx-al-st cross-icon cancelNotif pos-abs cs-ptr z-index-1">
                        <span class="cl-lt nb-icon-cancel-thin fz-22"></span>
                    </div>
                    <div class="dp-fx fx-js-bw fx-al-ct">
                        <div>
                            <p class="md-bd-4 bd-4">{!! $data['title'] !!}</p>
                            <p class="cl-lt md-reg-2 reg-2">{!! $data['subtitle'] !!}</p>
                            <button class="dp-ib bg-accent cl-primary br-rd-10 tx-ct bd-6 md-bd-6 ff-primary pd-t-10 pd-b-10 pd-l-20 pd-r-20 cs-ptr allowNotif mg-t-20">{!! $data['button']['title'] !!}</button>
                        </div>
                        <img src="{{config('web-components.services.'.config('web-components.services.assetSource').'.url', '')}}/assets/images/categories/{{strtolower($data['tag'])}}.png" alt="{{$data['tag']}} Thumbnail" class="wd-ht-px-140 animation-dur-scale-4 image"/>
                    </div>
                </div>
            </main>
            <aside class="col-12 col-lg-4 pd-l-lg-10 pd-r-lg-10"></aside>
        </section>
    </div>
</div>
@break
@endswitch