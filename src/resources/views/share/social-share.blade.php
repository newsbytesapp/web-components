<?php
$componentId = array_get($data, 'id', floor(time()/(30*86400))); //str_random(6)
?>
@switch($type)
    @case('amp')
    <div class="dp-fx neg-mg-lr-px-10-ac {{isset($classList) ? $classList : ''}}">
        <amp-social-share type="facebook" data-target="_blank" data-param-url="{{$data['url']}}" class="mg-l-10 mg-r-10 custom-style br-rd-pr-50" width="45" height="45" data-param-app_id="479402082226194"></amp-social-share>
        <amp-social-share type="whatsapp" data-target="_blank" data-param-via="{{empty(array_get($data, 'source', '')) ? 'newsbytesapp' : $data['source']}}" data-param-text="{{$data['text']}} {{$data['url']}}" class="mg-l-10 mg-r-10 custom-style br-rd-pr-50" width="45" height="45"></amp-social-share>
        <amp-social-share type="twitter" data-target="_blank" data-param-via="{{empty(array_get($data, 'source', '')) ? 'newsbytesapp' : $data['source']}}" data-param-url="{{$data['url']}}" data-param-text="{{$data['text']}}" class="mg-l-10 mg-r-10 custom-style br-rd-pr-50" width="45" height="45"></amp-social-share>
        <amp-social-share type="linkedin" data-target="_blank" data-param-via="{{empty(array_get($data, 'source', '')) ? 'newsbytesapp' : $data['source']}}" data-param-url="{{$data['url']}}" class="mg-l-10 mg-r-10 custom-style br-rd-pr-50" width="45" height="45"></amp-social-share>
    </div>
    @break
    @default
        <div class="pos-rel ovr-initial drdown-parent ft-pr reg-3 md-reg-3 {{isset($classList) ? $classList : ''}}">
            <button class="dp-ib bg-primary cl-link br-rd-10 pd-l-30 pd-r-30 pd-l-md-40 pd-r-md-40 pd-t-14 pd-b-14 ft-ter md-ter-bd-2 ter-bd-2 navy-blue-cl-hover alice-blue-bg-hover drdown-selector cs-ptr" id="ss-{{$componentId}}">
                <div class="dp-fx fx-al-ct toggle-label" data-status="on">
                    <span class="nb-icon-social-share fz-24"></span>
                    @if(array_get($data, 'title', '') && (!empty($data['title'])))
                    <span class="pd-l-10">{!! $data['title'] !!}</span>
                    @else
                    <span class="pd-l-10">Share <span class="hidden-in-touch">this page</span></span>
                    @endif
                </div>
            </button>
            <div class="hidden pos-abs z-index-1 wd-full bg-primary box-shdw-drdown-content-box alice-blue-bg-hover-box pd-t-10 pd-b-10 br-rd-10 animation-dur-scale-2 drdown-box" data-animation-entrance="ent-slide-down-mg" data-animation-exit="ext-slide-up-mg" data-type="stateless">
                <a class="dp-fx fx-al-ct pd-l-20 pd-t-12 pd-b-12 cl-lt black-cl-hover alice-blue-bg-hover share-link" href="https://www.facebook.com/sharer/sharer.php?u={{$data['url']}}" target="_blank" rel="nofollow noreferrer" aria-label="Facebook" data-share-link="https://www.facebook.com/sharer/sharer.php?u=<url>">
                    <span class="nb-icon-facebook fz-17"></span>
                    <span class="pd-l-12">Facebook</span>
                </a>
                <a class="dp-fx fx-al-ct pd-l-20 pd-t-12 pd-b-12 cl-lt black-cl-hover alice-blue-bg-hover share-link hidden-md" href="whatsapp://send?text={{rawurlencode($data['text'])}}%20{{$data['url']}}" target="_blank" rel="nofollow noreferrer" aria-label="Whatsapp" data-share-link="whatsapp://send?text={{rawurlencode($data['text'])}}%20<url>">
                    <span class="nb-icon-whatsapp fz-17"></span>
                    <span class="pd-l-12">Whatsapp</span>
                </a>
                <a class="dp-fx fx-al-ct pd-l-20 pd-t-12 pd-b-12 cl-lt black-cl-hover alice-blue-bg-hover share-link" href="https://twitter.com/intent/tweet?via={{empty(array_get($data, 'source', '')) ? 'newsbytesapp' : $data['source']}}&text={{rawurlencode($data['text'])}}&url={{$data['url']}}" target="_blank" aria-label="Twitter" rel="nofollow noreferrer" data-share-link="https://twitter.com/intent/tweet?via={{empty(array_get($data, 'source', '')) ? 'newsbytesapp' : $data['source']}}&text={{rawurlencode($data['text'])}}&url=<url>">
                    <span class="nb-icon-twitter fz-17"></span>
                    <span class="pd-l-12">Twitter</span>
                </a>
                <a class="dp-fx fx-al-ct pd-l-20 pd-t-12 pd-b-12 cl-lt black-cl-hover alice-blue-bg-hover share-link" href="https://www.linkedin.com/shareArticle?mini=true&source={{empty(array_get($data, 'source', '')) ? 'newsbytes' : $data['source']}}&url={{$data['url']}}" target="_blank" rel="nofollow noreferrer" aria-label="Linkedin" data-share-link="https://www.linkedin.com/shareArticle?mini=true&source={{empty(array_get($data, 'source', '')) ? 'newsbytes' : $data['source']}}&url=<url>">
                    <span class="nb-icon-linkedin fz-17"></span>
                    <span class="pd-l-12">Linkedin</span>
                </a>
            </div>
        </div>
    @break
@endswitch