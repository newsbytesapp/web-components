<div class="dp-fx fx-js-ct wd-full {{isset($classList) ? $classList : ''}}">
    <div class="pd-l-10 pd-r-10">
        <a class="dp-fx fx-al-ct fx-js-ct wd-ht-px-40 bg-white-smoke cl-ink-dk br-rd-pr-50" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$data['url']}}" rel="nofollow noreferrer" aria-label="Facebook">
            <span class="nb-icon-facebook fz-16 mg-t-6"></span>
            <span class="hidden">Facebook</span>
        </a>
    </div>
    <div class="pd-l-10 pd-r-10 hidden-md">
        <a class="dp-fx fx-al-ct fx-js-ct wd-ht-px-40 bg-white-smoke cl-ink-dk br-rd-pr-50" target="_blank" href="whatsapp://send?text={{rawurlencode($data['text'])}}%20{{$data['url']}}" rel="nofollow noreferrer" aria-label="Whatsapp">
            <span class="nb-icon-whatsapp fz-16 mg-t-6"></span>
            <span class="hidden">Whatsapp</span>
        </a>
    </div>
    <div class="pd-l-10 pd-r-10">
        <a class="dp-fx fx-al-ct fx-js-ct wd-ht-px-40 bg-white-smoke cl-ink-dk br-rd-pr-50" target="_blank" href="https://twitter.com/intent/tweet?via={{empty(array_get($data, 'source', '')) ? 'newsbytesapp' : $data['source']}}&text={{rawurlencode($data['text'])}}&url={{$data['url']}}" rel="nofollow noreferrer" aria-label="Twitter">
            <span class="nb-icon-twitter fz-16 mg-t-6"></span>
            <span class="hidden">Twitter</span>
        </a>
    </div>
    <div class="pd-l-10 pd-r-10">
        <a class="dp-fx fx-al-ct fx-js-ct wd-ht-px-40 bg-white-smoke cl-ink-dk br-rd-pr-50" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&source={{empty(array_get($data, 'source', '')) ? 'newsbytes' : $data['source']}}&url={{$data['url']}}" rel="nofollow noreferrer" aria-label="Linkedin">
            <span class="nb-icon-linkedin fz-16 mg-t-6"></span>
            <span class="hidden">Linkedin</span>
        </a>
    </div>
</div>