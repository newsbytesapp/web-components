
@switch($type)
    @case('amp')
    <div class="dp-fx neg-mg-lr-px-10-ac {{isset($classList) ? $classList : ''}}">
        <amp-social-share type="facebook" data-target="_blank" data-param-url="{{$data['url']}}" class="mg-l-10 mg-r-10 custom-style br-rd-pr-50" width="45" height="45" data-param-app_id="479402082226194"></amp-social-share>
        <amp-social-share type="whatsapp" data-target="_blank" data-param-via="{{empty(array_get($data, 'source', '')) ? 'newsbytesapp' : $data['source']}}" data-param-text="{{$data['text']}} {{$data['url']}}" class="mg-l-10 mg-r-10 custom-style br-rd-pr-50" width="45" height="45"></amp-social-share>
        <amp-social-share type="twitter" data-target="_blank" data-param-via="{{empty(array_get($data, 'source', '')) ? 'newsbytesapp' : $data['source']}}" data-param-url="{{$data['url']}}" data-param-text="{{$data['text']}}" class="mg-l-10 mg-r-10 custom-style br-rd-pr-50" width="45" height="45"></amp-social-share>
        <amp-social-share type="linkedin" data-target="_blank" data-param-via="{{empty(array_get($data, 'source', '')) ? 'newsbytesapp' : $data['source']}}" data-param-url="{{$data['url']}}" class="mg-l-10 mg-r-10 custom-style br-rd-pr-50" width="45" height="45"></amp-social-share>
    </div>
    @break
    @default
        <div class="fx-row cl-ink-dk {{isset($classList) ? $classList : ''}}">
            <a href="https://www.facebook.com/sharer/sharer.php?u={{$data['url']}}" target="_blank" rel="nofollow noreferrer" aria-label="Facebook" class="dp-fx wd-ht-px-50 fx-al-ct fx-js-ct">
                <span class="hidden">Facebook</span>
                <span class="nb-icon-facebook fz-14"></span>
            </a>
            <a href="whatsapp://send?text={{rawurlencode($data['text'])}}%20{{$data['url']}}" target="_blank" rel="nofollow noreferrer" aria-label="Whatsapp" class="hidden-md dp-fx wd-ht-px-50 fx-al-ct fx-js-ct">
                <span class="hidden">Whatsapp</span>
                <span class="nb-icon-whatsapp fz-14"></span>
            </a>
            <a href="https://twitter.com/intent/tweet?via={{empty(array_get($data, 'source', '')) ? 'newsbytesapp' : $data['source']}}&text={{rawurlencode($data['text'])}}&url={{$data['url']}}" target="_blank" rel="nofollow noreferrer" aria-label="Twitter" class="dp-fx wd-ht-px-50 fx-al-ct fx-js-ct">
                <span class="hidden">Twitter</span>
                <span class="nb-icon-twitter fz-14"></span>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&source={{empty(array_get($data, 'source', '')) ? 'newsbytes' : $data['source']}}&url={{$data['url']}}" target="_blank" rel="nofollow noreferrer" aria-label="Linkedin" class="dp-fx wd-ht-px-50 fx-al-ct fx-js-ct">
                <span class="hidden">Linkedin</span>
                <span class="nb-icon-linkedin fz-14"></span>
            </a>
        </div>
    @break
@endswitch