<?php

return [
    'services' => [

        's3' => [
            'url' => env('S3_URL_PREFIX', ''),
        ],

        'cloudfront' => [
            'url' => env('CLOUDFRONT_URL_PREFIX', ''),
        ],

        'imagekit' => [
            'url' => env('IMAGEKIT_URL_PREFIX', ''),
        ],

        'akamaiimage' => [
            'url' => env('AKAMAI_IMAGE_URL_PREFIX', ''),
        ],

        'akamaiasset' => [
            'url' => env('AKAMAI_ASSET_URL_PREFIX', ''),
        ],

        'analytics' => [
            'ga' => env('GOOGLE_TRACKING_ID', ''),
            'alexa' => env('ALEXA_TRACKING_ID', ''),
            'pixel' => env('FB_PIXEL_ID', ''),
            'comscore' => env('COMSCORE_TRACKING_ID', ''),
            'matomo' => [
                'id' => env('MATOMO_ID', ''),
                'active' => env('MATOMO_ACTIVE', 'off'),
            ],
        ],

        'firebase' => [
            'projectId' => env('FIREBASE_APP_ID'),
            'projectKey' => env('FIREBASE_APP_KEY'),
            'messagingId' => env('FIREBASE_MESSAGING_ID'),
            'credentials' => env('FIREBASE_CREDENTIALS', ''),
        ],

        'assetSource' => env('ASSET_SOURCE_SERVICE', ''),
        'imageSource' => env('IMAGE_SOURCE_SERVICE', ''),
    ],

    'colorMap' => [
        'blue-violet' => [],
        'persimmon' => [],
        'default' => ['default'],
        'cerulean-blue' => [],
        'indigo' => [],
        'pure-gold' => [],
        'kermit-green' => [],
        'iris-blue' => [],
        'razzmatazz' => [],
        'windows-blue' => [],
        'mountain-meadow' => [],
        'citrus' => [],
        'rusty-orange' => [],
        'electric-purple' => [],
        'spark-red' => [],
        'acadia' => [],
        'lily-white-gray-nurse' => [],
        'blumine' => [],
        'sweet-pink' => [],
        'froly' => [],
        'tangerine-yellow' => [],
    ],

    'advt' => [
        /* 'clientId' => '',
        'slotIds' => [],
        'slotIdsDfp' => [],
        'slotAttrs' => [], */

        'slots' => [],
    ],

    'appVersion' => env('APP_VERSION', ''),

    'oneSignalSafariPushId' => env('ONESIGNAL_SAFARI_PUSH_ID', ''),

    'oneSignalWebPushId' => env('ONESIGNAL_WEB_PUSH_ID', ''),

    'widgets' => [
        'ticker' => [
            'status' => false,
            'model' => 1,
            'route' => '',
        ],
    ],

    'nblocale' => env('NB_LOCALE', 'en'),
];
