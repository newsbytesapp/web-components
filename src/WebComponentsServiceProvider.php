<?php

namespace NewsBytes\WebComponents;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use NewsBytes\WebComponents\App\Services\ColorMapper;

class WebComponentsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/resources/views/', 'web-components');
        $this->publishes([
            __DIR__.'/config/web-components.php' => config_path('web-components.php'),
        ]);
        if ($this->app->runningInConsole()) {
            $this->commands([
                App\Console\Commands\Publish::class,
            ]);
        }
        $this->app->singleton('ColorMap', function ($app) {
            return ColorMapper::createCategoryMap();
        });
        Blade::directive('nbColor', function ($expression) {
            return "<?php echo \NewsBytes\WebComponents\App\Services\ColorMapper::color($expression); ?>";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
