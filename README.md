# NewsBytes UI Layer

## Todo
1. Establish guidelines for newly introduced V3 module
2. Establish Design system guidelines.
3. Improve `v3.cards.chats`.
4. Shift new svg custom logo mechanism to V3 module.
5. Empty data case to be applied globally/across the directory.

## Installation
1. execute `composer require newsbytesapp\web-components`
2. execute `php artisan vendor:publish`
3. execute `php artisan publish:package`

The configuration published has all the relevant env parameters. Please update them in the env file as required.

For configuring sass, refer to `'vendor/newsbytesapp/web-components/src/assets/sass/<filename>.scss'`.
For configuring js, refer to `'vendor/newsbytesapp/web-components/src/assets/js/<filename>.js'`.

## Usage
All the components are published in `web-components` namespace. To use any component, one can refer it like `web-components::<folder>.<file>`. For example, refer to basic button, call `@include('web-components::buttons.basic', $data)`.

All the components have specific data format referred as contracts.